<?php

/**		
 * @author		Dwebgame	
 * @copyright	2014 - 2015	
 * @website		http://dwebgame.net	
 * @HotLine		0169 330 22 84	
 * @Version		v1.05.62		
*/	

include('checklic.php');
include('request_count.php');

include('func_item.php');

$itemdata_arr = ItemDataArr();

$action = $_POST['action'];

$info = 'OK';

switch ($action){ 
	case 'longcondor_check_item':
    case 'longcondor_quay':
        $warehouse1 = $_POST['warehouse1'];
		$item_success = "350000123456780000D0000000000000";

        
        $check_info_arr = array();
        $item_nguyenlieu = array();
        
        // Define
        $percent_max = $_POST['percent_max'];			$percent_max = abs(intval($percent_max));
        $percent_luck = $_POST['percent_luck'];			  $percent_luck = abs(intval($percent_luck));
        $percent_skill = $_POST['percent_skill'];			$percent_skill = abs(intval($percent_skill));
        $percent_ancient_lv = $_POST['percent_ancient_lv'];		$percent_ancient_lv = abs(intval($percent_ancient_lv));
        
        
        $percent = 1;
        for($i=0; $i<=4; $i++) {
            $item_arr[$i]['slg'] = 0;
            $item_arr[$i]['check'] = 0; 
        }
        $percent_ancient = $percent_ancient_lv;
        
        
        for($i=0; $i<32; ++$i) {
            $item = substr($warehouse1,$i*32,32);
            if($item != 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF') {
                $item_info = ItemInfo($itemdata_arr, $item);

                $item_info['vitri'] = $i;
                
                if((ceil(($item_info['vitri']+1)/8) + $item_info['y'] - 1) <= 4) {
                    $item_serial = $item_info['serial'];
                    if( hexdec($item_info['serial']) == 4294967280 ) {
                        $err .= $item_info['name'] . " đang được bảo vệ. Không thể sử dụng để xoay Lông Vũ Condor.<br />";
                    }
                            
                    if( $item_info['group'] == 12 && $item_info['id'] == 15 && $item_info['level'] == 0 ) { // Chao
                        ++$item_arr[0]['slg'];
                        $item_arr[0]['check']   = 1;
                        $item_nguyenlieu[] = $item;
                        $itemview_arr[] = array(
							'slg'	=> $item_arr[0]['slg'],
                            'info'  =>  $item_info['info']
                        );
                    } elseif( $item_info['group'] == 14 && $item_info['id'] == 22 && $item_info['level'] == 0 ) {   // Cre
                        ++$item_arr[1]['slg'];
                        $item_arr[1]['check']   = 1;
                        $item_nguyenlieu[] = $item;
                        $itemview_arr[] = array(
							'slg'	=> $item_arr[1]['slg'],
                            'info'  =>  $item_info['info']
                        );
                    } elseif( $item_info['group'] == 12 && $item_info['id'] == 31 && $item_info['level'] == 0 ) {   // Soul+1
                        ++$item_arr[2]['slg'];
                        $item_arr[2]['check']   = 1;
                        $item_nguyenlieu[] = $item;
                        $itemview_arr[] = array(
							'slg'	=> $item_arr[2]['slg'],
                            'info'  =>  $item_info['info']
                        );
                    } elseif( $item_info['type'] == 3 ) {   // Wing 2
                        ++$item_arr[3]['slg'];
                        if($item_info['level'] >= 9 && $item_info['opt'] >= 1 ) {
                            $item_arr[3]['check']   = 1;
                            $item_nguyenlieu[] = $item;
                        } else {
                            $item_arr[3]['check']   = 2;    // Kg dat dieu kien
                            $err .= $item_info['name'] . " không thể sử dụng để xoay Lông Vũ Condor.<br />";
                        }
                        
                        $itemview_arr[] = array(
							'slg'	=> $item_arr[3]['slg'],
                            'info'  =>  $item_info['info']
                        );
                    } elseif( $item_info['exc_anc'] == 2 ) {   // Item Than
                        if($item_info['level'] >= 7 && $item_info['opt'] >= 1 ) {
                            $item_arr[4]['slg'] = 1;
                            $item_arr[4]['check']   = 1;
                            $item_nguyenlieu[] = $item;
                            
                            if($item_info['level'] == 7) {
                                $percent_ancient = 2;
                            } else {
                                $lv_multi = $item_info['level']-7;
                                for($j=0; $j<$lv_multi; $j++) {
                                    $percent_ancient *= $percent_ancient_lv;
                                }
                            }
                            
                            if($item_info['luck'] == 1) $percent_ancient += $percent_luck;
                            if($item_info['skill'] == 1) $percent_ancient += $percent_skill;
                            
                            $percent += $percent_ancient;
                            
                            $itemview_arr[] = array(
								'slg'	=> $item_arr[4]['slg'],
                                'info'  =>  $item_info['info'],
                            );
                        } else {
                            $item_arr[4]['check']   = 2;    // Kg dat dieu kien
                            $err .= $item_info['name'] . " không thể sử dụng để xoay Lông Vũ Condor.<br />";
                        }
                    } else {
                        $err .= $item_info['name'] . " không thể sử dụng để xoay Lông Vũ Condor.<br />";
                    }
                }
            }
        }
        
        if($percent > $percent_max) $percent = $percent_max;
        for($i=0; $i<=4; $i++) {
            switch ($i){ 
            	case 0:
                    $item_name = "Ngọc Hỗn Nguyên";
					$item_code = "0F00008BBE7A000000C0000000000000";
            	break;
            
            	case 1:
                    $item_name = "Ngọc Sáng Tạo";
					$item_code = "1600008BBE7A000000E0000000000000";
            	break;
            
            	case 2:
                    $item_name = "Cụm Ngọc Tâm Linh +1";
					$item_code = "1F00008BBE7A000000C0000000000000";			
            	break;
                
                case 3:
                    $item_name = "Cánh Cấp 2 +9 +Tự động hồi phục HP+4% trở lên";
					$item_code = "";
            	break;
                
                case 4:
                    $item_name = "Item Thần +7 +4op trở lên";
					$item_code = "";
            	break;
            }
            if($item_arr[$i]['check'] == 0) {
                $err .= "Thiếu <strong>$item_name</strong>.<br />";
            }
            if($item_arr[$i]['slg'] > 1) {
                $err .= "Số lượng <strong>$item_name</strong> vượt quá mức quy định.<br />";
            }
        }
        
        $check_info_arr['item'] = $item_arr;
        $check_info_arr['item_view'] = $itemview_arr;
        $check_info_arr['percent'] = $percent;
        $check_info_arr['err'] = $err;
        $check_info_arr['accept'] = 0;
        if($item_arr[0]['check'] == 1 && $item_arr[1]['check'] == 1 && $item_arr[2]['check'] == 1 && $item_arr[3]['check'] == 1 && $item_arr[4]['check'] == 1 && strlen($check_info_arr['err']) == 0) $check_info_arr['accept'] = 1;
        
        
        switch ($action) {
        	case 'longcondor_quay':
                $serial = $_POST['serial'];
                $msg = '';
                $success = 0;
                $item_nguyenlieu_data = '';
                $item_epsuccess = '';
                
                if($check_info_arr['accept'] == 1) {
                    
                    $empty_item = '';
                    for($i=0; $i<32*8*4; $i++) {
                        $empty_item .= 'F';
                    }
                    
                    $warehouse1_new = substr_replace($warehouse1, $empty_item, 0, 32*8*4);
                    
                    $rand = rand(1, 99);
                    if($rand <= $percent) {
                        $success = 1;   // Ep thanh cong
                        $item_epsuccess = substr_replace($item_success, $serial, 6, 8);
                        $warehouse1_new = substr_replace($warehouse1_new, $item_epsuccess, 0, 32);
                    } else {
                        $success = 2;   // Ep xit
                    }
                    
                    $item_nguyenlieu_data = json_encode($item_nguyenlieu);
                }
                
                echo "
                    <info>OK</info>
                    <reponse>$success</reponse>
                    <warehouse1>$warehouse1_new</warehouse1>
                    <item_nguyenlieu>$item_nguyenlieu_data</item_nguyenlieu>
                    <item_epsuccess>$item_epsuccess</item_epsuccess>
                ";
        	break;
			
        
        	default :
                
                $check_info = json_encode($check_info_arr);
                echo "
                    <info>OK</info>
                    <check_info>" . $check_info ."</check_info>
                ";
        }
        
  
    break;
	
	case 'trader_getimg':
            $trader_item_dis_code = $_POST['trader_item_dis_code'];
            $trader_item_source_slg = $_POST['trader_item_source_slg'];
            $trader_item_source = $_POST['trader_item_source'];
            
			$item_dis_slg = floor(strlen($trader_item_dis_code)/32);
			$trader_item_dis_arr = array();
            $trader_item_source_arr = array();
			
			for($x=0; $x<$item_dis_slg; ++$x) {
				$item1 = substr($trader_item_dis_code,$x*32,32);
                if($item1 != 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF') {
                    $item_info1 = ItemInfo($itemdata_arr, $item1);
					$trader_item_dis_arr []= array(
						'img'  =>  $item_info1['image'],
						'name'  =>  $item_info1['name']
					);
                }
            }
			for($i=0; $i<$trader_item_source_slg; ++$i) {
				$item2 = $trader_item_source[$i]['code'];
                if($item2 != 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF') {
                    $item_info2 = ItemInfo($itemdata_arr, $item2);
					$trader_item_source_arr [$i]['name']= $item_info2['name'];
					$trader_item_source_arr [$i]['img']= $item_info2['image'];
                }
            }
            $trader_item_dis_data = json_encode($trader_item_dis_arr);
            $trader_item_source_data = json_encode($trader_item_source_arr);
            echo "
                <info>OK</info>
				<trader_item_dis_data>" . $trader_item_dis_data ."</trader_item_dis_data>
                <trader_item_source_data>" . $trader_item_source_data ."</trader_item_source_data>
            ";
       break;
        
	case 'trader_check_item':
    case 'trader_quay':
        $warehouse1 = $_POST['warehouse1'];
        
        $check_info_arr = array();
        $item_nguyenlieu = array();
		$trader_group_arr = array();
		$trader_id_arr = array();
        
        // Define
		$trader_percent = $_POST['trader_percent'];
		$trader_item_source_arr = $_POST['trader_item_source_arr'];
		$trader_item_count = count($trader_item_source_arr);
        
        
        for($x=0; $x<$trader_item_count; ++$x) {
            $itemview_arr[$x]['slg_api'] = 0;
            $itemview_arr[$x]['check'] = 0; 
			$item_getinfo[$x]	= ItemInfo($itemdata_arr, $trader_item_source_arr[$x]['code']);
        }
        
        
        for($i=0; $i<32; ++$i) {
            $item = substr($warehouse1,$i*32,32);
            if($item != 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF') {
                $item_info = ItemInfo($itemdata_arr, $item);

                $item_info['vitri'] = $i;
                
                if((ceil(($item_info['vitri']+1)/8) + $item_info['y'] - 1) <= 4) {
                    $item_serial = $item_info['serial'];
                    if( hexdec($item_info['serial']) == 4294967280 ) {
                        $err .= $item_info['name'] . " đang được bảo vệ. Không thể sử dụng để đổi.<br />";
                    }
					    for($trader_i=0; $trader_i<$trader_item_count; ++$trader_i) {
							if(!isset($trader_item_source_arr[$trader_i]['luck'])) $luck = 0; else $luck = 1;
							if(!isset($trader_item_source_arr[$trader_i]['skill'])) $skill = 0; else $skill = 1;
							if( $item_info['group'] == $item_getinfo[$trader_i]['group'] && $item_info['id'] == $item_getinfo[$trader_i]['id'] && $item_info['level'] == $item_getinfo[$trader_i]['level'] && $item_info['exc_opt'] == $item_getinfo[$trader_i]['exc_opt'] && $item_info['exc_opt'] >= $trader_item_source_arr[$trader_i]['exl'] && $item_info['opt'] >= $trader_item_source_arr[$trader_i]['option'] && $item_info['luck'] == $luck && $item_info['skill'] >= $skill) { 
								++$itemview_arr[$trader_i]['slg_api'];
								$itemview_arr[$trader_i]['check']  = 1;
								$item_nguyenlieu[] = $item;
								$itemview_arr[$trader_i]['vitri'][] = $i;
							}
							if(($item_info['level'] != $item_getinfo[$trader_i]['level']) || ($item_info['group'] != $item_getinfo[$trader_i]['group'] && $item_info['level'] != $item_getinfo[$trader_i]['level'])) { 
								$err .= $item_info['name'] . " không thể sử dụng để đổi.<br />";
							} 						
						}											
                }
            }
        }
        $check_accept = 0;
		$itemview_arr[0]['tyle'] = 1;
        for($j=0; $j<$trader_item_count; $j++) {						
            if($itemview_arr[$j]['check'] == 0) {
                $err .= "Thiếu <strong>" . $trader_item_source_arr[$j]['name'] . "</strong>.<br />";
            }
            if($itemview_arr[$j]['slg_api'] > $trader_item_source_arr[$j]['slg']) {
                $itemview_arr[$j]['tyle'] = floor($itemview_arr[$j]['slg_api']/$trader_item_source_arr[$j]['slg']);
				if($itemview_arr[0]['tyle']>=$itemview_arr[$j]['tyle']) $itemview_arr[0]['tyle'] = $itemview_arr[$j]['tyle'];
            }
			$check_accept +=$itemview_arr[$j]['check'];
        }
        
        $check_info_arr['item'] = $itemview_arr;
        $check_info_arr['percent'] = $trader_percent;
        $check_info_arr['err'] = $err;
        $check_info_arr['accept'] = 0;
		
        if($check_accept > 0 && strlen($check_info_arr['err']) == 0) $check_info_arr['accept'] = 1;
       
        
        
        switch ($action) {
        	case 'trader_quay':
				$trader_item_dis_code = $_POST['trader_item_dis_code'];
                $serial = $_POST['serial'];
                $msg = '';
                $success = 0;
                $item_nguyenlieu_data = '';
                $item_epsuccess = '';

                if($check_info_arr['accept'] == 1) {
                    $warehouse1_new = $warehouse1;
					for($o=0; $o<$trader_item_count; ++$o) {						
						for($vitri_i=0; $vitri_i<($trader_item_source_arr[$o]['slg']*$itemview_arr[0]['tyle']); ++$vitri_i) {
							$empty_item = $itemview_arr[$o]['vitri'];
							$warehouse1_new = substr_replace($warehouse1_new, 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', $empty_item[$vitri_i]*32, 32);
						}
					}
                    $rand = rand(1, 99);
                    if($rand <= $trader_percent) {
                        $success = 1;   // Ep thanh cong
						$item_dis_slg = floor(strlen($trader_item_dis_code)/32);
						for($i=0; $i<$item_dis_slg; ++$i) {
							$item = substr($trader_item_dis_code,$i*32,32);
							$item_getcode = GetCode($item);
							$item_info1 = ItemsData($itemdata_arr, $item_getcode['id'],$item_getcode['group'],$item_getcode['level']);
										
							$item_x = $item_info1['x'];
							$item_y = $item_info1['y'];
							for($tyle_i=0; $tyle_i<$itemview_arr[0]['tyle']; ++$tyle_i) {
								$slot_accept = CheckSlot($itemdata_arr, $warehouse1_new,$item_x,$item_y);
								if(is_numeric($slot_accept)){
									$item_epsuccess = substr_replace($item, $serial[$tyle_i], 6, 8);
									$warehouse1_new = substr_replace($warehouse1_new, $item_epsuccess, ($slot_accept-1)*32, 32);
								}      
							}
						}
                    } else {
                        $success = 2;   // Ep xit
                    }
                    
                    $item_nguyenlieu_data = json_encode($item_nguyenlieu);
                }
                
                echo "
                    <info>OK</info>
                    <reponse>$success</reponse>
                    <warehouse1>$warehouse1_new</warehouse1>
                    <item_nguyenlieu>$item_nguyenlieu_data</item_nguyenlieu>
                    <item_epsuccess>$item_epsuccess</item_epsuccess>
                ";
        	break;
        
        	default :
                
                $check_info = json_encode($check_info_arr);
                echo "
                    <info>OK</info>
                    <check_info>" . $check_info ."</check_info>
                ";
        }
        
        
    break;
    
}

?>