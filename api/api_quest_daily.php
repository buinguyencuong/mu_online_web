<?php

/**		
 * @author		Dwebgame	
 * @copyright	2014 - 2015	
 * @website		http://dwebgame.net	
 * @HotLine		0169 330 22 84	
 * @Version		v1.05.62		
*/	

include('checklic.php');
include('func_item.php');

$itemdata_arr = ItemDataArr();

$type = $_POST['type'];

if($type=='item_time'){
	// Thuc hien chuc nang
	$item_time = $_POST['item_time'];
	$item_time_gift_arr = array();
	$item_time_itemtotal = floor(strlen($item_time)/32);
		for($i=0; $i<$item_time_itemtotal; ++$i) {
			$item = substr($item_time,$i*32,32);
			if($item != 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF') {
				$item_getcode = GetCode($item);
				$Num = $item_getcode['Num'];
				$Lvl = $item_getcode['Lvl'];
				$Opt = $item_getcode['Opt'];
				$Luck = $item_getcode['Luck'];
				$Skill = $item_getcode['Skill'];
				$Dur = $item_getcode['Dur']; 
				$Excellent = $item_getcode['Excellent'];
				$Ancient = $item_getcode['Ancient'];
				$JOH = $item_getcode['JOH'];
				$Sock1 = $item_getcode['Sock1'];
				$Sock2 = $item_getcode['Sock2'];
				$Sock3 = $item_getcode['Sock3'];
				$Sock4 = $item_getcode['Sock4'];
				$Sock5 = $item_getcode['Sock5'];
		
				$item_time_gift_arr[] = array(
					'Num'	=> $Num,
					'Lvl' => $Lvl,
					'Opt' => $Opt,
					'Luck' => $Luck,
					'Skill' => $Skill,
					'Dur' => $Dur,
					'Excellent' => $Excellent,
					'Ancient' => $Ancient,
					'JOH' => $JOH,
					'Sock1' => $Sock1,
					'Sock2' => $Sock2,
					'Sock3' => $Sock3,
					'Sock4' => $Sock4,
					'Sock5' => $Sock5,
					'Days'	=> $day,
					'SerialFFFFFFFE'	=> 0
				);
			}
		}

		$item_time = json_encode($item_time_gift_arr);
		echo "
			<info>OK</info>
			<item_time>" . $item_time ."</item_time>
		";
} else {
	// Thuc hien chuc nang
	$item_read = $_POST['item'];
	$warehouse1 = $_POST['warehouse1'];

	$info = "OK";
	$msg = "";
	$warehouse1_check = $warehouse1;
	$item_choise_itemtotal = floor(strlen($item_read)/32);
	$item_gift_arr = array();

	if(strlen($warehouse1) < 8*4*32) {
		$info = "Error";
		$msg = "Rương đồ chung chưa mở. Vui lòng vào nhân vật, mở Rương đồ chung rồi nhận lại.";
	} else {
		for($i=0; $i<$item_choise_itemtotal; ++$i) {
			$item = substr($item_read,$i*32,32);
			if($item != 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF') {
				$item_getcode = GetCode($item);
				$item_info = ItemsData($itemdata_arr, $item_getcode['id'],$item_getcode['group'],$item_getcode['level']);
						
				$item_x = $item_info['x'];
				$item_y = $item_info['y'];
				
				$slot_accept = CheckSlot($itemdata_arr, $warehouse1_check,$item_x,$item_y);
				
				if($slot_accept == 0) {
					$info = "Error";
					$msg = "Rương đồ chung không đủ chỗ chứa Item. Vui lòng dọn dẹp lại Rương đồ chung.";
					break;
				} else {
					$warehouse1_check = substr_replace($warehouse1_check, $item, ($slot_accept-1)*32, 32);
					$vitri = $slot_accept-1;
					$item_gift_arr[] = array(
						'code'  =>  $item,
						'vitri' =>  $vitri
					);
				}
			}
		}
	}

		

		$item_data = json_encode($item_gift_arr);
		echo "
			<info>$info</info>
			<msg>$msg</msg>
			<item_data>" . $item_data ."</item_data>
		";
}
?>