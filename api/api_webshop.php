<?php

/**		
 * @author		Dwebgame	
 * @copyright	2014 - 2015	
 * @website		http://dwebgame.net	
 * @HotLine		0169 330 22 84	
 * @Version		v1.05.62		
*/	
include('checklic.php');
include('func_item.php');

$itemdata_arr = ItemDataArr();

$action = $_POST['action'];

switch ($action){ 
    
    case 'webshop_cfg_add':
        $code = $_POST['code'];
        
        $item_info = ItemInfo($itemdata_arr, $code);
        
        $item_webshop_info['code'] = $code;
        $item_webshop_info['name'] = $item_info['name'];
		$item_webshop_info['name_en'] = $item_info['name_en'];
        $item_webshop_info['image'] = $item_info['image'];
        $item_webshop_info['info'] = $item_info['info'];
        $item_webshop_info['exl_type'] = $item_info['type'];
        
        if($item_info['exc_anc'] == 2) {
            $item_webshop_info['webshop_type'] = 14;
        } else {
            switch ($item_info['group']){ 
            	case 0:
                    $item_webshop_info['webshop_type'] = 2;
            	break;
            
            	case 1:
                    $item_webshop_info['webshop_type'] = 5;
            	break;
            
            	case 2:
				  if($item_info['id']==5)  $item_webshop_info['webshop_type'] = 2;
				  else $item_webshop_info['webshop_type'] = 5;                  
            	break;
            
            	case 3:
                    $item_webshop_info['webshop_type'] = 5;
            	break;
            
            	case 4:
                    $item_webshop_info['webshop_type'] = 4;
            	break;
            
            	case 5:
                    $item_webshop_info['webshop_type'] = 3;
            	break;
            
            	case 6:
                    $item_webshop_info['webshop_type'] = 6;
            	break;
            
            	case 7:
                    $item_webshop_info['webshop_type'] = 7;
            	break;
            
            	case 8:
                    $item_webshop_info['webshop_type'] = 8;
            	break;
            
            	case 9:
                    $item_webshop_info['webshop_type'] = 9;
            	break;
            
            	case 10:
                    $item_webshop_info['webshop_type'] = 10;
            	break;
            
            	case 11:
                    $item_webshop_info['webshop_type'] = 11;
            	break;
            
            	case 12:
                    if(in_array($item_info['id'], array(0, 1, 2))) {
                        $item_webshop_info['webshop_type'] = 13;
                    } elseif(in_array($item_info['type'], array(3, 7, 8, 9))) {
                        $item_webshop_info['webshop_type'] = 13;
                    } else {
                        $item_webshop_info['webshop_type'] = 1;
                    }
                    
            	break;
            
            	case 13:
                    if(in_array($item_info['type'], array(3, 7, 8, 9))) {
                        $item_webshop_info['webshop_type'] = 13;
                    } elseif(in_array($item_info['type'], array(4, 5))) {
                        $item_webshop_info['webshop_type'] = 12;
                    } else {
                        $item_webshop_info['webshop_type'] = 1;
                    }
                    
            	break;
				
				case 14:
					if(in_array($item_info['id'], array(13, 14, 16, 22, 31, 41, 42, 43, 44))) {
                        $item_webshop_info['webshop_type'] = 1;
                    } else {
                        $item_webshop_info['webshop_type'] = 15;
                    }
            	break;
            
            	default :
                    $item_webshop_info['webshop_type'] = 1;
            }
        }
        
        $info = "OK";
        $message = "";
            
        $item_webshop = json_encode($item_webshop_info);
            
        echo "
            <info>$info</info>
            <message>" . $message ."</message>
            <item_webshop_info>" . $item_webshop ."</item_webshop_info>
        ";
    break;
    
    case 'webshop_buy':
        $data_send = $_POST['data_send'];
        $data_send_arr = unserialize_safe($data_send);
        $item = $data_send_arr['item_code'];
        $item_slg = $data_send_arr['item_slg'];
        $seri = $data_send_arr['seri'];
        
        $warehouse1 = $data_send_arr['warehouse1'];
        
        $item_getcode = GetCode($data_send_arr['item_code']);
        $item_info = ItemsData($itemdata_arr, $item_getcode['id'],$item_getcode['group'],$item_getcode['level']);
                
        $item_x = $item_info['x'];
        $item_y = $item_info['y'];
        
        $info = "OK";
        $msg = "";
        for($i=0; $i<$item_slg; $i++) {
            $slot_accept = CheckSlot($itemdata_arr, $warehouse1,$item_x,$item_y);
            
            if($slot_accept == 0) {
                $info = "Error";
                $msg = "Hòm đồ không đủ chỗ chứa Item. Vui lòng dọn dẹp lại hòm đồ.";
                break;
            } else {
                $item = substr_replace($item, $seri[$i], 6, 8);
                
                $warehouse1 = substr_replace($warehouse1, $item, ($slot_accept-1)*32, 32);
            }
        }
        
        echo "
            <info>$info</info>
            <msg>$msg</msg>
            <warehouse1>" . $warehouse1 ."</warehouse1>
        ";
    break;
	
	case 'webshop_exl_buy':
		$code = $_POST['code'];
		$luck = abs(intval($_POST['luck']));         if($luck != 1) $luck = 0;
		$exl[1] = abs(intval($_POST['exl1']));         if($exl[1] != 1) $exl[1] = 0;
		$exl[2] = abs(intval($_POST['exl2']));         if($exl[2] != 1) $exl[2] = 0;
		$exl[3] = abs(intval($_POST['exl3']));         if($exl[3] != 1) $exl[3] = 0;
		$exl[4] = abs(intval($_POST['exl4']));         if($exl[4] != 1) $exl[4] = 0;
		$exl[5] = abs(intval($_POST['exl5']));         if($exl[5] != 1) $exl[5] = 0;
		$exl[6] = abs(intval($_POST['exl6']));         if($exl[6] != 1) $exl[6] = 0;
		$lvl = abs(intval($_POST['lvl']));           if($lvl < 0 || $lvl > 15) $lvl = 0;
		$opt = abs(intval($_POST['opt']));           if($opt < 0 || $opt > 7) $opt = 0;
        $serial = $_POST['serial'];
        
        $warehouse1 = $_POST['warehouse1'];
		
		$item_getcode = GetCode($code);
        $item_info = ItemsData($itemdata_arr, $item_getcode['id'],$item_getcode['group'],$item_getcode['level']);
		// Cấp độ Item
		$exc_option = 0;
		if($opt<4) {
			$option		= $item_getcode['Opt']+$opt+$lvl*8;
		} else {
			if($opt==4) $option		= $item_getcode['Opt']+$lvl*8;
			if($opt==5) $option		= $item_getcode['Opt']+1+$lvl*8;
			if($opt==6) $option		= $item_getcode['Opt']+2+$lvl*8;
			if($opt==7) $option		= $item_getcode['Opt']+3+$lvl*8;
			$exc_option = 4;
		}
		// Item luck
		$option	= $option+$luck*4;
		
		$option = strtoupper(dechex($option));
		if(strlen($option)==1) $option = "0".$option;
		$item = substr_replace($code, $option, 2, 2);
		
		// Excellent Option
        $Excellent = 0;
        if($exl[1] == 1) $Excellent += 1;
        if($exl[2] == 1) $Excellent += 2;
        if($exl[3] == 1) $Excellent += 4;
        if($exl[4] == 1) $Excellent += 8;
        if($exl[5] == 1) $Excellent += 16;
        if($exl[6] == 1) $Excellent += 32;
		if($exc_option == 4)	$Excellent += 64;

		
		$Excellent = strtoupper(dechex($Excellent));
		if(strlen($Excellent)==1) $Excellent = "0".$Excellent;
        $item = substr_replace($item, $Excellent, 14, 2);
       
        $item_x = $item_info['x'];
        $item_y = $item_info['y'];

        $slot_accept = CheckSlot($itemdata_arr, $warehouse1,$item_x,$item_y);
		
        if(is_numeric($slot_accept)){
			$item = substr_replace($item, $serial, 6, 8);
			$warehouse1_new = substr_replace($warehouse1, $item, ($slot_accept-1)*32, 32);
			$info = "OK";
			$msg = "";
		}
		else{
			$info = "Error";
            $msg = "Hòm đồ không đủ chỗ chứa Item. Vui lòng dọn dẹp lại hòm đồ.";
		}
        echo "
            <info>$info</info>
            <msg>$msg</msg>
            <warehouse1>" . $warehouse1_new ."</warehouse1>
        ";
    break;
}

?>