<?php

/**		
 * @author		Dwebgame	
 * @copyright	2014 - 2015	
 * @website		http://dwebgame.net	
 * @HotLine		0169 330 22 84	
 * @Version		v1.05.62		
*/	

include('checklic.php');
include('request_count.php');

// Thuc hien chuc nang
$tiente = $_POST['tiente'];

if ($tiente == 'gcoin') {
    $gcoin_before = $_POST['gcoin_before'];
    $gcoin_km_before = $_POST['gcoin_km_before'];
    $gcoin_rs = $_POST['gcoin_rs'];
    
    if($gcoin_km_before >= $gcoin_rs) {
		$gcoin_km_after = $gcoin_km_before - $gcoin_rs;
		$gcoin_after = $gcoin_before;
	}	
    else {
        $gcoin_after = $gcoin_before - ($gcoin_rs - $gcoin_km_before);
        $gcoin_km_after = 0;
    }
    
    echo "
        <info>OK</info>
        <gcoin_after>" . $gcoin_after ."</gcoin_after>
        <gcoin_km_after>" . $gcoin_km_after ."</gcoin_km_after>
    ";
} else {
    $gcoin_reset_vip = $_POST['gcoin_reset_vip'];
    $vpoint_extra = $_POST['vpoint_extra'];
    $vpoint_before = $_POST['vpoint_before'];
	$vpoint_km_before = $_POST['vpoint_km_before'];
	
    $vpoint_reset_vip = floor($gcoin_reset_vip*(1+($vpoint_extra/100)));
	if($vpoint_km_before >= $vpoint_reset_vip) {
		$vpoint_km_after = $vpoint_km_before - $vpoint_reset_vip;
		$vpoint_after = $vpoint_before;
		echo "
				<info>OK</info>
				<vpoint_after>" . $vpoint_after ."</vpoint_after>
				<vpoint_km_after>" . $vpoint_km_after ."</vpoint_km_after>
			";	
	} 
    else {
		if ($vpoint_before+$vpoint_km_before < $vpoint_reset_vip) 
		{
			echo "
				<info>Error</info>
				<message>Không có đủ Vpoint yêu cầu Reset. Bạn cần có $vpoint_reset_vip Gcent chi phí Reset VIP</message>
			";
		}
		else {
		   $vpoint_after = $vpoint_before - ($vpoint_reset_vip - $vpoint_km_before);
		   $vpoint_km_after = 0;
		   echo "
				<info>OK</info>
				<vpoint_after>" . $vpoint_after ."</vpoint_after>
				<vpoint_km_after>" . $vpoint_km_after ."</vpoint_km_after>
			";	
		}        
    }		
}
?>