<?php

/**		
 * @author		Dwebgame	
 * @copyright	2014 - 2015	
 * @website		http://dwebgame.net	
 * @HotLine		0169 330 22 84	
 * @Version		v1.05.62		
*/	

include('checklic.php');
include('request_count.php');

include('func_item.php');

$itemdata_arr = ItemDataArr();

$action = $_POST['action'];

$info = 'OK';

switch ($action){ 
	case 'check_itemsocket':
    case 'punch_socketitem':
        $warehouse1 = $_POST['warehouse1'];
        
        $check_info_arr = array();
        
        // Define
		$line_socket[1] = $_POST['line_socket1'];
		$line_socket[2] = $_POST['line_socket2'];
		$line_socket[3] = $_POST['line_socket3'];
		$line_socket[4] = $_POST['line_socket4'];
		$line_socket[5] = $_POST['line_socket5'];
		$gcoinnew = $_POST['gcoinnew'];
		$gcoin_km = $_POST['gcoin_km'];
		$percent_success = $_POST['percent_success'];			$percent_success = abs(intval($percent_success));
    
		$item_arr[0]['slg'] = 0;
		$item_arr[0]['check'] = 0; 
       
        for($i=0; $i<32; ++$i) {
            $item = substr($warehouse1,$i*32,32);
            if($item != 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF') {
                $item_info = ItemInfo($itemdata_arr, $item);

                $item_info['vitri'] = $i;
                
                if((ceil(($item_info['vitri']+1)/8) + $item_info['y'] - 1) <= 4) {
                    $item_serial = $item_info['serial'];
                    if( hexdec($item_info['serial']) == 4294967280 ) {
                        $err .= $item_info['name'] . " đang được bảo vệ. Không thể sử dụng để đục lỗ Socket.<br />";
                    }
                            
                    if( $item_info['group'] < 12 && hexdec($item_info['serial']) != 4294967295) {		
                        ++$item_arr[0]['slg'];
						$socket_slot[1]	= hexdec(substr($item,22,2)); 	// Socket data
						$socket_slot[2]	= hexdec(substr($item,24,2)); 	// Socket data
						$socket_slot[3]	= hexdec(substr($item,26,2)); 	// Socket data
						$socket_slot[4]	= hexdec(substr($item,28,2)); 	// Socket data
						$socket_slot[5]	= hexdec(substr($item,30,2)); 	// Socket data
                        $item_arr[0]['check']   = 1;
                        $itemview_arr[] = array(
                            'info'  =>  $item_info['info'],
                            'img'   =>  $item_info['image']
                        );
                    }  else {
                        $err .= $item_info['name'] . " không thể sử dụng để đục lỗ Socket.<br />";
                    }
                }
            }
        }
		$percent = $percent_success;
		for ($slot=1;$slot<6;$slot++) {
			if (in_array($socket_slot[$slot], array(1,2,3,4,5,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,30,31,32,33,37,51,52,53,54,55,56,61,62,63,64,65,67,68,69,70,71,72,73,74,75,76,77,80,81,82,83,87,101,102,103,104,105,106,111,112,113,114,115,117,118,119,120,121,122,123,124,125,126,127,130,131,132,133,137,151,152,153,154,155,156,161,162,163,164,165,167,168,169,170,171,172,173,174,175,176,177,180,181,182,183,187,201,202,203,204,205,206,211,212,213,214,215,217,218,219,220,221,222,223,224,225,226,227,230,231,232,233,237,255))) $item_socket_check[$slot] = 0;
			else {
				$item_socket_check[$slot] = 1;
			}					 
        }
		$line = 0;
		if($item_socket_check[1]==1){
			$socket_add = substr_replace($item_info['item_code'], "FF", 22, 2);		
			$gcoin_chiphi = $line_socket[1];
			$line =1;
		}
		elseif($item_socket_check[2]==1){
			$socket_add = substr_replace($item_info['item_code'], "FF", 24, 2);		
			$gcoin_chiphi = $line_socket[2];
			$line =2;
		} 
		elseif($item_socket_check[3]==1){
			$socket_add = substr_replace($item_info['item_code'], "FF", 26, 2);		
			$gcoin_chiphi = $line_socket[3];
			 $line =3;
		}
		elseif($item_socket_check[4]==1){
			$socket_add = substr_replace($item_info['item_code'], "FF", 28, 2);		
			$gcoin_chiphi = $line_socket[4];
			$line =4;
		} 
		elseif($item_socket_check[5]==1){
			$socket_add = substr_replace($item_info['item_code'], "FF", 30, 2);		
			$gcoin_chiphi = $line_socket[5];
			$line =5;
		}
		else{
			$err .= "Item này đã được đục lỗ.<br />";
		}
		
		if($item_arr[0]['check'] == 0) {
				$err .= "Không tìm thấy Item Socket trên hòm đồ của bạn.<br />";
		}
		if($item_arr[0]['slg'] > 1) {
				$err .= "Số lượng Item đục lỗ vượt quá mức quy định.<br />";
		}		
        $check_info_arr['item'] = $item_arr;
        $check_info_arr['item_view'] = $itemview_arr;
        $check_info_arr['percent'] = $percent;
        $check_info_arr['err'] = $err;
        $check_info_arr['accept'] = 0;
	
        if($item_arr[0]['check'] == 1 && strlen($check_info_arr['err']) == 0) $check_info_arr['accept'] = 1;
        
        
        switch ($action) {
        	case 'punch_socketitem':
			    $serial = $_POST['serial'];
                $msg = '';
                $success = 0;
                $item_sksuccess = '';
                
                if($check_info_arr['accept'] == 1) {
                                 
					
					if($gcoin_km >= $gcoin_chiphi) $gcoin_km = $gcoin_km - $gcoin_chiphi;
					else {
						$gcoinnew = $gcoinnew - ($gcoin_chiphi - $gcoin_km);
						$gcoin_km = 0;
					}
					$empty_item = '';
                    for($i=0; $i<32*8*4; $i++) {
                        $empty_item .= 'F';
                    }
                    
                    $warehouse1_new = substr_replace($warehouse1, $empty_item, 0, 32*8*4);
		                                     
                    $rand = rand(1, 99);
                    if($rand <= $percent) {
                        $success = 1;   // Ep thanh cong
                        $item_sksuccess = substr_replace($socket_add, $serial, 6, 8);
                        $warehouse1_new = substr_replace($warehouse1_new, $item_sksuccess, 0, 32);
                    } else {
                        $success = 2;   // Ep xit
						$warehouse1_new = $warehouse1;
                    }
                 }
                
                echo "
                    <info>OK</info>
                    <reponse>$success</reponse>
					<line>$line</line>
                    <warehouse1>$warehouse1_new</warehouse1>
                    <item_sksuccess>$item_sksuccess</item_sksuccess>
					<gcoin_chiphi>$gcoin_chiphi</gcoin_chiphi>
					<gcoinnew>" . $gcoinnew ."</gcoinnew>
					<gcoin_km>" . $gcoin_km ."</gcoin_km>
                ";
        	break;
        
        	default :
                
                $check_info = json_encode($check_info_arr);
                echo "
                    <info>OK</info>
                    <check_info>" . $check_info ."</check_info>
                ";
        }
        
        
    break;
    
    
}

?>