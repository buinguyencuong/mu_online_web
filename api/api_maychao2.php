<?php

/**		
 * @author		Dwebgame	
 * @copyright	2014 - 2015	
 * @website		http://dwebgame.net	
 * @HotLine		0169 330 22 84	
 * @Version		v1.05.62		
*/	

include('checklic.php');
include('request_count.php');

include('func_item.php');

$itemdata_arr = ItemDataArr();

$action = $_POST['action'];

$info = 'OK';

$item_success = $_POST['item_success'];

switch ($action){ 
	case 'formula_check_item':
    case 'changeitem_change':
        $warehouse1 = $_POST['warehouse1'];
        
        $check_info_arr = array();
        $item_nguyenlieu = array();
		
        
        // Define
		$changeitem_name = $_POST['changeitem_name'];
		$percent_success = $_POST['percent_success'];
		$changeitem_slg = $_POST['changeitem_slg'];
		$changeitem_des = $_POST['changeitem_des'];

       	for($j=1;$j<=$changeitem_slg;$j++)
		{
			
			$item_name[$j] 	= $_POST['item_name'.$j.''];
			$item_code[$j] 	= $_POST['item_code'.$j.''];
			$item_slg[$j] 	= $_POST['item_slg'.$j.''];
			$item_getcode[$j]	= GetCode($item_code[$j]);
			$exc_option[$j]		= hexdec(substr($item_code[$j],14,2));
		}
        
        
        for($i=1; $i<=$changeitem_slg; $i++) {
            $item_arr[$i]['slg'] = 0;
            $item_arr[$i]['check'] = 0; 
        }
        
        
        for($i=0; $i<32; ++$i) {
            $item = substr($warehouse1,$i*32,32);
            if($item != 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF') {
                $item_info = ItemInfo($itemdata_arr, $item);

                $item_info['vitri'] = $i;
                
                if((ceil(($item_info['vitri']+1)/8) + $item_info['y'] - 1) <= 4) {
                    $item_serial = $item_info['serial'];
                    if( hexdec($item_info['serial']) == 4294967280 ) {
                        $err .= $item_info['name'] . " đang được bảo vệ. Không thể sử dụng để $changeitem_name.<br />";
                    }
						if( $item_info['group'] == $item_getcode[1]['group'] && $item_info['id'] == $item_getcode[1]['id'] && $item_info['level'] == $item_getcode[1]['level'] && $item_info['exc_opt'] == $exc_option[1]) { 
							++$item_arr[1]['slg'];
							$item_arr[1]['check']   = 1;
							$item_nguyenlieu[] = $item;
							$itemview_arr[] = array(
								'slg'	=> $item_arr[1]['slg'],
								'info'  =>  $item_info['info'],
								'img'   =>  $item_info['image']
							);	
						} elseif( $item_info['group'] == $item_getcode[2]['group'] && $item_info['id'] == $item_getcode[2]['id'] && $item_info['level'] == $item_getcode[2]['level'] && $item_info['exc_opt'] == $exc_option[2]) { 
                        ++$item_arr[2]['slg'];
                        $item_arr[2]['check']   = 1;
                        $item_nguyenlieu[] = $item;
                        $itemview_arr[] = array(
							'slg'	=> $item_arr[2]['slg'],
                            'info'  =>  $item_info['info'],
                            'img'   =>  $item_info['image']
                        );
						} elseif( $item_info['group'] == $item_getcode[3]['group'] && $item_info['id'] == $item_getcode[3]['id'] && $item_info['level'] == $item_getcode[3]['level'] && $item_info['exc_opt'] == $exc_option[3]) { 
                        ++$item_arr[3]['slg'];
                        $item_arr[3]['check']   = 1;
                        $item_nguyenlieu[] = $item;
                        $itemview_arr[] = array(
							'slg'	=> $item_arr[3]['slg'],
                            'info'  =>  $item_info['info'],
                            'img'   =>  $item_info['image']
                        );
						} elseif( $item_info['group'] == $item_getcode[4]['group'] && $item_info['id'] == $item_getcode[4]['id'] && $item_info['level'] == $item_getcode[4]['level'] && $item_info['exc_opt'] == $exc_option[4]) { 
                        ++$item_arr[4]['slg'];
                        $item_arr[4]['check']   = 1;
                        $item_nguyenlieu[] = $item;
                        $itemview_arr[] = array(
							'slg'	=> $item_arr[4]['slg'],
                            'info'  =>  $item_info['info'],
                            'img'   =>  $item_info['image']
                        );
						} elseif( $item_info['group'] == $item_getcode[5]['group'] && $item_info['id'] == $item_getcode[5]['id'] && $item_info['level'] == $item_getcode[5]['level'] && $item_info['exc_opt'] == $exc_option[5]) { 
                        ++$item_arr[5]['slg'];
                        $item_arr[5]['check']   = 1;
                        $item_nguyenlieu[] = $item;
                        $itemview_arr[] = array(
							'slg'	=> $item_arr[5]['slg'],
                            'info'  =>  $item_info['info'],
                            'img'   =>  $item_info['image']
                        );
						} else {
                        $err .= $item_info['name'] . " không thể sử dụng để $changeitem_name.<br />";
                    }					                  
                }
            }
        }
        
        $percent = $percent_success;
		$check_accept = 0;
        for($j=1; $j<=$changeitem_slg; $j++) {						
            if($item_arr[$j]['check'] == 0 || $item_arr[$j]['slg'] < $item_slg[$j]) {
				$check_accept=-5;
				$slg_check = $item_slg[$j] - $item_arr[$j]['slg'];
				$item_img = check_images($item_code[$j]);
				$itemviewerr_arr[] = array(
					'slg'	=>	$item_arr[$j]['slg'],
					'info'	=> 	$item_name[$j],
					'img'   =>  $item_img[ima]
				);
                $err .= "Thiếu <strong>$slg_check $item_name[$j]</strong>.<br />";
            }
            if($item_arr[$j]['slg'] > $item_slg[$j]) {
				$item_img = check_images($item_code[$j]);
				$itemviewerr_arr[] = array(
					'slg'	=>	$item_arr[$j]['slg'],
					'info'	=> 	$item_name[$j],
					'img'   =>  $item_img[ima]
				);
                $err .= "Số lượng <strong>$item_name[$j]</strong> vượt quá mức quy định.<br />";
            }
			if($item_arr[$j]['check'] == 1) ++$check_accept;				
        }
		$item_imgsuccess = check_images($item_success);
        
        $check_info_arr['item'] = $item_arr;
		$check_info_arr['item_success'] = $item_imgsuccess[ima];
		$check_info_arr['item_viewerr'] = $itemviewerr_arr;
        $check_info_arr['item_view'] = $itemview_arr;
        $check_info_arr['percent'] = $percent;
        $check_info_arr['err'] = $err;
        $check_info_arr['accept'] = 0;
		if($check_accept > 0 && strlen($check_info_arr['err']) == 0) $check_info_arr['accept'] = 1;
       
        
        
        switch ($action) {
        	case 'changeitem_change':
                $serial = $_POST['serial'];
                $msg = '';
                $success = 0;
                $item_nguyenlieu_data = '';
                $item_epsuccess = '';
                
                if($check_info_arr['accept'] == 1) {
                    
                    $empty_item = '';
                    for($i=0; $i<32*8*4; $i++) {
                        $empty_item .= 'F';
                    }
                    
                    $warehouse1_new = substr_replace($warehouse1, $empty_item, 0, 32*8*4);
                    
                    $rand = rand(1, 99);
                    if($rand <= $percent) {
                        $success = 1;   // Ep thanh cong
                        $item_epsuccess = substr_replace($item_success, $serial, 6, 8);
                        $warehouse1_new = substr_replace($warehouse1_new, $item_epsuccess, 0, 32);
                    } else {
                        $success = 2;   // Ep xit
                    }
                    
                    $item_nguyenlieu_data = json_encode($item_nguyenlieu);
                }
                
                echo "
                    <info>OK</info>
                    <reponse>$success</reponse>
					<changename>$changeitem_name</changename>
					<changeitem_des>$changeitem_des</changeitem_des>
                    <warehouse1>$warehouse1_new</warehouse1>
                    <item_nguyenlieu>$item_nguyenlieu_data</item_nguyenlieu>
                    <item_epsuccess>$item_epsuccess</item_epsuccess>
                ";
        	break;
        
        	default :
                
                $check_info = json_encode($check_info_arr);
                echo "
                    <info>OK</info>
                    <check_info>" . $check_info ."</check_info>
                ";
        }
        
        
    break;
    
    
}

?>