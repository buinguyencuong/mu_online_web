<?php

/**		
 * @author		Dwebgame	
 * @copyright	2014 - 2015	
 * @website		http://dwebgame.net	
 * @HotLine		0169 330 22 84	
 * @Version		v1.05.62		
*/	

include('checklic.php');
include('request_count.php');

include('func_item.php');

$itemdata_arr = ItemDataArr();

$action = $_POST['action'];

switch ($action){ 
	case 'item_list':
        $warehouse = $_POST['warehouse'];     
        
        $listitem_arr = array();
        
        $warehouse_itemtotal = floor(strlen($warehouse)/32);
        for($i=0; $i<$warehouse_itemtotal; ++$i) {
            $item = substr($warehouse,$i*32,32);
            if($item != 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF') {
                $item_info = ItemInfo($itemdata_arr, $item);
                $item_info['vitri'] = $i;
                    $item_serial = $item_info['serial'];
                    if( hexdec($item_info['serial']) == 0 ) {
                        $item_info['item_spec'] = 1;    // Seri 0
						$listitem_arr[] = array(
							'item_image' => $item_info['image'],
							'item_code' => $item_info['item_code'],
							'item_info' => $item_info['info']
						);
                    } else if (hexdec($item_info['serial']) < 4294967280) {
                        $item_info['item_spec'] = 0;
						$listitem_arr[] = array(
							'item_image' => $item_info['image'],
							'item_code' => $item_info['item_code'],
							'item_info' => $item_info['info']
						);
					} else if( hexdec($item_info['serial']) == 4294967280 ) {
                        $item_info['item_spec'] = 2;    // Item da bao ve
						$listitem_arr[] = array(
							'item_image' => $item_info['image'],
							'item_code' => $item_info['item_code'],
							'item_info' => $item_info['info']
						);
					} else {
                        $item_info['item_spec'] = 3;    // Item dac biet
						$listitem_arr[] = array(
							'item_image' => $item_info['image'],
							'item_code' => $item_info['item_code'],
							'item_info' => $item_info['info']
						);
					}           
            }
        }
        
        $warehouse_info = json_encode($listitem_arr);
        echo "
            <info>OK</info>
            <warehouse_info>" . $warehouse_info ."</warehouse_info>
        ";
    break;
    
    case 'item_info':
	    $item = $_POST['item'];
        
        $item_info = ItemInfo($itemdata_arr, $item);
		$item_info_arr = array();
		
        $item_info_arr['item_name'] = $item_info['name'];
		$item_info_arr['item_serial'] = $item_info['serial'];
        $item_info_arr['item_info'] = $item_info['info'];
        $item_info_arr['item_image'] = $item_info['image'];

            
        $iteminfo = json_encode($item_info_arr);
        echo "
            <info>OK</info>
            <iteminfo>". $iteminfo ."</iteminfo>
        ";
    break;
    
    case 'item_rut':
        $itemcode = $_POST['item'];
        $warehouse1 = $_POST['warehouse1'];
		
        $item_getcode = GetCode($itemcode);
        $item_info = ItemsData($itemdata_arr, $item_getcode['id'],$item_getcode['group'],$item_getcode['level']);
                
        $item_x = $item_info['x'];
        $item_y = $item_info['y'];
					
        $slot_accept = CheckSlot($itemdata_arr, $warehouse1,$item_x,$item_y);
		
		if(is_numeric($slot_accept)){
			$warehouse1_new = substr_replace($warehouse1, $itemcode, ($slot_accept-1)*32, 32);
			$info = "OK";
			$msg = "";
		}
		else{
			$warehouse1_new = $warehouse1;
			$info = "Error";
            $msg = "Hòm đồ không đủ chỗ chứa Item. Vui lòng dọn dẹp lại hòm đồ.";
		}       
		
        echo "
            <info>$info</info>
			<message>$msg</message>
            <warehouse1_new>". $warehouse1_new ."</warehouse1_new>
        ";
    break;
}

?>