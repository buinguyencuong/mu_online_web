<?php

/**		
 * @author		Dwebgame	
 * @copyright	2014 - 2015	
 * @website		http://dwebgame.net	
 * @HotLine		0169 330 22 84	
 * @Version		v1.05.62		
*/	

include('checklic.php');

include('func_item.php');

$itemdata_arr = ItemDataArr();

$item_read = $_POST['item'];
$item_itemtotal = floor(strlen($item_read)/32);
$item_info_arr = array();

	for($i=0; $i<$item_itemtotal; ++$i) {
		$item = substr($item_read,$i*32,32);
		if($item != 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF') {
			$item_info = ItemInfo($itemdata_arr, $item);
			$item_info_arr [] = array(
				'item_code'	=> $item,
				'item_name'   =>  $item_info['name'],
				'item_info' =>  $item_info['info'],
				'item_serial' =>  $item_info['serial'],
				'item_image'	=>	$item_info['image']
			);
		}
	}

$iteminfo = json_encode($item_info_arr);
echo "
    <info>OK</info>
    <iteminfo>" . $iteminfo ."</iteminfo>
";

?>