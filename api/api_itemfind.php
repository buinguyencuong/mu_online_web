<?php

/**		
 * @author		Dwebgame	
 * @copyright	2014 - 2015	
 * @website		http://dwebgame.net	
 * @HotLine		0169 330 22 84	
 * @Version		v1.05.62		
*/	

include('checklic.php');
include('request_count.php');

include('func_item.php');

$itemdata_arr = ItemDataArr();

$action = $_POST['action'];

$info = 'OK';

switch ($action){ 	
	case 'itemfind_getimg':
            $itemfind_item_source_slg = $_POST['itemfind_item_source_slg'];
            $itemfind_item_source = $_POST['itemfind_item_source'];
            
            $itemfind_item_source_arr = array();
			
			for($i=0; $i<$itemfind_item_source_slg; ++$i) {
				$item = $itemfind_item_source[$i]['code'];
                if($item != 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF') {
                    $item_info = ItemInfo($itemdata_arr, $item);
					$itemfind_item_source_arr [$i]['name']= $item_info['name'];
					$itemfind_item_source_arr [$i]['img']= $item_info['image'];
                }
            }
            $itemfind_item_source_data = json_encode($itemfind_item_source_arr);
            echo "
                <info>OK</info>
				<itemfind_item_source_data>" . $itemfind_item_source_data ."</itemfind_item_source_data>
            ";
       break;
        
	case 'itemfind_check_item':
    case 'itemfind_quay':
        $warehouse1 = $_POST['warehouse1'];
        
        $check_info_arr = array();
        $item_nguyenlieu = array();
		
        
        // Define
		$itemfind_item_source_arr = $_POST['itemfind_item_source_arr'];
		$find_item_count = count($itemfind_item_source_arr);
        
        
        for($x=0; $x<$find_item_count; ++$x) {
            $itemview_arr[$x]['slg_api'] = 0;
            $itemview_arr[$x]['check'] = 0; 
			$item_getinfo[$x]	= ItemInfo($itemdata_arr, $itemfind_item_source_arr[$x]['code']);
        }
        
        
        for($i=0; $i<32; ++$i) {
            $item = substr($warehouse1,$i*32,32);
            if($item != 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF') {
                $item_info = ItemInfo($itemdata_arr, $item);

                $item_info['vitri'] = $i;
                
                if((ceil(($item_info['vitri']+1)/8) + $item_info['y'] - 1) <= 4) {
                    $item_serial = $item_info['serial'];
                    if( hexdec($item_info['serial']) == 4294967280 ) {
                        $err .= $item_info['name'] . " đang được bảo vệ. Không thể sử dụng để đổi.<br />";
                    }
					    for($find_i=0; $find_i<$find_item_count; ++$find_i) {
							if(!isset($itemfind_item_source_arr[$find_i]['luck'])) $luck = 0; else $luck = 1;
							if(!isset($itemfind_item_source_arr[$find_i]['skill'])) $skill = 0; else $skill = 1;
							if( $item_info['group'] == $item_getinfo[$find_i]['group'] && $item_info['id'] == $item_getinfo[$find_i]['id'] && $item_info['level'] == $item_getinfo[$find_i]['level'] && $item_info['exc_total'] >= $find_item_source_arr[$find_i]['exl'] && $item_info['opt'] >= $find_item_source_arr[$find_i]['option'] && $item_info['luck'] == $luck && $item_info['skill'] == $skill) { 
								++$itemview_arr[$find_i]['slg_api'];
								$itemview_arr[$find_i]['check']  = 1;
								$item_nguyenlieu[] = $item;
							}else {
								$err .= $item_info['name'] . " không thể sử dụng để đổi. <br />";
							}
						}						                  
                }
            }
        }
        $check_accept = 0;
		$itemview_arr[0]['tyle'] = 1;
        for($j=0; $j<$find_item_count; $j++) {						
            if($itemview_arr[$j]['check'] == 0) {
                $err .= "Thiếu <strong>" . $itemfind_item_source_arr[$j]['name'] . "</strong>.<br />";
            }
            if($itemview_arr[$j]['slg_api'] > $itemfind_item_source_arr[$j]['slg']) {
                $itemview_arr[$j]['tyle'] = floor($itemview_arr[$j]['slg_api']/$itemfind_item_source_arr[$j]['slg']);
				if($itemview_arr[0]['tyle']>=$itemview_arr[$j]['tyle']) $itemview_arr[0]['tyle'] = $itemview_arr[$j]['tyle'];
            }
			$check_accept +=$itemview_arr[$j]['check'];
        }
        
        $check_info_arr['item'] = $itemview_arr;
        $check_info_arr['percent'] = $trader_percent;
        $check_info_arr['err'] = $err;
        $check_info_arr['accept'] = 0;
		
        if($check_accept > 0 && strlen($check_info_arr['err']) == 0) $check_info_arr['accept'] = 1;
       
        
        
        switch ($action) {
        	case 'itemfind_quay':
				$itemfind_item_dis_code = $_POST['itemfind_item_dis_code'];
                $serial = $_POST['serial'];
				$itemfind_percent = $_POST['itemfind_percent'];
                $msg = '';
                $success = 0;
                $item_nguyenlieu_data = '';
                $item_epsuccess = '';
                
                if($check_info_arr['accept'] == 1) {
                    
                    $empty_item = '';
                    for($i=0; $i<32*8*4; $i++) {
                        $empty_item .= 'F';
                    }
                    
                    $warehouse1_new = substr_replace($warehouse1, $empty_item, 0, 32*8*4);
                    
                    $rand = rand(1, 99);
                    if($rand <= $itemfind_percent) {
                        $success = 1;   // Ep thanh cong
						$item_dis_slg = floor(strlen($itemfind_item_dis_code)/32);
						for($i=0; $i<$item_dis_slg; $i++) {
							$item = substr($itemfind_item_dis_code,$i*32,32);
							for($j=0; $j<$itemview_arr[0]['tyle']; $j++) {
								$item_epsuccess = substr_replace($item, $serial[$j], 6, 8);
								$warehouse1_new = substr_replace($warehouse1_new, $item_epsuccess, $j*32, 32);
							}
						}
                    } else {
                        $success = 2;   // Ep xit
                    }
                    
                    $item_nguyenlieu_data = json_encode($item_nguyenlieu);
                }
                
                echo "
                    <info>OK</info>
                    <reponse>$success</reponse>
                    <warehouse1>$warehouse1_new</warehouse1>
                    <item_nguyenlieu>$item_nguyenlieu_data</item_nguyenlieu>
                    <item_epsuccess>$item_epsuccess</item_epsuccess>
                ";
        	break;
        
        	default :
                
                $check_info = json_encode($check_info_arr);
                echo "
                    <info>OK</info>
                    <check_info>" . $check_info ."</check_info>
                ";
        }
        
        
    break;
    
}

?>