<?php

/**		
 * @author		Dwebgame	
 * @copyright	2014 - 2015	
 * @website		http://dwebgame.net	
 * @HotLine		0169 330 22 84	
 * @Version		v1.05.62		
*/	

include('checklic.php');
include('func_item.php');

$itemdata_arr = ItemDataArr();

$type = $_POST['type'];

if($type=='gift_time'){
	// Thuc hien chuc nang
	$item_time_read = stripcslashes($_POST['item_time_read']);
	$item_time_read_arr = json_decode($item_time_read, true);
	$item_time_count = count($item_time_read_arr);
	$item_time_gift_arr = array();
	$info = "OK";
	for($x=0; $x<$item_time_count; ++$x) {
	$item_choise = $item_time_read_arr[$x]['itemcode'];
	$day = $item_time_read_arr[$x]['day'];
	$item_time_choise_itemtotal = floor(strlen($item_choise)/32);
		for($i=0; $i<$item_time_choise_itemtotal; ++$i) {
			$item = substr($item_choise,$i*32,32);
			if($item != 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF') {
				$item_getcode = GetCode($item);
				$Num = $item_getcode['Num'];
				$Lvl = $item_getcode['Lvl'];
				$Opt = $item_getcode['Opt'];
				$Luck = $item_getcode['Luck'];
				$Skill = $item_getcode['Skill'];
				$Dur = $item_getcode['Dur']; 
				$Excellent = $item_getcode['Excellent'];
				$Ancient = $item_getcode['Ancient'];
				$JOH = $item_getcode['JOH'];
				$Sock1 = $item_getcode['Sock1'];
				$Sock2 = $item_getcode['Sock2'];
				$Sock3 = $item_getcode['Sock3'];
				$Sock4 = $item_getcode['Sock4'];
				$Sock5 = $item_getcode['Sock5'];
		
				$item_time_gift_arr[] = array(
					'Num'	=> $Num,
					'Lvl' => $Lvl,
					'Opt' => $Opt,
					'Luck' => $Luck,
					'Skill' => $Skill,
					'Dur' => $Dur,
					'Excellent' => $Excellent,
					'Ancient' => $Ancient,
					'JOH' => $JOH,
					'Sock1' => $Sock1,
					'Sock2' => $Sock2,
					'Sock3' => $Sock3,
					'Sock4' => $Sock4,
					'Sock5' => $Sock5,
					'Days'	=> $day,
					'SerialFFFFFFFE'	=> 0
				);
			}
		}
	}

		$item_time_gift = json_encode($item_time_gift_arr);
		echo "
			<info>$info</info>
			<item_time_gift>" . $item_time_gift ."</item_time_gift>
		";
} else {
	// Thuc hien chuc nang
	$item_read = stripcslashes($_POST['item_read']);
	$item_read_arr = json_decode($item_read, true);
	$warehouse1 = $_POST['warehouse1'];

	$total_rate = 0;
	$item_read_count = count($item_read_arr);
	for($j=0; $j<$item_read_count; $j++) {
		$total_rate += $item_read_arr[$j]['rate'];
	}

	$item_choise_rate = rand(1, $total_rate);
	for($i=0; $i<$item_read_count; $i++) {
		if($item_choise_rate >= $item_read_arr[$i]['rate_from'] && $item_choise_rate <= $item_read_arr[$i]['rate_to']) {
			$item_choise_i = $i;
			$item_choise = $item_read_arr[$i]['itemcode'];
			$gcoin_km = $item_read_arr[$i]['gcoin_km'];
			$vpoint_km = $item_read_arr[$i]['vpoint_km'];
			$bank_zen = $item_read_arr[$i]['bank_zen'];
			$bank_chao = $item_read_arr[$i]['bank_chao'];
			$bank_cre = $item_read_arr[$i]['bank_cre'];
			$bank_blue = $item_read_arr[$i]['bank_blue'];
			$phucloi_point = $item_read_arr[$i]['phucloi_point'];
			break;
		}
	}

	$info = "OK";
	$msg = "";
	$warehouse1_check = $warehouse1;
	$item_choise_itemtotal = floor(strlen($item_choise)/32);
	$item_gift_arr = array();

	if(strlen($warehouse1) < 8*4*32) {
		$info = "Error";
		$msg = "Rương đồ chung chưa mở. Vui lòng vào nhân vật, mở Rương đồ chung rồi nhận lại.";
	} else {
		for($i=0; $i<$item_choise_itemtotal; ++$i) {
			$item = substr($item_choise,$i*32,32);
			if($item != 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF') {
				$item_getcode = GetCode($item);
				$item_info = ItemsData($itemdata_arr, $item_getcode['id'],$item_getcode['group'],$item_getcode['level']);
						
				$item_x = $item_info['x'];
				$item_y = $item_info['y'];
				
				$slot_accept = CheckSlot($itemdata_arr, $warehouse1_check,$item_x,$item_y);
				
				if($slot_accept == 0) {
					$info = "Error";
					$msg = "Rương đồ chung không đủ chỗ chứa Item. Vui lòng dọn dẹp lại Rương đồ chung.";
					break;
				} else {
					$warehouse1_check = substr_replace($warehouse1_check, $item, ($slot_accept-1)*32, 32);
					$vitri = $slot_accept-1;
					$item_gift_arr[] = array(
						'code'  =>  $item,
						'vitri' =>  $vitri
					);
				}
			}
		}
	}

		

		$item_gift = json_encode($item_gift_arr);
		echo "
			<info>$info</info>
			<msg>$msg</msg>
			<item_choise>$item_choise_i</item_choise>
			<gcoin_km>$gcoin_km</gcoin_km>
			<vpoint_km>$vpoint_km</vpoint_km>
			<bank_zen>$bank_zen</bank_zen>
			<bank_chao>$bank_chao</bank_chao>
			<bank_cre>$bank_cre</bank_cre>
			<bank_blue>$bank_blue</bank_blue>
			<phucloi_point>$phucloi_point</phucloi_point>
			<item_gift>" . $item_gift ."</item_gift>
		";
}
?>