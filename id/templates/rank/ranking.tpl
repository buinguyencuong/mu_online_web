<div class="primary-block">
    <div class="main-content">
        <h1 class="title-content">Xếp hạng Reset nhân vật</h1>
        <div class="intro-content">
            <div class="in-20">
                <span class="color-violet">Xếp hạng Theo Số lần Reset và Relife hiện có của nhân vật</span><br />
                    <span class="tag tag-red">Reset</span>
                    <span class="tag tag-red">Relife</span>
              
            </div>
        </div>
    </div>
</div>

<div class="primary-block out-top-20">
    <?php include('templates/rank/rankrs_top.tpl'); ?>
    
    <div class="in-20 font-14 text-semibold">
        <p class="update-time"><span class="color-red">Cập nhập lúc:</span> <span class="color-brown"><?php echo $time_top; ?><span class="color-red"> <span class="color-red">( 5 phút cập nhập 1 lần )</span></p>

        <div class="out-top-20">
            <table class="table table-blue-white table-border-2 table-text-center table-sort">
                <thead>
                <tr>
            <th align="center" scope="col">#</th>
		    <th align="center" scope="col">Nhân vật</th>
		    <th align="center" scope="col"><font color="red">RL</font>/<font color="blue">RS</font>/LV</th>
		    <th align="center" scope="col">Lớp nhân vật</th>
            <th align="center" scope="col">RS Cuối</th>
                </tr>
                </thead>
                <tbody>
            <?php for($i=0;$i<count($char);$i++) { ?>
		  <tr bgcolor="#FFFFFF">
		    <td align="center"><?php $j=$i+1; echo $j; ?></td>
		    <td align="center"><?php echo $char[$i]['name']; ?></td>
		    <td align="center"><font color="red"><?php echo $char[$i]['relife']; ?></font> / <font color="blue"><?php echo $char[$i]['reset']; ?></font>/<?php echo $char[$i]['level']; ?></td>
		    <td align="center"><?php echo $char[$i]['nvclass']; ?></td>
            <td align="center"><?php echo $char[$i]['Resets_Time']; ?></td>
		  </tr>
<?php } ?>
                
                </tbody>
            </table>
        </div>
    </div>
</div>