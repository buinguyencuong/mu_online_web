﻿$(function(){
	
	$('#home-tab-controls .item').hover(function(){
		$('#home-tab-controls .item, #home-tabs .tab').removeClass('active');
		$('#home-tab-controls .item:eq('+$(this).index()+'), #home-tabs .tab:eq('+$(this).index()+')').addClass('active');
		$('.news .more').attr('href', $(this).attr('href'));
	});
	$('.characters .char-icon li').hover(function(){
		$('.characters .chars li, .characters .char-icon li').removeClass('active');
		$('.characters .chars li:eq('+$(this).index()+'), .characters .char-icon li:eq('+$(this).index()+')').addClass('active');
	});
	$('body').click(function(){
		$('.download .part').removeClass('active');
	});
	$('.download .part').click(function(e){
		e.stopPropagation();
		$('.download .part').removeClass('active');
		$(this).addClass('active');
	});

	$.ajaxSetup({
	    headers: {
	        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
	    }
	});

});
