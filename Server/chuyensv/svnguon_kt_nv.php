<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
include('config.php');
include('function.php');

$nv_nguon = $_REQUEST['nv_nguon'];
$acc_nguon = $_REQUEST['acc_nguon'];

    if(check_nv($acc_nguon, $nv_nguon) == 0) {
        echo "Nhân vật <b>{$nv_nguon}</b> không nằm trong tài khoản <b>{$acc_nguon}</b>. Vui lòng kiểm tra lại."; exit();
    }

    $nv_check_query = "SELECT Class, CAST(Inventory AS image), Money, CAST(Quest AS image), Resets, Relifes, NoResetInDay, SCFPCPoints, point_event, PointUyThac, SCFMasterLevel, SCFSealItem, SCFSealTime, SCFScrollItem, SCFScrollTime FROM Character WHERE Name='$nv_nguon' AND AccountID='$acc_nguon'";
	$nv_check_result = $db->Execute($nv_check_query);
        check_queryerror($nv_check_query, $nv_check_result);
        
		$nv_check = $nv_check_result->fetchrow();
	
		$inventory = bin2hex($nv_check[1]);
		$inventory = strtoupper($inventory);
		$inventory = substr($inventory,0,76*32);

		$quest = bin2hex($nv_check[3]);
		$quest = strtoupper($quest);
        
        $class = $nv_check[0];
        $Money = $nv_check[2];
        $Resets = $nv_check[4];
        $NoResetInDay = $nv_check[6];
        $Relifes = $nv_check[5];
        $SCFPCPoints = $nv_check[7];
        $point_event = $nv_check[8];
        $PointUyThac = $nv_check[9];
        $SCFMasterLevel = $nv_check[10];
        $SCFSealItem = $nv_check[11];
        $SCFSealTime = $nv_check[12];
        $SCFScrollItem = $nv_check[13];
        $SCFScrollTime = $nv_check[14];
        
        $output = "
            <info>OK</info>
            <class>$class</class>
            <inventory>$inventory</inventory>
            <money>$Money</money>
            <quest>$quest</quest>
            <resets>$Resets</resets>
            <resetday>$NoResetInDay</resetday>
            <relife>$Relifes</relife>
            <scfpoint>$SCFPCPoints</scfpoint>
            <pointevent>$point_event</pointevent>
            <pointuythac>$PointUyThac</pointuythac>
            <scfmasterlv>$SCFMasterLevel</scfmasterlv>
            <SCFSealItem>$SCFSealItem</SCFSealItem>
            <SCFSealTime>$SCFSealTime</SCFSealTime>
            <SCFScrollItem>$SCFScrollItem</SCFScrollItem>
            <SCFScrollTime>$SCFScrollTime</SCFScrollTime>
        ";
        
		echo $output;
?>