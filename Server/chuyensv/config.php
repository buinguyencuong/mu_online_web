<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
//Info Data
$type_connect		=		'odbc';			//Dạng kết nối Database: 'odbc' hoac 'mssql'
$localhost			=		'localhost';
$databaseuser		=		'sa';			//User quản lý SQL MuOnline (Thường là 'sa')
$databsepassword	=		'123456';		//Mật khẩu quản lý SQL MuOnline
$database			=		'MuOnline';		//Database MuOnline ('MuOnline' hoặc 'MeMuOnline')

include("../adodb/adodb.inc.php");

if($type_connect=='odbc'){
	$db = &ADONewConnection('odbc');
	$connect_mssql = $db->Connect($database,$databaseuser,$databsepassword);
	if (!$connect_mssql) die("Ket noi voi SQL Server loi! Hay kiem tra lai ODBC ton tai hoac User & Pass SQL dung.");
}

elseif($type_connect=='mssql'){
	if (extension_loaded('mssql'))
	{echo("");}
	else Die("Loi! Khong the load thu vien php_mssql.dll. Hay cho phep su dung php_mssql.dll trong php.ini");

	$db = &ADONewConnection('mssql');
	$connect_mssql = $db->Connect($localhost,$databaseuser,$databsepassword,$database);
	if (!$connect_mssql) die("Loi! Khong the ket noi SQL Server");

}

//Danh sách IP của Hosting cho phép truy cập vào Web trên Server
$list_ip = array(
	"127.0.0.1",		// Local
	"123.30.168.73"		// HOST
	);

if ( !in_array($_SERVER['REMOTE_ADDR'], $list_ip, true) ){ 
    echo "Khong co quyen truy cap";
    exit();
}

?>