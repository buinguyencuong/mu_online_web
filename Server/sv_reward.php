<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
	include_once("security.php");
include_once("config.php");
include('config/config_rewarditem.php');
$file_thueitem = 'config/rewarditem.txt';


$login = $_POST['login'];
$name = $_POST['name'];
$action = $_POST['action'];
$passtransfer = $_POST["passtransfer"];

if ($passtransfer == $transfercode) {
    
    switch ($action) { 
    
    	case 'reward_item':
            $reward_type = abs(intval($_POST['reward_type']));   if($reward_type < 1 || $reward_type > 13) $reward_type = 99;
            $code = $_POST['code'];
            $luck = abs(intval($_POST['luck']));         if($luck != 1) $luck = 0;
            $exl[1] = abs(intval($_POST['exl1']));         if($exl[1] != 1) $exl[1] = 0;
            $exl[2] = abs(intval($_POST['exl2']));         if($exl[2] != 1) $exl[2] = 0;
            $exl[3] = abs(intval($_POST['exl3']));         if($exl[3] != 1) $exl[3] = 0;
            $exl[4] = abs(intval($_POST['exl4']));         if($exl[4] != 1) $exl[4] = 0;
            $exl[5] = abs(intval($_POST['exl5']));         if($exl[5] != 1) $exl[5] = 0;
            $exl[6] = abs(intval($_POST['exl6']));         if($exl[6] != 1) $exl[6] = 0;
            $lvl = abs(intval($_POST['lvl']));           if($lvl < 0 || $lvl > 15) $lvl = 0;
            $opt = abs(intval($_POST['opt']));           if($opt < 0 || $opt > 7) $opt = 0;
            $rewardday = abs(intval($_POST['rewardday']));           if(!in_array($rewardday, array(1,3,7,15,30))) $rewardday = 0;
            
            $error = "";
            if($rewardday == 0) {
                $error .= "Chưa chọn ngày Thuê.<br />";
            }
            if($use_day[$rewardday] != 1) {
                $error .= "Thời gian Thuê không cho phép. Vui lòng chọn lại thời gian thuê.<br />";
            }
            if (!preg_match("/^[A-F0-9]*$/i", $code))
        	{
                $error .= "Dữ liệu lỗi <strong>Mã Item</strong> : $code . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
        	}
            if( strlen($code) <> 32 ) {
                $error .= "Dữ liệu lỗi <strong>Mã Item</strong> : $code phải có 32 ký tự.<br />";
            }
            if($lvl > $item_lv_max) {
                $error .= "Dữ liệu lỗi <strong>Cấp độ Item</strong> : <strong>$lvl</strong> không được lớn hơn <strong>$item_lv_max</strong>.<br />";
            }
        
            if(strlen($error) == 0) {
                //Đọc File Reward
            	$fopen_host = fopen($file_thueitem, "r");
                $item_get = fgets($fopen_host);
            	fclose($fopen_host);
                
                $item_listall_arr = json_decode($item_get, true);
                
                $item = array();
                if( isset($item_listall_arr[$reward_type][$code]) && $item_listall_arr[$reward_type][$code]['stat'] == 1 ) {
                    $item = $item_listall_arr[$reward_type][$code];
                    $item_name = $item['item_name'];
                    $price_reward = $item['price'];
                    
                    // Luck
                    if($luck == 1) {
                        $price_reward += $price_luck;
                    }
                    
                    
                    switch ($item['exl_type']) {
                    	case 0 :
                            $price_exl[1]	= $vukhi_exl[1];
                    		$price_exl[2]	= $vukhi_exl[2];
                    		$price_exl[3]	= $vukhi_exl[3];
                    		$price_exl[4]	= $vukhi_exl[4];
                    		$price_exl[5]	= $vukhi_exl[5];
                    		$price_exl[6]	= $vukhi_exl[6];
                            
                            $exl_info[1]	= 'Tăng lượng MANA khi giết quái (MANA/8)';
                    		$exl_info[2]	= 'Tăng lượng LIFE khi giết quái (LIFE/8)';
                    		$exl_info[3]	= 'Tốc độ tấn công +7';
                    		$exl_info[4]	= 'Tăng lực tấn công 2%';
                    		$exl_info[5]	= 'Tăng lực tấn công (Cấp độ/20)';
                    		$exl_info[6]	= 'Khả năng xuất hiện lực tấn công hoàn hảo +10%';
                            $opt_info['option_type'] = 'Tăng thêm sát thương'; 
                            $opt_info['option_mul'] = 4;
                            $opt_info['option_bonus'] = '';
                            
                            $exl_use = $vukhi_exl_use;
                    		break;
                    	case 1:
                            $price_exl[1]	= $giap_exl[1];
                    		$price_exl[2]	= $giap_exl[2];
                    		$price_exl[3]	= $giap_exl[3];
                    		$price_exl[4]	= $giap_exl[4];
                    		$price_exl[5]	= $giap_exl[5];
                    		$price_exl[6]	= $giap_exl[6];
                            
                            $exl_info[1]	= 'Lượng ZEN rơi ra khi giết quái +40%';
                    		$exl_info[2]	= 'Khả năng xuất hiện phòng thủ hoàn hảo +10%';
                    		$exl_info[3]	= 'Phản hồi sát thương +5%';
                    		$exl_info[4]	= 'Giảm sát thương +4%';
                    		$exl_info[5]	= 'Lượng MANA tối đa +4%';
                    		$exl_info[6]	= 'Lượng HP tối đa +4%';
                            
                            $price_lv_plus[$lvl] = $price_shield_lv_plus[$lvl];
                            
                            $opt_info['option_type'] = 'Tăng thêm phòng thủ'; 
                            $opt_info['option_mul'] = 5;
                            $opt_info['option_bonus'] = '';
                            
                            $exl_use = $giap_exl_use;
                    		break;
                    	case 2:
                            $price_exl[1]	= $giap_exl[1];
                    		$price_exl[2]	= $giap_exl[2];
                    		$price_exl[3]	= $giap_exl[3];
                    		$price_exl[4]	= $giap_exl[4];
                    		$price_exl[5]	= $giap_exl[5];
                    		$price_exl[6]	= $giap_exl[6];
                            
                            $exl_info[1]	= 'Lượng ZEN rơi ra khi giết quái +40%';
                    		$exl_info[2]	= 'Khả năng xuất hiện phòng thủ hoàn hảo +10%';
                    		$exl_info[3]	= 'Phản hồi sát thương +5%';
                    		$exl_info[4]	= 'Giảm sát thương +4%';
                    		$exl_info[5]	= 'Lượng MANA tối đa +4%';
                    		$exl_info[6]	= 'Lượng HP tối đa +4%';
                            
                            $price_lv_plus[$lvl] = $price_armor_lv_plus[$lvl];
                            
                            $opt_info['option_type'] = 'Tăng thêm phòng thủ'; 
                            $opt_info['option_mul'] = 4;
                            $opt_info['option_bonus'] = '';
                            
                            $exl_use = $giap_exl_use;
                    		break;
                    	case 3: 
                            $price_exl[1]	= $wing2_exl[1];
                    		$price_exl[2]	= $wing2_exl[2];
                    		$price_exl[3]	= $wing2_exl[3];
                    		$price_exl[4]	= $wing2_exl[4];
                    		$price_exl[5]	= $wing2_exl[5];
                            
                            $exl_info[1]	= '+ 115 Lượng HP tối đa';
                    		$exl_info[2]	= '+ 115 Lượng MP tối đa';
                    		$exl_info[3]	= 'Khả năng loại bỏ phòng thủ đối phương +3%';
                    		$exl_info[4]	= '+ 50 Lực hành động tối đa';
                    		$exl_info[5]	= 'Tốc độ tấn công +7';
                            
                            $price_lv_plus[$lvl] = $wing2_price_lv_plus[$lvl];
                            
                            $opt_info['option_type'] = 'Tự động hồi phục HP'; 
                            $opt_info['option_mul'] = 1;
                            $opt_info['option_bonus'] = '%';
                            
                            $exl_use = $wing2_exl_use;
                    		break;
                    	case 4:
                            $price_exl[1]	= $giap_exl[1];
                    		$price_exl[2]	= $giap_exl[2];
                    		$price_exl[3]	= $giap_exl[3];
                    		$price_exl[4]	= $giap_exl[4];
                    		$price_exl[5]	= $giap_exl[5];
                    		$price_exl[6]	= $giap_exl[6];
                            
                            $exl_info[1]	= 'Lượng ZEN rơi ra khi giết quái +40%';
                    		$exl_info[2]	= 'Khả năng xuất hiện phòng thủ hoàn hảo +10%';
                    		$exl_info[3]	= 'Phản hồi sát thương +5%';
                    		$exl_info[4]	= 'Giảm sát thương +4%';
                    		$exl_info[5]	= 'Lượng MANA tối đa 4%';
                    		$exl_info[6]	= 'Lượng HP tối đa 4%';
                            
                            $price_option[$opt] = $pent_ring_price_option[$opt];
                            $price_lv_plus[$lvl] = $price_ring_lv_plus[$lvl];
                            
                            $opt_info['option_type'] = 'Tự động hồi phục HP'; 
                            $opt_info['option_mul'] = 1;
                            $opt_info['option_bonus'] = '%';
                            
                            $exl_use = $giap_exl_use;
                    		break;
                    	case 5:
                            $price_exl[1]	= $vukhi_exl[1];
                    		$price_exl[2]	= $vukhi_exl[2];
                    		$price_exl[3]	= $vukhi_exl[3];
                    		$price_exl[4]	= $vukhi_exl[4];
                    		$price_exl[5]	= $vukhi_exl[5];
                    		$price_exl[6]	= $vukhi_exl[6];
                            
                            $exl_info[1]	= 'Tăng lượng MANA khi giết quái (MANA/8)';
                    		$exl_info[2]	= 'Tăng lượng LIFE khi giết quái (LIFE/8)';
                    		$exl_info[3]	= 'Tốc độ tấn công +7';
                    		$exl_info[4]	= 'Tăng lực tấn công 2%';
                    		$exl_info[5]	= 'Tăng lực tấn công (Cấp độ/20)';
                    		$exl_info[6]	= 'Khả năng xuất hiện lực tấn công hoàn hảo +10%';
                            
                            $price_option[$opt] = $pent_ring_price_option[$opt];
                            $price_lv_plus[$lvl] = $price_pendant_lv_plus[$lvl];
                            
                            $opt_info['option_type'] = 'Tự động hồi phục HP'; 
                            $opt_info['option_mul'] = 1;
                            $opt_info['option_bonus'] = '%';
                            
                            $exl_use = $vukhi_exl_use;
                    		break;
                    	case 6:
                            $price_exl[1]	= 'Sói Tấn Công';
                    		$price_exl[2]	= 'Sói Phòng Thủ';
                    		$price_exl[3]	= 'Sói Hoàng Kim';
                            
                            $exl_info[1]	= 'Sói Tấn Công';
                    		$exl_info[2]	= 'Sói Phòng Thủ';
                    		$exl_info[3]	= 'Sói Hoàng Kim';
                            $opt_info['option_type'] = ''; 
                            $opt_info['option_mul'] = 1;
                            $opt_info['option_bonus'] = '';
                    		break;
                        case 7:
                            $price_exl[1]	= $wing3_exl[1];
                    		$price_exl[2]	= $wing3_exl[2];
                    		$price_exl[3]	= $wing3_exl[3];
                    		$price_exl[4]	= $wing3_exl[4];
                            
                            $exl_info[1]	= '5% cơ hội loại bỏ sức phòng thủ';
                    		$exl_info[2]	= '5 % phản đòn khi cận chiến';
                    		$exl_info[3]	= '5% khả năng hồi phục hoàn toàn HP';
                    		$exl_info[4]	= '5% khả năng hồi phục hoàn toàn nội lực';
                            
                            $price_option[$opt] = $w3_price_option[$opt];
                            $price_lv_plus[$lvl] = $w3_price_lv_plus[$lvl];
                            
                            $opt_info['option_type'] = 'Tự động hồi phục HP'; 
                            $opt_info['option_mul'] = 1;
                            $opt_info['option_bonus'] = '%';
                            
                            $exl_use = $wing3_exl_use;
                    		break;
                        case 8:
                            $price_exl[1]	= $wing25_exl[1];
                    		$price_exl[2]	= $wing25_exl[2];
                    		$price_exl[3]	= $wing25_exl[3];
                    		$price_exl[4]	= $wing25_exl[4];
                            
                            $exl_info[1]	= '3% cơ hội loại bỏ sức phòng thủ';
                    		$exl_info[2]	= '3 % phản đòn khi cận chiến';
                    		$exl_info[3]	= '3% khả năng hồi phục hoàn toàn HP';
                    		$exl_info[4]	= '3% khả năng hồi phục hoàn toàn nội lực';
                            
                            $price_option[$opt] = $wing25_price_option[$opt];
                            $price_lv_plus[$lvl] = $wing25_price_lv_plus[$lvl];
                            
                            $opt_info['option_type'] = 'Tự động hồi phục HP';
                            $opt_info['option_mul'] = 1; 
                            $opt_info['option_bonus'] = '%';
                            
                            $exl_use = $wing25_exl_use;
                    		break;
                        case 9:
                            $price_exl[1]	= $wing4_exl[1];
                    		$price_exl[2]	= $wing4_exl[2];
                    		$price_exl[3]	= $wing4_exl[3];
                    		$price_exl[4]	= $wing4_exl[4];
                            
                            $exl_info[1]	= '7% cơ hội loại bỏ sức phòng thủ';
                    		$exl_info[2]	= '7 % phản đòn khi cận chiến';
                    		$exl_info[3]	= '7% khả năng hồi phục hoàn toàn HP';
                    		$exl_info[4]	= '7% khả năng hồi phục hoàn toàn nội lực';
                            
                            $price_option[$opt] = $w4_price_option[$opt];
                            $price_lv_plus[$lvl] = $w4_price_lv_plus[$lvl];
                            
                            $opt_info['option_type'] = 'Tự động hồi phục HP'; 
                            $opt_info['option_mul'] = 1;
                            $opt_info['option_bonus'] = '%';
                            
                            $exl_use = $wing4_exl_use;
                    		break;
                    	default:
                   	}
                    
                    // Lvl
                    if($lvl >= 1 && $lvl <= 15) {
                        $price_reward += $price_lv_plus[$lvl];
                    }
                    
                    // Option
                    if($opt >= 1 && $opt <= 7) {
                        $price_reward += $price_option[$opt];
                    }
                    
                    // Excellent
                    $exl_total = 0;
                    foreach($exl as $k => $v) {
                        if($v == 1) {
                            if($exl_use[$k] == 1) {
                                ++$exl_total;
                                $price_reward += $price_exl[$k];
                            } else {
                                echo "Dòng hoàn hảo : <strong>". $exl_info[$k] ."</strong> không được thuê.<br />Vui lòng tải lại trang để lấy dữ liệu mới nhất.";
                                exit();
                            }
                        }
                    }
                    
                    if($exl_total > 0 && $price_exl_plus[$exl_total] > 0) {
                        $price_reward += $price_exl_plus[$exl_total];
                    }
                    
                    if($rewardday > 1) {
                        $price_reward = $price_reward * $rewardday;
                        
                        $price_redure = floor($price_reward * $price_day_redure[$rewardday] / 100);
                        
                        $price_reward -= $price_redure;
                    }
                    
                    $money_q = "SELECT gcoin,gcoin_km FROM MEMB_INFO WHERE memb___id='$login'";
                    $money_r = $db->Execute($money_q);
                        check_queryerror($money_q, $money_r);
                    $money_f = $money_r->FetchRow();
                    $gcoin_before = $money_f[0];
                    $gcoin_km_before = $money_f[1];
                    $gcoin_total = $gcoin_before + $gcoin_km_before;
                    
                    if($gcoin_total < $price_reward) {
                        $gcoin_less = $price_reward - $gcoin_total;
                        $error = "Hiện tại đang có : $gcoin_before Gcoin + $gcoin_km_before Gcon khuyến mãi.<br /> Chi phí cần để thuê Item : $price_reward Gcoin.<br /> <strong>Thiếu : $gcoin_less Gcoin</strong>";
                    } else {
                        if($gcoin_km_before >= $price_reward) {
                            $gcoin_after = $gcoin_before;
                            $gcoin_km_after = $gcoin_km_before - $price_reward;
                        }
                        else {
                            $gcoin_after = $gcoin_before - ($price_reward - $gcoin_km_before);
                            $gcoin_km_after = 0;
                        }
                        
                        
                        include_once('config_license.php');
                        include_once('func_getContent.php');
                        $getcontent_url = $url_license . "/api_reward.php";
                        $getcontent_data = array(
                            'acclic'    =>  $acclic,
                            'key'    =>  $key,
                            'action'    =>  'reward_item_info',
                            
                            'code'    =>  $code,
                            'reward_type'    =>  $reward_type,
                            'luck'    =>  $luck,
                            'exl1'    =>  $exl[1],
                            'exl2'    =>  $exl[2],
                            'exl3'    =>  $exl[3],
                            'exl4'    =>  $exl[4],
                            'exl5'    =>  $exl[5],
                            'exl6'    =>  $exl[6],
                            'lvl'    =>  $lvl,
                            'opt'    =>  $opt,
                            'rewardday'    =>  $rewardday,
                        ); 
                        
                        $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
                    
                    	if ( empty($reponse) ) {
                            $error = "Server bảo trì vui lòng liên hệ Admin để FIX";
                        }
                        else {
                            $info = read_TagName($reponse, 'info');
                            if($info == "Error") {
                                $error = read_TagName($reponse, 'message');
                            } elseif ($info == "OK") {
                                $item_reward_info = read_TagName($reponse, 'item_reward_info');
                                if(strlen($item_reward_info) == 0) {
                                    echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                                    
                                    $arr_view = "\nDataSend:\n";
                                    foreach($getcontent_data as $k => $v) {
                                        $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                                    }
                                    writelog("log_api.txt", $arr_view . $reponse);
                                    exit();
                                }
                            } else {
                                echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
                                writelog("log_api.txt", $reponse);
                                exit();
                            }
                        }
    
                        if(strlen($error) == 0) {
                            
                            $item_reward = json_decode($item_reward_info, true);
                            
                            $reward_check_q = "SELECT count(*) FROM NBB_ThueItem WHERE AccountID='$login' AND Name='$name' AND Num=". $item_reward['Num'] ." AND Lvl=". $item_reward['Lvl'] ." AND Opt=". $item_reward['Opt'] ." AND Luck=". $item_reward['Luck'] ." AND Skill=". $item_reward['Skill'] ." AND Dur=". $item_reward['Dur'] ." AND Excellent=". $item_reward['Excellent'] ." AND Ancient=". $item_reward['Ancient'] ." AND JOH=". $item_reward['JOH'] ." AND Sock1=". $item_reward['Sock1'] ." AND Sock2=". $item_reward['Sock2'] ." AND Sock3=". $item_reward['Sock3'] ." AND Sock4=". $item_reward['Sock4'] ." AND Sock5=". $item_reward['Sock5'] ." AND Days=". $item_reward['Days'] ." AND SerialFFFFFFFE=". $item_reward['SerialFFFFFFFE'] ." AND Status=0";
                            $reward_check_r = $db->Execute($reward_check_q);
                                check_queryerror($reward_check_q, $reward_check_r);
                            $reward_check_f = $reward_check_r->FetchRow();
                            if($reward_check_f[0] > 0 ) {
                                $error = "Item muốn Thuê trùng với Item đã thuê trước đó chưa lấy ra.<br /> Vui lòng vào Game lấy Item đã thuê mới được tiếp tục thuê lại.";
                            } else {
                                $time_create = date('Y/m/d H:i', $timestamp);
                                
                                $item_level = '';
                                if ($lvl == 0 || $item_reward['Dur'] == 0) $item_level = '';
                                else $item_level = ' +'.$lvl;
                                
                                $luck_info = '';
                                if($luck == 1) {
                                    $luck_info	= '<font color=#8CB0EA>May Mắn (Tỉ lệ ép Ngọc Tâm Linh + 25%)<br>May Mắn (Sát thương tối đa + 5%)</font><br>';
                                }
                                $skill_info = '';
                                if($item_reward['Skill'] == 1) {
                                    $skill_info	= '<font color=#8CB0EA>Vũ khí có tuyệt chiêu</font><br>';
                                }
                                $option_info = '';
                                if ($item_reward['Opt'] > 0 && strlen($opt_info['option_type']) > 0) {
                                    $option_info = "<font color=#9AADD5>".$opt_info['option_type']." +". $opt_info['option_mul']*$item_reward['Opt'] ." ". $opt_info['option_bonus'] ."</font><br>"; 
                                }
                                $item_exc = '';
                                if ($exl[1] == 1) $item_exc .= '<br>'.$exl_info[1];
                            	if ($exl[2] == 1) $item_exc .= '<br>'.$exl_info[2];
                            	if ($exl[3] == 1) $item_exc .= '<br>'.$exl_info[3];
                            	if ($exl[4] == 1) $item_exc .= '<br>'.$exl_info[4];
                            	if ($exl[5] == 1) $item_exc .= '<br>'.$exl_info[5];
                            	if ($exl[6] == 1) $item_exc .= '<br>'.$exl_info[6];
                                $item_exc = '<font color=#2FF387>'.$item_exc.'</font><br>';
                                $item_info = '<center><strong><span style=color:#FFFFFF><font color=#2FF387>'.$item_name.$item_level.'</font><br>'
                    				.$luck_info
                    				.$skill_info
                    				.$option_info
                    				.$item_exc
                                    .'</span></strong></center>';
                                
                                $reward_q = "INSERT INTO NBB_ThueItem (AccountID, Name, Zen, VIPMoney, Num, Lvl, Opt, Luck, Skill, Dur, Excellent, Ancient, JOH, Sock1, Sock2, Sock3, Sock4, Sock5, Days, SerialFFFFFFFE, Time_Created, item_info, item_img) VALUES ('$login', '$name', 0, 0, ". $item_reward['Num'] .", ". $item_reward['Lvl'] .", ". $item_reward['Opt'] .", ". $item_reward['Luck'] .", ". $item_reward['Skill'] .", ". $item_reward['Dur'] .", ". $item_reward['Excellent'] .", ". $item_reward['Ancient'] .", ". $item_reward['JOH'] .", ". $item_reward['Sock1'] .", ". $item_reward['Sock2'] .", ". $item_reward['Sock3'] .", ". $item_reward['Sock4'] .", ". $item_reward['Sock5'] .", ". $item_reward['Days'] .", ". $item_reward['SerialFFFFFFFE'] .", '$time_create', N'$item_info', '". $item['img'] ."')";
                                $reward_r = $db->Execute($reward_q);
                                    check_queryerror($reward_q, $reward_r);
                                
                                $money_update_q = "UPDATE MEMB_INFO SET gcoin = $gcoin_after, gcoin_km = $gcoin_km_after WHERE memb___id='$login'";
                                $money_update_r = $db->Execute($money_update_q);
                                    check_queryerror($money_update_q, $money_update_r);
                                
                                _use_money($login, $gcoin_before-$gcoin_after, $gcoin_km_before - $gcoin_km_after, 0);
                                
                                echo "<info>OK</info><gcoin>$gcoin_after</gcoin><gcoinkm>$gcoin_km_after</gcoinkm>";
                            } 
                        }
                    }
                } else {
                    $error = "Item muốn Thuê không có.";
                }
            }
            
            if(strlen($error) > 0) {
                echo $error;
            }
               
        	break;
            
    }
}
$db->Close();
?>