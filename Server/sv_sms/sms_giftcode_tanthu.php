<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
include_once('config/config_giftcode_tanthu.php');

if($giftcode_tanthu_use != 1) {
    $content = "Cu phap tin nhan sai, vui long kiem tra lai.";
} else {
        //Kiểm tra tài khoản đã nhận giftcode chưa
    $check_use_giftcode_query = "SELECT status, gift_code FROM GiftCode WHERE acc='$taikhoan' AND type=9";
    $check_use_giftcode_result = $db->Execute($check_use_giftcode_query);
    $check_use_giftcode = $check_use_giftcode_result->numrows();
    if($check_use_giftcode > 0)
    {
        $gift_status = $check_use_giftcode_result->FetchRow();
        if($gift_status[0] == 1) {
            $content = "Tai khoan $taikhoan da nhan GiftCode voi ma : " . $gift_status[1];
        } else {
            $content = "Tai khoan $taikhoan da su dung GiftCode, khong the nhan them.";
        }
    }
    else {
    	//Config
    	$characters = 'abcdefghijklmnpqrstuvwxyz123456789';
    	$random_string_length = 6;
        $gifttime = substr(time(), -4);
    	
        // Create GiftCode
        $gift_created = 0;
        while($gift_created == 0) {
            $giftcode = ''; 
         	for ($i = 0; $i < $random_string_length; $i++) { 
        		$giftcode .= $characters[rand(0, strlen($characters) - 1)]; 
         	}
            $giftcode .= $gifttime;
            $giftcode = strtoupper($giftcode);
            
            $giftcode_exits_query = "SELECT * FROM GiftCode WHERE gift_code='$giftcode' AND (status=0 OR status=1)";
            $giftcode_exits_result = $db->Execute($giftcode_exits_query);
            $giftcode_exits = $giftcode_exits_result->NumRows();
            if($giftcode_exits == 0) $gift_created = 1;
        }
    
        $giftcode_insert_query = "INSERT INTO GiftCode (gift_code, acc, type, gift_time, ngay, status) VALUES ('$giftcode', '$taikhoan', 9, $timestamp, '".date("Y-m-d",$timestamp)."', 1)";
        $giftcode_insert_result = $db->Execute($giftcode_insert_query);
        $content = "Ma GiftCode Tan Thu cua tai khoan $taikhoan : $giftcode";
    }
}
 

?>