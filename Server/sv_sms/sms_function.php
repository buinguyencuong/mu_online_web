<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
function check_queryerror($query,$result) {
    if ($result === false) die("Query Error : $query");
}

function check_phone($phone,$taikhoan)
{
	include('config.php');
    if(substr($phone, 0, 2) == '84') {
        $phone = substr($phone, 2);
    	$phone = '0'.$phone;
    }
	$sql_phone_check = $db->Execute("SELECT * FROM MEMB_INFO WHERE tel__numb='$phone' AND memb___id='$taikhoan'"); 
	$phone_check = $sql_phone_check->numrows();
    
    // verify SMS
    if($phone_check >0) {
        $checksms_status_update_q = "UPDATE MEMB_INFO SET checksms_status=1 WHERE checksms_status=0 AND memb___id='$taikhoan'";
        $checksms_status_update_r = $db->Execute($checksms_status_update_q);
            check_queryerror($checksms_status_update_q, $checksms_status_update_r);
    }
    
	return $phone_check;
}

function check_taikhoan($taikhoan) {
	include('config.php');
	$sql_acc_check_query = "SELECT * FROM MEMB_INFO WHERE memb___id='$taikhoan'";
	$sql_acc_check = $db->Execute($sql_acc_check_query);
	$acc_check = $sql_acc_check->numrows();
	return $acc_check;
}

function check_nv($name) {
	include('config.php');
	$sql_nv_check_query = "SELECT * FROM Character WHERE Name='$name'";
	$sql_nv_check = $db->Execute($sql_nv_check_query);
	$nv_check = $sql_nv_check->numrows();
	return $nv_check;
}

function check_online($taikhoan) {
	include('config.php');
	$sql_online_check = $db->Execute("SELECT * FROM MEMB_STAT WHERE memb___id='$taikhoan' AND ConnectStat='1'");
	$online_check = $sql_online_check->numrows();
	return $online_check;
}

function check_doinv($name) {
	include('config.php');
    
	$sql_doinv_check = $db->Execute("SELECT * FROM AccountCharacter WHERE GameIDC='$name'");
	$doinv_check = $sql_doinv_check->numrows();
	return $doinv_check;
}

if(!function_exists(_sno_numb)) {
    function _sno_numb($sno_numb) {
        $sno_year = rand(0, 95);
        if(strlen($sno_year) == 1) $sno_year = '0' . $sno_year;
        
        $sno_month = rand(1, 12);
        if(strlen($sno_month) == 1) $sno_month = '0'. $sno_month;
        
        $sno_day = rand(1, 31);
        if(strlen($sno_day) == 1) $sno_day = '0'. $sno_day;
        
        $sno_numb = abs(intval($sno_numb));
        $sno_numb_len = strlen($sno_numb);
        if($sno_numb_len < 7) {
            for($i=0; $i<(7-$sno_numb_len); ++$i) {
                $sno_numb = '0'. $sno_numb;
            }
        }
        
        $sno = $sno_year . $sno_month . $sno_day . $sno_numb;
        
        return $sno;
    }
}


function _writelog_tiente($login, $log_price, $log_Des)
{
    global $db, $type_connect, $localhost, $databaseuser, $databsepassword, $database, $database_log, $timestamp;
    
    $info_log_query = "SELECT gcoin, gcoin_km, vpoint, vpoint_km FROM MEMB_INFO WHERE memb___id='$login'";
    $info_log_result = $db->Execute($info_log_query);
        check_queryerror($info_log_query, $info_log_result);
    $info_log = $info_log_result->FetchRow();
    
    $log_gcoin = $info_log[0];
    $log_gcoin_km = $info_log[1];
    $log_vpoint = $info_log[2];
    $log_vpoint_km = $info_log[3];
    
    include_once("adodb/adodb.inc.php");
    
    // Connect DB Log
    if($type_connect == 'odbc') {
		$db = &ADONewConnection('odbc');
		$connect_mssql = $db->Connect($database_log,$databaseuser,$databsepassword);
		if (!$connect_mssql) die("Ket noi voi SQL Server loi! Hay kiem tra lai ODBC ton tai hoac User & Pass SQL dung.");
	} elseif ($type_connect == 'mssql'){
		$db = &ADONewConnection('mssql');
		$connect_mssql = $db->Connect($localhost,$databaseuser,$databsepassword,$database_log);
		if (!$connect_mssql) die("Loi! Khong the ket noi SQL Server : $database_log");
	}
    
    $insert_log_query = "INSERT INTO Log_TienTe (acc, gcoin, gcoin_km, vpoint, vpoint_km, price, Des, time) VALUES ('$login', $log_gcoin, $log_gcoin_km, $log_vpoint, $log_vpoint_km, '$log_price', N'$log_Des', $timestamp)";
    $insert_log_result = $db->execute($insert_log_query);
        check_queryerror($insert_log_query, $insert_log_result);
    
    // ReConnect DB MU
    if($type_connect == 'odbc') {
    	$db = &ADONewConnection('odbc');
    	$connect_mssql = $db->Connect($database,$databaseuser,$databsepassword);
    	if (!$connect_mssql) die("Ket noi voi SQL Server loi! Hay kiem tra lai ODBC ton tai hoac User & Pass SQL dung.");
    } elseif ($type_connect == 'mssql'){
    	$db = &ADONewConnection('mssql');
    	$connect_mssql = $db->Connect($localhost,$databaseuser,$databsepassword,$database);
    	if (!$connect_mssql) die("Loi! Khong the ket noi SQL Server : $database");
    }
}

function get_ip()
{
	# Enable X_FORWARDED_FOR IP matching?
	$do_check = 1;
	$addrs = array();

	if( $do_check )
	{
		foreach( array_reverse(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR'])) as $x_f )
		{
			$x_f = trim($x_f);
			if( preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/', $x_f) )
			{
				$addrs[] = $x_f;
			}
		}

		$addrs[] = $_SERVER['HTTP_CLIENT_IP'];
		$addrs[] = $_SERVER['HTTP_PROXY_USER'];
	}

	$addrs[] = $_SERVER['REMOTE_ADDR'];

	foreach( $addrs as $v )
	{
		if( $v )
		{
			preg_match("/^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$/", $v, $match);
			$ip = $match[1].'.'.$match[2].'.'.$match[3].'.'.$match[4];

			if( $ip && $ip != '...' )
			{
				break;
			}
		}
	}

	if( ! $ip || $ip == '...' )
	{
		print_error("Không thể xác định địa chỉ IP của bạn.");
	}

	return $ip;
}

if(!function_exists(read_TagName)) {
        /**
     * read_TagName()
     * 
     * @param mixed $content
     * @param mixed $tagname
     * @param integer $vitri
     * $vitri = 0 : output All
     * $vitri = x : output Element x, Element 0 : Count Total Element
     * @return
     */
    function read_TagName($content, $tagname, $vitri = 1)
    {
        $tag_begin = '<'. $tagname . '>';
        $tag_end = '</'. $tagname . '>';
        $content1 = explode($tag_begin, $content);
        $slg_string = count($content1)-1;
        $output[] = $slg_string;    // Vị trí đầu tiên xuất ra số lượng phần tử
        for($i=1; $i<count($content1); $i++)    // Duyệt từ phần tử thứ 1 đến hết
        {
            $content2 = explode($tag_end, $content1[$i]);
            $output[] = $content2[0];
        }
        
        if($vitri == 0) return $output;
        else return $output[$vitri];
    }
}
?>