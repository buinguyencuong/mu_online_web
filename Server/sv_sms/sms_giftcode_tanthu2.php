<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
include_once("config/config_giftcode_tanthu2.php");

$day_reg_q = "SELECT appl_days FROM MEMB_INFO WHERE memb___id='$taikhoan'";
$day_reg_r = $db->Execute($day_reg_q);
    check_queryerror($day_reg_q, $day_reg_r);
$day_reg_f = $day_reg_r->FetchRow();

if( strtotime($day_reg_f[0]) < strtotime($giftcode_tanthu2_acc_reg)) {
    $content = "Tai khoan '$taikhoan' dang ky truoc ngay ". date('d/m/Y', strtotime($giftcode_tanthu2_acc_reg)) ." . Khong the nhan Giftcode Tan Thu 2";
} elseif(strtotime($giftcode_tanthu2_begin) > $timestamp) {
    $content = "Chua den thoi gian nhan thuong. Khong the lay Giftcode.";
} elseif(strtotime($giftcode_tanthu2_end) + 24*60*60 < $timestamp) {
    $content = "Da het thoi gian nhan thuong. Khong the lay Giftcode.";
} else {
        //Kiểm tra tài khoản đã nhận giftcode chưa
    $check_use_giftcode_query = "SELECT status, gift_code FROM GiftCode WHERE acc='$taikhoan' AND type=8";
    $check_use_giftcode_result = $db->Execute($check_use_giftcode_query);
    $check_use_giftcode = $check_use_giftcode_result->numrows();
    if($check_use_giftcode > 0)
    {
        $gift_status = $check_use_giftcode_result->FetchRow();
        if($gift_status[0] == 1) {
            $content = "Tai khoan $taikhoan da nhan GiftCode voi ma : " . $gift_status[1];
        } else {
            $content = "Tai khoan $taikhoan da su dung GiftCode, khong the nhan them.";
        }
    }
    else {
    	//Config
    	$characters = 'abcdefghijklmnpqrstuvwxyz123456789';
    	$random_string_length = 6;
        $gifttime = substr(time(), -4);
    	
        // Create GiftCode
        $gift_created = 0;
        while($gift_created == 0) {
            $giftcode = ''; 
         	for ($i = 0; $i < $random_string_length; $i++) { 
        		$giftcode .= $characters[rand(0, strlen($characters) - 1)]; 
         	}
            $giftcode .= $gifttime;
            $giftcode = strtoupper($giftcode);
            
            $giftcode_exits_query = "SELECT * FROM GiftCode WHERE gift_code='$giftcode' AND (status=0 OR status=1)";
            $giftcode_exits_result = $db->Execute($giftcode_exits_query);
            $giftcode_exits = $giftcode_exits_result->NumRows();
            if($giftcode_exits == 0) $gift_created = 1;
        }
    
        $giftcode_insert_query = "INSERT INTO GiftCode (gift_code, acc, type, gift_time, ngay, status) VALUES ('$giftcode', '$taikhoan', 8, $timestamp, '".date("Y-m-d",$timestamp)."', 1)";
        $giftcode_insert_result = $db->Execute($giftcode_insert_query);
        $content = "Ma GiftCode Tan Thu 2 cua tai khoan $taikhoan : $giftcode";
    }
}
?>