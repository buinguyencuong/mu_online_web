<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
//Kiểm tra xem có phải tài khoản mới đăng kí
$acc_new_query = "SELECT * FROM MEMB_INFO WHERE checksms_status='0' AND memb___id='$taikhoan'";
$acc_new_result = $db->Execute($acc_new_query);
$acc_new = $acc_new_result->numrows();
//Là acc mới
if($acc_new > 0)
{
	$time_checksms_add = 25*24*60*60;
	$update_checksms_query = "UPDATE MEMB_INFO SET checksms_status='1', time_checksms=time_checksms+$time_checksms_add WHERE memb___id='$taikhoan'";
	$update_checksms_result = $db->Execute($update_checksms_query);
}
//Không phải là acc mới
else {
	//Kiểm tra xem có đang bị Block
	$check_block_query = "SELECT * FROM MEMB_INFO WHERE bloc_code='1' AND memb___id='$taikhoan' AND admin_block='0'";
	$check_block_result = $db->Execute($check_block_query);
	$check_block = $check_block_result->numrows();
	//Nếu đang bị block
	if($check_block > 0)
	{
		$update_checksms_query = "UPDATE MEMB_INFO SET time_checksms='$timestamp',bloc_code='0' WHERE memb___id='$taikhoan'";
		$update_checksms_result = $db->Execute($update_checksms_query);
	}
	//Nếu không bị Block
	else {
		$time_checksms_add = 30*24*60*60;
		$update_checksms_query = "UPDATE MEMB_INFO SET checksms_status='1', time_checksms=time_checksms+$time_checksms_add WHERE memb___id='$taikhoan'";
		$update_checksms_result = $db->Execute($update_checksms_query);
	}
}
$content = "Xac minh tai khoan $taikhoan thanh cong. Vui long thoat tai khoan Web va dang nhap lai de cap nhap thong tin";
?>