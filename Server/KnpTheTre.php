<?php

/**
 * @author NetBanBe
 * @copyright 2014
 */

	include_once("security.php");
include_once("config.php");
include_once("config/config_napthe.php");
include_once("function.php");
include_once('autonap_func.php');

$TxtTransId = $_REQUEST['TxtTransId'];
$TxtMenhGia = $_REQUEST['TxtMenhGia'];

if(strlen($TxtTransId) > 0 && strlen($TxtMenhGia) > 0) {
    $stt = substr($TxtTransId, 2);
    
    $card_info_q = "SELECT acc, card_serial, status, card_type, menhgia FROM CardPhone WHERE stt='$stt'";
    $card_info_r = $db->Execute($card_info_q);
        check_queryerror($card_info_q, $card_info_r);
    $card_info_c = $card_info_r->NumRows();
    if($card_info_c == 0) {
        echo "Thẻ số $stt không tồn tại.";
    } else {
        $card_info_f = $card_info_r->FetchRow();
        $login = $card_info_f[0];
        $card_serial = $card_info_f[1];
        $card_status = $card_info_f[2];
        $cardtype = $card_info_f[3];
        $menhgia = $card_info_f[4];
        
        if($card_status == 2) {
            echo "Thẻ nạp cho tài khoản $login với Serial $card_serial đã có người duyệt thẻ đúng từ trước. Hệ thống trả thẻ trễ không duyệt lại.";
        } elseif($card_status == 3) {
            echo "Thẻ nạp cho tài khoản $login với Serial $card_serial đã có người duyệt thẻ sai từ trước. Hệ thống trả thẻ trễ không duyệt lại.";
        } else {
            if($TxtMenhGia == 0) {
                $up_stat = 3;
                include('autonap_duyet.php');
                echo "Thẻ thứ: $stt. Tài khoản: $login nạp thẻ sai với Serial $card_serial.";
            } else {
                $up_stat = 2;
                $edit_menhgia = $TxtMenhGia;
                include('autonap_duyet.php');
                echo "Thẻ thứ: $stt. Tài khoản: $login nạp thẻ đúng với Serial $card_serial. Mệnh giá thẻ : $TxtMenhGia";
            }
        }
    }
} else {
    echo "Tham số truyền vào không hợp lệ - TxtTransId : $TxtTransId - TxtMenhGia : $TxtMenhGia";
}

?>