<?php

/**
 * @author NetBanBe
 * @copyright 2014
 */

    include_once("security.php");
include_once("config.php");
include_once("function.php");

$play_daily = 3;            // So luong lan choi trong ngay

$gift1_score = 10;          // Dat diem tren moc se nhan duoc giftcode 1
    $gift1_type = 0;        // Ma loai Giftcode 1
$gift2_score = 30;          // Dat diem tren moc se nhan duoc giftcode 2
    $gift2_type = 1;        // Ma loai Giftcode 2
$gift3_score = 50;          // Dat diem tren moc se nhan duoc giftcode 3
    $gift3_type = 3;        // Ma loai Giftcode 3

$time_begin_to_cot1 = 3.4;  // Thoi gian bat dau den cot 1
$time_2cot = 1.7;           // Thoi gian giua 2 cot

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

$action = $_REQUEST['action'];

switch ($action) { 
	case 'login':
        $username = $_REQUEST['username'];
        $passweb = $_REQUEST['passweb'];
        $passweb_md5 = md5($passweb);
        $acc_check_q = "SELECT TOP 1 memb__pwdmd5, bloc_code FROM MEMB_INFO WHERE memb___id='$username'";
        $acc_check_r = $db->Execute($acc_check_q);
            check_queryerror($acc_check_q, $acc_check_r);
        $acc_check_c = $acc_check_r->NumRows();
        
        
        $error = false;
        $message = "OK";
        if($acc_check_c == 0) {
            $error = true;
            $message = "Tài khoản không tồn tại.";
        } else {
            $acc_check_f = $acc_check_r->FetchRow();
            if($acc_check_f[0] != $passweb_md5) {
                $error = true;
                $message = "Sai mật khẩu Web 1.";
            } else if($acc_check_f[1] != 0) {
                $error = true;
                $message = "Tài khoản đang bị khóa.";
            }
        }
        
        if($error === false) {
            $top_score_q = "SELECT TOP 1 Score FROM NBB_FlappyBird WHERE AccountID='$username' ORDER BY Score DESC";
            $top_score_r = $db->Execute($top_score_q);
                check_queryerror($top_score_q, $top_score_r);
            $top_score_f = $top_score_r->FetchRow();
            
            $output_arr['diemmax'] = $top_score_f[0];
        }
        
        $output_arr = array(
            'error' =>  $error,
            'message'   =>  $message
        );
        
        echo json_encode($output_arr);
	break;

	case 'getinfo':
        $output_arr['error'] = false;
    
        $username = $_REQUEST['username'];
        $day_begin = date('Y-m-d', $timestamp) ." 0:0:0";
        $day_end = date('Y-m-d', $timestamp) ." 23:59:59";
        $play_slg_q = "SELECT count(AccountID) FROM NBB_FlappyBird WHERE AccountID='$username' AND ngay >= '$day_begin' AND ngay <= '$day_end'";
        $play_slg_r = $db->Execute($play_slg_q);
            check_queryerror($play_slg_q, $play_slg_r);
        $play_slg_f = $play_slg_r->FetchRow();
        
        $top_score_q = "SELECT TOP 1 Score FROM NBB_FlappyBird WHERE AccountID='$username' ORDER BY Score DESC";
        $top_score_r = $db->Execute($top_score_q);
            check_queryerror($top_score_q, $top_score_r);
        $top_score_f = $top_score_r->FetchRow();
        
        $gift_exists_q = "SELECT gift_code, giftcode_type FROM GiftCode WHERE acc='$username' AND type=4 AND (giftcode_type=$gift1_type OR giftcode_type=$gift2_type OR giftcode_type=$gift3_type) ORDER BY gift_time";
        $gift_exists_r = $db->Execute($gift_exists_q);
            check_queryerror($gift_exists_q, $gift_exists_r);
        
        $output_arr['message'][0] = "";
        $output_arr['message'][1] = "";
        $output_arr['message'][2] = "";
        while($gift_exists_f = $gift_exists_r->FetchRow()) {
            if($gift_exists_f[1] == $gift1_type) {
                $output_arr['message'][0] = $gift_exists_f[0];
            } else if($gift_exists_f[1] == $gift2_type) {
                $output_arr['message'][1] = $gift_exists_f[0];
            } else {
                $output_arr['message'][2] = $gift_exists_f[0];
            }
        }
        
        foreach($output_arr['message'] as $key => $val) {
            if(strlen($val) == 0) {
                unset($output_arr['message'][$key]);
            }
        }
            
        $output_arr['dachoi'] = $play_slg_f[0];
        $output_arr['maxlan'] = $play_daily;
        $output_arr['diemmax'] = $top_score_f[0];
        
        echo json_encode($output_arr);
	break;
    
    case 'start':
        $time_start = microtime_float();
        
        $output_arr['error'] = false;
        $username = $_REQUEST['username'];
        
        $day_begin = date('Y-m-d', $timestamp);
        $day_end = date('Y-m-d H:i', $timestamp);
        $play_slg_q = "SELECT count(AccountID) FROM NBB_FlappyBird WHERE AccountID='$username' AND ngay >= '$day_begin' AND ngay <= '$day_end'";
        $play_slg_r = $db->Execute($play_slg_q);
            check_queryerror($play_slg_q, $play_slg_r);
        $play_slg_f = $play_slg_r->FetchRow();
        if($play_slg_f[0] >= $play_daily) {
            $output_arr['error'] = true;
            $output_arr['message'] = "Hôm nay đã chơi đến giới hạn : $play_daily lần. Hẹn gặp bạn vào ngày mai.";
        } else {
            $start_del_q = "DELETE NBB_FlappyBird WHERE AccountID='$username' AND Score=0 AND time_end=0";
            $start_del_r = $db->Execute($start_del_q);
                check_queryerror($start_del_q, $start_del_r);
            
            $score_insert_q = "INSERT INTO NBB_FlappyBird (AccountID, time_start) VALUES ('$username', $time_start)";
            $score_insert_r = $db->Execute($score_insert_q);
                check_queryerror($score_insert_q, $score_insert_r);
        }
        
        echo json_encode($output_arr);
    break;
    
	case 'sendscore':
        $time_end = microtime_float();
        
        $output_arr['error'] = false;
        
        $username = $_REQUEST['username'];
        $score = $_REQUEST['score'];        $score = abs(intval($score));
        
        $day_begin = date('Y-m-d', $timestamp);
        $day_end = date('Y-m-d H:i', $timestamp);
        $play_slg_q = "SELECT count(AccountID) FROM NBB_FlappyBird WHERE AccountID='$username' AND ngay >= '$day_begin' AND ngay <= '$day_end'";
        $play_slg_r = $db->Execute($play_slg_q);
            check_queryerror($play_slg_q, $play_slg_r);
        $play_slg_f = $play_slg_r->FetchRow();
        if($play_slg_f[0] >= $play_daily) {
            $output_arr['error'] = true;
            $output_arr['message'] = "Hôm nay đã chơi đến giới hạn : $play_daily lần. Hẹn gặp bạn vào ngày mai.";
        } else {
            if($score == 0) {
                $start_del_q = "DELETE NBB_FlappyBird WHERE AccountID='$username' AND Score=0 AND time_end=0";
                $start_del_r = $db->Execute($start_del_q);
                    check_queryerror($start_del_q, $start_del_r);
            } else if($score > 0) {
                $time_start_q = "SELECT time_start FROM NBB_FlappyBird WHERE AccountID='$username' AND Score=0 AND time_end=0";
                $time_start_r = $db->Execute($time_start_q);
                    check_queryerror($time_start_q, $time_start_r);
                $time_start_c = $time_start_r->NumRows();
                if($time_start_c > 1) {
                    $output_arr['error'] = true;
                    $output_arr['message'] = "Bạn vui lòng không chơi FlappyBird ở 2 trình duyệt hoặc 2 máy tính trong cùng thời gian.";
                    
                    $start_del_q = "DELETE NBB_FlappyBird WHERE AccountID='$username' AND Score=0 AND time_end=0";
                    $start_del_r = $db->Execute($start_del_q);
                        check_queryerror($start_del_q, $start_del_r);
                } else if($time_start_c == 1) {
                    $time_start_f = $time_start_r->FetchRow();
                    $time_start = $time_start_f[0];
                    
                    $time_play = $time_end - $time_start;
                    $time_check = $time_begin_to_cot1 + $score*$time_2cot;
                    
                    if($time_play >= ($time_check - 5*$time_2cot) && $time_play <= ($time_check + 5*$time_2cot)) {
                        $score_update_q = "UPDATE NBB_FlappyBird SET Score=$score, time_end=$time_end WHERE AccountID='$username' AND Score=0 AND time_end=0";
                        $score_update_r = $db->Execute($score_update_q);
                            check_queryerror($score_update_q, $score_update_r);
                    } else {
                        $output_arr['error'] = true;
                        $output_arr['message'] = "Có sự can thiệp của phần mềm thứ 3. Dữ liệu không được tính.";
                        
                        $start_del_q = "DELETE NBB_FlappyBird WHERE AccountID='$username' AND Score=0 AND time_end=0";
                        $start_del_r = $db->Execute($start_del_q);
                            check_queryerror($start_del_q, $start_del_r);
                    }
                } else {
                    $output_arr['error'] = true;
                    $output_arr['message'] = "Dữ liệu bắt đầu chơi chưa được khởi tạo.";
                }
            }
            
            if($output_arr['error'] === false) {
                $output_arr['message'][0] = '';
                $output_arr['message'][1] = '';
                $output_arr['message'][2] = '';
                
                $top_score_q = "SELECT TOP 1 Score FROM NBB_FlappyBird WHERE AccountID='$username' ORDER BY Score DESC";
                $top_score_r = $db->Execute($top_score_q);
                    check_queryerror($top_score_q, $top_score_r);
                $top_score_f = $top_score_r->FetchRow();
                
                $gift_slg = 0;
                $gift1_exists = false;
                if($top_score_f[0] >= $gift1_score) {
                    $gift1_exists_q = "SELECT TOP 1 gift_code FROM GiftCode WHERE acc='$username' AND type=4 AND giftcode_type=$gift1_type";
                    $gift1_exists_r = $db->Execute($gift1_exists_q);
                        check_queryerror($gift1_exists_q, $gift1_exists_r);
                    $gift1_exists_c = $gift1_exists_r->NumRows();
                    if($gift1_exists_c > 0) {
                        $gift1_exists_f = $gift1_exists_r->FetchRow();
                        $output_arr['message'][0] = $gift1_exists_f[0];
                        $gift1_exists = true;
                    } else {
                        $gift_slg++;
                    }
                }
                
                $gift2_exists = false;
                if($top_score_f[0] >= $gift2_score) {
                    $gift2_exists_q = "SELECT TOP 1 gift_code FROM GiftCode WHERE acc='$username' AND type=4 AND giftcode_type=$gift2_type";
                    $gift2_exists_r = $db->Execute($gift2_exists_q);
                        check_queryerror($gift2_exists_q, $gift2_exists_r);
                    $gift2_exists_c = $gift2_exists_r->NumRows();
                    if($gift2_exists_c > 0) {
                        $gift2_exists_f = $gift2_exists_r->FetchRow();
                        $output_arr['message'][1] = $gift2_exists_f[0];
                        $gift2_exists = true;
                    } else {
                        $gift_slg++;
                    }
                }
                
                $gift3_exists = false;
                if($top_score_f[0] >= $gift3_score) {
                    $gift3_exists_q = "SELECT TOP 1 gift_code FROM GiftCode WHERE acc='$username' AND type=4 AND giftcode_type=$gift3_type";
                    $gift3_exists_r = $db->Execute($gift3_exists_q);
                        check_queryerror($gift3_exists_q, $gift3_exists_r);
                    $gift3_exists_c = $gift3_exists_r->NumRows();
                    if($gift3_exists_c > 0) {
                        $gift3_exists_f = $gift3_exists_r->FetchRow();
                        $output_arr['message'][2] = $gift3_exists_f[0];
                        $gift3_exists = true;
                    } else {
                        $gift_slg++;
                    }
                }
                
                if($gift_slg > 0) {
                    //writelog('log_flappybird.txt', "So luong Giftcode can tao : ". $gift_slg);
                    include_once('config_license.php');
                    include_once('func_getContent.php');
                    $getcontent_url = $url_license . "/api_giftcode_create.php";
                    $getcontent_data = array(
                        'acclic'    =>  $acclic,
                        'key'    =>  $key,
                        
                        'gift_slg'  =>  $gift_slg
                    ); 
                    
                    $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
                
                	if ( empty($reponse) ) {
                        $err .= "Server bao tri vui long lien he Admin de FIX";
                        $err_flag = true;
                    }
                    else {
                        $info = read_TagName($reponse, 'info');
                        if($info == "Error") {
                            $message = read_TagName($reponse, 'message');
                            $err .= $message;
                            $err_flag = true;
                        } elseif($info == "OK") {
                            $giftcode = read_TagName($reponse, 'giftcode');
                            if(strlen($giftcode) == 0) {
                                $err .= "Du lieu tra ve loi. Vui long lien he Admin de FIX";
                                $err_flag = true;
                            }
                        } else {
                            $err .= "Ket noi API gap su co. Admin MU vui long lien he nha cung cap DWebMu de kiem tra";
                            $err_flag = true;
                        }
                    }
                    //writelog('log_flappybird.txt', "Giftcode da tao : ". $giftcode);
                    if($gift_slg == 1) {
                        if($gift3_exists === false && $top_score_f[0] >= $gift3_score) {
                            $giftcode_insert_query = "INSERT INTO GiftCode (gift_code, acc, type, giftcode_type, gift_time, ngay, status) VALUES ('$giftcode', '". $username ."', 4, $gift3_type, $timestamp, '".date("Y-m-d",$timestamp)."', 1)";
                            $giftcode_insert_result = $db->Execute($giftcode_insert_query);
                                check_queryerror($giftcode_insert_query, $giftcode_insert_result);
                            $output_arr['message'][2] = $giftcode;
                        } else if($gift2_exists === false && $top_score_f[0] >= $gift2_score) {
                            $giftcode_insert_query = "INSERT INTO GiftCode (gift_code, acc, type, giftcode_type, gift_time, ngay, status) VALUES ('$giftcode', '". $username ."', 4, $gift2_type, $timestamp, '".date("Y-m-d",$timestamp)."', 1)";
                            $giftcode_insert_result = $db->Execute($giftcode_insert_query);
                                check_queryerror($giftcode_insert_query, $giftcode_insert_result);
                            $output_arr['message'][1] = $giftcode;
                        } else {
                            $giftcode_insert_query = "INSERT INTO GiftCode (gift_code, acc, type, giftcode_type, gift_time, ngay, status) VALUES ('$giftcode', '". $username ."', 4, $gift1_type, $timestamp, '".date("Y-m-d",$timestamp)."', 1)";
                            $giftcode_insert_result = $db->Execute($giftcode_insert_query);
                                check_queryerror($giftcode_insert_query, $giftcode_insert_result);
                            $output_arr['message'][0] = $giftcode;
                        }
                    } else {
                        $giftcode_arr = json_decode($giftcode, true);
                        foreach($giftcode_arr as $key => $giftcode_val) {
                            if($gift3_exists === false && $top_score_f[0] >= $gift3_score) {
                                $giftcode_insert_query = "INSERT INTO GiftCode (gift_code, acc, type, giftcode_type, gift_time, ngay, status) VALUES ('$giftcode_val', '". $username ."', 4, $gift3_type, $timestamp, '".date("Y-m-d",$timestamp)."', 1)";
                                $giftcode_insert_result = $db->Execute($giftcode_insert_query);
                                    check_queryerror($giftcode_insert_query, $giftcode_insert_result);
                                $output_arr['message'][2] = $giftcode_val;
                                $gift3_exists = true;
                                
                            } else if($gift2_exists === false && $top_score_f[0] >= $gift2_score) {
                                $giftcode_insert_query = "INSERT INTO GiftCode (gift_code, acc, type, giftcode_type, gift_time, ngay, status) VALUES ('$giftcode_val', '". $username ."', 4, $gift2_type, $timestamp, '".date("Y-m-d",$timestamp)."', 1)";
                                $giftcode_insert_result = $db->Execute($giftcode_insert_query);
                                    check_queryerror($giftcode_insert_query, $giftcode_insert_result);
                                $output_arr['message'][1] = $giftcode_val;
                                $gift2_exists = true;
                                
                            } else if($gift1_exists === false && $top_score_f[0] >= $gift1_score) {
                                $giftcode_insert_query = "INSERT INTO GiftCode (gift_code, acc, type, giftcode_type, gift_time, ngay, status) VALUES ('$giftcode_val', '". $username ."', 4, $gift1_type, $timestamp, '".date("Y-m-d",$timestamp)."', 1)";
                                $giftcode_insert_result = $db->Execute($giftcode_insert_query);
                                    check_queryerror($giftcode_insert_query, $giftcode_insert_result);
                                $output_arr['message'][0] = $giftcode_val;
                                $gift1_exists = true;
                            }
                        }
                    }
                    
                    foreach($output_arr['message'] as $key => $val) {
                        if(strlen($val) == 0) {
                            unset($output_arr['message'][$key]);
                        }
                    }
                }
            }
        }
        
        echo json_encode($output_arr);
	break;

	default :      // topscore
        $output_arr['error'] = false;
        
        $top_score_q = "SELECT DISTINCT TOP 10 AccountID, MAX(Score) FROM NBB_FlappyBird JOIN MEMB_INFO ON NBB_FlappyBird.AccountID collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND bloc_code=0 GROUP BY AccountID ORDER BY MAX(Score) DESC";
        $top_score_r = $db->Execute($top_score_q);
            check_queryerror($top_score_q, $top_score_r);
        while($top_score_f = $top_score_r->FetchRow()) {
            $top[] = array(
                'acc'   =>  substr($top_score_f[0], 0, 3) ."*****". substr($top_score_f[0], -2),
                'score'   =>  $top_score_f[1]
            );
        }
        
        $output_arr['message'] = $top;
        
        echo json_encode($output_arr);
}

//echo "Success";
?>