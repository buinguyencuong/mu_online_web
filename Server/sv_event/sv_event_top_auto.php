<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
	include_once("security.php");
include_once("config.php");
include_once('config/config_thehe.php');
include_once('config/config_vip.php');
include_once("function.php");

$passtransfer = $_POST["passtransfer"];

if ($passtransfer == $transfercode) {
    $action = $_POST['action'];
    
    switch ($action){ 
    	case 'nhanthuong':
            $login=$_POST["login"];
            $name=$_POST["name"];
            $type = $_POST['type'];
            $date = $_POST['date'];
            
            $top = 0;
            $top_check = 0;
            $danhan = 0;
            
            $acc_info_q = "SELECT thehe FROM MEMB_INFO WHERE memb___id='$login'";
            $acc_info_r = $db->Execute($acc_info_q);
                check_queryerror($acc_info_q, $acc_info_r);
            $acc_info_f = $acc_info_r->FetchRow();
            $thehe = $acc_info_f[0];
            
            switch ($type){ 
            	case 'rs':
                    $top_type_name = "Reset";
                    $top_type_notice_name = "Bá Đạo";
                    switch ($date){ 
                    	case 'day':
                            $top_date_name = "Ngày";
                            //********* TOP Reset Score Day **************************
                            $date_now = date('d/m', $timestamp);
                            
                            $day_yesterday = date('d', $timestamp - 24*60*60);
                            $month_yesterday = date('m', $timestamp - 24*60*60);
                            $year_yesterday = date('Y', $timestamp - 24*60*60);
                            $date_yesterday = $day_yesterday ."/". $month_yesterday;
                            $date_begin = strtotime(date('Y-m-d', $timestamp));;
                            
                            // TOP Daily YESTERDAY
                            $top_daily_query = "SELECT TOP 3 TopReset.name, reset_all, Class, thehe, lastrs_time, nbb_vip_time, nbb_vip_time_begin FROM TopReset JOIN MEMB_INFO ON TopReset.acc collate DATABASE_DEFAULT =MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=". $thehe ." AND [day]=". $day_yesterday ." AND [month]=". $month_yesterday ." AND [year]=". $year_yesterday ." JOIN Character ON TopReset.name collate DATABASE_DEFAULT = Character.Name collate DATABASE_DEFAULT ORDER BY reset_all DESC, lastrs_time";
                            $top_daily_result = $db->Execute($top_daily_query);
                                check_queryerror($top_daily_query, $top_daily_result);
                            while($top_daily_fetch = $top_daily_result->FetchRow()) {
                                $top_check++;
                                
                                if($name == $top_daily_fetch[0]) {
                                    $top = $top_check;
                                }
                            }
                    	break;
                    
                    	case 'week':
                            $top_date_name = "Tuần";
                            //********* TOP Reset Score WEEK **************************
                            $week_now = date('W', $timestamp);
                            $week_before = date('W', $timestamp - 7*24*60*60);
                            $date_begin = (date('N', $timestamp) == 1) ? strtotime(date('Y-m-d', $timestamp)) : strtotime(date('Y-m-d', strtotime('last Monday', $timestamp)));;
                                                        
                            // TOP Week Before
                            $top_week_query = "SELECT TOP 3 A.name, SUM(reset_all) AS ResetScoreWeek, Class, thehe, (SELECT TOP 1 lastrs_time FROM TopReset C WHERE C.name=A.name AND [week]=". $week_before ." ORDER BY lastrs_time DESC) AS lastreset_time, nbb_vip_time, nbb_vip_time_begin FROM TopReset A JOIN MEMB_INFO B ON A.acc collate DATABASE_DEFAULT = B.memb___id collate DATABASE_DEFAULT AND thehe=". $thehe ." AND [week]=". $week_before ." JOIN Character ON A.name collate DATABASE_DEFAULT = Character.Name collate DATABASE_DEFAULT GROUP BY A.name, Class, Thehe, nbb_vip_time, nbb_vip_time_begin ORDER BY ResetScoreWeek DESC, lastreset_time";
                            $top_week_result = $db->Execute($top_week_query);
                                check_queryerror($top_week_query, $top_week_result);
                            while($top_week_fetch = $top_week_result->FetchRow()) {
                                $top_check++;
                                
                                if($name == $top_week_fetch[0]) {
                                    $top = $top_check;
                                }
                            }
                    	break;
                    
                    	case 'month':
                            $top_date_name = "Tháng";
                            //********* TOP Reset Score MONTH **************************
                            if($month == 1) {
                                $month_before = 12;
                                $year_before = $year - 1;
                            } else {
                                $month_before = $month - 1;
                                $year_before = $year;
                            }
                            $date_begin = strtotime(date('Y-m-01'));
                
                            // TOP Month Before
                            $top_month_query = "SELECT TOP 3 A.name, SUM(reset_all) AS ResetScoreMonth, Class, thehe, (SELECT TOP 1 lastrs_time FROM TopReset C WHERE C.name=A.name AND [month]=". $month_before ." AND [year]=". $year_before ." ORDER BY lastrs_time DESC) AS lastreset_time, nbb_vip_time, nbb_vip_time_begin FROM TopReset A JOIN MEMB_INFO B ON A.acc collate DATABASE_DEFAULT = B.memb___id collate DATABASE_DEFAULT AND thehe=". $thehe ." AND [month]=". $month_before ." AND [year]=". $year_before ." JOIN Character ON A.name collate DATABASE_DEFAULT = Character.Name collate DATABASE_DEFAULT GROUP BY A.name, Class, Thehe, nbb_vip_time, nbb_vip_time_begin ORDER BY ResetScoreMonth DESC, lastreset_time";
                            $top_month_result = $db->Execute($top_month_query);
                                check_queryerror($top_month_query, $top_month_result);
                            while($top_month_fetch = $top_month_result->FetchRow()) {
                                $top_check++;
                                
                                if($name == $top_month_fetch[0]) {
                                    $top = $top_check;
                                }
                            }
                    	break;
                    }
                    
                    
            	break;
            
            	case 'card':
                    $top_type_name = "Nạp thẻ";
                    $top_type_notice_name = "Bá Giả";
                    switch ($date){ 
                    	case 'day':
                            $top_date_name = "Ngày";
                            //********* TOP Reset Score Day **************************
                            $date_now = date('d/m', $timestamp);
                            $day_now_begin = date('Y-m-d', $timestamp);
                            $day_now_end = date('Y-m-d', $timestamp + 24*60*60);
                            
                            $day_before = date('d/m', $timestamp - 24*60*60);
                            $day_before_begin = date('Y-m-d', $timestamp - 24*60*60);
                            $day_before_end = date('Y-m-d', $timestamp);
                            $date_begin = strtotime(date('Y-m-d', $timestamp));;
                            
                            // TOP Daily YESTERDAY
                            $top_daily_query = "SELECT TOP 3 acc, SUM(Event_TOP_Card.gcoin) as CardTotal, nbb_vip_time, nbb_vip_time_begin FROM Event_TOP_Card JOIN MEMB_INFO ON Event_TOP_Card.acc collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=$thehe AND [time] >= '$day_before_begin' AND [time] < '$day_before_end' GROUP BY acc, nbb_vip_time, nbb_vip_time_begin ORDER BY CardTotal DESC, acc";
                            $top_daily_result = $db->Execute($top_daily_query);
                                check_queryerror($top_daily_query, $top_daily_result);
                            while($top_daily_fetch = $top_daily_result->FetchRow()) {
                                $top_check++;
                                
                                if($login == $top_daily_fetch[0]) {
                                    $top = $top_check;
                                }
                            }
                    	break;
                    
                    	case 'week':
                            $top_date_name = "Tuần";
                            //********* TOP Reset Score WEEK **************************
                            $week_now = date('W', $timestamp);
                            $week_now_begin = (date('N', $timestamp) == 1) ? date('Y-m-d', $timestamp) : date('Y-m-d', strtotime('last Monday', $timestamp));
                            
                            $week_before_time = $timestamp - 7*24*60*60;
                            $week_before = date('W', $week_before_time);
                            $week_before_begin = (date('N', $week_before_time) == 1) ? date('Y-m-d', $week_before_time) : date('Y-m-d', strtotime('last Monday', $week_before_time));
                            $week_before_end = $week_now_begin;
                            $date_begin = strtotime($week_now_begin);
                                                        
                            // TOP Week Before
                            $top_week_query = "SELECT TOP 3 acc, SUM(Event_TOP_Card.gcoin) as CardTotal, nbb_vip_time, nbb_vip_time_begin FROM Event_TOP_Card JOIN MEMB_INFO ON Event_TOP_Card.acc collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=$thehe AND [time] >= '$week_before_begin' AND [time] < '$week_before_end' GROUP BY acc, nbb_vip_time, nbb_vip_time_begin ORDER BY CardTotal DESC, acc";
                            $top_week_result = $db->Execute($top_week_query);
                                check_queryerror($top_week_query, $top_week_result);
                            while($top_week_fetch = $top_week_result->FetchRow()) {
                                $top_check++;
                                
                                if($login == $top_week_fetch[0]) {
                                    $top = $top_check;
                                }
                            }
                    	break;
                    
                    	case 'month':
                            $top_date_name = "Tháng";
                            //********* TOP Reset Score MONTH **************************
                            $month_now_time = strtotime(date('Y-m-01'));
                            $month_now = date('m', $month_now_time);
                            $month_now_begin = date('Y-m-d', $month_now_time);
                            
                            $month_before_time = strtotime('-1 month', $month_now_time);
                            $month_before = date('m', $month_before_time);
                            $month_before_begin = date('Y-m-d', $month_before_time);
                            $month_before_end = $month_now_begin;
                            $date_begin = $month_now_time;
                            
                            // TOP Month Before
                            $top_month_query = "SELECT TOP 3 acc, SUM(Event_TOP_Card.gcoin) as CardTotal, nbb_vip_time, nbb_vip_time_begin FROM Event_TOP_Card JOIN MEMB_INFO ON Event_TOP_Card.acc collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=$thehe AND [time] >= '$month_before_begin' AND [time] < '$month_before_end' GROUP BY acc, nbb_vip_time, nbb_vip_time_begin ORDER BY CardTotal DESC, acc";
                            $top_month_result = $db->Execute($top_month_query);
                                check_queryerror($top_month_query, $top_month_result);
                            while($top_month_fetch = $top_month_result->FetchRow()) {
                                $top_check++;
                                
                                if($login == $top_month_fetch[0]) {
                                    $top = $top_check;
                                }
                            }
                    	break;
                    }
            	break;
            }
            
            if($top == 0) {
                echo "Nhân vật không nằm trong TOP. Vui lòng kiểm tra lại hoặc liên hệ ADMIN nếu có lỗi."; exit();
            } else {
                // Check da nhan
                $danhan_c_q = "SELECT time FROM nbb_event_top_auto WHERE acc='$login' AND name='$name' AND event_type='$type' AND event_date='$date' AND time >= $date_begin AND time <= $timestamp";
                $danhan_c_r = $db->Execute($danhan_c_q);
                    check_queryerror($danhan_c_q, $danhan_c_r);
                $danhan = $danhan_c_r->NumRows();
                
                if($danhan == 0) {  //==> Chua nhan - Phat thuong
                    // Read Config
                    $file_gift_data = 'config/event_top_rscard_auto.txt';
                    $data_cfg_arr = _json_fileload($file_gift_data);
                    // Read Config End
                    
                    switch($date)
                    {
                    	case "week":
                            $week_begin = (date('N', $timestamp) == 1) ? strtotime(date('Y-m-d', $timestamp)) : strtotime('last Monday', $timestamp);
                            if(strtotime($TopAuto_RS_Week_Begin) < $week_begin) {
                                $allow_nhangiai = true;
                                $date_now_index = date('W', $week_begin) % 2;
                                $date_before_index = date('W', $week_begin - 24*60*60) % 2;
                            }
                        break;
                        
                    	case "month":
                            $month_begin = (date('d', $timestamp) == 1) ? strtotime(date('Y-m-d', $timestamp)) : strtotime(date('Y-m-01'));
                            if(strtotime($TopAuto_RS_Month_Begin) < $month_begin) {
                                $allow_nhangiai = true;
                                $date_now_index = date('m', $month_begin) % 2;
                                $date_before_index = date('m', $month_begin - 24*60*60) % 2;
                            }
                        break;
                        
                        default:
                            $yesterday = $timestamp - 24*60*60;
                            if(strtotime($TopAuto_RS_Day_Begin) < $yesterday) {
                                $allow_nhangiai = true;
                                $date_now_index = date('w', $timestamp);
                                $date_before_index = date('w', $yesterday);
                            }
                    }
                    $gift = $data_cfg_arr[$type][$date][$date_before_index][$top];
                    //echo json_encode($data_cfg_arr[$type][$date][$date_before_index]); exit();
                    
                    $char_q = "SELECT class FROM Character WHERE name='$name'";
                    $char_r = $db->Execute($char_q);
                        check_queryerror($char_q, $char_r);
                    $char_f = $char_r->FetchRow();
                    $char_class = $char_f[0];
                    
                    switch ($char_class){ 
                    	case 0:
                        case 1:
                        case 2:
                        case 3:
                            $class_name = "DW";
                            $item = $gift['item_dw'];
                            $msg_item = $gift['msg_item_dw'];
                            
                            $item_time = $gift['item_time_dw'];
                            $msg_item_time = $gift['msg_item_time_dw'];
                            $day_item_time = $gift['day_item_time_dw'];
                    	break;
                    
                    	case 16:
                        case 17:
                        case 18:
                        case 19:
                            $class_name = "DK";
                            $item = $gift['item_dk'];
                            $msg_item = $gift['msg_item_dk'];
                            
                            $item_time = $gift['item_time_dk'];
                            $msg_item_time = $gift['msg_item_time_dk'];
                            $day_item_time = $gift['day_item_time_dk'];
                    	break;
                    
                    	case 32:
                        case 33:
                        case 34:
                        case 35:
                            $class_name = "ELF";
                            $item = $gift['item_elf'];
                            $msg_item = $gift['msg_item_elf'];
                            
                            $item_time = $gift['item_time_elf'];
                            $msg_item_time = $gift['msg_item_time_elf'];
                            $day_item_time = $gift['day_item_time_elf'];
                    	break;
                        
                        case 48:
                        case 49:
                        case 50:
                            $class_name = "MG";
                            $item = $gift['item_mg'];
                            $msg_item = $gift['msg_item_mg'];
                            
                            $item_time = $gift['item_time_mg'];
                            $msg_item_time = $gift['msg_item_time_mg'];
                            $day_item_time = $gift['day_item_time_mg'];
                    	break;
                        
                        case 64:
                        case 65:
                        case 66:
                            $class_name = "DL";
                            $item = $gift['item_dl'];
                            $msg_item = $gift['msg_item_dl'];
                            
                            $item_time = $gift['item_time_dl'];
                            $msg_item_time = $gift['msg_item_time_dl'];
                            $day_item_time = $gift['day_item_time_dl'];
                    	break;
                        
                        case 80:
                        case 81:
                        case 82:
                        case 83:
                            $class_name = "SUM";
                            $item = $gift['item_sum'];
                            $msg_item = $gift['msg_item_sum'];
                            
                            $item_time = $gift['item_time_sum'];
                            $msg_item_time = $gift['msg_item_time_sum'];
                            $day_item_time = $gift['day_item_time_sum'];
                    	break;
                        
                        case 96:
                        case 97:
                        case 98:
                            $class_name = "RF";
                            $item = $gift['item_rf'];
                            $msg_item = $gift['msg_item_rf'];
                            
                            $item_time = $gift['item_time_rf'];
                            $msg_item_time = $gift['msg_item_time_rf'];
                            $day_item_time = $gift['day_item_time_rf'];
                    	break;
                    
                    	default :  echo "Lớp nhân vật chưa định nghĩa."; exit();
                    }
                    
                    $item .= $gift['item_all'];
                    $online_check = 0;
                    
                    if(strlen($item) > 0 || (strlen($gift['item_time_all']) > 0 && $gift['day_item_time_all'] > 0) || (strlen($item_time) > 0 && $day_item_time > 0) ) {
                        $warehouse1 = '';
                        
                        if(strlen($item) > 0) {
                            $warehouse_query = "SELECT CAST(Items AS image) FROM warehouse WHERE AccountID = '$login'";
                            $warehouse_result = $db->Execute($warehouse_query);
                                check_queryerror($warehouse_query, $warehouse_result);
                            $warehouse_c = $warehouse_result->NumRows();
                            if($warehouse_c == 0) {
                                echo "Chưa mở Rương đồ chung. Hãy vào Game, mở Rương đồ chung, sau đó thoát Game rồi nhận thưởng.";
                                exit();
                            }
                            $warehouse_fetch = $warehouse_result->FetchRow();
                            $warehouse = $warehouse_fetch[0];
                            $warehouse = bin2hex($warehouse);
                            $warehouse = strtoupper($warehouse);
                            
                            $warehouse1 = substr($warehouse,0,120*32);
                            $warehouse2 = substr($warehouse,120*32);
                            
                            if(strlen($warehouse1) < 120*32) {
                                echo "Chưa 1 lần mở hòm đồ chung. Vui lòng vào Game mở hòm đồ chung ít nhất 1 lần mới được nhận thưởng.";
                                exit();
                            }
                        }
                        
                        $item_time_arr[0] = array(
                            'itemcode'  =>  $gift['item_time_all'],
                            'day'   =>  $gift['day_item_time_all']
                        );
                        $item_time_arr[1] = array(
                            'itemcode'  =>  $item_time,
                            'day'   =>  $day_item_time
                        );
                        
                        include_once('config_license.php');
                        include_once('func_getContent.php');
                        $getcontent_url = $url_license . "/api_event_top_auto.php";
                        $getcontent_data = array(
                            'acclic'    =>  $acclic,
                            'key'    =>  $key,
                            
                            'action'    =>  'gift_item',
                            
                            'item'    =>  $item,
                            'item_time' =>  $item_time_arr,
                            'warehouse1'   =>  $warehouse1
                        );
                        
                        $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
                        
                        if ( empty($reponse) ) {
                            $notice = "Server bảo trì vui lòng liên hệ Admin để FIX";
                            echo $notice;
                            exit();
                        }
                        else {
                            $info = read_TagName($reponse, 'info');
                            if($info == "Error") {
                                $message = read_TagName($reponse, 'msg');
                                echo $message;
                                exit();
                            } elseif($info == "OK") {
                                $item_gift = read_TagName($reponse, 'item_gift');
                                $item_time_gift = read_TagName($reponse, 'item_time_gift');
                                
                                if(strlen($item_gift) == 0 && strlen($item_time_gift) == 0) {
                                    echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                                    $arr_view = "\nDataSend:\n";
                                    foreach($getcontent_data as $k => $v) {
                                        $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                                    }
                                    writelog("log_api.txt", $arr_view . $reponse);
                                    exit();
                                }
                            } else {
                                echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
                                writelog("log_api.txt", $reponse);
                                exit();
                            }
                        }
                    }
                    
                    $item_gift_arr = json_decode($item_gift, true);
                    
                    $itemgift_count = count($item_gift_arr);
                    if($itemgift_count > 0) {
                        
                        $warehouse1_new = $warehouse1;
                        for($i=0; $i<$itemgift_count; $i++) {
                            $item = $item_gift_arr[$i]['code'];
                                
                            $item_seri = substr($item, 6, 8);
                            $item_seri_dec = hexdec($item_seri);
                            
                            if($item_seri_dec < hexdec('FFFFFFF0')) {
                            	$serial = _getSerial();
                            	$item = substr_replace($item, $serial, 6, 8);
                            }
                            $warehouse1_new = substr_replace($warehouse1_new, $item, $item_gift_arr[$i]['vitri']*32, 32);
                        }
                        kiemtra_online($login);
                        
                        $warehouse_new = $warehouse1_new . $warehouse2;
                        
                        $warehouse_update_query = "UPDATE warehouse SET Items=0x$warehouse_new WHERE AccountID='$login'";
                        $warehouse_update_result = $db->Execute($warehouse_update_query);
                            check_queryerror($warehouse_update_query, $warehouse_update_result);
                        
                        $notice_gift_item .= $gift['msg_item_all'] ."<br />". $msg_item;
                    }
                    
                    $item_time_gift_arr = json_decode($item_time_gift, true);
                    $item_time_gift_count = count($item_time_gift_arr);
                    
                    for($i=0; $i< $item_time_gift_count; $i++) {
                        $nbb_time_del = 0;
                        switch ($date){ 
                        	case 'day':
                                $nbb_time_del = $timestamp + 2*24*60*60;
                        	break;
                        
                        	case 'week':
                                $nbb_time_del = $timestamp + 8*24*60*60;
                        	break;
                        
                        	case 'month':
                                $nbb_time_del = $timestamp + 35*24*60*60;
                        	break;
                        }
                        $item_time_insert_q = "INSERT INTO Titan_Rewards (AccountID, Name, Zen, VIPMoney, Num, Lvl, Opt, Luck, Skill, Dur, Excellent, Ancient, JOH, Sock1, Sock2, Sock3, Sock4, Sock5, Days, SerialFFFFFFFE, type, nbb_time_add, nbb_time_del) VALUES ('$login', '$name', 0, 0, ". $item_time_gift_arr[$i]['Num'] .", ". $item_time_gift_arr[$i]['Lvl'] .", ". $item_time_gift_arr[$i]['Opt'] .", ". $item_time_gift_arr[$i]['Luck'] .", ". $item_time_gift_arr[$i]['Skill'] .", ". $item_time_gift_arr[$i]['Dur'] .", ". $item_time_gift_arr[$i]['Excellent'] .", ". $item_time_gift_arr[$i]['Ancient'] .", ". $item_time_gift_arr[$i]['JOH'] .", ". $item_time_gift_arr[$i]['Sock1'] .", ". $item_time_gift_arr[$i]['Sock2'] .", ". $item_time_gift_arr[$i]['Sock3'] .", ". $item_time_gift_arr[$i]['Sock4'] .", ". $item_time_gift_arr[$i]['Sock5'] .", ". $item_time_gift_arr[$i]['Days'] .", ". $item_time_gift_arr[$i]['SerialFFFFFFFE'] .", 99, ". $timestamp .", ". $nbb_time_del .")";
                        $item_time_insert_r = $db->Execute($item_time_insert_q);
                            check_queryerror($item_time_insert_q, $item_time_insert_r);
                    }
                    
                    $item_time_msg = '';
                    if(strlen($gift['item_time_all']) > 0 && $gift['day_item_time_all'] > 0) {
                        $item_time_msg .= "<br />Phần thưởng Item sử dụng trong <strong>". $gift['day_item_time_all'] ." ngày</strong> : ". $gift['msg_item_time_all'];
                    }
                    if(strlen($item_time) > 0 && $day_item_time > 0) {
                        $item_time_msg .= "<br />Phần thưởng Item sử dụng trong <strong>". $day_item_time ." ngày</strong> : ". $msg_item_time;
                    }
                    if(strlen($item_time_msg) > 0) {
                        $item_time_msg .= "<br />nhận tại NPC <strong>ReWard - Lorencia (143, 138) Server-1</strong> .";
                    }
                    
                    $buff_arr = array();
                    if(isset($gift['buff_day']) && $gift['buff_day'] > 0) {
                        $buff_get_q = "SELECT SCFSealItem, SCFSealTime FROM Character WHERE Name='$name'";
                        $buff_get_r = $db->Execute($buff_get_q);
                            check_queryerror($buff_get_q, $buff_get_r);
                        $buff_get_f = $buff_get_r->FetchRow();
                        
                        if($buff_get_f[1] >= $timestamp) {
                            $SCFSealTime_new = $buff_get_f[1] + $gift['buff_day']*24*60*60;
                            $buff_q = "UPDATE Character SET SCFSealTime = $SCFSealTime_new WHERE Name='$name'";
                            $buff_notice = "<br /><strong>Bùa</strong> : Tăng thời gian Bùa đang sử dụng thêm <strong>". $gift['buff_day'] ." ngày</strong>";
                            
                            $buff_arr['SCFSealItem'] = 0;
                            $buff_arr['SCFSealTime'] = $SCFSealTime_new;
                        } else {
                            $SCFSealTime_new = $timestamp + $gift['buff_day']*24*60*60;
                            $buff_q = "UPDATE Character SET SCFSealItem=6700, SCFSealTime = $SCFSealTime_new WHERE Name='$name'";
                            $buff_notice = "<br /><strong>Bùa</strong> : Nhận Buff <strong>Bùa thiên sứ</strong> sử dụng trong <strong>". $gift['buff_day'] ." ngày</strong>";
                            $buff_arr['SCFSealItem'] = 6700;
                            $buff_arr['SCFSealTime'] = $SCFSealTime_new;
                        }
                        
                        if(strlen($buff_q) > 0) {
                            $buff_r = $db->Execute($buff_q);
                                check_queryerror($buff_q, $buff_r);
                            $buff_data = json_encode($buff_arr);
                        }
                    }
                    
                    
                    $bank_update = "";
                    $nganhang_msg = "";
                    
                    if($gift['gcent_km'] > 0) {
                        if(strlen($bank_update) > 0) $bank_update .= ",";
                        $bank_update .= "gcoin_km = gcoin_km + ". $gift['gcent_km'];
                        
                        if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
                        $nganhang_msg .= $gift['gcent_km'] ." Gcent+";
                    }
                    if($gift['vcent_km'] > 0) {
                        if(strlen($bank_update) > 0) $bank_update .= ",";
                        $bank_update .= "vpoint_km = vpoint_km + ". $gift['vcent_km'];
                        
                        if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
                        $nganhang_msg .= $gift['vcent_km'] ." VCent+";
                    }
                    if($gift['zen'] > 0) {
                        if(strlen($bank_update) > 0) $bank_update .= ",";
                        $bank_update .= "bank = bank + ". $gift['zen'];
                        
                        if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
                        $nganhang_msg .= $gift['zen'] ." ZEN";
                    }
                    if($gift['chao'] > 0) {
                        if(strlen($bank_update) > 0) $bank_update .= ",";
                        $bank_update .= "jewel_chao = jewel_chao + ". $gift['chao'];
                        
                        if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
                        $nganhang_msg .= $gift['chao'] ." Ngọc Hỗn Nguyên";
                    }
                    if($gift['cre'] > 0) {
                        if(strlen($bank_update) > 0) $bank_update .= ",";
                        $bank_update .= "jewel_cre = jewel_cre + ". $gift['cre'];
                        
                        if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
                        $nganhang_msg .= $gift['cre'] ." Ngọc Sáng Tạo";
                    }
                    if($gift['blue'] > 0) {
                        if(strlen($bank_update) > 0) $bank_update .= ",";
                        $bank_update .= "jewel_blue = jewel_blue + ". $gift['blue'];
                        
                        if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
                        $nganhang_msg .= $gift['blue'] ." Lông Vũ";
                    }
                    if($gift['pp_extra'] > 0) {
                        if(strlen($bank_update) > 0) $bank_update .= ",";
                        $bank_update .= "nbb_pl_extra = nbb_pl_extra + ". $gift['pp_extra'];
                        
                        if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
                        $nganhang_msg .= $gift['pp_extra'] ." PP+";
                    }
                    
                    $bank_arr = array();
                    if(strlen($bank_update) > 0) {
                        $bank_update_q = "UPDATE MEMB_INFO SET $bank_update WHERE memb___id='$login'";
                        $bank_update_r = $db->Execute($bank_update_q);
                            check_queryerror($bank_update_q, $bank_update_r);
                        
                        $bank_select_q = "SELECT gcoin_km, vpoint_km, bank, jewel_chao, jewel_cre, jewel_blue, nbb_pl_extra FROM MEMB_INFO WHERE memb___id='$login'";
                        $bank_select_r = $db->Execute($bank_select_q);
                            check_queryerror($bank_select_q, $bank_select_r);
                        $bank_select_f = $bank_select_r->FetchRow();
                        $bank_arr = array(
                            'gcoin_km'  =>  $bank_select_f[0],
                            'vpoint_km'  =>  $bank_select_f[1],
                            'bank'  =>  $bank_select_f[2],
                            'jewel_chao'  =>  $bank_select_f[3],
                            'jewel_cre'  =>  $bank_select_f[4],
                            'jewel_blue'  =>  $bank_select_f[5],
                            'nbb_pl_extra'  =>  $bank_select_f[6]
                        );
                    }
                    $bank_data = json_encode($bank_arr);
                    
                    // Insert History
                    $history_q = "INSERT INTO nbb_event_top_auto VALUES ('$login', '$name', '$type', '$date', GETDATE(), $timestamp)";
                    $history_r = $db->Execute($history_q);
                        check_queryerror($history_q, $history_r);
                    // Insert History End
                    
                    //Ghi vào Log
                        $log_price = "-";
                        $log_Des = "Nhân vật <strong>$name</strong> đã nhận thưởng <strong>TOP $top $top_type_name $top_date_name</strong> .<br /> Phần thưởng : $notice_gift_item . $item_time_msg . <br />Ngân hàng : $nganhang_msg . $buff_notice";
                        _writelog_tiente($login, $log_price, $log_Des, 8);
                    //End Ghi vào Log nhung nhan vat mua Item
                    
                    $notice = "Bạn đã nhận thưởng <strong>TOP $top $top_type_notice_name $top_date_name</strong> .";
                    if(strlen($notice_gift_item) > 0) {
                        $notice .= "<br><strong>Phần thưởng không giới hạn thời gian</strong> :<br /> $notice_gift_item <br />đã được đưa vào rương đồ chung.";
                    }
                    if(strlen($item_time_msg) > 0) {
                        $notice .= $item_time_msg;
                    }
                    if(strlen($nganhang_msg) > 0) {
                        $notice .= "<br /><strong>Ngân hàng</strong> : $nganhang_msg";
                    }
                    if(strlen($buff_notice) > 0) {
                        $notice .= "$buff_notice";
                    }
                    
                    echo "
                        <info>OK</info>
                        <msg>$notice</msg>
                        <time_receive>$timestamp</time_receive>
                        <bank_data>$bank_data</bank_data>
                        <buff_data>$buff_data</buff_data>
                    ";
                }   //==> nhan thuong end
                else {
                    echo "Đã nhận phần thưởng này."; exit();
                }
            }
    	break;
    
    	default :  // GET TOP
            $rank_slg = 10;
            
            $type = $_POST['type'];
            $date = $_POST['date'];
            switch ($type){ 
            	case 'rs':
                    switch ($date){ 
                    	case 'day':
                            //********* TOP Reset Score Day **************************
                            $date_now = date('d/m', $timestamp);
                            
                            $day_yesterday = date('d', $timestamp - 24*60*60);
                            $month_yesterday = date('m', $timestamp - 24*60*60);
                            $year_yesterday = date('Y', $timestamp - 24*60*60);
                            $date_yesterday = $day_yesterday ."/". $month_yesterday;
                            
                            for($i=1;$i<count($thehe_choise);$i++) {
                                if(strlen($thehe_choise[$i]) > 0) {
                                    // TOP Daily NOW
                                    $TopData_Arr[$date_now][$i] = array();
                                    $top_daily_query = "SELECT TOP $rank_slg TopReset.name, reset_all, Class, thehe, lastrs_time, nbb_vip_time, nbb_vip_time_begin FROM TopReset JOIN MEMB_INFO ON TopReset.acc collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=". $i ." AND [day]=". $day ." AND [month]=". $month ." AND [year]=". $year ." JOIN Character ON TopReset.name collate DATABASE_DEFAULT = Character.Name collate DATABASE_DEFAULT ORDER BY reset_all DESC, lastrs_time";
                                    $top_daily_result = $db->Execute($top_daily_query);
                                        check_queryerror($top_daily_query, $top_daily_result);
                                    while($top_daily_fetch = $top_daily_result->FetchRow()) {
                                        if($top_daily_fetch[5] > $timestamp) {
                                            if($top_daily_fetch[6] > 0) {
                                                $nbb_vip_time_used = $timestamp - $top_daily_fetch[6];
                                                $nbb_vip_time_used_day = floor($nbb_vip_time_used/(24*60*60));
                                                $vip = 1;
                                                if(is_array($vip_level_day)) {
                                                    foreach($vip_level_day as $k => $v) {
                                                        if($nbb_vip_time_used_day >= $v) $vip = $k+1;
                                                        else break;
                                                    }
                                                }
                                            } else $vip = 1;
                                        } else $vip = 0;
                                        
                                        $vip_day = 0;
                                        if($vip > 1) $vip_day = $vip_level_day[$vip - 1];
                                        
                                        $TopData_Arr[$date_now][$i][] = array(
                                            'name'  =>  $top_daily_fetch[0],
                                            'reset'   =>  $top_daily_fetch[1],
                                            'class' =>  $top_daily_fetch[2],
                                            'lastrs_time' =>  $top_daily_fetch[4],
                                            'vip' =>  $vip,
                                            'vip_day' =>  $vip_day,
                                            'thehe' =>  $top_daily_fetch[3]
                                        );
                                    }
                                    
                                    // TOP Daily YESTERDAY
                                    $TopData_Arr[$date_yesterday][$i] = array();
                                    $top_daily_query = "SELECT TOP $rank_slg TopReset.name, reset_all, Class, thehe, lastrs_time, nbb_vip_time, nbb_vip_time_begin FROM TopReset JOIN MEMB_INFO ON TopReset.acc collate DATABASE_DEFAULT =MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=". $i ." AND [day]=". $day_yesterday ." AND [month]=". $month_yesterday ." AND [year]=". $year_yesterday ." JOIN Character ON TopReset.name collate DATABASE_DEFAULT = Character.Name collate DATABASE_DEFAULT ORDER BY reset_all DESC, lastrs_time";
                                    $top_daily_result = $db->Execute($top_daily_query);
                                        check_queryerror($top_daily_query, $top_daily_result);
                                    while($top_daily_fetch = $top_daily_result->FetchRow()) {
                                        if($top_daily_fetch[5] > $timestamp) {
                                            if($top_daily_fetch[6] > 0) {
                                                $nbb_vip_time_used = $timestamp - $top_daily_fetch[6];
                                                $nbb_vip_time_used_day = floor($nbb_vip_time_used/(24*60*60));
                                                $vip = 1;
                                                if(is_array($vip_level_day)) {
                                                    foreach($vip_level_day as $k => $v) {
                                                        if($nbb_vip_time_used_day >= $v) $vip = $k+1;
                                                        else break;
                                                    }
                                                }
                                            } else $vip = 1;
                                        } else $vip = 0;
                                        
                                        $vip_day = 0;
                                        if($vip > 1) $vip_day = $vip_level_day[$vip - 1];
                                        
                                        $TopData_Arr[$date_yesterday][$i][] = array(
                                            'name'  =>  $top_daily_fetch[0],
                                            'reset'   =>  $top_daily_fetch[1],
                                            'class' =>  $top_daily_fetch[2],
                                            'lastrs_time' =>  $top_daily_fetch[4],
                                            'vip' =>  $vip,
                                            'vip_day' =>  $vip_day,
                                            'thehe' =>  $top_daily_fetch[3]
                                        );
                                    }
                                }
                            }
                    	break;
                    
                    	case 'week':
                            //********* TOP Reset Score WEEK **************************
                            $week_now = date('W', $timestamp);
                            $week_before = date('W', $timestamp - 7*24*60*60);
                                                        
                            for($i=1;$i<count($thehe_choise);$i++) {
                                if(strlen($thehe_choise[$i]) > 0) {
                                    // TOP WEEK NOW
                                    $TopData_Arr[$week_now][$i] = array();
                                    $top_week_query = "SELECT TOP $rank_slg A.name, SUM(reset_all) AS ResetScoreWeek, Class, thehe, (SELECT TOP 1 lastrs_time FROM TopReset C WHERE C.name=A.name AND [week]=". $week_now ." ORDER BY lastrs_time DESC) AS lastreset_time, nbb_vip_time, nbb_vip_time_begin FROM TopReset A JOIN MEMB_INFO B ON A.acc collate DATABASE_DEFAULT = B.memb___id collate DATABASE_DEFAULT AND thehe=". $i ." AND [week]=". $week_now ." JOIN Character ON A.name collate DATABASE_DEFAULT = Character.Name collate DATABASE_DEFAULT GROUP BY A.name, Class, Thehe, nbb_vip_time, nbb_vip_time_begin ORDER BY ResetScoreWeek DESC, lastreset_time";
                                    $top_week_result = $db->Execute($top_week_query);
                                        check_queryerror($top_week_query, $top_week_result);
                                    while($top_week_fetch = $top_week_result->FetchRow()) {
                                        if($top_week_fetch[5] > $timestamp) {
                                            if($top_week_fetch[6] > 0) {
                                                $nbb_vip_time_used = $timestamp - $top_week_fetch[6];
                                                $nbb_vip_time_used_day = floor($nbb_vip_time_used/(24*60*60));
                                                $vip = 1;
                                                if(is_array($vip_level_day)) {
                                                    foreach($vip_level_day as $k => $v) {
                                                        if($nbb_vip_time_used_day >= $v) $vip = $k+1;
                                                        else break;
                                                    }
                                                }
                                            } else $vip = 1;
                                        } else $vip = 0;
                                        
                                        $vip_day = 0;
                                        if($vip > 1) $vip_day = $vip_level_day[$vip - 1];
                                        
                                        $TopData_Arr[$week_now][$i][] = array(
                                            'name'  =>  $top_week_fetch[0],
                                            'reset'   =>  $top_week_fetch[1],
                                            'class' =>  $top_week_fetch[2],
                                            'lastrs_time' =>  $top_week_fetch[4],
                                            'vip' =>  $vip,
                                            'vip_day' =>  $vip_day,
                                            'thehe' =>  $top_week_fetch[3]
                                        );
                                    }
                                    
                                    // TOP Week Before
                                    $TopData_Arr[$week_before][$i] = array();
                                    $top_week_query = "SELECT TOP $rank_slg A.name, SUM(reset_all) AS ResetScoreWeek, Class, thehe, (SELECT TOP 1 lastrs_time FROM TopReset C WHERE C.name=A.name AND [week]=". $week_before ." ORDER BY lastrs_time DESC) AS lastreset_time, nbb_vip_time, nbb_vip_time_begin FROM TopReset A JOIN MEMB_INFO B ON A.acc collate DATABASE_DEFAULT = B.memb___id collate DATABASE_DEFAULT AND thehe=". $i ." AND [week]=". $week_before ." JOIN Character ON A.name collate DATABASE_DEFAULT = Character.Name collate DATABASE_DEFAULT GROUP BY A.name, Class, Thehe, nbb_vip_time, nbb_vip_time_begin ORDER BY ResetScoreWeek DESC, lastreset_time";
                                    $top_week_result = $db->Execute($top_week_query);
                                        check_queryerror($top_week_query, $top_week_result);
                                    while($top_week_fetch = $top_week_result->FetchRow()) {
                                        if($top_week_fetch[5] > $timestamp) {
                                            if($top_week_fetch[6] > 0) {
                                                $nbb_vip_time_used = $timestamp - $top_week_fetch[6];
                                                $nbb_vip_time_used_day = floor($nbb_vip_time_used/(24*60*60));
                                                $vip = 1;
                                                if(is_array($vip_level_day)) {
                                                    foreach($vip_level_day as $k => $v) {
                                                        if($nbb_vip_time_used_day >= $v) $vip = $k+1;
                                                        else break;
                                                    }
                                                }
                                            } else $vip = 1;
                                        } else $vip = 0;
                                        
                                        $vip_day = 0;
                                        if($vip > 1) $vip_day = $vip_level_day[$vip - 1];
                                        
                                        $TopData_Arr[$week_before][$i][] = array(
                                            'name'  =>  $top_week_fetch[0],
                                            'reset'   =>  $top_week_fetch[1],
                                            'class' =>  $top_week_fetch[2],
                                            'lastrs_time' =>  $top_week_fetch[4],
                                            'vip' =>  $vip,
                                            'vip_day' =>  $vip_day,
                                            'thehe' =>  $top_week_fetch[3]
                                        );
                                    }
                                }
                            }
                    	break;
                    
                    	case 'month':
                            //********* TOP Reset Score MONTH **************************
                            if($month == 1) {
                                $month_before = 12;
                                $year_before = $year - 1;
                            } else {
                                $month_before = $month - 1;
                                $year_before = $year;
                            }
                
                            for($i=1;$i<count($thehe_choise);$i++) {
                                if(strlen($thehe_choise[$i]) > 0) {
                                    // TOP MONTH NOW
                                    $TopData_Arr[$month][$i] = array();
                                    $top_month_query = "SELECT TOP $rank_slg A.name, SUM(reset_all) AS ResetScoreMonth, Class, thehe, (SELECT TOP 1 lastrs_time FROM TopReset C WHERE C.name=A.name AND [month]=". $month ." AND [year]=". $year ." ORDER BY lastrs_time DESC) AS lastreset_time, nbb_vip_time, nbb_vip_time_begin FROM TopReset A JOIN MEMB_INFO B ON A.acc collate DATABASE_DEFAULT = B.memb___id collate DATABASE_DEFAULT AND thehe=". $i ." AND [month]=". $month ." AND [year]=". $year ." JOIN Character ON A.name collate DATABASE_DEFAULT = Character.Name collate DATABASE_DEFAULT GROUP BY A.name, Class, Thehe, nbb_vip_time, nbb_vip_time_begin ORDER BY ResetScoreMonth DESC, lastreset_time";
                                    $top_month_result = $db->Execute($top_month_query);
                                        check_queryerror($top_month_query, $top_month_result);
                                    while($top_month_fetch = $top_month_result->FetchRow()) {
                                        if($top_month_fetch[5] > $timestamp) {
                                            if($top_month_fetch[6] > 0) {
                                                $nbb_vip_time_used = $timestamp - $top_month_fetch[6];
                                                $nbb_vip_time_used_day = floor($nbb_vip_time_used/(24*60*60));
                                                $vip = 1;
                                                if(is_array($vip_level_day)) {
                                                    foreach($vip_level_day as $k => $v) {
                                                        if($nbb_vip_time_used_day >= $v) $vip = $k+1;
                                                        else break;
                                                    }
                                                }
                                            } else $vip = 1;
                                        } else $vip = 0;
                                        
                                        $vip_day = 0;
                                        if($vip > 1) $vip_day = $vip_level_day[$vip - 1];
                                        
                                        $TopData_Arr[$month][$i][] = array(
                                            'name'  =>  $top_month_fetch[0],
                                            'reset'   =>  $top_month_fetch[1],
                                            'class' =>  $top_month_fetch[2],
                                            'lastrs_time' =>  $top_month_fetch[4],
                                            'vip' =>  $vip,
                                            'vip_day' =>  $vip_day,
                                            'thehe' =>  $top_month_fetch[3]
                                        );
                                    }
                                    
                                    // TOP Month Before
                                    $TopData_Arr[$month_before][$i] = array();
                                    $top_month_query = "SELECT TOP $rank_slg A.name, SUM(reset_all) AS ResetScoreMonth, Class, thehe, (SELECT TOP 1 lastrs_time FROM TopReset C WHERE C.name=A.name AND [month]=". $month_before ." AND [year]=". $year_before ." ORDER BY lastrs_time DESC) AS lastreset_time, nbb_vip_time, nbb_vip_time_begin FROM TopReset A JOIN MEMB_INFO B ON A.acc collate DATABASE_DEFAULT = B.memb___id collate DATABASE_DEFAULT AND thehe=". $i ." AND [month]=". $month_before ." AND [year]=". $year_before ." JOIN Character ON A.name collate DATABASE_DEFAULT = Character.Name collate DATABASE_DEFAULT GROUP BY A.name, Class, Thehe, nbb_vip_time, nbb_vip_time_begin ORDER BY ResetScoreMonth DESC, lastreset_time";
                                    $top_month_result = $db->Execute($top_month_query);
                                        check_queryerror($top_month_query, $top_month_result);
                                    while($top_month_fetch = $top_month_result->FetchRow()) {
                                        if($top_month_fetch[5] > $timestamp) {
                                            if($top_month_fetch[6] > 0) {
                                                $nbb_vip_time_used = $timestamp - $top_month_fetch[6];
                                                $nbb_vip_time_used_day = floor($nbb_vip_time_used/(24*60*60));
                                                $vip = 1;
                                                if(is_array($vip_level_day)) {
                                                    foreach($vip_level_day as $k => $v) {
                                                        if($nbb_vip_time_used_day >= $v) $vip = $k+1;
                                                        else break;
                                                    }
                                                }
                                            } else $vip = 1;
                                        } else $vip = 0;
                                        
                                        $vip_day = 0;
                                        if($vip > 1) $vip_day = $vip_level_day[$vip - 1];
                                        
                                        $TopData_Arr[$month_before][$i][] = array(
                                            'name'  =>  $top_month_fetch[0],
                                            'reset'   =>  $top_month_fetch[1],
                                            'class' =>  $top_month_fetch[2],
                                            'lastrs_time' =>  $top_month_fetch[4],
                                            'vip' =>  $vip,
                                            'vip_day' =>  $vip_day,
                                            'thehe' =>  $top_month_fetch[3]
                                        );
                                    }
                                }    
                            }
                    	break;
                    }
                    
                    $TopData = json_encode($TopData_Arr);
                    echo "<nbb>OK<nbb>" . $TopData . "<nbb>";
            	break;
            
            	case 'card':
                    switch ($date){ 
                    	case 'day':
                            //********* TOP Reset Score Day **************************
                            $date_now = date('d/m', $timestamp);
                            $day_now_begin = date('Y-m-d', $timestamp);
                            $day_now_end = date('Y-m-d', $timestamp + 24*60*60);
                            
                            $day_before = date('d/m', $timestamp - 24*60*60);
                            $day_before_begin = date('Y-m-d', $timestamp - 24*60*60);
                            $day_before_end = date('Y-m-d', $timestamp);
                            
                            for($i=1;$i<count($thehe_choise);$i++) {
                                if(strlen($thehe_choise[$i]) > 0) {
                                    // TOP Daily NOW
                                    $TopData_Arr[$date_now][$i] = array();
                                    $top_daily_query = "SELECT TOP $rank_slg acc, SUM(Event_TOP_Card.gcoin) as CardTotal, nbb_vip_time, nbb_vip_time_begin FROM Event_TOP_Card JOIN MEMB_INFO ON Event_TOP_Card.acc collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=$i AND [time] >= '$day_now_begin' GROUP BY acc, nbb_vip_time, nbb_vip_time_begin ORDER BY CardTotal DESC, acc";
                                    $top_daily_result = $db->Execute($top_daily_query);
                                        check_queryerror($top_daily_query, $top_daily_result);
                                    while($top_daily_fetch = $top_daily_result->FetchRow()) {
                                        if($top_daily_fetch[2] > $timestamp) {
                                            if($top_daily_fetch[3] > 0) {
                                                $nbb_vip_time_used = $timestamp - $top_daily_fetch[3];
                                                $nbb_vip_time_used_day = floor($nbb_vip_time_used/(24*60*60));
                                                $vip = 1;
                                                if(is_array($vip_level_day)) {
                                                    foreach($vip_level_day as $k => $v) {
                                                        if($nbb_vip_time_used_day >= $v) $vip = $k+1;
                                                        else break;
                                                    }
                                                }
                                            } else $vip = 1;
                                        } else $vip = 0;
                                        
                                        $vip_day = 0;
                                        if($vip > 1) $vip_day = $vip_level_day[$vip - 1];
                                        
                                        $nvchinh_query = "SELECT TOP 1 Name, class FROM Character WHERE AccountID='". $top_daily_fetch[0] ."' ORDER BY Relifes DESC, Resets DESC, cLevel DESC";
                                        $nvchinh_result = $db->Execute($nvchinh_query);
                                            check_queryerror($nvchinh_query, $nvchinh_result);
                                        $nvchinh_fetch = $nvchinh_result->FetchRow();
                                        
                                        $TopData_Arr[$date_now][$i][] = array(
                                            'name'  =>  $nvchinh_fetch[0],
                                            'class' =>  $nvchinh_fetch[1],
                                            'card'   =>  $top_daily_fetch[1],
                                            'vip' =>  $vip,
                                            'vip_day' =>  $vip_day,
                                            'thehe' =>  $i
                                        );
                                    }
                                    
                                    // TOP Daily YESTERDAY
                                    $TopData_Arr[$day_before][$i] = array();
                                    $top_daily_query = "SELECT TOP $rank_slg acc, SUM(Event_TOP_Card.gcoin) as CardTotal, nbb_vip_time, nbb_vip_time_begin FROM Event_TOP_Card JOIN MEMB_INFO ON Event_TOP_Card.acc collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=$i AND [time] >= '$day_before_begin' AND [time] < '$day_before_end' GROUP BY acc, nbb_vip_time, nbb_vip_time_begin ORDER BY CardTotal DESC, acc";
                                    $top_daily_result = $db->Execute($top_daily_query);
                                        check_queryerror($top_daily_query, $top_daily_result);
                                    while($top_daily_fetch = $top_daily_result->FetchRow()) {
                                        if($top_daily_fetch[2] > $timestamp) {
                                            if($top_daily_fetch[3] > 0) {
                                                $nbb_vip_time_used = $timestamp - $top_daily_fetch[3];
                                                $nbb_vip_time_used_day = floor($nbb_vip_time_used/(24*60*60));
                                                $vip = 1;
                                                if(is_array($vip_level_day)) {
                                                    foreach($vip_level_day as $k => $v) {
                                                        if($nbb_vip_time_used_day >= $v) $vip = $k+1;
                                                        else break;
                                                    }
                                                }
                                            } else $vip = 1;
                                        } else $vip = 0;
                                        
                                        $vip_day = 0;
                                        if($vip > 1) $vip_day = $vip_level_day[$vip - 1];
                                        
                                        $nvchinh_query = "SELECT TOP 1 Name, Class FROM Character WHERE AccountID='". $top_daily_fetch[0] ."' ORDER BY Relifes DESC, Resets DESC, cLevel DESC";
                                        $nvchinh_result = $db->Execute($nvchinh_query);
                                            check_queryerror($nvchinh_query, $nvchinh_result);
                                        $nvchinh_fetch = $nvchinh_result->FetchRow();
                                        
                                        $TopData_Arr[$day_before][$i][] = array(
                                            'name'  =>  $nvchinh_fetch[0],
                                            'class' =>  $nvchinh_fetch[1],
                                            'card'   =>  $top_daily_fetch[1],
                                            'vip' =>  $vip,
                                            'vip_day' =>  $vip_day,
                                            'thehe' =>  $i
                                        );
                                    }
                                }
                            }
                    	break;
                    
                    	case 'week':
                            //********* TOP Reset Score WEEK **************************
                            $week_now = date('W', $timestamp);
                            $week_now_begin = (date('N', $timestamp) == 1) ? date('Y-m-d', $timestamp) : date('Y-m-d', strtotime('last Monday', $timestamp));
                            
                            $week_before_time = $timestamp - 7*24*60*60;
                            $week_before = date('W', $week_before_time);
                            $week_before_begin = (date('N', $week_before_time) == 1) ? date('Y-m-d', $week_before_time) : date('Y-m-d', strtotime('last Monday', $week_before_time));
                            $week_before_end = $week_now_begin;
                                                        
                            for($i=1;$i<count($thehe_choise);$i++) {
                                if(strlen($thehe_choise[$i]) > 0) {
                                    // TOP WEEK NOW
                                    $TopData_Arr[$week_now][$i] = array();
                                    $top_week_query = "SELECT TOP $rank_slg acc, SUM(Event_TOP_Card.gcoin) as CardTotal, nbb_vip_time, nbb_vip_time_begin FROM Event_TOP_Card JOIN MEMB_INFO ON Event_TOP_Card.acc collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=$i AND [time] >= '$week_now_begin' GROUP BY acc, nbb_vip_time, nbb_vip_time_begin ORDER BY CardTotal DESC, acc";
                                    $top_week_result = $db->Execute($top_week_query);
                                        check_queryerror($top_week_query, $top_week_result);
                                    while($top_week_fetch = $top_week_result->FetchRow()) {
                                        if($top_week_fetch[2] > $timestamp) {
                                            if($top_week_fetch[3] > 0) {
                                                $nbb_vip_time_used = $timestamp - $top_week_fetch[3];
                                                $nbb_vip_time_used_day = floor($nbb_vip_time_used/(24*60*60));
                                                $vip = 1;
                                                if(is_array($vip_level_day)) {
                                                    foreach($vip_level_day as $k => $v) {
                                                        if($nbb_vip_time_used_day >= $v) $vip = $k+1;
                                                        else break;
                                                    }
                                                }
                                            } else $vip = 1;
                                        } else $vip = 0;
                                        
                                        $vip_day = 0;
                                        if($vip > 1) $vip_day = $vip_level_day[$vip - 1];
                                        
                                        $nvchinh_query = "SELECT TOP 1 Name, class FROM Character WHERE AccountID='". $top_week_fetch[0] ."' ORDER BY Relifes DESC, Resets DESC, cLevel DESC";
                                        $nvchinh_result = $db->Execute($nvchinh_query);
                                            check_queryerror($nvchinh_query, $nvchinh_result);
                                        $nvchinh_fetch = $nvchinh_result->FetchRow();
                                        
                                        $TopData_Arr[$week_now][$i][] = array(
                                            'name'  =>  $nvchinh_fetch[0],
                                            'class' =>  $nvchinh_fetch[1],
                                            'card'   =>  $top_week_fetch[1],
                                            'vip' =>  $vip,
                                            'vip_day' =>  $vip_day,
                                            'thehe' =>  $i
                                        );
                                    }
                                    
                                    // TOP Week Before
                                    $TopData_Arr[$week_before][$i] = array();
                                    $top_week_query = "SELECT TOP $rank_slg acc, SUM(Event_TOP_Card.gcoin) as CardTotal, nbb_vip_time, nbb_vip_time_begin FROM Event_TOP_Card JOIN MEMB_INFO ON Event_TOP_Card.acc collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=$i AND [time] >= '$week_before_begin' AND [time] < '$week_before_end' GROUP BY acc, nbb_vip_time, nbb_vip_time_begin ORDER BY CardTotal DESC, acc";
                                    $top_week_result = $db->Execute($top_week_query);
                                        check_queryerror($top_week_query, $top_week_result);
                                    while($top_week_fetch = $top_week_result->FetchRow()) {
                                        if($top_week_fetch[2] > $timestamp) {
                                            if($top_week_fetch[3] > 0) {
                                                $nbb_vip_time_used = $timestamp - $top_week_fetch[3];
                                                $nbb_vip_time_used_day = floor($nbb_vip_time_used/(24*60*60));
                                                $vip = 1;
                                                if(is_array($vip_level_day)) {
                                                    foreach($vip_level_day as $k => $v) {
                                                        if($nbb_vip_time_used_day >= $v) $vip = $k+1;
                                                        else break;
                                                    }
                                                }
                                            } else $vip = 1;
                                        } else $vip = 0;
                                        
                                        $vip_day = 0;
                                        if($vip > 1) $vip_day = $vip_level_day[$vip - 1];
                                        
                                        $nvchinh_query = "SELECT TOP 1 Name, class FROM Character WHERE AccountID='". $top_week_fetch[0] ."' ORDER BY Relifes DESC, Resets DESC, cLevel DESC";
                                        $nvchinh_result = $db->Execute($nvchinh_query);
                                            check_queryerror($nvchinh_query, $nvchinh_result);
                                        $nvchinh_fetch = $nvchinh_result->FetchRow();
                                        
                                        $TopData_Arr[$week_before][$i][] = array(
                                            'name'  =>  $nvchinh_fetch[0],
                                            'class' =>  $nvchinh_fetch[1],
                                            'card'   =>  $top_week_fetch[1],
                                            'vip' =>  $vip,
                                            'vip_day' =>  $vip_day,
                                            'thehe' =>  $i
                                        );
                                    }
                                }
                            }
                    	break;
                    
                    	case 'month':
                            //********* TOP Reset Score MONTH **************************
                            $month_now_time = (date('d', $timestamp) == 1) ? strtotime(date('Y-m-d', $timestamp)) : strtotime(date('Y-m-01'));
                            $month_now = date('m', $month_now_time);
                            $month_now_begin = date('Y-m-d', $month_now_time);
                            
                            $month_before_time = strtotime('-1 month', $month_now_time);
                            $month_before = date('m', $month_before_time);
                            $month_before_begin = date('Y-m-d', $month_before_time);
                            $month_before_end = $month_now_begin;
                            
                
                            for($i=1;$i<count($thehe_choise);$i++) {
                                if(strlen($thehe_choise[$i]) > 0) {
                                    // TOP MONTH NOW
                                    $TopData_Arr[$month_now][$i] = array();
                                    $top_month_query = "SELECT TOP $rank_slg acc, SUM(Event_TOP_Card.gcoin) as CardTotal, nbb_vip_time, nbb_vip_time_begin FROM Event_TOP_Card JOIN MEMB_INFO ON Event_TOP_Card.acc collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=$i AND [time] >= '$month_now_begin' GROUP BY acc, nbb_vip_time, nbb_vip_time_begin ORDER BY CardTotal DESC, acc";
                                    $top_month_result = $db->Execute($top_month_query);
                                        check_queryerror($top_month_query, $top_month_result);
                                    while($top_month_fetch = $top_month_result->FetchRow()) {
                                        if($top_month_fetch[2] > $timestamp) {
                                            if($top_month_fetch[3] > 0) {
                                                $nbb_vip_time_used = $timestamp - $top_month_fetch[3];
                                                $nbb_vip_time_used_day = floor($nbb_vip_time_used/(24*60*60));
                                                $vip = 1;
                                                if(is_array($vip_level_day)) {
                                                    foreach($vip_level_day as $k => $v) {
                                                        if($nbb_vip_time_used_day >= $v) $vip = $k+1;
                                                        else break;
                                                    }
                                                }
                                            } else $vip = 1;
                                        } else $vip = 0;
                                        
                                        $vip_day = 0;
                                        if($vip > 1) $vip_day = $vip_level_day[$vip - 1];
                                        
                                        $nvchinh_query = "SELECT TOP 1 Name, class FROM Character WHERE AccountID='". $top_month_fetch[0] ."' ORDER BY Relifes DESC, Resets DESC, cLevel DESC";
                                        $nvchinh_result = $db->Execute($nvchinh_query);
                                            check_queryerror($nvchinh_query, $nvchinh_result);
                                        $nvchinh_fetch = $nvchinh_result->FetchRow();
                                        
                                        $TopData_Arr[$month_now][$i][] = array(
                                            'name'  =>  $nvchinh_fetch[0],
                                            'class' =>  $nvchinh_fetch[1],
                                            'card'   =>  $top_month_fetch[1],
                                            'vip' =>  $vip,
                                            'vip_day' =>  $vip_day,
                                            'thehe' =>  $i
                                        );
                                    }
                                    
                                    // TOP Month Before
                                    $TopData_Arr[$month_before][$i] = array();
                                    $top_month_query = "SELECT TOP $rank_slg acc, SUM(Event_TOP_Card.gcoin) as CardTotal, nbb_vip_time, nbb_vip_time_begin FROM Event_TOP_Card JOIN MEMB_INFO ON Event_TOP_Card.acc collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=$i AND [time] >= '$month_before_begin' AND [time] < '$month_before_end' GROUP BY acc, nbb_vip_time, nbb_vip_time_begin ORDER BY CardTotal DESC, acc";
                                    $top_month_result = $db->Execute($top_month_query);
                                        check_queryerror($top_month_query, $top_month_result);
                                    while($top_month_fetch = $top_month_result->FetchRow()) {
                                        if($top_month_fetch[2] > $timestamp) {
                                            if($top_month_fetch[3] > 0) {
                                                $nbb_vip_time_used = $timestamp - $top_month_fetch[3];
                                                $nbb_vip_time_used_day = floor($nbb_vip_time_used/(24*60*60));
                                                $vip = 1;
                                                if(is_array($vip_level_day)) {
                                                    foreach($vip_level_day as $k => $v) {
                                                        if($nbb_vip_time_used_day >= $v) $vip = $k+1;
                                                        else break;
                                                    }
                                                }
                                            } else $vip = 1;
                                        } else $vip = 0;
                                        
                                        $vip_day = 0;
                                        if($vip > 1) $vip_day = $vip_level_day[$vip - 1];
                                        
                                        $nvchinh_query = "SELECT TOP 1 Name, class FROM Character WHERE AccountID='". $top_month_fetch[0] ."' ORDER BY Relifes DESC, Resets DESC, cLevel DESC";
                                        $nvchinh_result = $db->Execute($nvchinh_query);
                                            check_queryerror($nvchinh_query, $nvchinh_result);
                                        $nvchinh_fetch = $nvchinh_result->FetchRow();
                                        
                                        $TopData_Arr[$month_before][$i][] = array(
                                            'name'  =>  $nvchinh_fetch[0],
                                            'class' =>  $nvchinh_fetch[1],
                                            'card'   =>  $top_month_fetch[1],
                                            'vip' =>  $vip,
                                            'vip_day' =>  $vip_day,
                                            'thehe' =>  $i
                                        );
                                    }
                                }    
                            }
                    	break;
                    }
                    
                    $TopData = json_encode($TopData_Arr);
                    echo "<nbb>OK<nbb>" . $TopData . "<nbb>";
            	break;
            }
    }
}
$db->Close();
?>