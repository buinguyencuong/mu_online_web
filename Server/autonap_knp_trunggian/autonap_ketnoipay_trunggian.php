<?php
/**abc*/

include('autonap_ketnoipay_api.php');
$domain_ip_allow = array('svml.myvnc.com', 'svdn.myvnc.com');

function _checkip($domain) {
    $ipdomain = _domain2ip($domain);
    $fetchip = fetch_ip();
    if($ipdomain == $fetchip) return true;
    else return false;
}

function _domain2ip($domain) {
    $ip = gethostbyname($domain);
    return $ip;
}

function fetch_ip()
{
	# Enable X_FORWARDED_FOR IP matching?
	$do_check = 1;
	$addrs = array();

	if( $do_check )
	{
		foreach( array_reverse(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR'])) as $x_f )
		{
			$x_f = trim($x_f);
			if( preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/', $x_f) )
			{
				$addrs[] = $x_f;
			}
		}

		$addrs[] = $_SERVER['HTTP_CLIENT_IP'];
		$addrs[] = $_SERVER['HTTP_PROXY_USER'];
	}

	$addrs[] = $_SERVER['REMOTE_ADDR'];

	foreach( $addrs as $v )
	{
		if( $v )
		{
			preg_match("/^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$/", $v, $match);
			$ip = $match[1].'.'.$match[2].'.'.$match[3].'.'.$match[4];

			if( $ip && $ip != '...' )
			{
				break;
			}
		}
	}

	if( ! $ip || $ip == '...' )
	{
		print_error("Không thể xác định địa chỉ IP của bạn.");
	}

	return $ip;
}

$ketnoipay_partnerid = $_POST['ketnoipay_partnerid'];
$TxtType = $_POST['TxtType'];
$card_num = $_POST['card_num'];
$card_serial = $_POST['card_serial'];
$TxtTransId = $_POST['TxtTransId'];
$TxtKey = $_POST['TxtKey'];
$TxtUrl = $_POST['TxtUrl'];

if(is_array($domain_ip_allow)) {
    foreach($domain_ip_allow as $domain) {
        $check_ip = _checkip($domain);
        if($check_ip === true) break;
    }
}

if($check_ip === true) {
    $gateWay  = new gateWay($ketnoipay_partnerid,$TxtType,$card_num,$card_serial,$TxtTransId,$TxtKey,$TxtUrl);
    $response = $gateWay->ReturnResult();
    
    echo $response;
} else echo "IP Check Error.";
?>