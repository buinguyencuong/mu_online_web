<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
    include_once('config/config_event.php');
    include_once('config/config_ipbonus.php');

$timestamp = _time();
$time_now = $timestamp;
$day_now = date("d",$time_now);
$month_now = date("m",$time_now);
$year_now = date("Y",$time_now);

    
// Neu the dung
if($up_stat == 2) {
    if ($edit_menhgia != $menhgia) {
    	$query_editmenhgia = "Update CardPhone Set menhgia=$edit_menhgia Where stt=$stt";
    	$editmenhgia =$db->Execute($query_editmenhgia);
    	$menhgia = $edit_menhgia;
    }
    
    $query_upstat = "Update CardPhone Set status=2 Where stt=$stt";
    $upstat =$db->Execute($query_upstat);
        $gcoinadd = 0;
        if ($menhgia == 10000) { $gcoinadd = $menhgia10000; }
    	if ($menhgia == 20000) { $gcoinadd = $menhgia20000; }
    	if ($menhgia == 30000) { $gcoinadd = $menhgia30000; }
    	if ($menhgia == 50000) { $gcoinadd = $menhgia50000; }
    	if ($menhgia == 100000) { $gcoinadd = $menhgia100000; }
    	if ($menhgia == 200000) { $gcoinadd = $menhgia200000; }
    	if ($menhgia == 300000) { $gcoinadd = $menhgia300000; }
    	if ($menhgia == 500000) { $gcoinadd = $menhgia500000; }
        if ($menhgia == 1000000) { $gcoinadd = $menhgia1000000; }
    	
        
    	//Gcoin khi nạp thẻ VTC nhiều hơn các thẻ khác
    	if ($cardtype == 'VTC' && $khuyenmai_vtc > 0) {
    	   $gcoinadd = floor($gcoinadd*(1+($khuyenmai_vtc/100)));
    	}
        //Gcoin khi nạp thẻ GATE nhiều hơn các thẻ khác
    	if ($cardtype == 'GATE' && $khuyenmai_gate > 0) {
    	   $gcoinadd = floor($gcoinadd*(1+($khuyenmai_gate/100)));
    	}
        
        $gcoin_km = 0;
    	//Khuyen mai chung
    	if (isset($khuyenmai_begin) && isset($khuyenmai_end) && strtotime($khuyenmai_begin) <= $timestamp && (strtotime($khuyenmai_end) + 24*60*60 - 1) >= $timestamp && $khuyenmai_phantram > 0) {
    		$gcoin_km = floor($gcoinadd*($khuyenmai_phantram/100));
    	} else {
            $day_of_week = date('w', $timestamp);
            $khuyenmai_phantram = $khuyenmai_week_phantram[$day_of_week];
            if($khuyenmai_phantram > 0) {
                $gcoin_km = floor($gcoinadd*($khuyenmai_phantram/100));
            }
    	}
        
        $gcoin_km_ipbonus = 0;
        $ipbonus_active = 0;
        if(isset($use_ipbonus) && $use_ipbonus == 1) {
            $ipbonus_play_query = "SELECT count(memb___id) FROM MEMB_INFO A JOIN IPBonus B ON A.ip=B.ip AND memb___id='$login'";
            $ipbonus_play_result = $db->Execute($ipbonus_play_query);
                check_queryerror($ipbonus_play_query, $ipbonus_play_result);
            $ipbonus_check = $ipbonus_play_result->FetchRow();
            if($ipbonus_check[0] > 0) $ipbonus_active = 1;
        }
        if($ipbonus_active == 1) {
            $gcoin_km_ipbonus = floor($gcoinadd*($ipbonus_kmcard/100));
        }
        $gcoin_km_after = $gcoin_km + $gcoin_km_ipbonus;
            
    	//Begin Kiểm tra có tồn tại doanh thu của loại thẻ nạp
    	$check_tontai_doanhthu_cardtype = $db->Execute("SELECT month FROM doanhthu WHERE month='$month' and year='$year' AND card_type='$cardtype'");
    	$tontai_doanhthu_cardtype = $check_tontai_doanhthu_cardtype->numrows();
    	if ($tontai_doanhthu_cardtype == 0) {
    		$update_doanhthu_cardtype = $db->Execute("INSERT INTO doanhthu (month, year,card_type) VALUES ($month, $year,'$cardtype')");
    	}
    	//End Kiểm tra có tồn tại doanh thu của loại thẻ nạp
        // Update doanh thu
    	$query_updatedoanhthu = "Update doanhthu set money=money+$menhgia Where month='$month' And year='$year' AND card_type='$cardtype'";
    	$updatedoanhthu =$db->Execute($query_updatedoanhthu);
        // End Update doanh thu
        
        // Lay Gcoin cua tai khoan hien co
    		$gcoin_truoc_query = "SELECT gcoin FROM MEMB_INFO WHERE memb___id='$login'";
    		$gcoin_truoc_result = $db->Execute($gcoin_truoc_query);
    		$gcoin_truoc = $gcoin_truoc_result->fetchrow();
    	// End Lay Gcoin cua tai khoan hien co	
        // Cong Gcoin
    		$query_addgcoin = "Update MEMB_INFO set gcoin=gcoin+$gcoinadd,gcoin_km=gcoin_km+$gcoin_km_after Where memb___id='$login'";
    		$addgcoin =$db->Execute($query_addgcoin);
       // End Cong Gcoin
       
       // Update Card
    		$query_statgcoin = "Update CardPhone set addvpoint=1,timeduyet=$timestamp Where stt=$stt";
    		$statgcoin =$db->Execute($query_statgcoin);
    	// End Update Card
        
    		//Ghi vào Log
    	        $log_price = "+ $gcoinadd Gcent";
                if($gcoin_km > 0) $log_price .= ", $gcoin_km Gcent+ KM";
                if($gcoin_km_ipbonus > 0) $log_price .= ", $gcoin_km_ipbonus Gcent+ IP Bonus";
    	        $log_Des = "Nạp thẻ $cardtype : $menhgia VNĐ";
    	        _writelog_tiente($login, $log_price, $log_Des, 5);
    		//End Ghi vào Log
    		//Begin Invite
    			//Kiem tra co tai khoan gioi thieu khong
    			$invite_check_query = "SELECT acc_invite FROM Invite WHERE acc_accept='$login'";
    			$invite_check_result = $db->Execute($invite_check_query);
    			$invite_have = $invite_check_result->numrows();
    			if($invite_have > 0)
    			{
    				$vpoint_invite = floor($gcoinadd*5/100);
    				$acc_invite = $invite_check_result->fetchrow();
    				$update_vpoint_meminvite_query = "UPDATE MEMB_INFO SET vpoint=vpoint+$vpoint_invite WHERE memb___id='$acc_invite[0]'";
    				$update_vpoin_memtinvite_result = $db->Execute($update_vpoint_meminvite_query);
    				
    				$update_vpoint_invite_query = "UPDATE Invite SET vpoint_invite=vpoint_invite+$vpoint_invite WHERE acc_accept='$login' AND acc_invite='$acc_invite[0]'";
    				$update_vpoint_invite_result = $db->Execute($update_vpoint_invite_query);
    			}
    		//End Invite
    	//Event TOP Reset in Time
        $acc = $login;
    	include_once('event_topcard_intime.php');
		include_once('event_nt_giftcode.php');
        
        $notice_nap = "Thẻ đúng. Mệnh giá thẻ : $edit_menhgia VNĐ<br />Nhận : $gcoinadd Gcent";
        if($gcoin_km > 0) $notice_nap .= " + $gcoin_km Gcent+ ($khuyenmai_phantram% Khuyến mại)";
        if($gcoin_km_ipbonus > 0) $notice_nap .= " + $gcoin_km_ipbonus Gcent+ ($ipbonus_kmcard% IP Bonus)";
}
// Neu the sai
elseif($up_stat == 3) {
    $card_sai_query = "Update CardPhone set timeduyet=$timestamp, status=3 Where stt=$stt";
    $card_sai_result =$db->Execute($card_sai_query) OR DIE("Query Error : $card_sai_query");
    
    $notice_nap = "Thẻ Sai. Vui lòng kiểm tra lại thông tin thẻ nạp.";
    
    $datasend = "";
    if(isset($postdata)) {
        $datasend = urldecode(http_build_query($postdata, '', '&'));
        $log_napthe = "Thẻ sai. Mã thẻ: $card_num_encode, Seri: $card_serial_encode. Thông tin gửi đi: $datasend. Thông tin nhận : $notice_nap";
    } else {
        $log_napthe = "Thẻ sai. Thẻ thứ: $stt. Tài khoản: $login nạp thẻ sai với Serial $card_serial.";
    }
    _writelog("log_napthe.txt", $log_napthe);
}
// Neu he thong nap the bao tri
elseif($up_stat == 9) {
    $card_del_q = "Update CardPhone set timeduyet=$timestamp, status=9 Where stt=$stt";
    $card_del_r =$db->Execute($card_del_q) OR DIE("Query Error : $card_del_q");
    
    $datasend = "";
    if(isset($postdata)) $datasend = urldecode(http_build_query($postdata, '', '&'));
    $log_napthe = "Hệ thống duyệt thẻ Bảo trì. Mã thẻ: $card_num_encode, Seri: $card_serial_encode. Thông tin gửi đi: $datasend. Thông tin nhận : $response";
    _writelog("log_napthe.txt", $log_napthe);
}

?>