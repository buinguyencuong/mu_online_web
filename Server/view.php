<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
	include_once("security.php");
include_once ('config.php');
include_once ('function.php');

$action = $_POST['action'];
$login = $_POST['login'];
$passtransfer = $_POST['passtransfer'];

if ($passtransfer == $transfercode) {

switch ($action)
{
	
    case 'view_infoacc':
        include_once('config/config_event.php');
        include_once('config/config_event1.php');
        include_once('config/config_gioihanrs.php');
        
        $name = $_POST['name'];
        
        $query_getacc = "SELECT gcoin, vpoint, bank, jewel_chao, jewel_cre, jewel_blue, tel__numb, passran, gcoin_km, thehe, IPBonusPoint, jewel_heart, WCoin, WCoinP, GoblinCoin, vpoint_km, nbb_pl, nbb_pl_extra FROM MEMB_INFO WHERE memb___id='$login'";
		$result_getacc = $db->Execute($query_getacc);
		$getacc = $result_getacc->fetchrow();
        
    		$acc_gcoin = $getacc[0];
            $acc_gcoin_km = $getacc[8];
    		$acc_vpoint = $getacc[1];
            $acc_vpoint_km = $getacc[15];
    		$acc_zen = $getacc[2];
    		$acc_chao = $getacc[3];
    		$acc_cre = $getacc[4];
    		$acc_blue = $getacc[5];
            $acc_IPBonusPoint = $getacc[10];
            $acc_heart = $getacc[11];
            $acc_WCoin = $getacc[12];
            $acc_WCoinP = $getacc[13];
            $acc_GoblinCoin = $getacc[14];
            $acc_ppoint = $getacc[16];
            $acc_ppoint_extra = $getacc[17];
            $thehe = $getacc[9];

		$query_nv = "select cLevel, LevelUpPoint, pointdutru, Money, Resets, Relifes, khoado, UyThac, PointUyThac, uythacoffline_stat, uythacoffline_time, Top50, UyThacOffline_Daily, point_event, PointUyThac_Event FROM Character WHERE AccountID='$login' AND Name='$name'";
		$result_nv = $db->Execute( $query_nv );
		$nv= $result_nv->fetchrow();
			$nv_Level = $nv[0];
			$nv_point = $nv[1];
			$nv_point_dutru = $nv[2];
			$nv_zen = $nv[3];
			$nv_reset = $nv[4];  if($nv_reset < 0) $nv_reset = 0;
			$nv_relife = $nv[5];
			$nv_khoado = $nv[6];
			$nv_uythacon = $nv[7];
			$nv_pointuythac = $nv[8];
            $nv_pointuythac_event = $nv[14];
			$nv_uythac_offline = $nv[9];
            $nv_uythac_offline_daily = $nv[12];
			if ($nv[10]>0) $nv_uythac_offline_time = floor(($timestamp-$nv[10])/60);
			else $nv_uythac_offline_time=0;
            $nv_top50 = $nv[11];
            $nv_point_event = $nv[13];
            $nv_event1_type1 = event1_type1_slg($name);
            $nv_event1_type2 = event1_type2_slg($name);
            $nv_event1_type3 = event1_type3_slg($name);
            $nv_event1_type1_daily = event1_type1_daily_slg($name);
            $nv_event1_type2_daily = event1_type2_daily_slg($name);
            $nv_event1_type3_daily = event1_type3_daily_slg($name);
            $nv_resetday = _get_reset_day($name,$timestamp);
            $nv_resetmonth = _get_reset_month($name);
            
		$sql_doinv_check = $db->Execute("SELECT * FROM AccountCharacter WHERE Id='$login' AND GameIDC='$name'");
		$doinv_check = $sql_doinv_check->numrows();
			if ($doinv_check > 0) { $doinv = '0'; } else { $doinv = '1'; }
		
		$sql_online_check = $db->Execute("SELECT * FROM MEMB_STAT WHERE memb___id='$login' AND ConnectStat='1'");
		$online_check = $sql_online_check->numrows();
			if ($online_check > 0) { $online = '1'; } else { $online = '0'; }
        
        $quest_daily_arr = _quest_daily($login, $name);
        $quest_wait = $quest_daily_arr['quest_wait'];
        
         
        $data_arr = array(
            'acc_gcoin' =>  $acc_gcoin,
            'acc_gcoin_km'  =>  $acc_gcoin_km,
            'acc_vpoint'    =>  $acc_vpoint,
            'acc_vpoint_km'    =>  $acc_vpoint_km,
            'acc_zen'   =>  $acc_zen,
            'acc_heart'  =>  $acc_heart,
            'acc_WCoin'  =>  $acc_WCoin,
            'acc_WCoinP'  =>  $acc_WCoinP,
            'acc_GoblinCoin'  =>  $acc_GoblinCoin,
            'acc_chao'  =>  $acc_chao,
            'acc_cre'   =>  $acc_cre,
            'acc_blue'  =>  $acc_blue,
            'acc_ppoint'  =>  $acc_ppoint,
            'acc_ppoint_extra'  =>  $acc_ppoint_extra,
            'acc_ipbonuspoint'  =>  $acc_IPBonusPoint,
                        
            'nv_level'  =>  $nv_Level,
            'nv_point'  =>  $nv_point,
            'nv_pointdutru' =>  $nv_point_dutru,
            'nv_zen'    =>  $nv_zen,
            'nv_reset'  =>  $nv_reset,
            'nv_resetday'   =>  $nv_resetday,
            'nv_resetmonth' =>  $nv_resetmonth,
            'nv_relife' =>  $nv_relife,
            'nv_khoado' =>  $nv_khoado,
            'nv_uythacon'   =>  $nv_uythacon,
            'nv_pointuythac'    =>  $nv_pointuythac,
            'nv_pointuythac_event'    =>  $nv_pointuythac_event,
            'nv_uythac_offline' =>  $nv_uythac_offline,
            'nv_uythac_offline_daily'   =>  $nv_uythac_offline_daily,
            'nv_uythac_offline_time'    =>  $nv_uythac_offline_time,
            'nv_top50'  =>  $nv_top50,
            'nv_point_event'    =>  $nv_point_event,
            'nv_event1_type1'   =>  $nv_event1_type1,
            'nv_event1_type2'   =>  $nv_event1_type2,
            'nv_event1_type3'   =>  $nv_event1_type3,
            'nv_event1_type1_daily'   =>  $nv_event1_type1_daily,
            'nv_event1_type2_daily'   =>  $nv_event1_type2_daily,
            'nv_event1_type3_daily'   =>  $nv_event1_type3_daily,
            'qwait'   =>  $quest_wait,
            'doinv' =>  $doinv,
            'online'    =>  $online
        );
            $data = serialize($data_arr);
            echo "$data";
    break;
    
    case 'login':
        include('config/config_thehe.php');
		$file_edit = 'config/config_title.txt';
		$fopen_host = fopen($file_edit, "r");
		$title_data = fgets($fopen_host);
		fclose($fopen_host);
		$title_arr = json_decode($title_data, true);
		if(!is_array($title_arr)) $title_arr = array();
		
		$pass = $_POST['pass'];
		$ip = $_POST['ip'];
		
		if ($type_acc == 1) {
			kiemtra_kituso($login);
		} else {
			kiemtra_kitudacbiet($login);
		}
		kiemtra_kitudacbiet($pass);
	
		kiemtra_acc($login);
		kiemtra_pass($login,$pass);
		kiemtra_block_acc($login);
		
		$query_getacc = "SELECT gcoin, vpoint, bank, jewel_chao, jewel_cre, jewel_blue, tel__numb, passran, gcoin_km, thehe, IPBonusPoint, jewel_heart, WCoin, WCoinP, GoblinCoin, mail_addr, vpoint_km, nbb_pl, nbb_pl_extra, appl_days, nbb_warehouse_secure_slot, nbb_fb_fanpage_like, nbb_vip_time, nbb_vip_time_begin, acc_vip, acc_vip_day, acc_vip_time FROM MEMB_INFO WHERE memb___id='$login'";
		$result_getacc = $db->Execute($query_getacc) OR DIE("Lỗi Query : $query_getacc");
		$getacc = $result_getacc->fetchrow();
        
    		$gcoin = $getacc[0];
            $gcoin_km = $getacc[8];
    		$vpoint = $getacc[1];
            $vpoint_km = $getacc[16];
    		$zen = $getacc[2];
    		$chao = $getacc[3];
    		$cre = $getacc[4];
    		$blue = $getacc[5];
    		$phone = substr($getacc[6],-3);
    		$passran = $getacc[7];
            $thehe = $getacc[9];
            $IPBonusPoint = $getacc[10];
            $heart = $getacc[11];
            $WCoin = $getacc[12];
            $WCoinP = $getacc[13];
            $GoblinCoin = $getacc[14];
            $acc_ppoint = $getacc[17];
            $acc_ppoint_extra = $getacc[18];
            $reg_day = $getacc[19];
            $nbb_warehouse_secure_slot = $getacc[20];
            $nbb_fb_fanpage_like = $getacc[21];
            $nbb_vip_time = $getacc[22];
                if($nbb_vip_time > $timestamp) $nbb_vip = 1;
                else $nbb_vip = 0;
            $nbb_vip_time_begin = $getacc[23];
                if($nbb_vip = 1) $nbb_vip_time_used = $timestamp - $nbb_vip_time_begin;
                else $nbb_vip_time_used = 0;
			$acc_vip = $getacc[24];
			$acc_vip_day = $getacc[25];	
			$acc_vip_time = $getacc[26];
				if($acc_vip_time > $timestamp) {
					$acc_vip = $acc_vip;
					$acc_vip_day = $acc_vip_day;
				}	
                else {
					$acc_vip = 0;
					$acc_vip_day = 0;
				} 
            $email = $getacc[15];
            $email_explode = explode('@', $email);
            $email_view = substr($email_explode[0], 0, 3) ."******". substr($email_explode[0], -3) ."@". $email_explode[1];
            
        
        if(strlen($thehe_choise[$thehe]) == 0) {
            echo "Tài khoản không thuộc Server này.";
            exit();
        } else {
            $nbb_warehouse_secure_item = 0;
            if($nbb_warehouse_secure_slot > 0) {
                $nbb_warehouse_secure_item_q = "SELECT count(*) FROM nbb_warehouse_secure WHERE acc='$login' AND status=0";
                $nbb_warehouse_secure_item_r = $db->Execute($nbb_warehouse_secure_item_q);
                    check_queryerror($nbb_warehouse_secure_item_q, $nbb_warehouse_secure_item_r);
                $nbb_warehouse_secure_item_f = $nbb_warehouse_secure_item_r->FetchRow();
                $nbb_warehouse_secure_item = $nbb_warehouse_secure_item_f[0];
            }
            
            $card_total_q = "SELECT SUM(menhgia) FROM CardPhone WHERE acc='$login' AND status=2";
            $card_total_r = $db->Execute($card_total_q);
                check_queryerror($card_total_q, $card_total_r);
            $card_total_f = $card_total_r->FetchRow();
            $card_total = abs(intval($card_total_f[0]));
            
            
            $query_getnv = "SELECT GameID1,GameID2,GameID3,GameID4,GameID5 FROM AccountCharacter Where Id='$login'";
    		$result_getnv = $db->Execute($query_getnv) OR DIE("Lỗi Query : $query_getnv");
    		$getnv = $result_getnv->fetchrow();
    		
        		$char1 = $getnv[0];
        		$char2 = $getnv[1];
        		$char3 = $getnv[2];
        		$char4 = $getnv[3];
        		$char5 = $getnv[4];
    		
    				$numran = rand(10000,99999);
    				$time_now = $timestamp;
    				$string_login = $numran.$time_now;
    				$string_login = md5($string_login);
    			$query_stringlogin = "Update MEMB_INFO SET checklogin='$string_login',ip='$ip' WHERE memb___id='$login'";
    			$result_stringlogin = $db->Execute($query_stringlogin);
    			if($result_stringlogin === false) DIE("QUERY ERROR: $query_stringlogin");
                
                $ipmasternet_query = "SELECT ip FROM IPBonus WHERE acc='$login'";
                $ipmasternet_result = $db->Execute($ipmasternet_query);
                    check_queryerror($ipmasternet_query, $ipmasternet_result);
                $masternet_exits = $ipmasternet_result->NumRows();
                if($masternet_exits > 0) {
                    $ipmasternet_fetch = $ipmasternet_result->FetchRow();
                    $ipmasternet = $ipmasternet_fetch[0];
                    if($ipmasternet != $ip) {
                        $ipmasternet_update_query = "UPDATE IPBonus SET ip='$ip' WHERE acc='$login'";
                        $ipmasternet_update_result = $db->Execute($ipmasternet_update_query);
                            check_queryerror($ipmasternet_update_query, $ipmasternet_update_result);
                    }
                    $ipbonus_active = 1;
                    $ipbonus_info = "Bạn là chủ Quán NET đăng ký IP Bonus. IP tại quán NET của bạn đã được cập nhập. Cảm ơn đã tham gia chương trình IP Bonus.";
                } else {
                    $ipbonus_play_query = "SELECT TOP 1 InternetName, Address, QuanHuyen, ThanhPho, ipbonus_id FROM IPBonus WHERE ip='$ip'";
                    $ipbonus_play_result = $db->Execute($ipbonus_play_query);
                        check_queryerror($ipbonus_play_query, $ipbonus_play_result);
                    $ipbonus_check = $ipbonus_play_result->NumRows();
                    
                    if($ipbonus_check == 0) {
                    	$ipbonus_active = 0;
                        $ipbonus_info = 'Bạn hiện không chơi ở Quán NET đăng ký IP Bonus.<br />Hãy chơi ở quán NET đăng ký IP Bonus để nhận được nhiều ưu đãi trong quá trình chơi.';
                    } else {
                        $ipbonus_play_fetch = $ipbonus_play_result->FetchRow();
                        $ipbonus_active = 1;
                        $ipbonus_info = "Bạn đang chơi tại quán NET <strong>$ipbonus_play_fetch[0]</strong> đã <strong>đăng ký IP Bonus</strong>.<br /> <strong>Địa chỉ</strong> $ipbonus_play_fetch[1], $ipbonus_play_fetch[2], $ipbonus_play_fetch[3].";
                        $accinnet_query = "SELECT ipbonus_id FROM IPBonus_acc WHERE acc='$login'";
                        $accinnet_result = $db->Execute($accinnet_query);
                            check_queryerror($accinnet_query, $accinnet_result);
                        $accinnet_check = $accinnet_result->NumRows();
                        if($accinnet_check == 0) {
                            $insert_accinnet_query = "INSERT INTO IPBonus_acc (ipbonus_id, acc, ipbonus_last_login) VALUES ($ipbonus_play_fetch[4], '$login', $timestamp)";
                            $insert_accinnet_result = $db->Execute($insert_accinnet_query);
                                check_queryerror($insert_accinnet_query, $insert_accinnet_result);
                            
                            $incr_totalacc_query = "UPDATE IPBonus SET totalacc=totalacc+1 WHERE ipbonus_id=$ipbonus_play_fetch[4]";
                            $incre_totalacc_result = $db->Execute($incr_totalacc_query);
                                check_queryerror($incr_totalacc_query, $incre_totalacc_result);
                        } else {
                            $accinnet_fetch = $accinnet_result->FetchRow();
                            if($accinnet_fetch[0] != $ipbonus_play_fetch[4]) {
                                $update_accinnet_q = "UPDATE IPBonus_acc SET ipbonus_id=$ipbonus_play_fetch[4], ipbonus_last_login=$timestamp WHERE acc='$login'";
                            	$update_accinnet_r = $db->Execute($update_accinnet_q);
                            		check_queryerror($update_accinnet_q, $update_accinnet_r);
                                
                                $decr_totalacc_query = "UPDATE IPBonus SET totalacc=totalacc-1 WHERE ipbonus_id=$accinnet_fetch[0]";
                                $decre_totalacc_result = $db->Execute($decr_totalacc_query);
                                    check_queryerror($decr_totalacc_query, $decre_totalacc_result);
                                
                                $incr_totalacc_query = "UPDATE IPBonus SET totalacc=totalacc+1 WHERE ipbonus_id=$ipbonus_play_fetch[4]";
                                $incre_totalacc_result = $db->Execute($incr_totalacc_query);
                                    check_queryerror($incr_totalacc_query, $incre_totalacc_result);
                            } else {
                                $update_accinnet_q = "UPDATE IPBonus_acc SET ipbonus_last_login=$timestamp WHERE acc='$login'";
                            	$update_accinnet_r = $db->Execute($update_accinnet_q);
                            		check_queryerror($update_accinnet_q, $update_accinnet_r);
                            }
                        }
                    }
                }
			if($acc_vip_time < $timestamp) {
                $acc_vip_update_q = "UPDATE MEMB_INFO SET acc_vip=0, acc_vip_day=0,acc_vip_time=0 WHERE memb___id='$login'";
                $acc_vip_update_r = $db->Execute($acc_vip_update_q);
                    check_queryerror($acc_vip_update_q, $acc_vip_update_r);
				$count_vip_type = count($title_arr['vip2']);				
				for($i=0; $i<$count_vip_type; $i++){
					$danhhieu_index = $title_arr['vip2'][$i][1];
					switch ($title_arr['vip2'][$i][5]){ 
                    	case 1:
                            $danhhieu_data = "RankTitle1";
                    	break;
                    
                    	case 2:
                            $danhhieu_data = "RankTitle2";
                    	break;
                    
                    	case 3:
                            $danhhieu_data = "RankTitle3";
                    	break;
                    
                    	default :
                            $danhhieu_data = "danhhieu";
                    }
					$danhhieu_update_q = "UPDATE Character SET ". $danhhieu_data ."=0 WHERE AccountID='$login' AND ". $danhhieu_data ."=$danhhieu_index";
                        $danhhieu_update_r = $db->Execute($danhhieu_update_q);
                            check_queryerror($danhhieu_update_q,$danhhieu_update_r);
				}
            }		

            $acc_arr['stringlogin'] = $string_login;
            $acc_arr['thehe'] = $thehe;
            $acc_arr['card_total'] = $card_total;
            $acc_arr['gcoin'] = $gcoin;
            $acc_arr['gcoinkm'] = $gcoin_km;
            $acc_arr['vpoint'] = $vpoint;
            $acc_arr['vpoint_km'] = $vpoint_km;
            $acc_arr['WCoin'] = $WCoin;
            $acc_arr['WCoinP'] = $WCoinP;
            $acc_arr['GoblinCoin'] = $GoblinCoin;
            $acc_arr['ppoint'] = $acc_ppoint;
            $acc_arr['ppoint_extra'] = $acc_ppoint_extra;
            $acc_arr['zen'] = $zen;
            $acc_arr['heart'] = $heart;
            $acc_arr['chao'] = $chao;
            $acc_arr['create'] = $cre;
            $acc_arr['blue'] = $blue;
            $acc_arr['phone'] = $phone;
            $acc_arr['email'] = $email_view;
            $acc_arr['reg_day'] = strtotime($reg_day);
            $acc_arr['warehouse_secure_slot'] = $nbb_warehouse_secure_slot;
            $acc_arr['warehouse_secure_item'] = $nbb_warehouse_secure_item;
            $acc_arr['nbb_fb_fanpage_like'] = $nbb_fb_fanpage_like;
            $acc_arr['nbb_vip'] = $nbb_vip;
            $acc_arr['nbb_vip_time'] = $nbb_vip_time;
            $acc_arr['nbb_vip_time_used'] = $nbb_vip_time_used;
			$acc_arr['acc_vip'] = $acc_vip;
			$acc_arr['acc_vip_day'] = $acc_vip_day;
			$acc_arr['acc_vip_time'] = $acc_vip_time;
            $acc_arr['passran'] = $passran;
            $acc_arr['IPBonusPoint'] = $IPBonusPoint;
            $acc_arr['ipbonus_active'] = $ipbonus_active;
            $acc_arr['ipbonus_info'] = $ipbonus_info;
            $acc_arr['char1'] = $char1;
            $acc_arr['char2'] = $char2;
            $acc_arr['char3'] = $char3;
            $acc_arr['char4'] = $char4;
            $acc_arr['char5'] = $char5;
            
                $acc_data = json_encode($acc_arr);
    		  echo "
                <info>OK</info>
                <acc_data>$acc_data</acc_data>
              ";
        }
		break;

	case 'view_invite':
		kiemtra_acc($login);
		$query = "SELECT acc_accept,time_invite,vpoint_invite FROM Invite WHERE acc_invite='$login'";
		$result = $db->Execute( $query );
		$content = "OK<netbanbe>";
		
		while( $row = $result->fetchrow() )
		{
			$char_chinh_check = $db->SelectLimit("Select Name From Character where AccountID='$row[0]' ORDER BY Relifes DESC, Resets DESC, cLevel DESC", 1, 0);
			$char_chinh = $char_chinh_check->fetchrow();
			
	
			$character = $char_chinh[0];
			$time_inv = $row[1];
			$vpoint_inv = $row[2];
			
			$content .= "$character<nbb>$time_inv<nbb>$vpoint_inv<netbanbe>";
		}

		echo $content;
		break;
		
	case 'view_acc':
		kiemtra_acc($login);
		$query = "select bank,vpoint,jewel_chao,jewel_cre,jewel_blue,gcoin from MEMB_INFO WHERE memb___id='$login'";
		$result = $db->Execute( $query );
		$row = $result->fetchrow();
		
		$zen = $row[0];
		$vpoint = $row[1];
		$gcoin = $row[5];
		
		$chao = $row[2];
		$cre = $row[3];
		$blue = $row[4];

		echo "OK<netbanbe>$zen<netbanbe>$vpoint<netbanbe>$chao<netbanbe>$cre<netbanbe>$blue<netbanbe>$gcoin";
		break;
		
	case 'view_lostinfo':
		kiemtra_acc($login);
		$query = "select mail_addr,memb__pwd2,pass2,fpas_ques,fpas_answ from MEMB_INFO WHERE memb___id='$login'";
		$result = $db->Execute( $query );
		$row = $result->fetchrow();
		
		$email = $row[0];
		$pass1 = $row[1];
		$pass2 = $row[2];
		$quest = $row[3];
		$ans = $row[4];
		
	
		echo "$email<netbanbe>$pass1<netbanbe>$pass2<netbanbe>$quest<netbanbe>$ans<netbanbe>OK";
		break;
	
	case 'view_char':
		$query = "select GameID1,GameID2,GameID3,GameID4,GameID5 from AccountCharacter WHERE Id='$login'";
		$result = $db->Execute( $query );
		$row = $result->fetchrow();

		echo "$row[0]<netbanbe>$row[1]<netbanbe>$row[2]<netbanbe>$row[3]<netbanbe>$row[4]";
		break;
		
	case 'chonNV':
        include_once('config/config_event.php');
        include_once('config/config_event1.php');
        include_once('config/config_gioihanrs.php');
        include_once('config/config_relife.php');

		$string_login = $_POST['string_login'];
		checklogin($login,$string_login);
        
		$name = $_POST['name'];
        fixrs($name);
		$query_nv = "select Class, cLevel, LevelUpPoint, pointdutru, Money, Resets, Relifes, khoado, IsThuePoint, point_event, UyThac, PointUyThac, uythacoffline_stat, uythacoffline_time, Top50, UyThacOffline_Daily, point_event, PointUyThac_Event, Strength, Dexterity, Vitality, Energy, Leadership, reset_total, DanhHieu, SCFMasterLevel, SCFMasterPoints, nbb_pmaster, SCFSealItem, SCFSealTime, SCFScrollItem, SCFScrollTime, PkLevel, PkCount, NBB_Relifes_0h, NBB_Resets_0h, RankTitle1, RankTitle2, RankTitle3 FROM Character WHERE AccountID='$login' AND Name='$name'";
		$result_nv = $db->Execute( $query_nv ) OR DIE("Lỗi Query: $query_nv");
		$nv= $result_nv->fetchrow();
			$class = $nv[0];
			$Level = $nv[1];
			$point = $nv[2];
			$point_dutru = $nv[3];
			$zen = $nv[4];
			$reset = $nv[5];
			$relife = $nv[6];
			$khoado = $nv[7];
			$thuepoint = $nv[8];
			$point_event = $nv[9];
			$uythacon = $nv[10];
			$pointuythac = $nv[11];
            $pointuythac_event = $nv[17];
			$uythac_offline = $nv[12];
            $uythac_offline_daily = $nv[15];
			if ($nv[13]>0) $uythac_offline_time = floor(($timestamp-$nv[13])/60);
			else $uythac_offline_time=0;
            $top50 = $nv[14];
            $point_event = $nv[16];
            $event1_type1 = event1_type1_slg($name);
            $event1_type2 = event1_type2_slg($name);
            $event1_type3 = event1_type3_slg($name);
            $event1_type1_daily = event1_type1_daily_slg($name);
            $event1_type2_daily = event1_type2_daily_slg($name);
            $event1_type3_daily = event1_type3_daily_slg($name);
            $resetday = _get_reset_day($name,$timestamp);
            $reset_yesterday = _get_reset_day($name,$timestamp - 24*60*60);
            $Reset_bu_now_q = "SELECT rs_bu FROM TopReset WHERE name='$name' AND year=$year AND month=$month AND day=$day";
            $Reset_bu_now_r = $db->Execute($Reset_bu_now_q);
                check_queryerror($Reset_bu_now_q, $Reset_bu_now_r);
            $Reset_bu_now_f = $Reset_bu_now_r->FetchRow();
            $Reset_bu_now = abs(intval($Reset_bu_now_f[0]));
            $reset_uythac_day = _get_reset_uythac_day($name,$timestamp);
            $resetmonth = _get_reset_month($name);
            $str = $nv[18];
            $dex = $nv[19];
            $vit = $nv[20];
            $ene = $nv[21];
            $com = $nv[22];
            $rs_total = $nv[23];
            if($relife == 0 && $rs_total < $reset) $rs_total = $reset;
            $title = $nv[24];
            $RankTitle1 = $nv[36];
            $RankTitle2 = $nv[37];
            $RankTitle3 = $nv[38];
            $SCFMasterLevel = $nv[25];
            $SCFMasterPoints = $nv[26];
            $nbb_pmaster = $nv[27];
            $PkLevel = $nv[32];
            $PkCount = $nv[33];     if($PkLevel >3) $PkCount = 0;
            
            $reset_now = $nv[5];
            $relife_now = $nv[6];
            $reset_0h = $nv[35];
            $relife_0h = $nv[34];
            
            $SCFSealItem = $nv[28];
            $SCFSealTime = $nv[29];
            $SCFScrollItem = $nv[30];
            $SCFScrollTime = $nv[31];
            
            switch ($SCFSealItem){ 
            	case 6699:
                    $SCFSealItem_info = "Bùa tăng Exp";
            	break;
            
            	case 6700:
                    $SCFSealItem_info = "Bùa Thiên Sứ";
            	break;
            
            	case 6701:
                    $SCFSealItem_info = "Bùa không tăng Exp";
            	break;
            
            	case 6749:
                    $SCFSealItem_info = "Bùa tăng Exp Master";
            	break;
            
            	default :
                    $SCFSealItem_info = "None";
            }
            if($SCFSealTime > 0) {
                if($SCFSealTime >= $timestamp) {
                    $SCFSealTime_info = date('d/m H:i', $SCFSealTime);
                } else {
                    $SCFSealTime_info = date('d/m H:i', $SCFSealTime);
                }
            }
            else $SCFSealTime_info = '-';
            
            switch ($SCFScrollItem){ 
            	case 7240:
                    $SCFScrollItem_info = "Bùa tốc độ tấn công";
            	break;
            
            	case 7242:
                    $SCFScrollItem_info = "Bùa tăng sát thương";
            	break;
            
            	case 7243:
                    $SCFScrollItem_info = "Bùa tăng ma thuật";
            	break;
            
            	case 7266:
                    $SCFScrollItem_info = "Bùa tăng tấn công";
            	break;
            
            	case 7245:
                    $SCFScrollItem_info = "Bùa tăng Mana";
            	break;
            
            	case 7244:
                    $SCFScrollItem_info = "Bùa tăng HP";
            	break;
            
            	default :
                    $SCFScrollItem_info = "None";
            }
            if($SCFScrollTime > 0) {
                if($SCFScrollTime >= $timestamp) {
                    $SCFScrollTime_info = date('d/m H:i', $SCFScrollTime);
                } else {
                    $SCFScrollTime_info = date('d/m H:i', $SCFScrollTime);
                }
            }
            else $SCFScrollTime_info = '-';
		
		$sql_doinv_check = $db->Execute("SELECT * FROM AccountCharacter WHERE Id='$login' AND GameIDC='$name'");
		$doinv_check = $sql_doinv_check->numrows();
			if ($doinv_check > 0) { $doinv = '0'; } else { $doinv = '1'; }
		
		$sql_online_check = $db->Execute("SELECT * FROM MEMB_STAT WHERE memb___id='$login' AND ConnectStat='1'");
		$online_check = $sql_online_check->numrows();
			if ($online_check > 0) { $online = '1'; } else { $online = '0'; }
        
        $quest_daily_arr = _quest_daily($login, $name);
        $quest_wait = $quest_daily_arr['quest_wait'];
        
        $thehe_q = "SELECT thehe, nbb_vip_time, nbb_vip_time_begin FROM MEMB_INFO WHERE memb___id='$login'";
        $thehe_r = $db->Execute($thehe_q);
            check_queryerror($thehe_q, $thehe_r);
        $thehe_f = $thehe_r->FetchRow();
        $thehe = $thehe_f[0];
        $nbb_vip_time = $thehe_f[1];
            if($nbb_vip_time > $timestamp) $nbb_vip = 1;
            else $nbb_vip = 0;
        $nbb_vip_time_begin = $thehe_f[2];
            if($nbb_vip == 1) {
                if($nbb_vip_time_begin > 0) $nbb_vip_time_used = $timestamp - $nbb_vip_time_begin;
                else {
                    $nbb_vip_time_begin_q = "UPDATE MEMB_INFO SET nbb_vip_time_begin = $timestamp WHERE memb___id='$login'";
                    $nbb_vip_time_begin_r = $db->execute($nbb_vip_time_begin_q);
                        check_queryerror($nbb_vip_time_begin_q, $nbb_vip_time_begin_r);
                    
                    $nbb_vip_time_used = 0;
                }
            } else $nbb_vip_time_used = 0;
                
        
        $top1_rl = 0;
        $top1_rs = 0;
        $gioihanrs = 0;
        $rs_total_max = 0;
        
        if($use_gioihanrs[$thehe] == 2) {
            $top1_q = "SELECT NBB_Relifes_0h, NBB_Resets_0h FROM Character JOIN MEMB_INFO ON Character.Top50=1 AND Character.AccountID collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=$thehe";
            $top1_r = $db->Execute($top1_q);
                check_queryerror($top1_q, $top1_r);
            
            $top1_c = $top1_r->NumRows();
            if($top1_c > 0) {
                $top1_f = $top1_r->FetchRow();
                $top1_rl = $top1_f[0];
                $top1_rs = $top1_f[1];
            }
            
            include_once('config_license.php');
            include_once('func_getContent.php');
            $getcontent_url = $url_license . "/api_gioihanrs3.php";
            $getcontent_data = array(
                'acclic'    =>  $acclic,
                'key'    =>  $key,
                
                'rl_reset_relife' =>  $rl_reset_relife,
                        
                'top1_rl'   =>  $top1_rl,
                'top1_rs'   =>  $top1_rs,
                
                'resetday'  =>  $resetday,
                'char_rs_now'   =>  $reset_now,
                'char_rl_now'   =>  $relife_now,
                'char_rs_0h'   =>  $reset_0h,
                'char_rl_0h'   =>  $relife_0h,
                'rs_uythac_day' =>  $reset_uythac_day,
                
                'char_in_top'    =>  $top50,
                
                'ghrs_cap_max'  =>  $ghrs_cap_max,
                'ghrs2_top_cap'  =>  $ghrs2_top_cap,
                'ghrs2_rsmax_cap'    =>  $ghrs2_rsmax_cap[$thehe],
                'ghrs2_rsmax_daily'    =>  $ghrs2_rsmax_daily[$thehe],
		
        		'gioihanrs_rs_bu'	=>	$gioihanrs2_rs_bu[$thehe],
        		'gioihanrs_up_rs'	=>	$gioihanrs2_up_rs[$thehe],
                
                'overrs_sat_extra'    =>  $overrs_sat_extra[$thehe],
                'overrs_sun_extra'    =>  $overrs_sun_extra[$thehe],
                'dayofweek'         =>  date('w', $timestamp)
            ); 
            
            $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
            
        	if ( empty($reponse) ) {
                $notice = "Server bảo trì vui lòng liên hệ Admin để FIX";
            }
            else {
                $info = read_TagName($reponse, 'info');
                if($info == "Error") {
                    $message = read_TagName($reponse, 'message');
                } elseif ($info == "OK") {
                    $gioihanrs = read_TagName($reponse, 'gioihanrs');
                    $rs_total_max = read_TagName($reponse, 'rs_total_max');
                    $gc_rs_bu = read_TagName($reponse, 'gc_rs_bu');
                    $gc_rs_up = read_TagName($reponse, 'gc_rs_up');
                    if(strlen($gioihanrs) == 0) {
                        echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                        
                        $arr_view = "\nDataSend:\n";
                        foreach($getcontent_data as $k => $v) {
                            $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                        }
                        writelog("log_api.txt", $arr_view . $reponse);
                        exit();
                    }
                } else {
                    echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
                    writelog("log_api.txt", $reponse);
                    exit();
                }
            }
         }
            
        $award_unreceive_query = "SELECT count(award_id) FROM NBB_Award WHERE acc='$login' AND name='$name' AND status=0";
        $award_unreceive_result = $db->Execute($award_unreceive_query);
            check_queryerror($award_unreceive_query, $award_unreceive_result);
        $award_unreceive_f = $award_unreceive_result->FetchRow();
        $award_unreceive_count = $award_unreceive_f[0];
            
            if(!isset($gc_rs_bu)) $gc_rs_bu = 0;
            if(!isset($gc_rs_up)) $gc_rs_up = 0;
        
        // Event TOP Auto
        for($i=0; $i<2; $i++) {
            switch ($i){ 
            	case 0:
                    $event_type = 'rs';
            	break;
            
            	case 1:
                    $event_type = 'card';
            	break;
            }
            for($j=0; $j<3; $j++) {
                switch ($j){ 
                	case 0:
                        $event_date = 'day';
                        $date_begin = strtotime(date('Y-m-d', $timestamp));
                	break;
                
                	case 1:
                        $event_date = 'week';
                        $date_begin = (date('N', $timestamp) == 1) ? strtotime(date('Y-m-d', $timestamp)) : strtotime(date('Y-m-d', strtotime('last Monday', $timestamp)));
                	break;
                    
                    case 2:
                        $event_date = 'month';
                        $date_begin = strtotime(date('Y-m-01'));
                	break;
                }
                
                $event_top_auto_day_q = "SELECT time FROM nbb_event_top_auto WHERE acc='$login' AND name='$name' AND event_type='$event_type' AND event_date='$event_date' AND time >= $date_begin AND time <= $timestamp";
                $event_top_auto_day_r = $db->Execute($event_top_auto_day_q);
                    check_queryerror($event_top_auto_day_q, $event_top_auto_day_r);
                $event_top_auto_day_c = $event_top_auto_day_r->NumRows();
                
                $string_key = "event_top_auto_". $event_type ."_". $event_date;
                if($event_top_auto_day_c == 0) {
                    $char_info[$string_key] = 0;
                } else {
                    $event_top_auto_day_f = $event_top_auto_day_r->FetchRow();
                    $char_info[$string_key] = $event_top_auto_day_f[0];
                }
            }
        }
        //==> Event TOP Auto End
            $char_info['online'] = $online;
            $char_info['doinv'] = $doinv;
            $char_info['class'] = $class;
            $char_info['level'] = $Level;
            $char_info['point'] = $point;
            $char_info['point_dutru'] = $point_dutru;
            $char_info['zen'] = $zen;
            $char_info['reset'] = $reset;
            $char_info['resetday'] = $resetday;
            $char_info['reset_yesterday'] = $reset_yesterday;
            $char_info['reset_bu_now'] = $Reset_bu_now;
            $char_info['gc_rs_bu'] = abs(intval($gc_rs_bu));
            $char_info['gc_rs_up'] = abs(intval($gc_rs_up));
            $char_info['resetmonth'] = $resetmonth;
            $char_info['reset_uythac_day'] = $reset_uythac_day;
            $char_info['relife'] = $relife;
            $char_info['khoado'] = $khoado;
            $char_info['thuepoint'] = $thuepoint;
            $char_info['pointevent'] = $point_event;
            $char_info['uythacon'] = $uythacon;
            $char_info['pointuythac'] = $pointuythac;
            $char_info['pointuythac_event'] = $pointuythac_event;
            $char_info['uythacoff'] = $uythac_offline;
            $char_info['uythacoff_time'] = $uythac_offline_time;
            $char_info['uythacoff_daily'] = $uythac_offline_daily;
            $char_info['top50'] = $top50;
            $char_info['top1_rl'] = $top1_rl;
            $char_info['top1_rs'] = $top1_rs;
            $char_info['gioihanrs'] = $gioihanrs;
            $char_info['rs_total_max'] = $rs_total_max;
            $char_info['point_event'] = $point_event;
            $char_info['event1_type1'] = $event1_type1;
            $char_info['event1_type2'] = $event1_type2;
            $char_info['event1_type3'] = $event1_type3;
            $char_info['event1_type1_daily'] = $event1_type1_daily;
            $char_info['event1_type2_daily'] = $event1_type2_daily;
            $char_info['event1_type3_daily'] = $event1_type3_daily;
            $char_info['qwait'] = $quest_wait;
            $char_info['str'] = $str;
            $char_info['dex'] = $dex;
            $char_info['vit'] = $vit;
            $char_info['ene'] = $ene;
            $char_info['com'] = $com;
            $char_info['rs_total'] = $rs_total;
            $char_info['title'] = $title;
            $char_info['RankTitle1'] = $RankTitle1;
            $char_info['RankTitle2'] = $RankTitle2;
            $char_info['RankTitle3'] = $RankTitle3;
            $char_info['MasterLevel'] = $SCFMasterLevel;
            $char_info['MasterPoints'] = $SCFMasterPoints;
            $char_info['nbb_pmaster'] = $nbb_pmaster;
            $char_info['award_unreceive_count'] = $award_unreceive_count;
            $char_info['PkCount'] = $PkCount;
            
            $char_info['SCFSealItem'] = $SCFSealItem_info;
            $char_info['SCFSealTime'] = $SCFSealTime_info;
            $char_info['SCFScrollItem'] = $SCFScrollItem_info;
            $char_info['SCFScrollTime'] = $SCFScrollTime_info;
            
            $char_info['nbb_vip'] = $nbb_vip;
            $char_info['nbb_vip_time'] = $nbb_vip_time;
            $char_info['nbb_vip_time_used'] = $nbb_vip_time_used;
            
            $char_info_data = json_encode($char_info);
            echo "
                <info>OK</info>
                <char_info_data>$char_info_data</char_info_data>
            ";
		break;
		
	case 'view_charrs':
		$query = "select GameID1,GameID2,GameID3,GameID4,GameID5 from AccountCharacter WHERE Id='$login'";
		$result = $db->Execute( $query );
		$row = $result->fetchrow();
		$time = $timestamp;
		for ($i=0;$i<5;++$i)
		{
			if ( !empty($row[$i]) ) {
				$query_reset = $db->Execute("SELECT cLevel,Resets FROM Character WHERE Name='$row[$i]'");
				$rs_reset = $query_reset->fetchrow();
				$level[] = $rs_reset[0];
				$reset[] = $rs_reset[1];
				$resetinday[] = _get_reset_day($row[$i],$timestamp);
			}
			else { $reset[] = 0; $level[] = 0; $resetinday[] = 0; }
		}

		echo "$row[0]<nbb>$level[0]<nbb>$reset[0]<nbb>$resetinday[0]<netbanbe>$row[1]<nbb>$level[1]<nbb>$reset[1]<nbb>$resetinday[1]<netbanbe>$row[2]<nbb>$level[2]<nbb>$reset[2]<nbb>$resetinday[2]<netbanbe>$row[3]<nbb>$level[3]<nbb>$reset[3]<nbb>$resetinday[3]<netbanbe>$row[4]<nbb>$level[4]<nbb>$reset[4]<nbb>$resetinday[4]";
		break;
		
	case 'view_charrl':
		$query = "select GameID1,GameID2,GameID3,GameID4,GameID5 from AccountCharacter WHERE Id='$login'";
		$result = $db->Execute( $query );
		$row = $result->fetchrow();
		
		for ($i=0;$i<5;++$i)
		{
			if ( !empty($row[$i]) ) {
				$query_reset = $db->Execute("SELECT cLevel,Resets,ReLifes FROM Character WHERE Name='$row[$i]'");
				$rs_reset = $query_reset->fetchrow();
				$level[] = $rs_reset[0];
				$reset[] = $rs_reset[1];
				$relife[] = $rs_reset[2];
			}
			else { $reset[] = 0; $level[] = 0; }
		}

		echo "$row[0]<nbb>$level[0]<nbb>$reset[0]<nbb>$relife[0]<netbanbe>$row[1]<nbb>$level[1]<nbb>$reset[1]<nbb>$relife[1]<netbanbe>$row[2]<nbb>$level[2]<nbb>$reset[2]<nbb>$relife[2]<netbanbe>$row[3]<nbb>$level[3]<nbb>$reset[3]<nbb>$relife[3]<netbanbe>$row[4]<nbb>$level[4]<nbb>$reset[4]<nbb>$relife[4]";
		break;

	case 'view_charpk':
		$query = "select GameID1,GameID2,GameID3,GameID4,GameID5 from AccountCharacter WHERE Id='$login'";
		$result = $db->Execute( $query );
		$row = $result->fetchrow();
		
		for ($i=0;$i<5;++$i)
		{
			if ( !empty($row[$i]) ) {
				$query_PK = $db->Execute("SELECT PkLevel,PkCount FROM Character WHERE Name='$row[$i]'");
				$rs_PK = $query_PK->fetchrow();
				$PkLevel[] = $rs_PK[0];
				$PkCount[] = $rs_PK[1];
			}
			else { $PkLevel[] = 0; $PkCount[] = 0; }
		}

		echo "$row[0]<nbb>$PkLevel[0]<nbb>$PkCount[0]<netbanbe>$row[1]<nbb>$PkLevel[1]<nbb>$PkCount[1]<netbanbe>$row[2]<nbb>$PkLevel[2]<nbb>$PkCount[2]<netbanbe>$row[3]<nbb>$PkLevel[3]<nbb>$PkCount[3]<netbanbe>$row[4]<nbb>$PkLevel[4]<nbb>$PkCount[4]";
		break;
		
	case 'view_chargioitinh':
		$query = "select GameID1,GameID2,GameID3,GameID4,GameID5 from AccountCharacter WHERE Id='$login'";
		$result = $db->Execute( $query );
		$row = $result->fetchrow();
		
		for ($i=0;$i<5;++$i)
		{
			if ( !empty($row[$i]) ) {
				$query_Class = $db->Execute("SELECT Class FROM Character WHERE Name='$row[$i]'");
				$rs_Class = $query_Class->fetchrow();
				$Class[] = $rs_Class[0];
			}
			else { $Class[] = 0; }
		}

		echo "$row[0]<nbb>$Class[0]<netbanbe>$row[1]<nbb>$Class[1]<netbanbe>$row[2]<nbb>$Class[2]<netbanbe>$row[3]<nbb>$Class[3]<netbanbe>$row[4]<nbb>$Class[4]";
		break;
	
		case 'view_charblock':
		$query = "select GameID1,GameID2,GameID3,GameID4,GameID5 from AccountCharacter WHERE Id='$login'";
		$result = $db->Execute( $query );
		$row = $result->fetchrow();
		
		for ($i=0;$i<5;++$i)
		{
			if ( !empty($row[$i]) ) {
				$query_Class = $db->Execute("SELECT CtlCode FROM Character WHERE Name='$row[$i]'");
				$rs_Class = $query_Class->fetchrow();
				if ($rs_Class[0] == 1) $block[] = 1;
				else { $block[] = 0; } 
			}
			else { $block[] = 0; }
		}

		echo "$row[0]<nbb>$block[0]<netbanbe>$row[1]<nbb>$block[1]<netbanbe>$row[2]<nbb>$block[2]<netbanbe>$row[3]<nbb>$block[3]<netbanbe>$row[4]<nbb>$block[4]";
		break;
	
	case 'view_charaddpoint':
		$query = "select GameID1,GameID2,GameID3,GameID4,GameID5 from AccountCharacter WHERE Id='$login'";
		$result = $db->Execute( $query );
		$row = $result->fetchrow();
		$time = $timestamp;
		for ($i=0;$i<5;++$i)
		{
			if ( !empty($row[$i]) ) {
				$query_point = $db->Execute("SELECT LevelUpPoint FROM Character WHERE Name='$row[$i]'");
				$rs_point = $query_point->fetchrow();
				$point[] = $rs_point[0];
			}
			else { $point[] = 0; }
		}

		echo "$row[0]<nbb>$point[0]<netbanbe>$row[1]<nbb>$point[1]<netbanbe>$row[2]<nbb>$point[2]<netbanbe>$row[3]<nbb>$point[3]<netbanbe>$row[4]<nbb>$point[4]";
		break;
		
	case 'view_combo':
		$query = "select GameID1,GameID2,GameID3,GameID4,GameID5 from AccountCharacter WHERE Id='$login'";
		$result = $db->Execute( $query );
		$row = $result->fetchrow();
		$time = $timestamp;
		for ($i=0;$i<5;++$i)
		{
			if ( !empty($row[$i]) ) {
				$query_leveldk3 = $db->Execute("SELECT Clevel FROM Character WHERE Name='$row[$i]' AND (Class='2' OR Class='18' OR Class='34')");
				$rs_leveldk3 = $query_leveldk3->fetchrow();
				$leveldk3[] = $rs_leveldk3[0];
			}
			else { $leveldk3[] = 0; }
		}

		echo "$row[0]<nbb>$leveldk3[0]<netbanbe>$row[1]<nbb>$leveldk3[1]<netbanbe>$row[2]<nbb>$leveldk3[2]<netbanbe>$row[3]<nbb>$leveldk3[3]<netbanbe>$row[4]<nbb>$leveldk3[4]";
		break;
		
	case 'view_charrs2item':
		$query = "select GameID1,GameID2,GameID3,GameID4,GameID5 from AccountCharacter WHERE Id='$login'";
		$result = $db->Execute( $query );
		$row = $result->fetchrow();
		$time = $timestamp;
		for ($i=0;$i<5;++$i)
		{
			if ( !empty($row[$i]) ) {
				$query_reset = $db->Execute("SELECT Resets,ReLifes FROM Character WHERE Name='$row[$i]'");
				$rs_reset = $query_reset->fetchrow();
				$reset[] = $rs_reset[0];
				$relife[] = $rs_reset[1];
			}
			else { $reset[] = 0; $relife[] = 0; }
		}

		echo "$row[0]<nbb>$reset[0]<nbb>$relife[0]<netbanbe>$row[1]<nbb>$reset[1]<nbb>$relife[1]<netbanbe>$row[2]<nbb>$reset[2]<nbb>$relife[2]<netbanbe>$row[3]<nbb>$reset[3]<nbb>$relife[3]<netbanbe>$row[4]<nbb>$reset[4]<nbb>$relife[4]";
		break;
		
	case 'view_charrutpoint':
		$query = "select GameID1,GameID2,GameID3,GameID4,GameID5 from AccountCharacter WHERE Id='$login'";
		$result = $db->Execute( $query );
		$row = $result->fetchrow();
		$time = $timestamp;
		for ($i=0;$i<5;++$i)
		{
			if ( !empty($row[$i]) ) {
				$query_point = $db->Execute("SELECT LevelUpPoint,pointdutru FROM Character WHERE Name='$row[$i]'");
				$point = $query_point->fetchrow();
				$point_free[] = $point[0];
				$point_dutru[] = $point[1];
			}
			else { $point_free[] = 0; $point_dutru[] = 0; }
		}

		echo "$row[0]<nbb>$point_free[0]<nbb>$point_dutru[0]<netbanbe>$row[1]<nbb>$point_free[1]<nbb>$point_dutru[1]<netbanbe>$row[2]<nbb>$point_free[2]<nbb>$point_dutru[2]<netbanbe>$row[3]<nbb>$point_free[3]<nbb>$point_dutru[3]<netbanbe>$row[4]<nbb>$point_free[4]<nbb>$point_dutru[4]";
		break;
		
	case 'view_charthuepoint':
		$query = "select GameID1,GameID2,GameID3,GameID4,GameID5 from AccountCharacter WHERE Id='$login'";
		$result = $db->Execute( $query );
		$row = $result->fetchrow();
		for ($i=0;$i<5;++$i)
		{
			if ( !empty($row[$i]) ) {
				$query_point = $db->Execute("SELECT IsThuePoint,TimeThuePoint FROM Character WHERE Name='$row[$i]'");
				$point = $query_point->fetchrow();
				$point_status[] = $point[0];
				$point_time[] = $timestamp-$point[1];
			}
			else { $point_status[] = 0; $point_time[] = 0; }
		}

		echo "$row[0]<nbb>$point_status[0]<nbb>$point_time[0]<netbanbe>$row[1]<nbb>$point_status[1]<nbb>$point_time[1]<netbanbe>$row[2]<nbb>$point_status[2]<nbb>$point_time[2]<netbanbe>$row[3]<nbb>$point_status[3]<nbb>$point_time[3]<netbanbe>$row[4]<nbb>$point_status[4]<nbb>$point_time[4]";
		break;
		
	case 'view_charkhoaitem':
		$query = "select GameID1,GameID2,GameID3,GameID4,GameID5 from AccountCharacter WHERE Id='$login'";
		$result = $db->Execute( $query );
		$row = $result->fetchrow();
		for ($i=0;$i<5;++$i)
		{
			if ( !empty($row[$i]) ) {
				$query_khoaitem = $db->Execute("SELECT khoado FROM Character WHERE Name='$row[$i]'");
				$rs_khoaitem = $query_khoaitem->fetchrow();
				$status_khoado[] = $rs_khoaitem[0];
			}
			else { $status_khoado[] = 0; }
		}

		echo "$row[0]<nbb>$status_khoado[0]<netbanbe>$row[1]<nbb>$status_khoado[1]<netbanbe>$row[2]<nbb>$status_khoado[2]<netbanbe>$row[3]<nbb>$status_khoado[3]<netbanbe>$row[4]<nbb>$status_khoado[4]";
		break;
		
	case 'viewcard':
		$fpage = intval($_POST['page']);
		if(empty($fpage)){ $fpage = 1; }
	
		$fstart = ($fpage-1)*$Card_per_page;
		$fstart = round($fstart,0);
	
		$result = $db->SelectLimit("Select menhgia,card_num,card_serial,time_create,accused,time_nap,status from Card_Vpoint WHERE accdl='" . $login . "' ORDER BY id DESC", $Card_per_page, $fstart);
	
		while($row = $result->fetchrow()) 	{
		echo"$row[0]<nbb>$row[1]<nbb>$row[2]<nbb>$row[3]<nbb>$row[4]<nbb>$row[5]<nbb>$row[6]<netbanbe>";
		}
		break;
		
	
	case 'createcard':
		$menhgia = $_POST['menhgia'];
		if ($menhgia == 10) { $vpoint_tru = $menhgia10000; }
		if ($menhgia == 20) { $vpoint_tru = $menhgia20000; }
		if ($menhgia == 30) { $vpoint_tru = $menhgia30000; }
		if ($menhgia == 40) { $vpoint_tru = $menhgia40000; }
		if ($menhgia == 50) { $vpoint_tru = $menhgia50000; }
		if ($menhgia == 100) { $vpoint_tru = $menhgia100000; }
		
		$query_kt_vpoint = "SELECT vpoint FROM DaiLy WHERE accdl='$login'";
		$result_kt_vpoint = $db->Execute($query_kt_vpoint);
		$kt_vpoint = $result_kt_vpoint->fetchrow();
		
		$vpoint_after = $kt_vpoint[0] - $vpoint_tru;
	
		if ($vpoint_after < 0) { echo "Không đủ Vcent để tạo thẻ."; exit(); }
	
		$serial = $timestamp;
		$mathe_1 = $menhgia;
		$mathe_2 = '-';
		$mathe_3 = rand(100000,999999);
		$mathe = $mathe_1.$mathe_2.$mathe_3;
	
		$time_create = date('H:i d-m-Y',$serial);
		
		$query_insert_card = "INSERT INTO Card_Vpoint (accdl,menhgia,card_num,card_serial,time_create) VALUES ('$login','$menhgia','$mathe','$serial','$time_create')";
		$result_insert_card = $db->Execute($query_insert_card);
	
		$query_tru_vpoint = "UPDATE DaiLy SET vpoint='$vpoint_after' WHERE accdl='$login'";
		$result_tru_vpoint = $db->Execute($query_tru_vpoint);
	
		echo "Tạo thẻ " . $menhgia. ".000 VND thành công.<br>Mã thẻ: $mathe<br>Serial: $serial";
		break;
		
	case 'view_infomu':
		$query_total_acc = "SELECT Count(*) FROM MEMB_INFO";
		$result_total_acc = $db->Execute($query_total_acc);
		$total_acc = $result_total_acc->fetchrow();
	
		$query_total_char = "SELECT Count(*) FROM Character";
		$result_total_char = $db->Execute($query_total_char);
		$total_char = $result_total_char->fetchrow();
	
		echo "$total_acc[0]<nbb>$total_char[0]";
		break;
	
    case 'lvitem':
        $name = $_POST['name'];
        $inventory_query = "SELECT CAST(Inventory AS image) FROM Character WHERE AccountID='$login' AND Name='$name'";
        $inventory_result_sql = $db->Execute($inventory_query);
            if($inventory_result_sql === false) DIE("Query Error : $inventory_query");
        $inventory_result = $inventory_result_sql->fetchrow();

        $inventory = $inventory_result[0];
        $inventory = bin2hex($inventory);
        $item = substr($inventory,12*32,1*32);
        $item = strtoupper($item);

        if($item != 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF') {
            $iop 	= hexdec(substr($item,2,2)); 	// Item Level/Skill/Option Data
            // Skill Check
        	if ($iop<128) 
        		$skill	= '';
        	else {
        		$skill	= '<font color=#99FFCC size=2>Vũ khí có tuyệt chiêu</font>';
        		$iop	= $iop-128;
        	}
        	// Item Level Check
        	$itemlevel	= floor($iop/8);
        	$iop		= $iop-$itemlevel*8;
        	// Luck Check
        	if($iop<4)
        		$luck	= ''; 
        	else {
        		$luck	= "<font size=2>May mắn (Tỉ lệ thành công khi dùng Ngọc Tâm Linh +25%)<br />May mắn (Tỉ lệ gây sát thương tối đa +5%)</font>";
        		$iop	= $iop-4;
        	}
            if($itemlevel == 0) echo 'ko';
            else echo $itemlevel;
        } else echo "Item không có trong ô đầu tiên trên túi đồ.";
        break;
}


} else echo "Error";
$db->Close();
?>