<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
	include_once("security.php");
include_once("config.php");
include ('config/config_thehe.php');
include('config/config_ranking.php');

if(!isset($rank_other_slg) || abs(intval($rank_other_slg)) < 20) $rank_other_slg = 20;

$passtransfer = $_POST["passtransfer"];

if ($passtransfer == $transfercode) {
    $toptuluyen_arr = array();
    for($i=1;$i<count($thehe_choise);$i++) {
        if(strlen($thehe_choise[$i]) > 1) {
            $toptuluyen_q = "SELECT TOP $rank_other_slg Name, nbbtuluyen_str_point, nbbtuluyen_agi_point, nbbtuluyen_vit_point, nbbtuluyen_ene_point, (nbbtuluyen_str_point + nbbtuluyen_agi_point + nbbtuluyen_vit_point + nbbtuluyen_ene_point) FROM Character JOIN MEMB_INFO ON (nbbtuluyen_str_point + nbbtuluyen_agi_point + nbbtuluyen_vit_point + nbbtuluyen_ene_point)>0 AND Character.AccountID collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=$i ORDER BY (nbbtuluyen_str_point + nbbtuluyen_agi_point + nbbtuluyen_vit_point + nbbtuluyen_ene_point) DESC";
            $toptuluyen_r = $db->Execute($toptuluyen_q);
                check_queryerror($toptuluyen_q, $toptuluyen_r);
            while($toptuluyen_f = $toptuluyen_r->FetchRow()) {
                $toptuluyen_arr[$i][] = array(
                    'name'  => $toptuluyen_f[0],
                    'str'   =>  $toptuluyen_f[1],
                    'agi'   =>  $toptuluyen_f[2],
                    'vit'   =>  $toptuluyen_f[3],
                    'ene'   =>  $toptuluyen_f[4],
                    'all'   =>  $toptuluyen_f[5]
                );
            }
        }
    }
    
    $toptuluyen_data = json_encode($toptuluyen_arr);
    echo "<info>OK</info><toptuluyen>$toptuluyen_data</toptuluyen>";
}
$db->Close();
?>