<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$page = $_GET['page'];
$file_edit = 'config/rewarditem.txt';

switch ($act)
{
	case 'reward_taphoa': 
		$reward_type = 1;
        $tilte = "Cho Thuê Tạp Hóa";
		break;
	case 'reward_kiem': 
		$reward_type = 2;
		$tilte = "Cho Thuê Kiếm";
		break;
	case 'reward_gay': 
		$reward_type = 3;
		$tilte = "Cho Thuê Gậy";
		break;
	case 'reward_cung': 
        $reward_type = 4;
		$tilte = "Cho Thuê Cung";
		break;
	case 'reward_vukhikhac':
        $reward_type = 5; 
		$tilte = "Cho Thuê Vũ Khí Khác";
		break;
	case 'reward_khien': 
        $reward_type = 6;
		$tilte = "Cho Thuê Khiên";
		break;
	case 'reward_mu': 
        $reward_type = 7;
		$tilte = "Cho Thuê Mũ";
		break;
	case 'reward_ao': 
        $reward_type = 8;
		$tilte = "Cho Thuê Áo";
		break;
	case 'reward_quan': 
        $reward_type = 9;
		$tilte = "Cho Thuê Quần";
		break;
	case 'reward_tay': 
        $reward_type = 10;
		$tilte = "Cho Thuê Tay";
		break;
	case 'reward_chan': 
        $reward_type = 11;
		$tilte = "Cho Thuê Chân";
		break;
	case 'reward_trangsuc':
        $reward_type = 12; 
		$tilte = "Cho Thuê Trang Sức";
		break;
	case 'reward_canh': 
        $reward_type = 13;
		$tilte = "Cho Thuê Cánh";
		break;
	default: $file_edit = ''; break;
}

if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

if($accept == 0) $show_accept = "disabled='disabled'";
else $show_accept = "";

require_once('admin_cfg/function.php');


$action = $_POST[action];

switch ($action)
{
	case 'add':
		$code = $_POST['code'];       $code = strtoupper($code);
		$price = $_POST['price']; $price = abs(intval($price));
        $stat = $_POST['stat'];
		
        $error = "";
        if (!preg_match("/^[A-F0-9]*$/i", $code))
    	{
            $error .= "Dữ liệu lỗi <strong>Mã Item</strong> : $code . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($code) <> 32 ) {
            $error .= "Dữ liệu lỗi <strong>Mã Item</strong> : $code phải có 32 ký tự.<br />";
        }
        
        if(strlen($error) > 0) {
            $notice = "<center><b><font color='red'>$error</font></b></center>";
        } else {
            $itemreward_list_arr = reward_load($file_edit);
            
            include_once('config_license.php');
            include_once('func_getContent.php');
            $getcontent_url = $url_license . "/api_reward.php";
            $getcontent_data = array(
                'acclic'    =>  $acclic,
                'key'    =>  $key,
                'action'    =>  'reward_cfg_add',
                
                'code'    =>  $code
            ); 
            
            $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
            
            if ( empty($reponse) ) {
                echo "Kết nối đến API bị gián đoạn.";
                exit();
            }
            else {
                $info = read_TagName($reponse, 'info');
                if($info == "Error") {
                    echo read_TagName($reponse, 'message');
                    exit();
                } elseif ($info == "OK") {
                    $item_reward_info = read_TagName($reponse, 'item_reward_info');
                    if(strlen($item_reward_info) == 0) {
                        echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                        
                        $arr_view = "\nDataSend:\n";
                        foreach($getcontent_data as $k => $v) {
                            $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                        }
                        writelog("log_api.txt", $arr_view . $reponse);
                        exit();
                    } else {
                        $item_reward_info_arr = json_decode($item_reward_info, true);
                        $item_code = $item_reward_info_arr['code'];
                        $reward_type = $item_reward_info_arr['reward_type'];
                        
                        if(isset($itemreward_list_arr[$reward_type][$item_code])) {
                            $err = "Item ". $itemreward_list_arr[$reward_type][$item_code]['item_name'] ." đã có sẵn. Không thể thêm trùng lặp.";
                        } else {
                            if($stat != 1) $stat = 0;
                            $itemreward_list_arr[$reward_type][$item_code] = array(
                                'item_name'  =>  $item_reward_info_arr['name'],
                                'price'  =>  $price,
                                'img'  =>  $item_reward_info_arr['image'],
                                'exl_type'  =>  $item_reward_info_arr['exl_type'],
                                'stat'  =>  $stat
                            );
                        }
                    }
                } else {
                    echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
                    writelog("log_api.txt", $reponse);
                    exit();
                }
            }
		    
			if($err) {
		        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
		    } else {
		    	$content = '';
                $content = json_encode($itemreward_list_arr);
        		
        		replacecontent($file_edit,$content);
    
        		include('config/config_sync.php');
    		    for($i=0; $i<count($url_hosting); $i++)
    		    {
    		        if($url_hosting[$i]) {
    		            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
    		            if($sync_send == 'OK') {
    		                
    		            } else {
    		                $err .= $sync_send;
    		            }
    		        }
    		    }
                
                $notice = "<center><font color='blue'>Thêm <strong>". $itemreward_list_arr[$reward_type][$item_code]['item_name'] ."</strong> thành công</font></center>";
		    }
        }
    		
		break;
	
	case 'edit':
		$code = $_POST['code'];
		$price = $_POST['price'];     $price = abs(intval($price));
        $reward_type = $_POST['reward_type'];
        $stat = $_POST['stat'];
		
        $error = "";
        if (!preg_match("/^[A-F0-9]*$/i", $code))
    	{
            $error .= "Dữ liệu lỗi <strong>Mã Item</strong> : $code . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($code) <> 32 ) {
            $error .= "Dữ liệu lỗi <strong>Mã Item</strong> : $code phải gồm 32 ký tự.<br />";
        }
        
        if(strlen($error) > 0) {
            $notice = "<center><b><font color='red'>$error</font></b></center>";
        } else {
            $itemreward_list_arr = reward_load($file_edit);
    		$itemreward_list_arr[$reward_type][$code]['price'] = $price;
            if($stat != 1) $stat = 0;
            $itemreward_list_arr[$reward_type][$code]['stat'] = $stat;
            
			if($err) {
		        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
		    } else {
		    	$content = '';
                $content = json_encode($itemreward_list_arr);
        		
        		replacecontent($file_edit,$content);
        		
        		include('config/config_sync.php');
    		    for($i=0; $i<count($url_hosting); $i++)
    		    {
    		        if($url_hosting[$i]) {
    		            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
    		            if($sync_send == 'OK') {
    		                
    		            } else {
    		                $err .= $sync_send;
    		            }
    		        }
    		    }
                
                $notice = "<center><font color='blue'>Sửa <strong>". $itemreward_list_arr[$reward_type][$code]['item_name'] ."</strong> thành công</font></center>";
		    }
        }
		break;
	
	case 'del':
		$code = $_POST['code'];
		$price = $_POST['price'];     $price = abs(intval($price));
        $reward_type = $_POST['reward_type'];
		
        $error = "";
        if (!preg_match("/^[A-F0-9]*$/i", $code))
    	{
            $error .= "Dữ liệu lỗi <strong>Mã Item</strong> : $code . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($code) <> 32 ) {
            $error .= "Dữ liệu lỗi <strong>Mã Item</strong> : $code phải gồm 32 ký tự.<br />";
        }
        
        if(strlen($error) > 0) {
            $notice = "<center><b><font color='red'>$error</font></b></center>";
        } else {
            $itemreward_list_arr = reward_load($file_edit);
            $item_name = $itemreward_list_arr[$reward_type][$code]['item_name'];            
    		unset($itemreward_list_arr[$reward_type][$code]);
            
			if($err) {
		        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
		    } else {
		    	$content = '';
                $content = json_encode($itemreward_list_arr);
        		
        		replacecontent($file_edit,$content);
        		
        		include('config/config_sync.php');
    		    for($i=0; $i<count($url_hosting); $i++)
    		    {
    		        if($url_hosting[$i]) {
    		            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
    		            if($sync_send == 'OK') {
    		                
    		            } else {
    		                $err .= $sync_send;
    		            }
    		        }
    		    }
                
                $notice = "<center><font color='blue'>Xóa <strong>". $item_name ."</strong> thành công</font></center>";
		    }
        }
		break;
}


$item_read = reward_load($file_edit);
?>
		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình <?php echo $tilte; ?></h1>
			</div><br>
				Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php 
if($notice) echo $notice;

switch ($page)
{
	case 'add': 
		$content = "<center><b>Thêm Item</b></center><br>
			<form id='editconfig' name='editconfig' method='post' action='admin.php?mod=editreward&act=".$act."'>
			<input type='hidden' name='action' value='add'/>
			<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
				<tr bgcolor='#FFFFFF' >
					<td >Mã Item</td>
					<td ><input type='text' name='code' value='' size='36' maxlength='32'/> <i>(32 Mã lấy từ MUMaker)</i></td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >Giá thuê 1 ngày</td>
					<td ><input type='text' name='price' value='' size='5'/> Gcoin</td>
				</tr>
                <tr bgcolor='#FFFFFF' >
					<td >Cho Thuê</td>
					<td ><input type='checkbox' name='stat' value='1' checked /></td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >&nbsp;</td>
					<td align='center'><input type='submit' name='Submit' value='Thêm Item' ". $show_accept ." /></td>
				</tr>
			</table>
			</form>";
		echo $content;
		break;
	
	case 'edit': 
		$item = $_GET['item'];
        $reward_type = $_GET['reward_type'];
        
        $status = ($item_read[$reward_type][$item]['stat'] == 1) ? 'checked' : '';
        
		$content = "<center><b>Sửa Item</b></center><br>
			<form id='editconfig' name='editconfig' method='post' action='admin.php?mod=editreward&act=".$act."'>
			<input type='hidden' name='action' value='edit'/>
            <input type='hidden' name='code' value='". $item ."'/>
            <input type='hidden' name='reward_type' value='". $reward_type ."'/>
			<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
				<tr bgcolor='#FFFFFF' >
					<td width='100'>Tên Item</td>
					<td >". $item_read[$reward_type][$item]['item_name'] ."</td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >Giá thuê 1 ngày</td>
					<td ><input type='text' name='price' value='". $item_read[$reward_type][$item]['price'] ."' size='5'/> Gcoin</td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >Hình Item</td>
					<td bgcolor='#333'><img src='items/". $item_read[$reward_type][$item]['img'] .".gif'></td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >Cho Thuê</td>
					<td ><input type='checkbox' name='stat' value='1' ". $status ." /></td>
				</tr>
                <tr bgcolor='#FFFFFF' >
					<td >&nbsp;</td>
					<td align='center'><input type='submit' name='Submit' value='Sửa' ". $show_accept ." /></td>
				</tr>
			</table>
			</form>";
		echo $content;
		break;
	
	case 'del':
		$item = $_GET['item'];
		$reward_type = $_GET['reward_type'];
        
		$content = "<center><b>Xóa Item</b></center><br>
			<form id='delitem' name='delitem' method='post' action='admin.php?mod=editreward&act=".$act."'>
			<input type='hidden' name='action' value='del'/>
			<input type='hidden' name='code' value='". $item ."'/>
            <input type='hidden' name='reward_type' value='". $reward_type ."'/>
			<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
				<tr bgcolor='#FFFFFF' >
					<td >Tên Item</td>
					<td >". $item_read[$reward_type][$item]['item_name'] ."</td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >Giá thuê 1 ngày</td>
					<td >". $item_read[$reward_type][$item]['price'] ." Gcoin</td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >Hình Item</td>
					<td bgcolor='#333'><img src='items/". $item_read[$reward_type][$item]['img'] .".gif'></td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >&nbsp;</td>
					<td align='center'><input type='submit' name='Submit' value='Xóa' ". $show_accept ." /></td>
				</tr>
			</table>
			</form>";
		echo $content;
		
		break;	
		
	default: 
		echo "<div align='right'><a href='admin.php?mod=editreward&act=". $act ."&page=add'>+ Thêm Item cho thuê</a></div><br>";
		reward_display($item_read, $reward_type, $act); 
		break;
	
}

?>
				
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
