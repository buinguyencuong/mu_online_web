<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$act = $_GET['act'];
switch ($act) {
	case 'event_week': include('editevent/adm_event_week.php'); break;
    case 'event_month': include('editevent/adm_event_month.php'); break;
    
    case 'event_giftthangcap': include('editevent/adm_event_giftthangcap.php'); break;
    case 'event1': include('editevent/adm_event1.php'); break;
    case 'event2': include('editevent/adm_event2.php'); break;
	case 'event_goldbox': include('editevent/adm_event_goldbox.php'); break;
    case 'event_santa': include('editevent/adm_event_santa.php'); break;
    case 'event_itemfind': include('editevent/adm_event_itemfind.php'); break;
    
    case 'giftcode_tanthu': include('editevent/adm_giftcode_tanthu.php'); break;
    case 'giftcode_tanthu2': include('editevent/adm_giftcode_tanthu2.php'); break;
    case 'giftcode_rs': include('editevent/adm_giftcode_rs.php'); break;
    case 'giftcode_week': include('editevent/adm_giftcode_week.php'); break;
    case 'giftcode_month': include('editevent/adm_giftcode_month.php'); break;
    case 'giftcode_all': include('editevent/adm_giftcode_all.php'); break;
    
    case 'giftcode_type': include('editevent/adm_giftcode_type.php'); break;
    case 'giftcode_random': include('editevent/adm_giftcode_random.php'); break;
    
    case 'event_top_auto': include('editevent/adm_event_top_auto.php'); break;
    
	default: include('editevent/adm_event.php'); break;
}
?>