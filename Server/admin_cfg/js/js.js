$(document).ready(function() {
	$('#event_napthegc_begin').datepicker({dateFormat: 'yy-mm-dd'});
    $('#event_napthegc_end').datepicker({dateFormat: 'yy-mm-dd'});
	
    $('#event_epitem_begin').datepicker({dateFormat: 'yy-mm-dd'});
    $('#event_epitem_end').datepicker({dateFormat: 'yy-mm-dd'});
    
    $('#event_toprs_begin').datepicker({dateFormat: 'yy-mm-dd'});
    $('#event_toprs_end').datepicker({dateFormat: 'yy-mm-dd'});
    
    $('#event_toppoint_begin').datepicker({dateFormat: 'yy-mm-dd'});
    $('#event_toppoint_end').datepicker({dateFormat: 'yy-mm-dd'});
    
    $('#event_topcard_begin').datepicker({dateFormat: 'yy-mm-dd'});
    $('#event_topcard_end').datepicker({dateFormat: 'yy-mm-dd'});
    
    $('#event_goldbox_begin').datepicker({dateFormat: 'yy-mm-dd'});
    $('#event_goldbox_end').datepicker({dateFormat: 'yy-mm-dd'});
    
    $('#giftcode_tanthu2_begin').datepicker({dateFormat: 'yy-mm-dd'});
    $('#giftcode_tanthu2_end').datepicker({dateFormat: 'yy-mm-dd'});
    $('#giftcode_tanthu2_acc_reg').datepicker({dateFormat: 'yy-mm-dd'});
    
    $('#event2_begin').datepicker({dateFormat: 'yy-mm-dd'});
    $('#event2_end').datepicker({dateFormat: 'yy-mm-dd'});
    
    $('#khuyenmai_begin').datepicker({dateFormat: 'yy-mm-dd'});
    $('#khuyenmai_end').datepicker({dateFormat: 'yy-mm-dd'});
    
    $('.time_choise').datepicker({dateFormat: 'yy-mm-dd'});
});