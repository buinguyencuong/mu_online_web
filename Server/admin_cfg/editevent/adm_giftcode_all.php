<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/

$file_edit = 'config/giftcode_all.txt';
$file_giftcode = 'config/giftcode_random.txt';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

if($accept==0) $show_accept = "disabled='disabled'";
else $show_accept = "";

$page = $_GET['page'];

require_once('admin_cfg/function.php');

// Read Giftcode
$giftcode_all_arr = _giftcode_all_load($file_edit);
$giftcode_arr = _giftcode_load($file_giftcode);
// Read Giftcode End

if($_POST) {
    $action = $_POST[action];
    
    switch ($action) {
        
        case 'add':
            $gift_type = $_POST['gift_type'];         $gift_type = abs(intval($gift_type));
    
            $error = "";
            if(!isset($giftcode_arr[$gift_type]['name']) || strlen(($giftcode_arr[$gift_type]['name'])) < 5) {
                $error .= "Dữ liệu lỗi <strong>Phần thưởng Giftcode</strong> : ". $giftcode_arr[$gift_type]['name'] ." không tồn tại.<br />";
            }
            
            if(strlen($error) > 0) {
                $notice = "<center><b><font color='red'>" . $error . "</font></b></center>";
            } else {
                
                //Config
            	$characters = 'abcdef123456789';
            	$random_string_length = 6;
            	
                // Create GiftCode
                $giftcode_created_flag = false;
                while($giftcode_created_flag === false) {
                    $giftcode = "";
                    for ($i = 0; $i < $random_string_length; $i++) { 
                		$giftcode .= $characters[rand(0, strlen($characters) - 1)]; 
                 	}
                    $giftcode = strtoupper($giftcode);
                    
                    if(!isset($giftcode_all_arr[$giftcode])) $giftcode_created_flag = true;
                }
                
                $giftcode_all_arr[$giftcode] = $gift_type;
                
                $content = json_encode($giftcode_all_arr);
                
        		replacecontent($file_edit,$content);
        		
        		include('config/config_sync.php');
    		    for($i=0; $i<count($url_hosting); $i++)
    		    {
    		        if($url_hosting[$i]) {
    		            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
    		            if($sync_send == 'OK') {
    		                
    		            } else {
    		                $err .= $sync_send;
    		            }
    		        }
    		    }
    		    
    			if($err) {
    		        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    		    } else {
    		    	$notice = "<center><font color='blue'>Thêm thành công</font></center>";
    		    }
            }
            
    		break;
    	
    	case 'edit':
    		$item = $_POST['item'];
            $gift_type = $_POST['gift_type'];         $gift_type = abs(intval($gift_type));
    
            $error = "";
            if(!isset($giftcode_arr[$gift_type]['name']) || strlen(($giftcode_arr[$gift_type]['name'])) < 5) {
                $error .= "Dữ liệu lỗi <strong>Phần thưởng Giftcode</strong> : ". $giftcode_arr[$gift_type]['name'] ." không tồn tại.<br />";
            }
            
            if(strlen($error) > 0) {
                $notice = "<center><b><font color='red'>" . $error . "</font></b></center>";
            } else {
                $giftcode_all_arr[$item] = $gift_type;
                
                $content = json_encode($giftcode_all_arr);
                    
                replacecontent($file_edit,$content);
        		
        		include('config/config_sync.php');
    		    for($i=0; $i<count($url_hosting); $i++)
    		    {
    		        if($url_hosting[$i]) {
    		            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
    		            if($sync_send == 'OK') {
    		                
    		            } else {
    		                $err .= $sync_send;
    		            }
    		        }
    		    }
    		    
    			if($err) {
    		        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    		    } else {
    		    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    		    }
            }
    		break;
    	
    	case 'del':
    		$item = $_POST['item'];
    		
            unset($giftcode_all_arr[$item]);
            $content = json_encode($giftcode_all_arr);
    		
    		replacecontent($file_edit,$content);
    		include('config/config_sync.php');
    	    for($i=0; $i<count($url_hosting); $i++)
    	    {
    	        if($url_hosting[$i]) {
    	            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
    	            if($sync_send == 'OK') {
    	                
    	            } else {
    	                $err .= $sync_send;
    	            }
    	        }
    	    }
    	    
    		if($err) {
    	        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    	    } else {
    	    	$notice = "<center><font color='blue'>Xóa thành công</font></center>";
    	    }
    		break;
    }
}
?>
		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình <a href="admin.php?mod=editevent&act=giftcode_random&type=<?php echo $type; ?>" target="_self"><?php echo $tilte; ?></a></h1>
			</div><br />
				Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php 
if($notice) echo $notice;

switch ($page)
{
    case 'add': 
		$content = "<center>Thêm GiftCode 1ForALL</center><br>
			<form id='editconfig' name='editconfig' method='post' action='admin.php?mod=editevent&act=giftcode_all&page=add'>
			<input type='hidden' name='action' value='add'/>
			<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Phần thưởng Giftcode</td>
					<td >
                        <select name='gift_type'>";
                            if(is_array($giftcode_arr)) {
                                foreach($giftcode_arr as $k => $v) {
                                    $content .= "<option value='$k' ";
                                    if($_POST['gift_type']==$k) $content .= "selected";
                                    $content .= " >". $v['name'] ."</option>";
                                }
                            }
        $content .= "    </select>
                    </td>
				</tr>
                
				<tr bgcolor='#FFFFFF' >
					<td >&nbsp;</td>
					<td><input type='submit' name='Submit' value='Thêm Giftcode 1ForALL' ". $show_accept ." /></td>
				</tr>
			</table>
			</form>";
		echo $content;
		break;
	
	case 'edit': 
        $item = $_GET['item'];          $item = $item;
		$content = "<center><b>Sửa Giftcode 1ForALL</b></center><br>
			<form id='editconfig' name='editconfig' method='post' action='admin.php?mod=editevent&act=giftcode_all&page=edit&item=".$item."'>
			<input type='hidden' name='action' value='edit'/>
			<input type='hidden' name='item' value='".$item."'/>
			<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >GiftCode</td>
					<td >". $item ."</td>
				</tr>
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Phần thưởng Giftcode</td>
					<td >
                        <select name='gift_type'>";
                            if(is_array($giftcode_arr)) {
                                foreach($giftcode_arr as $k => $v) {
                                    $content .= "<option value='$k' ";
                                    if($giftcode_all_arr[$item] == $k) $content .= "selected";
                                    $content .= " >". $v['name'] ."</option>";
                                }
                            }
        $content .= "    </select>
                    </td>
				</tr>
				
				<tr bgcolor='#FFFFFF' >
					<td >&nbsp;</td>
					<td align='center'><input type='submit' name='Submit' value='Sửa' ". $show_accept ." /></td>
				</tr>
			</table>
			</form>";
		echo $content;
		break;
	
	case 'del':
		$item = $_GET['item'];
        $gifttype = $giftcode_all_arr[$item];
		$content = "<center><b>Xóa Loại Giftcode</b></center><br>
			<form id='delitem' name='delitem' method='post' action='admin.php?mod=editevent&act=giftcode_all'>
			<input type='hidden' name='action' value='del'/>
			<input type='hidden' name='item' value='".$item."'/>
			<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >GiftCode</td>
					<td >". $item ."</td>
				</tr>
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Phần thưởng Giftcode</td>
					<td >". $giftcode_arr[$gifttype]['name'] ."</td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >&nbsp;</td>
					<td align='left'><input type='submit' name='Submit' value='Xóa' ". $show_accept ." /></td>
				</tr>
			</table>
			</form>";
		echo $content;
		
		break;	
		
	default: 
		echo "<b>Tạo GiftCode tương ứng với phần thưởng nhận được</b>.<br />
            GiftCode 1ForALL : 1 GiftCode có thể sử dụng cho tất cả các tài khoản.
            <br /><br />
        ";

        echo "<div align='right'><a href='admin.php?mod=editevent&act=giftcode_all&page=add'>+ Thêm GiftCode 1ForALL</a></div><br>";
        _giftcode_all_display($giftcode_all_arr, $giftcode_arr); 
        
		
		break;
	
}

?>
				
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
