<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/

$file_edit = 'config/giftcode_random.txt';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

if($accept==0) $show_accept = "disabled='disabled'";
else $show_accept = "";

if(isset($_GET['type'])) $type = $_GET['type'];

$page = $_GET['page'];

require_once('admin_cfg/function.php');

// Read Giftcode
$giftcode_arr = _giftcode_load($file_edit);
// Read Giftcode End
$tilte = $giftcode_arr[$type]['name'];

if($_POST) {
    $action = $_POST[action];
    
    switch ($action) {
    	case 'type_create':
    		$type_name = $_POST['type_name'];
    
            $error = "";
            if (strlen($type_name) <= 0)
        	{
                $error .= "Dữ liệu lỗi <strong>Chưa nhập tên loại phần thưởng Giftcode</strong>.<br />"; 
        	}
            
            if(strlen($error) > 0) {
                $notice = "<center><b><font color='red'>" . $error . "</font></b></center>";
            } else {
                if(!is_array($giftcode_arr)) $giftcode_arr = array();
                $id_last = count($giftcode_arr) - 1;
                $id_new = $id_last + 1;
                $giftcode_arr[$id_new]['name'] = $type_name;
                $content = json_encode($giftcode_arr);
                
        		replacecontent($file_edit,$content);
        		
        		include('config/config_sync.php');
    		    for($i=0; $i<count($url_hosting); $i++)
    		    {
    		        if($url_hosting[$i]) {
    		            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
    		            if($sync_send == 'OK') {
    		                
    		            } else {
    		                $err .= $sync_send;
    		            }
    		        }
    		    }
    		    
    			if($err) {
    		        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    		    } else {
    		    	$notice = "<center><font color='blue'>Tạo Loại phần thưởng GiftCode thành công</font></center>";
    		    }
            }
            
    		break;
        
        case 'add':
    		$code = $_POST['code'];
            $gcoin_km = $_POST['gcoin_km'];         $gcoin_km = abs(intval($gcoin_km));
            $vpoint_km = $_POST['vpoint_km'];       $vpoint_km = abs(intval($vpoint_km));
            $bank_zen = $_POST['bank_zen'];         $bank_zen = abs(intval($bank_zen));
            $bank_chao = $_POST['bank_chao'];       $bank_chao = abs(intval($bank_chao));
            $bank_cre = $_POST['bank_cre'];         $bank_cre = abs(intval($bank_cre));
            $bank_blue = $_POST['bank_blue'];       $bank_blue = abs(intval($bank_blue));
            $phucloi_point = $_POST['phucloi_point'];   $phucloi_point = abs(intval($phucloi_point));
    		$des = $_POST['des'];
    		$rate = $_POST['rate']; $rate = abs(intval($rate));
    
            $error = "";
            if (!preg_match("/^[A-F0-9]*$/i", $code))
        	{
                $error .= "Dữ liệu lỗi <strong>Mã phần thưởng</strong> : $code . Chỉ được sử dụng ký tự a-f, A-F, so (0-9).<br />"; 
        	}
            if( strlen($code)%32 != 0 ) {
                $error .= "Dữ liệu lỗi <strong>Mã phần thưởng</strong> : $code phải có tổng ký tự chia hết cho 32.<br />";
            }
            if( strlen($des) <= 5 ) {
                $error .= "Dữ liệu lỗi <strong>Mô tả phần thưởng</strong> : $des chưa rõ ràng.<br />";
            }
            if(strlen($code) == 0 && $gcoin_km == 0 && $vpoint_km == 0 && $bank_zen == 0 && $bank_chao == 0 && $bank_cre == 0 && $bank_blue == 0 && $phucloi_point == 0) {
                $error .= "Dữ liệu lỗi <strong>Chưa có dữ liệu phần thưởng</strong>.<br />";
            }
            
            if(strlen($error) > 0) {
                $notice = "<center><b><font color='red'>" . $error . "</font></b></center>";
            } else {
                $count_gift = count($giftcode_arr[$type]['gift']);
                if($count_gift == 0 && $rate == 0) $rate = 10;
                $rate_from = 1;
                $rate_to = $rate;
                $giftcode_arr[$type]['gift'][] = array(
                    'itemcode'  =>  $code,
                    'gcoin_km'  =>  $gcoin_km,
                    'vpoint_km'  =>  $vpoint_km,
                    'bank_zen'  =>  $bank_zen,
                    'bank_chao' =>  $bank_chao,
                    'bank_cre'  =>  $bank_cre,
                    'bank_blue' =>  $bank_blue,
                    'phucloi_point' =>  $phucloi_point,
                    'des' =>  $des,
                    'rate'   =>  $rate,
                    'tyle' =>  10000,
                    'rate_from'  =>  $rate_from,
                    'rate_to'    =>  $rate_to
                );
                
                if($count_gift > 0) {
                    $giftcode_arr = _giftcode_calculator_rate($giftcode_arr, $type);
                }
                $content = json_encode($giftcode_arr);
                
        		replacecontent($file_edit,$content);
        		
        		include('config/config_sync.php');
    		    for($i=0; $i<count($url_hosting); $i++)
    		    {
    		        if($url_hosting[$i]) {
    		            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
    		            if($sync_send == 'OK') {
    		                
    		            } else {
    		                $err .= $sync_send;
    		            }
    		        }
    		    }
    		    
    			if($err) {
    		        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    		    } else {
    		    	$notice = "<center><font color='blue'>Thêm thành công</font></center>";
    		    }
            }
            
    		break;
    	
    	case 'edit':
    		$item = $_POST['item'];
    		$code = $_POST['code'];
            $gcoin_km = $_POST['gcoin_km'];         $gcoin_km = abs(intval($gcoin_km));
            $vpoint_km = $_POST['vpoint_km'];       $vpoint_km = abs(intval($vpoint_km));
            $bank_zen = $_POST['bank_zen'];         $bank_zen = abs(intval($bank_zen));
            $bank_chao = $_POST['bank_chao'];       $bank_chao = abs(intval($bank_chao));
            $bank_cre = $_POST['bank_cre'];         $bank_cre = abs(intval($bank_cre));
            $bank_blue = $_POST['bank_blue'];       $bank_blue = abs(intval($bank_blue));
            $phucloi_point = $_POST['phucloi_point'];   $phucloi_point = abs(intval($phucloi_point));
    		$des = $_POST['des'];
    		$rate = $_POST['rate']; $rate = abs(intval($rate));
    		
            $error = "";
            if (!preg_match("/^[A-F0-9]*$/i", $code))
        	{
                $error .= "Dữ liệu lỗi <strong>Mã phần thưởng</strong> : $code . Chỉ được sử dụng ký tự a-f, A-F, so (0-9).<br />"; 
        	}
            if( strlen($code)%32 != 0 ) {
                $error .= "Dữ liệu lỗi <strong>Mã phần thưởng</strong> : $code phải có tổng ký tự chia hết cho 32.<br />";
            }
            if( strlen($des) <= 5 ) {
                $error .= "Dữ liệu lỗi <strong>Mô tả phần thưởng</strong> : $des chưa rõ ràng.<br />";
            }
            if(strlen($code) == 0 && $gcoin_km == 0 && $vpoint_km == 0 && $bank_zen == 0 && $bank_chao == 0 && $bank_cre == 0 && $bank_blue == 0 && $phucloi_point == 0) {
                $error .= "Dữ liệu lỗi <strong>Chưa có dữ liệu phần thưởng</strong>.<br />";
            }
            
            if(strlen($error) > 0) {
                $notice = "<center><b><font color='red'>" . $error . "</font></b></center>";
            } else {
                $edit_flag = false;
        		if($giftcode_arr[$type]['gift'][$item]['itemcode'] != $code) {$giftcode_arr[$type]['gift'][$item]['itemcode'] = $code; $edit_flag = true;}
        		if($giftcode_arr[$type]['gift'][$item]['gcoin_km'] != $gcoin_km) {$giftcode_arr[$type]['gift'][$item]['gcoin_km'] = $gcoin_km; $edit_flag = true;}
                if($giftcode_arr[$type]['gift'][$item]['vpoint_km'] != $vpoint_km) {$giftcode_arr[$type]['gift'][$item]['vpoint_km'] = $vpoint_km; $edit_flag = true;}
                if($giftcode_arr[$type]['gift'][$item]['bank_zen'] != $bank_zen) {$giftcode_arr[$type]['gift'][$item]['bank_zen'] = $bank_zen; $edit_flag = true;}
                if($giftcode_arr[$type]['gift'][$item]['bank_chao'] != $bank_chao) {$giftcode_arr[$type]['gift'][$item]['bank_chao'] = $bank_chao; $edit_flag = true;}
                if($giftcode_arr[$type]['gift'][$item]['bank_cre'] != $bank_cre) {$giftcode_arr[$type]['gift'][$item]['bank_cre'] = $bank_cre; $edit_flag = true;}
                if($giftcode_arr[$type]['gift'][$item]['bank_blue'] != $bank_blue) {$giftcode_arr[$type]['gift'][$item]['bank_blue'] = $bank_blue; $edit_flag = true;}
                if($giftcode_arr[$type]['gift'][$item]['phucloi_point'] != $phucloi_point) {$giftcode_arr[$type]['gift'][$item]['phucloi_point'] = $phucloi_point; $edit_flag = true;}
                if($giftcode_arr[$type]['gift'][$item]['des'] != $des) {$giftcode_arr[$type]['gift'][$item]['des'] = $des; $edit_flag = true;}
                if($giftcode_arr[$type]['gift'][$item]['rate'] != $rate) {$giftcode_arr[$type]['gift'][$item]['rate'] = $rate; $edit_flag = true;}
        		
        		if($edit_flag === true) {
                    $giftcode_arr = _giftcode_calculator_rate($giftcode_arr, $type);
                        
                    $content = json_encode($giftcode_arr);
                    
                    replacecontent($file_edit,$content);
            		
            		include('config/config_sync.php');
        		    for($i=0; $i<count($url_hosting); $i++)
        		    {
        		        if($url_hosting[$i]) {
        		            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
        		            if($sync_send == 'OK') {
        		                
        		            } else {
        		                $err .= $sync_send;
        		            }
        		        }
        		    }
        		}
    		    
    			if($err) {
    		        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    		    } else {
    		    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    		    }
            }
    		break;
    	
    	case 'del':
    		$item = $_POST['item'];
    		
            unset($giftcode_arr[$type]['gift'][$item]);
            $giftcode_arr = _giftcode_calculator_rate($giftcode_arr, $type);
            $content = json_encode($giftcode_arr);
    		
    		replacecontent($file_edit,$content);
    		include('config/config_sync.php');
    	    for($i=0; $i<count($url_hosting); $i++)
    	    {
    	        if($url_hosting[$i]) {
    	            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
    	            if($sync_send == 'OK') {
    	                
    	            } else {
    	                $err .= $sync_send;
    	            }
    	        }
    	    }
    	    
    		if($err) {
    	        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    	    } else {
    	    	$notice = "<center><font color='blue'>Xóa thành công</font></center>";
    	    }
    		break;
    }
}
?>
		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình <a href="admin.php?mod=editevent&act=giftcode_random&type=<?php echo $type; ?>" target="_self"><?php echo $tilte; ?></a></h1>
			</div><br />
				Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php 
if($notice) echo $notice;

switch ($page)
{
	case 'type_create': 
		$content = "<center><b>Tạo phần thưởng GiftCode mới</b></center><br>
			<form id='editconfig' name='editconfig' method='post' action='admin.php?mod=editevent&act=giftcode_random&type=".$type."'>
			<input type='hidden' name='action' value='type_create'/>
			<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
				<tr bgcolor='#FFFFFF' >
					<td >Tên loại GiftCode</td>
					<td ><input type='text' name='type_name' value='' size='36' /></td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >&nbsp;</td>
					<td align='left'><input type='submit' name='Submit' value='Thêm phần thưởng GiftCode' ". $show_accept ." /></td>
				</tr>
			</table>
			</form>";
		echo $content;
		break;
	
	
    case 'add': 
        $rate = isset($_POST['rate']) ? $_POST['rate'] : 100;
		$content = "<center>Thêm phần thưởng <b>". $giftcode_arr[$type]['name'] ."</b></center><br>
			<form id='editconfig' name='editconfig' method='post' action='admin.php?mod=editevent&act=giftcode_random&type=".$type."&page=add'>
			<input type='hidden' name='action' value='add'/>
			<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >Mã Item</td>
					<td ><input type='text' name='code' value='". $_POST['code'] ."' size='80' /> <br />Mỗi Item là 32 mã ItemCode lấy từ MuMaker đặt liên tiếp nhau.</td>
				</tr>
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Gcent Khuyến mãi</td>
					<td ><input type='text' name='gcoin_km' value='". $_POST['gcoin_km'] ."' size='20' /></td>
				</tr>
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Vcent Event</td>
					<td ><input type='text' name='vpoint_km' value='". $_POST['vpoint_km'] ."' size='20' /></td>
				</tr>
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Zen ngân hàng</td>
					<td ><input type='text' name='bank_zen' value='". $_POST['bank_zen'] ."' size='20' /></td>
				</tr>
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Chao ngân hàng</td>
					<td ><input type='text' name='bank_chao' value='". $_POST['bank_chao'] ."' size='10' /></td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >Creation ngân hàng</td>
					<td ><input type='text' name='bank_cre' value='". $_POST['bank_cre'] ."' size='10' /></td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >Blue ngân hàng</td>
					<td ><input type='text' name='bank_blue' value='". $_POST['bank_blue'] ."' size='10' /></td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >PPoint +</td>
					<td ><input type='text' name='phucloi_point' value='". $_POST['phucloi_point'] ."' size='10' /></td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >Mổ tả phần thưởng</td>
					<td ><input type='text' name='des' value='". $_POST['des'] ."' size='30'/> (Chú giải về phần thưởng như : 10 viên Bless, 10 viên Soul)</td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >Tỷ lệ xuất hiện</td>
					<td ><input type='text' name='rate' value='". $rate ."' size='5'/> (Tỷ lệ càng cao, khả năng ra phần thường này càng lớn)</td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >&nbsp;</td>
					<td align='center'><input type='submit' name='Submit' value='Thêm phần thưởng' ". $show_accept ." /></td>
				</tr>
			</table>
			</form>";
		echo $content;
		break;
	
	case 'edit': 
		$item = $_GET['item'];
		$content = "<center><b>Sửa Phần thưởng</b></center><br>
			<form id='editconfig' name='editconfig' method='post' action='admin.php?mod=editevent&act=giftcode_random&type=".$type."&page=edit&item=".$item."'>
			<input type='hidden' name='action' value='edit'/>
			<input type='hidden' name='item' value='".$item."'/>
			<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >Mã Item</td>
					<td ><input type='text' name='code' value='". $giftcode_arr[$type]['gift'][$item]['itemcode'] ."' size='80' /> <br />Mỗi Item là 32 mã ItemCode lấy từ MuMaker đặt liên tiếp nhau.</td>
				</tr>
                
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Gcent Khuyến mãi</td>
					<td ><input type='text' name='gcoin_km' value='". $giftcode_arr[$type]['gift'][$item]['gcoin_km'] ."' size='20' /></td>
				</tr>
                
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Vcent Event</td>
					<td ><input type='text' name='vpoint_km' value='". $giftcode_arr[$type]['gift'][$item]['vpoint_km'] ."' size='20' /></td>
				</tr>
                
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Zen ngân hàng</td>
					<td ><input type='text' name='bank_zen' value='". $giftcode_arr[$type]['gift'][$item]['bank_zen'] ."' size='20' /></td>
				</tr>
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Chao ngân hàng</td>
					<td ><input type='text' name='bank_chao' value='". $giftcode_arr[$type]['gift'][$item]['bank_chao'] ."' size='10' /></td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >Creation ngân hàng</td>
					<td ><input type='text' name='bank_cre' value='". $giftcode_arr[$type]['gift'][$item]['bank_cre'] ."' size='10' /></td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >Blue ngân hàng</td>
					<td ><input type='text' name='bank_blue' value='". $giftcode_arr[$type]['gift'][$item]['bank_blue'] ."' size='10' /></td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >PPoint +</td>
					<td ><input type='text' name='phucloi_point' value='". $giftcode_arr[$type]['gift'][$item]['phucloi_point'] ."' size='10' /></td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >Mổ tả phần thưởng</td>
					<td ><input type='text' name='des' value='". $giftcode_arr[$type]['gift'][$item]['des'] ."' size='30'/> (Chú giải về phần thưởng như : 10 viên Bless, 10 viên Soul)</td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >Tỷ lệ xuất hiện</td>
					<td ><input type='text' name='rate' value='". $giftcode_arr[$type]['gift'][$item]['rate'] ."' size='5'/> (Tỷ lệ càng cao, khả năng ra phần thường này càng lớn)</td>
				</tr>
				
				<tr bgcolor='#FFFFFF' >
					<td >&nbsp;</td>
					<td align='center'><input type='submit' name='Submit' value='Sửa' ". $show_accept ." /></td>
				</tr>
			</table>
			</form>";
		echo $content;
		break;
	
	case 'del':
		$item = $_GET['item'];
		$content = "<center><b>Xóa phần thưởng</b></center><br>
			<form id='delitem' name='delitem' method='post' action='admin.php?mod=editevent&act=giftcode_random&type=".$type."'>
			<input type='hidden' name='action' value='del'/>
			<input type='hidden' name='item' value='".$item."'/>
			<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >Mã Item</td>
					<td >". $giftcode_arr[$type]['gift'][$item]['itemcode'] ."</td>
				</tr>
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Gcent Khuyến mãi</td>
					<td >". $giftcode_arr[$type]['gift'][$item]['gcoin_km'] ."</td>
				</tr>
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Vcent Event</td>
					<td >". $giftcode_arr[$type]['gift'][$item]['vpoint_km'] ."</td>
				</tr>
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Zen ngân hàng</td>
					<td >". $giftcode_arr[$type]['gift'][$item]['bank_zen'] ."</td>
				</tr>
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Chao ngân hàng</td>
					<td >". $giftcode_arr[$type]['gift'][$item]['bank_chao'] ."</td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >Creation ngân hàng</td>
					<td >". $giftcode_arr[$type]['gift'][$item]['bank_cre'] ."</td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >Blue ngân hàng</td>
					<td >". $giftcode_arr[$type]['gift'][$item]['bank_blue'] ."</td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >Điểm Phúc Lợi</td>
					<td >". $giftcode_arr[$type]['gift'][$item]['phucloi_point'] ."</td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >Mổ tả phần thưởng</td>
					<td >". $giftcode_arr[$type]['gift'][$item]['des'] ."</td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >Tỷ lệ xuất hiện</td>
					<td >". $giftcode_arr[$type]['gift'][$item]['rate'] ."</td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >&nbsp;</td>
					<td align='left'><input type='submit' name='Submit' value='Xóa' ". $show_accept ." /></td>
				</tr>
			</table>
			</form>";
		echo $content;
		
		break;	
		
	default: 
		echo "<b>Tỷ lệ xuất hiện</b>: Tỷ lệ này càng cao thì khả năng ra đồ càng nhiều.<br>
				Phần thưởng quý nên để tỷ lệ xuất hiện thấp.<br /><br />
        ";
        echo "<a href='admin.php?mod=editevent&act=giftcode_random&page=type_create'>Tạo phần thưởng Giftcode mới</a><br />";
        echo "<form action='admin.php' method='GET'>";
        echo "<input type='hidden' name='mod' value='editevent' />";
        echo "<input type='hidden' name='act' value='giftcode_random' />";
        echo "<select name='type' onchange='this.form.submit()'>";
        echo "<option value=''>Chọn phần thưởng GiftCode</option>";
        if(is_array($giftcode_arr)) {
            foreach($giftcode_arr as $k => $v) {
                echo "<option value='$k' ";
                if(strlen($type)>0 && $type==$k) echo "selected";
                echo " >". $v['name'] ."</option>";
            }
        }
        echo "</select>";
        echo "</form>";
        echo "<br />";
        if(strlen($type) > 0) {
            echo "<div align='right'><a href='admin.php?mod=editevent&act=giftcode_random&type=".$type."&page=add'>+ Thêm phần thưởng</a></div><br>";
            _giftcode_display($giftcode_arr,$type); 
        }
        
		
		break;
	
}

?>
				
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
