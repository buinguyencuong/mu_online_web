<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/config_event2.txt';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

require_once('admin_cfg/function.php');
// Read Event2 CFG
$event2 = _event2_loadcfg($file_edit);
// Read Event2 CFG End

$action = $_POST[action];

if($action == 'edit')
{
	$event2 = $_POST['event2'];
    
    $err = '';
    
    for($i=0; $i <= 3; $i++) {
        if (!preg_match("/^[A-F0-9]*$/i", $event2['item_source'][$i]['code']))
    	{
            $err .= "Dữ liệu lỗi <strong>Mã nguyên liệu</strong> : ". $event2['item_source'][$i]['code'] ." . Chỉ được sử dụng ký tự a-f, A-F, so (0-9).<br />"; 
    	}
        if( strlen($event2['item_source'][$i]['code']) != 32 ) {
            $err .= "Dữ liệu lỗi <strong>Mã nguyên liệu</strong> : ". $event2['item_source'][$i]['code'] ." phải có 32 ký tự.<br />";
        }
    }
    
    if(strlen($err) == 0) {
        include_once('config_license.php');
        include_once('func_getContent.php');
        $getcontent_url = $url_license . "/api_event2.php";
        $getcontent_data = array(
            'acclic'    =>  $acclic,
            'key'    =>  $key,
            'action'    =>  'get_source_info',
            
            'item_source_code0'  =>  $event2['item_source'][0]['code'],
            'item_source_code1'  =>  $event2['item_source'][1]['code'],
            'item_source_code2'  =>  $event2['item_source'][2]['code'],
            'item_source_code3'  =>  $event2['item_source'][3]['code']
        ); 
        
        $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
        
        if ( empty($reponse) ) {
            echo "Kết nối đến API bị gián đoạn.";
            exit();
        }
        else {
            $info = read_TagName($reponse, 'info');
            if($info == "Error") {
                echo read_TagName($reponse, 'message');
                exit();
            } elseif ($info == "OK") {
                $item_source_data = read_TagName($reponse, 'item_source_data');
                
                if(strlen($item_source_data) == 0 ) {
                    echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                    
                    $arr_view = "\nDataSend:\n";
                    foreach($getcontent_data as $k => $v) {
                        $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                    }
                    writelog("log_api.txt", $arr_view . $reponse);
                    exit();
                }
            } else {
                echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
                writelog("log_api.txt", $reponse);
                exit();
            }
        }
        
        $item_source_arr = json_decode($item_source_data, true);
        $event2['item_source'][0]['name'] = $item_source_arr[0]['name'];
        $event2['item_source'][0]['img'] = $item_source_arr[0]['img'];
        $event2['item_source'][0]['group'] = $item_source_arr[0]['group'];
        $event2['item_source'][0]['id'] = $item_source_arr[0]['id'];
        $event2['item_source'][0]['level'] = $item_source_arr[0]['level'];
        
        $event2['item_source'][1]['name'] = $item_source_arr[1]['name'];
        $event2['item_source'][1]['img'] = $item_source_arr[1]['img'];
        $event2['item_source'][1]['group'] = $item_source_arr[1]['group'];
        $event2['item_source'][1]['id'] = $item_source_arr[1]['id'];
        $event2['item_source'][1]['level'] = $item_source_arr[1]['level'];
        
        $event2['item_source'][2]['name'] = $item_source_arr[2]['name'];
        $event2['item_source'][2]['img'] = $item_source_arr[2]['img'];
        $event2['item_source'][2]['group'] = $item_source_arr[2]['group'];
        $event2['item_source'][2]['id'] = $item_source_arr[2]['id'];
        $event2['item_source'][2]['level'] = $item_source_arr[2]['level'];
        
        $event2['item_source'][3]['name'] = $item_source_arr[3]['name'];
        $event2['item_source'][3]['img'] = $item_source_arr[3]['img'];
        $event2['item_source'][3]['group'] = $item_source_arr[3]['group'];
        $event2['item_source'][3]['id'] = $item_source_arr[3]['id'];
        $event2['item_source'][3]['level'] = $item_source_arr[3]['level'];
    }
    
    $content = json_encode($event2);
                    
    replacecontent($file_edit,$content);
	
	include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
	
	
	replacecontent($file_edit,$content);
	
	include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}

?>
		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình Event Item 3 màu</h1>
			</div><br>
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				<form id="editconfig" name="editconfig" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
				<table>
					<tr><td colspan="2"><b>Mô tả sự kiện</b><br>
					    - Trong quá trình đánh quái, người chơi sẽ nhặt được <?php echo $event2['item_source'][1]['name']; ?>, <?php echo $event2['item_source'][2]['name']; ?> và <?php echo $event2['item_source'][3]['name']; ?><br />
					    - Sử dụng <?php echo $event1_itemdrop1_name; ?>, <?php echo $event1_itemdrop2_name; ?> để đổi lấy phần thưởng sự kiện.<br />
					</td>
					</tr>
					
					<tr>
						<td width='130' align='right'>Mã Item màu 1: </td>
						<td>
                            <input type="text" name="event2[item_source][1][code]" value="<?php echo $event2['item_source'][1]['code']; ?>" size="40" maxlength="32"/> (32 mã lấy từ MuMaker)<br />
                            <?php echo $event2['item_source'][1]['name']. " <img src='items/". $event2['item_source'][1]['img'] .".gif' border='0' /> | Group : ". $event2['item_source'][1]['group'] ." | ID : ". $event2['item_source'][1]['id'] ." | Level : ". $event2['item_source'][1]['level']; ?> 
                            
                        </td>
					</tr>
					<tr><td colspan="2"><hr /></td></tr>
					
					<tr>
						<td align='right'>Mã Item màu 2: </td>
						<td>
                            <input type="text" name="event2[item_source][2][code]" value="<?php echo $event2['item_source'][2]['code']; ?>" size="40" maxlength="32"/> (32 mã lấy từ MuMaker)<br />
                            <?php echo $event2['item_source'][2]['name']. " <img src='items/". $event2['item_source'][2]['img'] .".gif' border='0' /> | Group : ". $event2['item_source'][2]['group'] ." | ID : ". $event2['item_source'][2]['id'] ." | Level : ". $event2['item_source'][2]['level']; ?>
                        </td>
					</tr>
					<tr><td colspan="2"><hr /></td></tr>
                    
                    <tr>
						<td align='right'>Mã Item màu 3: </td>
						<td>
                            <input type="text" name="event2[item_source][3][code]" value="<?php echo $event2['item_source'][3]['code']; ?>" size="40" maxlength="32"/> (32 mã lấy từ MuMaker)<br />
                            <?php echo $event2['item_source'][3]['name']. " <img src='items/". $event2['item_source'][3]['img'] .".gif' border='0' /> | Group : ". $event2['item_source'][3]['group'] ." | ID : ". $event2['item_source'][3]['id'] ." | Level : ". $event2['item_source'][3]['level']; ?>
                        </td>
					</tr>
					<tr><td colspan="2"><hr /></td></tr>
					
					<tr>
						<td align='right'>Mã Item đổi kèm ở Loại 2 : </td>
						<td>
                            <input type="text" name="event2[item_source][0][code]" value="<?php echo $event2['item_source'][0]['code']; ?>" size="40" maxlength="32"/> (32 mã lấy từ MuMaker)<br />
                            <?php echo $event2['item_source'][0]['name']. " <img src='items/". $event2['item_source'][0]['img'] .".gif' border='0' /> | Group : ". $event2['item_source'][0]['group'] ." | ID : ". $event2['item_source'][0]['id'] ." | Level : ". $event2['item_source'][0]['level']; ?>
                        </td>
					</tr>
					<tr><td colspan="2"><hr></td></tr>
					
					<tr><td colspan="2"><b>Công thức đổi phần thưởng</b> :<br />
    					<b>- Phần thưởng loại 1</b> : ZEN <br />
                        Đổi tối đa trong ngày : <input type="text" name="event2[type1][daily_slg]" value="<?php echo $event2['type1']['daily_slg']; ?>" size="2"/> phần thưởng<br>
                        Đổi tối đa toàn Event : <input type="text" name="event2[type1][all_slg]" value="<?php echo $event2['type1']['all_slg']; ?>" size="2"/> phần thưởng
                        
                        <table width="100%" border="0" bgcolor="#9999FF">
                            <thead>
                                <tr bgcolor="#FFFFFF">
                                    <th align="center" rowspan="2"><b>Nguyên Liệu</b></th>
                                    <th align="center" rowspan="2"><b>ZEN</b></th>
                                    <th align="center" colspan="11"><b>Phần thưởng</b></th>
                                </tr>
                                <tr bgcolor="#FFFFFF">
                                    <th align="center"><b>Gc+</b></th>
                                    <th align="center"><b>Vc+</b></th>
                                    <th align="center"><b>Pp</b></th>
                                    <th align="center"><b>Pp+</b></th>
                                    <th align="center"><b>PcP</b></th>
                                    <th align="center"><b>Wc</b></th>
                                    <th align="center"><b>Chao</b></th>
                                    <th align="center"><b>Cre</b></th>
                                    <th align="center"><b>Blue</b></th>
                                    <th align="center"><b>Exp(p)</b></th>
                                    <th align="center"><b>PMaster</b></th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <tr bgcolor="#FFFFFF">
                                    <td align="center"><b>Màu 1</b></td>
                                    <td align="center"><input type="text" name="event2[type1][0][money]" value="<?php echo $event2['type1'][0]['money']; ?>" size="8"/></td>
                                    <td align="center"><input type="text" name="event2[type1][0][gc_extra]" value="<?php echo $event2['type1'][0]['gc_extra']; ?>" size="3"/></td>
                                    <td align="center"><input type="text" name="event2[type1][0][vc_extra]" value="<?php echo $event2['type1'][0]['vc_extra']; ?>" size="3"/></td>
                                    <td align="center"><input type="text" name="event2[type1][0][pp]" value="<?php echo $event2['type1'][0]['pp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][0][pp_extra]" value="<?php echo $event2['type1'][0]['pp_extra']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][0][pcp]" value="<?php echo $event2['type1'][0]['pcp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][0][wc]" value="<?php echo $event2['type1'][0]['wc']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][0][chao]" value="<?php echo $event2['type1'][0]['chao']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][0][cre]" value="<?php echo $event2['type1'][0]['cre']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][0][blue]" value="<?php echo $event2['type1'][0]['blue']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][0][exp]" value="<?php echo $event2['type1'][0]['exp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][0][pmaster]" value="<?php echo $event2['type1'][0]['pmaster']; ?>" size="1"/></td>
                                </tr>
                                <tr bgcolor="#FFFFFF">
                                    <td align="center"><b>Màu 1+2</b></td>
                                    <td align="center"><input type="text" name="event2[type1][1][money]" value="<?php echo $event2['type1'][1]['money']; ?>" size="8"/></td>
                                    <td align="center"><input type="text" name="event2[type1][1][gc_extra]" value="<?php echo $event2['type1'][1]['gc_extra']; ?>" size="3"/></td>
                                    <td align="center"><input type="text" name="event2[type1][1][vc_extra]" value="<?php echo $event2['type1'][1]['vc_extra']; ?>" size="3"/></td>
                                    <td align="center"><input type="text" name="event2[type1][1][pp]" value="<?php echo $event2['type1'][1]['pp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][1][pp_extra]" value="<?php echo $event2['type1'][1]['pp_extra']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][1][pcp]" value="<?php echo $event2['type1'][1]['pcp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][1][wc]" value="<?php echo $event2['type1'][1]['wc']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][1][chao]" value="<?php echo $event2['type1'][1]['chao']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][1][cre]" value="<?php echo $event2['type1'][1]['cre']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][1][blue]" value="<?php echo $event2['type1'][1]['blue']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][1][exp]" value="<?php echo $event2['type1'][1]['exp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][1][pmaster]" value="<?php echo $event2['type1'][1]['pmaster']; ?>" size="1"/></td>
                                </tr>
                                <tr bgcolor="#FFFFFF">
                                    <td align="center"><b>Màu 1+2+3</b></td>
                                    <td align="center"><input type="text" name="event2[type1][2][money]" value="<?php echo $event2['type1'][2]['money']; ?>" size="8"/></td>
                                    <td align="center"><input type="text" name="event2[type1][2][gc_extra]" value="<?php echo $event2['type1'][2]['gc_extra']; ?>" size="3"/></td>
                                    <td align="center"><input type="text" name="event2[type1][2][vc_extra]" value="<?php echo $event2['type1'][2]['vc_extra']; ?>" size="3"/></td>
                                    <td align="center"><input type="text" name="event2[type1][2][pp]" value="<?php echo $event2['type1'][2]['pp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][2][pp_extra]" value="<?php echo $event2['type1'][2]['pp_extra']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][2][pcp]" value="<?php echo $event2['type1'][2]['pcp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][2][wc]" value="<?php echo $event2['type1'][2]['wc']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][2][chao]" value="<?php echo $event2['type1'][2]['chao']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][2][cre]" value="<?php echo $event2['type1'][2]['cre']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][2][blue]" value="<?php echo $event2['type1'][2]['blue']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][2][exp]" value="<?php echo $event2['type1'][2]['exp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type1][2][pmaster]" value="<?php echo $event2['type1'][2]['pmaster']; ?>" size="1"/></td>
                                </tr>
                            </tbody>
                        </table>
    					
    					<br /><br />
                        
                        <b>- Phần thưởng loại 2</b> : Vcent <br />
                        Đổi tối đa trong ngày : <input type="text" name="event2[type2][daily_slg]" value="<?php echo $event2['type2']['daily_slg']; ?>" size="2"/> phần thưởng<br>
                        Đổi tối đa toàn Event : <input type="text" name="event2[type2][all_slg]" value="<?php echo $event2['type2']['all_slg']; ?>" size="2"/> phần thưởng
                        
                        <table width="100%" border="0" bgcolor="#9999FF">
                            <thead>
                                <tr bgcolor="#FFFFFF">
                                    <th align="center" rowspan="2"><b>Nguyên Liệu</b></th>
                                    <th align="center" rowspan="2"><b>Item kèm</b></th>
                                    <th align="center" colspan="11"><b>Phần thưởng</b></th>
                                </tr>
                                <tr bgcolor="#FFFFFF">
                                    <th align="center"><b>Gc+</b></th>
                                    <th align="center"><b>Vc+</b></th>
                                    <th align="center"><b>Pp</b></th>
                                    <th align="center"><b>Pp+</b></th>
                                    <th align="center"><b>PcP</b></th>
                                    <th align="center"><b>Wc</b></th>
                                    <th align="center"><b>Chao</b></th>
                                    <th align="center"><b>Cre</b></th>
                                    <th align="center"><b>Blue</b></th>
                                    <th align="center"><b>Exp(p)</b></th>
                                    <th align="center"><b>PMaster</b></th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <tr bgcolor="#FFFFFF">
                                    <td align="center"><b>Màu 1</b></td>
                                    <td align="center"><input type="text" name="event2[type2][0][money]" value="<?php echo $event2['type2'][0]['money']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][0][gc_extra]" value="<?php echo $event2['type2'][0]['gc_extra']; ?>" size="3"/></td>
                                    <td align="center"><input type="text" name="event2[type2][0][vc_extra]" value="<?php echo $event2['type2'][0]['vc_extra']; ?>" size="3"/></td>
                                    <td align="center"><input type="text" name="event2[type2][0][pp]" value="<?php echo $event2['type2'][0]['pp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][0][pp_extra]" value="<?php echo $event2['type2'][0]['pp_extra']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][0][pcp]" value="<?php echo $event2['type2'][0]['pcp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][0][wc]" value="<?php echo $event2['type2'][0]['wc']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][0][chao]" value="<?php echo $event2['type2'][0]['chao']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][0][cre]" value="<?php echo $event2['type2'][0]['cre']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][0][blue]" value="<?php echo $event2['type2'][0]['blue']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][0][exp]" value="<?php echo $event2['type2'][0]['exp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][0][pmaster]" value="<?php echo $event2['type2'][0]['pmaster']; ?>" size="1"/></td>
                                </tr>
                                <tr bgcolor="#FFFFFF">
                                    <td align="center"><b>Màu 1+2</b></td>
                                    <td align="center"><input type="text" name="event2[type2][1][money]" value="<?php echo $event2['type2'][1]['money']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][1][gc_extra]" value="<?php echo $event2['type2'][1]['gc_extra']; ?>" size="3"/></td>
                                    <td align="center"><input type="text" name="event2[type2][1][vc_extra]" value="<?php echo $event2['type2'][1]['vc_extra']; ?>" size="3"/></td>
                                    <td align="center"><input type="text" name="event2[type2][1][pp]" value="<?php echo $event2['type2'][1]['pp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][1][pp_extra]" value="<?php echo $event2['type2'][1]['pp_extra']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][1][pcp]" value="<?php echo $event2['type2'][1]['pcp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][1][wc]" value="<?php echo $event2['type2'][1]['wc']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][1][chao]" value="<?php echo $event2['type2'][1]['chao']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][1][cre]" value="<?php echo $event2['type2'][1]['cre']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][1][blue]" value="<?php echo $event2['type2'][1]['blue']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][1][exp]" value="<?php echo $event2['type2'][1]['exp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][1][pmaster]" value="<?php echo $event2['type2'][1]['pmaster']; ?>" size="1"/></td>
                                </tr>
                                <tr bgcolor="#FFFFFF">
                                    <td align="center"><b>Màu 1+2+3</b></td>
                                    <td align="center"><input type="text" name="event2[type2][2][money]" value="<?php echo $event2['type2'][2]['money']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][2][gc_extra]" value="<?php echo $event2['type2'][2]['gc_extra']; ?>" size="3"/></td>
                                    <td align="center"><input type="text" name="event2[type2][2][vc_extra]" value="<?php echo $event2['type2'][2]['vc_extra']; ?>" size="3"/></td>
                                    <td align="center"><input type="text" name="event2[type2][2][pp]" value="<?php echo $event2['type2'][2]['pp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][2][pp_extra]" value="<?php echo $event2['type2'][2]['pp_extra']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][2][pcp]" value="<?php echo $event2['type2'][2]['pcp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][2][wc]" value="<?php echo $event2['type2'][2]['wc']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][2][chao]" value="<?php echo $event2['type2'][2]['chao']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][2][cre]" value="<?php echo $event2['type2'][2]['cre']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][2][blue]" value="<?php echo $event2['type2'][2]['blue']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][2][exp]" value="<?php echo $event2['type2'][2]['exp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type2][2][pmaster]" value="<?php echo $event2['type2'][2]['pmaster']; ?>" size="1"/></td>
                                </tr>
                            </tbody>
                        </table>
    					
    					<br /><br />
                        
                        <b>- Phần thưởng loại 3</b> : Gcent <br />
                        Đổi tối đa trong ngày : <input type="text" name="event2[type3][daily_slg]" value="<?php echo $event2['type3']['daily_slg']; ?>" size="2"/> phần thưởng<br>
                        Đổi tối đa toàn Event : <input type="text" name="event2[type3][all_slg]" value="<?php echo $event2['type3']['all_slg']; ?>" size="2"/> phần thưởng
                        
                        <table width="100%" border="0" bgcolor="#9999FF">
                            <thead>
                                <tr bgcolor="#FFFFFF">
                                    <th align="center" rowspan="2"><b>Nguyên Liệu</b></th>
                                    <th align="center" rowspan="2"><b>Gcent</b></th>
                                    <th align="center" colspan="11"><b>Phần thưởng</b></th>
                                </tr>
                                <tr bgcolor="#FFFFFF">
                                    <th align="center"><b>Gc+</b></th>
                                    <th align="center"><b>Vc+</b></th>
                                    <th align="center"><b>Pp</b></th>
                                    <th align="center"><b>Pp+</b></th>
                                    <th align="center"><b>PcP</b></th>
                                    <th align="center"><b>Wc</b></th>
                                    <th align="center"><b>Chao</b></th>
                                    <th align="center"><b>Cre</b></th>
                                    <th align="center"><b>Blue</b></th>
                                    <th align="center"><b>Exp(p)</b></th>
                                    <th align="center"><b>PMaster</b></th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <tr bgcolor="#FFFFFF">
                                    <td align="center"><b>Màu 1</b></td>
                                    <td align="center"><input type="text" name="event2[type3][0][money]" value="<?php echo $event2['type3'][0]['money']; ?>" size="8"/></td>
                                    <td align="center"><input type="text" name="event2[type3][0][gc_extra]" value="<?php echo $event2['type3'][0]['gc_extra']; ?>" size="3"/></td>
                                    <td align="center"><input type="text" name="event2[type3][0][vc_extra]" value="<?php echo $event2['type3'][0]['vc_extra']; ?>" size="3"/></td>
                                    <td align="center"><input type="text" name="event2[type3][0][pp]" value="<?php echo $event2['type3'][0]['pp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][0][pp_extra]" value="<?php echo $event2['type3'][0]['pp_extra']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][0][pcp]" value="<?php echo $event2['type3'][0]['pcp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][0][wc]" value="<?php echo $event2['type3'][0]['wc']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][0][chao]" value="<?php echo $event2['type3'][0]['chao']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][0][cre]" value="<?php echo $event2['type3'][0]['cre']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][0][blue]" value="<?php echo $event2['type3'][0]['blue']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][0][exp]" value="<?php echo $event2['type3'][0]['exp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][0][pmaster]" value="<?php echo $event2['type3'][0]['pmaster']; ?>" size="1"/></td>
                                </tr>
                                <tr bgcolor="#FFFFFF">
                                    <td align="center"><b>Màu 1+2</b></td>
                                    <td align="center"><input type="text" name="event2[type3][1][money]" value="<?php echo $event2['type3'][1]['money']; ?>" size="8"/></td>
                                    <td align="center"><input type="text" name="event2[type3][1][gc_extra]" value="<?php echo $event2['type3'][1]['gc_extra']; ?>" size="3"/></td>
                                    <td align="center"><input type="text" name="event2[type3][1][vc_extra]" value="<?php echo $event2['type3'][1]['vc_extra']; ?>" size="3"/></td>
                                    <td align="center"><input type="text" name="event2[type3][1][pp]" value="<?php echo $event2['type3'][1]['pp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][1][pp_extra]" value="<?php echo $event2['type3'][1]['pp_extra']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][1][pcp]" value="<?php echo $event2['type3'][1]['pcp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][1][wc]" value="<?php echo $event2['type3'][1]['wc']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][1][chao]" value="<?php echo $event2['type3'][1]['chao']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][1][cre]" value="<?php echo $event2['type3'][1]['cre']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][1][blue]" value="<?php echo $event2['type3'][1]['blue']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][1][exp]" value="<?php echo $event2['type3'][1]['exp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][1][pmaster]" value="<?php echo $event2['type3'][1]['pmaster']; ?>" size="1"/></td>
                                </tr>
                                <tr bgcolor="#FFFFFF">
                                    <td align="center"><b>Màu 1+2+3</b></td>
                                    <td align="center"><input type="text" name="event2[type3][2][money]" value="<?php echo $event2['type3'][2]['money']; ?>" size="8"/></td>
                                    <td align="center"><input type="text" name="event2[type3][2][gc_extra]" value="<?php echo $event2['type3'][2]['gc_extra']; ?>" size="3"/></td>
                                    <td align="center"><input type="text" name="event2[type3][2][vc_extra]" value="<?php echo $event2['type3'][2]['vc_extra']; ?>" size="3"/></td>
                                    <td align="center"><input type="text" name="event2[type3][2][pp]" value="<?php echo $event2['type3'][2]['pp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][2][pp_extra]" value="<?php echo $event2['type3'][2]['pp_extra']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][2][pcp]" value="<?php echo $event2['type3'][2]['pcp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][2][wc]" value="<?php echo $event2['type3'][2]['wc']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][2][chao]" value="<?php echo $event2['type3'][2]['chao']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][2][cre]" value="<?php echo $event2['type3'][2]['cre']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][2][blue]" value="<?php echo $event2['type3'][2]['blue']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][2][exp]" value="<?php echo $event2['type3'][2]['exp']; ?>" size="1"/></td>
                                    <td align="center"><input type="text" name="event2[type3][2][pmaster]" value="<?php echo $event2['type3'][2]['pmaster']; ?>" size="1"/></td>
                                </tr>
                            </tbody>
                        </table>
    					
    					<br /><br />
                        
                        
                        
					</td></tr>
					
					
					<tr>
						<td>&nbsp;</td>
						<td align="center"><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></td>
					</tr>
				</table>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
