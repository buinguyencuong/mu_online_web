<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/

$file_edit = 'config/event_itemfind.txt';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

require_once('admin_cfg/function.php');
// Read Config
$itemfind_arr = _json_fileload($file_edit);
// Read Config End

if($accept==0) $show_accept = "disabled='disabled'";
else $show_accept = "";

$page = $_GET['page'];


if($_POST) {
    $action = $_POST[action];
    
    switch ($action) {
        
        case 'add':
    		$itemfind_name = $_POST['itemfind_name'];
            $itemfind_stat = $_POST['itemfind_stat'];
            $itemfind_item_source_slg = $_POST['itemfind_item_source_slg'];
            $itemfind_item_source = $_POST['itemfind_item_source'];
            
    
            $error = "";
            if( strlen($itemfind_name) <= 5 ) {
                $error .= "Dữ liệu lỗi <strong>Tên công thức</strong> : '$itemfind_name' chưa rõ ràng.<br />";
            }
            if( $itemfind_item_source_slg <= 0 ) {
                $error .= "Dữ liệu lỗi <strong>Số loại Nguyên Liệu</strong> : '$itemfind_item_source_slg' phải lớn hơn 0.<br />";
            }
            
            for($i=0; $i<$itemfind_item_source_slg; $i++) {
                $stt = $i+1;
                $itemfind_item_source[$i]['slg'] = abs(intval($itemfind_item_source[$i]['slg']));
                
                if( strlen($itemfind_item_source[$i]['name']) <= 5 ) {
                    $error .= "Dữ liệu lỗi <strong>Mô tả nguyên liệu $stt</strong> : '". $itemfind_item_source[$i]['name'] ."' chưa rõ ràng.<br />";
                }
                if (!preg_match("/^[A-F0-9]*$/i", $itemfind_item_source[$i]['code']))
            	{
                    $error .= "Dữ liệu lỗi <strong>Item nguyên liệu $stt</strong> : '". $itemfind_item_source[$i]['code'] ."' chỉ được sử dụng ký tự a-f, A-F, so (0-9).<br />"; 
            	}
                if( strlen($itemfind_item_source[$i]['code']) != 32 ) {
                    $error .= "Dữ liệu lỗi <strong>Item nguyên liệu $stt</strong> : '". $itemfind_item_source[$i]['code'] ."' phải có 32 ký tự.<br />";
                }
                if( $itemfind_item_source[$i]['slg'] <= 0 ) {
                    $error .= "Dữ liệu lỗi <strong>Số lượng Nguyên Liệu $stt</strong> : '". $itemfind_item_source[$i]['slg'] ."' phải lớn hơn 0.<br />";
                }
            }
            
            if(strlen($error) > 0) {
                $notice = "<center><b><font color='red'>" . $error . "</font></b></center>";
            } else {
                
                include_once('config_license.php');
                include_once('func_getContent.php');
                $getcontent_url = $url_license . "/api_itemfind.php";
                $getcontent_data = array(
                    'acclic'    =>  $acclic,
                    'key'    =>  $key,
                    'action'    =>  'itemfind_getimg',
                    
                    'itemfind_item_source_slg'  =>  $itemfind_item_source_slg,
                    'itemfind_item_source'  =>  $itemfind_item_source
                ); 
                
                $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
                
                if ( empty($reponse) ) {
                    echo "Kết nối đến API bị gián đoạn.";
                    exit();
                }
                else {
                    $info = read_TagName($reponse, 'info');
                    if($info == "Error") {
                        echo read_TagName($reponse, 'message');
                        exit();
                    } elseif ($info == "OK") {
                        $itemfind_item_source_data = read_TagName($reponse, 'itemfind_item_source_data');
                        
                        if(strlen($itemfind_item_source_data) == 0) {
                            echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                            
                            $arr_view = "\nDataSend:\n";
                            foreach($getcontent_data as $k => $v) {
                                $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                            }
                            writelog("log_api.txt", $arr_view . $reponse);
                            exit();
                        }
                    } else {
                        echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
                        writelog("log_api.txt", $reponse);
                        exit();
                    }
                }
                
                $itemfind_item_source_arr = json_decode($itemfind_item_source_data, true);
                for($i=0; $i<$itemfind_item_source_slg; $i++) {
                    $itemfind_item_source[$i]['des'] = $itemfind_item_source_arr[$i]['name'];
                    $itemfind_item_source[$i]['img'] = $itemfind_item_source_arr[$i]['img'];
                }
                
                $itemfind_arr[] = array(
                    'name'  =>  $itemfind_name,
                    'itemfind_stat'   =>  $itemfind_stat,
                    'itemfind_item_source_slg'  =>  $itemfind_item_source_slg,
                    'itemfind_item_source'  =>  $itemfind_item_source
                );
                
                $content = json_encode($itemfind_arr);
                
        		replacecontent($file_edit,$content);
        		
        		include('config/config_sync.php');
    		    for($i=0; $i<count($url_hosting); $i++)
    		    {
    		        if($url_hosting[$i]) {
    		            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
    		            if($sync_send == 'OK') {
    		                
    		            } else {
    		                $err .= $sync_send;
    		            }
    		        }
    		    }
    		    
    			if($err) {
    		        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    		    } else {
    		    	$notice = "<center><font color='blue'>Thêm thành công</font></center>";
    		    }
            }
            
    		break;
    	
    	case 'edit':
    		$item = $_POST['item'];
    		$itemfind_name = $_POST['itemfind_name'];
            $itemfind_stat = $_POST['itemfind_stat'];
            $itemfind_item_source_slg = abs(intval($_POST['itemfind_item_source_slg']));
            $itemfind_item_source = $_POST['itemfind_item_source'];
            
    
            $error = "";
            if( strlen($itemfind_name) <= 5 ) {
                $error .= "Dữ liệu lỗi <strong>Tên công thức</strong> : '$itemfind_name' chưa rõ ràng.<br />";
            }
            if( $itemfind_item_source_slg <= 0 ) {
                $error .= "Dữ liệu lỗi <strong>Số loại Nguyên Liệu</strong> : '$itemfind_item_source_slg' phải lớn hơn 0.<br />";
            }
            
            for($i=0; $i<$itemfind_item_source_slg; $i++) {
                $stt = $i+1;
                $itemfind_item_source[$i]['slg'] = abs(intval($itemfind_item_source[$i]['slg']));
                
                if( strlen($itemfind_item_source[$i]['name']) <= 5 ) {
                    $error .= "Dữ liệu lỗi <strong>Mô tả nguyên liệu $stt</strong> : '". $itemfind_item_source[$i]['name'] ."' chưa rõ ràng.<br />";
                }
                if (!preg_match("/^[A-F0-9]*$/i", $itemfind_item_source[$i]['code']))
            	{
                    $error .= "Dữ liệu lỗi <strong>Item nguyên liệu $stt</strong> : '". $itemfind_item_source[$i]['code'] ."' chỉ được sử dụng ký tự a-f, A-F, so (0-9).<br />"; 
            	}
                if( strlen($itemfind_item_source[$i]['code']) != 32 ) {
                    $error .= "Dữ liệu lỗi <strong>Item nguyên liệu $stt</strong> : '". $itemfind_item_source[$i]['code'] ."' phải có 32 ký tự.<br />";
                }
                if( $itemfind_item_source[$i]['slg'] <= 0 ) {
                    $error .= "Dữ liệu lỗi <strong>Số lượng Nguyên Liệu $stt</strong> : '". $itemfind_item_source[$i]['slg'] ."' phải lớn hơn 0.<br />";
                }
            }
            
            if(strlen($error) > 0) {
                $notice = "<center><b><font color='red'>" . $error . "</font></b></center>";
            } else {
                
                include_once('config_license.php');
                include_once('func_getContent.php');
                $getcontent_url = $url_license . "/api_itemfind.php";
                $getcontent_data = array(
                    'acclic'    =>  $acclic,
                    'key'    =>  $key,
                    'action'    =>  'itemfind_getimg',
                    
                    'itemfind_item_source_slg'  =>  $itemfind_item_source_slg,
                    'itemfind_item_source'  =>  $itemfind_item_source
                ); 
                
                $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
                
                if ( empty($reponse) ) {
                    echo "Kết nối đến API bị gián đoạn.";
                    exit();
                }
                else {
                    $info = read_TagName($reponse, 'info');
                    if($info == "Error") {
                        echo read_TagName($reponse, 'message');
                        exit();
                    } elseif ($info == "OK") {
                        $itemfind_item_source_data = read_TagName($reponse, 'itemfind_item_source_data');
                        
                        if(strlen($itemfind_item_source_data) == 0) {
                            echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                            
                            $arr_view = "\nDataSend:\n";
                            foreach($getcontent_data as $k => $v) {
                                $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                            }
                            writelog("log_api.txt", $arr_view . $reponse);
                            exit();
                        }
                    } else {
                        echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
                        writelog("log_api.txt", $reponse);
                        exit();
                    }
                }
                
                $itemfind_item_source_arr = json_decode($itemfind_item_source_data, true);
                for($i=0; $i<$itemfind_item_source_slg; $i++) {
                    $itemfind_item_source[$i]['des'] = $itemfind_item_source_arr[$i]['name'];
                    $itemfind_item_source[$i]['img'] = $itemfind_item_source_arr[$i]['img'];
                }
                
                $itemfind_arr[$item] = array(
                    'name'  =>  $itemfind_name,
                    'itemfind_stat'   =>  $itemfind_stat,
                    'itemfind_item_source_slg'  =>  $itemfind_item_source_slg,
                    'itemfind_item_source'  =>  $itemfind_item_source
                );
                
                $content = json_encode($itemfind_arr);
                    
                replacecontent($file_edit,$content);
        		
        		include('config/config_sync.php');
    		    for($i=0; $i<count($url_hosting); $i++)
    		    {
    		        if($url_hosting[$i]) {
    		            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
    		            if($sync_send == 'OK') {
    		                
    		            } else {
    		                $err .= $sync_send;
    		            }
    		        }
    		    }
    		    
    			if($err) {
    		        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    		    } else {
    		    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    		    }
            }
    		break;
    	
    	case 'del':
    		$item = $_POST['item'];
    		
            unset($itemfind_arr[$item]);
            $content = json_encode($itemfind_arr);
    		
    		replacecontent($file_edit,$content);
    		include('config/config_sync.php');
    	    for($i=0; $i<count($url_hosting); $i++)
    	    {
    	        if($url_hosting[$i]) {
    	            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
    	            if($sync_send == 'OK') {
    	                
    	            } else {
    	                $err .= $sync_send;
    	            }
    	        }
    	    }
    	    
    		if($err) {
    	        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    	    } else {
    	    	$notice = "<center><font color='blue'>Xóa thành công</font></center>";
    	    }
    		break;
    }
}
?>
		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình <a href="admin.php?mod=editevent&act=giftcode_random&type=<?php echo $type; ?>" target="_self"><?php echo $tilte; ?></a></h1>
			</div><br />
				Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php 
if($notice) echo $notice;

switch ($page)
{
    case 'add': 
		$itemfind_stat = '';
        if(!isset($_POST['itemfind_stat'])) {
            $itemfind_stat = 'checked';
        } elseif($_POST['itemfind_stat'] == 1) $itemfind_stat = 'checked';
        
        if(!isset($_POST['itemfind_item_source_slg']) || abs(intval($_POST['itemfind_item_source_slg'])) <= 0) $itemfind_item_source_slg = 5;
        else $itemfind_item_source_slg = abs(intval($_POST['itemfind_item_source_slg']));
        if(!isset($_POST['itemfind_percent']) || abs(intval($_POST['itemfind_percent'])) <= 0) $itemfind_percent = 100;
        else $itemfind_percent = abs(intval($_POST['itemfind_percent']));
        
        $content = "
            <center>Thêm công thức nộp Item</center><br>
			<form id='editconfig' name='editconfig' method='post' action='admin.php?mod=editevent&act=event_itemfind&page=add'>
			<input type='hidden' name='action' value='add'/>
			<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
				<tr bgcolor='#FFFFFF' >
					<td align='right' width='120' >Tên công thức</td>
					<td ><input type='text' name='itemfind_name' value='". $_POST['itemfind_name'] ."' size='60' /></td>
				</tr>
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Sử dụng</td>
					<td ><input type='checkbox' name='itemfind_stat' value='1' $itemfind_stat /></td>
				</tr>
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Số loại Item nộp</td>
					<td ><input type='text' name='itemfind_item_source_slg' value='". $itemfind_item_source_slg ."' size='5' /></td>
				</tr>
            ";
        
        for($i=0; $i < $itemfind_item_source_slg; $i++) {
            $nl_stt = $i+1;
            
            $exl_select0 = (!isset($_POST['itemfind_item_source'][$i]['exl']) || $_POST['itemfind_item_source'][$i]['exl']==0) ? 'selected' : '';
            $exl_select1 = (isset($_POST['itemfind_item_source'][$i]['exl']) && $_POST['itemfind_item_source'][$i]['exl']==1) ? 'selected' : '';
            $exl_select2 = (isset($_POST['itemfind_item_source'][$i]['exl']) && $_POST['itemfind_item_source'][$i]['exl']==2) ? 'selected' : '';
            $exl_select3 = (isset($_POST['itemfind_item_source'][$i]['exl']) && $_POST['itemfind_item_source'][$i]['exl']==3) ? 'selected' : '';
            $exl_select4 = (isset($_POST['itemfind_item_source'][$i]['exl']) && $_POST['itemfind_item_source'][$i]['exl']==4) ? 'selected' : '';
            $exl_select5 = (isset($_POST['itemfind_item_source'][$i]['exl']) && $_POST['itemfind_item_source'][$i]['exl']==5) ? 'selected' : '';
            $exl_select8 = (isset($_POST['itemfind_item_source'][$i]['exl']) && $_POST['itemfind_item_source'][$i]['exl']==8) ? 'selected' : '';
            $exl_select9 = (isset($_POST['itemfind_item_source'][$i]['exl']) && $_POST['itemfind_item_source'][$i]['exl']==9) ? 'selected' : '';
            
            $lvl_select0 = (!isset($_POST['itemfind_item_source'][$i]['lvl']) || $_POST['itemfind_item_source'][$i]['lvl']==0) ? 'selected' : '';
            $lvl_select1 = (isset($_POST['itemfind_item_source'][$i]['lvl']) && $_POST['itemfind_item_source'][$i]['lvl']==1) ? 'selected' : '';
            $lvl_select2 = (isset($_POST['itemfind_item_source'][$i]['lvl']) && $_POST['itemfind_item_source'][$i]['lvl']==2) ? 'selected' : '';
            $lvl_select3 = (isset($_POST['itemfind_item_source'][$i]['lvl']) && $_POST['itemfind_item_source'][$i]['lvl']==3) ? 'selected' : '';
            $lvl_select4 = (isset($_POST['itemfind_item_source'][$i]['lvl']) && $_POST['itemfind_item_source'][$i]['lvl']==4) ? 'selected' : '';
            $lvl_select5 = (isset($_POST['itemfind_item_source'][$i]['lvl']) && $_POST['itemfind_item_source'][$i]['lvl']==5) ? 'selected' : '';
            $lvl_select6 = (isset($_POST['itemfind_item_source'][$i]['lvl']) && $_POST['itemfind_item_source'][$i]['lvl']==6) ? 'selected' : '';
            $lvl_select7 = (isset($_POST['itemfind_item_source'][$i]['lvl']) && $_POST['itemfind_item_source'][$i]['lvl']==7) ? 'selected' : '';
            $lvl_select8 = (isset($_POST['itemfind_item_source'][$i]['lvl']) && $_POST['itemfind_item_source'][$i]['lvl']==8) ? 'selected' : '';
            $lvl_select9 = (isset($_POST['itemfind_item_source'][$i]['lvl']) && $_POST['itemfind_item_source'][$i]['lvl']==9) ? 'selected' : '';
            $lvl_select10 = (isset($_POST['itemfind_item_source'][$i]['lvl']) && $_POST['itemfind_item_source'][$i]['lvl']==10) ? 'selected' : '';
            $lvl_select11 = (isset($_POST['itemfind_item_source'][$i]['lvl']) && $_POST['itemfind_item_source'][$i]['lvl']==11) ? 'selected' : '';
            $lvl_select12 = (isset($_POST['itemfind_item_source'][$i]['lvl']) && $_POST['itemfind_item_source'][$i]['lvl']==12) ? 'selected' : '';
            $lvl_select13 = (isset($_POST['itemfind_item_source'][$i]['lvl']) && $_POST['itemfind_item_source'][$i]['lvl']==13) ? 'selected' : '';
            $lvl_select14 = (isset($_POST['itemfind_item_source'][$i]['lvl']) && $_POST['itemfind_item_source'][$i]['lvl']==14) ? 'selected' : '';
            $lvl_select99 = (isset($_POST['itemfind_item_source'][$i]['lvl']) && $_POST['itemfind_item_source'][$i]['lvl']==99) ? 'selected' : '';
            
            $option_select0 = (!isset($_POST['itemfind_item_source'][$i]['option']) || $_POST['itemfind_item_source'][$i]['option']==0) ? 'selected' : '';
            $option_select1 = (isset($_POST['itemfind_item_source'][$i]['option']) && $_POST['itemfind_item_source'][$i]['option']==1) ? 'selected' : '';
            $option_select2 = (isset($_POST['itemfind_item_source'][$i]['option']) && $_POST['itemfind_item_source'][$i]['option']==2) ? 'selected' : '';
            $option_select3 = (isset($_POST['itemfind_item_source'][$i]['option']) && $_POST['itemfind_item_source'][$i]['option']==3) ? 'selected' : '';
            $option_select4 = (isset($_POST['itemfind_item_source'][$i]['option']) && $_POST['itemfind_item_source'][$i]['option']==4) ? 'selected' : '';
            $option_select5 = (isset($_POST['itemfind_item_source'][$i]['option']) && $_POST['itemfind_item_source'][$i]['option']==5) ? 'selected' : '';
            $option_select6 = (isset($_POST['itemfind_item_source'][$i]['option']) && $_POST['itemfind_item_source'][$i]['option']==6) ? 'selected' : '';
            $option_select99 = (isset($_POST['itemfind_item_source'][$i]['option']) && $_POST['itemfind_item_source'][$i]['option']==99) ? 'selected' : '';
            
            if(!isset($_POST['itemfind_item_source'][$i]['slg']) || abs(intval($_POST['itemfind_item_source'][$i]['slg'])) <= 0) $itemfind_item_source[$i]['slg'] = 1;
            else $itemfind_item_source[$i]['slg'] = abs(intval($_POST['itemfind_item_source'][$i]['slg']));
            
            if(!isset($_POST['itemfind_item_source'][$i]['luck']) || abs(intval($_POST['itemfind_item_source'][$i]['luck'])) != 1) $itemfind_item_source[$i]['luck'] = '';
            else $itemfind_item_source[$i]['luck'] = 'checked';
            
            if(!isset($_POST['itemfind_item_source'][$i]['skill']) || abs(intval($_POST['itemfind_item_source'][$i]['skill'])) != 1) $itemfind_item_source[$i]['skill'] = '';
            else $itemfind_item_source[$i]['skill'] = 'checked';
        
            $content .= "
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Loại Item nộp ". $nl_stt ."</td>
					<td >
                        Mô tả Item nộp : <input type='text' name='itemfind_item_source[". $i ."][name]' value='". $_POST['itemfind_item_source'][$i]['name'] ."' size='40' /><br />
                        <div align='right'>
                        32 Mã Item : <input type='text' name='itemfind_item_source[". $i ."][code]' value='". $_POST['itemfind_item_source'][$i]['code'] ."' size='35' /><br />
                        Số lượng : <input type='text' name='itemfind_item_source[". $i ."][slg]' value='". $itemfind_item_source[$i]['slg'] ."' size='5' />
                        Có Luck : <input type='checkbox' name='itemfind_item_source[". $i ."][luck]' value='1' ". $itemfind_item_source[$i]['luck'] ." />
                        Có Skill : <input type='checkbox' name='itemfind_item_source[". $i ."][skill]' value='1' ". $itemfind_item_source[$i]['skill'] ." /><br />
                        Dòng hoàn hảo yêu cầu : 
                        <select name='itemfind_item_source[". $i ."][exl]'>
                            <option value=0 ". $exl_select0 .">Chính xác như Item mẫu</option>
                            <option value=1 ". $exl_select1 .">1 dòng hoàn hảo trở lên</option>
                            <option value=2 ". $exl_select2 .">2 dòng hoàn hảo trở lên</option>
                            <option value=3 ". $exl_select3 .">3 dòng hoàn hảo trở lên</option>
                            <option value=4 ". $exl_select4 .">4 dòng hoàn hảo trở lên</option>
                            <option value=5 ". $exl_select5 .">5 dòng hoàn hảo trở lên</option>
                            <option value=8 ". $exl_select9 .">Item Thần</option>
                            <option value=9 ". $exl_select9 .">Tùy ý</option>
                        </select><br />
                        Cấp độ yêu cầu : 
                        <select name='itemfind_item_source[". $i ."][lvl]'>
                            <option value=0 ". $lvl_select0 .">Chính xác như Item mẫu</option>
                            <option value=1 ". $lvl_select1 .">+1 trở lên</option>
                            <option value=2 ". $lvl_select2 .">+2 trở lên</option>
                            <option value=3 ". $lvl_select3 .">+3 trở lên</option>
                            <option value=4 ". $lvl_select4 .">+4 trở lên</option>
                            <option value=5 ". $lvl_select5 .">+5 trở lên</option>
                            <option value=6 ". $lvl_select6 .">+6 trở lên</option>
                            <option value=7 ". $lvl_select7 .">+7 trở lên</option>
                            <option value=8 ". $lvl_select8 .">+8 trở lên</option>
                            <option value=9 ". $lvl_select9 .">+9 trở lên</option>
                            <option value=10 ". $lvl_select10 .">+10 trở lên</option>
                            <option value=11 ". $lvl_select11 .">+11 trở lên</option>
                            <option value=12 ". $lvl_select12 .">+12 trở lên</option>
                            <option value=13 ". $lvl_select13 .">+13 trở lên</option>
                            <option value=14 ". $lvl_select14 .">+14 trở lên</option>
                            <option value=99 ". $lvl_select99 .">Tùy ý</option>
                        </select><br />
                        Option yêu cầu : 
                        <select name='itemfind_item_source[". $i ."][option]'>
                            <option value=0 ". $option_select0 .">Chính xác như Item mẫu</option>
                            <option value=1 ". $option_select1 .">Option 4 trở lên</option>
                            <option value=2 ". $option_select2 .">Option 8 trở lên</option>
                            <option value=3 ". $option_select3 .">Option 12 trở lên</option>
                            <option value=4 ". $option_select4 .">Option 16 trở lên</option>
                            <option value=5 ". $option_select5 .">Option 20 trở lên</option>
                            <option value=6 ". $option_select6 .">Option 24 trở lên</option>
                            <option value=99 ". $option_select99 .">Tùy ý</option>
                        </select><br />
                        
                        </div>
                    </td>
				</tr>
                ";
        }
        
        $content .= "
            <tr bgcolor='#FFFFFF' >
					<td >&nbsp;</td>
					<td align='center'><input type='submit' name='Submit' value='Thêm công thức Đổi Item' ". $show_accept ." /></td>
				</tr>
			</table>
			</form>
            ";
		echo $content;
		break;
	
	case 'edit': 
        $item = $_GET['item'];          $item = abs(intval($item));
        
        if(!isset($_POST['itemfind_name'])) $itemfind_name = $itemfind_arr[$item]['name'];
        else $itemfind_name = $_POST['itemfind_name'];
        
        $itemfind_stat = '';
        if(!isset($_POST['itemfind_stat'])) {
            if(isset($itemfind_arr[$item]['itemfind_stat']) && $itemfind_arr[$item]['itemfind_stat'] == 1) $itemfind_stat = 'checked';
        } elseif($_POST['itemfind_stat'] == 1) $itemfind_stat = 'checked';
        
        if(!isset($_POST['itemfind_item_source_slg'])) {
            if(isset($itemfind_arr[$item]['itemfind_item_source_slg'])) $itemfind_item_source_slg = $itemfind_arr[$item]['itemfind_item_source_slg'];
            else $itemfind_item_source_slg = 5;
        } else $itemfind_item_source_slg = abs(intval($_POST['itemfind_item_source_slg']));
        
		$content = "<center><b>Sửa Loại Nộp Item</b></center><br>
			<form id='editconfig' name='editconfig' method='post' action='admin.php?mod=editevent&act=event_itemfind&page=edit&item=".$item."'>
			<input type='hidden' name='action' value='edit'/>
			<input type='hidden' name='item' value='".$item."'/>
            
			<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
				<tr bgcolor='#FFFFFF' >
					<td align='right' width='120' >Tên công thức</td>
					<td ><input type='text' name='itemfind_name' value='". $itemfind_name ."' size='50' /></td>
				</tr>
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Sử dụng</td>
					<td ><input type='checkbox' name='itemfind_stat' value='1' $itemfind_stat /></td>
				</tr>
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Số loại Item nộp</td>
					<td ><input type='text' name='itemfind_item_source_slg' value='". $itemfind_item_source_slg ."' size='5' /></td>
				</tr>
            ";
        
        for($i=0; $i < $itemfind_item_source_slg; $i++) {
            $nl_stt = $i+1;
            
            if(!isset($_POST['itemfind_item_source'][$i]['name'])) {
                if(isset($itemfind_arr[$item]['itemfind_item_source'][$i]['name'])) $itemfind_item_source[$i]['name'] = $itemfind_arr[$item]['itemfind_item_source'][$i]['name'];
                else $itemfind_item_source[$i]['name'] = '';
            } else $itemfind_item_source[$i]['name'] = $_POST['itemfind_item_source'][$i]['name'];
            
            if(!isset($_POST['itemfind_item_source'][$i]['code'])) {
                if(isset($itemfind_arr[$item]['itemfind_item_source'][$i]['code'])) $itemfind_item_source[$i]['code'] = $itemfind_arr[$item]['itemfind_item_source'][$i]['code'];
                else $itemfind_item_source[$i]['code'] = '';
            } else $itemfind_item_source[$i]['code'] = $_POST['itemfind_item_source'][$i]['code'];
            
            if(!isset($_POST['itemfind_item_source'][$i]['exl'])) {
                if(isset($itemfind_arr[$item]['itemfind_item_source'][$i]['exl'])) $exl_select = $itemfind_arr[$item]['itemfind_item_source'][$i]['exl'];
                else $exl_select = 0;
            } else $exl_select = $_POST['itemfind_item_source'][$i]['exl'];
            $exl_select0 = ($exl_select==0) ? 'selected' : '';
            $exl_select1 = ($exl_select==1) ? 'selected' : '';
            $exl_select2 = ($exl_select==2) ? 'selected' : '';
            $exl_select3 = ($exl_select==3) ? 'selected' : '';
            $exl_select4 = ($exl_select==4) ? 'selected' : '';
            $exl_select5 = ($exl_select==5) ? 'selected' : '';
            $exl_select8 = ($exl_select==8) ? 'selected' : '';
            $exl_select9 = ($exl_select==9) ? 'selected' : '';
            
            if(!isset($_POST['itemfind_item_source'][$i]['lvl'])) {
                if(isset($itemfind_arr[$item]['itemfind_item_source'][$i]['lvl'])) $lvl_select = $itemfind_arr[$item]['itemfind_item_source'][$i]['lvl'];
                else $lvl_select = 0;
            } else $lvl_select = $_POST['itemfind_item_source'][$i]['lvl'];
            $lvl_select0 = ($lvl_select==0) ? 'selected' : '';
            $lvl_select1 = ($lvl_select==1) ? 'selected' : '';
            $lvl_select2 = ($lvl_select==2) ? 'selected' : '';
            $lvl_select3 = ($lvl_select==3) ? 'selected' : '';
            $lvl_select4 = ($lvl_select==4) ? 'selected' : '';
            $lvl_select5 = ($lvl_select==5) ? 'selected' : '';
            $lvl_select6 = ($lvl_select==6) ? 'selected' : '';
            $lvl_select7 = ($lvl_select==7) ? 'selected' : '';
            $lvl_select8 = ($lvl_select==8) ? 'selected' : '';
            $lvl_select9 = ($lvl_select==9) ? 'selected' : '';
            $lvl_select10 = ($lvl_select==10) ? 'selected' : '';
            $lvl_select11 = ($lvl_select==11) ? 'selected' : '';
            $lvl_select12 = ($lvl_select==12) ? 'selected' : '';
            $lvl_select13 = ($lvl_select==13) ? 'selected' : '';
            $lvl_select14 = ($lvl_select==14) ? 'selected' : '';
            $lvl_select99 = ($lvl_select==99) ? 'selected' : '';
            
            if(!isset($_POST['itemfind_item_source'][$i]['option'])) {
                if(!isset($itemfind_arr[$item]['itemfind_item_source'][$i]['option'])) $option_select = 0;
                else $option_select = $itemfind_arr[$item]['itemfind_item_source'][$i]['option'];
            } else $option_select = $_POST['itemfind_item_source'][$i]['option'];
            $option_select0 = ($option_select==0) ? 'selected' : '';
            $option_select1 = ($option_select==1) ? 'selected' : '';
            $option_select2 = ($option_select==2) ? 'selected' : '';
            $option_select3 = ($option_select==3) ? 'selected' : '';
            $option_select4 = ($option_select==4) ? 'selected' : '';
            $option_select5 = ($option_select==5) ? 'selected' : '';
            $option_select6 = ($option_select==6) ? 'selected' : '';
            $option_select99 = ($option_select==99) ? 'selected' : '';
            
            if(!isset($_POST['itemfind_item_source'][$i]['slg'])) {
                if(isset($itemfind_arr[$item]['itemfind_item_source'][$i]['slg'])) $itemfind_item_source[$i]['slg'] = $itemfind_arr[$item]['itemfind_item_source'][$i]['slg'];
                else $itemfind_item_source[$i]['slg'] = 1;
            } else $itemfind_item_source[$i]['slg'] = abs(intval($_POST['itemfind_item_source'][$i]['slg']));
            
            $itemfind_item_source[$i]['luck'] = '';
            if(!isset($_POST['itemfind_item_source'][$i]['luck'])) {
                if(isset($itemfind_arr[$item]['itemfind_item_source'][$i]['luck']) && $itemfind_arr[$item]['itemfind_item_source'][$i]['luck'] == 1) $itemfind_item_source[$i]['luck'] = 'checked';
            } elseif($_POST['itemfind_item_source'][$i]['luck'] == 1) $itemfind_item_source[$i]['luck'] = 'checked';
            
            $itemfind_item_source[$i]['skill'] = '';
            if(!isset($_POST['itemfind_item_source'][$i]['skill'])) {
                if(isset($itemfind_arr[$item]['itemfind_item_source'][$i]['skill']) && $itemfind_arr[$item]['itemfind_item_source'][$i]['skill'] == 1) $itemfind_item_source[$i]['luck'] = 'checked';
            } elseif($_POST['itemfind_item_source'][$i]['skill'] == 1) $itemfind_item_source[$i]['skill'] = 'checked';
        
            $content .= "
                <tr bgcolor='#FFFFFF' >
					<td align='right' >Loại Item nộp ". $nl_stt ."</td>
					<td >
                        Mô tả Item nộp : <input type='text' name='itemfind_item_source[". $i ."][name]' value='". $itemfind_item_source[$i]['name'] ."' size='40' /><br />
                        <div align='right'>
                        32 Mã Item : <input type='text' name='itemfind_item_source[". $i ."][code]' value='". $itemfind_item_source[$i]['code'] ."' size='35' /><br />
                        Số lượng : <input type='text' name='itemfind_item_source[". $i ."][slg]' value='". $itemfind_item_source[$i]['slg'] ."' size='5' />
                        Có Luck : <input type='checkbox' name='itemfind_item_source[". $i ."][luck]' value='1' ". $itemfind_item_source[$i]['luck'] ." />
                        Có Skill : <input type='checkbox' name='itemfind_item_source[". $i ."][skill]' value='1' ". $itemfind_item_source[$i]['skill'] ." /><br />
                        Dòng hoàn hảo yêu cầu : 
                        <select name='itemfind_item_source[". $i ."][exl]'>
                            <option value=0 ". $exl_select0 .">Chính xác như Item mẫu</option>
                            <option value=1 ". $exl_select1 .">1 dòng hoàn hảo trở lên</option>
                            <option value=2 ". $exl_select2 .">2 dòng hoàn hảo trở lên</option>
                            <option value=3 ". $exl_select3 .">3 dòng hoàn hảo trở lên</option>
                            <option value=4 ". $exl_select4 .">4 dòng hoàn hảo trở lên</option>
                            <option value=5 ". $exl_select5 .">5 dòng hoàn hảo trở lên</option>
                            <option value=8 ". $exl_select9 .">Item Thần</option>
                            <option value=9 ". $exl_select9 .">Tùy ý</option>
                        </select><br />
                        Cấp độ yêu cầu : 
                        <select name='itemfind_item_source[". $i ."][lvl]'>
                            <option value=0 ". $lvl_select0 .">Chính xác như Item mẫu</option>
                            <option value=1 ". $lvl_select1 .">+1 trở lên</option>
                            <option value=2 ". $lvl_select2 .">+2 trở lên</option>
                            <option value=3 ". $lvl_select3 .">+3 trở lên</option>
                            <option value=4 ". $lvl_select4 .">+4 trở lên</option>
                            <option value=5 ". $lvl_select5 .">+5 trở lên</option>
                            <option value=6 ". $lvl_select6 .">+6 trở lên</option>
                            <option value=7 ". $lvl_select7 .">+7 trở lên</option>
                            <option value=8 ". $lvl_select8 .">+8 trở lên</option>
                            <option value=9 ". $lvl_select9 .">+9 trở lên</option>
                            <option value=10 ". $lvl_select10 .">+10 trở lên</option>
                            <option value=11 ". $lvl_select11 .">+11 trở lên</option>
                            <option value=12 ". $lvl_select12 .">+12 trở lên</option>
                            <option value=13 ". $lvl_select13 .">+13 trở lên</option>
                            <option value=14 ". $lvl_select14 .">+14 trở lên</option>
                            <option value=99 ". $lvl_select99 .">Tùy ý</option>
                        </select><br />
                        Option yêu cầu : 
                        <select name='itemfind_item_source[". $i ."][option]'>
                            <option value=0 ". $option_select0 .">Chính xác như Item mẫu</option>
                            <option value=1 ". $option_select1 .">Option 4 trở lên</option>
                            <option value=2 ". $option_select2 .">Option 8 trở lên</option>
                            <option value=3 ". $option_select3 .">Option 12 trở lên</option>
                            <option value=4 ". $option_select4 .">Option 16 trở lên</option>
                            <option value=5 ". $option_select5 .">Option 20 trở lên</option>
                            <option value=6 ". $option_select6 .">Option 24 trở lên</option>
                            <option value=99 ". $option_select99 .">Tùy ý</option>
                        </select><br />
                        
                        </div>
                    </td>
				</tr>
                ";
        }
        
        $content .= "
            <tr bgcolor='#FFFFFF' >
					<td >&nbsp;</td>
					<td align='center'><input type='submit' name='Submit' value='Sửa công thức Đổi Item' ". $show_accept ." /></td>
				</tr>
			</table>
			</form>";
		echo $content;
		break;
	
	case 'del':
		$item = $_GET['item'];
		$content = "<center><b>Xóa Công thức đổi Item</b></center><br>
			<form id='delitem' name='delitem' method='post' action='admin.php?mod=editevent&act=event_itemfind'>
			<input type='hidden' name='action' value='del'/>
			<input type='hidden' name='item' value='".$item."'/>
			<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
				<tr bgcolor='#FFFFFF' >
					<td align='right' >Tên công thức Đổi Item</td>
					<td >". $itemfind_arr[$item]['name'] ."</td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >&nbsp;</td>
					<td align='left'><input type='submit' name='Submit' value='Xóa' ". $show_accept ." /></td>
				</tr>
			</table>
			</form>";
		echo $content;
		
		break;	
		
	default: 
		echo "<b>Tạo công thức Nộp Item</b>.<br /><br />
        ";

        echo "<div align='right'><a href='admin.php?mod=editevent&act=event_itemfind&page=add'>+ Thêm công thức Nộp Item</a></div><br>";
        
        $content = "<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
    		<tr bgcolor='#FFFFFF' >
    			<td align='center'>Tên Công thức</td>
                <td align='center'>Tình trạng</td>
    		    <td align='center' width='50'>&nbsp;</td>
    		</tr>";
    	if(is_array($itemfind_arr)) {
    	   foreach($itemfind_arr as $k => $v) {
                if($v['itemfind_stat'] == 1) $status = '<font color=blue><strong>Bật</strong></font>';
                else $status = '<font color=red>Tắt</font>';
        		$content .= "<tr bgcolor='#FFFFFF' >
        			<td align='center'>".$v['name']."</td>
                    <td align='center'>".$status."</td>
        			<td align='center'><a href='admin.php?mod=editevent&act=event_itemfind&page=edit&item=".$k."' target='_self'>Sửa</a> / <a href='admin.php?mod=editevent&act=event_itemfind&page=del&item=".$k."' target='_self'>Xóa</a></td>
        		</tr>";
        	}
    	}
            
    	$content .= "</table>";
        
		echo $content;
		break;
	
}

?>
				
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
