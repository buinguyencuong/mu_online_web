<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/config_event_goldbox.php';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

$action = $_POST[action];

if($action == 'edit')
{

	// Doi phan thuong loai 1
	$event_goldbox_loai1_daily_slg = $_POST['event_goldbox_loai1_daily_slg'];    $event_goldbox_loai1_daily_slg = abs(intval($event_goldbox_loai1_daily_slg));
    $event_goldbox_loai1_slg = $_POST['event_goldbox_loai1_slg'];    $event_goldbox_loai1_slg = abs(intval($event_goldbox_loai1_slg));
	$event_goldbox_loai1_zen1 = $_POST['event_goldbox_loai1_zen1'];  $event_goldbox_loai1_zen1 = abs(intval($event_goldbox_loai1_zen1));
	$event_goldbox_loai1_zen2 = $_POST['event_goldbox_loai1_zen2'];  $event_goldbox_loai1_zen2 = abs(intval($event_goldbox_loai1_zen2));
	
	$event_goldbox_loai1_pl1_min = $_POST['event_goldbox_loai1_pl1_min'];  $event_goldbox_loai1_pl1_min = abs(intval($event_goldbox_loai1_pl1_min));
	$event_goldbox_loai1_pl1_max = $_POST['event_goldbox_loai1_pl1_max'];  $event_goldbox_loai1_pl1_max = abs(intval($event_goldbox_loai1_pl1_max));
	
	$event_goldbox_loai1_pl2_min = $_POST['event_goldbox_loai1_pl2_min'];  $event_goldbox_loai1_pl2_min = abs(intval($event_goldbox_loai1_pl2_min));
	$event_goldbox_loai1_pl2_max = $_POST['event_goldbox_loai1_pl2_max'];  $event_goldbox_loai1_pl2_max = abs(intval($event_goldbox_loai1_pl2_max));

	// Doi phan thuong loai 2
	$event_goldbox_loai2_daily_slg = $_POST['event_goldbox_loai2_daily_slg'];    $event_goldbox_loai2_daily_slg = abs(intval($event_goldbox_loai2_daily_slg));
    $event_goldbox_loai2_slg = $_POST['event_goldbox_loai2_slg'];    $event_goldbox_loai2_slg = abs(intval($event_goldbox_loai2_slg));
	$event_goldbox_loai2_gcoin1 = $_POST['event_goldbox_loai2_gcoin1'];  $event_goldbox_loai2_gcoin1 = abs(intval($event_goldbox_loai2_gcoin1));
	$event_goldbox_loai2_gcoin2 = $_POST['event_goldbox_loai2_gcoin2'];  $event_goldbox_loai2_gcoin2 = abs(intval($event_goldbox_loai2_gcoin2));
	
	$event_goldbox_loai2_pl1_min = $_POST['event_goldbox_loai2_pl1_min'];  $event_goldbox_loai2_pl1_min = abs(intval($event_goldbox_loai2_pl1_min));
	$event_goldbox_loai2_pl1_max = $_POST['event_goldbox_loai2_pl1_max'];  $event_goldbox_loai2_pl1_max = abs(intval($event_goldbox_loai2_pl1_max));
	
	$event_goldbox_loai2_pl2_min = $_POST['event_goldbox_loai2_pl2_min'];  $event_goldbox_loai2_pl2_min = abs(intval($event_goldbox_loai2_pl2_min));
	$event_goldbox_loai2_pl2_max = $_POST['event_goldbox_loai2_pl2_max'];  $event_goldbox_loai2_pl2_max = abs(intval($event_goldbox_loai2_pl2_max));
	
	$content = "<?php\n";
	$content .= "\$event_goldbox_loai1_slg	= $event_goldbox_loai1_slg;\n";
    $content .= "\$event_goldbox_loai1_daily_slg	= $event_goldbox_loai1_daily_slg;\n";
	$content .= "\$event_goldbox_loai1_zen1	= $event_goldbox_loai1_zen1;\n";
	$content .= "\$event_goldbox_loai1_zen2	= $event_goldbox_loai1_zen2;\n";
	$content .= "\$event_goldbox_loai1_pl1_min	= $event_goldbox_loai1_pl1_min;\n";
	$content .= "\$event_goldbox_loai1_pl1_max	= $event_goldbox_loai1_pl1_max;\n";
	$content .= "\$event_goldbox_loai1_pl2_min	= $event_goldbox_loai1_pl2_min;\n";
	$content .= "\$event_goldbox_loai1_pl2_max	= $event_goldbox_loai1_pl2_max;\n";
	$content .= "\$event_goldbox_loai2_slg	= $event_goldbox_loai2_slg;\n";
    $content .= "\$event_goldbox_loai2_daily_slg	= $event_goldbox_loai2_daily_slg;\n";
	$content .= "\$event_goldbox_loai2_gcoin1	= $event_goldbox_loai2_gcoin1;\n";
	$content .= "\$event_goldbox_loai2_gcoin2	= $event_goldbox_loai2_gcoin2;\n";
	$content .= "\$event_goldbox_loai2_pl1_min	= $event_goldbox_loai2_pl1_min;\n";
	$content .= "\$event_goldbox_loai2_pl1_max	= $event_goldbox_loai2_pl1_max;\n";
	$content .= "\$event_goldbox_loai2_pl2_min	= $event_goldbox_loai2_pl2_min;\n";
	$content .= "\$event_goldbox_loai2_pl2_max	= $event_goldbox_loai2_pl2_max;\n";
	$content .= "?>";
	
	require_once('admin_cfg/function.php');
	replacecontent($file_edit,$content);
	
	include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}

include($file_edit);
?>
		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình Event Hộp Vàng</h1>
			</div><br>
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				<form id="editconfig" name="editconfig" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
				<table>
					<tr><td colspan="2"><b>Mô tả sự kiện</b><br>
					    - Trong quá trình đánh quái, người chơi sẽ nhặt được Hộp vàng, Hộp bạc, Chìa khóa vàng, Chìa khóa bạc.<br>
                        - Sử dụng chìa khóa bạc mở hộp bạc được Hộp quà Bạc.<br />
                        - Sử dụng chìa khóa vàng mở hộp vàng được Hộp quà Vàng.<br />
					    - Sử dụng Hộp quà Bạc, Hộp quà Vàng để đổi lấy phần thưởng sự kiện.<br />
					</td>
					</tr>
					
					<tr>
						<td width="100">Hộp Vàng: </td>
						<td>Type ID Level : 14 121 0, Mã HEX : 790000123456780000E0000000000000</td>
					</tr>
                    <tr>
						<td width="100">Chìa khóa Vàng: </td>
						<td>Type ID Level : 14 113 0, Mã HEX : 710000123456780000E0000000000000</td>
					</tr>
                    <tr>
						<td width="100">Hộp quà Vàng: </td>
						<td>Type ID Level : 14 123 0, Mã HEX : 7B0000123456780000E0000000000000</td>
					</tr>
                    
                    <tr>
						<td width="100">Hộp Bạc: </td>
						<td>Type ID Level : 14 122 0, Mã HEX : 7A0000123456780000E0000000000000</td>
					</tr>
                    <tr>
						<td width="100">Chìa khóa Bạc: </td>
						<td>Type ID Level : 14 112 0, Mã HEX : 700000123456780000E0000000000000</td>
					</tr>
                    <tr>
						<td width="100">Hộp quà Bạc: </td>
						<td>Type ID Level : 14 124 0, Mã HEX : 7C0000123456780000E0000000000000</td>
					</tr>
					
					<tr><td colspan="2"><hr></td></tr>
					
					<tr><td colspan="2"><b>Công thức đổi phần thưởng</b> :<br>
					<b>- Phần thưởng loại 1</b> :<br>
					1 Hộp quà Bạc + <input type="text" name="event_goldbox_loai1_zen1" value="<?php echo $event_goldbox_loai1_zen1; ?>" size="10"/> Zen = <input type="text" name="event_goldbox_loai1_pl1_min" value="<?php echo $event_goldbox_loai1_pl1_min; ?>" size="2"/> - <input type="text" name="event_goldbox_loai1_pl1_max" value="<?php echo $event_goldbox_loai1_pl1_max; ?>" size="2"/> Điểm Phúc Lợi<br>
					1 Hộp quà Vàng + <input type="text" name="event_goldbox_loai1_zen2" value="<?php echo $event_goldbox_loai1_zen2; ?>" size="10"/> Zen = <input type="text" name="event_goldbox_loai1_pl2_min" value="<?php echo $event_goldbox_loai1_pl2_min; ?>" size="2"/> - <input type="text" name="event_goldbox_loai1_pl2_max" value="<?php echo $event_goldbox_loai1_pl2_max; ?>" size="2"/> Điểm Phúc Lợi<br>
					Đổi tối đa trong ngày : <input type="text" name="event_goldbox_loai1_daily_slg" value="<?php echo $event_goldbox_loai1_daily_slg; ?>" size="2"/> phần thưởng<br>
                    Đổi tối đa : <input type="text" name="event_goldbox_loai1_slg" value="<?php echo $event_goldbox_loai1_slg; ?>" size="2"/> phần thưởng
					<br /><br />
					
					<b>- Phần thưởng loại 2</b> :<br>
					1 Hộp quà Bạc + <input type="text" name="event_goldbox_loai2_gcoin1" value="<?php echo $event_goldbox_loai2_gcoin1; ?>" size="5"/> Gcoin = <input type="text" name="event_goldbox_loai2_pl1_min" value="<?php echo $event_goldbox_loai2_pl1_min; ?>" size="2"/> - <input type="text" name="event_goldbox_loai2_pl1_max" value="<?php echo $event_goldbox_loai2_pl1_max; ?>" size="2"/> Điểm Phúc Lợi<br>
					1 Hộp quà Vàng + <input type="text" name="event_goldbox_loai2_gcoin2" value="<?php echo $event_goldbox_loai2_gcoin2; ?>" size="5"/> Gcoin = <input type="text" name="event_goldbox_loai2_pl2_min" value="<?php echo $event_goldbox_loai2_pl2_min; ?>" size="2"/> - <input type="text" name="event_goldbox_loai2_pl2_max" value="<?php echo $event_goldbox_loai2_pl2_max; ?>" size="2"/> Điểm Phúc Lợi<br>
					Đổi tối đa trong ngày : <input type="text" name="event_goldbox_loai2_daily_slg" value="<?php echo $event_goldbox_loai2_daily_slg; ?>" size="2"/> phần thưởng<br>
                    Đổi tối đa : <input type="text" name="event_goldbox_loai2_slg" value="<?php echo $event_goldbox_loai2_slg; ?>" size="2"/> phần thưởng
					</td></tr>
					
					
					<tr>
						<td>&nbsp;</td>
						<td align="center"><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></td>
					</tr>
				</table>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
