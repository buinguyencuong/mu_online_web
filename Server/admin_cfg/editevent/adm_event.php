<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/config_event.php';
$file_giftcode = 'config/giftcode_random.txt';

if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }
require_once('admin_cfg/function.php');
// Read Giftcode
$giftcode_arr = _giftcode_load($file_giftcode);
// Read Giftcode End	
	
$action = $_POST[action];

if($action == 'edit')
{
	$content = "<?php\n";
	
    $event_giftthangcap_name = $_POST['event_giftthangcap_name'];		$content .= "\$event_giftthangcap_name	= '$event_giftthangcap_name';\n";
    $event_giftthangcap_on = $_POST['event_giftthangcap_on'];           $content .= "\$event_giftthangcap_on	= '$event_giftthangcap_on';\n";
    
    $Use_TopAuto_RS_Day = abs(intval($_POST['Use_TopAuto_RS_Day']));            $content .= "\$Use_TopAuto_RS_Day	= $Use_TopAuto_RS_Day;\n";
    $Use_TopAuto_RS_Week = abs(intval($_POST['Use_TopAuto_RS_Week']));          $content .= "\$Use_TopAuto_RS_Week	= $Use_TopAuto_RS_Week;\n";
    $Use_TopAuto_RS_Month = abs(intval($_POST['Use_TopAuto_RS_Month']));        $content .= "\$Use_TopAuto_RS_Month	= $Use_TopAuto_RS_Month;\n";
    $Use_TopAuto_card_Day = abs(intval($_POST['Use_TopAuto_card_Day']));        $content .= "\$Use_TopAuto_card_Day	= $Use_TopAuto_card_Day;\n";
    $Use_TopAuto_card_Week = abs(intval($_POST['Use_TopAuto_card_Week']));      $content .= "\$Use_TopAuto_card_Week	= $Use_TopAuto_card_Week;\n";
    $Use_TopAuto_card_Month = abs(intval($_POST['Use_TopAuto_card_Month']));    $content .= "\$Use_TopAuto_card_Month	= $Use_TopAuto_card_Month;\n";
    $TopAuto_RS_Day_Begin = $_POST['TopAuto_RS_Day_Begin'];            $content .= "\$TopAuto_RS_Day_Begin	= '$TopAuto_RS_Day_Begin';\n";
    $TopAuto_RS_Week_Begin = $_POST['TopAuto_RS_Week_Begin'];          $content .= "\$TopAuto_RS_Week_Begin	= '$TopAuto_RS_Week_Begin';\n";
    $TopAuto_RS_Month_Begin = $_POST['TopAuto_RS_Month_Begin'];        $content .= "\$TopAuto_RS_Month_Begin	= '$TopAuto_RS_Month_Begin';\n";
    $TopAuto_card_Day_Begin = $_POST['TopAuto_card_Day_Begin'];        $content .= "\$TopAuto_card_Day_Begin	= '$TopAuto_card_Day_Begin';\n";
    $TopAuto_card_Week_Begin = $_POST['TopAuto_card_Week_Begin'];      $content .= "\$TopAuto_card_Week_Begin	= '$TopAuto_card_Week_Begin';\n";
    $TopAuto_card_Month_Begin = $_POST['TopAuto_card_Month_Begin'];    $content .= "\$TopAuto_card_Month_Begin	= '$TopAuto_card_Month_Begin';\n";
    
	$giftcode_nt_name = $_POST['giftcode_nt_name'];		$content .= "\$giftcode_nt_name	= '$giftcode_nt_name';\n";
	$event_napthegc_begin = $_POST['event_napthegc_begin'];		$content .= "\$event_napthegc_begin	= '$event_napthegc_begin';\n";
	$event_napthegc_end = $_POST['event_napthegc_end'];		$content .= "\$event_napthegc_end	= '$event_napthegc_end';\n";
	$giftcode_nt_use = $_POST['giftcode_nt_use'];		$content .= "\$giftcode_nt_use	= $giftcode_nt_use;\n";
	$giftcode_nt_type = $_POST['giftcode_nt_type'];		$content .= "\$giftcode_nt_type	= $giftcode_nt_type;\n";
	
	$trade = $_POST['trade'];
        $trade = abs(intval($trade));
    		$content .= "\$trade	= '$trade';\n";
    
    $sell = $_POST['sell'];
        $sell = abs(intval($sell));
    		$content .= "\$sell	= '$sell';\n";
    
    $repair = $_POST['repair'];
        $repair = abs(intval($repair));
    		$content .= "\$repair	= '$repair';\n";
		
	$GiftCodeThe10000 	= $_POST['GiftCodeThe10000'];
	    $GiftCodeThe10000 = abs(intval($GiftCodeThe10000));
			$content .= "\$GiftCodeThe10000	= $GiftCodeThe10000;\n";
	$GiftCodeThe20000 	= $_POST['GiftCodeThe20000'];
	    $GiftCodeThe20000 = abs(intval($GiftCodeThe20000));
			$content .= "\$GiftCodeThe20000	= $GiftCodeThe20000;\n";
	$GiftCodeThe30000 	= $_POST['GiftCodeThe30000'];
	    $GiftCodeThe30000 = abs(intval($GiftCodeThe30000));
			$content .= "\$GiftCodeThe30000	= $GiftCodeThe30000;\n";
	$GiftCodeThe50000 	= $_POST['GiftCodeThe50000'];
	    $GiftCodeThe50000 = abs(intval($GiftCodeThe50000));
			$content .= "\$GiftCodeThe50000	= $GiftCodeThe50000;\n";
	$GiftCodeThe100000 	= $_POST['GiftCodeThe100000'];
	    $GiftCodeThe100000 = abs(intval($GiftCodeThe100000));
			$content .= "\$GiftCodeThe100000	= $GiftCodeThe100000;\n";
	$GiftCodeThe200000 	= $_POST['GiftCodeThe200000'];
	    $GiftCodeThe200000 = abs(intval($GiftCodeThe200000));
			$content .= "\$GiftCodeThe200000	= $GiftCodeThe200000;\n";
	$GiftCodeThe300000 	= $_POST['GiftCodeThe300000'];
	    $GiftCodeThe300000 = abs(intval($GiftCodeThe300000));
			$content .= "\$GiftCodeThe300000	= $GiftCodeThe300000;\n";
	$GiftCodeThe500000 	= $_POST['GiftCodeThe500000'];
	    $GiftCodeThe500000 = abs(intval($GiftCodeThe500000));
			$content .= "\$GiftCodeThe500000	= $GiftCodeThe500000;\n";
	
    $event_epitem_name = $_POST['event_epitem_name'];		$content .= "\$event_epitem_name	= '$event_epitem_name';\n";
	$event_epitem_on = $_POST['event_epitem_on'];			$content .= "\$event_epitem_on	= $event_epitem_on;\n";
    $event_epitem_begin = $_POST['event_epitem_begin'];	$content .= "\$event_epitem_begin	= '$event_epitem_begin';\n";
	$event_epitem_end = $_POST['event_epitem_end'];	$content .= "\$event_epitem_end	= '$event_epitem_end';\n";
    
	$event_epitem_exlmin_begin = $_POST['event_epitem_exlmin_begin'];
        $event_epitem_exlmin_begin = abs(intval($event_epitem_exlmin_begin));
    		$content .= "\$event_epitem_exlmin_begin	= $event_epitem_exlmin_begin;\n";
	$event_epitem_exlmax_begin = $_POST['event_epitem_exlmax_begin'];
        $event_epitem_exlmax_begin = abs(intval($event_epitem_exlmax_begin));
    		$content .= "\$event_epitem_exlmax_begin	= $event_epitem_exlmax_begin;\n";
    $event_epitem_lvlmin_begin = $_POST['event_epitem_lvlmin_begin'];
        $event_epitem_lvlmin_begin = abs(intval($event_epitem_lvlmin_begin));
    		$content .= "\$event_epitem_lvlmin_begin	= $event_epitem_lvlmin_begin;\n";
	$event_epitem_lvlmax_begin = $_POST['event_epitem_lvlmax_begin'];
        $event_epitem_lvlmax_begin = abs(intval($event_epitem_lvlmax_begin));
    		$content .= "\$event_epitem_lvlmax_begin	= $event_epitem_lvlmax_begin;\n";
    
    $event_epitem_exlmin_end = $_POST['event_epitem_exlmin_end'];
        $event_epitem_exlmin_end = abs(intval($event_epitem_exlmin_end));
    		$content .= "\$event_epitem_exlmin_end	= $event_epitem_exlmin_end;\n";
	$event_epitem_exlmax_end = $_POST['event_epitem_exlmax_end'];
        $event_epitem_exlmax_end = abs(intval($event_epitem_exlmax_end));
    		$content .= "\$event_epitem_exlmax_end	= $event_epitem_exlmax_end;\n";
    $event_epitem_lvlmin_end = $_POST['event_epitem_lvlmin_end'];
        $event_epitem_lvlmin_end = abs(intval($event_epitem_lvlmin_end));
    		$content .= "\$event_epitem_lvlmin_end	= $event_epitem_lvlmin_end;\n";
	$event_epitem_lvlmax_end = $_POST['event_epitem_lvlmax_end'];
        $event_epitem_lvlmax_end = abs(intval($event_epitem_lvlmax_end));
    		$content .= "\$event_epitem_lvlmax_end	= $event_epitem_lvlmax_end;\n";
             
	$event_bongda_name = $_POST['event_bongda_name'];		$content .= "\$event_bongda_name	= '$event_bongda_name';\n";
	$event_bongda_on = $_POST['event_bongda_on'];			$content .= "\$event_bongda_on	= $event_bongda_on;\n";
	$event_bongda_giai1 = $_POST['event_bongda_giai1'];
        $event_bongda_giai1 = abs(intval($event_bongda_giai1));
    		$content .= "\$event_bongda_giai1	= $event_bongda_giai1;\n";
	$event_bongda_giai2 = $_POST['event_bongda_giai2'];
        $event_bongda_giai2 = abs(intval($event_bongda_giai2));
    		$content .= "\$event_bongda_giai2	= $event_bongda_giai2;\n";
	$event_bongda_giai3 = $_POST['event_bongda_giai3'];
        $event_bongda_giai3 = abs(intval($event_bongda_giai3));
    		$content .= "\$event_bongda_giai3	= $event_bongda_giai3;\n";

	$event1_on = $_POST['event1_on'];		$content .= "\$event1_on	= $event1_on;\n";
	$event1_name = $_POST['event1_name'];	$content .= "\$event1_name	= '$event1_name';\n";
    
    $event2_on = $_POST['event2_on'];		$content .= "\$event2_on	= $event2_on;\n";
	$event2_name = $_POST['event2_name'];   $content .= "\$event2_name	= '$event2_name';\n";
    $event2_begin = $_POST['event2_begin']; $content .= "\$event2_begin	= '$event2_begin';\n";
	$event2_end = $_POST['event2_end'];     $content .= "\$event2_end	= '$event2_end';\n";
    
    $event3_on = $_POST['event3_on'];		$content .= "\$event3_on	= $event3_on;\n";
	$event3_name = $_POST['event3_name'];   $content .= "\$event3_name	= '$event3_name';\n";
    $event3_begin = $_POST['event3_begin']; $content .= "\$event3_begin	= '$event3_begin';\n";
	$event3_end = $_POST['event3_end'];     $content .= "\$event3_end	= '$event3_end';\n";
	
	$event_santa_ticket = $_POST['event_santa_ticket'];
        $event_santa_ticket = abs(intval($event_santa_ticket));
    		$content .= "\$event_santa_ticket	= $event_santa_ticket;\n";
	$event_santa_ticket_name = $_POST['event_santa_ticket_name'];	$content .= "\$event_santa_ticket_name	= '$event_santa_ticket_name';\n";
	
	$event_toprs_on = abs(intval($_POST['event_toprs_on']));		$content .= "\$event_toprs_on	= $event_toprs_on;\n";
	$event_toprs_name = $_POST['event_toprs_name'];	$content .= "\$event_toprs_name	= '$event_toprs_name';\n";
	$event_toprs_begin = $_POST['event_toprs_begin'];	$content .= "\$event_toprs_begin	= '$event_toprs_begin';\n";
	$event_toprs_end = $_POST['event_toprs_end'];	$content .= "\$event_toprs_end	= '$event_toprs_end';\n";
    $event_toprs_rstype = $_POST['event_toprs_rstype'];     $content .= "\$event_toprs_rstype	= '$event_toprs_rstype';\n";
    
    $event_toprs = $_POST['event_toprs'];    
    for($toprs_i=2; $toprs_i <= 5; $toprs_i++) {
        $event_toprs[$toprs_i]['on'] = abs(intval($event_toprs[$toprs_i]['on']));		
        $content .= "\$event_toprs[". $toprs_i ."]['on']	= ". $event_toprs[$toprs_i]['on'] .";\n";
        $content .= "\$event_toprs[". $toprs_i ."]['name']	= '". $event_toprs[$toprs_i]['name'] ."';\n";
        $content .= "\$event_toprs[". $toprs_i ."]['begin']	= '". $event_toprs[$toprs_i]['begin'] ."';\n";
        $content .= "\$event_toprs[". $toprs_i ."]['end']	= '". $event_toprs[$toprs_i]['end'] ."';\n";
        $content .= "\$event_toprs[". $toprs_i ."]['rstype']	= ". $event_toprs[$toprs_i]['rstype'] .";\n";
            
    }
    
    $event_toprs_maxrs = abs(intval($_POST['event_toprs_maxrs']));     $content .= "\$event_toprs_maxrs	= '$event_toprs_maxrs';\n";
	
	$event_toppoint_on = $_POST['event_toppoint_on'];		$content .= "\$event_toppoint_on	= $event_toppoint_on;\n";
	$event_toppoint_name = $_POST['event_toppoint_name'];	$content .= "\$event_toppoint_name	= '$event_toppoint_name';\n";
	$event_toppoint_begin = $_POST['event_toppoint_begin'];	$content .= "\$event_toppoint_begin	= '$event_toppoint_begin';\n";
    $event_toppoint_end = $_POST['event_toppoint_end'];	$content .= "\$event_toppoint_end	= '$event_toppoint_end';\n";
	
    $event_topcard_on = abs(intval($_POST['event_topcard_on']));		$content .= "\$event_topcard_on	= $event_topcard_on;\n";
	$event_topcard_name = $_POST['event_topcard_name'];	$content .= "\$event_topcard_name	= '$event_topcard_name';\n";
	$event_topcard_begin = $_POST['event_topcard_begin'];	$content .= "\$event_topcard_begin	= '$event_topcard_begin';\n";
	$event_topcard_end = $_POST['event_topcard_end'];	$content .= "\$event_topcard_end	= '$event_topcard_end';\n";
    
    $event_topcard = $_POST['event_topcard'];
    for($topcard_i=2; $topcard_i <= 5; $topcard_i++) {
        $event_topcard[$topcard_i]['on'] = abs(intval($event_topcard[$topcard_i]['on']));		
        $content .= "\$event_topcard[". $topcard_i ."]['on']	= ". $event_topcard[$topcard_i]['on'] .";\n";
        $content .= "\$event_topcard[". $topcard_i ."]['name']	= '". $event_topcard[$topcard_i]['name'] ."';\n";
        $content .= "\$event_topcard[". $topcard_i ."]['begin']	= '". $event_topcard[$topcard_i]['begin'] ."';\n";
        $content .= "\$event_topcard[". $topcard_i ."]['end']	= '". $event_topcard[$topcard_i]['end'] ."';\n";
    }
    
    $hotroitem_on = $_POST['hotroitem_on'];		$content .= "\$hotroitem_on	= $hotroitem_on;\n";
	$hotroitem_name = $_POST['hotroitem_name'];	$content .= "\$hotroitem_name	= '$hotroitem_name';\n";;
    
    $event_goldbox_on = $_POST['event_goldbox_on'];		$content .= "\$event_goldbox_on	= $event_goldbox_on;\n";
	$event_goldbox_name = $_POST['event_goldbox_name'];	$content .= "\$event_goldbox_name	= '$event_goldbox_name';\n";
    $event_goldbox_begin = $_POST['event_goldbox_begin'];		$content .= "\$event_goldbox_begin	= '$event_goldbox_begin';\n";
    $event_goldbox_end = $_POST['event_goldbox_end'];		$content .= "\$event_goldbox_end	= '$event_goldbox_end';\n";
    $event_top_goldbox_name = $_POST['event_top_goldbox_name'];		$content .= "\$event_top_goldbox_name	= '$event_top_goldbox_name';\n";
    $event_top_goldbox_on = $_POST['event_top_goldbox_on'];		$content .= "\$event_top_goldbox_on	= $event_top_goldbox_on;\n";
    
	$content .= "?>";
	
	require_once('admin_cfg/function.php');
	replacecontent($file_edit,$content);
	
	include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}

include($file_edit);
?>
		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình Event</h1>
			</div><br>
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				<form id="editconfig" name="editconfig" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
				<table>
					<!-- Event Thăng cấp nhận thưởng -->
					<tr><td colspan="2"><b><font color='red'>Event : Thăng cấp nhận thưởng</font></b></td></tr>
					<tr>
						<td width="100">Tên Event: </td>
						<td><input type="text" name="event_giftthangcap_name" value="<?php echo $event_giftthangcap_name; ?>" size="50"/></td>
					</tr>
                    <tr>
						<td>Sử dụng: </td>
						<td>Không <input name="event_giftthangcap_on" type="radio" value="0" <?php if($event_giftthangcap_on==0) echo "checked='checked'"; ?>/>
						Có <input name="event_giftthangcap_on" type="radio" value="1" <?php if($event_giftthangcap_on==1) echo "checked='checked'"; ?>/></td>
					</tr>
                    <tr><td colspan="2"><hr /></td></tr>
                    
                    <!-- Event Đua TOP Tự động -->
					<tr><td colspan="2">
                        <b><font color='red'>Event : Đua TOP tự động</font></b><br />
                        TOP Bá Đạo : TOP Reset<br />
                        TOP Bá Giả : TOP Nạp thẻ<br />
                    </td></tr>
					<tr>
						<td width="100" align="right">TOP Bá Đạo ngày: </td>
						<td><input type="checkbox" name="Use_TopAuto_RS_Day" value="1" <?php if($Use_TopAuto_RS_Day == 1) echo "checked"; ?>/> bắt đầu từ <input type="text" name="TopAuto_RS_Day_Begin" value="<?php echo $TopAuto_RS_Day_Begin; ?>" class="time_choise" size="10" maxlength="10" /></td>
					</tr>
                    <tr>
						<td width="100" align="right">TOP Bá Đạo Tuần: </td>
						<td><input type="checkbox" name="Use_TopAuto_RS_Week" value="1" <?php if($Use_TopAuto_RS_Week == 1) echo "checked"; ?>/> bắt đầu từ <input type="text" name="TopAuto_RS_Week_Begin" value="<?php echo $TopAuto_RS_Week_Begin; ?>" class="time_choise" size="10" maxlength="10" /> (Phải chọn đúng Thứ 2)</td>
					</tr>
                    <tr>
						<td width="100" align="right">TOP Bá Đạo Tháng: </td>
						<td><input type="checkbox" name="Use_TopAuto_RS_Month" value="1" <?php if($Use_TopAuto_RS_Month == 1) echo "checked"; ?>/> bắt đầu từ <input type="text" name="TopAuto_RS_Month_Begin" value="<?php echo $TopAuto_RS_Month_Begin; ?>" class="time_choise" size="10" maxlength="10" /> (Phải chọn đúng mùng 1)</td>
					</tr>
                    
                    <tr>
						<td width="100" align="right">TOP Bá Giả ngày: </td>
						<td><input type="checkbox" name="Use_TopAuto_card_Day" value="1" <?php if($Use_TopAuto_card_Day == 1) echo "checked"; ?>/> bắt đầu từ <input type="text" name="TopAuto_card_Day_Begin" value="<?php echo $TopAuto_card_Day_Begin; ?>" class="time_choise" size="10" maxlength="10" /></td>
					</tr>
                    <tr>
						<td width="100" align="right">TOP Bá Giả Tuần: </td>
						<td><input type="checkbox" name="Use_TopAuto_card_Week" value="1" <?php if($Use_TopAuto_card_Week == 1) echo "checked"; ?>/> bắt đầu từ <input type="text" name="TopAuto_card_Week_Begin" value="<?php echo $TopAuto_card_Week_Begin; ?>" class="time_choise" size="10" maxlength="10" /> (Phải chọn đúng Thứ 2)</td>
					</tr>
                    <tr>
						<td width="100" align="right">TOP Bá Giả Tháng: </td>
						<td><input type="checkbox" name="Use_TopAuto_card_Month" value="1" <?php if($Use_TopAuto_card_Month == 1) echo "checked"; ?>/> bắt đầu từ <input type="text" name="TopAuto_card_Month_Begin" value="<?php echo $TopAuto_card_Month_Begin; ?>" class="time_choise" size="10" maxlength="10" /> (Phải chọn đúng Thứ 2)</td>
					</tr>
					<tr><td colspan="2"><hr /></td></tr>
                    
                    <!-- Event Nạp Thẻ Nhận GiftCode -->
					<tr><td colspan="2"><b><font color='red'>Event : Nạp Thẻ Nhận GiftCode</font></b> - <font color='blue'><strong>Chức năng cần LIC</strong></font></td></tr>
					<tr>
						<td width="100">Tên Event: </td>
						<td><input type="text" name="giftcode_nt_name" value="<?php echo $giftcode_nt_name; ?>" size="50"/></td>
					</tr>	
					<tr>
						<td width="100">Thời gian Event: </td>
						<td>0h00 <input type="text" name="event_napthegc_begin" id="event_napthegc_begin" value="<?php echo $event_napthegc_begin; ?>" size="10" maxlength="10" /> - 24h00 <input type="text" name="event_napthegc_end" id="event_napthegc_end" value="<?php echo $event_napthegc_end; ?>" size="10" maxlength="10" /><br>
						<b>Lưu ý</b> : ghi chính xác dạng thời gian <b>năm-tháng-ngày</b> (YYYY-MM-DD)
						</td>
					</tr>
                    <tr>
						<td width="200" align='right'>Sử dụng GiftCode Nạp Thẻ: </td>
						<td>
                            <select name="giftcode_nt_use">
                                <option value="none" <?php if($giftcode_nt_use == 'none') echo "selected='selected'"; ?> >Không</option>
                                <?php 
                                    if(is_array($giftcode_arr)) {
                                        foreach($giftcode_arr as $k => $v) {
                                            echo "<option value='$k' ";
                                            if($giftcode_nt_use == "$k") echo "selected";
                                            echo " >". $v['name'] ."</option>";
                                        }
                                    }
                                ?>
                              </select>
                        </td>
					</tr>					
                    <td align='right' valign="top">Có thể Giao dịch: </td>
						<td>
                            <select name="trade">
                                <option value="1" <?php if($trade == 1) echo "selected='selected'"; ?> >Có</option>
                                <option value="0" <?php if($trade == 0) echo "selected='selected'"; ?> >Không</option>
                            </select>
                        </td>
					</tr>
                    
                    <tr>
						<td align='right' valign="top">Có thể Bán Shop: </td>
						<td>
                            <select name="sell">
                                <option value="1" <?php if($sell == 1) echo "selected='selected'"; ?> >Có</option>
                                <option value="0" <?php if($sell == 0) echo "selected='selected'"; ?> >Không</option>
                            </select>
                        </td>
					</tr>
                    
                    <tr>
						<td align='right' valign="top">Có thể Sửa: </td>
						<td>
                            <select name="repair">
                                <option value="1" <?php if($repair == 1) echo "selected='selected'"; ?> >Có</option>
                                <option value="0" <?php if($repair == 0) echo "selected='selected'"; ?> >Không</option>
                            </select>
                        </td>
					</tr>
					
					<tr>
						<td width="156">Sử dụng Loại : </td>
						<td>
                            <select name="giftcode_nt_type">
                                <option value="0" <?php if($giftcode_nt_type == 0) echo "selected='selected'"; ?> >GiftCode giới hạn</option>
                                <option value="1" <?php if($giftcode_nt_type == 1) echo "selected='selected'"; ?> >GiftCode không giới hạn</option>
                              </select>
							  <br>
						  <b>Lưu ý</b> : GiftCode giới hạn chỉ nhận được một lần với mỗi mệnh giá tương ứng.
                        </td>
					</tr>
					<tr>
						<td width="156">Thẻ 10.000 VNĐ : </td>
						<td width="568"><div style="float: left;"><input type="text" name="GiftCodeThe10000" value="<?php echo $GiftCodeThe10000; ?>" size="3"/></div>  
					   GiftCode</td>
					</tr>
					<tr>
						<td width="156">Thẻ 20.000 VNĐ : </td>
					  <td><div style="float: left;"><input type="text" name="GiftCodeThe20000" value="<?php echo $GiftCodeThe20000; ?>" size="3"/></div> 
				        GiftCode</td>
					</tr>
					<tr>
						<td width="156">Thẻ 30.000 VNĐ : </td>
					  <td><div style="float: left;"><input type="text" name="GiftCodeThe30000" value="<?php echo $GiftCodeThe30000; ?>" size="3"/></div> 
					      GiftCode</td>
					</tr>
					<tr>
						<td width="156">Thẻ 50.000 VNĐ : </td>
					  <td><div style="float: left;"><input type="text" name="GiftCodeThe50000" value="<?php echo $GiftCodeThe50000; ?>" size="3"/></div> 
					      GiftCode</td>
					</tr>
					<tr>
						<td width="156">Thẻ 100.000 VNĐ : </td>
					  <td><div style="float: left;"><input type="text" name="GiftCodeThe100000" value="<?php echo $GiftCodeThe100000; ?>" size="3"/></div> 
					     GiftCode</td>
					</tr>
					<tr>
						<td width="156">Thẻ 200.000 VNĐ : </td>
					  <td><div style="float: left;"><input type="text" name="GiftCodeThe200000" value="<?php echo $GiftCodeThe200000; ?>" size="3"/></div> 
					     GiftCode</td>
					</tr>
					<tr>
						<td width="156">Thẻ 300.000 VNĐ : </td>
					  <td><div style="float: left;"><input type="text" name="GiftCodeThe300000" value="<?php echo $GiftCodeThe300000; ?>" size="3"/></div> 
					     GiftCode</td>
					</tr>
					<tr>
						<td width="156">Thẻ 500.000 VNĐ : </td>
					  <td><div style="float: left;"><input type="text" name="GiftCodeThe500000" value="<?php echo $GiftCodeThe500000; ?>" size="3"/></div> 
					     GiftCode</td>
					</tr>                    
                    <tr><td colspan="2"><hr /></td></tr>
                    
                    <!-- Event Ép Item -->
					<tr><td colspan="2"><b><font color='red'>Event : Ép Item</font></b> - <font color='blue'><strong>Chức năng cần LIC</strong></font></td></tr>
					<tr>
						<td width="100">Tên Event: </td>
						<td><input type="text" name="event_epitem_name" value="<?php echo $event_epitem_name; ?>" size="50"/></td>
					</tr>
					<tr>
						<td width="100">Thời gian Event: </td>
						<td>0h00 <input type="text" name="event_epitem_begin" id="event_epitem_begin" value="<?php echo $event_epitem_begin; ?>" size="10" maxlength="10" /> - 24h00 <input type="text" name="event_epitem_end" id="event_epitem_end" value="<?php echo $event_epitem_end; ?>" size="10" maxlength="10" /><br>
						<b>Lưu ý</b> : ghi chính xác dạng thời gian <b>năm-tháng-ngày</b> (YYYY-MM-DD)
						</td>
					</tr>
                    <tr>
						<td>Sử dụng: </td>
						<td>Không <input name="event_epitem_on" id="event_epitem_on" type="radio" value="0" <?php if($event_epitem_on==0) echo "checked='checked'"; ?>/>
						Có <input name="event_epitem_on" type="radio" value="1" <?php if($event_epitem_on==1) echo "checked='checked'"; ?>/></td>
					</tr>
					
                    <tr>
						<td width="100" colspan="2"><strong>Điều kiện Item đăng ký</strong>: </td>
					</tr>
                    <tr>
						<td width="100">Dòng hoàn hảo : </td>
						<td>từ <input type="text" name="event_epitem_exlmin_begin" value="<?php echo $event_epitem_exlmin_begin; ?>" size="2"/> đến <input type="text" name="event_epitem_exlmax_begin" value="<?php echo $event_epitem_exlmax_begin; ?>" size="2"/> <i>(Số dòng hoàn hảo của Item cho phép đăng ký)</i></td>
					</tr>
					<tr>
						<td width="100">Cấp độ Item: </td>
						<td>từ <input type="text" name="event_epitem_lvlmin_begin" value="<?php echo $event_epitem_lvlmin_begin; ?>" size="2"/> đến <input type="text" name="event_epitem_lvlmax_begin" value="<?php echo $event_epitem_lvlmax_begin; ?>" size="2"/> <i>(Cấp độ Item của Item cho phép đăng ký)</i></td>
					</tr>
                    
                    <tr>
						<td width="100" colspan="2"><strong>Điều kiện Item hoàn thành</strong>: </td>
					</tr>
                    <tr>
						<td width="100">Dòng hoàn hảo : </td>
						<td>từ <input type="text" name="event_epitem_exlmin_end" value="<?php echo $event_epitem_exlmin_end; ?>" size="2"/> đến <input type="text" name="event_epitem_exlmax_end" value="<?php echo $event_epitem_exlmax_end; ?>" size="2"/> <i>(Số dòng hoàn hảo của Item cho phép hoàn thành)</i></td>
					</tr>
					<tr>
						<td width="100">Cấp độ Item: </td>
						<td>từ <input type="text" name="event_epitem_lvlmin_end" value="<?php echo $event_epitem_lvlmin_end; ?>" size="2"/> đến <input type="text" name="event_epitem_lvlmax_end" value="<?php echo $event_epitem_lvlmax_end; ?>" size="2"/> <i>(Cấp độ của Item cho phép hoàn thành)</i></td>
					</tr>
                    
					<tr><td colspan="2"><hr></td></tr>
                    
                    <!-- Event dự đoán bóng đá -->
					<tr><td colspan="2"><b><font color='red'>Event : Dự đoán bóng đá</font></b></td></tr>
					<tr>
						<td width="100">Tên Event: </td>
						<td><input type="text" name="event_bongda_name" value="<?php echo $event_bongda_name; ?>" size="50"/></td>
					</tr>
					<tr>
						<td>Sử dụng: </td>
						<td>Không <input name="event_bongda_on" type="radio" value="0" <?php if($event_bongda_on==0) echo "checked='checked'"; ?>/>
						Có <input name="event_bongda_on" type="radio" value="1" <?php if($event_bongda_on==1) echo "checked='checked'"; ?>/></td>
					</tr>
					<tr>
						<td width="100">Giải nhất: </td>
						<td><input type="text" name="event_bongda_giai1" value="<?php echo $event_bongda_giai1; ?>" size="10"/> Vcent</td>
					</tr>
					<tr>
						<td width="100">Giải nhì: </td>
						<td><input type="text" name="event_bongda_giai2" value="<?php echo $event_bongda_giai2; ?>" size="10"/> Vcent</td>
					</tr>
					<tr>
						<td width="100">Giải ba: </td>
						<td><input type="text" name="event_bongda_giai3" value="<?php echo $event_bongda_giai3; ?>" size="10"/> Vcent</td>
					</tr>
					<tr><td colspan="2"><hr></td></tr>
					
					<!-- Event Đổi vé làng Santa lấy Vcent -->
					<tr><td colspan="2"><b><font color='red'>Event : Đổi vé làng Santa -> Vcent</font></b> - <font color='blue'><strong>Chức năng cần LIC</strong></font></td></tr>
					<tr>
						<td width="100">Tên Event: </td>
						<td><input type="text" name="event_santa_ticket_name" value="<?php echo $event_santa_ticket_name; ?>" size="50"/></td>
					</tr>
					<tr>
						<td>Sử dụng: </td>
						<td>Không <input name="event_santa_ticket" type="radio" value="0" <?php if($event_santa_ticket==0) echo "checked='checked'"; ?>/>
						Có <input name="event_santa_ticket" type="radio" value="1" <?php if($event_santa_ticket==1) echo "checked='checked'"; ?>/></td>
					</tr>
					<tr><td colspan="2"><hr></td></tr>
					
                    <!-- Event Săn bảo vật -->
					<tr><td colspan="2"><b><font color='red'>Event : Săn Bảo vật</font></b> - <font color='blue'><strong>Chức năng cần LIC</strong></font></td></tr>
					<tr>
						<td width="100">Tên Event: </td>
						<td><input type="text" name="event3_name" value="<?php echo $event3_name; ?>" size="50"/></td>
					</tr>
					<tr>
						<td>Sử dụng: </td>
						<td>Không <input name="event3_on" type="radio" value="0" <?php if($event3_on == 0) echo "checked='checked'"; ?>/>
						Có <input name="event3_on" type="radio" value="1" <?php if($event3_on == 1) echo "checked='checked'"; ?>/></td>
					</tr>
					<tr>
						<td width="100">Thời gian Event: </td>
						<td>0h00 <input type="text" name="event3_begin" class="time_choise" value="<?php echo $event3_begin; ?>" size="10" maxlength="10" /> - 24h00 <input type="text" name="event3_end" class="time_choise" value="<?php echo $event3_end; ?>" size="10" maxlength="10" /></td>
					</tr>
					<tr><td colspan="2"><hr /></td></tr>
                    
                    <!-- Event Đổi Item 3 màu -->
					<tr><td colspan="2"><b><font color='red'>Event : Item 3 màu</font></b> - <font color='blue'><strong>Chức năng cần LIC</strong></font></td></tr>
					<tr>
						<td width="100">Tên Event: </td>
						<td><input type="text" name="event2_name" value="<?php echo $event2_name; ?>" size="50"/></td>
					</tr>
					<tr>
						<td>Sử dụng: </td>
						<td>Không <input name="event2_on" type="radio" value="0" <?php if($event2_on == 0) echo "checked='checked'"; ?>/>
						Có <input name="event2_on" type="radio" value="1" <?php if($event2_on == 1) echo "checked='checked'"; ?>/></td>
					</tr>
					<tr>
						<td width="100">Thời gian Event: </td>
						<td>0h00 <input type="text" name="event2_begin" id="event2_begin" value="<?php echo $event2_begin; ?>" size="10" maxlength="10" /> - 24h00 <input type="text" name="event2_end" id="event2_end" value="<?php echo $event2_end; ?>" size="10" maxlength="10" /></td>
					</tr>
					<tr><td colspan="2"><hr /></td></tr>
                    
                    <!-- Event Đổi Item lấy Vcent -->
					<tr><td colspan="2"><b><font color='red'>Event : Huy Chương</font></b> - <font color='blue'><strong>Chức năng cần LIC</strong></font></td></tr>
					<tr>
						<td width="100">Tên Event: </td>
						<td><input type="text" name="event1_name" value="<?php echo $event1_name; ?>" size="50"/></td>
					</tr>
					<tr>
						<td>Sử dụng: </td>
						<td>Không <input name="event1_on" type="radio" value="0" <?php if($event1_on==0) echo "checked='checked'"; ?>/>
						Có <input name="event1_on" type="radio" value="1" <?php if($event1_on==1) echo "checked='checked'"; ?>/></td>
					</tr>
					<tr>
						<td width="100">Thời gian Event: </td>
						<td>0h00 <input type="text" name="event_toppoint_begin" id="event_toppoint_begin" value="<?php echo $event_toppoint_begin; ?>" size="10" maxlength="10" /> - 24h00 <input type="text" name="event_toppoint_end" id="event_toppoint_end" value="<?php echo $event_toppoint_end; ?>" size="10" maxlength="10" /></td>
					</tr>
					
					
					<tr><td colspan="2"><b>Event : TOP Đổi Huy Chương</b></td></tr>
					<tr>
						<td width="100">Tên Event: </td>
						<td><input type="text" name="event_toppoint_name" value="<?php echo $event_toppoint_name; ?>" size="50"/></td>
					</tr>
					<tr>
						<td>Sử dụng: </td>
						<td>Không <input name="event_toppoint_on" type="radio" value="0" <?php if($event_toppoint_on==0) echo "checked='checked'"; ?>/>
						Có <input name="event_toppoint_on" type="radio" value="1" <?php if($event_toppoint_on==1) echo "checked='checked'"; ?>/></td>
					</tr>
					<tr><td colspan="2"><hr></td></tr>
                    
                    <!-- Event Đổi Hộp vàng lấy Phúc Lợi -->
					<tr><td colspan="2"><b><font color='red'>Event : Hộp Vàng</font></b> - <font color='blue'><strong>Chức năng cần LIC</strong></font></td></tr>
					<tr>
						<td width="100">Tên Event: </td>
						<td><input type="text" name="event_goldbox_name" value="<?php echo $event_goldbox_name; ?>" size="50"/></td>
					</tr>
					<tr>
						<td>Sử dụng: </td>
						<td>Không <input name="event_goldbox_on" type="radio" value="0" <?php if($event_goldbox_on==0) echo "checked='checked'"; ?>/>
						Có <input name="event_goldbox_on" type="radio" value="1" <?php if($event_goldbox_on==1) echo "checked='checked'"; ?>/></td>
					</tr>
                    <tr>
						<td width="100">Thời gian Event: </td>
						<td>0h00 <input type="text" name="event_goldbox_begin" id="event_goldbox_begin" value="<?php echo $event_goldbox_begin; ?>" size="10" maxlength="10" /> - 24h00 <input type="text" name="event_goldbox_end" id="event_goldbox_end" value="<?php echo $event_goldbox_end; ?>" size="10" maxlength="10" /></td>
					</tr>
					
					<tr><td colspan="2"><b>Event : TOP Đổi Hộp Vàng</b></td></tr>
					<tr>
						<td width="100">Tên Event: </td>
						<td><input type="text" name="event_top_goldbox_name" value="<?php echo $event_top_goldbox_name; ?>" size="50"/></td>
					</tr>
					<tr>
						<td>Sử dụng: </td>
						<td>Không <input name="event_top_goldbox_on" type="radio" value="0" <?php if($event_top_goldbox_on==0) echo "checked='checked'"; ?>/>
						Có <input name="event_top_goldbox_on" type="radio" value="1" <?php if($event_top_goldbox_on==1) echo "checked='checked'"; ?>/></td>
					</tr>
					<tr><td colspan="2"><hr></td></tr>
					
					<!-- Event TOP Nạp thẻ -->
					<tr><td colspan="2"><b><font color='red'>Event : TOP  Nạp thẻ</font></b></td></tr>
                    <tr><td colspan="2">
                        <table border="1" style="border-collapse: collapse;" width="100%">
                            <tr>
                                <td align="center"><strong>Dùng</strong></td>
                                <td align="center"><strong>Tên Event</strong></td>
                                <td align="center"><strong>Thời gian</strong></td>
                            </tr>
                            <tr>
                                <td align="center"><input type="checkbox" name="event_topcard_on" value="1" <?php if($event_topcard_on == 1) echo "checked"; ?>/></td>
                                <td align="center"><input type="text" name="event_topcard_name" value="<?php echo $event_topcard_name; ?>" size="40"/></td>
                                <td align="center"><input type="text" name="event_topcard_begin" class="time_choise" value="<?php echo $event_topcard_begin; ?>" size="6" maxlength="10" />  - <input type="text" name="event_topcard_end" class="time_choise" value="<?php echo $event_topcard_end; ?>" size="6" maxlength="10" /></td>
                            </tr>
                            <?php for($topcard_i=2; $topcard_i <= 5; $topcard_i++) { ?>
                            <tr>
                                <td align="center"><input type="checkbox" name="event_topcard[<?php echo $topcard_i; ?>][on]" value="1" <?php if($event_topcard[$topcard_i]['on'] == 1) echo "checked"; ?>/></td>
                                <td align="center"><input type="text" name="event_topcard[<?php echo $topcard_i; ?>][name]" value="<?php echo $event_topcard[$topcard_i]['name']; ?>" size="40"/></td>
                                <td align="center"><input type="text" name="event_topcard[<?php echo $topcard_i; ?>][begin]" class="time_choise" value="<?php echo $event_topcard[$topcard_i]['begin']; ?>" size="6" maxlength="10" /> - <input type="text" name="event_topcard[<?php echo $topcard_i; ?>][end]" class="time_choise" value="<?php echo $event_topcard[$topcard_i]['end']; ?>" size="6" maxlength="10" /></td>
                            </tr>
                            <?php } ?>
                        </table>
                    </td></tr>
                    
                    
                    <tr><td colspan="2"><hr></td></tr>
                    
                    <!-- Event TOP Reset trong 1 khoảng thời gian -->
					<tr><td colspan="2"><b><font color='red'>Event : TOP  Reset trong 1 khoảng thời gian</font></b></td></tr>
                    <tr><td colspan="2">
                        <table border="1" style="border-collapse: collapse;" width="100%">
                            <tr>
                                <td align="center"><strong>Dùng</strong></td>
                                <td align="center"><strong>Tên Event</strong></td>
                                <td align="center"><strong>Thời gian</strong></td>
                                <td align="center"><strong>Loại RS</strong></td>
                            </tr>
                            <tr>
                                <td align="center"><input type="checkbox" name="event_toprs_on" value="1" <?php if($event_toprs_on == 1) echo "checked"; ?>/></td>
                                <td align="center"><input type="text" name="event_toprs_name" value="<?php echo $event_toprs_name; ?>" size="40"/></td>
                                <td align="center"><input type="text" name="event_toprs_begin" class="time_choise" value="<?php echo $event_toprs_begin; ?>" size="6" maxlength="10" /> - <input type="text" name="event_toprs_end" class="time_choise" value="<?php echo $event_toprs_end; ?>" size="6" maxlength="10" /></td>
                                <td align="center">
                                    <select name="event_toprs_rstype">
                                        <option value="0" <?php if(!isset($event_toprs_rstype) || $event_toprs_rstype == 0) echo "selected='selected'"; ?> >Loại 1</option>
                                        <option value="1" <?php if(isset($event_toprs_rstype) && $event_toprs_rstype == 1) echo "selected='selected'"; ?> >Loại 2</option>
                                        <option value="2" <?php if(isset($event_toprs_rstype) && $event_toprs_rstype == 2) echo "selected='selected'"; ?> >Loại 3</option>
                                 
                            <?php for($toprs_i=2; $toprs_i <= 5; $toprs_i++) { ?>
                            <tr>
                                <td align="center"><input type="checkbox" name="event_toprs[<?php echo $toprs_i; ?>][on]" value="1" <?php if($event_toprs[$toprs_i]['on'] == 1) echo "checked"; ?>/></td>
                                <td align="center"><input type="text" name="event_toprs[<?php echo $toprs_i; ?>][name]" value="<?php echo $event_toprs[$toprs_i]['name']; ?>" size="40"/></td>
                                <td align="center"><input type="text" name="event_toprs[<?php echo $toprs_i; ?>][begin]" class="time_choise" value="<?php echo $event_toprs[$toprs_i]['begin']; ?>" size="6" maxlength="10" /> - <input type="text" name="event_toprs[<?php echo $toprs_i; ?>][end]" class="time_choise" value="<?php echo $event_toprs[$toprs_i]['end']; ?>" size="6" maxlength="10" /></td>
                                <td align="center">
                                    <select name="event_toprs[<?php echo $toprs_i; ?>][rstype]">
                                        <option value="0" <?php if(!isset($event_toprs[$toprs_i]['rstype']) || $event_toprs[$toprs_i]['rstype'] == 0) echo "selected='selected'"; ?> >Loại 1</option>
                                        <option value="1" <?php if(isset($event_toprs[$toprs_i]['rstype']) && $event_toprs[$toprs_i]['rstype'] == 1) echo "selected='selected'"; ?> >Loại 2</option>
                                        <option value="2" <?php if(isset($event_toprs[$toprs_i]['rstype']) && $event_toprs[$toprs_i]['rstype'] == 2) echo "selected='selected'"; ?> >Loại 3</option>
                                    </select>
                                </td>
                            </tr>
                            <?php } ?>                                                        
                                                                </select>
                                </td>
                            </tr>
                        </table>
                        <strong>Reset Max</strong> : <input type="text" name="event_toprs_maxrs" value="<?php echo isset($event_toprs_maxrs) ? $event_toprs_maxrs : 9999; ?>" size="5"/> (Số lần RS tối đa trong ngày được tính vào BXH, vượt quá sẽ kg tính - Chỉ có tác dụng khi chọn Loại RS có Reset OVER)
                        
                        <fieldset>
                        <legend><strong>Ghi chú</strong></legend>
                        <ul>
                            <li>
                                <strong>Loại Reset</strong> :
                                <ul>
                                    <li><strong>Loại 1</strong> : RS + RS VIP + RS Ủy Thác + RS Ủy Thác VIP</li>
                                    <li><strong>Loại 2</strong> : RS + RS VIP + RS OVER + RS OVER VIP</li>
                                    <li><strong>Loại 3</strong> : RS + RS VIP + RS OVER + RS OVER VIP + RS Ủy Thác + RS Ủy Thác VIP</li>
                                </ul>
                            </li>
                        </ul>
                        </fieldset>
                    </td></tr>
                    
					<tr><td colspan="2"><hr></td></tr>
                    
                    <!-- Hỗ trợ Item tham gia Event -->
					<tr><td colspan="2"><b><font color='red'>Hỗ trợ Item tham gia Event</font></b></td></tr>
					<tr>
						<td width="100">Tên Event: </td>
						<td><input type="text" name="hotroitem_name" value="<?php echo $hotroitem_name; ?>" size="50"/></td>
					</tr>
					<tr>
						<td>Sử dụng: </td>
						<td>Không <input name="hotroitem_on" type="radio" value="0" <?php if($hotroitem_on==0) echo "checked='checked'"; ?>/>
						Có <input name="hotroitem_on" type="radio" value="1" <?php if($hotroitem_on==1) echo "checked='checked'"; ?>/></td>
					</tr>
					
					<tr>
						<td>&nbsp;</td>
						<td align="center"><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></td>
					</tr>
				</table>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
