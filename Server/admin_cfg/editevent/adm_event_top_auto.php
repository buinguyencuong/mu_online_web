<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/

$file_edit = 'config/event_top_rscard_auto.txt';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

if($accept==0) $show_accept = "disabled='disabled'";
else $show_accept = "";

$get_page = $_GET['page'];
$get_type = $_GET['type'];  // rs, card
    switch ($get_type){ 
    	case 'rs':
            $type_name = "Reset";
    	break;
    
    	case 'card':
            $type_name = "Nạp Thẻ";
    	break;
    }
$get_date = $_GET['date'];  // day, week, month
    switch ($get_date){ 
    	case 'day':
            $date_name = "Ngày";
            $date_i_max = 7;
    	break;
    
    	case 'week':
            $date_name = "Tuần";
            $date_i_max = 2;
    	break;
    
    	case 'month':
            $date_name = "Tháng";
            $date_i_max = 2;
    	break;
    }

require_once('admin_cfg/function.php');

// Read Config
$data_cfg_arr = _json_fileload($file_edit);
// Read Config End

if($_POST) {
    $action = $_POST['action'];
    
    switch ($action) {
    	case 'edit':
    		$gift = $_POST['gift'];
            $item = $_GET['item'];          $item = abs(intval($item));
            $top = $_GET['top'];
            
            $gift['item_all'] = strtoupper($gift['item_all']);
            if (!preg_match("/^[A-F0-9]*$/i", $gift['item_all']))
        	{
                $error .= "Dữ liệu lỗi Mã Item chung <strong>kg thời hạn</strong> : ". $gift['item_all'] ." . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
        	}
            if( strlen($gift['item_all'])%32 != 0 ) {
                $error .= "Dữ liệu lỗi Mã Item chung <strong>kg thời hạn</strong>  : ". $gift['item_all'] ." sai cấu trúc.<br />";
            }
            
            $gift['msg_item_all'] = htmlspecialchars($gift['msg_item_all'], ENT_QUOTES, 'UTF-8');
            
            $gift['item_time_all'] = strtoupper($gift['item_time_all']);
            if (!preg_match("/^[A-F0-9]*$/i", $gift['item_time_all']))
        	{
                $error .= "Dữ liệu lỗi Mã Item chung <strong>có thời hạn</strong> : ". $gift['item_time_all'] ." . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
        	}
            if( strlen($gift['item_time_all'])%32 != 0 ) {
                $error .= "Dữ liệu lỗi Mã Item chung <strong>có thời hạn</strong>  : ". $gift['item_time_all'] ." sai cấu trúc.<br />";
            }
            
            $gift['msg_item_time_all'] = htmlspecialchars($gift['msg_item_time_all'], ENT_QUOTES, 'UTF-8');
            
            $gift['day_item_time_all'] = abs(intval($gift['day_item_time_all']));
            
            $gift['gcent_km'] = abs(intval($gift['gcent_km']));
            $gift['vcent_km'] = abs(intval($gift['vcent_km']));
            $gift['zen'] = abs(intval($gift['zen']));
            $gift['chao'] = abs(intval($gift['chao']));
            $gift['cre'] = abs(intval($gift['cre']));
            $gift['blue'] = abs(intval($gift['blue']));
            $gift['pp_extra'] = abs(intval($gift['pp_extra']));
            $gift['buff_day'] = abs(intval($gift['buff_day']));
            
            $gift['item_dw'] = strtoupper($gift['item_dw']);
            if (!preg_match("/^[A-F0-9]*$/i", $gift['item_dw']))
        	{
                $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>DW</strong> : ". $gift['item_dw'] ." . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
        	}
            if( strlen($gift['item_dw'])%32 != 0 ) {
                $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong>cho <strong>DW</strong>  : ". $gift['item_dw'] ." sai cấu trúc.<br />";
            }
            $gift['msg_item_dw'] = htmlspecialchars($gift['msg_item_dw'], ENT_QUOTES, 'UTF-8');
            $gift['item_time_dw'] = strtoupper($gift['item_time_dw']);
            if (!preg_match("/^[A-F0-9]*$/i", $gift['item_time_dw']))
        	{
                $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>DW</strong> : ". $gift['item_time_dw'] ." . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
        	}
            if( strlen($gift['item_time_dw'])%32 != 0 ) {
                $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>DW</strong>  : ". $gift['item_time_dw'] ." sai cấu trúc.<br />";
            }
            $gift['msg_item_time_dw'] = htmlspecialchars($gift['msg_item_time_dw'], ENT_QUOTES, 'UTF-8');
            $gift['day_item_time_dw'] = abs(intval($gift['day_item_time_dw']));
            
            $gift['item_dk'] = strtoupper($gift['item_dk']);
            if (!preg_match("/^[A-F0-9]*$/i", $gift['item_dk']))
        	{
                $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>DK</strong> : ". $gift['item_dk'] ." . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
        	}
            if( strlen($gift['item_dk'])%32 != 0 ) {
                $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong>cho <strong>DK</strong>  : ". $gift['item_dk'] ." sai cấu trúc.<br />";
            }
            $gift['msg_item_dk'] = htmlspecialchars($gift['msg_item_dk'], ENT_QUOTES, 'UTF-8');
            $gift['item_time_dk'] = strtoupper($gift['item_time_dk']);
            if (!preg_match("/^[A-F0-9]*$/i", $gift['item_time_dk']))
        	{
                $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>DK</strong> : ". $gift['item_time_dk'] ." . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
        	}
            if( strlen($gift['item_time_dk'])%32 != 0 ) {
                $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>DK</strong>  : ". $gift['item_time_dk'] ." sai cấu trúc.<br />";
            }
            $gift['msg_item_time_dk'] = htmlspecialchars($gift['msg_item_time_dk'], ENT_QUOTES, 'UTF-8');
            $gift['day_item_time_dk'] = abs(intval($gift['day_item_time_dk']));
            
            $gift['item_elf'] = strtoupper($gift['item_elf']);
            if (!preg_match("/^[A-F0-9]*$/i", $gift['item_elf']))
        	{
                $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>ELF</strong> : ". $gift['item_elf'] ." . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
        	}
            if( strlen($gift['item_elf'])%32 != 0 ) {
                $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong>cho <strong>ELF</strong>  : ". $gift['item_elf'] ." sai cấu trúc.<br />";
            }
            $gift['msg_item_elf'] = htmlspecialchars($gift['msg_item_elf'], ENT_QUOTES, 'UTF-8');
            $gift['item_time_elf'] = strtoupper($gift['item_time_elf']);
            if (!preg_match("/^[A-F0-9]*$/i", $gift['item_time_elf']))
        	{
                $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>ELF</strong> : ". $gift['item_time_elf'] ." . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
        	}
            if( strlen($gift['item_time_elf'])%32 != 0 ) {
                $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>ELF</strong>  : ". $gift['item_time_elf'] ." sai cấu trúc.<br />";
            }
            $gift['msg_item_time_elf'] = htmlspecialchars($gift['msg_item_time_elf'], ENT_QUOTES, 'UTF-8');
            $gift['day_item_time_elf'] = abs(intval($gift['day_item_time_elf']));
            
            $gift['item_mg'] = strtoupper($gift['item_mg']);
            if (!preg_match("/^[A-F0-9]*$/i", $gift['item_mg']))
        	{
                $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>MG</strong> : ". $gift['item_mg'] ." . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
        	}
            if( strlen($gift['item_mg'])%32 != 0 ) {
                $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong>cho <strong>MG</strong>  : ". $gift['item_mg'] ." sai cấu trúc.<br />";
            }
            $gift['msg_item_mg'] = htmlspecialchars($gift['msg_item_mg'], ENT_QUOTES, 'UTF-8');
            $gift['item_time_mg'] = strtoupper($gift['item_time_mg']);
            if (!preg_match("/^[A-F0-9]*$/i", $gift['item_time_mg']))
        	{
                $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>MG</strong> : ". $gift['item_time_mg'] ." . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
        	}
            if( strlen($gift['item_time_mg'])%32 != 0 ) {
                $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>MG</strong>  : ". $gift['item_time_mg'] ." sai cấu trúc.<br />";
            }
            $gift['msg_item_time_mg'] = htmlspecialchars($gift['msg_item_time_mg'], ENT_QUOTES, 'UTF-8');
            $gift['day_item_time_mg'] = abs(intval($gift['day_item_time_mg']));
            
            $gift['item_dl'] = strtoupper($gift['item_dl']);
            if (!preg_match("/^[A-F0-9]*$/i", $gift['item_dl']))
        	{
                $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>DL</strong> : ". $gift['item_dl'] ." . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
        	}
            if( strlen($gift['item_dl'])%32 != 0 ) {
                $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong>cho <strong>DL</strong>  : ". $gift['item_dl'] ." sai cấu trúc.<br />";
            }
            $gift['msg_item_dl'] = htmlspecialchars($gift['msg_item_dl'], ENT_QUOTES, 'UTF-8');
            $gift['item_time_dl'] = strtoupper($gift['item_time_dl']);
            if (!preg_match("/^[A-F0-9]*$/i", $gift['item_time_dl']))
        	{
                $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>DL</strong> : ". $gift['item_time_dl'] ." . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
        	}
            if( strlen($gift['item_time_dl'])%32 != 0 ) {
                $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>DL</strong>  : ". $gift['item_time_dl'] ." sai cấu trúc.<br />";
            }
            $gift['msg_item_time_dl'] = htmlspecialchars($gift['msg_item_time_dl'], ENT_QUOTES, 'UTF-8');
            $gift['day_item_time_dl'] = abs(intval($gift['day_item_time_dl']));
            
            $gift['item_sum'] = strtoupper($gift['item_sum']);
            if (!preg_match("/^[A-F0-9]*$/i", $gift['item_sum']))
        	{
                $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>SUM</strong> : ". $gift['item_sum'] ." . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
        	}
            if( strlen($gift['item_sum'])%32 != 0 ) {
                $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong>cho <strong>SUM</strong>  : ". $gift['item_sum'] ." sai cấu trúc.<br />";
            }
            $gift['msg_item_sum'] = htmlspecialchars($gift['msg_item_sum'], ENT_QUOTES, 'UTF-8');
            $gift['item_time_sum'] = strtoupper($gift['item_time_sum']);
            if (!preg_match("/^[A-F0-9]*$/i", $gift['item_time_sum']))
        	{
                $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>SUM</strong> : ". $gift['item_time_sum'] ." . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
        	}
            if( strlen($gift['item_time_sum'])%32 != 0 ) {
                $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>SUM</strong>  : ". $gift['item_time_sum'] ." sai cấu trúc.<br />";
            }
            $gift['msg_item_time_sum'] = htmlspecialchars($gift['msg_item_time_sum'], ENT_QUOTES, 'UTF-8');
            $gift['day_item_time_sum'] = abs(intval($gift['day_item_time_sum']));
            
            $gift['item_rf'] = strtoupper($gift['item_rf']);
            if (!preg_match("/^[A-F0-9]*$/i", $gift['item_rf']))
        	{
                $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>RF</strong> : ". $gift['item_rf'] ." . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
        	}
            if( strlen($gift['item_rf'])%32 != 0 ) {
                $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong>cho <strong>RF</strong>  : ". $gift['item_rf'] ." sai cấu trúc.<br />";
            }
            $gift['msg_item_rf'] = htmlspecialchars($gift['msg_item_rf'], ENT_QUOTES, 'UTF-8');
            $gift['item_time_rf'] = strtoupper($gift['item_time_rf']);
            if (!preg_match("/^[A-F0-9]*$/i", $gift['item_time_rf']))
        	{
                $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>RF</strong> : ". $gift['item_time_rf'] ." . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
        	}
            if( strlen($gift['item_time_rf'])%32 != 0 ) {
                $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>RF</strong>  : ". $gift['item_time_rf'] ." sai cấu trúc.<br />";
            }
            $gift['msg_item_time_rf'] = htmlspecialchars($gift['msg_item_time_rf'], ENT_QUOTES, 'UTF-8');
            $gift['day_item_time_rf'] = abs(intval($gift['day_item_time_rf']));
            
            if(strlen($error) > 0) {
                $notice = "<center><b><font color='red'>" . $error . "</font></b></center>";
            } else {
                
                $data_cfg_arr[$get_type][$get_date][$item][$top] = $gift;
                
                $content = json_encode($data_cfg_arr);
                
        		replacecontent($file_edit,$content);
        		
        		include('config/config_sync.php');
    		    for($i=0; $i<count($url_hosting); $i++)
    		    {
    		        if($url_hosting[$i]) {
    		            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
    		            if($sync_send == 'OK') {
    		                
    		            } else {
    		                $err .= $sync_send;
    		            }
    		        }
    		    }
    		    
    			if($err) {
    		        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    		    } else {
    		    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    		    }
            }
    		break;
    	
    }
}
?>
		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình <a href="admin.php?mod=editevent&act=giftcode_random&type=<?php echo $type; ?>" target="_self"><?php echo $tilte; ?></a></h1>
			</div><br />
				Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php 
if($notice) echo $notice;

switch ($get_page)
{
	case 'edit': 
        $item = $_GET['item'];          $item = abs(intval($item));
        $top = $_GET['top'];
        
        $gift = $data_cfg_arr[$get_type][$get_date][$item][$top];
        
            if($get_date == 'day') {
                switch ($item){ 
                	case 0:
                        $date_i_name = "Chủ nhật";
                	break;
                
                	case 1:
                        $date_i_name = "Thứ 2";
                	break;
                
                	case 2:
                        $date_i_name = "Thứ 3";
                	break;
                
                	case 3:
                        $date_i_name = "Thứ 4";
                	break;
                
                	case 4:
                        $date_i_name = "Thứ 5";
                	break;
                
                	case 5:
                        $date_i_name = "Thứ 6";
                	break;
                
                	case 6:
                        $date_i_name = "Thứ 7";
                	break;
                }
            } elseif ($get_date == 'week') {
                switch ($item){ 
                	case 0:
                        $date_i_name = "Tuần Chẵn";
                	break;
                
                	case 1:
                        $date_i_name = "Tuần Lẻ";
                	break;
                }
            } elseif ($get_date == 'month') {
                switch ($item){ 
                	case 0:
                        $date_i_name = "Tháng Chẵn";
                	break;
                
                	case 1:
                        $date_i_name = "Tháng Lẻ";
                	break;
                }
            }
?>
        <center>Sửa phần thưởng TOP <?php echo "<strong>". $type_name ."</strong> ". $date_name ?></center><br />
		<form id='editconfig' name='editconfig' method='post' action='admin.php?mod=editevent&act=event_top_auto&type=<?php echo $get_type; ?>&date=<?php echo $get_date; ?>&page=edit&item=<?php echo $item; ?>&top=<?php echo $top; ?>'>
		<input type='hidden' name='action' value='edit'/>

        <table>
			<tr><td colspan="2" align="center"><strong>Thời gian : <?php echo $date_i_name; ?></strong></td></tr>
            
            <tr><td colspan="2"><hr /></td></tr>
            
            <tr><td colspan="2" align="center"><strong>Phần thưởng chung</strong></td></tr>
			<tr>
				<td align='right'>Mã Item <strong>kg thời hạn</strong> : </td>
				<td>
                    <input type="text" name="gift[item_all]" value="<?php echo $gift['item_all']; ?>" size="70"/><br />
                    Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                </td>
			</tr>
            <tr>
				<td align='right'>Mô tả Item kg thời hạn : </td>
				<td><input type="text" name="gift[msg_item_all]" value="<?php echo $gift['msg_item_all']; ?>" size="70"/></td>
			</tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
				<td align='right'>Mã Item <strong>có thời hạn</strong> : </td>
				<td>
                    <input type="text" name="gift[item_time_all]" value="<?php echo $gift['item_time_all']; ?>" size="70"/><br />
                    Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                </td>
			</tr>
            <tr>
				<td align='right'>Mô tả Item có thời hạn : </td>
				<td><input type="text" name="gift[msg_item_time_all]" value="<?php echo $gift['msg_item_time_all']; ?>" size="70"/></td>
			</tr>
            <tr>
				<td align='right'>Thời hạn : </td>
				<td><input type="text" name="gift[day_item_time_all]" value="<?php echo $gift['day_item_time_all']; ?>" size="4"/> ngày</td>
			</tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            
            <tr>
				<td align='right'>Gcent+ : </td>
				<td><input type="text" name="gift[gcent_km]" value="<?php echo $gift['gcent_km']; ?>" size="4"/> </td>
			</tr>
            <tr>
				<td align='right'>Vcent+ : </td>
				<td><input type="text" name="gift[vcent_km]" value="<?php echo $gift['vcent_km']; ?>" size="4"/> </td>
			</tr>
            <tr>
				<td align='right'>ZEN ngân hàng : </td>
				<td><input type="text" name="gift[zen]" value="<?php echo $gift['zen']; ?>" size="4"/> </td>
			</tr>
            <tr>
				<td align='right'>Ngọc Hỗn Nguyên ngân hàng : </td>
				<td><input type="text" name="gift[chao]" value="<?php echo $gift['chao']; ?>" size="4"/> </td>
			</tr>
            <tr>
				<td align='right'>Ngọc Sáng Tạo ngân hàng : </td>
				<td><input type="text" name="gift[cre]" value="<?php echo $gift['cre']; ?>" size="4"/> </td>
			</tr>
            <tr>
				<td align='right'>Lông Vũ ngân hàng : </td>
				<td><input type="text" name="gift[blue]" value="<?php echo $gift['blue']; ?>" size="4"/> </td>
			</tr>
            <tr>
				<td align='right'>PPoint+ : </td>
				<td><input type="text" name="gift[pp_extra]" value="<?php echo $gift['pp_extra']; ?>" size="4"/> </td>
			</tr>
            <tr>
				<td align='right'>Bùa : </td>
				<td>
                    <input type="text" name="gift[buff_day]" value="<?php echo $gift['buff_day']; ?>" size="4"/> ngày <br />(Tăng thời gian dùng bùa nếu đang có bùa, nếu chưa có thì add mặc định Bùa Thiên Sứ)
                </td>
			</tr>
            <tr><td colspan="2"><hr /></td></tr>
            
            
            <tr><td colspan="2" align="center"><strong>Phần thưởng dành cho DW</strong></td></tr>
			<tr>
				<td align='right'>Mã Item <strong>kg thời hạn</strong> : </td>
				<td>
                    <input type="text" name="gift[item_dw]" value="<?php echo $gift['item_dw']; ?>" size="70"/><br />
                    Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                </td>
			</tr>
            <tr>
				<td align='right'>Mô tả Item kg thời hạn: </td>
				<td><input type="text" name="gift[msg_item_dw]" value="<?php echo $gift['msg_item_dw']; ?>" size="70"/></td>
			</tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
				<td align='right'>Mã Item <strong>có thời hạn</strong> : </td>
				<td>
                    <input type="text" name="gift[item_time_dw]" value="<?php echo $gift['item_time_dw']; ?>" size="70"/><br />
                    Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                </td>
			</tr>
            <tr>
				<td align='right'>Mô tả Item có thời hạn: </td>
				<td><input type="text" name="gift[msg_item_time_dw]" value="<?php echo $gift['msg_item_time_dw']; ?>" size="70"/></td>
			</tr>
            <tr>
				<td align='right'>Thời hạn: </td>
				<td><input type="text" name="gift[day_item_time_dw]" value="<?php echo $gift['day_item_time_dw']; ?>" size="4"/> ngày</td>
			</tr>
            <tr><td colspan="2"><hr /></td></tr>
            
			<tr><td colspan="2" align="center"><strong>Phần thưởng dành cho DK</strong></td></tr>
			<tr>
				<td align='right'>Mã Item <strong>kg thời hạn</strong> : </td>
				<td>
                    <input type="text" name="gift[item_dk]" value="<?php echo $gift['item_dk']; ?>" size="70"/><br />
                    Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                </td>
			</tr>
            <tr>
				<td align='right'>Mô tả Item kg thời hạn: </td>
				<td><input type="text" name="gift[msg_item_dk]" value="<?php echo $gift['msg_item_dk']; ?>" size="70"/></td>
			</tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
				<td align='right'>Mã Item <strong>có thời hạn</strong> : </td>
				<td>
                    <input type="text" name="gift[item_time_dk]" value="<?php echo $gift['item_time_dk']; ?>" size="70"/><br />
                    Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                </td>
			</tr>
            <tr>
				<td align='right'>Mô tả Item có thời hạn: </td>
				<td><input type="text" name="gift[msg_item_time_dk]" value="<?php echo $gift['msg_item_time_dk']; ?>" size="70"/></td>
			</tr>
            <tr>
				<td align='right'>Thời hạn: </td>
				<td><input type="text" name="gift[day_item_time_dk]" value="<?php echo $gift['day_item_time_dk']; ?>" size="4"/> ngày</td>
			</tr>
            <tr><td colspan="2"><hr /></td></tr>
            
            <tr><td colspan="2" align="center"><strong>Phần thưởng dành cho ELF</strong></td></tr>
			<tr>
				<td align='right'>Mã Item <strong>kg thời hạn</strong> : </td>
				<td>
                    <input type="text" name="gift[item_elf]" value="<?php echo $gift['item_elf']; ?>" size="70"/><br />
                    Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                </td>
			</tr>
            <tr>
				<td align='right'>Mô tả Item kg thời hạn: </td>
				<td><input type="text" name="gift[msg_item_elf]" value="<?php echo $gift['msg_item_elf']; ?>" size="70"/></td>
			</tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
				<td align='right'>Mã Item <strong>có thời hạn</strong> : </td>
				<td>
                    <input type="text" name="gift[item_time_elf]" value="<?php echo $gift['item_time_elf']; ?>" size="70"/><br />
                    Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                </td>
			</tr>
            <tr>
				<td align='right'>Mô tả Item có thời hạn: </td>
				<td><input type="text" name="gift[msg_item_time_elf]" value="<?php echo $gift['msg_item_time_elf']; ?>" size="70"/></td>
			</tr>
            <tr>
				<td align='right'>Thời hạn: </td>
				<td><input type="text" name="gift[day_item_time_elf]" value="<?php echo $gift['day_item_time_elf']; ?>" size="4"/> ngày</td>
			</tr>
            <tr><td colspan="2"><hr /></td></tr>
            
            <tr><td colspan="2" align="center"><strong>Phần thưởng dành cho MG</strong></td></tr>
			<tr>
				<td align='right'>Mã Item <strong>kg thời hạn</strong> : </td>
				<td>
                    <input type="text" name="gift[item_mg]" value="<?php echo $gift['item_mg']; ?>" size="70"/><br />
                    Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                </td>
			</tr>
            <tr>
				<td align='right'>Mô tả Item kg thời hạn: </td>
				<td><input type="text" name="gift[msg_item_mg]" value="<?php echo $gift['msg_item_mg']; ?>" size="70"/></td>
			</tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
				<td align='right'>Mã Item <strong>có thời hạn</strong> : </td>
				<td>
                    <input type="text" name="gift[item_time_mg]" value="<?php echo $gift['item_time_mg']; ?>" size="70"/><br />
                    Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                </td>
			</tr>
            <tr>
				<td align='right'>Mô tả Item có thời hạn: </td>
				<td><input type="text" name="gift[msg_item_time_mg]" value="<?php echo $gift['msg_item_time_mg']; ?>" size="70"/></td>
			</tr>
            <tr>
				<td align='right'>Thời hạn: </td>
				<td><input type="text" name="gift[day_item_time_mg]" value="<?php echo $gift['day_item_time_mg']; ?>" size="4"/> ngày</td>
			</tr>
            <tr><td colspan="2"><hr /></td></tr>
            
            <tr><td colspan="2" align="center"><strong>Phần thưởng dành cho DL</strong></td></tr>
			<tr>
				<td align='right'>Mã Item <strong>kg thời hạn</strong> : </td>
				<td>
                    <input type="text" name="gift[item_dl]" value="<?php echo $gift['item_dl']; ?>" size="70"/><br />
                    Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                </td>
			</tr>
            <tr>
				<td align='right'>Mô tả Item kg thời hạn: </td>
				<td><input type="text" name="gift[msg_item_dl]" value="<?php echo $gift['msg_item_dl']; ?>" size="70"/></td>
			</tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
				<td align='right'>Mã Item <strong>có thời hạn</strong> : </td>
				<td>
                    <input type="text" name="gift[item_time_dl]" value="<?php echo $gift['item_time_dl']; ?>" size="70"/><br />
                    Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                </td>
			</tr>
            <tr>
				<td align='right'>Mô tả Item có thời hạn: </td>
				<td><input type="text" name="gift[msg_item_time_dl]" value="<?php echo $gift['msg_item_time_dl']; ?>" size="70"/></td>
			</tr>
            <tr>
				<td align='right'>Thời hạn: </td>
				<td><input type="text" name="gift[day_item_time_dl]" value="<?php echo $gift['day_item_time_dl']; ?>" size="4"/> ngày</td>
			</tr>
            <tr><td colspan="2"><hr /></td></tr>
            
            <tr><td colspan="2" align="center"><strong>Phần thưởng dành cho SUM</strong></td></tr>
			<tr>
				<td align='right'>Mã Item <strong>kg thời hạn</strong> : </td>
				<td>
                    <input type="text" name="gift[item_sum]" value="<?php echo $gift['item_sum']; ?>" size="70"/><br />
                    Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                </td>
			</tr>
            <tr>
				<td align='right'>Mô tả Item kg thời hạn: </td>
				<td><input type="text" name="gift[msg_item_sum]" value="<?php echo $gift['msg_item_sum']; ?>" size="70"/></td>
			</tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
				<td align='right'>Mã Item <strong>có thời hạn</strong> : </td>
				<td>
                    <input type="text" name="gift[item_time_sum]" value="<?php echo $gift['item_time_sum']; ?>" size="70"/><br />
                    Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                </td>
			</tr>
            <tr>
				<td align='right'>Mô tả Item có thời hạn: </td>
				<td><input type="text" name="gift[msg_item_time_sum]" value="<?php echo $gift['msg_item_time_sum']; ?>" size="70"/></td>
			</tr>
            <tr>
				<td align='right'>Thời hạn: </td>
				<td><input type="text" name="gift[day_item_time_sum]" value="<?php echo $gift['day_item_time_sum']; ?>" size="4"/> ngày</td>
			</tr>
            <tr><td colspan="2"><hr /></td></tr>
            
            <tr><td colspan="2" align="center"><strong>Phần thưởng dành cho RF</strong></td></tr>
			<tr>
				<td align='right'>Mã Item <strong>kg thời hạn</strong> : </td>
				<td>
                    <input type="text" name="gift[item_rf]" value="<?php echo $gift['item_rf']; ?>" size="70"/><br />
                    Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                </td>
			</tr>
            <tr>
				<td align='right'>Mô tả Item kg thời hạn: </td>
				<td><input type="text" name="gift[msg_item_rf]" value="<?php echo $gift['msg_item_rf']; ?>" size="70"/></td>
			</tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
				<td align='right'>Mã Item <strong>có thời hạn</strong> : </td>
				<td>
                    <input type="text" name="gift[item_time_rf]" value="<?php echo $gift['item_time_rf']; ?>" size="70"/><br />
                    Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                </td>
			</tr>
            <tr>
				<td align='right'>Mô tả Item có thời hạn: </td>
				<td><input type="text" name="gift[msg_item_time_rf]" value="<?php echo $gift['msg_item_time_rf']; ?>" size="70"/></td>
			</tr>
            <tr>
				<td align='right'>Thời hạn: </td>
				<td><input type="text" name="gift[day_item_time_rf]" value="<?php echo $gift['day_item_time_rf']; ?>" size="4"/> ngày</td>
			</tr>
            <tr><td colspan="2"><hr /></td></tr>
            	
			<tr>
				<td>&nbsp;</td>
				<td align="center"><input type="submit" name="Submit" value="Sửa phần thưởng thăng cấp" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></td>
			</tr>
		</table>
        </form>
<?php
		break;
		
	default: 
		echo "<b>Tạo phần thưởng tương ứng với TOP $type_name $date_name</b>.<br />
            <br /><br />
        ";

        $content = "<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
    		<tr bgcolor='#FFFFFF' >
    			<td align='center'>#</td>
    			<td align='center' ><strong>TOP</strong></td>
                <td align='center' ><strong>Phần thưởng</strong></td>
    		    <td align='center' width='50'>&nbsp;</td>
    		</tr>";

       for($date_i=0; $date_i<$date_i_max; $date_i++) {
            if($get_date == 'day') {
                switch ($date_i){ 
                	case 0:
                        $date_i_name = "Chủ nhật";
                	break;
                
                	case 1:
                        $date_i_name = "Thứ 2";
                	break;
                
                	case 2:
                        $date_i_name = "Thứ 3";
                	break;
                
                	case 3:
                        $date_i_name = "Thứ 4";
                	break;
                
                	case 4:
                        $date_i_name = "Thứ 5";
                	break;
                
                	case 5:
                        $date_i_name = "Thứ 6";
                	break;
                
                	case 6:
                        $date_i_name = "Thứ 7";
                	break;
                }
            } elseif ($get_date == 'week') {
                switch ($date_i){ 
                	case 0:
                        $date_i_name = "Tuần Chẵn";
                	break;
                
                	case 1:
                        $date_i_name = "Tuần Lẻ";
                	break;
                }
            } elseif ($get_date == 'month') {
                switch ($date_i){ 
                	case 0:
                        $date_i_name = "Tháng Chẵn";
                	break;
                
                	case 1:
                        $date_i_name = "Tháng Lẻ";
                	break;
                }
            }
            
            for($top_i=1; $top_i <= 3; $top_i++) {
                $v = $data_cfg_arr[$get_type][$get_date][$date_i][$top_i];
                $gift_des = '';
                if(strlen($v['item_all']) > 0 && strlen($v['item_all'])%32 == 0) {
                    $gift_des .= "Phần thưởng Item vĩnh viễn : ". $v['msg_item_all'] ."<br />";
                }
                if(strlen($v['item_time_all']) > 0 && strlen($v['item_time_all'])%32 == 0) {
                    $gift_des .= "Phần thưởng Item sử dụng trong <strong>". $v['day_item_time_all'] ." ngày</strong> : ". $v['msg_item_time_all'] ."<br />";
                }
                if($v['buff_day'] > 0) {
                    $gift_des .= "Bùa : ". $v['buff_day'] ." ngày.<br />";
                }
                
                $nganhang_msg = '';
                if($v['gcent_km'] > 0) $nganhang_msg .= number_format($v['gcent_km'], 0, ',', '.') ." G+";
                if($v['vcent_km'] > 0) {
                    if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
                    $nganhang_msg .= number_format($v['vcent_km'], 0, ',', '.') ." V+";
                }
                if($v['zen'] > 0) {
                    if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
                    $nganhang_msg .= number_format($v['zen'], 0, ',', '.') ." ZEN";
                }
                if($v['chao'] > 0) {
                    if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
                    $nganhang_msg .= number_format($v['chao'], 0, ',', '.') ." Chao";
                }
                if($v['cre'] > 0) {
                    if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
                    $nganhang_msg .= number_format($v['cre'], 0, ',', '.') ." Cre";
                }
                if($v['blue'] > 0) {
                    if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
                    $nganhang_msg .= number_format($v['blue'], 0, ',', '.') ." Blue";
                }
                if($v['pp_extra'] > 0) {
                    if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
                    $nganhang_msg .= number_format($v['pp_extra'], 0, ',', '.') ." PP+";
                }
                if(strlen($nganhang_msg) > 0) $nganhang_msg .= "<br />";
                $gift_des .= $nganhang_msg;
                
                $gift_dw = '';
                if(strlen($v['item_dw']) > 0 && strlen($v['item_dw'])%32 == 0) {
                    $gift_dw .= "Phần thưởng Item vĩnh viễn : ". $v['msg_item_dw'] ."<br />";
                }
                if(strlen($v['item_time_dw']) > 0 && strlen($v['item_time_dw'])%32 == 0 && $v['day_item_time_dw'] > 0) {
                    $gift_dw .= "Phần thưởng Item sử dụng trong <strong>". $v['day_item_time_dw'] ." ngày</strong> : ". $v['msg_item_time_dw'] ."<br />";
                }
                if(strlen($gift_dw) > 0) $gift_des .= "<hr /><strong>DW</strong><br />$gift_dw";
                
                $gift_dk = '';
                if(strlen($v['item_dk']) > 0 && strlen($v['item_dk'])%32 == 0) {
                    $gift_dk .= "Phần thưởng Item vĩnh viễn : ". $v['msg_item_dk'] ."<br />";
                }
                if(strlen($v['item_time_dk']) > 0 && strlen($v['item_time_dk'])%32 == 0 && $v['day_item_time_dk'] > 0) {
                    $gift_dk .= "Phần thưởng Item sử dụng trong <strong>". $v['day_item_time_dk'] ." ngày</strong> : ". $v['msg_item_time_dk'] ."<br />";
                }
                if(strlen($gift_dk) > 0) $gift_des .= "<hr /><strong>DK</strong><br />$gift_dk";
                
                $gift_elf = '';
                if(strlen($v['item_elf']) > 0 && strlen($v['item_elf'])%32 == 0) {
                    $gift_elf .= "Phần thưởng Item vĩnh viễn : ". $v['msg_item_elf'] ."<br />";
                }
                if(strlen($v['item_time_elf']) > 0 && strlen($v['item_time_elf'])%32 == 0 && $v['day_item_time_elf'] > 0) {
                    $gift_elf .= "Phần thưởng Item sử dụng trong <strong>". $v['day_item_time_elf'] ." ngày</strong> : ". $v['msg_item_time_elf'] ."<br />";
                }
                if(strlen($gift_elf) > 0) $gift_des .= "<hr /><strong>ELF</strong><br />$gift_elf";
                
                $gift_mg = '';
                if(strlen($v['item_mg']) > 0 && strlen($v['item_mg'])%32 == 0) {
                    $gift_mg .= "Phần thưởng Item vĩnh viễn : ". $v['msg_item_mg'] ."<br />";
                }
                if(strlen($v['item_time_mg']) > 0 && strlen($v['item_time_mg'])%32 == 0 && $v['day_item_time_mg'] > 0) {
                    $gift_mg .= "Phần thưởng Item sử dụng trong <strong>". $v['day_item_time_mg'] ." ngày</strong> : ". $v['msg_item_time_mg'] ."<br />";
                }
                if(strlen($gift_mg) > 0) $gift_des .= "<hr /><strong>MG</strong><br />$gift_mg";
                
                $gift_dl = '';
                if(strlen($v['item_dl']) > 0 && strlen($v['item_dl'])%32 == 0) {
                    $gift_dl .= "Phần thưởng Item vĩnh viễn : ". $v['msg_item_dl'] ."<br />";
                }
                if(strlen($v['item_time_dl']) > 0 && strlen($v['item_time_dl'])%32 == 0 && $v['day_item_time_dl'] > 0) {
                    $gift_dl .= "Phần thưởng Item sử dụng trong <strong>". $v['day_item_time_dl'] ." ngày</strong> : ". $v['msg_item_time_dl'] ."<br />";
                }
                if(strlen($gift_dl) > 0) $gift_des .= "<hr /><strong>DL</strong><br />$gift_dl";
                
                $gift_sum = '';
                if(strlen($v['item_sum']) > 0 && strlen($v['item_sum'])%32 == 0) {
                    $gift_sum .= "Phần thưởng Item vĩnh viễn : ". $v['msg_item_sum'] ."<br />";
                }
                if(strlen($v['item_time_sum']) > 0 && strlen($v['item_time_sum'])%32 == 0 && $v['day_item_time_sum'] > 0) {
                    $gift_sum .= "Phần thưởng Item sử dụng trong <strong>". $v['day_item_time_sum'] ." ngày</strong> : ". $v['msg_item_time_sum'] ."<br />";
                }
                if(strlen($gift_sum) > 0) $gift_des .= "<hr /><strong>SUM</strong><br />$gift_sum";
                
                $gift_rf = '';
                if(strlen($v['item_rf']) > 0 && strlen($v['item_rf'])%32 == 0) {
                    $gift_rf .= "Phần thưởng Item vĩnh viễn : ". $v['msg_item_rf'] ."<br />";
                }
                if(strlen($v['item_time_rf']) > 0 && strlen($v['item_time_rf'])%32 == 0 && $v['day_item_time_rf'] > 0) {
                    $gift_rf .= "Phần thưởng Item sử dụng trong <strong>". $v['day_item_time_rf'] ." ngày</strong> : ". $v['msg_item_time_rf'] ."<br />";
                }
                if(strlen($gift_rf) > 0) $gift_des .= "<hr /><strong>RF</strong><br />$gift_rf";
                
                if($date_i % 2 == 0) {
                    $tr_color = "#EEE";
                } else {
                    $tr_color = "#FFF";
                }
        		$content .= "<tr bgcolor='". $tr_color ."' >";
       			if($top_i == 1) {
                    $content .= "<td align='center' rowspan='3'>".$date_i_name."</td>";
       			}
                
                $content .= "<td align='center'>TOP ". $top_i ."</td>
                    <td align='center'>". $gift_des ."</td>
        			<td align='center'><a href='admin.php?mod=editevent&act=event_top_auto&type=". $get_type ."&date=". $get_date ."&page=edit&item=".$date_i."&top=".$top_i."' target='_self'>Sửa</a></td>
        		</tr>";
            }
    	}
            
    	$content .= "</table>";
        
        echo $content;
		
		break;
	
}

?>
				
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
