<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/config_giftcode_tanthu2.php';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

$action = $_POST[action];

if($action == 'edit')
{
    $error = "";
    
	$content = "<?php\n";
	
	$giftcode_tanthu2_use = $_POST['giftcode_tanthu2_use'];		
        $giftcode_tanthu2_use = abs(intval($giftcode_tanthu2_use));
            $content .= "\$giftcode_tanthu2_use	= $giftcode_tanthu2_use;\n";
    $giftcode_tanthu2_name = $_POST['giftcode_tanthu2_name'];    $giftcode_tanthu2_name = htmlspecialchars($giftcode_tanthu2_name, ENT_QUOTES, 'UTF-8');
            $content .= "\$giftcode_tanthu2_name	= '$giftcode_tanthu2_name';\n";
	$giftcode_tanthu2_begin = $_POST['giftcode_tanthu2_begin'];
        $content .= "\$giftcode_tanthu2_begin	= '$giftcode_tanthu2_begin';\n";
    $giftcode_tanthu2_end = $_POST['giftcode_tanthu2_end'];
        $content .= "\$giftcode_tanthu2_end	= '$giftcode_tanthu2_end';\n";
    $giftcode_tanthu2_acc_reg = $_POST['giftcode_tanthu2_acc_reg'];
        $content .= "\$giftcode_tanthu2_acc_reg	= '$giftcode_tanthu2_acc_reg';\n";
    
    $gift_all = $_POST['gift_all'];
        $gift_all = strtoupper($gift_all);
        if (!preg_match("/^[A-F0-9]*$/i", $gift_all))
    	{
            $error .= "Dữ liệu lỗi Mã Item chung <strong>kg thời hạn</strong> : $gift_all . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($gift_all)%32 != 0 ) {
            $error .= "Dữ liệu lỗi Mã Item chung <strong>kg thời hạn</strong>  : $gift_all sai cấu trúc.<br />";
        }
    		$content .= "\$gift_all	= '$gift_all';\n";
    $msg_all = $_POST['msg_all'];    $msg_all = htmlspecialchars($msg_all, ENT_QUOTES, 'UTF-8');
            $content .= "\$msg_all	= '$msg_all';\n";
            
    $gift_time_all = $_POST['gift_time_all'];
        $gift_time_all = strtoupper($gift_time_all);
        if (!preg_match("/^[A-F0-9]*$/i", $gift_time_all))
    	{
            $error .= "Dữ liệu lỗi Mã Item chung <strong>có thời hạn</strong> : $gift_time_all . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($gift_time_all)%32 != 0 ) {
            $error .= "Dữ liệu lỗi Mã Item chung <strong>có thời hạn</strong>  : $gift_time_all sai cấu trúc.<br />";
        }
    		$content .= "\$gift_time_all	= '$gift_time_all';\n";
    $msg_time_all = $_POST['msg_time_all'];    $msg_time_all = htmlspecialchars($msg_time_all, ENT_QUOTES, 'UTF-8');
            $content .= "\$msg_time_all	= '$msg_time_all';\n";
    $day_time_all = $_POST['day_time_all'];
        $day_time_all = abs(intval($day_time_all));
            $content .= "\$day_time_all	= $day_time_all;\n";        

    $gcent_km = $_POST['gcent_km'];		
        $gcent_km = abs(intval($gcent_km));
            $content .= "\$gcent_km	= $gcent_km;\n";
	$vcent_km = $_POST['vcent_km'];		
        $vcent_km = abs(intval($vcent_km));
            $content .= "\$vcent_km	= $vcent_km;\n";
	$zen = $_POST['zen'];		
        $zen = abs(intval($zen));
            $content .= "\$zen	= $zen;\n";
	$chao = $_POST['chao'];		
        $chao = abs(intval($chao));
            $content .= "\$chao	= $chao;\n";
	$cre = $_POST['cre'];		
        $cre = abs(intval($cre));
            $content .= "\$cre	= $cre;\n";
	$blue = $_POST['blue'];		
        $blue = abs(intval($blue));
            $content .= "\$blue	= $blue;\n";
	$pp_extra = $_POST['pp_extra'];		
        $pp_extra = abs(intval($pp_extra));
            $content .= "\$pp_extra	= $pp_extra;\n";
	$buff = $_POST['buff'];		
        $buff = abs(intval($buff));
            $content .= "\$buff	= $buff;\n";
	$buff_day = $_POST['buff_day'];		
        $buff_day = abs(intval($buff_day));
            $content .= "\$buff_day	= $buff_day;\n";
	
            
    // DW ---------------------------------------------------------------------------------------------------------------------
    $gift_dw = $_POST['gift_dw'];
        $gift_dw = strtoupper($gift_dw);
        if (!preg_match("/^[A-F0-9]*$/i", $gift_dw))
    	{
            $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>DW</strong> : $gift_dw . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($gift_dw)%32 != 0 ) {
            $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong>cho <strong>DW</strong>  : $gift_dw sai cấu trúc.<br />";
        }
    		$content .= "\$gift_dw	= '$gift_dw';\n";
	$msg_dw = $_POST['msg_dw'];    $msg_dw = htmlspecialchars($msg_dw, ENT_QUOTES, 'UTF-8');
            $content .= "\$msg_dw	= '$msg_dw';\n";
    
    $gift_time_dw = $_POST['gift_time_dw'];
        $gift_time_dw = strtoupper($gift_time_dw);
        if (!preg_match("/^[A-F0-9]*$/i", $gift_time_dw))
    	{
            $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>DW</strong> : $gift_time_dw . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($gift_time_dw)%32 != 0 ) {
            $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>DW</strong>  : $gift_time_dw sai cấu trúc.<br />";
        }
    		$content .= "\$gift_time_dw	= '$gift_time_dw';\n";
	$msg_time_dw = $_POST['msg_time_dw'];    $msg_time_dw = htmlspecialchars($msg_time_dw, ENT_QUOTES, 'UTF-8');
            $content .= "\$msg_time_dw	= '$msg_time_dw';\n";
    $day_time_dw = $_POST['day_time_dw'];
        $day_time_dw = abs(intval($day_time_dw));
            $content .= "\$day_time_dw	= $day_time_dw;\n";        

    
    
    
    // DK ---------------------------------------------------------------------------------------------------------------------
    $gift_dk = $_POST['gift_dk'];
        $gift_dk = strtoupper($gift_dk);
        if (!preg_match("/^[A-F0-9]*$/i", $gift_dk))
    	{
            $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>DK</strong> : $gift_dk . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($gift_dk)%32 != 0 ) {
            $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>DK</strong>  : $gift_dk sai cấu trúc.<br />";
        }
    		$content .= "\$gift_dk	= '$gift_dk';\n";
	$msg_dk = $_POST['msg_dk'];    $msg_dk = htmlspecialchars($msg_dk, ENT_QUOTES, 'UTF-8');
            $content .= "\$msg_dk	= '$msg_dk';\n";
    
    $gift_time_dk = $_POST['gift_time_dk'];
        $gift_time_dk = strtoupper($gift_time_dk);
        if (!preg_match("/^[A-F0-9]*$/i", $gift_time_dk))
    	{
            $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>DK</strong> : $gift_time_dk . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($gift_time_dk)%32 != 0 ) {
            $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>DK</strong>  : $gift_time_dk sai cấu trúc.<br />";
        }
    		$content .= "\$gift_time_dk	= '$gift_time_dk';\n";
	$msg_time_dk = $_POST['msg_time_dk'];    $msg_time_dk = htmlspecialchars($msg_time_dk, ENT_QUOTES, 'UTF-8');
            $content .= "\$msg_time_dk	= '$msg_time_dk';\n";
    $day_time_dk = $_POST['day_time_dk'];
        $day_time_dk = abs(intval($day_time_dk));
            $content .= "\$day_time_dk	= $day_time_dk;\n";   
    
    // ELF ---------------------------------------------------------------------------------------------------------------------
    $gift_elf = $_POST['gift_elf'];
        $gift_elf = strtoupper($gift_elf);
        if (!preg_match("/^[A-F0-9]*$/i", $gift_elf))
    	{
            $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>ELF</strong> : $gift_elf . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($gift_elf)%32 != 0 ) {
            $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>ELF</strong>  : $gift_elf sai cấu trúc.<br />";
        }
    		$content .= "\$gift_elf	= '$gift_elf';\n";
	$msg_elf = $_POST['msg_elf'];    $msg_elf = htmlspecialchars($msg_elf, ENT_QUOTES, 'UTF-8');
            $content .= "\$msg_elf	= '$msg_elf';\n";
    
    $gift_time_elf = $_POST['gift_time_elf'];
        $gift_time_elf = strtoupper($gift_time_elf);
        if (!preg_match("/^[A-F0-9]*$/i", $gift_time_elf))
    	{
            $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>ELF</strong> : $gift_time_elf . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($gift_time_elf)%32 != 0 ) {
            $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>ELF</strong>  : $gift_time_elf sai cấu trúc.<br />";
        }
    		$content .= "\$gift_time_elf	= '$gift_time_elf';\n";
	$msg_time_elf = $_POST['msg_time_elf'];    $msg_time_elf = htmlspecialchars($msg_time_elf, ENT_QUOTES, 'UTF-8');
            $content .= "\$msg_time_elf	= '$msg_time_elf';\n";
    $day_time_elf = $_POST['day_time_elf'];
        $day_time_elf = abs(intval($day_time_elf));
            $content .= "\$day_time_elf	= $day_time_elf;\n";   

    // MG ---------------------------------------------------------------------------------------------------------------------
    $gift_mg = $_POST['gift_mg'];
        $gift_mg = strtoupper($gift_mg);
        if (!preg_match("/^[A-F0-9]*$/i", $gift_mg))
    	{
            $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>MG</strong> : $gift_mg . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($gift_mg)%32 != 0 ) {
            $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>MG</strong>  : $gift_mg sai cấu trúc.<br />";
        }
    		$content .= "\$gift_mg	= '$gift_mg';\n";
	$msg_mg = $_POST['msg_mg'];    $msg_mg = htmlspecialchars($msg_mg, ENT_QUOTES, 'UTF-8');
            $content .= "\$msg_mg	= '$msg_mg';\n";
    
    $gift_time_mg = $_POST['gift_time_mg'];
        $gift_time_mg = strtoupper($gift_time_mg);
        if (!preg_match("/^[A-F0-9]*$/i", $gift_time_mg))
    	{
            $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>MG</strong> : $gift_time_mg . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($gift_time_mg)%32 != 0 ) {
            $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>MG</strong>  : $gift_time_mg sai cấu trúc.<br />";
        }
    		$content .= "\$gift_time_mg	= '$gift_time_mg';\n";
	$msg_time_mg = $_POST['msg_time_mg'];    $msg_time_mg = htmlspecialchars($msg_time_mg, ENT_QUOTES, 'UTF-8');
            $content .= "\$msg_time_mg	= '$msg_time_mg';\n";
    $day_time_mg = $_POST['day_time_mg'];
        $day_time_mg = abs(intval($day_time_mg));
            $content .= "\$day_time_mg	= $day_time_mg;\n";   

    // DL ---------------------------------------------------------------------------------------------------------------------
    $gift_dl = $_POST['gift_dl'];
        $gift_dl = strtoupper($gift_dl);
        if (!preg_match("/^[A-F0-9]*$/i", $gift_dl))
    	{
            $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>DL</strong> : $gift_dl . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($gift_dl)%32 != 0 ) {
            $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>DL</strong>  : $gift_dl sai cấu trúc.<br />";
        }
    		$content .= "\$gift_dl	= '$gift_dl';\n";
	$msg_dl = $_POST['msg_dl'];    $msg_dl = htmlspecialchars($msg_dl, ENT_QUOTES, 'UTF-8');
            $content .= "\$msg_dl	= '$msg_dl';\n";
    
    $gift_time_dl = $_POST['gift_time_dl'];
        $gift_time_dl = strtoupper($gift_time_dl);
        if (!preg_match("/^[A-F0-9]*$/i", $gift_time_dl))
    	{
            $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>DL</strong> : $gift_time_dl . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($gift_time_dl)%32 != 0 ) {
            $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>DL</strong>  : $gift_time_dl sai cấu trúc.<br />";
        }
    		$content .= "\$gift_time_dl	= '$gift_time_dl';\n";
	$msg_time_dl = $_POST['msg_time_dl'];    $msg_time_dl = htmlspecialchars($msg_time_dl, ENT_QUOTES, 'UTF-8');
            $content .= "\$msg_time_dl	= '$msg_time_dl';\n";
    $day_time_dl = $_POST['day_time_dl'];
        $day_time_dl = abs(intval($day_time_dl));
            $content .= "\$day_time_dl	= $day_time_dl;\n";   

    // SUM ---------------------------------------------------------------------------------------------------------------------
    $gift_sum = $_POST['gift_sum'];
        $gift_sum = strtoupper($gift_sum);
        if (!preg_match("/^[A-F0-9]*$/i", $gift_sum))
    	{
            $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>SUM</strong> : $gift_sum . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($gift_sum)%32 != 0 ) {
            $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>SUM</strong>  : $gift_sum sai cấu trúc.<br />";
        }
    		$content .= "\$gift_sum	= '$gift_sum';\n";
	$msg_sum = $_POST['msg_sum'];    $msg_sum = htmlspecialchars($msg_sum, ENT_QUOTES, 'UTF-8');
            $content .= "\$msg_sum	= '$msg_sum';\n";
    
    $gift_time_sum = $_POST['gift_time_sum'];
        $gift_time_sum = strtoupper($gift_time_sum);
        if (!preg_match("/^[A-F0-9]*$/i", $gift_time_sum))
    	{
            $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>SUM</strong> : $gift_time_sum . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($gift_time_sum)%32 != 0 ) {
            $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>SUM</strong>  : $gift_time_sum sai cấu trúc.<br />";
        }
    		$content .= "\$gift_time_sum	= '$gift_time_sum';\n";
	$msg_time_sum = $_POST['msg_time_sum'];    $msg_time_sum = htmlspecialchars($msg_time_sum, ENT_QUOTES, 'UTF-8');
            $content .= "\$msg_time_sum	= '$msg_time_sum';\n";
    $day_time_sum = $_POST['day_time_sum'];
        $day_time_sum = abs(intval($day_time_sum));
            $content .= "\$day_time_sum	= $day_time_sum;\n";   

    // RF ---------------------------------------------------------------------------------------------------------------------
    $gift_rf = $_POST['gift_rf'];
        $gift_rf = strtoupper($gift_rf);
        if (!preg_match("/^[A-F0-9]*$/i", $gift_rf))
    	{
            $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>RF</strong> : $gift_rf . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($gift_rf)%32 != 0 ) {
            $error .= "Dữ liệu lỗi Mã Item <strong>kg thời gian</strong> cho <strong>RF</strong>  : $gift_rf sai cấu trúc.<br />";
        }
    		$content .= "\$gift_rf	= '$gift_rf';\n";
	$msg_rf = $_POST['msg_rf'];    $msg_rf = htmlspecialchars($msg_rf, ENT_QUOTES, 'UTF-8');
            $content .= "\$msg_rf	= '$msg_rf';\n";
    
    $gift_time_rf = $_POST['gift_time_rf'];
        $gift_time_rf = strtoupper($gift_time_rf);
        if (!preg_match("/^[A-F0-9]*$/i", $gift_time_rf))
    	{
            $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>RF</strong> : $gift_time_rf . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($gift_time_rf)%32 != 0 ) {
            $error .= "Dữ liệu lỗi Mã Item <strong>có thời gian</strong> cho <strong>RF</strong>  : $gift_time_rf sai cấu trúc.<br />";
        }
    		$content .= "\$gift_time_rf	= '$gift_time_rf';\n";
	$msg_time_rf = $_POST['msg_time_rf'];    $msg_time_rf = htmlspecialchars($msg_time_rf, ENT_QUOTES, 'UTF-8');
            $content .= "\$msg_time_rf	= '$msg_time_rf';\n";
    $day_time_rf = $_POST['day_time_rf'];
        $day_time_rf = abs(intval($day_time_rf));
            $content .= "\$day_time_rf	= $day_time_rf;\n";   
    

	$content .= "?>";
	
	require_once('admin_cfg/function.php');
    
    if(strlen($error) > 0) {
        $notice = "<center><font color='red'>$error</font></center>";
    } else {
        replacecontent($file_edit,$content);
    	include('config/config_sync.php');
	    for($i=0; $i<count($url_hosting); $i++)
	    {
	        if($url_hosting[$i]) {
	            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
	            if($sync_send == 'OK') {
	                
	            } else {
	                $err .= $sync_send;
	            }
	        }
	    }
	    
		if($err) {
	        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
	    } else {
	    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
	    }
    }
}

include($file_edit);
?>
		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình GiftCode Tân thủ</h1>
			</div><br>
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				<form id="editconfig" name="editconfig" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
				<table>

					<tr>
						<td width="150" align='right'>Sử dụng GiftCode Tân Thủ 2 : </td>
						<td>
                            <select name="giftcode_tanthu2_use">
                                <option value="0" <?php if($giftcode_tanthu2_use == 0) echo "selected='selected'"; ?> >Không</option>
                                <option value="1" <?php if($giftcode_tanthu2_use == 1) echo "selected='selected'"; ?> >Có</option>
                              </select>
                        </td>
					</tr>
                    <tr>
						<td align='right'>Tên Event: </td>
						<td><input type="text" name="giftcode_tanthu2_name" value="<?php echo $giftcode_tanthu2_name; ?>" size="70"/></td>
					</tr>
                    <tr>
						<td align='right'>Thời gian Event : </td>
						<td>0h00 <input type="text" name="giftcode_tanthu2_begin" id="giftcode_tanthu2_begin" value="<?php echo $giftcode_tanthu2_begin; ?>" size="10" maxlength="10" /> - 24h00 <input type="text" name="giftcode_tanthu2_end" id="giftcode_tanthu2_end" value="<?php echo $giftcode_tanthu2_end; ?>" size="10" maxlength="10" /></td>
					</tr>
                    <tr>
						<td align='right'>Tài khoản đăng ký từ : </td>
						<td><input type="text" name="giftcode_tanthu2_acc_reg" id="giftcode_tanthu2_acc_reg" value="<?php echo $giftcode_tanthu2_acc_reg; ?>" size="10" maxlength="10" /> (Chỉ dành cho tài khoản đăng ký sau)</td>
					</tr>
                    <tr><td colspan="2"><hr /></td></tr>
                    
                    <tr><td colspan="2" align="center"><strong>Giftcode chung</strong></td></tr>
					<tr>
						<td align='right'>Mã Item <strong>kg thời hạn</strong> : </td>
						<td>
                            <input type="text" name="gift_all" value="<?php echo $gift_all; ?>" size="70"/><br />
                            Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                        </td>
					</tr>
                    <tr>
						<td align='right'>Mô tả Item kg thời hạn : </td>
						<td><input type="text" name="msg_all" value="<?php echo $msg_all; ?>" size="70"/></td>
					</tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
						<td align='right'>Mã Item <strong>có thời hạn</strong> : </td>
						<td>
                            <input type="text" name="gift_time_all" value="<?php echo $gift_time_all; ?>" size="70"/><br />
                            Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                        </td>
					</tr>
                    <tr>
						<td align='right'>Mô tả Item có thời hạn : </td>
						<td><input type="text" name="msg_time_all" value="<?php echo $msg_time_all; ?>" size="70"/></td>
					</tr>
                    <tr>
						<td align='right'>Thời hạn : </td>
						<td><input type="text" name="day_time_all" value="<?php echo $day_time_all; ?>" size="4"/> ngày</td>
					</tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    
                    <tr>
						<td align='right'>Gcent+ : </td>
						<td><input type="text" name="gcent_km" value="<?php echo $gcent_km; ?>" size="4"/> </td>
					</tr>
                    <tr>
						<td align='right'>Vcent+ : </td>
						<td><input type="text" name="vcent_km" value="<?php echo $vcent_km; ?>" size="4"/> </td>
					</tr>
                    <tr>
						<td align='right'>ZEN ngân hàng : </td>
						<td><input type="text" name="zen" value="<?php echo $zen; ?>" size="4"/> </td>
					</tr>
                    <tr>
						<td align='right'>Ngọc Hỗn Nguyên ngân hàng : </td>
						<td><input type="text" name="chao" value="<?php echo $chao; ?>" size="4"/> </td>
					</tr>
                    <tr>
						<td align='right'>Ngọc Sáng Tạo ngân hàng : </td>
						<td><input type="text" name="cre" value="<?php echo $cre; ?>" size="4"/> </td>
					</tr>
                    <tr>
						<td align='right'>Lông Vũ ngân hàng : </td>
						<td><input type="text" name="blue" value="<?php echo $blue; ?>" size="4"/> </td>
					</tr>
                    <tr>
						<td align='right'>PPoint+ : </td>
						<td><input type="text" name="pp_extra" value="<?php echo $pp_extra; ?>" size="4"/> </td>
					</tr>
                    <tr>
						<td align='right'>Bùa : </td>
						<td>
                            <select name="buff">
                                <option value="0" <?php if(!isset($buff) || $buff == 0) echo 'selected="selected"'; ?> >Không</option>
                                <option value="6699" <?php if(isset($buff) && $buff == 6699) echo 'selected="selected"'; ?> >Bùa tăng Exp</option>
                                <option value="6700" <?php if(isset($buff) && $buff == 6700) echo 'selected="selected"'; ?> >Bùa Thiên Sứ</option>
                                <option value="6749" <?php if(isset($buff) && $buff == 6749) echo 'selected="selected"'; ?> >Bùa Master</option>
                            </select> 
                            <input type="text" name="buff_day" value="<?php echo $buff_day; ?>" size="4"/> ngày
                        </td>
					</tr>
                    <tr><td colspan="2"><hr /></td></tr>
                    
                    
                    <tr><td colspan="2" align="center"><strong>Giftcode dành cho DW</strong></td></tr>
					<tr>
						<td align='right'>Mã Item <strong>kg thời hạn</strong> : </td>
						<td>
                            <input type="text" name="gift_dw" value="<?php echo $gift_dw; ?>" size="70"/><br />
                            Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                        </td>
					</tr>
                    <tr>
						<td align='right'>Mô tả Item kg thời hạn: </td>
						<td><input type="text" name="msg_dw" value="<?php echo $msg_dw; ?>" size="70"/></td>
					</tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
						<td align='right'>Mã Item <strong>có thời hạn</strong> : </td>
						<td>
                            <input type="text" name="gift_time_dw" value="<?php echo $gift_time_dw; ?>" size="70"/><br />
                            Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                        </td>
					</tr>
                    <tr>
						<td align='right'>Mô tả Item có thời hạn: </td>
						<td><input type="text" name="msg_time_dw" value="<?php echo $msg_time_dw; ?>" size="70"/></td>
					</tr>
                    <tr>
						<td align='right'>Thời hạn: </td>
						<td><input type="text" name="day_time_dw" value="<?php echo $day_time_dw; ?>" size="4"/> ngày</td>
					</tr>
                    <tr><td colspan="2"><hr /></td></tr>
                    
					<tr><td colspan="2" align="center"><strong>Giftcode dành cho DK</strong></td></tr>
					<tr>
						<td align='right'>Mã Item <strong>kg thời hạn</strong> : </td>
						<td>
                            <input type="text" name="gift_dk" value="<?php echo $gift_dk; ?>" size="70"/><br />
                            Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                        </td>
					</tr>
                    <tr>
						<td align='right'>Mô tả Item kg thời hạn: </td>
						<td><input type="text" name="msg_dk" value="<?php echo $msg_dk; ?>" size="70"/></td>
					</tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
						<td align='right'>Mã Item <strong>có thời hạn</strong> : </td>
						<td>
                            <input type="text" name="gift_time_dk" value="<?php echo $gift_time_dk; ?>" size="70"/><br />
                            Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                        </td>
					</tr>
                    <tr>
						<td align='right'>Mô tả Item có thời hạn: </td>
						<td><input type="text" name="msg_time_dk" value="<?php echo $msg_time_dk; ?>" size="70"/></td>
					</tr>
                    <tr>
						<td align='right'>Thời hạn: </td>
						<td><input type="text" name="day_time_dk" value="<?php echo $day_time_dk; ?>" size="4"/> ngày</td>
					</tr>
                    <tr><td colspan="2"><hr /></td></tr>
                    
                    <tr><td colspan="2" align="center"><strong>Giftcode dành cho ELF</strong></td></tr>
					<tr>
						<td align='right'>Mã Item <strong>kg thời hạn</strong> : </td>
						<td>
                            <input type="text" name="gift_elf" value="<?php echo $gift_elf; ?>" size="70"/><br />
                            Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                        </td>
					</tr>
                    <tr>
						<td align='right'>Mô tả Item kg thời hạn: </td>
						<td><input type="text" name="msg_elf" value="<?php echo $msg_elf; ?>" size="70"/></td>
					</tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
						<td align='right'>Mã Item <strong>có thời hạn</strong> : </td>
						<td>
                            <input type="text" name="gift_time_elf" value="<?php echo $gift_time_elf; ?>" size="70"/><br />
                            Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                        </td>
					</tr>
                    <tr>
						<td align='right'>Mô tả Item có thời hạn: </td>
						<td><input type="text" name="msg_time_elf" value="<?php echo $msg_time_elf; ?>" size="70"/></td>
					</tr>
                    <tr>
						<td align='right'>Thời hạn: </td>
						<td><input type="text" name="day_time_elf" value="<?php echo $day_time_elf; ?>" size="4"/> ngày</td>
					</tr>
                    <tr><td colspan="2"><hr /></td></tr>
                    
                    <tr><td colspan="2" align="center"><strong>Giftcode dành cho MG</strong></td></tr>
					<tr>
						<td align='right'>Mã Item <strong>kg thời hạn</strong> : </td>
						<td>
                            <input type="text" name="gift_mg" value="<?php echo $gift_mg; ?>" size="70"/><br />
                            Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                        </td>
					</tr>
                    <tr>
						<td align='right'>Mô tả Item kg thời hạn: </td>
						<td><input type="text" name="msg_mg" value="<?php echo $msg_mg; ?>" size="70"/></td>
					</tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
						<td align='right'>Mã Item <strong>có thời hạn</strong> : </td>
						<td>
                            <input type="text" name="gift_time_mg" value="<?php echo $gift_time_mg; ?>" size="70"/><br />
                            Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                        </td>
					</tr>
                    <tr>
						<td align='right'>Mô tả Item có thời hạn: </td>
						<td><input type="text" name="msg_time_mg" value="<?php echo $msg_time_mg; ?>" size="70"/></td>
					</tr>
                    <tr>
						<td align='right'>Thời hạn: </td>
						<td><input type="text" name="day_time_mg" value="<?php echo $day_time_mg; ?>" size="4"/> ngày</td>
					</tr>
                    <tr><td colspan="2"><hr /></td></tr>
                    
                    <tr><td colspan="2" align="center"><strong>Giftcode dành cho DL</strong></td></tr>
					<tr>
						<td align='right'>Mã Item <strong>kg thời hạn</strong> : </td>
						<td>
                            <input type="text" name="gift_dl" value="<?php echo $gift_dl; ?>" size="70"/><br />
                            Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                        </td>
					</tr>
                    <tr>
						<td align='right'>Mô tả Item kg thời hạn: </td>
						<td><input type="text" name="msg_dl" value="<?php echo $msg_dl; ?>" size="70"/></td>
					</tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
						<td align='right'>Mã Item <strong>có thời hạn</strong> : </td>
						<td>
                            <input type="text" name="gift_time_dl" value="<?php echo $gift_time_dl; ?>" size="70"/><br />
                            Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                        </td>
					</tr>
                    <tr>
						<td align='right'>Mô tả Item có thời hạn: </td>
						<td><input type="text" name="msg_time_dl" value="<?php echo $msg_time_dl; ?>" size="70"/></td>
					</tr>
                    <tr>
						<td align='right'>Thời hạn: </td>
						<td><input type="text" name="day_time_dl" value="<?php echo $day_time_dl; ?>" size="4"/> ngày</td>
					</tr>
                    <tr><td colspan="2"><hr /></td></tr>
                    
                    <tr><td colspan="2" align="center"><strong>Giftcode dành cho SUM</strong></td></tr>
					<tr>
						<td align='right'>Mã Item <strong>kg thời hạn</strong> : </td>
						<td>
                            <input type="text" name="gift_sum" value="<?php echo $gift_sum; ?>" size="70"/><br />
                            Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                        </td>
					</tr>
                    <tr>
						<td align='right'>Mô tả Item kg thời hạn: </td>
						<td><input type="text" name="msg_sum" value="<?php echo $msg_sum; ?>" size="70"/></td>
					</tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
						<td align='right'>Mã Item <strong>có thời hạn</strong> : </td>
						<td>
                            <input type="text" name="gift_time_sum" value="<?php echo $gift_time_sum; ?>" size="70"/><br />
                            Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                        </td>
					</tr>
                    <tr>
						<td align='right'>Mô tả Item có thời hạn: </td>
						<td><input type="text" name="msg_time_sum" value="<?php echo $msg_time_sum; ?>" size="70"/></td>
					</tr>
                    <tr>
						<td align='right'>Thời hạn: </td>
						<td><input type="text" name="day_time_sum" value="<?php echo $day_time_sum; ?>" size="4"/> ngày</td>
					</tr>
                    <tr><td colspan="2"><hr /></td></tr>
                    
                    <tr><td colspan="2" align="center"><strong>Giftcode dành cho RF</strong></td></tr>
					<tr>
						<td align='right'>Mã Item <strong>kg thời hạn</strong> : </td>
						<td>
                            <input type="text" name="gift_rf" value="<?php echo $gift_rf; ?>" size="70"/><br />
                            Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                        </td>
					</tr>
                    <tr>
						<td align='right'>Mô tả Item kg thời hạn: </td>
						<td><input type="text" name="msg_rf" value="<?php echo $msg_rf; ?>" size="70"/></td>
					</tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
						<td align='right'>Mã Item <strong>có thời hạn</strong> : </td>
						<td>
                            <input type="text" name="gift_time_rf" value="<?php echo $gift_time_rf; ?>" size="70"/><br />
                            Gồm nhiều 32 mã ItemCode lấy từ MuMaker nối tiếp nhau.
                        </td>
					</tr>
                    <tr>
						<td align='right'>Mô tả Item có thời hạn: </td>
						<td><input type="text" name="msg_time_rf" value="<?php echo $msg_time_rf; ?>" size="70"/></td>
					</tr>
                    <tr>
						<td align='right'>Thời hạn: </td>
						<td><input type="text" name="day_time_rf" value="<?php echo $day_time_rf; ?>" size="4"/> ngày</td>
					</tr>
                    <tr><td colspan="2"><hr /></td></tr>
                    
                    
                    	
					<tr>
						<td>&nbsp;</td>
						<td align="center"><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></td>
					</tr>
				</table>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
