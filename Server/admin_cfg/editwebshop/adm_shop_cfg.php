<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/config_webshop.php';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

$action = $_POST[action];

if($action == 'edit')
{
	$content = "<?php\n";
	    
    $price_exl_plus = $_POST['price_exl_plus'];
    foreach($price_exl_plus as $k => $v) {
        $v = abs(intval($v));
    		$content .= "\$price_exl_plus[$k]	= $v;\n";
    }
    
    $price_luck = $_POST['price_luck'];
        $price_luck = abs(intval($price_luck));
    		$content .= "\$price_luck	= $price_luck;\n";
    
    $price_option = $_POST['price_option'];
    foreach($price_option as $k => $v) {
        $v = abs(intval($v));
    		$content .= "\$price_option[$k]	= $v;\n";
    }
    
    $item_lv_max = $_POST['item_lv_max'];
        $item_lv_max = abs(intval($item_lv_max));
    		$content .= "\$item_lv_max	= $item_lv_max;\n";
    
    $price_lv_plus = $_POST['price_lv_plus'];
    foreach($price_lv_plus as $k => $v) {
        $v = abs(intval($v));
    		$content .= "\$price_lv_plus[$k]	= $v;\n";
    }
    
    $vukhi_exl = $_POST['vukhi_exl'];
    foreach($vukhi_exl as $vk_k => $vk_v) {
        $vk_v = abs(intval($vk_v));
    		$content .= "\$vukhi_exl[$vk_k]	= $vk_v;\n";
    }
    
    $giap_exl = $_POST['giap_exl'];
    foreach($giap_exl as $g_k => $g_v) {
        $g_v = abs(intval($g_v));
    		$content .= "\$giap_exl[$g_k]	= $g_v;\n";
    }
    
    $wing2_exl = $_POST['wing2_exl'];
    foreach($wing2_exl as $w2_k => $w2_v) {
        $w2_v = abs(intval($w2_v));
    		$content .= "\$wing2_exl[$w2_k]	= $w2_v;\n";
    }
    
    $pent_ring_price_option = $_POST['pent_ring_price_option'];
    foreach($pent_ring_price_option as $k => $v) {
        $v = abs(intval($v));
    		$content .= "\$pent_ring_price_option[$k]	= $v;\n";
    }
    
    $wing25_price_option = $_POST['wing25_price_option'];
    foreach($wing25_price_option as $k => $v) {
        $v = abs(intval($v));
    		$content .= "\$wing25_price_option[$k]	= $v;\n";
    }
    $wing25_price_lv_plus = $_POST['wing25_price_lv_plus'];
    foreach($wing25_price_lv_plus as $k => $v) {
        $v = abs(intval($v));
    		$content .= "\$wing25_price_lv_plus[$k]	= $v;\n";
    }
    $wing25_exl = $_POST['wing25_exl'];
    foreach($wing25_exl as $k => $v) {
        $v = abs(intval($v));
    		$content .= "\$wing25_exl[$k]	= $v;\n";
    }
    
    $w3_price_option = $_POST['w3_price_option'];
    foreach($w3_price_option as $k => $v) {
        $v = abs(intval($v));
    		$content .= "\$w3_price_option[$k]	= $v;\n";
    }
    $w3_price_lv_plus = $_POST['w3_price_lv_plus'];
    foreach($w3_price_lv_plus as $k => $v) {
        $v = abs(intval($v));
    		$content .= "\$w3_price_lv_plus[$k]	= $v;\n";
    }
    $wing3_exl = $_POST['wing3_exl'];
    foreach($wing3_exl as $w3_k => $w3_v) {
        $w3_v = abs(intval($w3_v));
    		$content .= "\$wing3_exl[$w3_k]	= $w3_v;\n";
    }
    
    $w4_price_option = $_POST['w4_price_option'];
    foreach($w4_price_option as $k => $v) {
        $v = abs(intval($v));
    		$content .= "\$w4_price_option[$k]	= $v;\n";
    }
    $w4_price_lv_plus = $_POST['w4_price_lv_plus'];
    foreach($w4_price_lv_plus as $k => $v) {
        $v = abs(intval($v));
    		$content .= "\$w4_price_lv_plus[$k]	= $v;\n";
    }
    $wing4_exl = $_POST['wing4_exl'];
    foreach($wing4_exl as $k => $v) {
        $v = abs(intval($v));
    		$content .= "\$wing4_exl[$k]	= $v;\n";
    }
    
	$content .= "?>";
	
	require_once('admin_cfg/function.php');
	replacecontent($file_edit,$content);
	
	include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}

include($file_edit);
?>


		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình Hệ thống WebShop</h1>
			</div><br />
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				
                <form id="editconfig" name="editconfig" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
                <center>
                    <strong>Thiết Lập Chung</strong><br />
                </center>
                
                Item mua có 1 dòng hoàn hảo trả thêm : <input type="text" name="price_exl_plus[1]" value="<?php echo $price_exl_plus[1]; ?>" size="5" /> Gcent<br />
                Item mua có 2 dòng hoàn hảo trả thêm : <input type="text" name="price_exl_plus[2]" value="<?php echo $price_exl_plus[2]; ?>" size="5" /> Gcent<br />
                Item mua có 3 dòng hoàn hảo trả thêm : <input type="text" name="price_exl_plus[3]" value="<?php echo $price_exl_plus[3]; ?>" size="5" /> Gcent<br />
                Item mua có 4 dòng hoàn hảo trả thêm : <input type="text" name="price_exl_plus[4]" value="<?php echo $price_exl_plus[4]; ?>" size="5" /> Gcent<br />
                Item mua có 5 dòng hoàn hảo trả thêm : <input type="text" name="price_exl_plus[5]" value="<?php echo $price_exl_plus[5]; ?>" size="5" /> Gcent<br />
                Item mua có 6 dòng hoàn hảo trả thêm : <input type="text" name="price_exl_plus[6]" value="<?php echo $price_exl_plus[6]; ?>" size="5" /> Gcent<br />
                <br />
                
                Item mua có Luck trả thêm : <input type="text" name="price_luck" value="<?php echo $price_luck; ?>" size="5" /> Gcent<br />
                <br />
                
                Item mua có Option 4 : <input type="text" name="price_option[1]" value="<?php echo $price_option[1]; ?>" size="5" /> Gcent<br />
                Item mua có Option 8 : <input type="text" name="price_option[2]" value="<?php echo $price_option[2]; ?>" size="5" /> Gcent<br />
                Item mua có Option 12 : <input type="text" name="price_option[3]" value="<?php echo $price_option[3]; ?>" size="5" /> Gcent<br />
                Item mua có Option 16 : <input type="text" name="price_option[4]" value="<?php echo $price_option[4]; ?>" size="5" /> Gcent<br />
                Item mua có Option 20 : <input type="text" name="price_option[5]" value="<?php echo $price_option[5]; ?>" size="5" /> Gcent<br />
                Item mua có Option 24 : <input type="text" name="price_option[6]" value="<?php echo $price_option[6]; ?>" size="5" /> Gcent<br />
                Item mua có Option 28 : <input type="text" name="price_option[7]" value="<?php echo $price_option[7]; ?>" size="5" /> Gcent<br />
                <br />
                
                Cấp độ Item tối đa : <input type="text" name="item_lv_max" value="<?php echo $item_lv_max; ?>" size="5" /> <br />
                Item mua +1 trả thêm : <input type="text" name="price_lv_plus[1]" value="<?php echo $price_lv_plus[1]; ?>" size="5" /> Gcent<br />
                Item mua +2 trả thêm : <input type="text" name="price_lv_plus[2]" value="<?php echo $price_lv_plus[2]; ?>" size="5" /> Gcent<br />
                Item mua +3 trả thêm : <input type="text" name="price_lv_plus[3]" value="<?php echo $price_lv_plus[3]; ?>" size="5" /> Gcent<br />
                Item mua +4 trả thêm : <input type="text" name="price_lv_plus[4]" value="<?php echo $price_lv_plus[4]; ?>" size="5" /> Gcent<br />
                Item mua +5 trả thêm : <input type="text" name="price_lv_plus[5]" value="<?php echo $price_lv_plus[5]; ?>" size="5" /> Gcent<br />
                Item mua +6 trả thêm : <input type="text" name="price_lv_plus[6]" value="<?php echo $price_lv_plus[6]; ?>" size="5" /> Gcent<br />
                Item mua +7 trả thêm : <input type="text" name="price_lv_plus[7]" value="<?php echo $price_lv_plus[7]; ?>" size="5" /> Gcent<br />
                Item mua +8 trả thêm : <input type="text" name="price_lv_plus[8]" value="<?php echo $price_lv_plus[8]; ?>" size="5" /> Gcent<br />
                Item mua +9 trả thêm : <input type="text" name="price_lv_plus[9]" value="<?php echo $price_lv_plus[9]; ?>" size="5" /> Gcent<br />
                Item mua +10 trả thêm : <input type="text" name="price_lv_plus[10]" value="<?php echo $price_lv_plus[10]; ?>" size="5" /> Gcent<br />
                Item mua +11 trả thêm : <input type="text" name="price_lv_plus[11]" value="<?php echo $price_lv_plus[11]; ?>" size="5" /> Gcent<br />
                Item mua +12 trả thêm : <input type="text" name="price_lv_plus[12]" value="<?php echo $price_lv_plus[12]; ?>" size="5" /> Gcent<br />
                Item mua +13 trả thêm : <input type="text" name="price_lv_plus[13]" value="<?php echo $price_lv_plus[13]; ?>" size="5" /> Gcent<br />
                Item mua +14 trả thêm : <input type="text" name="price_lv_plus[14]" value="<?php echo $price_lv_plus[14]; ?>" size="5" /> Gcent<br />
                Item mua +15 trả thêm : <input type="text" name="price_lv_plus[15]" value="<?php echo $price_lv_plus[15]; ?>" size="5" /> Gcent<br />
                
                <hr />
                <center>
                    <strong>Chi phí dòng hoàn hảo</strong><br />
                </center><br />
                <strong><font color="red">Vũ khí + Dây Chuyền</font></strong> :<br />
                <table border="0">
                    <tr>
                        <td align="right">Tăng lượng MANA khi giết quái (MANA/8) :</td>
                        <td align="left"><input type="text" name="vukhi_exl[1]" value="<?php echo $vukhi_exl[1]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Tăng lượng LIFE khi giết quái (LIFE/8) :</td>
                        <td align="left"><input type="text" name="vukhi_exl[2]" value="<?php echo $vukhi_exl[2]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Tốc độ tấn công +7 :</td>
                        <td align="left"><input type="text" name="vukhi_exl[3]" value="<?php echo $vukhi_exl[3]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Tăng lực tấn công 2% :</td>
                        <td align="left"><input type="text" name="vukhi_exl[4]" value="<?php echo $vukhi_exl[4]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Tăng lực tấn công (Cấp độ/20) :</td>
                        <td align="left"><input type="text" name="vukhi_exl[5]" value="<?php echo $vukhi_exl[5]; ?> "size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Khả năng xuất hiện lực tấn công hoàn hảo +10% :</td>
                        <td align="left"><input type="text" name="vukhi_exl[6]" value="<?php echo $vukhi_exl[6]; ?>" size="5" /> Gcent</td>
                    </tr>
                </table>
                
                <hr />
                <strong><font color="red">Giáp + Khiên + Nhẫn</font></strong> :<br />
                
                <table border="0">
                    <tr>
                        <td align="right">Lượng ZEN rơi ra khi giết quái +40% :</td>
                        <td align="left"><input type="text" name="giap_exl[1]" value="<?php echo $giap_exl[1]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Khả năng xuất hiện phòng thủ hoàn hảo +10% :</td>
                        <td align="left"><input type="text" name="giap_exl[2]" value="<?php echo $giap_exl[2]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Phản hồi sát thương +5% :</td>
                        <td align="left"><input type="text" name="giap_exl[3]" value="<?php echo $giap_exl[3]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Giảm sát thương +4% :</td>
                        <td align="left"><input type="text" name="giap_exl[4]" value="<?php echo $giap_exl[4]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Lượng MANA tối đa +4% :</td>
                        <td align="left"><input type="text" name="giap_exl[5]" value="<?php echo $giap_exl[5]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Lượng HP tối đa +4% :</td>
                        <td align="left"><input type="text" name="giap_exl[6]" value="<?php echo $giap_exl[6]; ?>" size="5" /> Gcent</td>
                    </tr>
                </table>
                
                
                <hr />
                <strong><font color="red">Cánh cấp 2</font></strong> :<br />
                
                <table border="0">
                    <tr>
                        <td align="right">+ 115 Lượng HP tối đa :</td>
                        <td align="left"><input type="text" name="wing2_exl[1]" value="<?php echo $wing2_exl[1]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">+ 115 Lượng MP tối đa :</td>
                        <td align="left"><input type="text" name="wing2_exl[2]" value="<?php echo $wing2_exl[2]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Khả năng loại bỏ phòng thủ đối phương +3% :</td>
                        <td align="left"><input type="text" name="wing2_exl[3]" value="<?php echo $wing2_exl[3]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">+ 50 Lực hành động tối đa :</td>
                        <td align="left"><input type="text" name="wing2_exl[4]" value="<?php echo $wing2_exl[4]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Tốc độ tấn công +7 :</td>
                        <td align="left"><input type="text" name="wing2_exl[5]" value="<?php echo $wing2_exl[5]; ?>" size="5" /> Gcent</td>
                    </tr>
                </table>
                
                <hr />
                <strong><font color="red">Dây chuyền + Nhẫn</font></strong> :<br /><br />
                Dây chuyền - Nhẫn - hồi HP 1% : <input type="text" name="pent_ring_price_option[1]" value="<?php echo $pent_ring_price_option[1]; ?>" size="5" /> Gcent<br />
                Dây chuyền - Nhẫn - hồi HP 2% : <input type="text" name="pent_ring_price_option[2]" value="<?php echo $pent_ring_price_option[2]; ?>" size="5" /> Gcent<br />
                Dây chuyền - Nhẫn - hồi HP 3% : <input type="text" name="pent_ring_price_option[3]" value="<?php echo $pent_ring_price_option[3]; ?>" size="5" /> Gcent<br />
                Dây chuyền - Nhẫn - hồi HP 4% : <input type="text" name="pent_ring_price_option[4]" value="<?php echo $pent_ring_price_option[4]; ?>" size="5" /> Gcent<br />
                Dây chuyền - Nhẫn - hồi HP 5% : <input type="text" name="pent_ring_price_option[5]" value="<?php echo $pent_ring_price_option[5]; ?>" size="5" /> Gcent<br />
                Dây chuyền - Nhẫn - hồi HP 6% : <input type="text" name="pent_ring_price_option[6]" value="<?php echo $pent_ring_price_option[6]; ?>" size="5" /> Gcent<br />
                Dây chuyền - Nhẫn - hồi HP 7% : <input type="text" name="pent_ring_price_option[7]" value="<?php echo $pent_ring_price_option[7]; ?>" size="5" /> Gcent<br />
                
                <hr />
                <strong><font color="red">Cánh cấp 2.5</font></strong> :<br />
                
                <table border="0">
                    <tr>
                        <td align="right">Cơ hội loại bỏ sức phòng thủ 3% :</td>
                        <td align="left"><input type="text" name="wing25_exl[1]" value="<?php echo $wing25_exl[1]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Phản đòn khi cận chiến 3% :</td>
                        <td align="left"><input type="text" name="wing25_exl[2]" value="<?php echo $wing25_exl[2]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Khả năng hồi phục hoàn toàn HP 3% :</td>
                        <td align="left"><input type="text" name="wing25_exl[3]" value="<?php echo $wing25_exl[3]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Khả năng hồi phục hoàn toàn nội lực 3% :</td>
                        <td align="left"><input type="text" name="wing25_exl[4]" value="<?php echo $wing25_exl[4]; ?>" size="5" /> Gcent</td>
                    </tr>
                </table>
                
                <br />
                
                Wing 2.5 - hồi HP 1% : <input type="text" name="wing25_price_option[1]" value="<?php echo $wing25_price_option[1]; ?>" size="5" /> Gcent<br />
                Wing 2.5 - hồi HP 2% : <input type="text" name="wing25_price_option[2]" value="<?php echo $wing25_price_option[2]; ?>" size="5" /> Gcent<br />
                Wing 2.5 - hồi HP 3% : <input type="text" name="wing25_price_option[3]" value="<?php echo $wing25_price_option[3]; ?>" size="5" /> Gcent<br />
                Wing 2.5 - hồi HP 4% : <input type="text" name="wing25_price_option[4]" value="<?php echo $wing25_price_option[4]; ?>" size="5" /> Gcent<br />
                Wing 2.5 - hồi HP 5% : <input type="text" name="wing25_price_option[5]" value="<?php echo $wing25_price_option[5]; ?>" size="5" /> Gcent<br />
                Wing 2.5 - hồi HP 6% : <input type="text" name="wing25_price_option[6]" value="<?php echo $wing25_price_option[6]; ?>" size="5" /> Gcent<br />
                Wing 2.5 - hồi HP 7% : <input type="text" name="wing25_price_option[7]" value="<?php echo $wing25_price_option[7]; ?>" size="5" /> Gcent<br />
                <br />
                
                Wing 2.5 +1 trả thêm : <input type="text" name="wing25_price_lv_plus[1]" value="<?php echo $wing25_price_lv_plus[1]; ?>" size="5" /> Gcent<br />
                Wing 2.5 +2 trả thêm : <input type="text" name="wing25_price_lv_plus[2]" value="<?php echo $wing25_price_lv_plus[2]; ?>" size="5" /> Gcent<br />
                Wing 2.5 +3 trả thêm : <input type="text" name="wing25_price_lv_plus[3]" value="<?php echo $wing25_price_lv_plus[3]; ?>" size="5" /> Gcent<br />
                Wing 2.5 +4 trả thêm : <input type="text" name="wing25_price_lv_plus[4]" value="<?php echo $wing25_price_lv_plus[4]; ?>" size="5" /> Gcent<br />
                Wing 2.5 +5 trả thêm : <input type="text" name="wing25_price_lv_plus[5]" value="<?php echo $wing25_price_lv_plus[5]; ?>" size="5" /> Gcent<br />
                Wing 2.5 +6 trả thêm : <input type="text" name="wing25_price_lv_plus[6]" value="<?php echo $wing25_price_lv_plus[6]; ?>" size="5" /> Gcent<br />
                Wing 2.5 +7 trả thêm : <input type="text" name="wing25_price_lv_plus[7]" value="<?php echo $wing25_price_lv_plus[7]; ?>" size="5" /> Gcent<br />
                Wing 2.5 +8 trả thêm : <input type="text" name="wing25_price_lv_plus[8]" value="<?php echo $wing25_price_lv_plus[8]; ?>" size="5" /> Gcent<br />
                Wing 2.5 +9 trả thêm : <input type="text" name="wing25_price_lv_plus[9]" value="<?php echo $wing25_price_lv_plus[9]; ?>" size="5" /> Gcent<br />
                Wing 2.5 +10 trả thêm : <input type="text" name="wing25_price_lv_plus[10]" value="<?php echo $wing25_price_lv_plus[10]; ?>" size="5" /> Gcent<br />
                Wing 2.5 +11 trả thêm : <input type="text" name="wing25_price_lv_plus[11]" value="<?php echo $wing25_price_lv_plus[11]; ?>" size="5" /> Gcent<br />
                Wing 2.5 +12 trả thêm : <input type="text" name="wing25_price_lv_plus[12]" value="<?php echo $wing25_price_lv_plus[12]; ?>" size="5" /> Gcent<br />
                Wing 2.5 +13 trả thêm : <input type="text" name="wing25_price_lv_plus[13]" value="<?php echo $wing25_price_lv_plus[13]; ?>" size="5" /> Gcent<br />
                Wing 2.5 +14 trả thêm : <input type="text" name="wing25_price_lv_plus[14]" value="<?php echo $wing25_price_lv_plus[14]; ?>" size="5" /> Gcent<br />
                Wing 2.5 +15 trả thêm : <input type="text" name="wing25_price_lv_plus[15]" value="<?php echo $wing25_price_lv_plus[15]; ?>" size="5" /> Gcent<br />
                
                <hr />
                <strong><font color="red">Cánh cấp 3</font></strong> :<br />
                
                <table border="0">
                    <tr>
                        <td align="right">Cơ hội loại bỏ sức phòng thủ 5% :</td>
                        <td align="left"><input type="text" name="wing3_exl[1]" value="<?php echo $wing3_exl[1]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Phản đòn khi cận chiến 5% :</td>
                        <td align="left"><input type="text" name="wing3_exl[2]" value="<?php echo $wing3_exl[2]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Khả năng hồi phục hoàn toàn HP 5% :</td>
                        <td align="left"><input type="text" name="wing3_exl[3]" value="<?php echo $wing3_exl[3]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Khả năng hồi phục hoàn toàn nội lực 5% :</td>
                        <td align="left"><input type="text" name="wing3_exl[4]" value="<?php echo $wing3_exl[4]; ?>" size="5" /> Gcent</td>
                    </tr>
                </table>
                
                <br />
                
                Wing 3 - hồi HP 1% : <input type="text" name="w3_price_option[1]" value="<?php echo $w3_price_option[1]; ?>" size="5" /> Gcent<br />
                Wing 3 - hồi HP 2% : <input type="text" name="w3_price_option[2]" value="<?php echo $w3_price_option[2]; ?>" size="5" /> Gcent<br />
                Wing 3 - hồi HP 3% : <input type="text" name="w3_price_option[3]" value="<?php echo $w3_price_option[3]; ?>" size="5" /> Gcent<br />
                Wing 3 - hồi HP 4% : <input type="text" name="w3_price_option[4]" value="<?php echo $w3_price_option[4]; ?>" size="5" /> Gcent<br />
                Wing 3 - hồi HP 5% : <input type="text" name="w3_price_option[5]" value="<?php echo $w3_price_option[5]; ?>" size="5" /> Gcent<br />
                Wing 3 - hồi HP 6% : <input type="text" name="w3_price_option[6]" value="<?php echo $w3_price_option[6]; ?>" size="5" /> Gcent<br />
                Wing 3 - hồi HP 7% : <input type="text" name="w3_price_option[7]" value="<?php echo $w3_price_option[7]; ?>" size="5" /> Gcent<br />
                <br />
                
                W3 +1 trả thêm : <input type="text" name="w3_price_lv_plus[1]" value="<?php echo $w3_price_lv_plus[1]; ?>" size="5" /> Gcent<br />
                W3 +2 trả thêm : <input type="text" name="w3_price_lv_plus[2]" value="<?php echo $w3_price_lv_plus[2]; ?>" size="5" /> Gcent<br />
                W3 +3 trả thêm : <input type="text" name="w3_price_lv_plus[3]" value="<?php echo $w3_price_lv_plus[3]; ?>" size="5" /> Gcent<br />
                W3 +4 trả thêm : <input type="text" name="w3_price_lv_plus[4]" value="<?php echo $w3_price_lv_plus[4]; ?>" size="5" /> Gcent<br />
                W3 +5 trả thêm : <input type="text" name="w3_price_lv_plus[5]" value="<?php echo $w3_price_lv_plus[5]; ?>" size="5" /> Gcent<br />
                W3 +6 trả thêm : <input type="text" name="w3_price_lv_plus[6]" value="<?php echo $w3_price_lv_plus[6]; ?>" size="5" /> Gcent<br />
                W3 +7 trả thêm : <input type="text" name="w3_price_lv_plus[7]" value="<?php echo $w3_price_lv_plus[7]; ?>" size="5" /> Gcent<br />
                W3 +8 trả thêm : <input type="text" name="w3_price_lv_plus[8]" value="<?php echo $w3_price_lv_plus[8]; ?>" size="5" /> Gcent<br />
                W3 +9 trả thêm : <input type="text" name="w3_price_lv_plus[9]" value="<?php echo $w3_price_lv_plus[9]; ?>" size="5" /> Gcent<br />
                W3 +10 trả thêm : <input type="text" name="w3_price_lv_plus[10]" value="<?php echo $w3_price_lv_plus[10]; ?>" size="5" /> Gcent<br />
                W3 +11 trả thêm : <input type="text" name="w3_price_lv_plus[11]" value="<?php echo $w3_price_lv_plus[11]; ?>" size="5" /> Gcent<br />
                W3 +12 trả thêm : <input type="text" name="w3_price_lv_plus[12]" value="<?php echo $w3_price_lv_plus[12]; ?>" size="5" /> Gcent<br />
                W3 +13 trả thêm : <input type="text" name="w3_price_lv_plus[13]" value="<?php echo $w3_price_lv_plus[13]; ?>" size="5" /> Gcent<br />
                W3 +14 trả thêm : <input type="text" name="w3_price_lv_plus[14]" value="<?php echo $w3_price_lv_plus[14]; ?>" size="5" /> Gcent<br />
                W3 +15 trả thêm : <input type="text" name="w3_price_lv_plus[15]" value="<?php echo $w3_price_lv_plus[15]; ?>" size="5" /> Gcent<br />
                
                <hr />
                <strong><font color="red">Cánh cấp 4</font></strong> :<br />
                
                <table border="0">
                    <tr>
                        <td align="right">Cơ hội loại bỏ sức phòng thủ 7% :</td>
                        <td align="left"><input type="text" name="wing4_exl[1]" value="<?php echo $wing4_exl[1]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Phản đòn khi cận chiến 7% :</td>
                        <td align="left"><input type="text" name="wing4_exl[2]" value="<?php echo $wing4_exl[2]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Khả năng hồi phục hoàn toàn HP 7% :</td>
                        <td align="left"><input type="text" name="wing4_exl[3]" value="<?php echo $wing4_exl[3]; ?>" size="5" /> Gcent</td>
                    </tr>
                    <tr>
                        <td align="right">Khả năng hồi phục hoàn toàn nội lực 7% :</td>
                        <td align="left"><input type="text" name="wing4_exl[4]" value="<?php echo $wing4_exl[4]; ?>" size="5" /> Gcent</td>
                    </tr>
                </table>
                
                <br />
                
                Wing 4 - hồi HP 1% : <input type="text" name="w4_price_option[1]" value="<?php echo $w4_price_option[1]; ?>" size="5" /> Gcent<br />
                Wing 4 - hồi HP 2% : <input type="text" name="w4_price_option[2]" value="<?php echo $w4_price_option[2]; ?>" size="5" /> Gcent<br />
                Wing 4 - hồi HP 3% : <input type="text" name="w4_price_option[3]" value="<?php echo $w4_price_option[3]; ?>" size="5" /> Gcent<br />
                Wing 4 - hồi HP 4% : <input type="text" name="w4_price_option[4]" value="<?php echo $w4_price_option[4]; ?>" size="5" /> Gcent<br />
                Wing 4 - hồi HP 5% : <input type="text" name="w4_price_option[5]" value="<?php echo $w4_price_option[5]; ?>" size="5" /> Gcent<br />
                Wing 4 - hồi HP 6% : <input type="text" name="w4_price_option[6]" value="<?php echo $w4_price_option[6]; ?>" size="5" /> Gcent<br />
                Wing 4 - hồi HP 7% : <input type="text" name="w4_price_option[7]" value="<?php echo $w4_price_option[7]; ?>" size="5" /> Gcent<br />
                <br />
                
                W4 +1 trả thêm : <input type="text" name="w4_price_lv_plus[1]" value="<?php echo $w4_price_lv_plus[1]; ?>" size="5" /> Gcent<br />
                W4 +2 trả thêm : <input type="text" name="w4_price_lv_plus[2]" value="<?php echo $w4_price_lv_plus[2]; ?>" size="5" /> Gcent<br />
                W4 +3 trả thêm : <input type="text" name="w4_price_lv_plus[3]" value="<?php echo $w4_price_lv_plus[3]; ?>" size="5" /> Gcent<br />
                W4 +4 trả thêm : <input type="text" name="w4_price_lv_plus[4]" value="<?php echo $w4_price_lv_plus[4]; ?>" size="5" /> Gcent<br />
                W4 +5 trả thêm : <input type="text" name="w4_price_lv_plus[5]" value="<?php echo $w4_price_lv_plus[5]; ?>" size="5" /> Gcent<br />
                W4 +6 trả thêm : <input type="text" name="w4_price_lv_plus[6]" value="<?php echo $w4_price_lv_plus[6]; ?>" size="5" /> Gcent<br />
                W4 +7 trả thêm : <input type="text" name="w4_price_lv_plus[7]" value="<?php echo $w4_price_lv_plus[7]; ?>" size="5" /> Gcent<br />
                W4 +8 trả thêm : <input type="text" name="w4_price_lv_plus[8]" value="<?php echo $w4_price_lv_plus[8]; ?>" size="5" /> Gcent<br />
                W4 +9 trả thêm : <input type="text" name="w4_price_lv_plus[9]" value="<?php echo $w4_price_lv_plus[9]; ?>" size="5" /> Gcent<br />
                W4 +10 trả thêm : <input type="text" name="w4_price_lv_plus[10]" value="<?php echo $w4_price_lv_plus[10]; ?>" size="5" /> Gcent<br />
                W4 +11 trả thêm : <input type="text" name="w4_price_lv_plus[11]" value="<?php echo $w4_price_lv_plus[11]; ?>" size="5" /> Gcent<br />
                W4 +12 trả thêm : <input type="text" name="w4_price_lv_plus[12]" value="<?php echo $w4_price_lv_plus[12]; ?>" size="5" /> Gcent<br />
                W4 +13 trả thêm : <input type="text" name="w4_price_lv_plus[13]" value="<?php echo $w4_price_lv_plus[13]; ?>" size="5" /> Gcent<br />
                W4 +14 trả thêm : <input type="text" name="w4_price_lv_plus[14]" value="<?php echo $w4_price_lv_plus[14]; ?>" size="5" /> Gcent<br />
                W4 +15 trả thêm : <input type="text" name="w4_price_lv_plus[15]" value="<?php echo $w4_price_lv_plus[15]; ?>" size="5" /> Gcent<br />
                
				<center><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></center>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
