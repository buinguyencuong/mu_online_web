<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$page = $_GET['page'];
$file_edit = 'config/webshopitem.txt';

switch ($act)
{
	case 'shop_taphoa': 
		$webshop_type = 1;
        $tilte = "Cửa Hàng Tạp Hóa";
		break;
	case 'shop_event': 
		$webshop_type = 15;
        $tilte = "Cửa Hàng Tạp Hóa";
		break;
	case 'shop_kiem': 
		$webshop_type = 2;
		$tilte = "Cửa Hàng Kiếm";
		break;
	case 'shop_gay': 
		$webshop_type = 3;
		$tilte = "Cửa Hàng Gậy";
		break;
	case 'shop_cung': 
        $webshop_type = 4;
		$tilte = "Cửa Hàng Cung";
		break;
	case 'shop_vukhikhac':
        $webshop_type = 5; 
		$tilte = "Cửa Hàng Vũ Khí Khác";
		break;
	case 'shop_khien': 
        $webshop_type = 6;
		$tilte = "Cửa Hàng Khiên";
		break;
	case 'shop_mu': 
        $webshop_type = 7;
		$tilte = "Cửa Hàng Mũ";
		break;
	case 'shop_ao': 
        $webshop_type = 8;
		$tilte = "Cửa Hàng Áo";
		break;
	case 'shop_quan': 
        $webshop_type = 9;
		$tilte = "Cửa Hàng Quần";
		break;
	case 'shop_tay': 
        $webshop_type = 10;
		$tilte = "Cửa Hàng Tay";
		break;
	case 'shop_chan': 
        $webshop_type = 11;
		$tilte = "Cửa Hàng Chân";
		break;
	case 'shop_trangsuc':
        $webshop_type = 12; 
		$tilte = "Cửa Hàng Trang Sức";
		break;
	case 'shop_canh': 
        $webshop_type = 13;
		$tilte = "Cửa Hàng Cánh";
		break;
	case 'shop_acient': 
        $webshop_type = 14;
		$tilte = "Cửa Hàng SET Thần Thánh";
		break;
	default: $file_edit = ''; break;
}

if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

if($accept == 0) $show_accept = "disabled='disabled'";
else $show_accept = "";

require_once('admin_cfg/function.php');


$action = $_POST[action];

switch ($action)
{
	case 'add':
		$code = $_POST['code'];       $code = strtoupper($code);
		$price = $_POST['price']; $price = abs(intval($price));
        $money_type = $_POST['money_type'];
        $stat = $_POST['stat'];
        
        $error = "";
        if (!preg_match("/^[A-F0-9]*$/i", $code))
    	{
            $error .= "Dữ liệu lỗi <strong>Mã Item</strong> : $code . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($code) <> 32 ) {
            $error .= "Dữ liệu lỗi <strong>Mã Item</strong> : $code phải có 32 ký tự.<br />";
        }
        if( !isset($money_type) ) {
            $error .= "Dữ liệu lỗi <strong>Loại giá bán chưa chọn</strong>.<br />";
        }
        
        if(strlen($error) > 0) {
            $notice = "<center><b><font color='red'>$error</font></b></center>";
        } else {
            $itemwebshop_list_arr = webshop_load($file_edit);
            
            include_once('config_license.php');
            include_once('func_getContent.php');
            $getcontent_url = $url_license . "/api_webshop.php";
            $getcontent_data = array(
                'acclic'    =>  $acclic,
                'key'    =>  $key,
                'action'    =>  'webshop_cfg_add',
                
                'code'    =>  $code
            ); 
            
            $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
            
            if ( empty($reponse) ) {
                echo "Kết nối đến API bị gián đoạn.";
                exit();
            }
            else {
                $info = read_TagName($reponse, 'info');
                if($info == "Error") {
                    echo read_TagName($reponse, 'message');
                    exit();
                } elseif ($info == "OK") {
                    $item_webshop_info = read_TagName($reponse, 'item_webshop_info');
                    if(strlen($item_webshop_info) == 0) {
                        echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                        
                        $arr_view = "\nDataSend:\n";
                        foreach($getcontent_data as $k => $v) {
                            $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                        }
                        writelog("log_api.txt", $arr_view . $reponse);
                        exit();
                    } else {
                        $item_webshop_info_arr = json_decode($item_webshop_info, true);
                        $item_code = $item_webshop_info_arr['code'];
                        $webshop_type = $item_webshop_info_arr['webshop_type'];
                        
                        if(isset($itemwebshop_list_arr[$webshop_type][$item_code])) {
                            $err = "Item ". $itemwebshop_list_arr[$webshop_type][$item_code]['item_name'] ." đã có sẵn. Không thể thêm trùng lặp.";
                        } else {
                            if($stat != 1) $stat = 0;
                            $itemwebshop_list_arr[$webshop_type][$item_code] = array(
                                'item_name'  =>  $item_webshop_info_arr['name'],
                                'item_name_en'  =>  $item_webshop_info_arr['name_en'],
                                'price'  =>  $price,
                                'money_type'  =>  $money_type,
                                'img'  =>  $item_webshop_info_arr['image'],
                                'exl_type'  =>  $item_webshop_info_arr['exl_type'],
                                'stat'  =>  $stat
                            );
                        }
                    }
                } else {
                    echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
                    writelog("log_api.txt", $reponse);
                    exit();
                }
            }
		    
			if($err) {
		        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
		    } else {
		    	$content = '';
                $content = json_encode($itemwebshop_list_arr);
        		
        		replacecontent($file_edit,$content);
    
        		include('config/config_sync.php');
    		    for($i=0; $i<count($url_hosting); $i++)
    		    {
    		        if($url_hosting[$i]) {
    		            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
    		            if($sync_send == 'OK') {
    		                
    		            } else {
    		                $err .= $sync_send;
    		            }
    		        }
    		    }
                
                $notice = "<center><font color='blue'>Thêm <strong>". $itemwebshop_list_arr[$webshop_type][$item_code]['item_name'] ."</strong> thành công</font></center>";
		    }
        }
    		
		break;
	
	case 'edit':
		$code = $_POST['code'];
		$price = $_POST['price'];     $price = abs(intval($price));
        $money_type = $_POST['money_type'];
        $webshop_type = $_POST['webshop_type'];
        $stat = $_POST['stat'];
        
        $error = "";
        if (!preg_match("/^[A-F0-9]*$/i", $code))
    	{
            $error .= "Dữ liệu lỗi <strong>Mã Item</strong> : $code . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($code) <> 32 ) {
            $error .= "Dữ liệu lỗi <strong>Mã Item</strong> : $code phải gồm 32 ký tự.<br />";
        }
        if( !isset($money_type) ) {
            $error .= "Dữ liệu lỗi <strong>Loại giá bán chưa chọn</strong>.<br />";
        }
        
        if(strlen($error) > 0) {
            $notice = "<center><b><font color='red'>$error</font></b></center>";
        } else {
            $itemwebshop_list_arr = webshop_load($file_edit);
            
            include_once('config_license.php');
            include_once('func_getContent.php');
            $getcontent_url = $url_license . "/api_webshop.php";
            $getcontent_data = array(
                'acclic'    =>  $acclic,
                'key'    =>  $key,
                'action'    =>  'webshop_cfg_add',
                
                'code'    =>  $code
            ); 
            
            $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
            
            if ( empty($reponse) ) {
                echo "Kết nối đến API bị gián đoạn.";
                exit();
            }
            else {
                $info = read_TagName($reponse, 'info');
                if($info == "Error") {
                    echo read_TagName($reponse, 'message');
                    exit();
                } elseif ($info == "OK") {
                    $item_webshop_info = read_TagName($reponse, 'item_webshop_info');
                    if(strlen($item_webshop_info) == 0) {
                        echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                        
                        $arr_view = "\nDataSend:\n";
                        foreach($getcontent_data as $k => $v) {
                            $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                        }
                        writelog("log_api.txt", $arr_view . $reponse);
                        exit();
                    } else {
                        $item_webshop_info_arr = json_decode($item_webshop_info, true);
                        $item_code = $item_webshop_info_arr['code'];
                        
                        if($webshop_type != $item_webshop_info_arr['webshop_type'] || $code != $item_code) {
                            unset($itemwebshop_list_arr[$webshop_type][$code]);
                            $webshop_type = $item_webshop_info_arr['webshop_type'];
                        }
                        
                        if($stat != 1) $stat = 0;
                        $itemwebshop_list_arr[$webshop_type][$item_code] = array(
                            'item_name'  =>  $item_webshop_info_arr['name'],
                            'item_name_en'  =>  $item_webshop_info_arr['name_en'],
                            'price'  =>  $price,
                            'money_type'  =>  $money_type,
                            'img'  =>  $item_webshop_info_arr['image'],
                            'exl_type'  =>  $item_webshop_info_arr['exl_type'],
                            'stat'  =>  $stat
                        );
                    }
                } else {
                    echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
                    writelog("log_api.txt", $reponse);
                    exit();
                }
            }
            
			if($error) {
		        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$error</font></center>";
		    } else {
		    	$content = '';
                $content = json_encode($itemwebshop_list_arr);
        		
        		replacecontent($file_edit,$content);
        		
        		include('config/config_sync.php');
    		    for($i=0; $i<count($url_hosting); $i++)
    		    {
    		        if($url_hosting[$i]) {
    		            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
    		            if($sync_send == 'OK') {
    		                
    		            } else {
    		                $err .= $sync_send;
    		            }
    		        }
    		    }
                
                $notice = "<center><font color='blue'>Sửa <strong>". $itemwebshop_list_arr[$webshop_type][$code]['item_name'] ."</strong> thành công</font></center>";
		    }
        }
		break;
	
	case 'del':
		$code = $_POST['code'];
        $webshop_type = $_POST['webshop_type'];
		
        $error = "";
        if (!preg_match("/^[A-F0-9]*$/i", $code))
    	{
            $error .= "Dữ liệu lỗi <strong>Mã Item</strong> : $code . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($code) <> 32 ) {
            $error .= "Dữ liệu lỗi <strong>Mã Item</strong> : $code phải gồm 32 ký tự.<br />";
        }
        
        if(strlen($error) > 0) {
            $notice = "<center><b><font color='red'>$error</font></b></center>";
        } else {
            $itemwebshop_list_arr = webshop_load($file_edit);
            $item_name = $itemwebshop_list_arr[$webshop_type][$code]['item_name'];            
    		unset($itemwebshop_list_arr[$webshop_type][$code]);
            
			if($err) {
		        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
		    } else {
		    	$content = '';
                $content = json_encode($itemwebshop_list_arr);
        		
        		replacecontent($file_edit,$content);
        		
        		include('config/config_sync.php');
    		    for($i=0; $i<count($url_hosting); $i++)
    		    {
    		        if($url_hosting[$i]) {
    		            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
    		            if($sync_send == 'OK') {
    		                
    		            } else {
    		                $err .= $sync_send;
    		            }
    		        }
    		    }
                
                $notice = "<center><font color='blue'>Xóa <strong>". $item_name ."</strong> thành công</font></center>";
		    }
        }
		break;
}


$item_read = webshop_load($file_edit);
?>
		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình <?php echo $tilte; ?></h1>
			</div><br>
				Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php 
if($notice) echo $notice;

switch ($page)
{
	case 'add': 
		$content = "<center><b>Thêm Item</b></center><br>
			<form id='editconfig' name='editconfig' method='post' action='admin.php?mod=editwebshop&act=".$act."'>
			<input type='hidden' name='action' value='add'/>
			<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
				<tr bgcolor='#FFFFFF' >
					<td >Mã Item</td>
					<td ><input type='text' name='code' value='' size='36' maxlength='32'/> <i>(32 Mã lấy từ MUMaker)</i></td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >Giá bán</td>
					<td >
                        <input type='text' name='price' value='' size='5'/> 
                        <input type='checkbox' name='money_type[1]' value='1' checked='checked' /> Vcent
                        <input type='checkbox' name='money_type[2]' value='1' /> Vcent Event
                        <input type='checkbox' name='money_type[3]' value='1' /> Gcent
                        <input type='checkbox' name='money_type[4]' value='1' /> Gcent KM
                    </td>
				</tr>
                <tr bgcolor='#FFFFFF' >
					<td >Bán</td>
					<td ><input type='checkbox' name='stat' value='1' checked /></td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >&nbsp;</td>
					<td align='center'><input type='submit' name='Submit' value='Thêm Item' ". $show_accept ." /></td>
				</tr>
			</table>
			</form>";
		echo $content;
		break;
	
	case 'edit': 
		$item = $_GET['item'];
        $webshop_type = $_GET['webshop_type'];
        
        $status = ($item_read[$webshop_type][$item]['stat'] == 1) ? 'checked' : '';
        if(!isset($item_read[$webshop_type][$item]['price'])) $item_read[$webshop_type][$item]['price'] = 1;
        $vpoint_checked = '';
        $vpoint_km_checked = '';
        $gcoin_checked = '';
        $gcoin_km_checked = '';
        
        if(!isset($item_read[$webshop_type][$item]['money_type']) || (isset($item_read[$webshop_type][$item]['money_type'][1]) && $item_read[$webshop_type][$item]['money_type'][1] == 1) ) $vpoint_checked = 'checked="checked"';
        if(isset($item_read[$webshop_type][$item]['money_type'][2]) && $item_read[$webshop_type][$item]['money_type'][2] == 1) $vpoint_km_checked = 'checked="checked"';
        if(isset($item_read[$webshop_type][$item]['money_type'][3]) && $item_read[$webshop_type][$item]['money_type'][3] == 1) $gcoin_checked = 'checked="checked"';
        if(isset($item_read[$webshop_type][$item]['money_type'][4]) && $item_read[$webshop_type][$item]['money_type'][4] == 1) $gcoin_km_checked = 'checked="checked"';
        
		$content = "<center><b>Sửa Item</b></center><br>
			<form id='editconfig' name='editconfig' method='post' action='admin.php?mod=editwebshop&act=".$act."'>
			<input type='hidden' name='action' value='edit'/>
            <input type='hidden' name='code' value='". $item ."'/>
            <input type='hidden' name='webshop_type' value='". $webshop_type ."'/>
			<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
				<tr bgcolor='#FFFFFF' >
					<td width='100'>Item</td>
					<td>". $item_read[$webshop_type][$item]['item_name'];
        if(strlen($item_read[$webshop_type][$item]['item_name_en']) > 0) {
            $content .= " ( ". $item_read[$webshop_type][$item]['item_name_en'] ." )";
        }            
        $content .= "</td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >Giá bán</td>
					<td >
                        <input type='text' name='price' value='". $item_read[$webshop_type][$item]['price'] ."' size='5'/> 
                        <input type='checkbox' name='money_type[1]' value='1' ". $vpoint_checked ." /> Vcent
                        <input type='checkbox' name='money_type[2]' value='1' ". $vpoint_km_checked ." /> Vcent Event
                        <input type='checkbox' name='money_type[3]' value='1' ". $gcoin_checked ." /> Gcent
                        <input type='checkbox' name='money_type[4]' value='1' ". $gcoin_km_checked ." /> Gcent KM
                    </td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >Hình Item</td>
					<td bgcolor='#333'><img src='items/". $item_read[$webshop_type][$item]['img'] .".gif'></td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >Bán</td>
					<td ><input type='checkbox' name='stat' value='1' ". $status ." /></td>
				</tr>
                <tr bgcolor='#FFFFFF' >
					<td >&nbsp;</td>
					<td align='center'><input type='submit' name='Submit' value='Sửa' ". $show_accept ." /></td>
				</tr>
			</table>
			</form>";
		echo $content;
		break;
	
	case 'del':
		$item = $_GET['item'];
		$webshop_type = $_GET['webshop_type'];
        
        if(!isset($item_read[$webshop_type][$item]['money_type']) || (isset($item_read[$webshop_type][$item]['money_type'][1]) && $item_read[$webshop_type][$item]['money_type'][1] == 1) ) $money_name = '<font color="blue"><strong>VC</strong></font>';
        if(isset($item_read[$webshop_type][$item]['money_type'][2]) && $item_read[$webshop_type][$item]['money_type'][2] == 1) {
            if(strlen($money_name) > 0) $money_name .= ' + ';
            $money_name .= '<font color="green"><strong>VC*</strong></font>';
        }
        if(isset($item_read[$webshop_type][$item]['money_type'][3]) && $item_read[$webshop_type][$item]['money_type'][3] == 1) {
            if(strlen($money_name) > 0) $money_name .= ' + ';
            $money_name .= '<font color="red"><strong>GC</strong></font>';
        }
        if(isset($item_read[$webshop_type][$item]['money_type'][4]) && $item_read[$webshop_type][$item]['money_type'][4] == 1) {
            if(strlen($money_name) > 0) $money_name .= ' + ';
            $money_name .= '<font color="orange"><strong>GC*</strong></font>';
        }
		$content = "<center><b>Xóa Item</b></center><br>
			<form id='delitem' name='delitem' method='post' action='admin.php?mod=editwebshop&act=".$act."'>
			<input type='hidden' name='action' value='del'/>
			<input type='hidden' name='code' value='". $item ."'/>
            <input type='hidden' name='webshop_type' value='". $webshop_type ."'/>
			<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
				<tr bgcolor='#FFFFFF' >
					<td >Item</td>
					<td>". $item_read[$webshop_type][$item]['item_name'];
        if(strlen($item_read[$webshop_type][$item]['item_name_en']) > 0) {
            $content .= " ( ". $item_read[$webshop_type][$item]['item_name_en'] ." )";
        }  
        $content .= "</td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >Giá bán</td>
					<td >". number_format($item_read[$webshop_type][$item]['price'], 0, ',', '.') ." ". $money_name ."</td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >Hình Item</td>
					<td bgcolor='#333'><img src='items/". $item_read[$webshop_type][$item]['img'] .".gif'></td>
				</tr>
				<tr bgcolor='#FFFFFF' >
					<td >&nbsp;</td>
					<td align='center'><input type='submit' name='Submit' value='Xóa' ". $show_accept ." /></td>
				</tr>
			</table>
			</form>";
		echo $content;
		
		break;	
		
	default: 
		echo "<div align='right'><a href='admin.php?mod=editwebshop&act=". $act ."&page=add'>+ Thêm Item bán</a></div><br>";
		webshop_display($item_read, $webshop_type, $act); 
		break;
	
}

?>
				
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
