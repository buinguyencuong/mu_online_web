<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/shop_bank.php';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

$action = $_POST[action];

if($action == 'edit')
{
	$content = "<?php\n";
	
	$bank = $_POST['bank'];
    if(is_array($bank)) {
        foreach($bank as $k => $v) {
            $content .= "\$bank[$k]['money']	= $v[money];\n";
            if(is_array($v['moneytype'])) {
                foreach($v['moneytype'] as $k2 => $v2) {
                    if($v2 == 1) {
                        $content .= "\$bank[$k]['moneytype'][$k2]	= 1;\n";
                    }
                }
            }
        }
    }

	$content .= "?>";
	
	require_once('admin_cfg/function.php');
	replacecontent($file_edit,$content);
	
	include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}

include($file_edit);
?>


		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình Cửa hàng Zen</h1>
			</div><br>
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				<form id="editconfig" name="editconfig" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
				<table border="0">
                    <tr>
                        <td align="right">Ngọc hỗn nguyên</td>
                        <td align="left">
                            <input type="text" name="bank[0][money]" value="<?php echo $bank[0]['money']; ?>" size="5"/> 
                            <input type='checkbox' name='bank[0][moneytype][1]' value='1' <?php if($bank[0]['moneytype'][1] == 1) echo "checked"; ?> /> Vc
                            <input type='checkbox' name='bank[0][moneytype][2]' value='1' <?php if($bank[0]['moneytype'][2] == 1) echo "checked"; ?> /> Vc+
                            <input type='checkbox' name='bank[0][moneytype][3]' value='1' <?php if($bank[0]['moneytype'][3] == 1) echo "checked"; ?> /> Gc
                            <input type='checkbox' name='bank[0][moneytype][4]' value='1' <?php if($bank[0]['moneytype'][4] == 1) echo "checked"; ?> /> Gc+
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Ngọc sáng tạo</td>
                        <td align="left">
                            <input type="text" name="bank[1][money]" value="<?php echo $bank[1]['money']; ?>" size="5"/> 
                            <input type='checkbox' name='bank[1][moneytype][1]' value='1' <?php if($bank[1]['moneytype'][1] == 1) echo "checked"; ?> /> Vc
                            <input type='checkbox' name='bank[1][moneytype][2]' value='1' <?php if($bank[1]['moneytype'][2] == 1) echo "checked"; ?> /> Vc+
                            <input type='checkbox' name='bank[1][moneytype][3]' value='1' <?php if($bank[1]['moneytype'][3] == 1) echo "checked"; ?> /> Gc
                            <input type='checkbox' name='bank[1][moneytype][4]' value='1' <?php if($bank[1]['moneytype'][4] == 1) echo "checked"; ?> /> Gc+
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Ngọc Lông Vũ</td>
                        <td align="left">
                            <input type="text" name="bank[2][money]" value="<?php echo $bank[2]['money']; ?>" size="5"/> 
                            <input type='checkbox' name='bank[2][moneytype][1]' value='1' <?php if($bank[2]['moneytype'][1] == 1) echo "checked"; ?> /> Vc
                            <input type='checkbox' name='bank[2][moneytype][2]' value='1' <?php if($bank[2]['moneytype'][2] == 1) echo "checked"; ?> /> Vc+
                            <input type='checkbox' name='bank[2][moneytype][3]' value='1' <?php if($bank[2]['moneytype'][3] == 1) echo "checked"; ?> /> Gc
                            <input type='checkbox' name='bank[2][moneytype][4]' value='1' <?php if($bank[2]['moneytype'][4] == 1) echo "checked"; ?> /> Gc+
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Trái tim</td>
                        <td align="left">
                            <input type="text" name="bank[3][money]" value="<?php echo $bank[3]['money']; ?>" size="5"/> 
                            <input type='checkbox' name='bank[3][moneytype][1]' value='1' <?php if($bank[3]['moneytype'][1] == 1) echo "checked"; ?> /> Vc
                            <input type='checkbox' name='bank[3][moneytype][2]' value='1' <?php if($bank[3]['moneytype'][2] == 1) echo "checked"; ?> /> Vc+
                            <input type='checkbox' name='bank[3][moneytype][3]' value='1' <?php if($bank[3]['moneytype'][3] == 1) echo "checked"; ?> /> Gc
                            <input type='checkbox' name='bank[3][moneytype][4]' value='1' <?php if($bank[3]['moneytype'][4] == 1) echo "checked"; ?> /> Gc+
                        </td>
                    </tr>
                </table>
                
                <center><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></center>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
