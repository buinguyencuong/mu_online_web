<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$act = $_GET['act'];
switch ($act) {
	case 'config_facebook': include('editconfig/adm_config_facebook.php'); break;
    case 'config_ipbonus': include('editconfig/adm_config_ipbonus.php'); break;
    case 'config_notice': include('editconfig/adm_config_notice.php'); break;
    case 'config_sync': include('editconfig/adm_config_sync.php'); break;
    case 'config': include('editconfig/adm_config.php'); break;
	case 'config_antiddos': include('editconfig/adm_config_antiddos.php'); break;
	case 'config_dongbo': include('editconfig/adm_config_dongbo.php'); break;
	case 'config_domain': include('editconfig/adm_config_domain.php'); break;
	case 'activepro': include('editconfig/adm_activepro.php'); break;
	case 'activevip': include('editconfig/adm_activevip.php'); break;
	case 'config_chucnang': include('editconfig/adm_config_chucnang.php'); break;
	case 'config_sms': include('editconfig/adm_config_sms.php'); break;
	case 'config_autonap': include('editconfig/adm_config_autonap.php'); break;
	case 'config_license': include('editconfig/adm_config_license.php'); break;
	case 'config_sendmess': include('editconfig/adm_config_sendmess.php'); break;
    case 'config_event_in_game_time': include('editconfig/adm_config_event_in_game_time.php'); break;
    case 'config_vip': include('editconfig/adm_config_vip.php'); break;
	case 'config_vip_system': include('editconfig/adm_config_vip_system.php'); break;
    case 'config_ranking': include('editconfig/adm_config_ranking.php'); break;
    case 'config_alphatest': include('editconfig/adm_config_alphatest.php'); break;
    case 'config_warehouse_secure': include('editconfig/adm_config_warehouse_secure.php'); break;
	case 'config_linkdown': include('editconfig/adm_config_linkdown.php'); break;
	default: include('checkwrite.php'); break;
}
?>