<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/config_ipbonus.php';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

$action = $_POST[action];

if($action == 'edit')
{
	$content = "<?php\n";
	
    if(isset($_POST['use_ipbonus']) && $_POST['use_ipbonus'] == 1) $use_ipbonus 	= $_POST['use_ipbonus'];
    else $use_ipbonus = 0;
        $content .= "\$use_ipbonus	= $use_ipbonus;\n";
    
    
    $ipbonus_decre_price = abs(intval($_POST['ipbonus_decre_price']));
        $content .= "\$ipbonus_decre_price	= $ipbonus_decre_price;\n";
    $ipbonus_kmcard = abs(intval($_POST['ipbonus_kmcard']));
        $content .= "\$ipbonus_kmcard	= $ipbonus_kmcard;\n";
    
	
	$content .= "?>";
	
	require_once('admin_cfg/function.php');
	replacecontent($file_edit,$content);
	
	include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}

include($file_edit);
?>


		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình IP Bonus</h1>
			</div><br />
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				<form id="edit_ipbonus" name="edit_ipbonus" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
                Sử dụng IP Bonus : <input type="checkbox" name="use_ipbonus" value="1" <?php if($use_ipbonus == 1) echo "checked"; ?> /><br />
                Giảm phí RS VIP : <input type="text" name="ipbonus_decre_price" value="<?php if(isset($ipbonus_decre_price)) echo $ipbonus_decre_price; else echo 0; ?>" size="5" /> %<br />
                Tăng % KM Nạp thẻ : <input type="text" name="ipbonus_kmcard" value="<?php if(isset($ipbonus_kmcard)) echo $ipbonus_kmcard; else echo 0; ?>" size="5" /> %<br />
                
				<center><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></center>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br />
			- Tên WebSite<br />
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
