<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/config_vip_system.php';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

$action = $_POST[action];

if($action == 'edit')
{
	$content = "<?php\n";
	
	$gold_vip = $_POST['gold_vip'];
	$silver_vip = $_POST['silver_vip'];
	$enable_vip = $_POST['enable_vip'];
	$gold_daily = $_POST['gold_daily'];
	$silver_daily = $_POST['silver_daily'];

	
	if(is_array($gold_vip)) {
        foreach($gold_vip as $k => $v) {
			$k = stripslashes($k);
				if(is_array($v)) {
                    foreach($v as $key => $val) {
						$val = abs(intval($val));
						$content .= "\$gold_vip[$k][$key]	= $val;\n";
                    }
                } else {
					$v = abs(intval($v));
					$content .= "\$gold_vip[$k]	= $v;\n";
				}					
        }
    }
	if(is_array($silver_vip)) {
        foreach($silver_vip as $k => $v) {
			$k = stripslashes($k);			
				if(is_array($v)) {
                    foreach($v as $key => $val) {
						$val = abs(intval($val));
						$content .= "\$silver_vip[$k][$key]	= $val;\n";
                    }
                } else {
					$v = abs(intval($v));
					$content .= "\$silver_vip[$k]	= $v;\n";
				}
		}
    }
	if(is_array($enable_vip)) {
        foreach($enable_vip as $k => $v) {
			$k = stripslashes($k);
			$v = abs(intval($v));			
			$content .= "\$enable_vip[$k]	= $v;\n";
        }
    }
	if(is_array($gold_daily)) {
        foreach($gold_daily as $k => $v) {
			$k = stripslashes($k);
			$v = abs(intval($v));			
			$content .= "\$gold_daily[$k]	= $v;\n";
        }
    }
	if(is_array($silver_daily)) {
        foreach($silver_daily as $k => $v) {
			$k = stripslashes($k);
			$v = abs(intval($v));			
			$content .= "\$silver_daily[$k]	= $v;\n";
        }
    }
	
	$content .= "?>";
	
	require_once('admin_cfg/function.php');
	replacecontent($file_edit,$content);
	
	include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}

include($file_edit);
?>


		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình VIP Loại 2</h1>
			</div><br />
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				<form id="edit_vip_system" name="edit_vip_system" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
                <table border="1" style="border-collapse: collapse;" width="100%">
                    <tr>
                        <td align="center">Phúc Lợi</td>
                        <td align="center">VIP Vàng</td>
                        <td align="center">VIP Bạc</td>
						<td align="center">Ngày</td>
                    </tr>
					<tr>
						<td align="center">Giảm % Level khi Reset</td>
						<td align="center"><input type="text" name="gold_vip['level_reset']" value="<?php echo $gold_vip['level_reset']; ?>" size="5" /> %</td>
						<td align="center"><input type="text" name="silver_vip['level_reset']" value="<?php echo $silver_vip['level_reset']; ?>" size="5" /> %</td>
                    	<td align="center">
                            <select name="enable_vip['level_reset']">
                                <option value="3" <?php if($enable_vip['level_reset'] == 3) echo "selected='selected'"; ?> >3 Ngày</option>
                                <option value="7" <?php if($enable_vip['level_reset'] == 7) echo "selected='selected'"; ?> >7 Ngày</option>
								<option value="30" <?php if($enable_vip['level_reset'] == 30) echo "selected='selected'"; ?> >30 Ngày</option>
                            </select>
                        </td>
					</tr>
					<tr>
						<td align="center">Thêm % Point nhận được sau khi Reset</td>
						<td align="center"><input type="text" name="gold_vip['point_reset']" value="<?php echo $gold_vip['point_reset']; ?>" size="5" /> %</td>
						<td align="center"><input type="text" name="silver_vip['point_reset']" value="<?php echo $silver_vip['point_reset']; ?>" size="5" /> %</td>
                        <td align="center">
                            <select name="enable_vip['point_reset']">
                                <option value="3" <?php if($enable_vip['point_reset'] == 3) echo "selected='selected'"; ?> >3 Ngày</option>
                                <option value="7" <?php if($enable_vip['point_reset'] == 7) echo "selected='selected'"; ?> >7 Ngày</option>
								<option value="30" <?php if($enable_vip['point_reset'] == 30) echo "selected='selected'"; ?> >30 Ngày</option>
                            </select>
                        </td>
				   </tr> 
					<tr>
						<td align="center">Giảm % Zen khi Reset</td>
						<td align="center"><input type="text" name="gold_vip['zen_reset']" value="<?php echo $gold_vip['zen_reset']; ?>" size="5" /> %</td>
						<td align="center"><input type="text" name="silver_vip['zen_reset']" value="<?php echo $silver_vip['zen_reset']; ?>" size="5" /> %</td>
                        <td align="center">
                            <select name="enable_vip['zen_reset']">
                                <option value="3" <?php if($enable_vip['zen_reset'] == 3) echo "selected='selected'"; ?> >3 Ngày</option>
                                <option value="7" <?php if($enable_vip['zen_reset'] == 7) echo "selected='selected'"; ?> >7 Ngày</option>
								<option value="30" <?php if($enable_vip['zen_reset'] == 30) echo "selected='selected'"; ?> >30 Ngày</option>
                            </select>
                        </td>
					</tr> 
					<tr>
						<td align="center">Thêm % Điểm Ủy Thác khi Ủy Thác Offline</td>
						<td align="center"><input type="text" name="gold_vip['point_utoffline']" value="<?php echo $gold_vip['point_utoffline']; ?>" size="5" /> %</td>
						<td align="center"><input type="text" name="silver_vip['point_utoffline']" value="<?php echo $silver_vip['point_utoffline']; ?>" size="5" /> %</td>
                        <td align="center">
                            <select name="enable_vip['utoff']">
                                <option value="3" <?php if($enable_vip['utoff'] == 3) echo "selected='selected'"; ?> >3 Ngày</option>
                                <option value="7" <?php if($enable_vip['utoff'] == 7) echo "selected='selected'"; ?> >7 Ngày</option>
								<option value="30" <?php if($enable_vip['utoff'] == 30) echo "selected='selected'"; ?> >30 Ngày</option>
                            </select>
                        </td>
					</tr> 
					<tr>
						<td align="center">Giảm % trừ Reset sau khi Đổi giới tính</td>
						<td align="center"><input type="text" name="gold_vip['reset_dgt']" value="<?php echo $gold_vip['reset_dgt']; ?>" size="5" /> %</td>
						<td align="center"><input type="text" name="silver_vip['reset_dgt']" value="<?php echo $silver_vip['reset_dgt']; ?>" size="5" /> %</td>
                        <td align="center">
                            <select name="enable_vip['dgt']">
                                <option value="3" <?php if($enable_vip['dgt'] == 3) echo "selected='selected'"; ?> >3 Ngày</option>
                                <option value="7" <?php if($enable_vip['dgt'] == 7) echo "selected='selected'"; ?> >7 Ngày</option>
								<option value="30" <?php if($enable_vip['dgt'] == 30) echo "selected='selected'"; ?> >30 Ngày</option>
                            </select>
                        </td>                    
					</tr> 
					<tr>
						<td align="center">Giảm % Exp yêu cầu khi Thăng cấp Tu luyện</td>
						<td align="center"><input type="text" name="gold_vip['exp_tuluyen']" value="<?php echo $gold_vip['exp_tuluyen']; ?>" size="5" /> %</td>
						<td align="center"><input type="text" name="silver_vip['exp_tuluyen']" value="<?php echo $silver_vip['exp_tuluyen']; ?>" size="5" /> %</td>
                        <td align="center">
                            <select name="enable_vip['tuluyen']">
                                <option value="3" <?php if($enable_vip['tuluyen'] == 3) echo "selected='selected'"; ?> >3 Ngày</option>
                                <option value="7" <?php if($enable_vip['tuluyen'] == 7) echo "selected='selected'"; ?> >7 Ngày</option>
								<option value="30" <?php if($enable_vip['tuluyen'] == 30) echo "selected='selected'"; ?> >30 Ngày</option>
                            </select>
                        </td>                    
				    </tr> 
					<tr>
						<td align="center">Thêm số lượng Nhiệm vụ tối đa hàng ngày</td>
						<td align="center"><input type="text" name="gold_vip['questdaily']" value="<?php echo $gold_vip['questdaily']; ?>" size="5" /></td>
						<td align="center"><input type="text" name="silver_vip['questdaily']" value="<?php echo $silver_vip['questdaily']; ?>" size="5" /></td>
                        <td align="center">
                            <select name="enable_vip['questdaily']">
                                <option value="3" <?php if($enable_vip['questdaily'] == 3) echo "selected='selected'"; ?> >3 Ngày</option>
                                <option value="7" <?php if($enable_vip['questdaily'] == 7) echo "selected='selected'"; ?> >7 Ngày</option>
								<option value="30" <?php if($enable_vip['questdaily'] == 30) echo "selected='selected'"; ?> >30 Ngày</option>
                            </select>
                        </td>                       
					</tr> 					
                </table>               
				<table border="1" style="border-collapse: collapse;" width="100%">
                    <tr><td align="center" colspan="3"><strong>Phúc Lợi Hằng Ngày</strong></td></tr>
					<tr>
                        <td align="center">Phần thưởng</td>
                        <td align="center">VIP Vàng</td>
                        <td align="center">VIP Bạc</td>
                    </tr>
					<tr>
						<td align="center">PPoint+</td>
						<td align="center"><input type="text" name="gold_daily['ppextra']" value="<?php echo $gold_daily['ppextra']; ?>" size="5" /></td>
						<td align="center"><input type="text" name="silver_daily['ppextra']" value="<?php echo $silver_daily['ppextra']; ?>" size="5" /></td>
					</tr>
					<tr>
						<td align="center">Ngọc Hỗn Nguyên</td>
						<td align="center"><input type="text" name="gold_daily['chao']" value="<?php echo $gold_daily['chao']; ?>" size="5" /></td>
						<td align="center"><input type="text" name="silver_daily['chao']" value="<?php echo $silver_daily['chao']; ?>" size="5" /></td>
				   </tr> 
					<tr>
						<td align="center">Ngọc Sáng Tạo</td>
						<td align="center"><input type="text" name="gold_daily['cre']" value="<?php echo $gold_daily['cre']; ?>" size="5" /></td>
						<td align="center"><input type="text" name="silver_daily['cre']" value="<?php echo $silver_daily['cre']; ?>" size="5" /></td>
					</tr> 
					<tr>
						<td align="center">Lông Vũ</td>
						<td align="center"><input type="text" name="gold_daily['blue']" value="<?php echo $gold_daily['blue']; ?>" size="5" /></td>
						<td align="center"><input type="text" name="silver_daily['blue']" value="<?php echo $silver_daily['blue']; ?>" size="5" /></td>
					</tr> 
					<tr>
						<td align="center">Gcent+</td>
						<td align="center"><input type="text" name="gold_daily['gcent']" value="<?php echo $gold_daily['gcent']; ?>" size="5" /></td>
						<td align="center"><input type="text" name="silver_daily['gcent']" value="<?php echo $silver_daily['gcent']; ?>" size="5" /></td>                    
					</tr> 
					<tr>
						<td align="center">Vcent+</td>
						<td align="center"><input type="text" name="gold_daily['vcent']" value="<?php echo $gold_daily['vcent']; ?>" size="5" /></td>
						<td align="center"><input type="text" name="silver_daily['vcent']" value="<?php echo $silver_daily['vcent']; ?>" size="5" /></td>                  
				    </tr> 
					<tr>
						<td align="center">Điểm Ủy Thác</td>
						<td align="center"><input type="text" name="gold_daily['pointut']" value="<?php echo $gold_daily['pointut']; ?>" size="5" /></td>
						<td align="center"><input type="text" name="silver_daily['pointut']" value="<?php echo $silver_daily['pointut']; ?>" size="5" /></td>                      
					</tr>
					<tr>
						<td align="center">Zen</td>
						<td align="center"><input type="text" name="gold_daily['zen']" value="<?php echo $gold_daily['zen']; ?>" size="5" /></td>
						<td align="center"><input type="text" name="silver_daily['zen']" value="<?php echo $silver_daily['zen']; ?>" size="5" /></td>                      
					</tr>					
                </table>
				<table border="1" style="border-collapse: collapse;" width="100%">
                    <tr><td align="center" colspan="3"><strong>Chi Phí</strong></td></tr>					
					<tr>
                        <td align="center">Gói VIP</td>
                        <td align="center">VIP Vàng</td>
                        <td align="center">VIP Bạc</td>
                    </tr>
					<tr>
						<td align="center">3 ngày</td>
						<td align="center"><input type="text" name="gold_vip['valid'][0]" value="<?php echo $gold_vip['valid'][0]; ?>" size="5" /> G.Cent</td>
						<td align="center"><input type="text" name="silver_vip['valid'][0]" value="<?php echo $silver_vip['valid'][0]; ?>" size="5" /> G.Cent</td>
					</tr>
					<tr>
						<td align="center">7 ngày</td>
						<td align="center"><input type="text" name="gold_vip['valid'][1]" value="<?php echo $gold_vip['valid'][1]; ?>" size="5" /> G.Cent</td>
						<td align="center"><input type="text" name="silver_vip['valid'][1]" value="<?php echo $silver_vip['valid'][1]; ?>" size="5" /> G.Cent</td>
					</tr>
					<tr>
						<td align="center">30 ngày</td>
						<td align="center"><input type="text" name="gold_vip['valid'][2]" value="<?php echo $gold_vip['valid'][2]; ?>" size="5" /> G.Cent</td>
						<td align="center"><input type="text" name="silver_vip['valid'][2]" value="<?php echo $silver_vip['valid'][2]; ?>" size="5" /> G.Cent</td>
					</tr>
                </table>
                <br />
				<center><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></center>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br />
			- Tên WebSite<br />
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
