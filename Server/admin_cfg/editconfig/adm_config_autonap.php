<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config_autonap.php';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }
$action = $_POST[action];

if($action == 'edit')
{
	
	$content = "<?php\n";
	
	$auto_gate = $_POST['auto_gate'];			
        if(!isset($auto_gate)) $auto_gate = 'false';
        $content .= "\$auto_gate	= $auto_gate;\n";
    $gate_doitac = $_POST['gate_doitac'];			$content .= "\$gate_doitac	= '$gate_doitac';\n";
    
    
    $telcard_use = $_POST['telcard_use'];			
        if(!isset($telcard_use)) $telcard_use = 'false';
        $content .= "\$telcard_use	= $telcard_use;\n";
    $telcard_doitac = $_POST['telcard_doitac'];			$content .= "\$telcard_doitac	= '$telcard_doitac';\n";
    
    $accgate = $_POST['accgate'];			$content .= "\$accgate	= '$accgate';\n";
        
    $ketnoipay_partnerid = $_POST['ketnoipay_partnerid'];			$content .= "\$ketnoipay_partnerid	= '$ketnoipay_partnerid';\n";
    $ketnoipay_signal = $_POST['ketnoipay_signal'];			$content .= "\$ketnoipay_signal	= '$ketnoipay_signal';\n";
    $ketnoipay_resend = $_POST['ketnoipay_resend'];			$content .= "\$ketnoipay_resend	= '". strtoupper($ketnoipay_resend) ."';\n";
    $trunggian_use = $_POST['trunggian_use'];			
    	if(!isset($trunggian_use)) $trunggian_use = 'false';
    	$content .= "\$trunggian_use	= ". $trunggian_use .";\n";
    $trunggian_url = $_POST['trunggian_url'];			$content .= "\$trunggian_url	= '". $trunggian_url ."';\n";
    
    $vippay_merchant_id = $_POST['vippay_merchant_id'];			$content .= "\$vippay_merchant_id	= '$vippay_merchant_id';\n";
    $vippay_user = $_POST['vippay_user'];			            $content .= "\$vippay_user	= '$vippay_user';\n";
    $vippay_pass = $_POST['vippay_pass'];			            $content .= "\$vippay_pass	= '$vippay_pass';\n";
    $vippay_trunggian_use = $_POST['vippay_trunggian_use'];			
    	if(!isset($vippay_trunggian_use)) $vippay_trunggian_use = 'false';
    	$content .= "\$vippay_trunggian_use	= ". $vippay_trunggian_use .";\n";
    $vippay_trunggian_url = $_POST['vippay_trunggian_url'];			$content .= "\$vippay_trunggian_url	= '". $vippay_trunggian_url ."';\n";
    
    
	$content .= "?>";
	
	require_once('admin_cfg/function.php');
	replacecontent($file_edit,$content);
	
	$notice = "<center><font color='red'>Sửa thành công</font></center>";
}

include($file_edit);
?>
		<div id="center-column">
			<div class="top-bar">
				<h1>Auto Nạp Thẻ</h1>
			</div><br>
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				<form id="editconfig" name="editconfig" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
				<table>
					<tr><td colspan="2"><strong>Tự động nạp thẻ GATE</strong></td></tr>
                    <tr>
						<td width="150" align='right' valign='top'>Sử dụng tự động nạp thẻ GATE: </td>
						<td>
                            <input type="checkbox" name="auto_gate" value="true" <?php if($auto_gate == true) echo "checked"; ?>/>
                        </td>
                    </tr>
                    <tr>
                        <td align='right' valign='top'>Nạp thẻ GATE vào: </td>
						<td>
                            <select name="gate_doitac">
                                <option value="KETNOIPAY" <?php if($gate_doitac == 'KETNOIPAY') echo "selected='selected'"; ?> >KETNOIPAY - ketnoipay.com</option>
                                <option value="VIPPAY" <?php if($gate_doitac == 'VIPPAY') echo "selected='selected'"; ?> >VIPPAY - vippay.vn</option>
                                <option value="GATE" <?php if($gate_doitac == 'GATE') echo "selected='selected'"; ?> >GATE - pay.gate.vn</option>
                            </select>
                        </td>
					</tr>
                    
                    
                    <tr><td colspan="2"><strong>Tự động nạp thẻ Điện thoại : VinaPhone, MobiPhone, Viettel</strong></td></tr>
                    <tr>
						<td align='right' valign='top'>Sử dụng tự động nạp thẻ Điện Thoại: </td>
						<td>
                            <input type="checkbox" name="telcard_use" value="true" <?php if($telcard_use == true) echo "checked"; ?>/>
                        </td>
                    </tr>
                    <tr>
                        <td align='right' valign='top'>Nạp thẻ Điện thoại vào: </td>
						<td>
                            <select name="telcard_doitac">
                                <option value="KETNOIPAY" <?php if($telcard_doitac == 'KETNOIPAY') echo "selected='selected'"; ?> >KETNOIPAY - ketnoipay.com</option>
                                <option value="VIPPAY" <?php if($telcard_doitac == 'VIPPAY') echo "selected='selected'"; ?> >VIPPAY - vippay.vn</option>
                            </select>
                        </td>
					</tr>
					<tr><td colspan="2"><hr></td></tr>
					
                    <tr><td colspan="2"><strong>Thông tin hệ thống GATE - pay.gate.vn</strong></td></tr>
                    <tr>
						<td align='right' valign='top'>Tài khoản Gate: </td>
						<td><input type="text" name="accgate" value="<?php echo $accgate; ?>" size="50"/><br /><i>(Không cần điền nếu không sử dụng)</i></td>
					</tr>
                    <tr><td colspan="2"><hr></td></tr>
                    
                    <tr><td colspan="2"><strong>Thông tin hệ thống KETNOIPAY - ketnoipay.com</strong></td></tr>
                    <tr>
						<td align='right' valign='top'>Mã khách hàng: </td>
						<td><input type="text" name="ketnoipay_partnerid" value="<?php echo $ketnoipay_partnerid; ?>" size="50" /><br /><i>(Không cần điền nếu không sử dụng)</i></td>
					</tr>
                    <tr>
						<td align='right' valign='top'>Chữ ký giao dịch: </td>
						<td><input type="text" name="ketnoipay_signal" value="<?php echo $ketnoipay_signal; ?>" size="50" /><br /><i>(Không cần điền nếu không sử dụng)</i></td>
					</tr>
                    <tr>
						<td align='right' valign='top'>Mã trả thẻ trễ: </td>
						<td><input type="text" name="ketnoipay_resend" value="<?php echo $ketnoipay_resend; ?>" size="4" maxlength="2" /><br /><i>(Mã trả gồm 2 ký tự, không được giống với các Server khác)</i></td>
					</tr>
                    <tr>
						<td align='right' valign='top'>Trung gian: </td>
						<td><input type="checkbox" name="trunggian_use" value="true" <?php if($trunggian_use == true) echo "checked"; ?>/> <input type="text" name="trunggian_url" value="<?php echo $trunggian_url; ?>" size="45" /><br /><i>(Địa chỉ trang xử lý trung gian)</i></td>
					</tr>
                    
                    <tr><td colspan="2"><hr /></td></tr>
                    
                    <tr><td colspan="2"><strong>Thông tin hệ thống VIPPAY - vippay.vn</strong></td></tr>
                    <tr>
						<td align='right' valign='top'>Merchant ID: </td>
						<td><input type="text" name="vippay_merchant_id" value="<?php echo $vippay_merchant_id; ?>" size="50" /><br /><i>(Không cần điền nếu không sử dụng)</i></td>
					</tr>
                    <tr>
						<td align='right' valign='top'>API User: </td>
						<td><input type="text" name="vippay_user" value="<?php echo $vippay_user; ?>" size="50" /><br /><i>(Không cần điền nếu không sử dụng)</i></td>
					</tr>
                    <tr>
						<td align='right' valign='top'>API Password: </td>
						<td><input type="text" name="vippay_pass" value="<?php echo $vippay_pass; ?>" size="50" /><br /><i>(Không cần điền nếu không sử dụng)</i></td>
					</tr>
                    <tr>
						<td align='right' valign='top'>Trung gian: </td>
						<td><input type="checkbox" name="vippay_trunggian_use" value="true" <?php if($vippay_trunggian_use == true) echo "checked"; ?>/> <input type="text" name="vippay_trunggian_url" value="<?php echo $vippay_trunggian_url; ?>" size="45" /><br /><i>(Địa chỉ trang xử lý trung gian)</i></td>
					</tr>
                    
                    <tr><td colspan="2"><hr /></td></tr>
                    
					<tr>
						<td>&nbsp;</td>
						<td align="center"><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></td>
					</tr>
				</table>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
