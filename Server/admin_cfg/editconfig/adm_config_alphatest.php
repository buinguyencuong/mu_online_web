<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/config_alphatest.php';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

$action = $_POST[action];

if($action == 'edit')
{
	$content = "<?php\n";
	
    if(isset($_POST['use_alphatest']) && $_POST['use_alphatest'] == 1) $use_alphatest 	= $_POST['use_alphatest'];
    else $use_alphatest = 0;
        $content .= "\$use_alphatest	= $use_alphatest;\n";
    
    
    $alphatest_gc = abs(intval($_POST['alphatest_gc']));
        $content .= "\$alphatest_gc	= $alphatest_gc;\n";
    $alphatest_vc = abs(intval($_POST['alphatest_vc']));
        $content .= "\$alphatest_vc	= $alphatest_vc;\n";
    $alphatest_wc = abs(intval($_POST['alphatest_wc']));
        $content .= "\$alphatest_wc	= $alphatest_wc;\n";
    
	$content .= "?>";
	
	require_once('admin_cfg/function.php');
	replacecontent($file_edit,$content);
	
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}

include($file_edit);
?>


		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình AlphaTest</h1>
			</div><br />
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				<form id="edit_alphatest" name="edit_alphatest" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
                Sử dụng Alpha Test : <input type="checkbox" name="use_alphatest" value="1" <?php if($use_alphatest == 1) echo "checked"; ?> /><br />
                Gcent : <input type="text" name="alphatest_gc" value="<?php if(isset($alphatest_gc)) echo $alphatest_gc; else echo 0; ?>" size="10" /><br />
                Vcent : <input type="text" name="alphatest_vc" value="<?php if(isset($alphatest_vc)) echo $alphatest_vc; else echo 0; ?>" size="10" /><br />
                Wcoin : <input type="text" name="alphatest_wc" value="<?php if(isset($alphatest_wc)) echo $alphatest_wc; else echo 0; ?>" size="10" /><br />
                
				<center><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></center>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br />
			- Tên WebSite<br />
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
