<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/config_vip.php';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

$action = $_POST[action];

if($action == 'edit')
{
	$content = "<?php\n";
	
    $vip_price = abs(intval($_POST['vip_price']));
        $content .= "\$vip_price	= $vip_price;\n";
    $vip_day = abs(intval($_POST['vip_day']));
        $content .= "\$vip_day	= $vip_day;\n";
    $vip_bua_thiensu = abs(intval($_POST['vip_bua_thiensu']));
        $content .= "\$vip_bua_thiensu	= $vip_bua_thiensu;\n";
        
    
    $vip_level_slg = abs(intval($_POST['vip_level_slg']));
        $content .= "\$vip_level_slg	= $vip_level_slg;\n";
    $vip_level_day = $_POST['vip_level_day'];
    $vip_rs_lv_decre = $_POST['vip_rs_lv_decre'];
    $vip_rs_point_incre = $_POST['vip_rs_point_incre'];
    $vip_rs_point_percent_incre = $_POST['vip_rs_point_percent_incre'];
    $flag_count = 0;
    for($i=0; $i<$vip_level_slg; $i++) {
        if($i == 0) $vip_level_day[$i] = 0;
        else $vip_level_day[$i] = abs(intval($vip_level_day[$i]));
        $vip_rs_lv_decre[$i] = abs(intval($vip_rs_lv_decre[$i]));
        $vip_rs_point_incre[$i] = abs(intval($vip_rs_point_incre[$i]));
        $vip_rs_point_percent_incre[$i] = abs(intval($vip_rs_point_percent_incre[$i]));
        if($vip_rs_lv_decre[$i] > 0 || $vip_rs_point_incre[$i] > 0 || $vip_rs_point_percent_incre[$i] > 0) {
            $content .= "\$vip_level_day[$i]	= $vip_level_day[$i];\n";
            $content .= "\$vip_rs_lv_decre[$i]	= $vip_rs_lv_decre[$i];\n";
            $content .= "\$vip_rs_point_incre[$i]	= $vip_rs_point_incre[$i];\n";
            $content .= "\$vip_rs_point_percent_incre[$i]	= $vip_rs_point_percent_incre[$i];\n";
        }
    }
    
    $vip_rs_price_decre_slg = abs(intval($_POST['vip_rs_price_decre_slg']));
        $content .= "\$vip_rs_price_decre_slg	= $vip_rs_price_decre_slg;\n";
    
    $vip_rs_price_decre_percent = $_POST['vip_rs_price_decre_percent'];
    $vip_rs_price_decre_rsday = $_POST['vip_rs_price_decre_rsday'];
    $flag_count = 0;
    for($i=0; $i<$vip_rs_price_decre_slg; $i++) {
        $vip_rs_price_decre_percent[$i] = abs(intval($vip_rs_price_decre_percent[$i]));
        $vip_rs_price_decre_rsday[$i] = abs(intval($vip_rs_price_decre_rsday[$i]));
        if($vip_rs_price_decre_percent[$i] > 0 && $vip_rs_price_decre_rsday[$i] > 0) {
            $content .= "\$vip_rs_price_decre_percent[$flag_count]	= $vip_rs_price_decre_percent[$flag_count];\n";
            $content .= "\$vip_rs_price_decre_rsday[$flag_count]	= $vip_rs_price_decre_rsday[$flag_count];\n";
            $flag_count++;
        }
    }
	
	$content .= "?>";
	
	require_once('admin_cfg/function.php');
	replacecontent($file_edit,$content);
	
	include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}

include($file_edit);
?>


		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình VIP Loại 1</h1>
			</div><br />
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				<form id="edit_vip" name="edit_vip" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
                Chi Phí : <input type="text" name="vip_price" value="<?php if(isset($vip_price)) echo $vip_price; else echo 0; ?>" size="5" /> Gcent<br />
                Thời gian VIP : <input type="text" name="vip_day" value="<?php if(isset($vip_day)) echo $vip_day; else echo 0; ?>" size="5" /> Ngày<br />
                
                Bùa Thiên Sứ : <input type="text" name="vip_bua_thiensu" value="<?php if(isset($vip_bua_thiensu)) echo $vip_bua_thiensu; else echo 0; ?>" size="5" /> ngày<br />
                <hr />
                Số cấp VIP : <input type="text" name="vip_level_slg" value="<?php if(isset($vip_level_slg)) echo $vip_level_slg; else echo 1; ?>" size="5" /><br />
                <table border="1" style="border-collapse: collapse;" width="100%">
                    <tr>
                        <td align="center">Cấp VIP</td>
                        <td align="center">Số ngày đã sử dụng VIP</td>
                        <td align="center">Giảm LV khi Reset</td>
                        <td align="center">Thêm Point khi Reset</td>
                        <td align="center">Thêm % Point khi Reset</td>
                    </tr>
                    <?php
                    for($i=0; $i<$vip_level_slg; $i++) {
                        $vip_lv = $i+1;
                        echo "<tr>";
                            echo "<td align='center'>VIP ". $vip_lv ."</td>";
                            echo '<td align="center"><input type="text" name="vip_level_day['. $i .']" value="'. $vip_level_day[$i] .'" size="5" /> ngày</td>';
                            echo '<td align="center"><input type="text" name="vip_rs_lv_decre['. $i .']" value="'. $vip_rs_lv_decre[$i] .'" size="5" /> LV</td>';
                            echo '<td align="center"><input type="text" name="vip_rs_point_incre['. $i .']" value="'. $vip_rs_point_incre[$i] .'" size="5" /> Point</td>';
                            echo '<td align="center"><input type="text" name="vip_rs_point_percent_incre['. $i .']" value="'. $vip_rs_point_percent_incre[$i] .'" size="5" /> %</td>';
                        echo "</tr>";
                    }
                    ?>
                </table>
                
                <hr />
                Số mốc giảm phí Reset : <input type="text" name="vip_rs_price_decre_slg" value="<?php if(isset($vip_rs_price_decre_slg)) echo $vip_rs_price_decre_slg; else echo 0; ?>" size="5" /><br />
                <?php
                for($i=0; $i<$vip_rs_price_decre_slg; $i++) {
                ?>
                Giảm <input type="text" name="vip_rs_price_decre_percent[<?php echo $i; ?>]" value="<?php if(isset($vip_rs_price_decre_percent[$i])) echo $vip_rs_price_decre_percent[$i]; else echo 0; ?>" size="5" />% phí Reset VIP (Gc, Vc), phí Reset thường (ZEN, Chao, Cre, Feather) từ lần Reset thứ <input type="text" name="vip_rs_price_decre_rsday[<?php echo $i; ?>]" value="<?php if(isset($vip_rs_price_decre_rsday[$i])) echo $vip_rs_price_decre_rsday[$i]; else echo 0; ?>" size="5" /><br />
                <?php } ?>
				<center><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></center>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br />
			- Tên WebSite<br />
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
