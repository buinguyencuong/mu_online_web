<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/eventgame.txt';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

require_once('admin_cfg/function.php');

// Read Giftcode
$eventgame_arr = _json_fileload($file_edit);
// Read Giftcode End

$action = $_POST[action];

if($action == 'edit')
{
    $eventgame_arr = $_POST['eventgame_arr'];
    $stt = 0;
    $kun_stt = 0;
    if(is_array($eventgame_arr)) {
        foreach($eventgame_arr as $k => $v) {
            if($k != 'kundun') {
                if(strlen($v['name']) > 0 && strlen($v['server']) > 0 && strlen($v['time']) > 0 ) {
                    $stt++;
                    $eventgame_new_arr[$stt]['name'] = $v['name'];
                    $eventgame_new_arr[$stt]['server'] = $v['server'];
                    $eventgame_new_arr[$stt]['time'] = $v['time'];
                }
            } else {
                if(is_array($v)) {
                    foreach($v as $kun_k => $kun_v) {
                        if(strlen($kun_v['server']) > 0 && strlen($kun_v['time_exclude']) > 0 && strlen($kun_v['time_next']) > 0 ) {
                            $kun_stt++;
                            $eventgame_new_arr['kundun'][$kun_stt]['server'] = $kun_v['server'];
                            $eventgame_new_arr['kundun'][$kun_stt]['dir'] = $kun_v['dir'];
                            $eventgame_new_arr['kundun'][$kun_stt]['time_exclude'] = $kun_v['time_exclude'];
                            $eventgame_new_arr['kundun'][$kun_stt]['time_next'] = $kun_v['time_next'];
                        }
                    }
                }
            }
        }
    }
    
    unset($eventgame_arr);
    $eventgame_arr = $eventgame_new_arr;
    $content = json_encode($eventgame_arr);
    replacecontent($file_edit,$content);
    
	
	include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}
?>


		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình FaceBook</h1>
			</div><br />
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				<form id="edit_facebook" name="edit_facebook" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
				<table width="100%" border="0" bgcolor="#9999FF">
				  <tr bgcolor="#FFFFFF">
				    <th scope="col" align="center">STT</th>
				    <th scope="col" align="center">Tên Event</th>
                    <th scope="col" align="center">Server</th>
                    <th scope="col" align="center">Thời gian diễn ra sự kiện<br />Dạng : 1:00|15:35|20:15</th>
				  </tr>
                <?php 
                    $stt = 0;
                    if(is_array($eventgame_arr)) {
                    foreach($eventgame_arr as $eventgame_k => $eventgame_v) { 
                        if($eventgame_k != 'kundun') {
                        $stt++;
                ?>
				  <tr bgcolor="#FFFFFF">
				    <td align="center"><?php echo $stt; ?></td>
				    <td align="center"><input name="eventgame_arr[<?php echo $stt; ?>][name]" value="<?php echo $eventgame_v['name']; ?>" size="20" /></td>
                    <td align="center"><input name="eventgame_arr[<?php echo $stt; ?>][server]" value="<?php echo $eventgame_v['server']; ?>" size="10" /></td>
                    <td align="center"><input name="eventgame_arr[<?php echo $stt; ?>][time]" value="<?php echo $eventgame_v['time']; ?>" size="40" /></td>
				  </tr>
                <?php } } } $stt++; ?>
                  <tr bgcolor="#FFFFFF">
				    <td align="center"><?php echo $stt; ?></td>
				    <td align="center"><input name="eventgame_arr[<?php echo $stt; ?>][name]" value="" size="20" /></td>
                    <td align="center"><input name="eventgame_arr[<?php echo $stt; ?>][server]" value="" size="10" /></td>
                    <td align="center"><input name="eventgame_arr[<?php echo $stt; ?>][time]" value="" size="40" /></td>
				  </tr>
				</table>
                
                <br /><br />
                <center><strong>Kundun</strong></center>
                <table width="100%" border="0" bgcolor="#9999FF">
				  <tr bgcolor="#FFFFFF">
				    <th scope="col" align="center">STT</th>
                    <th scope="col" align="center">Server</th>
                    <th scope="col" align="center">Thư mục Kundun LOG</th>
                    <th scope="col" align="center">Thời gian bỏ qua<br />Dạng : 13:05-13:25</th>
                    <th>Sống lại<br />(giây)</th>
				  </tr>
                <?php 
                    $kun_stt = 0;
                    if(is_array($eventgame_arr['kundun'])) {
                    foreach($eventgame_arr['kundun'] as $kundun_k => $kundun_v) { 
                        $kun_stt++;
                ?>
				  <tr bgcolor="#FFFFFF">
				    <td align="center"><?php echo $kun_stt; ?></td>
                    <td align="center"><input name="eventgame_arr[kundun][<?php echo $kun_stt; ?>][server]" value="<?php echo $kundun_v['server']; ?>" size="10" /></td>
                    <td align="center"><input name="eventgame_arr[kundun][<?php echo $kun_stt; ?>][dir]" value="<?php echo $kundun_v['dir']; ?>" size="40" /></td>
                    <td align="center"><input name="eventgame_arr[kundun][<?php echo $kun_stt; ?>][time_exclude]" value="<?php echo $kundun_v['time_exclude']; ?>" size="10" /></td>
                    <td align="center"><input name="eventgame_arr[kundun][<?php echo $kun_stt; ?>][time_next]" value="<?php echo $kundun_v['time_next']; ?>" size="10" /></td>
				  </tr>
                <?php } } $kun_stt++; ?>
                  <tr bgcolor="#FFFFFF">
				    <td align="center"><?php echo $kun_stt; ?></td>
                    <td align="center"><input name="eventgame_arr[kundun][<?php echo $kun_stt; ?>][server]" value="" size="10" /></td>
                    <td align="center"><input name="eventgame_arr[kundun][<?php echo $kun_stt; ?>][dir]" value="" size="40" /></td>
                    <td align="center"><input name="eventgame_arr[kundun][<?php echo $kun_stt; ?>][time_exclude]" value="" size="10" /></td>
                    <td align="center"><input name="eventgame_arr[kundun][<?php echo $kun_stt; ?>][time_next]" value="" size="10" /></td>
				  </tr>
				</table>
                
				<center><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></center>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br />
			- Tên WebSite<br />
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
