<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/config_notice.txt';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

$action = $_POST[action];

if($action == 'edit')
{
	$config_notice = urlencode(htmlspecialchars(stripcslashes($_POST['config_notice'])));
	
    $content = $config_notice;;
	
	require_once('admin_cfg/function.php');
	replacecontent($file_edit,$content);
	
    include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}

$config_notice = file_get_contents($file_edit);
?>
		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình WebSite</h1>
			</div><br>
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				<form id="editconfig" name="editconfig" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
				
                Nội dung thông báo sau khi đăng nhập tài khoản : <br />
                <textarea name="config_notice" cols="70" rows="7" id="config_notice"><?php echo urldecode(htmlspecialchars_decode($config_notice)); ?></textarea>
                <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
                <script type="text/javascript" src="ckeditor/config.js" ></script>
                <script type="text/javascript">
    			//<![CDATA[
    
    				CKEDITOR.replace( 'config_notice',
    					{
    						skin : 'kama'
    					});
    
    			//]]>
    			</script>
                
				<input type="submit" name="Submit" value="Sửa Thông Báo" />
                </form>
                
                
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
