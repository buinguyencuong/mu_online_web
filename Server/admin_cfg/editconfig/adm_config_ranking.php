<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
 $file_edit = 'config/config_ranking.php';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }
$action = $_POST[action];

if($action == 'edit')
{
	
	$content = "<?php\n";
	
	$rank_0h_slg = $_POST['rank_0h_slg'];                   $content .= "\$rank_0h_slg	= ". abs(intval($rank_0h_slg)) .";\n";
    $rank_rsrl_all = $_POST['rank_rsrl_all'];			    $content .= "\$rank_rsrl_all	= ". abs(intval($rank_rsrl_all)) .";\n";
    $rank_rsrl_class = $_POST['rank_rsrl_class'];			$content .= "\$rank_rsrl_class	= ". abs(intval($rank_rsrl_class)) .";\n";
    $rank_day_slg = $_POST['rank_day_slg'];                 $content .= "\$rank_day_slg	= ". abs(intval($rank_day_slg)) .";\n";
    $rank_week_slg = $_POST['rank_week_slg'];			    $content .= "\$rank_week_slg	= ". abs(intval($rank_week_slg)) .";\n";
    $rank_month_slg = $_POST['rank_month_slg'];             $content .= "\$rank_month_slg	= ". abs(intval($rank_month_slg)) .";\n";
    $rank_event_rs_slg = $_POST['rank_event_rs_slg'];       $content .= "\$rank_event_rs_slg	= ". abs(intval($rank_event_rs_slg)) .";\n";
    $rank_event_card_slg = $_POST['rank_event_card_slg'];	$content .= "\$rank_event_card_slg	= ". abs(intval($rank_event_card_slg)) .";\n";
    $rank_other_slg = $_POST['rank_other_slg'];             $content .= "\$rank_other_slg	= ". abs(intval($rank_other_slg)) .";\n";
	
	$content .= "?>";
	
	require_once('admin_cfg/function.php');
	replacecontent($file_edit,$content);
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}

include($file_edit);
?>
		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu hình Bảng Xếp Hạng</h1>
			</div><br>
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				<form id="editconfig" name="editconfig" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
				<table>
					<tr>
						<td width="200" align="right">Số lượng Hiển Thị TOP 0h: </td>
						<td><input type="text" name="rank_0h_slg" value="<?php echo $rank_0h_slg; ?>" size="5"/></td>
					</tr>
                    <tr>
						<td width="200" align="right">Số lượng Hiển Thị TOP Reset/Relife ALL: </td>
						<td><input type="text" name="rank_rsrl_all" value="<?php echo $rank_rsrl_all; ?>" size="5"/></td>
					</tr>
                    <tr>
						<td width="200" align="right">Số lượng Hiển Thị TOP Reset/Relife Class: </td>
						<td><input type="text" name="rank_rsrl_class" value="<?php echo $rank_rsrl_class; ?>" size="5"/></td>
					</tr>
                    <tr>
						<td width="200" align="right">Số lượng Hiển Thị TOP Ngày: </td>
						<td><input type="text" name="rank_day_slg" value="<?php echo $rank_day_slg; ?>" size="5"/></td>
					</tr>
                    <tr>
						<td width="200" align="right">Số lượng Hiển Thị TOP Tuần: </td>
						<td><input type="text" name="rank_week_slg" value="<?php echo $rank_week_slg; ?>" size="5"/></td>
					</tr>
                    <tr>
						<td width="200" align="right">Số lượng Hiển Thị TOP Tháng: </td>
						<td><input type="text" name="rank_month_slg" value="<?php echo $rank_month_slg; ?>" size="5"/></td>
					</tr>
                    <tr>
						<td width="200" align="right">Số lượng Hiển Thị TOP Event Reset: </td>
						<td><input type="text" name="rank_event_rs_slg" value="<?php echo $rank_event_rs_slg; ?>" size="5"/></td>
					</tr>
                    <tr>
						<td width="200" align="right">Số lượng Hiển Thị TOP Event Nạp Thẻ: </td>
						<td><input type="text" name="rank_event_card_slg" value="<?php echo $rank_event_card_slg; ?>" size="5"/></td>
					</tr>
                    <tr>
						<td width="200" align="right">Số lượng Hiển Thị TOP Khác: </td>
						<td><input type="text" name="rank_other_slg" value="<?php echo $rank_other_slg; ?>" size="5"/></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td align="center"><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></td>
					</tr>
				</table>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
