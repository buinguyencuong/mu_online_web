<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/config_linkdown.php';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }
$action = $_POST[action];

if($action == 'edit')
{
	
	$content = "<?php\n";
	
	$download_link 	= $_POST['download_link'];
    $link_name 	= $_POST['link_name']; 
	$capacity 	= $_POST['capacity']; 		
    
    $index = 0;
    for($i=0; $i<count($download_link); $i++)
    {
        if(strlen($download_link[$i]) > 0 && strlen($link_name[$i]) > 0) {
			$content .= "\$link_name[$index]	= '$link_name[$i]';\n";
			$content .= "\$capacity[$index]	= '$capacity[$i]';\n";
            $content .= "\$download_link[$index]	= '$download_link[$i]';\n";
            $index++;
        }
    }
	
	$content .= "?>";
	
	require_once('admin_cfg/function.php');
	replacecontent($file_edit,$content);
	
	include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}

unset($download_link);
unset($link_name);
unset($capacity);
include($file_edit);
?>
		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình Links Tải Game</h1>
			</div><br>
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				<form id="editconfig" name="editconfig" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
				<table width="100%" border="0" bgcolor="#9999FF">
				  <tr bgcolor="#FFFFFF">
				    <th scope="col" align="center">STT</th>
					<th scope="col" align="center">Tên Link</th>
					<th scope="col" align="center">Dung Lượng (MB)</th>
				    <th scope="col" align="center">Link Down</th>
				  </tr>
                <?php 
                    for($i=0; $i<count($download_link); $i++) { 
                        $stt = $i+1;
                ?>
				  <tr bgcolor="#FFFFFF">
				    <td align="center"><?php echo $stt; ?></td>
					<td align="center"><input name="link_name[]" value="<?php echo $link_name[$i]; ?>" size="10" /></td>
					<td align="center"><input name="capacity[]" value="<?php echo $capacity[$i]; ?>" size="2" /></td>
				    <td align="center"><input name="download_link[]" value="<?php echo $download_link[$i]; ?>" size="55" /></td>
				  </tr>
                <?php } ?>
                  <tr bgcolor="#FFFFFF">
				    <td align="center"><?php echo $i+1; ?></td>
					<td align="center"><input name="link_name[]" value="" size="10" /></td>
					<td align="center"><input name="capacity[]" value="" size="2" /></td>
				    <td align="center"><input name="download_link[]" value="" size="55" /></td>
				  </tr>
				</table>
				<center><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></center>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
