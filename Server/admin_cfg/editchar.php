<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$act = $_GET['act'];
switch ($act) {
	case 'point_rsday': include('editchar/adm_point_rsday.php'); break;
    case 'quest_daily': include('editchar/adm_quest_daily.php'); break;
    case 'tuluyen': include('editchar/adm_tuluyen.php'); break;
    case 'songtu': include('editchar/adm_songtu.php'); break;
    case 'reset': include('editchar/adm_reset.php'); break;
	case 'resetvip': include('editchar/adm_resetvip.php'); break;
	case 'gioihanrs': include('editchar/adm_gioihanrs.php'); break;
	case 'hotrotanthu': include('editchar/adm_hotrotanthu.php'); break;
	case 'relife': include('editchar/adm_relife.php'); break;
	case 'uythacoffline': include('editchar/adm_uythacoffline.php'); break;
	case 'uythac_reset': include('editchar/adm_uythac_reset.php'); break;
	case 'uythac_resetvip': include('editchar/adm_uythac_resetvip.php'); break;
	case 'ruatoi': include('editchar/adm_ruatoi.php'); break;
	case 'rspoint': include('editchar/adm_rspoint.php'); break;
    case 'taytuy': include('editchar/adm_taytuy.php'); break;
	case 'doigioitinh': include('editchar/adm_doigioitinh.php'); break;
	case 'resetmaster': include('editchar/adm_resetmaster.php'); break;
    case 'thehe': include('editchar/adm_thehe.php'); break;
    case 'lockitem': include('editchar/adm_lockitem.php'); break;
    case 'reset_over': include('editchar/adm_reset_over.php'); break;
    case 'resetvip_over': include('editchar/adm_resetvip_over.php'); break;
    case 'buats': include('editchar/adm_buats.php'); break;
    case 'quest3': include('editchar/adm_quest3.php'); break;
    case 'title': include('editchar/adm_title.php'); break;
	default: include('checkwrite.php'); break;
}
?>