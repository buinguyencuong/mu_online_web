<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/config_maychao.txt';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

require_once('admin_cfg/function.php');
// Read MayChao Config
$maychao_arr = _json_fileload($file_edit);
// Read MayChao Config End

$action = $_POST[action];

if($action == 'edit')
{
	
	$percent_max 	= $_POST['percent_max'];
        $percent_max = abs(intval($percent_max));
	$percent_luck 	= $_POST['percent_luck'];
        $percent_luck = abs(intval($percent_luck));
	$percent_skill 	= $_POST['percent_skill'];
        $percent_skill = abs(intval($percent_skill));
	$percent_ancient_lv 	= $_POST['percent_ancient_lv'];
        $percent_ancient_lv = abs(intval($percent_ancient_lv));
	
	$maychao_arr['longcondor'] = array(
        'percent_max'   =>  $percent_max,
        'percent_luck'  =>  $percent_luck,
        'percent_skill' =>  $percent_skill,
        'percent_ancient_lv'    =>  $percent_ancient_lv
    );
    $content = json_encode($maychao_arr);
	
	replacecontent($file_edit,$content);
	
	include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}


?>


		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình Xoay Lông Vũ Condor</h1>
			</div><br />
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
            <font color='blue'><strong>Chức năng cần LIC</strong></font><br /><br />
<?php if($notice) echo $notice; ?>
				<form id="editconfig" name="editconfig" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
				<table>
					<tr>
						<td width="220" align="right">Xác xuất thành công tối đa : </td>
						<td><input type="text" name="percent_max" value="<?php echo $maychao_arr['longcondor']['percent_max']; ?>" size="5"/>%</td>
					</tr>
					<tr>
						<td align="right">Xác xuất tăng lên khi Item thần có Luck: </td>
						<td><input type="text" name="percent_luck" value="<?php echo $maychao_arr['longcondor']['percent_luck']; ?>" size="5"/></td>
					</tr>
                    <tr>
						<td align="right">Xác xuất tăng lên khi Item thần có Skill: </td>
						<td><input type="text" name="percent_skill" value="<?php echo $maychao_arr['longcondor']['percent_skill']; ?>" size="5"/></td>
					</tr>
                    <tr>
						<td align="right">Xác xuất tăng lên khi Item thần tăng 1 LV: </td>
						<td><input type="text" name="percent_ancient_lv" value="<?php echo $maychao_arr['longcondor']['percent_ancient_lv']; ?>" size="5"/></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td align="center"><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></td>
					</tr>
				</table>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
