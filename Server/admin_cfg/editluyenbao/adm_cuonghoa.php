<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/config_cuonghoa.php';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

$action = $_POST[action];

if($action == 'edit')
{
	$content = "<?php\n";
	
    
    $ch_max 	= $_POST['ch_max'];
        $ch_max = abs(intval($ch_max));
    		$content .= "\$ch_max	= $ch_max;\n";
	
    $ch_def 	= $_POST['ch_def'];
    		$content .= "\$ch_def	= $ch_def;\n";
	
    $ch_att 	= $_POST['ch_att'];
    		$content .= "\$ch_att	= $ch_att;\n";
	
    $ch_w2 	= $_POST['ch_w2'];
    		$content .= "\$ch_w2	= $ch_w2;\n";
	
    $ch_w3 	= $_POST['ch_w3'];
    		$content .= "\$ch_w3	= $ch_w3;\n";
	
    $ch_point_type 	= $_POST['ch_point_type'];
        $ch_point_type = abs(intval($ch_point_type));
    		$content .= "\$ch_point_type	= $ch_point_type;\n";
	
    $ch_pcpoint 	= $_POST['ch_pcpoint'];
        $ch_pcpoint = abs(intval($ch_pcpoint));
    		$content .= "\$ch_pcpoint	= $ch_pcpoint;\n";
	
    $ch_rsmax 	= $_POST['ch_rsmax'];
        $ch_rsmax = abs(intval($ch_rsmax));
    		$content .= "\$ch_rsmax	= $ch_rsmax;\n";
	
    $cuonghoa_cpcoban 	= $_POST['cuonghoa_cpcoban'];
        $cuonghoa_cpcoban = abs(intval($cuonghoa_cpcoban));
    		$content .= "\$cuonghoa_cpcoban	= $cuonghoa_cpcoban;\n";
	
    $cuonghoa_cpextra 	= $_POST['cuonghoa_cpextra'];
        $cuonghoa_cpextra = abs($cuonghoa_cpextra);
    		$content .= "\$cuonghoa_cpextra	= $cuonghoa_cpextra;\n";
	
    $ch_percent_lv = $_POST['ch_percent_lv'];
    foreach($ch_percent_lv as $k => $v) {
        $v = abs($v);
    		$content .= "\$ch_percent_lv[$k]	= $v;\n";
    }
    
	$content .= "?>";
	
	require_once('admin_cfg/function.php');
	replacecontent($file_edit,$content);
	
	include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}

include($file_edit);
?>


		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình Hệ thống Luyện Bảo - Cường Hóa</h1>
			</div><br />
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				
                <form id="editconfig" name="editconfig" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
                
                <table border='0' width='100%'>
                    <tr>
                        <td width='200' align='right'>Cường hóa cấp tối đa : </td>
                        <td><input type="text" name="ch_max" value="<?php echo $ch_max; ?>" size="4" /></td>
                    </tr>
                    <tr>
                        <td align='right'>Đồ Thủ : </td>
                        <td>Công thức cơ bản x <input type="text" name="ch_def" value="<?php echo $ch_def; ?>" size="4" /> </td>
                    </tr>
                    <tr>
                        <td align='right'>Đồ Công : </td>
                        <td>Công thức cơ bản x <input type="text" name="ch_att" value="<?php echo $ch_att; ?>" size="4" /> </td>
                    </tr>
                    <tr>
                        <td align='right'>Wing 2 : </td>
                        <td>Công thức cơ bản x <input type="text" name="ch_w2" value="<?php echo $ch_w2; ?>" size="4" /> </td>
                    </tr>
                    <tr>
                        <td align='right'>Wing 3 trở lên : </td>
                        <td>Công thức cơ bản x <input type="text" name="ch_w3" value="<?php echo $ch_w3; ?>" size="4" /> </td>
                    </tr>
                    <tr>
                        <td align='right'>Cường hóa sử dụng điểm : </td>
                        <td>
                            <select name="ch_point_type">
                                <option value="0" <?php if(!isset($ch_point_type) || $ch_point_type == 0) echo 'selected="selected"'; ?> >PP + PP+ + Điểm Cường Hóa</option>
                                <option value="1" <?php if($ch_point_type == 1) echo 'selected="selected"'; ?> >PP + PP+</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td align='right'>PCPoint khi Chúc Phúc 100% : </td>
                        <td><input type="text" name="ch_pcpoint" value="<?php echo $ch_pcpoint; ?>" size="4" /></td>
                    </tr>
                    <tr>
                        <td align='right'>Số lần Reset/ngày tính điểm Cường Hóa : </td>
                        <td><input type="text" name="ch_rsmax" value="<?php echo $ch_rsmax; ?>" size="4" /> (Chỉ tác dụng khi sử dụng Điểm Cường Hóa)</td>
                    </tr>
                </table>
                
                
                
                <center><strong>Công thức cơ bản</strong><br />
                Chúc phúc Cấp_x = <input type="text" name="cuonghoa_cpcoban" value="<?php echo $cuonghoa_cpcoban; ?>" size="3"/> * (1 + <input type="text" name="cuonghoa_cpextra" value="<?php echo $cuonghoa_cpextra; ?>" size="3"/> * (Cấp_x - 1))<br />
                Thăng cấp thất bại tốn 100 Điểm Cường Hóa + 1 Chao và nhận 4-5 điểm Chúc Phúc.<br /><br />
                <strong>Kiểm tra</strong> : <br />
                <?php
            	echo '<table border=1 width=600>';
                echo '<tr>';
                echo '<td align="center"><strong>Cấp CH</strong></td>';
                echo '<td align="center"><strong>Chúc Phúc</strong></td>';
                echo '<td align="center"><strong>Đồ Thủ</strong></td>';
                echo '<td align="center"><strong>Đồ Công</strong></td>';
                echo '<td align="center"><strong>W2</strong></td>';
                echo '<td align="center"><strong>W3</strong></td>';
                echo '<td align="center"><strong>%</strong></td>';
                echo '</tr>';
                $sumpoint = 0;
            	$exp_songtu_sum = 0;
                $cp_sum = 0;
            
            	for($i=1; $i<=15; $i++) {
                    $cp_cap = floor($cuonghoa_cpcoban * (1 + $cuonghoa_cpextra * ($i-1) ));
                    $cp_sum = $cp_sum + $cp_cap;
                    
                    $chao_min = ceil($cp_sum/5);
                    $chao_max = ceil($cp_sum/4);
                    
                    $cuonghoa_point_min = ceil($cp_sum/5)*100;
                    $cuonghoa_point_max = ceil($cp_sum/4)*100;
                    
                    echo '<tr>';
                    echo '<td align="center"><strong>'. $i .'</strong></td>';
                    echo '<td align="center"><strong>'. $cp_sum .'</strong></td>';
                    echo '<td align="center"><strong>'. floor($cuonghoa_point_min*$ch_def) .' - '. floor($cuonghoa_point_max*$ch_def) .'<br />'. floor($chao_min*$ch_def) .' - '. floor($chao_max*$ch_def) .'</strong></td>';
                    echo '<td align="center"><strong>'. floor($cuonghoa_point_min*$ch_att) .' - '. floor($cuonghoa_point_max*$ch_att) .'<br />'. floor($chao_min*$ch_att) .' - '. floor($chao_max*$ch_att) .'</strong></td>';
                    echo '<td align="center"><strong>'. floor($cuonghoa_point_min*$ch_w2) .' - '. floor($cuonghoa_point_max*$ch_w2) .'<br />'. floor($chao_min*$ch_w2) .' - '. floor($chao_max*$ch_w2) .'</strong></td>';
                    echo '<td align="center"><strong>'. floor($cuonghoa_point_min*$ch_w3) .' - '. floor($cuonghoa_point_max*$ch_w3) .'<br />'. floor($chao_min*$ch_w3) .' - '. floor($chao_max*$ch_w3) .'</strong></td>';
                    echo '<td align="center"><strong><input type="text" name="ch_percent_lv['. $i .']" value="'. $ch_percent_lv[$i] .'" size=4 /></strong></td>';
                    echo '</tr>';
            	}
                echo '</table>';
                ?>
                <br />
				<input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></center>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
