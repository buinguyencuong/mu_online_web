<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/

function _sync ($url_hosting, $file_sync, $data_sync) {
    global $typeupdate;
    if($typeupdate != 2) {
        include_once('admin_cfg/func_getContent.php');
        $getcontent_method = 'POST';
        $getcontent_curl = true;
        
        $getcontent_url = $url_hosting . "/hosting_sync.php";
        $getcontent_data = array(
            'file_sync'    =>  $file_sync,
            'data_sync'     =>  str_replace("+", "|plus|", $data_sync)
        ); 
        $reponse_sync = _getContent_sync($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
        
        if ( empty($reponse_sync) ) {
            $output = "Không kết nối được đến NWeb Hosting <strong>". $url_hosting ."</strong>. Vui lòng kiểm tra lại đường dẫn<br />";
        } else {
            $error_sync = read_TagName($reponse_sync, 'error');
            $success_sync = read_TagName($reponse_sync, 'success');
            
            if(strlen($error_sync) > 0) {
                switch ($error_sync){ 
                	case 1:
                        $output = "Thư mục <strong>config</strong> và các <strong>File trong thư mục config</strong> thuộc Hosting <strong>". $url_hosting ."</strong> không có quyền ghi.<br /> Vui lòng <strong>CHMOD</strong> thư mục <strong>config sang 777</strong>, <strong>các File trong thư mục config sang 666</strong><br />";
                	break;
                    
                    case 2:
                        $output = "Không tồn tại thư mục <strong>config</strong> trên Hosting <strong>{$url_hosting}</strong>.<br />";
                	break;
                    
                    case 3:
                        $output = "Thư mục <strong>config</strong> không có quyền ghi trên Hosting <strong>{$url_hosting}</strong>. Vui lòng CHMOD thư mục <strong>config</strong> sang 777.<br />";
                	break;
                    
                    case 4:
                        $output = "File trong thư mục <strong>config</strong> không có quyền ghi trên Hosting <strong>{$url_hosting}</strong>. Vui lòng CHMOD tất cả các File trong thư mục <strong>config</strong> sang 666.<br />";
                	break;
                    
                    case 8:
                        $output = "File <strong>$file_sync</strong> không tồn tại trên Hosting <strong>{$url_hosting}</strong>. Vui lòng sử dụng phần mềm FTP UP thư mục <strong>config</strong> trên Server lên Hosting<br />";
                	break;
                    
                    case 9:
                        $ip_sv_send = read_TagName($reponse_sync, 'ip');
                        $output = "IP Server $ip_sv_send gửi dữ liệu đồng bộ không cho phép trên Hosting <strong>{$url_hosting}</strong>. Vui lòng sửa đúng IP hoặc NoIP rồi sử dụng phần mềm FTP UP file <strong>config_sync.php</strong> vào thư mục <strong>config</strong> trên Hosting <strong>{$url_hosting}</strong>.<br />";
                	break;
                    
                	default :
                        $output = "Lỗi Đồng Bộ trên Hosting <strong>{$url_hosting}</strong> chưa định nghĩa.<br />";
                }
            } elseif($success_sync == 'OK') {
                $output = 'OK';
            } else {
                $output = "Thiết lập đường dẫn đến Hosting <strong>{$url_hosting}</strong> sai.<br />";
            }
        }
        
        return $output;
    } else {
        return "OK";
    }
        
}

function read_TagName($content, $tagname, $vitri = 1)
{
    $tag_begin = '<'. $tagname . '>';
    $tag_end = '</'. $tagname . '>';
    $content1 = explode($tag_begin, $content);
    $slg_string = count($content1)-1;
    $output[] = $slg_string;    // Vị trí đầu tiên xuất ra số lượng phần tử
    for($i=1; $i<count($content1); $i++)    // Duyệt từ phần tử thứ 1 đến hết
    {
        $content2 = explode($tag_end, $content1[$i]);
        $output[] = $content2[0];
    }
    
    if($vitri == 0) return $output;
    else return $output[$vitri];
}

if(!function_exists(writelog)) {
    function writelog($file, $logcontent) {
        $Date = date("h:i:sA, d/m/Y");  
    	$fp = fopen($file, "a+");  
    	fputs ($fp, "Lúc: $Date. $logcontent \n----------------------------------------------------------------------\n\n");
    	fclose($fp);
    }
}

function addcontent($filename,$content)
{
	$fp = fopen($filename, "a+");  
	fputs ($fp, "$content");  
	fclose($fp);
}

function replacecontent($filename,$content)
{
	include_once('config.php');
    
    $fp = fopen($filename, "w");  
	@fputs ($fp, "$content");  
	fclose($fp);
    
    include_once('config/config_sync.php');
    if(isset($typeupdate) && $typeupdate == 2) {
        $filename_slashes = addslashes($filename);
        $flag_update_file = "flag_update.txt";
        $fp = fopen($flag_update_file, "r");
    		$flag_update = fgets($fp);
    	fclose($fp);
        
        $flag_update_arr = json_decode($flag_update, true);
        if(!is_array($flag_update_arr)) $flag_update_arr = array();
        
        if(is_array($list_ip)) {
            foreach($list_ip as $hostip) {
                $flag_update_arr[$hostip][$filename_slashes] = 1;
            }
        }
            
        
        $flag_update_new = json_encode($flag_update_arr);
        $fp = fopen($flag_update_file, "w");  
    	@fputs ($fp, $flag_update_new);  
    	fclose($fp);
    }
}

function readcontent($filename)
{
	$fp = fopen($filename, "r");
	while (!feof($fp)) {
		$line[] = fgets($fp,1000);
	}
	fclose($fp);
	return $line;
}

function shop_load($filename)
{
	$stt = 0;
	if(is_file($filename)) {
		$fopen_host = fopen($filename, "r");
		
		while (!feof($fopen_host)) {
			$get_item = fgets($fopen_host,200);
			$get_item = preg_replace('(\n)', '', $get_item);
			if($get_item) {
				$item_info = explode('|', $get_item);
				
				$check_stat = substr($get_item,0,2);
				if($check_stat == '//') $stat = 0;
				else $stat = 1;
				
				$stt++;
				
				$item_read[] = array (
					'stt'	=> $stt,
					'key'	=> $item_info[0],
					'code'	=> $item_info[1],
					'des'	=> $item_info[2],
					'price'	=> $item_info[3],
					'target_x'		=> $item_info[4],
					'target_y'		=> $item_info[5],
					'img'	=> $item_info[6],
					'stat'	=> $stat
				);
			}
		}
	} else $fopen_host = fopen($filename, "w");
	fclose($fopen_host);
	return $item_read;
}

function display_shop($item_read,$act)
{
	$stt = count($item_read);
	
	$content = "<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
		<tr bgcolor='#FFFFFF' >
			<td align='center'>#</td>
			<td align='center'>Đồ vật</td>
			<td align='center'>Giá <br />
		    (V.Point)</td>
		    <td align='center'>x</td>
		    <td align='center'>y</td>
		    <td align='center'>Hình</td>
		    <td align='center' width='50'>&nbsp;</td>
		</tr>";
	for($i=0;$i<$stt;$i++) {
		$content .= "<tr bgcolor='#FFFFFF' >
			<td align='center'>".$item_read[$i][stt]."</td>
			<td align='center'>".$item_read[$i][des]."</td>
			<td align='center'>".number_format($item_read[$i][price], 0, ',', '.')."</td>
			<td align='center'>".$item_read[$i][target_x]."</td>
			<td align='center'>".$item_read[$i][target_y]."</td>
			<td align='center'><img src='img_item/shop_taphoa/".$item_read[$i][img]."'></td>
			<td align='center'><a href='admin.php?mod=editwebshop&act=".$act."&page=edit&item=".$item_read[$i][stt]."' target='_self'>Sửa</a> / <a href='admin.php?mod=editwebshop&act=".$act."&page=del&item=".$item_read[$i][stt]."' target='_self'>Xóa</a></td>
		</tr>";
	}
	$content .= "</table>";
	
	echo $content;
}


/**
 * reward_load()
 * $filename : file data
 * 
 * @return
 * $item_type_arr[code] = array(
 *      'item_name' =>  Item Name
 *      'price' =>  Price 1 Day
 *      'img'   =>  URL Image Item
 *      'exl_type'  =>  Loai Item hoan Hao
 *      'stat'  =>  0: Kg cho thue, 1: Cho thue
  * )
 */
function reward_load($filename)
{
	$stt = 0;
    $item_data_arr = array();
	if(is_file($filename)) {
		$fopen_host = fopen($filename, "r");
		$item_data = fgets($fopen_host);
        $item_data_arr = json_decode($item_data, true);
	} else $fopen_host = fopen($filename, "w");
	fclose($fopen_host);
	
    return $item_data_arr;
}

function reward_display($item_read, $reward_type, $act)
{
	$count_item = count($item_read[$reward_type]);
	
	$content = "<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
		<tr bgcolor='#FFFFFF' >
			<td align='center'>#</td>
			<td align='center'>Đồ vật</td>
			<td align='center'>Giá thuê 1 ngày<br />
		    (Gcoin)</td>
		    <td align='center'>Hình</td>
            <td align='center'>Cho Thuê</td>
		    <td align='center' width='50'>&nbsp;</td>
		</tr>";
	$stt = 0;
    if(is_array($item_read[$reward_type])) {
        foreach($item_read[$reward_type] as $item_key => $item_val) {
    	   $stt = ++$stt;
    		$status = ($item_val['stat'] == 1) ? '<font color="blue"><strong>Có</strong></font>' : '<font color="red">Không</font>';
            $content .= "<tr bgcolor='#FFFFFF' >
    			<td align='center'>". $stt ."</td>
    			<td align='center'>". $item_val['item_name'] ."</td>
    			<td align='center'>". number_format($item_val['price'], 0, ',', '.') ."</td>
    			<td align='center' bgcolor='#333'><img src='items/". $item_val['img'] .".gif'></td>
                <td align='center' >". $status ."</td>
    			<td align='center'><a href='admin.php?mod=editreward&act=". $act ."&page=edit&reward_type=". $reward_type ."&item=". $item_key ."' target='_self'>Sửa</a> / <a href='admin.php?mod=editreward&act=". $act ."&page=del&reward_type=". $reward_type ."&item=". $item_key ."' target='_self'>Xóa</a></td>
    		</tr>";
    	}
    }
        
	$content .= "</table>";
	
	echo $content;
}

/**
 * reward_load()
 * $filename : file data
 * 
 * @return
 * $item_type_arr[code] = array(
 *      'item_name' =>  Item Name
 *      'price' =>  Price 1 Day
 *      'img'   =>  URL Image Item
 *      'exl_type'  =>  Loai Item hoan Hao
 *      'stat'  =>  0: Kg cho thue, 1: Cho thue
  * )
 */
function webshop_load($filename)
{
	$stt = 0;
    $item_data_arr = array();
	if(is_file($filename)) {
		$fopen_host = fopen($filename, "r");
		$item_data = fgets($fopen_host);
        $item_data_arr = json_decode($item_data, true);
	} else $fopen_host = fopen($filename, "w");
	fclose($fopen_host);
	
    return $item_data_arr;
}

function webshop_display($item_read, $webshop_type, $act)
{
	$count_item = count($item_read[$webshop_type]);
	
	$content = "<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
		<tr bgcolor='#FFFFFF' >
			<td align='center'>#</td>
			<td align='center'>Đồ vật</td>
			<td align='center'>Giá bán</td>
		    <td align='center'>Hình</td>
            <td align='center'>Bán</td>
		    <td align='center' width='50'>&nbsp;</td>
		</tr>";
	$stt = 0;
    if(is_array($item_read[$webshop_type])) {
        foreach($item_read[$webshop_type] as $item_key => $item_val) {
            $stt = ++$stt;
            $money_name = '';
            if(!isset($item_val['money_type']) || (isset($item_val['money_type'][1]) && $item_val['money_type'][1] == 1) ) $money_name .= '<font color="blue"><strong>VC</strong></font>';
            if(isset($item_val['money_type'][2]) && $item_val['money_type'][2] == 1) {
                if(strlen($money_name) > 0) $money_name .= ' + ';
                $money_name .= '<font color="green"><strong>VC*</strong></font>';
            }
            if(isset($item_val['money_type'][3]) && $item_val['money_type'][3] == 1) {
                if(strlen($money_name) > 0) $money_name .= ' + ';
                $money_name .= '<font color="red"><strong>GC</strong></font>';
            }
            if(isset($item_val['money_type'][4]) && $item_val['money_type'][4] == 1) {
                if(strlen($money_name) > 0) $money_name .= ' + ';
                $money_name .= '<font color="orange"><strong>GC*</strong></font>';
            }
            
    		$status = ($item_val['stat'] == 1) ? '<font color="blue"><strong>Có</strong></font>' : '<font color="red">Không</font>';
            $content .= "<tr bgcolor='#FFFFFF' >
    			<td align='center'>". $stt ."</td>
    			<td align='center'>". $item_val['item_name'];
            if(strlen($item_val['item_name_en']) > 0) {
                $content .= " ( ". $item_val['item_name_en'] ." )";
            }
            $content .= "</td>
    			<td align='right'>". number_format($item_val['price'], 0, ',', '.') ."  ". $money_name ."</td>
    			<td align='center' bgcolor='#333'><img src='items/". $item_val['img'] .".gif'></td>
                <td align='center' >". $status ."</td>
    			<td align='center'><a href='admin.php?mod=editwebshop&act=". $act ."&page=edit&webshop_type=". $webshop_type ."&item=". $item_key ."' target='_self'>Sửa</a> / <a href='admin.php?mod=editwebshop&act=". $act ."&page=del&webshop_type=". $webshop_type ."&item=". $item_key ."' target='_self'>Xóa</a></td>
    		</tr>";
    	}
    }
        
	$content .= "</table>";
	
	echo $content;
}

function _json_fileload($filename)
{
	if(is_file($filename)) {
		$fopen_host = fopen($filename, "r");
        $file_read = fgets($fopen_host);
        
        $out_arr = json_decode($file_read, true);

	} else $fopen_host = fopen($filename, "w");
	fclose($fopen_host);
    
    if(!is_array($out_arr)) $out_arr = array();
	return $out_arr;
}

function _giftcode_load($filename)
{
	if(is_file($filename)) {
		$fopen_host = fopen($filename, "r");
        $giftcode_read = fgets($fopen_host);
        
        $giftcode_arr = json_decode($giftcode_read, true);
        /*
        $giftcode_arr = array(
            '1' =>  array(
                'name'  =>  'name',
                'gift'  =>  array(
                        '1' =>  array(
                            'itemcode'  =>  'lien tiep 32 ma item',
                            'bank_zen'  =>  zen ngan hang,
                            'bank_chao' =>  so luong chao ngan hang,
                            'bank_cre'  =>  so luong Cre ngan hang,
                            'bank_blue' =>  so luong Blue ngan hang,
                            'phucloi_point' =>  diem phuc loi,
                            'des' =>  mo ta,
                            'rate'   =>  xac xuat xay ra,
                            'tyle' =>  ty le x/10.000
                            'rate_from'  =>  diem random xac xuat bat dau,
                            'rate_to'    =>  diem random xac xuat ket thuc
                        )
                    )
            )
        )
        */
	} else $fopen_host = fopen($filename, "w");
	fclose($fopen_host);
    
    if(!is_array($giftcode_arr)) $giftcode_arr = array();
	return $giftcode_arr;
}

function _giftcode_calculator_rate($giftcode_arr, $type) {
    $rate_total = 0;
    if(is_array($giftcode_arr[$type]['gift'])) {
        foreach($giftcode_arr[$type]['gift'] as $k => $v) {
            $rate_total += $giftcode_arr[$type]['gift'][$k]['rate'];
        }
    }
    
    $rate_from = 1;
    $idx = 0;
    if(is_array($giftcode_arr[$type]['gift'])) {
        foreach($giftcode_arr[$type]['gift'] as $k => $v) {
            $giftcode_arr[$type]['gift'][$k]['tyle'] = floor($giftcode_arr[$type]['gift'][$k]['rate']*10000/$rate_total);
            $giftcode_arr[$type]['gift'][$k]['rate_from'] = $rate_from;
                $rate_from = $rate_from + $giftcode_arr[$type]['gift'][$k]['rate'];
            $giftcode_arr[$type]['gift'][$k]['rate_to'] = $rate_from - 1;
            
            if($idx != $k) {
                $giftcode_arr[$type]['gift'][$idx] = $giftcode_arr[$type]['gift'][$k];
                unset($giftcode_arr[$type]['gift'][$k]);
            }
            $idx++;
        }
    }
    
    return $giftcode_arr;
}

function _giftcode_display($giftcode_arr,$type)
{
	
	$content = "<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
		<tr bgcolor='#FFFFFF' >
			<td align='center'>#</td>
			<td align='center'>Phần thưởng</td>
			<td align='center'>Tỷ lệ xuất hiện</td>
            <td align='center'>Tỷ lệ x/10.000</td>
		    <td align='center' width='50'>&nbsp;</td>
		</tr>";
	if(is_array($giftcode_arr[$type]['gift'])) {
	   foreach($giftcode_arr[$type]['gift'] as $k => $v) {
            $stt = $k+1;
    		$content .= "<tr bgcolor='#FFFFFF' >
    			<td align='center'>".$stt."</td>
    			<td align='center'>".$v['des']."</td>
    			<td align='center'>".$v['rate']."</td>
                <td align='center'>".number_format($v['tyle'], 0, ',', '.')."</td>
    			<td align='center'><a href='admin.php?mod=editevent&act=giftcode_random&type=".$type."&page=edit&item=".$k."' target='_self'>Sửa</a> / <a href='admin.php?mod=editevent&act=giftcode_random&type=".$type."&page=del&item=".$k."' target='_self'>Xóa</a></td>
    		</tr>";
    	}
	}
	$content .= "</table>";
	
	echo $content;
}

function _giftcode_type_load($filename)
{
	if(is_file($filename)) {
		$fopen_host = fopen($filename, "r");
        $giftcode_type_read = fgets($fopen_host);
        
        $giftcode_type_arr = json_decode($giftcode_type_read, true);
        /*
        $giftcode_type_arr = array(
            '1' =>  array(
                'name'  =>  'name',
                'gift_type'  =>  1
            )
        )
        */
	} else $fopen_host = fopen($filename, "w");
	fclose($fopen_host);
    
    if(!is_array($giftcode_type_arr)) $giftcode_type_arr = array();
	return $giftcode_type_arr;
}

function _giftcode_type_display($giftcode_type_arr, $giftcode_arr)
{
	
	$content = "<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
		<tr bgcolor='#FFFFFF' >
			<td align='center'>Mã GC</td>
			<td align='center'>Tên loại GiftCode</td>
			<td align='center'>Tên phần thưởng GiftCode</td>
		    <td align='center' width='50'>&nbsp;</td>
		</tr>";
	if(is_array($giftcode_type_arr)) {
	   foreach($giftcode_type_arr as $k => $v) {
            $giftcode_type = $v['gift_type'];
    		$content .= "<tr bgcolor='#FFFFFF' >
    			<td align='center'>".$k."</td>
    			<td align='center'>".$v['name']."</td>
                <td align='center'>".$giftcode_arr[$giftcode_type]['name']."</td>
    			<td align='center'><a href='admin.php?mod=editevent&act=giftcode_type&page=edit&item=".$k."' target='_self'>Sửa</a></td>
    		</tr>";
    	}
	}
        
	$content .= "</table>";
	
	echo $content;
}

function _giftcode_all_load($filename)
{
	if(is_file($filename)) {
		$fopen_host = fopen($filename, "r");
        $giftcode_all_read = fgets($fopen_host);
        
        $giftcode_all_arr = json_decode($giftcode_all_read, true);
        /*
        $giftcode_all_arr = array(
            'giftcode'  =>  'gift_type'
        )
        */
	} else $fopen_host = fopen($filename, "w");
	fclose($fopen_host);
    
    if(!is_array($giftcode_all_arr)) $giftcode_all_arr = array();
	return $giftcode_all_arr;
}

function _giftcode_all_display($giftcode_all_arr, $giftcode_arr)
{
	
	$content = "<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
		<tr bgcolor='#FFFFFF' >
			<td align='center'>#</td>
			<td align='center'>GiftCode</td>
			<td align='center'>Tên phần thưởng GiftCode</td>
		    <td align='center' width='50'>&nbsp;</td>
		</tr>";
	if(is_array($giftcode_all_arr)) {
	   $stt = 0;
       foreach($giftcode_all_arr as $k => $v) {
            $stt++;
            $giftcode_type = $v;
    		$content .= "<tr bgcolor='#FFFFFF' >
    			<td align='center'>".$stt."</td>
    			<td align='center'>".$k."</td>
                <td align='center'>".$giftcode_arr[$giftcode_type]['name']."</td>
    			<td align='center'><a href='admin.php?mod=editevent&act=giftcode_all&page=edit&item=".$k."' target='_self'>Sửa</a> / <a href='admin.php?mod=editevent&act=giftcode_all&page=del&item=".$k."' target='_self'>Xóa</a></td>
    		</tr>";
    	}
	}
        
	$content .= "</table>";
	
	echo $content;
}

function giftcode_load($filename)
{
	$stt = 0;
	if(is_file($filename)) {
		$fopen_host = fopen($filename, "r");
		
		while (!feof($fopen_host)) {
			$get_item = fgets($fopen_host,1000);
			$get_item = preg_replace('(\n)', '', $get_item);
			if($get_item) {
				$item_info = explode('|', $get_item);
				if(strlen($item_info[1]) > 0 && strlen($item_info[1])%32 == 0) {
				    $check_stat = substr($get_item,0,2);
    				if($check_stat == '//') $stat = 0;
    				else $stat = 1;
    				
    				$stt++;
    				
    				$item_read[] = array (
    					'stt'	=> $stt,
    					'code'	=> $item_info[1],
    					'des'	=> $item_info[2],
    					'rate'	=> $item_info[3],
    					'stat'	=> $stat
    				);
				}
			}
		}
	} else $fopen_host = fopen($filename, "w");
	fclose($fopen_host);
	return $item_read;
}

function giftcode_display($item_read,$act)
{
	$stt = count($item_read);
	
	$content = "<table width='100%' border='0' cellpadding='3' cellspacing='1' bgcolor='#9999FF'>
		<tr bgcolor='#FFFFFF' >
			<td align='center'>#</td>
			<td align='center'>Phần thưởng</td>
			<td align='center'>Tỷ lệ xuất hiện</td>
		    <td align='center' width='50'>&nbsp;</td>
		</tr>";
	for($i=0;$i<$stt;$i++) {
		$content .= "<tr bgcolor='#FFFFFF' >
			<td align='center'>".$item_read[$i][stt]."</td>
			<td align='center'>".$item_read[$i][des]."</td>
			<td align='center'>".$item_read[$i][rate]."</td>
			<td align='center'><a href='admin.php?mod=editevent&act=".$act."&page=edit&item=".$item_read[$i][stt]."' target='_self'>Sửa</a> / <a href='admin.php?mod=editevent&act=".$act."&page=del&item=".$item_read[$i][stt]."' target='_self'>Xóa</a></td>
		</tr>";
	}
	$content .= "</table>";
	
	echo $content;
}

function _event2_loadcfg($filename)
{
	if(is_file($filename)) {
		$fopen_host = fopen($filename, "r");
        $event2_cfg_read = fgets($fopen_host);
        
        $event2_cfg_arr = json_decode($event2_cfg_read, true);
        /*
        $event2_cfg_arr = array(
            'item_source' => array(
                '0' => array(               // 0: Item them loai 2, 1-3: Item nguyen lieu
                    'code'  : 32 ma Item
                    'name'  :   Ten Item
                )
            ),
            'type1' => array(               // type1 : ZEN, type2 : so luong item them, type3 : Gcent
                'daily_slg' : So luong doi toi da trong ngay
                'all_slg'   : So luong doi toi da trong 1 dot
                '0' => array(               // 0: 1 mau, 1: 2 mau, 2: 3 mau
                    'money' : Zen           // type1 : ZEN, type2 : so luong item them, type3 : Gcent
                    'gc_extra'  :   gc+
                    'vc_extra'  :   vc+
                    'pp'        :   pp
                    'pp_extra'  :   pp+
                    'pcp'       :   pcp
                    'wc'        :   wc
                    'chao'      :   chao
                    'cre'       :   cre
                    'blue'      :   blue
                    'pmaster'   :   pmaster
                )
            )
        )
        */
	} else $fopen_host = fopen($filename, "w");
	fclose($fopen_host);
    
    if(!is_array($event2_cfg_arr)) $event2_cfg_arr = array();
	return $event2_cfg_arr;
}

function jum($local) {
	header("Location: $local");
}
?>