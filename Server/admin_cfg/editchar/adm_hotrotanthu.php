<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/config_hotrotanthu.php';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

$action = $_POST[action];

if($action == 'edit')
{
	$content = "<?php\n";
	
    foreach($_POST as $key => $value) {
	   if($key != 'Submit' && $key != 'action') {
            if(!is_array($value)) {
                $value = abs(intval($value));
                $content .= "\$". $key ."	= $value;\n";
            } else {
                foreach($value as $k2 => $v2) {
                    $v2 = abs(intval($v2));
                    $content .= "\$". $key ."[". $k2 ."]	= $v2;\n";
                }
            }
                	       
	   }
	}
    $tanthu_top_cap[0] = 1;
    $content .= "\$tanthu_top_cap[0]	= $tanthu_top_cap[0];\n";

	$content .= "?>";
	
	require_once('admin_cfg/function.php');
	replacecontent($file_edit,$content);
	
	include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}

include($file_edit);
?>


		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình Hỗ trợ Tân thủ</h1>
			</div><br>
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
            <font color='blue'><strong>Chức năng cần LIC</strong></font><br /><br />
<?php if($notice) echo $notice; ?>
				<form id="editconfig" name="editconfig" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
					- Sử dụng Hỗ trợ tân thủ : 
						Không <input name="hotrotanthu" type="radio" value="0" <?php if($hotrotanthu==0) echo "checked='checked'"; ?>/>
						Có <input name="hotrotanthu" type="radio" value="1" <?php if($hotrotanthu==1) echo "checked='checked'"; ?>/>
					<br />
                    Số cấp Hỗ trợ tân thủ : <input type="text" name="tanthu_cap_max" value="<?php echo $tanthu_cap_max; ?>" size="1" />
                    <br />
                    <br /><br />
					<table width="100%" border="0" bgcolor="#9999FF">
					  <tr bgcolor="#FFFFFF">
					    <td align="center"><b>Cấp Tân thủ</b></td>
                        <td align="center"><b>TOP</b></td>
					    <td align="center"><b>LV Giảm</b></td>
                        <td align="center"><b>Time RS Giảm</b><br />(Phút)</td>
                        <td align="center"><b>ZEN RS Giảm</b><br />(%)</td>
                        <td align="center"><b>Money RS Giảm</b><br />(%)</td>
					  </tr>
					  
                      <?php for($tanthu_cap_i = 1; $tanthu_cap_i < $tanthu_cap_max; $tanthu_cap_i++) { ?>
					  <tr bgcolor="#FFFFFF">
                        <td align="center"><?php echo $tanthu_cap_i; ?></td>
                        <td align="center">TOP <b><?php echo $tanthu_top_cap[$tanthu_cap_i-1] + 1; ?></b> - <input type="text" name="tanthu_top_cap[<?php echo $tanthu_cap_i ?>]" value="<?php echo $tanthu_top_cap[$tanthu_cap_i] ?>" size="4" /></td>
					    <td align="center"><input type="text" name="tanthu_lvredure[<?php echo $tanthu_cap_i ?>]" value="<?php echo $tanthu_lvredure[$tanthu_cap_i]; ?>" size="1"/></td>
                        <td align="center"><input type="text" name="tanthu_time_rs_redure[<?php echo $tanthu_cap_i ?>]" value="<?php echo $tanthu_time_rs_redure[$tanthu_cap_i]; ?>" size="1"/></td>
                        <td align="center"><input type="text" name="tanthu_zen_rs_redure[<?php echo $tanthu_cap_i ?>]" value="<?php echo $tanthu_zen_rs_redure[$tanthu_cap_i]; ?>" size="1"/></td>
                        <td align="center"><input type="text" name="tanthu_money_rs_redure[<?php echo $tanthu_cap_i ?>]" value="<?php echo $tanthu_money_rs_redure[$tanthu_cap_i]; ?>" size="1"/></td>
					  </tr>
					  <?php } ?>
					  
                      <tr bgcolor="#FFFFFF">
                        <td align="center"><?php echo $tanthu_cap_i; ?></td>
                        <td align="center"><b>TOP > <?php echo $tanthu_top_cap[$tanthu_cap_i-1]; ?></b></td>
					    <td align="center"><input type="text" name="tanthu_lvredure[<?php echo $tanthu_cap_i ?>]" value="<?php echo $tanthu_lvredure[$tanthu_cap_i]; ?>" size="1"/></td>
                        <td align="center"><input type="text" name="tanthu_time_rs_redure[<?php echo $tanthu_cap_i ?>]" value="<?php echo $tanthu_time_rs_redure[$tanthu_cap_i]; ?>" size="1"/></td>
                        <td align="center"><input type="text" name="tanthu_zen_rs_redure[<?php echo $tanthu_cap_i ?>]" value="<?php echo $tanthu_zen_rs_redure[$tanthu_cap_i]; ?>" size="1"/></td>
                        <td align="center"><input type="text" name="tanthu_money_rs_redure[<?php echo $tanthu_cap_i ?>]" value="<?php echo $tanthu_money_rs_redure[$tanthu_cap_i]; ?>" size="1"/></td>
					  </tr>
                      
                      <tr bgcolor="#FFFFFF">
                        <td align="center" colspan="6"><br />TOP hỗ trợ Tân Thủ nhập vào cao nhất là 500.</td>
					  </tr>
					  
					</table>
				<center><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></center>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
