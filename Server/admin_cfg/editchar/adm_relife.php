<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
require_once('config/config_reset.php');
require_once('config/config_dongbo.php');

$file_edit = 'config/config_relife.php';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

$action = $_POST[action];

if($action == 'edit')
{
	$content = "<?php\n";
	
	$cap_relife_max = $_POST['cap_relife_max'];
        $cap_relife_max = abs(intval($cap_relife_max));
    		$content .= "\$cap_relife_max	= $cap_relife_max;\n";
	
    $rl_reset_relife = $_POST['rl_reset_relife'];
    $rl_point_relife = $_POST['rl_point_relife'];
    $rl_ml_relife = $_POST['rl_ml_relife'];
    $rl_zen_relife = $_POST['rl_zen_relife'];
    $rl_chao_relife = $_POST['rl_chao_relife'];
    $rl_cre_relife = $_POST['rl_cre_relife'];
    $rl_blue_relife = $_POST['rl_blue_relife'];
    
    $content .= "\$rl_reset_relife[0]	= 0;\n";
    $content .= "\$rl_point_relife[0]	= 0;\n";
    $content .= "\$rl_ml_relife[0]	= 0;\n";
    $content .= "\$rl_zen_relife[0]	= 0;\n";
    $content .= "\$rl_chao_relife[0]	= 0;\n";
    $content .= "\$rl_cre_relife[0]	= 0;\n";
    $content .= "\$rl_blue_relife[0]	= 0;\n";
    
    for($i=1; $i <= $cap_relife_max; $i++) {
        $rl_reset_relife[$i] = abs(intval($rl_reset_relife[$i]));
    		$content .= "\$rl_reset_relife[$i]	= $rl_reset_relife[$i];\n";
        $rl_point_relife[$i] = abs(intval($rl_point_relife[$i]));
    		$content .= "\$rl_point_relife[$i]	= $rl_point_relife[$i];\n";
        $rl_ml_relife[$i] = abs(intval($rl_ml_relife[$i]));
    		$content .= "\$rl_ml_relife[$i]	= $rl_ml_relife[$i];\n";
        $rl_zen_relife[$i] = abs(intval($rl_zen_relife[$i]));
    		$content .= "\$rl_zen_relife[$i]	= $rl_zen_relife[$i];\n";
        $rl_chao_relife[$i] = abs(intval($rl_chao_relife[$i]));
    		$content .= "\$rl_chao_relife[$i]	= $rl_chao_relife[$i];\n";
        $rl_cre_relife[$i] = abs(intval($rl_cre_relife[$i]));
    		$content .= "\$rl_cre_relife[$i]	= $rl_cre_relife[$i];\n";
        $rl_blue_relife[$i] = abs(intval($rl_blue_relife[$i]));
    		$content .= "\$rl_blue_relife[$i]	= $rl_blue_relife[$i];\n";
	}
	
	$content .= "?>";
	
	require_once('admin_cfg/function.php');
	replacecontent($file_edit,$content);
	
	include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}

include($file_edit);
?>


		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình ReLife</h1>
			</div><br />
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				<form id="editconfig" name="editconfig" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
					- Số cấp Relife : <input type="text" name="cap_relife_max" value="<?php echo $cap_relife_max; ?>" size="1"/><br />
					<br /><br />
					<table width="100%" border="0" bgcolor="#9999FF">
					  <tr bgcolor="#FFFFFF">
					    <th align="center" scope="col">ReLife</th>
					    <th align="center" scope="col">Reset</th>
					    <th align="center" scope="col">Point</th>
					    <th align="center" scope="col">Mệnh lệnh</th>
                        <th align="center" scope="col">ZEN</th>
                        <th align="center" scope="col">Chao</th>
                        <th align="center" scope="col">Cre</th>
                        <th align="center" scope="col">Blue</th>
					  </tr>
                    <?php
                        for($i = 1; $i<= $cap_relife_max; $i++) {
                    ?>
					  <tr bgcolor="#FFFFFF">
					    <td align="center"><?php echo $i; ?></td>
					    <td align="center"><input type="text" name="rl_reset_relife[<?php echo $i; ?>]" value="<?php echo $rl_reset_relife[$i]; ?>" size="3"/></td>
					    <td align="center"><input type="text" name="rl_point_relife[<?php echo $i; ?>]" value="<?php echo $rl_point_relife[$i]; ?>" size="5"/></td>
					    <td align="center"><input type="text" name="rl_ml_relife[<?php echo $i; ?>]" value="<?php echo $rl_ml_relife[$i]; ?>" size="5"/></td>
                        <td align="center"><input type="text" name="rl_zen_relife[<?php echo $i; ?>]" value="<?php echo $rl_zen_relife[$i]; ?>" size="8"/></td>
                        <td align="center"><input type="text" name="rl_chao_relife[<?php echo $i; ?>]" value="<?php echo $rl_chao_relife[$i]; ?>" size="1"/></td>
                        <td align="center"><input type="text" name="rl_cre_relife[<?php echo $i; ?>]" value="<?php echo $rl_cre_relife[$i]; ?>" size="1"/></td>
                        <td align="center"><input type="text" name="rl_blue_relife[<?php echo $i; ?>]" value="<?php echo $rl_blue_relife[$i]; ?>" size="1"/></td>
					  </tr>
                    <?php
                        }
                    ?>
					</table>
				<center><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></center>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
