<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/config_tuluyen.php';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

$action = $_POST[action];

if($action == 'edit')
{
	$content = "<?php\n";
	
    $tl_max = $_POST['tl_max'];
        $tl_max = abs(intval($tl_max));
    		$content .= "\$tl_max	= $tl_max;\n";
    
	$tl_cp_min = $_POST['tl_cp_min'];
        $tl_cp_min = abs(intval($tl_cp_min));
    		$content .= "\$tl_cp_min	= $tl_cp_min;\n";
    
	$tl_cp_max = $_POST['tl_cp_max'];
        $tl_cp_max = abs(intval($tl_cp_max));
    		$content .= "\$tl_cp_max	= $tl_cp_max;\n";
    
    $tl_point_type = $_POST['tl_point_type'];
        $tl_point_type = abs(intval($tl_point_type));
    		$content .= "\$tl_point_type	= $tl_point_type;\n";
    
    $tl_rsmax = $_POST['tl_rsmax'];
        $tl_rsmax = abs(intval($tl_rsmax));
    		$content .= "\$tl_rsmax	= $tl_rsmax;\n";
    
	$tl_vip_money_price = $_POST['tl_vip_money_price'];
        $tl_vip_money_price = abs(intval($tl_vip_money_price));
    		$content .= "\$tl_vip_money_price	= $tl_vip_money_price;\n";
    
	$tl_vip_money_type = $_POST['tl_vip_money_type'];
        $tl_vip_money_type = abs(intval($tl_vip_money_type));
    		$content .= "\$tl_vip_money_type	= $tl_vip_money_type;\n";
    
	$tuluyen_expcoban 	= $_POST['tuluyen_expcoban'];
        $tuluyen_expcoban = abs(intval($tuluyen_expcoban));
    		$content .= "\$tuluyen_expcoban	= $tuluyen_expcoban;\n";
	
    $tuluyen_expextra 	= $_POST['tuluyen_expextra'];
        $tuluyen_expextra = abs($tuluyen_expextra);
    		$content .= "\$tuluyen_expextra	= $tuluyen_expextra;\n";
	
    $tuluyen_pointcoban 	= $_POST['tuluyen_pointcoban'];
        $tuluyen_pointcoban = abs(intval($tuluyen_pointcoban));
    		$content .= "\$tuluyen_pointcoban	= $tuluyen_pointcoban;\n";
	
    $tuluyen_pointextra 	= $_POST['tuluyen_pointextra'];
        $tuluyen_pointextra = abs($tuluyen_pointextra);
    		$content .= "\$tuluyen_pointextra	= $tuluyen_pointextra;\n";
	
	$tuluyen_cpcoban 	= $_POST['tuluyen_cpcoban'];
        $tuluyen_cpcoban = abs(intval($tuluyen_cpcoban));
    		$content .= "\$tuluyen_cpcoban	= $tuluyen_cpcoban;\n";
	
    $tuluyen_cpextra 	= $_POST['tuluyen_cpextra'];
        $tuluyen_cpextra = abs($tuluyen_cpextra);
    		$content .= "\$tuluyen_cpextra	= $tuluyen_cpextra;\n";
	
	$content .= "?>";
	
	require_once('admin_cfg/function.php');
	replacecontent($file_edit,$content);
	
	include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}

include($file_edit);
?>


		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình Hệ thống Tu Luyện</h1>
			</div><br>
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				<form id="editconfig" name="editconfig" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
                <table border='0' width='100%'>
                    <tr>
                        <td width='200' align='right'>Cấp tu luyện tối đa : </td>
                        <td><input type="text" name="tl_max" value="<?php echo $tl_max; ?>" size="4" /></td>
                    </tr>
                    <tr>
                        <td align='right'>Tu luyện sử dụng điểm : </td>
                        <td>
                            <select name="tl_point_type">
                                <option value="0" <?php if(!isset($tl_point_type) || $tl_point_type == 0) echo 'selected="selected"'; ?> >PP + PP+ + Điểm Tu Luyện</option>
                                <option value="1" <?php if($tl_point_type == 1) echo 'selected="selected"'; ?> >PP + PP+</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td align='right'>Số lần Reset/ngày tính điểm Tu Luyện : </td>
                        <td><input type="text" name="tl_rsmax" value="<?php echo $tl_rsmax; ?>" size="4" /> (Chỉ tác dụng khi sử dụng Điểm Tu Luyện)</td>
                    </tr>
                    <tr>
                        <td align='right'>Tu luyện VIP sử dụng tiền tệ : </td>
                        <td>
                            <input type="text" name="tl_vip_money_price" value="<?php echo $tl_vip_money_price; ?>" size="4" />
                            <select name="tl_vip_money_type">
                                <option value="0" <?php if(!isset($tl_vip_money_type) || $tl_vip_money_type == 0) echo 'selected="selected"'; ?> >Gcent + Gcent+ + Vcent + Vcent+</option>
                                <option value="1" <?php if($tl_vip_money_type == 1) echo 'selected="selected"'; ?> >Gcent + Gcent+ + Vcent</option>
                            </select>
                            / 1 Exp Tu Luyện
                        </td>
                    </tr>
                </table>
                
				<center><strong>Công thức</strong><br />
                Điểm Tu Luyện Cấp_x = <input type="text" name="tuluyen_expcoban" value="<?php echo $tuluyen_expcoban; ?>" size="3"/> * (1 + <input type="text" name="tuluyen_expextra" value="<?php echo $tuluyen_expextra; ?>" size="3"/> * (Cấp_x - 1))<br />
                Point Cấp_x = <input type="text" name="tuluyen_pointcoban" value="<?php echo $tuluyen_pointcoban; ?>" size="3"/> * (1 + <input type="text" name="tuluyen_pointextra" value="<?php echo $tuluyen_pointextra; ?>" size="3"/> * (Cấp_x - 1))<br />
                Chúc phúc Cấp_x = <input type="text" name="tuluyen_cpcoban" value="<?php echo $tuluyen_cpcoban; ?>" size="3"/> * (1 + <input type="text" name="tuluyen_cpextra" value="<?php echo $tuluyen_cpextra; ?>" size="3"/> * (Cấp_x - 1))<br />
                Thăng cấp thất bại tốn 1 Chao và nhận <input type="text" name="tl_cp_min" value="<?php echo $tl_cp_min; ?>" size="4" /> - <input type="text" name="tl_cp_max" value="<?php echo $tl_cp_max; ?>" size="4" /> điểm Chúc Phúc.<br />
                <strong>Kiểm tra</strong> : <br />
                <?php
            	$sumpoint = 0;
            	$exp_tuluyen_sum = 0;
                $cp_sum = 0;
                echo '<table border=1 width=600>';
                echo '<tr>';
                echo '<td align="center"><strong>Cấp TL</strong></td>';
                echo '<td align="center"><strong>Điểm TL cấp</strong></td>';
                echo '<td align="center"><strong>Điểm TL Tổng</strong></td>';
                echo '<td align="center"><strong>Điểm Stat cấp</strong></td>';
                echo '<td align="center"><strong>Điểm Stat Tổng</strong></td>';
                echo '<td align="center"><strong>Chúc Phúc Thăng Cấp</strong></td>';
                echo '<td align="center"><strong>Chao (Min-Max)</strong></td>';
                echo '</tr>';
            	for($i=1; $i<=$tl_max; $i++) {
            	    $exp_tuluyen = floor($tuluyen_expcoban * (1 + $tuluyen_expextra * ($i-1) ));
            	    $exp_tuluyen_sum = $exp_tuluyen_sum + $exp_tuluyen;
            	    
                    $pointcap = floor($tuluyen_pointcoban * (1 + $tuluyen_pointextra * ($i-1) ));
            	    $sumpoint = $sumpoint + $pointcap;
                    
                    $cp_cap = floor($tuluyen_cpcoban * (1 + $tuluyen_cpextra * ($i-1) ));
                    $cp_sum = $cp_sum + $cp_cap;
                    
                    $chao_min = ceil($cp_sum/$tl_cp_max);
                    $chao_max = ceil($cp_sum/$tl_cp_min);
                    
                    echo '<tr>';
                    echo "<td align='center'><strong>$i</strong></td>";
                    echo "<td align='right'>$exp_tuluyen</td>";
                    echo "<td align='right'>$exp_tuluyen_sum</td>";
                    echo "<td align='right'>$pointcap</td>";
                    echo "<td align='right'><strong>$sumpoint</strong></td>";
                    echo "<td align='right'>$cp_sum</td>";
                    echo "<td align='center'>$chao_min - $chao_max</td>";
                    echo '</tr>';
                    
            	    //echo "Cấp $i : Điểm TL cần <strong>$exp_tuluyen</strong> / $exp_tuluyen_sum Điểm TL Tổng - Point nhận : <strong>$pointcap</strong> / $sumpoint Point Tổng. Cần $cp_sum Chúc Phúc ($chao_min - $chao_max Chao)<br />";
            	}
                echo '</table>';
                ?>
                <br />
				<input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></center>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
