<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/config_questdaily.php';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

require_once('admin_cfg/function.php');

//Đọc File GiftCode Type
$file_giftcode_type = 'config/giftcode_type.txt';
$giftcode_type_arr = array();
if(is_file($file_giftcode_type)) {
	$fopen_host = fopen($file_giftcode_type, "r");
    $giftcode_type_read = fgets($fopen_host);
    
    $giftcode_type_arr = json_decode($giftcode_type_read, true);
} else $fopen_host = fopen($file_giftcode_type, "w");
fclose($fopen_host);

if(!is_array($giftcode_type_arr)) $giftcode_type_arr = array();

$action = $_POST[action];

if($action == 'edit')
{
	$content = "<?php\n";
	
    $quest_daily_slgmax = abs(intval($_POST['quest_daily_slgmax']));
        $content .= "\$quest_daily_slgmax	= $quest_daily_slgmax;\n";
        
    $quest_daily = $_POST['quest_daily'];
    $quest_daily_dk = $_POST['quest_daily_dk'];
    $quest_daily_gcoin = $_POST['quest_daily_gcoin'];
    $quest_daily_gcoinkm = $_POST['quest_daily_gcoinkm'];
    
    $quest_daily_vpoint = $_POST['quest_daily_vpoint'];
    $quest_daily_vpoint_km = $_POST['quest_daily_vpoint_km'];
    
    $quest_daily_pl = $_POST['quest_daily_pl'];
    $quest_daily_pl_extra = $_POST['quest_daily_pl_extra'];
    
    $quest_daily_pcpoint = $_POST['quest_daily_pcpoint'];
    $quest_daily_wcoin = $_POST['quest_daily_wcoin'];
    
    $quest_daily_chao = $_POST['quest_daily_chao'];
    $quest_daily_cre = $_POST['quest_daily_cre'];
    $quest_daily_blue = $_POST['quest_daily_blue'];
    
    $quest_daily_pmaster = $_POST['quest_daily_pmaster'];
    
    $quest_daily_exp = $_POST['quest_daily_exp'];
    $quest_daily_giftcode = $_POST['quest_daily_giftcode'];
    
    $quest_daily_item_reset = $_POST['quest_daily_item_reset'];
    
    $quest_daily_item_time = $_POST['quest_daily_item_time'];
    $quest_daily_item_time_day = $_POST['quest_daily_item_time_day'];
    
    $quest_daily_item = $_POST['quest_daily_item'];
    
    for($i=1; $i<=49; $i++) {
        if(!isset($quest_daily[$i])) $quest_daily[$i] = 0;
        if(!isset($quest_daily_dk[$i])) $quest_daily_dk[$i] = 0; else $quest_daily_dk[$i] = abs(intval($quest_daily_dk[$i]));
        if(!isset($quest_daily_gcoin[$i])) $quest_daily_gcoin[$i] = 0; else $quest_daily_gcoin[$i] = abs(intval($quest_daily_gcoin[$i]));
        if(!isset($quest_daily_gcoinkm[$i])) $quest_daily_gcoinkm[$i] = 0; else $quest_daily_gcoinkm[$i] = abs(intval($quest_daily_gcoinkm[$i]));
        
        if(!isset($quest_daily_vpoint[$i])) $quest_daily_vpoint[$i] = 0; else $quest_daily_vpoint[$i] = abs(intval($quest_daily_vpoint[$i]));
        if(!isset($quest_daily_vpoint_km[$i])) $quest_daily_vpoint_km[$i] = 0; else $quest_daily_vpoint_km[$i] = abs(intval($quest_daily_vpoint_km[$i]));
        
        if(!isset($quest_daily_pl[$i])) $quest_daily_pl[$i] = 0; else $quest_daily_pl[$i] = abs(intval($quest_daily_pl[$i]));
        if(!isset($quest_daily_pl_extra[$i])) $quest_daily_pl_extra[$i] = 0; else $quest_daily_pl_extra[$i] = abs(intval($quest_daily_pl_extra[$i]));
        
        if(!isset($quest_daily_pcpoint[$i])) $quest_daily_pcpoint[$i] = 0; else $quest_daily_pcpoint[$i] = abs(intval($quest_daily_pcpoint[$i]));
        if(!isset($quest_daily_wcoin[$i])) $quest_daily_wcoin[$i] = 0; else $quest_daily_wcoin[$i] = abs(intval($quest_daily_wcoin[$i]));
        
        if(!isset($quest_daily_chao[$i])) $quest_daily_chao[$i] = 0; else $quest_daily_chao[$i] = abs(intval($quest_daily_chao[$i]));
        if(!isset($quest_daily_cre[$i])) $quest_daily_cre[$i] = 0; else $quest_daily_cre[$i] = abs(intval($quest_daily_cre[$i]));
        if(!isset($quest_daily_blue[$i])) $quest_daily_blue[$i] = 0; else $quest_daily_blue[$i] = abs(intval($quest_daily_blue[$i]));
        
        if(!isset($quest_daily_pmaster[$i])) $quest_daily_pmaster[$i] = 0; else $quest_daily_pmaster[$i] = abs(intval($quest_daily_pmaster[$i]));
        
        if(!isset($quest_daily_exp[$i])) $quest_daily_exp[$i] = 0; else $quest_daily_exp[$i] = abs(intval($quest_daily_exp[$i]));
        if(!isset($quest_daily_giftcode[$i])) $quest_daily_giftcode[$i] = 0; else $quest_daily_giftcode[$i] = abs(intval($quest_daily_giftcode[$i]));
        
        if(!isset($quest_daily_item_reset[$i])) $quest_daily_item_reset[$i] = 0; else $quest_daily_item_reset[$i] = abs(intval($quest_daily_item_reset[$i]));
        
        if(!isset($quest_daily_item_time[$i])) $quest_daily_item_time[$i] = ''; else $quest_daily_item_time[$i] = $quest_daily_item_time[$i];
            if( strlen($quest_daily_item_time[$i])%32 != 0 ) {
                $err .= "Dữ liệu lỗi Mã Item nhiệm vụ thứ <strong>$i</strong> : <strong>". $quest_daily_item_time[$i] ."</strong> sai cấu trúc.<br />";
            }
        if(!isset($quest_daily_item_time_day[$i])) $quest_daily_item_time_day[$i] = 0; else $quest_daily_item_time_day[$i] = abs(intval($quest_daily_item_time_day[$i]));
        $quest_daily_item_time_info = '';
        
        if(!isset($quest_daily_item[$i])) $quest_daily_item[$i] = ''; else $quest_daily_item[$i] = $quest_daily_item[$i];
            if( strlen($quest_daily_item[$i])%32 != 0 ) {
                $err .= "Dữ liệu lỗi Mã Item nhiệm vụ thứ <strong>$i</strong> : <strong>". $quest_daily_item[$i] ."</strong> sai cấu trúc.<br />";
            }
        $quest_daily_item_info = '';
        
        if(strlen($quest_daily_item_time[$i]) > 0) {
            include_once('config_license.php');
            include_once('func_getContent.php');
            $getcontent_url = $url_license . "/api_iteminfo.php";
            $getcontent_data = array(
                'acclic'    =>  $acclic,
                'key'    =>  $key,
                
                'item'    =>  $quest_daily_item_time[$i]
            ); 
            
            $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
            
            if ( empty($reponse) ) {
                echo "Kết nối đến API bị gián đoạn.";
                exit();
            }
            else {
                $info = read_TagName($reponse, 'info');
                if($info == "Error") {
                    echo read_TagName($reponse, 'message');
                    exit();
                } elseif ($info == "OK") {
                    $iteminfo = read_TagName($reponse, 'iteminfo');
                    if(strlen($iteminfo) == 0) {
                        echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                        
                        $arr_view = "\nDataSend:\n";
                        foreach($getcontent_data as $k => $v) {
                            $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                        }
                        writelog("log_api.txt", $arr_view . $reponse);
                        exit();
                    } else {
                        $quest_daily_item_time_info[$i] = $iteminfo;
                    }
                } else {
                    echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
                    writelog("log_api.txt", $reponse);
                    exit();
                }
            }
        }
        
        if(strlen($quest_daily_item[$i]) > 0) {
            include_once('config_license.php');
            include_once('func_getContent.php');
            $getcontent_url = $url_license . "/api_iteminfo.php";
            $getcontent_data = array(
                'acclic'    =>  $acclic,
                'key'    =>  $key,
                
                'item'    =>  $quest_daily_item[$i]
            ); 
            
            $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
            
            if ( empty($reponse) ) {
                echo "Kết nối đến API bị gián đoạn.";
                exit();
            }
            else {
                $info = read_TagName($reponse, 'info');
                if($info == "Error") {
                    echo read_TagName($reponse, 'message');
                    exit();
                } elseif ($info == "OK") {
                    $iteminfo = read_TagName($reponse, 'iteminfo');
                    if(strlen($iteminfo) == 0) {
                        echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                        
                        $arr_view = "\nDataSend:\n";
                        foreach($getcontent_data as $k => $v) {
                            $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                        }
                        writelog("log_api.txt", $arr_view . $reponse);
                        exit();
                    } else {
                        $quest_daily_item_info[$i] = $iteminfo;
                    }
                } else {
                    echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
                    writelog("log_api.txt", $reponse);
                    exit();
                }
            }
        }
        
        $content .= "\$quest_daily[$i]	= $quest_daily[$i];\n";
        $content .= "\$quest_daily_dk[$i]	= $quest_daily_dk[$i];\n";
        
        $content .= "\$quest_daily_gcoin[$i]	= $quest_daily_gcoin[$i];\n";
        $content .= "\$quest_daily_gcoinkm[$i]	= $quest_daily_gcoinkm[$i];\n";
        
        $content .= "\$quest_daily_vpoint[$i]	= $quest_daily_vpoint[$i];\n";
        $content .= "\$quest_daily_vpoint_km[$i]	= $quest_daily_vpoint_km[$i];\n";
        
        $content .= "\$quest_daily_pl[$i]	= $quest_daily_pl[$i];\n";
        $content .= "\$quest_daily_pl_extra[$i]	= $quest_daily_pl_extra[$i];\n";
        
        $content .= "\$quest_daily_pcpoint[$i]	= $quest_daily_pcpoint[$i];\n";
        $content .= "\$quest_daily_wcoin[$i]	= $quest_daily_wcoin[$i];\n";
        
        $content .= "\$quest_daily_chao[$i]	= $quest_daily_chao[$i];\n";
        $content .= "\$quest_daily_cre[$i]	= $quest_daily_cre[$i];\n";
        $content .= "\$quest_daily_blue[$i]	= $quest_daily_blue[$i];\n";
        
        $content .= "\$quest_daily_pmaster[$i]	= $quest_daily_pmaster[$i];\n";
        
        $content .= "\$quest_daily_exp[$i]	= $quest_daily_exp[$i];\n";
        $content .= "\$quest_daily_giftcode[$i]	= $quest_daily_giftcode[$i];\n";
        
        $content .= "\$quest_daily_item_reset[$i]	= $quest_daily_item_reset[$i];\n";
        
        $content .= "\$quest_daily_item_time[$i]	= '$quest_daily_item_time[$i]';\n";
        $content .= "\$quest_daily_item_time_info[$i]	= '$quest_daily_item_time_info[$i]';\n";
        $content .= "\$quest_daily_item_time_day[$i]	= $quest_daily_item_time_day[$i];\n";
        
        $content .= "\$quest_daily_item[$i]	= '$quest_daily_item[$i]';\n";
        $content .= "\$quest_daily_item_info[$i]	= '$quest_daily_item_info[$i]';\n";
        
    }
    
	
	$content .= "?>";
	
	replacecontent($file_edit,$content);
	
	include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}

include($file_edit);
?>
<style type="text/css">
<!--
table.scroll tbody,
table.scroll thead { display: block; }

table.scroll tbody {
    height: 500px;
    overflow-y: auto;
    overflow-x: hidden;
}
-->
</style>
<script type='text/javascript'>//<![CDATA[ 
$(function(){
// Change the selector if needed
var $table = $('table.scroll'),
    $bodyCells = $table.find('tbody tr:first').children(),
    colWidth;

// Adjust the width of thead cells when window resizes
$(window).resize(function() {
    // Get the tbody columns width array
    colWidth = $bodyCells.map(function() {
        return $(this).width();
    }).get();
    
    // Set the width of thead columns
    $table.find('thead tr').children().each(function(i, v) {
        $(v).width(colWidth[i]);
    });    
}).resize(); // Trigger resize handler
});//]]>  

</script>
		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình Nhiệm vụ Phúc Lợi</h1>
			</div><br>
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				<form id="editconfig" name="editconfig" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
                Số lượng phần thưởng nhận tối đa : <input name="quest_daily_slgmax" value="<?php echo $quest_daily_slgmax; ?>" size="4" />
                <br /><br />
				<table width="620" border="0" bgcolor="#9999FF" class="scroll">
				  <tbody>
                  <?php
                    for($i=1; $i<=49; $i++) {
                        switch ($i){ 
                        	case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                                $quest_name1[$i] = "Online";
                                $quest_name2[$i] = "giờ";
                        	break;
                        
                        	case 7:
                            case 8:
                            case 9:
                            case 10:
                            case 11:
                            case 12:
                            case 13:
                                $quest_name1[$i] = "RS Tổng";
                                $quest_name2[$i] = "lần";
                        	break;
                        
                        	case 14:
                            case 15:
                            case 16:
                            case 17:
                            case 18:
                            case 19:
                            case 20:
                                $quest_name1[$i] = "RS VIP Tổng";
                                $quest_name2[$i] = "lần";
                        	break;
                        
                        	case 21:
                            case 22:
                            case 23:
                            case 24:
                            case 25:
                            case 26:
                            case 27:
                                $quest_name1[$i] = "RS OVER Tổng";
                                $quest_name2[$i] = "lần";
                        	break;
                            
                            case 28:
                            case 29:
                            case 30:
                            case 31:
                            case 32:
                            case 33:
                            case 34:
                                $quest_name1[$i] = "RS OVER VIP";
                                $quest_name2[$i] = "lần";
                        	break;
                        
                        	case 35:
                                $quest_name1[$i] = "Nạp thẻ lần";
                                $quest_name2[$i] = "";
                        	break;
                        
                        	case 36:
                            case 37:
                            case 38:
                            case 39:
                            case 40:
                            case 41:
                            case 42:
                                $quest_name1[$i] = "Nạp thẻ";
                                $quest_name2[$i] = "VNĐ";
                        	break;
                        
                      		case 43:
                            case 44:
                            case 45:
                            case 46:
                            case 47:
                            case 48:
                            case 49:
                                $quest_name1[$i] = "Sử dụng tiền tệ";
                                $quest_name2[$i] = "";
                        	break;
                
                            default :
                                $quest_name[$i] = "Chưa định nghĩa";
                        }
                        
                        
                  ?>
                  <tr bgcolor="#FFFFFF">
				    <td align="center" rowspan="2"><?php echo $i; ?><br /><input type="checkbox" name="quest_daily[<?php echo $i; ?>]" value="1" <?php if(isset($quest_daily[$i]) && $quest_daily[$i]==1) echo "checked"; ?> /></td>
				    <td align="left" width="70" rowspan="2"><strong><?php echo $quest_name1[$i] .' <input name="quest_daily_dk['. $i .']" value="'. $quest_daily_dk[$i] .'" size=6 /> '. $quest_name2[$i]; ?></strong></td>
                    
                    <td align="center" width="55">
                        Gc <input type="text" name="quest_daily_gcoin[<?php echo $i; ?>]" value="<?php echo isset($quest_daily_gcoin[$i]) ? $quest_daily_gcoin[$i] : 0; ?>" size="1" maxlength="5" /> <br />
                        Gc+ <input type="text" name="quest_daily_gcoinkm[<?php echo $i; ?>]" value="<?php echo isset($quest_daily_gcoinkm[$i]) ? $quest_daily_gcoinkm[$i] : 0; ?>" size="1" maxlength="5" />
                    </td>
                    
                    <td align="center" width="55">
                        Vc <input type="text" name="quest_daily_vpoint[<?php echo $i; ?>]" value="<?php echo isset($quest_daily_vpoint[$i]) ? $quest_daily_vpoint[$i] : 0; ?>" size="1" maxlength="5" /> <br />
                        Vc+ <input type="text" name="quest_daily_vpoint_km[<?php echo $i; ?>]" value="<?php echo isset($quest_daily_vpoint_km[$i]) ? $quest_daily_vpoint_km[$i] : 0; ?>" size="1" maxlength="5" />
                    </td>
                    
                    <td align="center" width="55">
                        PP <input type="text" name="quest_daily_pl[<?php echo $i; ?>]" value="<?php echo isset($quest_daily_pl[$i]) ? $quest_daily_pl[$i] : 0; ?>" size="1" maxlength="3" /><br />
                        PP+ <input type="text" name="quest_daily_pl_extra[<?php echo $i; ?>]" value="<?php echo isset($quest_daily_pl_extra[$i]) ? $quest_daily_pl_extra[$i] : 0; ?>" size="1" maxlength="3" />
                    </td>
                    
                    <td align="center" width="55">
                        PcP <input type="text" name="quest_daily_pcpoint[<?php echo $i; ?>]" value="<?php echo isset($quest_daily_pcpoint[$i]) ? $quest_daily_pcpoint[$i] : 0; ?>" size="1" maxlength="3" /><br />
                        Wc <input type="text" name="quest_daily_wcoin[<?php echo $i; ?>]" value="<?php echo isset($quest_daily_wcoin[$i]) ? $quest_daily_wcoin[$i] : 0; ?>" size="1" maxlength="5" />
                    </td>
                    
                    <td align="center" width="55">
                        chao <input type="text" name="quest_daily_chao[<?php echo $i; ?>]" value="<?php echo isset($quest_daily_chao[$i]) ? $quest_daily_chao[$i] : 0; ?>" size="1" maxlength="3" /><br />
                        cre <input type="text" name="quest_daily_cre[<?php echo $i; ?>]" value="<?php echo isset($quest_daily_cre[$i]) ? $quest_daily_cre[$i] : 0; ?>" size="1" maxlength="3" />
                    </td>
                    
                    <td align="center" width="55">
                        Blue <input type="text" name="quest_daily_blue[<?php echo $i; ?>]" value="<?php echo isset($quest_daily_blue[$i]) ? $quest_daily_blue[$i] : 0; ?>" size="1" maxlength="3" /><br />
                        PM <input type="text" name="quest_daily_pmaster[<?php echo $i; ?>]" value="<?php echo isset($quest_daily_pmaster[$i]) ? $quest_daily_pmaster[$i] : 0; ?>" size="1" maxlength="3" />
                    </td>
                    
                    <td align="center" width="145">
                        Bùa Exp <input type="text" name="quest_daily_exp[<?php echo $i; ?>]" value="<?php echo isset($quest_daily_exp[$i]) ? $quest_daily_exp[$i] : 0; ?>" size="1" maxlength="3" /> giờ<br />
                        <select style="width: 145px;" name="quest_daily_giftcode[<?php echo $i; ?>]">
                            <?php if(!isset($quest_daily_giftcode[$i])) $quest_daily_giftcode[$i] = 999999; ?>
                            <option value="999999" <?php if($quest_daily_giftcode[$i] == 999999) echo "selected"; ?> >- Không GiftCode -</option>
                            <?php
                            if(is_array($giftcode_type_arr)) {
                                
                                $content = '';
                                foreach($giftcode_type_arr as $k => $v) {
                                    $content .= "<option value='$k' ";
                                    if($quest_daily_giftcode[$i] == $k) $content .= "selected";
                                    $content .= " >". $v['name'] ."</option>";
                                }
                                echo $content;
                            }
                            ?>
                        </select>
                    </td>
				  </tr>
                  
                  <tr bgcolor="#FFFFFF">
                    <td colspan="7">
                        Phần thưởng vật phẩm khi Reset trên <input type="text" name="quest_daily_item_reset[<?php echo $i; ?>]" value="<?php echo isset($quest_daily_item_reset[$i]) ? $quest_daily_item_reset[$i] : 100; ?>" size="1" maxlength="3" /><br />
                        Item có thời hạn : <input type="text" name="quest_daily_item_time[<?php echo $i; ?>]" value="<?php echo isset($quest_daily_item_time[$i]) ? $quest_daily_item_time[$i] : ''; ?>" size="40" /> <input type="text" name="quest_daily_item_time_day[<?php echo $i; ?>]" value="<?php echo isset($quest_daily_item_time_day[$i]) ? $quest_daily_item_time_day[$i] : 1; ?>" size="1" maxlength="3" /> ngày
                        <br />
                        Item kg thời hạn : <input type="text" name="quest_daily_item[<?php echo $i; ?>]" value="<?php echo isset($quest_daily_item[$i]) ? $quest_daily_item[$i] : ''; ?>" size="40" />
                    </td>                  
                  </tr>
				  <?php } ?>
                  </tbody>
				</table>
				<center><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></center>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
