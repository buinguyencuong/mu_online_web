<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/config_gioihanrs.php';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

$action = $_POST[action];

if($action == 'edit')
{
	$content = "<?php\n";
	
    $ghrs_cap_max = $_POST['ghrs_cap_max'];
        $ghrs_cap_max = abs(intval($ghrs_cap_max));
        $content .= "\$ghrs_cap_max	= $ghrs_cap_max;\n";
    
    $ghrs_top_cap = $_POST['ghrs_top_cap'];
    $ghrs_top_cap[0]	= 0;
    ksort($ghrs_top_cap);
        foreach($ghrs_top_cap as $k => $v) {
            $ghrs_top_cap[$k] = abs(intval($v));
            if( ($k>0) && $ghrs_top_cap[$k] <= $ghrs_top_cap[$k-1]) $err .= "Loại 1 : TOP cấp GHRS $k phải lớn hơn TOP cấp GHRS trên.<br />";
            if( ($k>0) && $ghrs_top_cap[$k] > 500) $err .= "Loại 1 : TOP cấp GHRS $k không được lớn hơn 500.<br />";
            $content .= "\$ghrs_top_cap[$k]	= $ghrs_top_cap[$k];\n";
        }
    
    $use_gioihanrs = $_POST['use_gioihanrs'];
        foreach($use_gioihanrs as $k => $v) {
            $use_gioihanrs[$k] = abs(intval($v));
            $content .= "\$use_gioihanrs[$k]	= $use_gioihanrs[$k];\n";
        }
    
    $Use_RS_Bu = $_POST['Use_RS_Bu'];
        if(is_array($Use_RS_Bu)) {
            foreach($Use_RS_Bu as $k => $v) {
                $Use_RS_Bu[$k] = abs(intval($v));
                $content .= "\$Use_RS_Bu[$k]	= $Use_RS_Bu[$k];\n";
            }
        }
	
	$Use_UP_RS_Daily = $_POST['Use_UP_RS_Daily'];
        if(is_array($Use_UP_RS_Daily)) {
            foreach($Use_UP_RS_Daily as $k => $v) {
                $Use_UP_RS_Daily[$k] = abs(intval($v));
                $content .= "\$Use_UP_RS_Daily[$k]	= $Use_UP_RS_Daily[$k];\n";
            }
        }
    $ghrs_rsmax_cap = $_POST['ghrs_rsmax_cap'];
    $gioihanrs_rs_bu = $_POST['gioihanrs_rs_bu'];
    $gioihanrs_up_rs = $_POST['gioihanrs_up_rs'];
        foreach($ghrs_rsmax_cap as $k1 => $v1) {
            foreach($v1 as $k2 => $v2) {
                $ghrs_rsmax_cap[$k1][$k2] = abs(intval($v2));
                $content .= "\$ghrs_rsmax_cap[$k1][$k2]	= ". $ghrs_rsmax_cap[$k1][$k2] .";\n";
                
                $gioihanrs_rs_bu[$k1][$k2] = abs(intval($gioihanrs_rs_bu[$k1][$k2]));
                $content .= "\$gioihanrs_rs_bu[$k1][$k2]	= ". $gioihanrs_rs_bu[$k1][$k2] .";\n";
                
                $gioihanrs_up_rs[$k1][$k2] = abs(intval($gioihanrs_up_rs[$k1][$k2]));
                $content .= "\$gioihanrs_up_rs[$k1][$k2]	= ". $gioihanrs_up_rs[$k1][$k2] .";\n";
            }
        }
    
    $Use_RS_Bu2 = $_POST['Use_RS_Bu2'];
        if(is_array($Use_RS_Bu2)) {
            foreach($Use_RS_Bu2 as $k => $v) {
                $Use_RS_Bu2[$k] = abs(intval($v));
                $content .= "\$Use_RS_Bu2[$k]	= $Use_RS_Bu2[$k];\n";
            }
        }
	
	$Use_UP_RS_Daily2 = $_POST['Use_UP_RS_Daily2'];
        if(is_array($Use_UP_RS_Daily2)) {
            foreach($Use_UP_RS_Daily2 as $k => $v) {
                $Use_UP_RS_Daily2[$k] = abs(intval($v));
                $content .= "\$Use_UP_RS_Daily2[$k]	= $Use_UP_RS_Daily2[$k];\n";
            }
        }
        
    $ghrs2_top_cap = $_POST['ghrs2_top_cap'];
    $ghrs2_top_cap[0]	= 0;
    $ghrs2_top_cap[1]	= 1;
    ksort($ghrs2_top_cap);
        foreach($ghrs2_top_cap as $k => $v) {
            $ghrs2_top_cap[$k] = abs(intval($v));
            if( ($k>1) && $ghrs2_top_cap[$k] <= $ghrs2_top_cap[$k-1] ) $err .= "Loại 2 : TOP cấp GHRS $k phải lớn hơn TOP cấp GHRS trên.<br />";
            if( ($k>1) && $ghrs2_top_cap[$k] > 500 ) $err .= "Loại 2 : TOP cấp GHRS $k không được lớn hơn 500.<br />";
            $content .= "\$ghrs2_top_cap[$k]	= $ghrs2_top_cap[$k];\n";
        }
        
    $ghrs2_rsmax_cap = $_POST['ghrs2_rsmax_cap'];
    $gioihanrs2_rs_bu = $_POST['gioihanrs2_rs_bu'];
    $gioihanrs2_up_rs = $_POST['gioihanrs2_up_rs'];
        foreach($ghrs2_rsmax_cap as $k1 => $v1) {
            foreach($v1 as $k2 => $v2) {
                $ghrs2_rsmax_cap[$k1][$k2] = abs(intval($v2));
                $content .= "\$ghrs2_rsmax_cap[$k1][$k2]	= ". $ghrs2_rsmax_cap[$k1][$k2] .";\n";
                
                $gioihanrs2_rs_bu[$k1][$k2] = abs(intval($gioihanrs2_rs_bu[$k1][$k2]));
                $content .= "\$gioihanrs2_rs_bu[$k1][$k2]	= ". $gioihanrs2_rs_bu[$k1][$k2] .";\n";
                
                $gioihanrs2_up_rs[$k1][$k2] = abs(intval($gioihanrs2_up_rs[$k1][$k2]));
                $content .= "\$gioihanrs2_up_rs[$k1][$k2]	= ". $gioihanrs2_up_rs[$k1][$k2] .";\n";
            }
        }
    
    $ghrs2_rsmax_daily = $_POST['ghrs2_rsmax_daily'];
        foreach($ghrs2_rsmax_daily as $k1 => $v1) {
            foreach($v1 as $k2 => $v2) {
                $ghrs2_rsmax_daily[$k1][$k2] = abs(intval($v2));
                $content .= "\$ghrs2_rsmax_daily[$k1][$k2]	= ". $ghrs2_rsmax_daily[$k1][$k2] .";\n";
            }
        }
        
    $overrs_sat_extra = $_POST['overrs_sat_extra'];		
        foreach($overrs_sat_extra as $k => $v) {
            $overrs_sat_extra[$k] = abs(intval($v));
        }
    
    $overrs_sun_extra = $_POST['overrs_sun_extra'];		
        foreach($overrs_sun_extra as $k => $v) {
            $overrs_sun_extra[$k] = abs(intval($v));
        }
    
    $use_overrs = $_POST['use_overrs'];		
        foreach($use_overrs as $k => $v) {
            $use_overrs[$k] = abs(intval($v));
        }
            
    $overrs_rs = $_POST['overrs_rs'];		
        foreach($overrs_rs as $k => $v) {
            $overrs_rs[$k] = abs(intval($v));
        }
            
    $overrs_gcoin = $_POST['overrs_gcoin'];		
        foreach($overrs_gcoin as $k => $v) {
            $overrs_gcoin[$k] = abs(intval($v));
        }
    foreach($use_gioihanrs as $k1 => $v1) {
        $content .= "\$overrs_sat_extra[$k1]	= $overrs_sat_extra[$k1];\n";
        $content .= "\$overrs_sun_extra[$k1]	= $overrs_sun_extra[$k1];\n";
        $content .= "\$use_overrs[$k1]	= $use_overrs[$k1];\n";
        $content .= "\$overrs_rs[$k1]	= $overrs_rs[$k1];\n";
        $content .= "\$overrs_gcoin[$k1]	= $overrs_gcoin[$k1];\n";
    }
    
    
        
	$content .= "?>";
	
	require_once('admin_cfg/function.php');
	replacecontent($file_edit,$content);
	
	include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}

include($file_edit);
include('config/config_thehe.php');
?>


		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình Giới hạn Reset</h1>
			</div><br />
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
            <font color='blue'><strong>Chức năng cần LIC</strong></font><br /><br />
<?php if($notice) echo $notice; ?>
				<form id="editconfig" name="editconfig" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
                
                Số cấp Giới hạn Reset : <input type="text" name="ghrs_cap_max" value="<?php echo $ghrs_cap_max; ?>" size="1" />
                <br />
				<?php 
                    $view_change_ghrs_cap_top_flag = true; 
                    for($i=1; $i<count($thehe_choise); $i++) { 
                        if(strlen($thehe_choise[$i]) > 0) {
                ?>
                    - Sử dụng Giới hạn Reset <strong><font color="red"><?php echo $thehe_choise[$i]; ?></font></strong>: 
						<select name="use_gioihanrs[<?php echo $i; ?>]">
                            <option value="0" <?php if($use_gioihanrs[$i] == 0) echo "selected"; ?> >Không</option>
                            <option value="1" <?php if($use_gioihanrs[$i] == 1) echo "selected"; ?> >Loại 1</option>
                            <option value="2" <?php if($use_gioihanrs[$i] == 2) echo "selected"; ?> >Loại 2</option>
                        </select>
					<br />
                    <strong>Lưu ý</strong> : Chỉ áp dụng Giới hạn Reset <font color='blue'><strong>Loại 2</strong></font> khi đã có BXH TOP 50 lúc 0h.
                    <br /><br />
					
                <fieldset>
		           <legend>Loại 1</legend>
                    Sử dụng <strong>Reset Bù</strong> <input type="checkbox" name="Use_RS_Bu[<?php echo $i; ?>]" value="1" <?php if($Use_RS_Bu[$i] == 1) echo "checked"; ?>/> (Phí RS = Phí RS VIP + Phí thêm)<br />
                   Sử dụng <strong>Reset Hộ</strong> <input type="checkbox" name="Use_UP_RS_Daily[<?php echo $i; ?>]" value="1" <?php if($Use_UP_RS_Daily[$i] == 1) echo "checked"; ?>/> (Phí RS = Phí RS VIP + Phí thêm)<br />
                   <strong>Reset Bù</strong> & <strong>Reset Hộ</strong> chỉ có hiệu lực khi kích hoạt <strong>Reset VIP</strong>.<br /><br />
                    
                    <table width="100%" border="0" bgcolor="#9999FF">
					  <tr bgcolor="#FFFFFF">
					    <td align="center" rowspan="2"><b>Cấp GHRS</b></td>
                        <td align="center" rowspan="2"><b>TOP</b></td>
					    <td align="center" colspan="3"> <strong>Giới hạn số lần Reset tối đa / ngày</strong></td>
                        <td align="center" rowspan="2"> <strong>Reset Bù</strong><br />(Phí thêm)</td>
                        <td align="center" rowspan="2"> <strong>UP Reset</strong><br />(Phí thêm)</td>
					  </tr>
                      
                      <tr bgcolor="#FFFFFF">
					    <td align="center"><b>Thứ 2 - Thứ 6</b></td>
                        <td align="center"><b>Thứ 7</b></td>
                        <td align="center"><b>Chủ Nhật</b></td>
					  </tr>
					  
                      <?php for($ghrs_cap_i = 1; $ghrs_cap_i < $ghrs_cap_max; $ghrs_cap_i++) { ?>
                      <tr bgcolor="#FFFFFF">
					    <td align="center"><?php echo $ghrs_cap_i; ?></td>
                        <td align="center">TOP <b><?php echo $ghrs_top_cap[$ghrs_cap_i-1] + 1; ?></b> - <?php if($view_change_ghrs_cap_top_flag === true) echo '<input type="text" name="ghrs_top_cap['. $ghrs_cap_i .']" value="'. $ghrs_top_cap[$ghrs_cap_i] .'" size="4" />'; else echo $ghrs_top_cap[$ghrs_cap_i]; ?></td>
					    <td align="center">Reset tối đa <input type="text" name="ghrs_rsmax_cap[<?php echo $i; ?>][<?php echo $ghrs_cap_i; ?>]" value="<?php echo $ghrs_rsmax_cap[$i][$ghrs_cap_i]; ?>" size="1"/> lần/ngày</td>
                        <td align="center"><?php echo $ghrs_rsmax_cap[$i][$ghrs_cap_i] + floor($ghrs_rsmax_cap[$i][$ghrs_cap_i] * $overrs_sat_extra[$i]/100); ?> lần/ngày</td>
                        <td align="center"><?php echo $ghrs_rsmax_cap[$i][$ghrs_cap_i] + floor($ghrs_rsmax_cap[$i][$ghrs_cap_i] * $overrs_sun_extra[$i]/100); ?> lần/ngày</td>
                        <td align="center"><input type="text" name="gioihanrs_rs_bu[<?php echo $i; ?>][<?php echo $ghrs_cap_i; ?>]" value="<?php echo $gioihanrs_rs_bu[$i][$ghrs_cap_i]; ?>" size="1"/> Gcent</td>
                        <td align="center"><input type="text" name="gioihanrs_up_rs[<?php echo $i; ?>][<?php echo $ghrs_cap_i; ?>]" value="<?php echo $gioihanrs_up_rs[$i][$ghrs_cap_i]; ?>" size="1"/> Gcent</td>
					  </tr>
                      <?php } ?>
					  
					  <tr bgcolor="#FFFFFF">
					    <td align="center"><?php echo $ghrs_cap_i; ?></td>
                        <td align="center"><b>TOP > <?php echo $ghrs_top_cap[$ghrs_cap_i-1]; ?></b></td>
					    <td align="center">Reset tối đa <input type="text" name="ghrs_rsmax_cap[<?php echo $i; ?>][<?php echo $ghrs_cap_i; ?>]" value="<?php echo $ghrs_rsmax_cap[$i][$ghrs_cap_i]; ?>" size="1"/> lần/ngày</td>
                        <td align="center"><?php echo $ghrs_rsmax_cap[$i][$ghrs_cap_i]+floor($ghrs_rsmax_cap[$i][$ghrs_cap_i] * $overrs_sat_extra[$i]/100); ?> lần/ngày</td>
                        <td align="center"><?php echo $ghrs_rsmax_cap[$i][$ghrs_cap_i]+floor($ghrs_rsmax_cap[$i][$ghrs_cap_i] * $overrs_sun_extra[$i]/100); ?> lần/ngày</td>
                        <td align="center"><input type="text" name="gioihanrs_rs_bu[<?php echo $i; ?>][<?php echo $ghrs_cap_i; ?>]" value="<?php echo $gioihanrs_rs_bu[$i][$ghrs_cap_i]; ?>" size="1"/> Gcent</td>
                        <td align="center"><input type="text" name="gioihanrs_up_rs[<?php echo $i; ?>][<?php echo $ghrs_cap_i; ?>]" value="<?php echo $gioihanrs_up_rs[$i][$ghrs_cap_i]; ?>" size="1"/> Gcent</td>
					  </tr>
					</table>
                    <br />
                    <strong>Lưu ý</strong> : TOP GHRS nhập vào Cao nhất là <strong>500</strong>.
                </fieldset>
                
                <br /><br />
                
                <fieldset>
		           <legend>Loại 2</legend>
                   Sử dụng <strong>Reset Bù</strong> <input type="checkbox" name="Use_RS_Bu2[<?php echo $i; ?>]" value="1" <?php if($Use_RS_Bu2[$i] == 1) echo "checked"; ?>/> (Phí RS = Phí RS VIP + Phí thêm)<br />
                   Sử dụng <strong>UP Max Reset Ngày</strong> <input type="checkbox" name="Use_UP_RS_Daily2[<?php echo $i; ?>]" value="1" <?php if($Use_UP_RS_Daily2[$i] == 1) echo "checked"; ?>/> (Phí RS = Phí RS VIP + Phí thêm)<br />
                   <strong>Reset Bù</strong> & <strong>UP Max Reset ngày</strong> chỉ có hiệu lực khi kích hoạt <strong>Reset VIP</strong>.<br /><br />
                    
                   <center><strong>Lưu ý</strong> : Chỉ áp dụng Giới hạn Reset <font color='blue'><strong>Loại 2</strong></font> khi đã có BXH TOP 500 lúc 0h.</center>
                   <table width="100%" border="0" bgcolor="#9999FF">
					  <tr bgcolor="#FFFFFF">
					    <td align="center" rowspan="2"><b>Cấp GHRS</b></td>
                        <td align="center" rowspan="2"><b>TOP</b></td>
					    <td align="center" colspan="3"> <strong>Giới hạn số lần Reset tối đa / ngày</strong></td>
                        <td align="center" rowspan="2"> <strong>Reset Bù</strong><br />(Phí thêm)</td>
                        <td align="center" rowspan="2"> <strong>UP Reset</strong><br />(Phí thêm)</td>
					  </tr>
                      
                      <tr bgcolor="#FFFFFF">
					    <td align="center"><b>Thứ 2 - Thứ 6</b></td>
                        <td align="center"><b>Thứ 7</b></td>
                        <td align="center"><b>Chủ Nhật</b></td>
					  </tr>
					  
                      <tr bgcolor="#FFFFFF">
					    <td align="center">1</td>
                        <td align="center"><b>TOP 1</b></td>
					    <td align="center">Reset tối đa <input type="text" name="ghrs2_rsmax_cap[<?php echo $i; ?>][1]" value="<?php echo $ghrs2_rsmax_cap[$i][1]; ?>" size="1"/> lần/ngày</td>
                        <td align="center"><?php echo $ghrs2_rsmax_cap[$i][1]+floor($ghrs2_rsmax_cap[$i][1] * $overrs_sat_extra[$i]/100); ?> lần/ngày</td>
                        <td align="center"><?php echo $ghrs2_rsmax_cap[$i][1]+floor($ghrs2_rsmax_cap[$i][1] * $overrs_sun_extra[$i]/100); ?> lần/ngày</td>
                        <td align="center"><input type="text" name="gioihanrs2_rs_bu[<?php echo $i; ?>][1]" value="<?php echo $gioihanrs2_rs_bu[$i][1]; ?>" size="1"/> Gcent</td>
                        <td align="center"><input type="text" name="gioihanrs2_up_rs[<?php echo $i; ?>][1]" value="<?php echo $gioihanrs2_up_rs[$i][1]; ?>" size="1"/> Gcent</td>
					  </tr>
                      
                      <?php for($ghrs_cap_i = 2; $ghrs_cap_i < $ghrs_cap_max; $ghrs_cap_i++) { ?>
                      <tr bgcolor="#FFFFFF">
					    <td align="center"><?php echo $ghrs_cap_i; ?></td>
                        <td align="center">TOP <b><?php echo $ghrs2_top_cap[$ghrs_cap_i-1] + 1; ?></b> - <?php if($view_change_ghrs_cap_top_flag === true) echo '<input type="text" name="ghrs2_top_cap['. $ghrs_cap_i .']" value="'. $ghrs2_top_cap[$ghrs_cap_i] .'" size="4" />'; else echo $ghrs2_top_cap[$ghrs_cap_i]; ?></td>
					    <td align="left" colspan="3">Cách TOP 1 MAX ngày <input type="text" name="ghrs2_rsmax_cap[<?php echo $i; ?>][<?php echo $ghrs_cap_i; ?>]" value="<?php echo $ghrs2_rsmax_cap[$i][$ghrs_cap_i]; ?>" size="1"/> lần RS.<br />Tối đa <input type="text" name="ghrs2_rsmax_daily[<?php echo $i; ?>][<?php echo $ghrs_cap_i; ?>]" value="<?php echo $ghrs2_rsmax_daily[$i][$ghrs_cap_i]; ?>" size="1"/> lần RS/ngày</td>
                        <td align="center"><input type="text" name="gioihanrs2_rs_bu[<?php echo $i; ?>][<?php echo $ghrs_cap_i; ?>]" value="<?php echo $gioihanrs2_rs_bu[$i][$ghrs_cap_i]; ?>" size="1"/> Gcent</td>
                        <td align="center"><input type="text" name="gioihanrs2_up_rs[<?php echo $i; ?>][<?php echo $ghrs_cap_i; ?>]" value="<?php echo $gioihanrs2_up_rs[$i][$ghrs_cap_i]; ?>" size="1"/> Gcent</td>
					  </tr>
                      <?php } ?>
                      
                      
                      <tr bgcolor="#FFFFFF">
					    <td align="center"><?php echo $ghrs_cap_i; ?></td>
                        <td align="center"><b>TOP > <?php echo $ghrs2_top_cap[$ghrs_cap_i-1]; ?></b></td>
					    <td align="left" colspan="3">Cách TOP 1 MAX ngày <input type="text" name="ghrs2_rsmax_cap[<?php echo $i; ?>][<?php echo $ghrs_cap_i; ?>]" value="<?php echo $ghrs2_rsmax_cap[$i][$ghrs_cap_i]; ?>" size="1"/> lần RS.<br />Tối đa <input type="text" name="ghrs2_rsmax_daily[<?php echo $i; ?>][<?php echo $ghrs_cap_i; ?>]" value="<?php echo $ghrs2_rsmax_daily[$i][$ghrs_cap_i]; ?>" size="1"/> lần RS/ngày</td>
                        <td align="center"><input type="text" name="gioihanrs2_rs_bu[<?php echo $i; ?>][<?php echo $ghrs_cap_i; ?>]" value="<?php echo $gioihanrs2_rs_bu[$i][$ghrs_cap_i]; ?>" size="1"/> Gcent</td>
                        <td align="center"><input type="text" name="gioihanrs2_up_rs[<?php echo $i; ?>][<?php echo $ghrs_cap_i; ?>]" value="<?php echo $gioihanrs2_up_rs[$i][$ghrs_cap_i]; ?>" size="1"/> Gcent</td>
					  </tr>
					</table>
                    
                    <br />
                    <strong>Lưu ý</strong> : TOP GHRS nhập vào Cao nhất là <strong>500</strong>.
                </fieldset>
                    <br />
                    - Thứ 7 tăng giới hạn Reset thêm : <input name="overrs_sat_extra[<?php echo $i; ?>]" id="overrs_sat_extra" value="<?php echo $overrs_sat_extra[$i]; ?>" size="1" /> % <br />
                    - Chủ nhật tăng giới hạn Reset thêm : <input name="overrs_sun_extra[<?php echo $i; ?>]" id="overrs_sun_extra" value="<?php echo $overrs_sun_extra[$i]; ?>" size="1" /> % <br />
                    
                    <br /><br />
                    - Sử dụng Reset Vượt mức : 
						Không <input name="use_overrs[<?php echo $i; ?>]" type="radio" value="0" <?php if($use_overrs[$i]==0) echo "checked='checked'"; ?>/>
						Có <input name="use_overrs[<?php echo $i; ?>]" type="radio" value="1" <?php if($use_overrs[$i]==1) echo "checked='checked'"; ?>/>
					<br />
                    Số lần Reset vượt mức cho phép : <input name="overrs_rs[<?php echo $i; ?>]" id="overrs_rs" value="<?php echo $overrs_rs[$i]; ?>" /><br />
                    Gcoin cần thêm khi Reset vượt mức : <input name="overrs_gcoin[<?php echo $i; ?>]" id="overrs_gcoin" value="<?php echo $overrs_gcoin[$i]; ?>" />
                    <hr />
                <?php 
                        if($view_change_ghrs_cap_top_flag == true) $view_change_ghrs_cap_top_flag = false;
                    }
                }
                ?>
				<center><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></center>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br>
			- Tên WebSite<br>
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
