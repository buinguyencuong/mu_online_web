<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
$file_edit = 'config/config_title.txt';
if(!is_file($file_edit)) 
{ 
	$fp_host = fopen($file_edit, "w");
	fclose($fp_host);
}

if(is_writable($file_edit))	{ $can_write = "<font color=green>Có thể ghi</font>"; $accept = 1;}
	else { $can_write = "<font color=red>Không thể ghi - Hãy sử dụng chương trình FTP FileZilla chuyển <b>File permission</b> sang 666</font>"; $accept = 0; }

$action = $_POST[action];

if($action == 'edit')
{
	$title_arr 	= $_POST['title_arr'];
    
    if(is_array($title_arr)) {
        foreach($title_arr as $k1 => $v1) {
            if(is_array($title_arr[$k1])) {
                foreach($v1 as $k2 => $v2) {
                    $v2[5] = abs(intval($v2[5]));
                    if(strlen($v2[0]) == 0 || strlen($v2[1]) == 0 || strlen($v2[2]) == 0) {
                        unset($title_arr[$k1][$k2]);
                    }
                }
            }
        }
    }
    
    $content = json_encode($title_arr);
	
	require_once('admin_cfg/function.php');
	replacecontent($file_edit,$content);
	
	include('config/config_sync.php');
    for($i=0; $i<count($url_hosting); $i++)
    {
        if($url_hosting[$i]) {
            $sync_send = _sync($url_hosting[$i], $file_edit, $content);
            if($sync_send == 'OK') {
                
            } else {
                $err .= $sync_send;
            }
        }
    }
    
	if($err) {
        $notice = "<center><font color='red'><strong>Lỗi :</strong><br />$err</font></center>";
    } else {
    	$notice = "<center><font color='blue'>Sửa thành công</font></center>";
    }
}
$thehe_choise = null;

$fopen_host = fopen($file_edit, "r");
$title_data = fgets($fopen_host);
fclose($fopen_host);
$title_arr = json_decode($title_data, true);
if(!is_array($title_arr)) $title_arr = array();
?>


		<div id="center-column">
			<div class="top-bar">
				<h1>Cấu Hình Danh Hiệu</h1>
			</div><br />
			Tệp tin <?php echo "<b>".$file_edit."</b> : ".$can_write; ?>
		  <div class="select-bar"></div>
			<div class="table">
<?php if($notice) echo $notice; ?>
				<form id="edittitle" name="edittitle" method="post" action="">
				<input type="hidden" name="action" value="edit"/>
                
				<table width="100%" border="0" bgcolor="#9999FF" cellpadding="5" >
				  <tr bgcolor="#FFFFFF">
				    <th scope="col" align="center" colspan="4">Danh hiệu Reset</th>
				  </tr>
                  <tr bgcolor="#FFFFFF">
				    <th scope="col" align="center">Reset</th>
                    <th scope="col" align="center">Data Danh Hiệu</th>
				    <th scope="col" align="center">ID Danh Hiệu</th>
                    <th scope="col" align="center">Tên Danh Hiệu</th>
				  </tr>
                <?php 
                    $stt = 0;
                    if(is_array($title_arr['rs'])) {
                    foreach($title_arr['rs'] as $k => $v) { 
                    
                ?>
				  <tr bgcolor="#FFFFFF">
				    <td align="center"><input name="title_arr[rs][<?php echo $stt; ?>][0]" value="<?php echo $v[0]; ?>" size="4" /></td>
                    <td align="center">
                        <select name="title_arr[rs][<?php echo $stt; ?>][5]">
                            <option value="0" <?php if($title_arr['rs'][$stt][5] == 0) echo "selected"; ?>>danhhieu</option>
                            <option value="1" <?php if($title_arr['rs'][$stt][5] == 1) echo "selected"; ?>>RankTitle1</option>
                            <option value="2" <?php if($title_arr['rs'][$stt][5] == 2) echo "selected"; ?>>RankTitle2</option>
                            <option value="3" <?php if($title_arr['rs'][$stt][5] == 3) echo "selected"; ?>>RankTitle3</option>
                        </select>
                    </td>
				    <td align="center"><input name="title_arr[rs][<?php echo $stt; ?>][1]" value="<?php echo $v[1]; ?>" size="4" /></td>
                    <td align="center"><input name="title_arr[rs][<?php echo $stt; ?>][2]" value="<?php echo $v[2]; ?>" /></td>
				  </tr>
                <?php 
                        $stt++;
                    } }
                ?>
                  <tr bgcolor="#FFFFFF">
				    <td align="center"><input name="title_arr[rs][<?php echo $stt; ?>][0]" value="" size="4" /></td>
                    <td align="center">
                        <select name="title_arr[rs][<?php echo $stt; ?>][5]">
                            <option value="0">danhhieu</option>
                            <option value="1">RankTitle1</option>
                            <option value="2">RankTitle2</option>
                            <option value="3">RankTitle3</option>
                        </select>
                    </td>
				    <td align="center"><input name="title_arr[rs][<?php echo $stt; ?>][1]" value="" size="4" /></td>
                    <td align="center"><input name="title_arr[rs][<?php echo $stt; ?>][2]" value="" /></td>
				  </tr>
				</table>
                
                <table width="100%" border="0" bgcolor="#9999FF" cellpadding="5" >
				  <tr bgcolor="#FFFFFF">
				    <th scope="col" align="center" colspan="4">Danh hiệu Nạp thẻ</th>
				  </tr>
                  <tr bgcolor="#FFFFFF">
				    <th scope="col" align="center">Tổng tiền nạp</th>
                    <th scope="col" align="center">Data Danh Hiệu</th>
				    <th scope="col" align="center">ID Danh Hiệu</th>
                    <th scope="col" align="center">Tên Danh Hiệu</th>
				  </tr>
                <?php 
                    $stt = 0;
                    if(is_array($title_arr['card'])) {
                    foreach($title_arr['card'] as $k => $v) { 
                    
                ?>
				  <tr bgcolor="#FFFFFF">
				    <td align="center"><input name="title_arr[card][<?php echo $stt; ?>][0]" value="<?php echo $v[0]; ?>" size="8" /></td>
                    <td align="center">
                        <select name="title_arr[card][<?php echo $stt; ?>][5]">
                            <option value="0" <?php if($title_arr['card'][$stt][5] == 0) echo "selected"; ?>>danhhieu</option>
                            <option value="1" <?php if($title_arr['card'][$stt][5] == 1) echo "selected"; ?>>RankTitle1</option>
                            <option value="2" <?php if($title_arr['card'][$stt][5] == 2) echo "selected"; ?>>RankTitle2</option>
                            <option value="3" <?php if($title_arr['card'][$stt][5] == 3) echo "selected"; ?>>RankTitle3</option>
                        </select>
                    </td>
				    <td align="center"><input name="title_arr[card][<?php echo $stt; ?>][1]" value="<?php echo $v[1]; ?>" size="4" /></td>
                    <td align="center"><input name="title_arr[card][<?php echo $stt; ?>][2]" value="<?php echo $v[2]; ?>" /></td>
				  </tr>
                <?php 
                        $stt++;
                    } }
                ?>
                  <tr bgcolor="#FFFFFF">
				    <td align="center"><input name="title_arr[card][<?php echo $stt; ?>][0]" value="" size="8" /></td>
                    <td align="center">
                        <select name="title_arr[card][<?php echo $stt; ?>][5]">
                            <option value="0">danhhieu</option>
                            <option value="1">RankTitle1</option>
                            <option value="2">RankTitle2</option>
                            <option value="3">RankTitle3</option>
                        </select>
                    </td>
				    <td align="center"><input name="title_arr[card][<?php echo $stt; ?>][1]" value="" size="4" /></td>
                    <td align="center"><input name="title_arr[card][<?php echo $stt; ?>][2]" value="" /></td>
				  </tr>				  
				</table>
				
                <table width="100%" border="0" bgcolor="#9999FF" cellpadding="5" >
				  <tr bgcolor="#FFFFFF">
				    <th scope="col" align="center" colspan="4">Danh hiệu Hệ thống VIP loại 2</th>
				  </tr>
                  <tr bgcolor="#FFFFFF">
				    <th scope="col" align="center">Gói VIP</th>
                    <th scope="col" align="center">Data Danh Hiệu</th>
				    <th scope="col" align="center">ID Danh Hiệu</th>
                    <th scope="col" align="center">Tên Danh Hiệu</th>
				  </tr>
                <?php 
                    $stt = 0;
                    if(is_array($title_arr['vip2'])) {
                    foreach($title_arr['vip2'] as $k => $v) { 
                    
                ?>
				  <tr bgcolor="#FFFFFF">
				    <td align="center"><input name="title_arr[vip2][<?php echo $stt; ?>][0]" value="<?php echo $v[0]; ?>" size="8" /></td>
                    <td align="center">
                        <select name="title_arr[vip2][<?php echo $stt; ?>][5]">
                            <option value="0" <?php if($title_arr['vip2'][$stt][5] == 0) echo "selected"; ?>>danhhieu</option>
                            <option value="1" <?php if($title_arr['vip2'][$stt][5] == 1) echo "selected"; ?>>RankTitle1</option>
                            <option value="2" <?php if($title_arr['vip2'][$stt][5] == 2) echo "selected"; ?>>RankTitle2</option>
                            <option value="3" <?php if($title_arr['vip2'][$stt][5] == 3) echo "selected"; ?>>RankTitle3</option>
                        </select>
                    </td>
				    <td align="center"><input name="title_arr[vip2][<?php echo $stt; ?>][1]" value="<?php echo $v[1]; ?>" size="4" /></td>
                    <td align="center"><input name="title_arr[vip2][<?php echo $stt; ?>][2]" value="<?php echo $v[2]; ?>" /></td>
				  </tr>
                <?php 
                        $stt++;
                    } }
                ?>
                  <tr bgcolor="#FFFFFF">
				    <td align="center"><input name="title_arr[vip2][<?php echo $stt; ?>][0]" value="" size="8" /></td>
                    <td align="center">
                        <select name="title_arr[vip2][<?php echo $stt; ?>][5]">
                            <option value="0">danhhieu</option>
                            <option value="1">RankTitle1</option>
                            <option value="2">RankTitle2</option>
                            <option value="3">RankTitle3</option>
                        </select>
                    </td>
				    <td align="center"><input name="title_arr[vip2][<?php echo $stt; ?>][1]" value="" size="4" /></td>
                    <td align="center"><input name="title_arr[vip2][<?php echo $stt; ?>][2]" value="" /></td>
				  </tr>
				</table>
				<center><input type="submit" name="Submit" value="Sửa" <?php if($accept=='0') { ?> disabled="disabled" <?php } ?> /></center>
				</form>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">Thông tin</strong>
			<div class="box">Cấu hình :<br />
			- Tên WebSite<br />
			- Địa chỉ kết nối đến Server</div>
	  </div>
	  
