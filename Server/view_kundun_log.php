<?php

/**
 * @author NetBanBe
 * @copyright 2015
 */

$file_config = 'config/eventgame.txt';

//Đọc File Config
$eventgame_arr = array();
if(is_file($file_config)) {
	$fopen_host = fopen($file_config, "r");
    $eventgame_read = fgets($fopen_host);
    
    $eventgame_arr = json_decode($eventgame_read, true);
} else $fopen_host = fopen($file_config, "w");
fclose($fopen_host);

$kundun_log_arr = $eventgame_arr['kundun'];
if(!is_array($kundun_log_arr)) $kundun_log_arr = array();

foreach($kundun_log_arr as $kundun_log_k => $kundun_log_v) {
    $kundun_dir = $kundun_log_v['dir'];
    $time_uncheck = $kundun_log_v['time_exclude'];
    
    $time_uncheck = str_replace(':', '', $time_uncheck);
    $time_uncheck_explode = explode('-', $time_uncheck);
    
    $kundun[$kundun_log_k]['server'] = $kundun_log_v['server'];
    $kundun[$kundun_log_k]['stat'] = 0;     // 0 : Da chet, 1 : Dang Song
    $kundun[$kundun_log_k]['text'] = '';
    
    $files = glob("$kundun_dir/*.txt");
    if(count($files) > 0) {
        $files = array_combine($files, array_map("filemtime", $files));
        arsort($files);
        
        $latest_file = key($files);
        
        $latest_file_time = filectime($latest_file);
        $fopen_host = fopen($latest_file, "r");
        while (!feof($fopen_host)) {
            $line_content = fgets($fopen_host);
            $line_time = substr($line_content, 0, 5);
            $line_time = str_replace(':', '', $line_time);
            if($line_time < $time_uncheck_explode[0] || $line_time > $time_uncheck_explode[1]) {
                if(strpos($line_content, 'RefillHP') !== false) {
                    $kundun[$kundun_log_k]['stat'] = 1;
                } elseif(strpos($line_content, 'Kundun die') !== false) {
                    $kundun[$kundun_log_k]['stat'] = 0;
                    $time_die = strtotime(date('Y-m-d', $latest_file_time) ." ". substr($line_content, 0, 8));
                    $kundun[$kundun_log_k]['time_die'] = date('d/m H:i', $time_die);
                    $time_next = $time_die + $kundun_log_v['time_next'];
                    $kundun[$kundun_log_k]['next'] = date('d/m H:i', $time_next);
                    $kundun[$kundun_log_k]['date_next'] = date('Y-m-d H:i:s', $time_next);
                }
            }
        }
    } else {
        $kundun[$kundun_log_k]['stat'] = 1;
    }
        
}

$kundun_data = json_encode($kundun);
echo "<nbb>OK<nbb>$kundun_data<nbb>";

?>