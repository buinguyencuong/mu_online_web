<?php
include_once ('config.php');
include_once ('function.php');

// Chinh sua
$date = "2015-12-31";       // Ngay trao giai lo de dang : nam-thang-ngay (2015-12-31 hoac 2016-01-01)
// Chinh sua End

$kqxs_q = "SELECT giai0, giai1, giai21, giai22, giai31, giai32, giai33, giai34, giai35, giai36, giai41, giai42, giai43, giai44, giai51, giai52, giai53, giai54, giai55, giai56, giai61, giai62, giai63, giai71, giai72, giai73, giai74 FROM NBB_KQXS WHERE status=1 AND ngay='$date'";
$kqxs_r = $db->Execute($kqxs_q);
    check_queryerror($kqxs_q, $kqxs_r);
$kqxs_c = $kqxs_r->NumRows();

if($kqxs_c > 0) {
	while($kqxs_kq = $kqxs_r->FetchRow()) {
        
        $time_get_update_q = "UPDATE NBB_KQXS SET time_get = $timestamp WHERE ngay='$date'";
        $time_get_update_r = $db->Execute($time_get_update_q);
            check_queryerror($time_get_update_q, $time_get_update_r);
        
        // Trao giai De
        $de_finish = false;
        $de_q = "SELECT stt, acc, danhso, gcoin_danh, money_type FROM NBB_Relax_De WHERE ngay='$date' AND (status=0 OR status=2)";
        $de_r = $db->Execute($de_q);
            check_queryerror($de_q, $de_r);
        $de_c = $de_r->NumRows();
        if($de_c == 0) {
            $de_finish = true;
        } else {
            include('config/config_relax_de.php');
            while($de_f = $de_r->FetchRow()) {
                $de_stt = $de_f[0];
                $de_acc = $de_f[1];
                $de_danhso = $de_f[2];
                $de_gcoin_danh = $de_f[3];
                $de_money_type = $de_f[4];
                
                if($de_danhso == $kqxs_kq[0]) {
                    $de_gcoin_win = $de_gcoin_danh * $de_win;
                    
                    $de_update_q = "UPDATE NBB_Relax_De SET gcoin_win=$de_gcoin_win, status=1 WHERE stt=$de_stt AND status=0";
                    $de_update_r = $db->Execute($de_update_q);
                        check_queryerror($de_update_q, $de_update_r);
                    $update_de_row = $db->Affected_Rows();
                    if($update_de_row > 0) {
                        if($de_money_type == 2) {
                            $money_name = "Vcent";
                            $money_update_q = "UPDATE MEMB_INFO SET vpoint = vpoint + $de_gcoin_win WHERE memb___id='$de_acc'";
                        } else {
                            $money_name = "Gcent";
                            $money_update_q = "UPDATE MEMB_INFO SET gcoin = gcoin + $de_gcoin_win WHERE memb___id='$de_acc'";
                        }
                        
                        $money_update_r = $db->Execute($money_update_q);
                            check_queryerror($money_update_q, $money_update_r);
                            
                        //Ghi vào Log
                        $log_price = " + $de_gcoin_win $money_name";
                        $log_Des = "Đánh đề con ". $de_danhso ." dùng ". $de_gcoin_danh ." $money_name. Trúng giải nhận : $de_gcoin_win $money_name.";
                        _writelog_tiente($de_acc, $log_price, $log_Des);
                        //End Ghi vào Log
                    }
                } else {
                    $de_update_q = "UPDATE NBB_Relax_De SET status=2 WHERE stt=$de_stt";
                    $de_update_r = $db->Execute($de_update_q);
                        check_queryerror($de_update_q, $de_update_r);
                }
            }
        }
        
        // Trao giai Lo
        $lo_finish = false;
        $lo_q = "SELECT stt, acc, danhso, diem, money_type FROM NBB_Relax_Lo WHERE ngay='$date' AND (status=0 OR status=2)";
        $lo_r = $db->Execute($lo_q);
            check_queryerror($lo_q, $lo_r);
        $lo_c = $lo_r->NumRows();
        if($lo_c == 0) {
            $lo_finish = true;
        } else {
            include('config/config_relax_lo.php');
            while($lo_f = $lo_r->FetchRow()) {
                $lo_stt = $lo_f[0];
                $lo_acc = $lo_f[1];
                $lo_danhso = $lo_f[2];
                $lo_diem = $lo_f[3];
                $lo_money_type = $lo_f[4];
                
                $lo_nhay = 0;
                for($kqxs_i = 0; $kqxs_i <= 26; $kqxs_i++) {
                    if($lo_danhso == $kqxs_kq[$kqxs_i]) {
                        $lo_nhay++;
                    }
                }
                
                if($lo_nhay == 0) {
                    $lo_update_q = "UPDATE NBB_Relax_Lo SET status=2 WHERE stt=$lo_stt";
                    $lo_update_r = $db->Execute($lo_update_q);
                        check_queryerror($lo_update_q, $lo_update_r);
                } else {
                    $lo_gcoin_win = $lo_nhay * $lo_diem * $lo_win;
                    
                    $lo_update_q = "UPDATE NBB_Relax_Lo SET nhay_win = $lo_nhay, gcoin_win=$lo_gcoin_win, status=1 WHERE stt=$lo_stt AND status=0";
                    $lo_update_r = $db->Execute($lo_update_q);
                        check_queryerror($lo_update_q, $lo_update_r);
                    $update_lo_row = $db->Affected_Rows();
                    if($update_lo_row > 0) {
                        if($lo_money_type == 2) {
                            $money_name = "Vcent";
                            $money_update_q = "UPDATE MEMB_INFO SET vpoint = vpoint + $lo_gcoin_win WHERE memb___id='$lo_acc'";
                        } else {
                            $money_name = "Gcent";
                            $money_update_q = "UPDATE MEMB_INFO SET gcoin = gcoin + $lo_gcoin_win WHERE memb___id='$lo_acc'";
                        }
                        
                        $money_update_r = $db->Execute($money_update_q);
                            check_queryerror($money_update_q, $money_update_r);
                            
                        //Ghi vào Log
                        $log_price = " + $lo_gcoin_win $money_name";
                        $log_Des = "Đánh $lo_diem điểm con lô ". $lo_danhso .". Trúng giải nhận : $lo_gcoin_win $money_name.";
                        _writelog_tiente($lo_acc, $log_price, $log_Des);
                        //End Ghi vào Log
                    }
                }
            }
        }
        
        // UP status trao het Lo + De
        if($de_finish == true && $lo_finish == true) {
            $kqxs_update_q = "UPDATE NBB_KQXS SET status=2 WHERE ngay='$date'";
            $kqxs_update_r = $db->Execute($kqxs_update_q);
                check_queryerror($kqxs_update_q, $kqxs_update_r);
        }
	}
}

echo "Trao giai ngay $date hoan tat";
?>