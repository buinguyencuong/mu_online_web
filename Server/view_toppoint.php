<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
	include_once("security.php");
include_once("config.php");
include ('config/config_thehe.php');
include('config/config_ranking.php');

if(!isset($rank_other_slg) || abs(intval($rank_other_slg)) < 20) $rank_other_slg = 20;

$passtransfer = $_POST["passtransfer"];

if ($passtransfer == $transfercode) {
    $toppoint_arr = array();
    for($i=1;$i<count($thehe_choise);$i++) {
        if(strlen($thehe_choise[$i]) > 1) {
            $toppoint_q = "SELECT TOP $rank_other_slg Name, point_total, point_rs, point_rsday, point_event, point_songtu, point_tuluyen FROM nbb_toppoint JOIN MEMB_INFO ON point_total>0 AND nbb_toppoint.acc collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=$i ORDER BY point_total DESC";
            $toppoint_r = $db->Execute($toppoint_q);
                check_queryerror($toppoint_q, $toppoint_r);
            while($toppoint_f = $toppoint_r->FetchRow()) {
                $toppoint_arr[$i][] = array(
                    'name'  => $toppoint_f[0],
                    'total'   =>  $toppoint_f[1],
                    'rs'   =>  $toppoint_f[2],
                    'rsday'   =>  $toppoint_f[3],
                    'event'   =>  $toppoint_f[4],
                    'songtu'   =>  $toppoint_f[5],
                    'tuluyen'   =>  $toppoint_f[6]
                );
            }
        }
    }
    
    $toppoint_data = json_encode($toppoint_arr);
    echo "<info>OK</info><toppoint>$toppoint_data</toppoint>";
}
$db->Close();
?>