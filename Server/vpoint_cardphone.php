<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
	include_once("security.php");
include_once("config.php");
include_once("config/config_napthe.php");
include_once("function.php");

function _cardinfo($login, $msg, $money_get = 0) {
    if($money_get == 1) {
        global $db;
        $gcoin_query = "SELECT gcoin, gcoin_km FROM MEMB_INFO WHERE memb___id='$login'";
        $gcoin_result = $db->Execute($gcoin_query) OR DIE("Query Error : $gcoin_query");
        $gcoin_fetch = $gcoin_result->FetchRow();
        $gcoin = $gcoin_fetch[0];
        $gcoinkm = $gcoin_fetch[1];
        
        $card_total_q = "SELECT SUM(menhgia) FROM CardPhone WHERE acc='$login' AND status=2";
        $card_total_r = $db->Execute($card_total_q);
            check_queryerror($card_total_q, $card_total_r);
        $card_total_f = $card_total_r->FetchRow();
        $card_total = abs(intval($card_total_f[0]));
    } else {
        $gcoin = 0;
        $gcoinkm = 0;
        $card_total = 0;
    }
        
    
    echo "<reponse><msg>$msg</msg><gcoin>$gcoin</gcoin><gcoinkm>$gcoinkm</gcoinkm><card_total>$card_total</card_total></reponse>";
}


$login = $_POST['login'];
$cardtype = $_POST['cardtype'];
$menhgia = $_POST['menhgia'];
$card_num = $_POST['card_num'];
$card_serial = $_POST['card_serial'];
$passtransfer = $_POST["passtransfer"];

$card_num = strtoupper($card_num);
$card_serial = strtoupper($card_serial);
	
if ($passtransfer == $transfercode) {

if( ($cardtype == 'GATE' && $auto_gate === true && $gate_doitac == 'KETNOIPAY') || ($telcard_use === true && ($cardtype == 'MobiPhone' || $cardtype == 'VinaPhone' || $cardtype == 'Viettel') && $telcard_doitac == 'KETNOIPAY') ) {
    $fp_host = fopen("autonap_knp_baotri.txt", "r");
	$baotri_time = fgets($fp_host);
	fclose($fp_host);
    
    $baotri_time = intval($baotri_time);
    
    if( ($timestamp - $baotri_time) < 5*60 ) {
        echo "Hệ thống tự động nạp thẻ đang bảo trì. Vui lòng quay lại sau ít phút.";
        exit();
    }
} elseif( ($cardtype == 'GATE' && $auto_gate === true && $gate_doitac == 'VIPPAY') || ($telcard_use === true && ($cardtype == 'MobiPhone' || $cardtype == 'VinaPhone' || $cardtype == 'Viettel') && $telcard_doitac == 'VIPPAY') ) {
    $fp_host = fopen("autonap_vippay_baotri.txt", "r");
	$baotri_time = fgets($fp_host);
	fclose($fp_host);
    
    $baotri_time = intval($baotri_time);
    
    if( ($timestamp - $baotri_time) < 5*60 ) {
        echo "Hệ thống tự động nạp thẻ đang bảo trì. Vui lòng quay lại sau ít phút.";
        exit();
    }
} elseif($cardtype == 'GATE' && $auto_gate === true && $gate_doitac == 'GATE') {
    $fp_host = fopen("autonap_gate_baotri.txt", "r");
	$baotri_time = fgets($fp_host);
	fclose($fp_host);
    
    $baotri_time = intval($baotri_time);
    
    if( ($timestamp - $baotri_time) < 5*60 ) {
        echo "Hệ thống tự động nạp thẻ GATE đang bảo trì. Vui lòng quay lại sau ít phút.";
        exit();
    }
}

$string_login = $_POST['string_login'];
checklogin($login,$string_login);

$card_num = str_replace(" ", "", $card_num);            
    $card_num_md5 = md5($card_num);
    $card_num_encode = $card_num;
$card_serial = str_replace(" ", "", $card_serial);      $card_serial_encode = $card_serial;


$num_length = strlen($card_num);
$serial_length = strlen($card_serial);

$time_check = date("Y-m-d",$timestamp);

if ($cardtype == 'VinaPhone') { 
	kiemtra_kituso($card_num);
	if(!($num_length == 14 || $num_length == 12)) {
		$msg = "Thẻ sai. Mã thẻ phải có 12 hoặc 14 số."; 
        _cardinfo($login, $msg);
        exit(); 
    }
}

elseif ($cardtype == 'Viettel') { 
	kiemtra_kituso($card_num);
	kiemtra_kituso($card_serial);
	if($num_length != 13) {
		$msg = "Thẻ sai. Mã thẻ phải có 13 số."; 
        _cardinfo($login, $msg);
        exit(); 
    }
    if($serial_length != 11) {
		$msg = "Serial sai. Serial phải có 11 số."; 
        _cardinfo($login, $msg);
        exit(); 
    }
}

elseif ($cardtype == 'MobiPhone') { 
	kiemtra_kituso($card_num);
	kiemtra_kituso($card_serial);
	if( !($num_length == 14 || $num_length == 12) ) {
        $msg = "Thẻ sai. Mã thẻ phải có 12 hoặc 14 số."; 
        _cardinfo($login, $msg);
        exit(); 
	}
}

elseif ($cardtype == 'GATE') { 
	if( $num_length == 10 && $serial_length == 10 ) { 
	   
	}
	else	{ 
	   $msg = "Thẻ sai. Xin vui lòng kiểm tra kĩ lại thông tin thẻ."; 
       _cardinfo($login, $msg);
       exit(); 
    }
}
$card_num = nbb_encode($card_num);

$sql_char_check = $db->SelectLimit("Select Name,Resets,Relifes From Character where AccountID='$login' ORDER BY Relifes DESC, Resets DESC", 1, 0);
$char_check = $sql_char_check->fetchrow();

$slg_card_check = $db->Execute("Select * From CardPhone where acc='$login' and ngay='$time_check' AND (status=0 OR status IS NULL OR status=3)");
$slg_card_check = $slg_card_check->numrows();

if($cardtype == 'GATE') {
    $sql_card_wait = $db->Execute("Select * From CardPhone where (status=0 OR status IS NULL) AND card_serial='$card_serial'");
    $card_wait = $sql_card_wait->numrows();
    
    $sql_card_right = $db->Execute("Select * From CardPhone where status=2 AND card_serial='$card_serial'");
    $card_right = $sql_card_right->numrows();
} else {
    $sql_card_wait = $db->Execute("Select * From CardPhone where (status=0 OR status IS NULL) AND card_num_md5='$card_num_md5'");
    $card_wait = $sql_card_wait->NumRows();
    
    $sql_card_right = $db->Execute("Select * From CardPhone where status=2 AND card_num_md5='$card_num_md5'");
    $card_right = $sql_card_right->NumRows();
}

$card_check_query = "Select * From CardPhone where card_num_md5='$card_num_md5' AND card_serial='$card_serial' AND status <> 9";
$card_check_result = $db->execute($card_check_query);
    check_queryerror($card_check_query, $card_check_result);

$card_check = $card_check_result->NumRows();

$name = $char_check[0];
if ($char_check[1] <= $card_reset){ 
   $msg = "Nhân vật Reset ít hơn $card_reset lan."; 
   _cardinfo($login, $msg);
   exit();
}

if ($card_check > 2){ 
   $msg = "Thẻ trùng với thẻ đã nạp."; 
   _cardinfo($login, $msg);
   exit();
}
else if($card_wait > 0)
{
    $msg = "Thẻ đã nạp từ trước. Đang chờ duyệt. Nếu nạp nhầm mã thẻ, đợi duyệt sai sẽ được phép nạp lại."; 
    _cardinfo($login, $msg);
    exit();
}
else if($card_right > 0)
{
    $msg = "Thẻ đã nạp từ trước và được duyệt Đúng. Đề nghị không nạp lại."; 
    _cardinfo($login, $msg);
    exit();
}
	
if($char_check[2] == 0 && $char_check[1] < $reset_4) {
	if($char_check[1] < $reset_1 && $slg_card_check >= $slg_card_1) { 
		$msg = "Bạn đã nạp $slg_card_check thẻ trong hôm nay .<br>Nhân vật cấp cao nhất trong tài khoản của bạn: $name (ReLife: 0 - Reset: $char_check[1]) . Với cấp độ đó bạn chỉ được nạp tối đa $slg_card_1 thẻ/ngày."; 
        _cardinfo($login, $msg);
        exit();
	}
	elseif($char_check[1] >= $reset_1 && $char_check[1] < $reset_2 && $slg_card_check >= $slg_card_2) { 
		$msg = "Bạn đã nạp $slg_card_check thẻ trong hôm nay .<br>Nhân vật cấp cao nhất trong tài khoản của bạn: $name (ReLife: 0 - Reset: $char_check[1]) . Với cấp độ đó bạn chỉ được nạp tối đa $slg_card_2 thẻ/ngày."; 
        _cardinfo($login, $msg);
        exit();
	}
	elseif($char_check[1] >= $reset_2 && $char_check[1] < $reset_3 && $slg_card_check >= $slg_card_3) { 
		$msg = "Bạn đã nạp $slg_card_check thẻ trong hôm nay .<br>Nhân vật cấp cao nhất trong tài khoản của bạn: $name (ReLife: 0 - Reset: $char_check[1]) . Với cấp độ đó bạn chỉ được nạp tối đa $slg_card_3 thẻ/ngày."; 
        _cardinfo($login, $msg);
        exit();
	}
	elseif($char_check[1] >= $reset_3 && $char_check[1] < $reset_4 && $slg_card_check >= $slg_card_4) { 
		$msg = "Bạn đã nạp $slg_card_check thẻ trong hôm nay .<br>Nhân vật cấp cao nhất trong tài khoản của bạn: $name (ReLife: 0 - Reset: $char_check[1]) . Với cấp độ đó bạn chỉ được nạp tối đa $slg_card_4 thẻ/ngày."; 
        _cardinfo($login, $msg);
        exit();
	}
} else { 
    if($slg_card_check >= $slg_card_max) {
        $msg = "Bạn chỉ được nạp tối đa $slg_card_max thẻ/ngày."; 
        _cardinfo($login, $msg);
        exit();
    }
}

$msquery = "INSERT INTO CardPhone (acc, name, card_type, menhgia, card_num, card_num_md5, card_serial, ngay, timenap) VALUES ('$login', '$name', '$cardtype', '$menhgia', '$card_num', '$card_num_md5', '$card_serial', '".date("Y-m-d",$timestamp)."', '$timestamp')";
$msresults = $db->Execute($msquery);

    include_once('config_autonap.php');
    include_once('autonap_func.php');
    $stt_query = "SELECT stt FROM CardPhone WHERE card_type='$cardtype' AND card_num='$card_num' AND card_serial='$card_serial' AND timenap='$timestamp'";
    $stt_result = $db->execute($stt_query) OR DIE("Query Error : $stt_query");
    $stt_fetch = $stt_result->fetchrow();
    $stt = $stt_fetch[0];
    
    // Nap the GATE
    if($cardtype == 'GATE' && $auto_gate === true) {
        if($gate_doitac == 'GATE') {
            include_once('autonap_gate.php');
            $msg = $notice_nap;
            _cardinfo($login, $msg, 1);
        }
        else if($gate_doitac == 'KETNOIPAY') {
            include_once('autonap_ketnoipay.php');
            $msg = $notice_nap;
            _cardinfo($login, $msg, 1);
        }
        else if($gate_doitac == 'VIPPAY') {
            include_once('autonap_vippay.php');
            $msg = $notice_nap;
            _cardinfo($login, $msg, 1);
        }
    } 
    // Nap the Dien thoai
    else if( $telcard_use === true && ($cardtype == 'MobiPhone' || $cardtype == 'VinaPhone' || $cardtype == 'Viettel') ) {
        if($telcard_doitac == 'BAOKIM') {
            include_once('autonap_baokim.php');
            $msg = $notice_nap;
            _cardinfo($login, $msg, 1);
        }
        else if($telcard_doitac == 'KETNOIPAY') {
            include_once('autonap_ketnoipay.php');
            $msg = $notice_nap;
            _cardinfo($login, $msg, 1);
        }
        else if($telcard_doitac == 'VIPPAY') {
            include_once('autonap_vippay.php');
            $msg = $notice_nap;
            _cardinfo($login, $msg, 1);
        }
    }
    // Không dùng Auto
    else {
        $msg = "Đăng kí mua V.Point bằng thẻ <strong>$cardtype</strong> cho tài khoản <strong>$login</strong> thành công. Hãy theo dõi trong phần danh sách thẻ đã nạp.";
        _cardinfo($login, $msg);
    }
}
$db->Close();
?>