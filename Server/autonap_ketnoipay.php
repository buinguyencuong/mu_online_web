<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/

/**
 * @author NetBanBe
 * @copyright 2012
 */

switch ($cardtype){ 
    case 'MobiPhone':    // Mobifone    
        $TxtType = 'VMS';
        $TxtUrl  = 'http://api.knp.vn:64980';
    break;
    
    case 'VinaPhone':    // Vinaphone
        $TxtType = 'VNP';
        $TxtUrl  = 'http://api.knp.vn:64980';
    break;
    
    case 'Viettel':    // Viettel
        $TxtType = 'VTT';
        $TxtUrl  = 'http://api.knp.vn:64990';
    break;
    
    case 'GATE':    // Viettel
        $TxtType = 'GATE';
        $TxtUrl  = 'http://api.knp.vn:64986';
    break;
    
    case 'VTC':    // Viettel
        $TxtType = 'VTC';
        $TxtUrl  = 'http://api.knp.vn:64987';
    break;
}

# Gửi thẻ lên máy chủ FPAY
$TxtTransId = $ketnoipay_resend . $stt;
$TxtKey   = md5(trim($ketnoipay_partnerid.$TxtType.$TxtTransId.$card_num_encode.$ketnoipay_signal));

if(isset($trunggian_use) && $trunggian_use === true) {
    include_once('config_license.php');
    include_once('func_getContent.php');
    
    $getcontent_url = $trunggian_url;
    
    $getcontent_data = array(
        'ketnoipay_partnerid'    =>  $ketnoipay_partnerid,
        'TxtType'  =>  $TxtType,
        'card_num'  =>  $card_num_encode,
        'card_serial'    =>  $card_serial_encode,
        'TxtTransId'    =>  $TxtTransId,
        'TxtKey'    =>  $TxtKey,
        'TxtUrl'         =>  $TxtUrl
    );
    
    $TxtUrl .= " (Trung gian)";
    
    $response = _sendcard($getcontent_url, $getcontent_data);
    
} else {
    
    include('autonap_ketnoipay_api.php');
    $gateWay  = new gateWay($ketnoipay_partnerid,$TxtType,$card_num_encode,$card_serial_encode,$TxtTransId,$TxtKey,$TxtUrl);
    $response = $gateWay->ReturnResult();
}


if(strpos($response,'RESULT:10') !== false) // thẻ đúng
{
	$edit_menhgia = str_replace('RESULT:10@','',$response);
    $edit_menhgia = abs(intval($edit_menhgia));
    $up_stat = 2;

    $logcontent_after = "Thẻ đúng. Mệnh giá: $edit_menhgia VNĐ.";
    include('autonap_duyet.php');
} 
elseif(strpos($response,'RESULT:03') !== false || strpos($response,'RESULT:06') !== false || strpos($response,'RESULT:07') !== false || strpos($response,'RESULT:12') !== false)
{
    $error_code = "";
    $notice_nap = "Mã thẻ cào hoặc seri không chính xác. <br />Vui lòng kiểm tra lại cẩn thận.";
    $up_stat = 3;
    
    $logcontent_after = $notice_nap;
    include('autonap_duyet.php');
	
}
elseif(strpos($response,'RESULT:05') !== false || strpos($response,'RESULT:08') !== false)
{
    $error_code = "";
    $notice_nap = "Thẻ đã sử dụng hoặc thẻ chưa kích hoạt.";
    $up_stat = 3;
    
    $logcontent_after = $notice_nap;
    include('autonap_duyet.php');
	
}
elseif(strpos($response,'RESULT:00') !== false || strpos($response,'RESULT:99') !== false || strpos($response,'RESULT:01') !== false || strpos($response,'RESULT:04') !== false || strpos($response,'RESULT:09') !== false)
{
    $error_code = "";
    $notice_nap = "Hệ thống kiểm tra thẻ nạp đang bảo trì.<br />Vui lòng gửi lại sau.";
    $up_stat = 9;
    
    $fp = fopen("autonap_knp_baotri.txt", "w");  
	fputs ($fp, $timestamp);
	fclose($fp);
    
    $logcontent_after = "KNP Bảo trì.";
    include('autonap_duyet.php');
    
}
elseif(strpos($response,'RESULT:11') !== false)
{
    $error_code = "";
    $notice_nap = "Hệ thống duyệt thẻ gián đoạn.<br />Thẻ sẽ được duyệt ngay khi kết nối lại.<br />Vui lòng chờ trong ít phút.";
    
    $logcontent_after = "KNP Trễ.";
    
    $fp = fopen("autonap_knp_baotri.txt", "w");  
	fputs ($fp, $timestamp);
	fclose($fp);
} else {
	$error_code = "";
    $notice_nap = "Hệ thống kiểm tra thẻ nạp đang bảo trì.<br />Vui lòng gửi lại sau.";
    $up_stat = 9;
    
    $fp = fopen("autonap_knp_baotri.txt", "w");  
	fputs ($fp, $timestamp);
	fclose($fp);
    
    $logcontent_after = "KNP không kết nối được.";
    include('autonap_duyet.php');
}

// Write Log Nap KetNoiPay
    $logcontent = "Partnerid: $ketnoipay_partnerid. Signal: $ketnoipay_signal. Mã thẻ: $card_num_encode, Seri thẻ: $card_serial_encode. URL nạp: $TxtUrl. Reponse: $response";
    $logcontent .= " | ". $logcontent_after;
    _writelog("log_auto_ketnoipay.txt", $logcontent);
// End Write Log Nap Bao Kim
?>