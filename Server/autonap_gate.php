<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/

/**
 * @author NetBanBe
 * @copyright 2012
 */

    $url = 'https://pay.gate.vn/NoAuth/paymentexpress/Account/Default.aspx';
	$userAgent = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36';
	$postdata = array(
        '__EVENTTARGET'  =>  '',
        '__EVENTARGUMENT'  =>  '',
        '__LASTFOCUS'  =>  '',
        '__VIEWSTATE'  =>  urlencode('/wEPDwUKMTQ4MDc3MjYyOA9kFgJmD2QWAgIDD2QWAgILD2QWBGYPFgIeC18hSXRlbUNvdW50AgEWAmYPZBYCZg8VAxtodHRwczovL2RhaWx5LmdhdGUudm4vaG9tZS8QTXVhIC0gQsOhbiB0aOG6uxtMMGdzbjlBM2hRRmtBRndEMjk4eDE5Ny5naWZkAgEPFgIfAAIBFgJmD2QWAmYPFQMbaHR0cHM6Ly9kYWlseS5nYXRlLnZuL2hvbWUvEE11YSAtIELDoW4gdGjhursbTDBnc245QTNoUUZrQUZ3RDI5OHgxOTcuZ2lmZGSI8EvZr60EW3hquR1LiIA21jOnjw=='),
        'ctl00%24PSPContent%24ddlService' => 'GATE',
        'ctl00%24PSPContent%24txtGateName'  =>  $accgate,
        'ctl00%24PSPContent%24txtConfirmGateName' => $accgate,
        'ctl00%24PSPContent%24txtSerial'  =>  $card_serial_encode,
        'ctl00%24PSPContent%24txtPin'  =>  $card_num_encode,
        'ctl00%24PSPContent%24btnOK'  =>  urlencode('Nạp thẻ'),
        '__EVENTVALIDATION'  =>  urlencode('/wEWCwK54tFnAun9uiUC49SUvgwCspix/g0CofTbywkC+6OgnA4Clpja0QMCmrDn+w0CiMPdqw0CkZ3A+wUCiZDUjwOGVtwwjmiBvIu5ISDzTb3jZukq6g==')
    );
	$response = _sendcard($url, $postdata, $userAgent );
	
	preg_match('#<span id="PSPContent_lbMsg" style="color:Green;">Đã nạp thành công với mệnh giá (.*)</span>#',$response,$bacgate);
    //preg_match('#<span id="PSPContent_lbMsg">(.*)</span>#',$response,$response_notice);
    preg_match('#<span id="PSPContent_lbMsg">(.*)</span>#',$response,$response_error);
    
    // Write Log Nap GATE
    $logcontent = "Tài khoản GATE: $accgate. Mã thẻ: $card_num_encode, Seri thẻ: $card_serial_encode. URL nạp: $url. Thông tin trả về: $response_notice[1]. Thông tin lỗi: $response_error[1]";
    
    _writelog("log_autogate.txt", $logcontent);
    // End Write Log Nap GATE
    
    $edit_menhgia = abs(intval(str_replace(',','',$bacgate[1])));
	if($edit_menhgia > 0 || strlen($response_error[1]) > 0) {
        if($edit_menhgia > 0) {
            $up_stat = 2;
            $edit_menhgia = str_replace(',','',$bacgate[1]);
    	}
    	else {
    		$up_stat = 3;
            if(substr_count($response_error[1], "giao dịch nạp thẻ trong 5 phút")) {
            	$notice_nap = "Hệ thống nạp thẻ gián đoạn. Vui lòng chờ 5 phút sau nạp lại.";
            } else {
            	$notice_nap .= $response_error[1];
            }
    	}
        include('autonap_duyet.php');
	} else {
        $notice_nap = "Hệ thống kiểm tra thẻ nạp đang bảo trì.<br />Vui lòng gửi lại sau.";
        $up_stat = 9;
        
        $fp = fopen("autonap_gate_baotri.txt", "w");  
    	fputs ($fp, $timestamp);
    	fclose($fp);
        
        $logcontent_after = "Gate Bảo trì. $notice";
        include('autonap_duyet.php');
	}

?>