<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
include_once('sv_sms/sms_function.php');
include_once('config.php');
include_once("func_timechenh.php");

// Lấy nội dung tin nhắn
$phone = $_REQUEST['phone'];  // Cước SMS
$message = $_REQUEST['message']; // Nội dung tin

//MESS chuẩn
$message_true = $message;
$tmp_true=explode(" ",$message_true);//cắt nội dung tin

//MESS Upper
$message = strtoupper($message);
$tmp=explode(" ",$message);//cắt nội dung tin

//Lấy danh sách mật khẩu ngẫu nhiên
if ($tmp[0] == 'PR')
{
	$taikhoan = $tmp_true[1];
	if(check_taikhoan($taikhoan) == 0) { $content = "Tai khoan $taikhoan khong ton tai."; }
	else if( check_phone($phone,$taikhoan) <= 0 )
	{
		$content = "So Dien Thoai '$phone' khong phai cua Tai khoan : $taikhoan.";
	} else include_once('sv_sms/sms_passran.php');
}
//Xác minh tài khoản
else if ($tmp[0] == 'XM')
{
	$taikhoan = $tmp_true[1];
	if( check_taikhoan($taikhoan) == 0 ) { $content = "Tai khoan $taikhoan khong ton tai."; }
	else include_once('sv_sms/sms_verify.php');
}
//GiftCode Tan Thu
else if ($tmp[0] == 'TANTHU')
{
	$taikhoan = $tmp_true[1];
	if( check_taikhoan($taikhoan) == 0 ) { $content = "Tai khoan $taikhoan khong ton tai."; }
	else if( check_phone($phone,$taikhoan) <= 0 )
	{
		$content = "So Dien Thoai '$phone' khong phai cua Tai khoan : $taikhoan.";
	} else include_once('sv_sms/sms_giftcode_tanthu.php');
}
//GiftCode Tan Thu 2
else if ($tmp[0] == 'TANTHU2')
{
	$taikhoan = $tmp_true[1];
	if( check_taikhoan($taikhoan) == 0 ) { $content = "Tai khoan $taikhoan khong ton tai."; }
	else if( check_phone($phone,$taikhoan) <= 0 )
	{
		$content = "So Dien Thoai '$phone' khong phai cua Tai khoan : $taikhoan.";
	} else include_once('sv_sms/sms_giftcode_tanthu2.php');
}
//One Pass Day
else if ($tmp[0] == 'OPD')
{
	$taikhoan = $tmp_true[1];
	if( check_taikhoan($taikhoan) == 0 ) { $content = "Tai khoan $taikhoan khong ton tai."; }
	else if( check_phone($phone,$taikhoan) <= 0 )
	{
		$content = "So Dien Thoai '$phone' khong phai cua Tai khoan : $taikhoan.";
	} else include_once('sv_sms/sms_opd.php');
}
//Xử lý tin nhắn theo mã
else {
	$code = $tmp[0];
	if (!preg_match("/^[0-9]*$/i", $code))
	{
    		$content = "Ma xac thuc sai. Vui long kiem tra lai tin nhan.";
	}
	else 
	{
		$loaddata_query = "SELECT * FROM SMS WHERE Code='$code'";
		$sql_loaddata = $db->Execute($loaddata_query);
		$check_data = $sql_loaddata->numrows();
		$loaddata = $sql_loaddata->fetchrow();
			$taikhoan = $loaddata[0];
			$KeyXuLy = $loaddata[1];
			$time_load = $loaddata[2];
			$status = $loaddata[3];
            $dulieu1 = $loaddata[5];
		if($check_data < 1) 
		{ 
			$content = "Ma xac thuc '$code' khong ton tai hoac sai. Vui long kiem tra lai tin nhan."; 
		}
		else if( $timestamp > ($time_load + 3600) ) 
		{ 
			$content = "Thoi gian xac thuc vuot qua thoi gian cho phep. Yeu cau da bi huy."; 	
		}
		else if( $status == 1 ) 
		{ 
			$content = "Yeu cau da duoc thuc hien roi."; 
		}
		else if( check_phone($phone,$taikhoan) <= 0 )
		{
			$content = "So Dien Thoai '$phone' khong phai cua Tai khoan : $taikhoan.";
		}
		else {
			switch ($KeyXuLy) {
				
				case 'RECEIVEPASS1':
					include_once('sv_sms/sms_receivepass1.php');
					break;
					
				case 'CHANGEPASS1':
					include_once('sv_sms/sms_changepass1.php');
					break;
					
				case 'CHANGEPASS2':
					include_once('sv_sms/sms_changepass2.php');
					break;
					
				case 'CHANGEPASSGAME':
					include_once('sv_sms/sms_changepassgame.php');
					break;
					
				case 'CHANGEQUEST':
					include_once('sv_sms/sms_changequest.php');
					break;
					
				case 'CHANGEANS':
					include_once('sv_sms/sms_changeans.php');
					break;
				
                case 'CHANGESNONUMB':
					include_once('sv_sms/sms_changesnonumb.php');
					break;
					
				case 'CHANGEEMAIL':
					include_once('sv_sms/sms_changeemail.php');
					break;
					
				case 'CHANGESDT':
					include_once('sv_sms/sms_changesdt.php');
					break;

				case 'PASSRAN':
					include_once('sv_sms/sms_passran_onoff.php');
					break;
				case 'GIFTCODE_WEEK':
					include_once('sv_sms/sms_giftcode_week.php');
					break;
				case 'GIFTCODE_MONTH':
					include_once('sv_sms/sms_giftcode_month.php');
					break;
				case 'GIFTCODE_RS':
					include_once('sv_sms/sms_giftcode_rs.php');
					break;
						
				default: $content = "Du lieu khong dung. Xin vui long kiem tra lai noi dung Tin nhan hoac lien he BQT de tro giup.";
			}
		}
	}
}
	echo "<nbb>0K<nbb>$content<nbb>";
$db->Close();
?>