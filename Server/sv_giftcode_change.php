<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
	include_once("security.php");
include_once("config.php");

include_once("function.php");

$login=$_POST["login"];
$name=$_POST["name"];
$gift_code = $_POST['giftcode'];    $gift_code = strtoupper($gift_code);
$pass2 = $_POST['pass2'];

$passtransfer = $_POST["passtransfer"];

if ($passtransfer == $transfercode) {

$string_login = $_POST['string_login'];
checklogin($login,$string_login);
	
if(check_nv($login, $name) == 0) {
    echo "Nhân vật <b>{$name}</b> không nằm trong tài khoản <b>{$login}</b>. Vui lòng kiểm tra lại."; exit();
}

kiemtra_pass2($login,$pass2);
kiemtra_online($login);

if(check_tk_nv($login, $name) == 0) {
    echo "Tài khoản $login không có nhân vật $name. Vui lòng kiểm tra lại."; exit();
}

$giftcode_len = strlen($gift_code);
if($giftcode_len == 6) {    // Giftcode 1ForALL
    // Kiem tra GiftCode
    $giftcode_check_query = "SELECT TOP 1 gift_code FROM GiftCode WHERE acc='$login' AND gift_code='$gift_code' AND type=6";
    $giftcode_check_result = $db->Execute($giftcode_check_query);
    $giftcode_check = $giftcode_check_result->NumRows();
    if($giftcode_check > 0) {
        echo "Tài khoản $login đã sử dụng GiftCode $gift_code , không thể sử dụng lại."; exit();
    }
    
    $file_giftcode_all = 'config/giftcode_all.txt';
    $file_giftcode = 'config/giftcode_random.txt';

    //Đọc File GiftCode ALL
    $giftcode_all_arr = array();
    if(is_file($file_giftcode_all)) {
		$fopen_host = fopen($file_giftcode_all, "r");
        $giftcode_all_read = fgets($fopen_host);
        
        $giftcode_all_arr = json_decode($giftcode_all_read, true);
	} else $fopen_host = fopen($file_giftcode_all, "w");
	fclose($fopen_host);
    
    if(!is_array($giftcode_all_arr)) $giftcode_all_arr = array();
    if(!isset($giftcode_all_arr[$gift_code])) {
        echo "GIFTCODE không tồn tại hoặc nhập sai."; exit();
    }
    
    //Đọc File GiftCode
    $giftcode_arr = array();
	if(is_file($file_giftcode)) {
		$fopen_host = fopen($file_giftcode, "r");
        $giftcode_read = fgets($fopen_host);
        
        $giftcode_arr = json_decode($giftcode_read, true);
        fclose($fopen_host);
        if(!is_array($giftcode_arr)) $giftcode_arr = array();
    }
    
    $gift_type = $giftcode_all_arr[$gift_code];
    $gift_name = $giftcode_arr[$gift_type]['name'];
    $item_read = $giftcode_arr[$gift_type]['gift'];
        
} else {
    // Kiem tra GiftCode
    $giftcode_check_query = "SELECT TOP 1 status, name, type, acc, giftcode_type FROM GiftCode WHERE gift_code='$gift_code' ORDER BY status";
    $giftcode_check_result = $db->Execute($giftcode_check_query);
    $giftcode_check = $giftcode_check_result->NumRows();
    if($giftcode_check == 0) {
        echo "Tài khoản $login không tồn tại GiftCode $gift_code , vui lòng kiểm tra lại."; exit();
    } else {
        $giftcode__check_fetch = $giftcode_check_result->FetchRow();
        $giftcode_status = $giftcode__check_fetch[0];
        $giftcode_name = $giftcode__check_fetch[1];
        $giftcode_type = $giftcode__check_fetch[2];
        $giftcode_acc = $giftcode__check_fetch[3];
        $giftcode_gifttype = $giftcode__check_fetch[4];
        
        if($giftcode_status == 0) {
            echo "Mã GiftCode ". $gift_code ." chưa được kích hoạt."; exit();
        }
        else if($giftcode_status == 2) {
            echo "Tài khoản $giftcode_acc đã sử dụng Giftcode $gift_code cho nhân vật ". $giftcode_name ." . Không thể sử dụng lại."; exit();
        }
    }
    
    switch ($giftcode_type) {
    	// GiftCode Reset
        case 1:
            if($giftcode_name != $name) {
                echo "GiftCode $gift_code không phải của nhân vật $name . Không thể nhận thưởng."; exit();
            }
            include_once("config/config_giftcode_rs.php");
            $file_giftcode = 'config/giftcode_random.txt';
    
            //Đọc File GiftCode
            $giftcode_arr = array();
        	if(is_file($file_giftcode)) {
        		$fopen_host = fopen($file_giftcode, "r");
                $giftcode_read = fgets($fopen_host);
                
                $giftcode_arr = json_decode($giftcode_read, true);
                fclose($fopen_host);
                if(!is_array($giftcode_arr)) $giftcode_arr = array();
            }
            
            $gift_name = $giftcode_arr[$giftcode_rs_use]['name'];
            $item_read = $giftcode_arr[$giftcode_rs_use]['gift'];
    	break;
        
        // GiftCode Week
    	case 2:
            if($giftcode_acc != $login) {
                echo "Giftcode $giftcode không phải của tài khoản $login. Không thể nhận thưởng"; exit();
            }
            include_once("config/config_giftcode_week.php");
            $file_giftcode = 'config/giftcode_random.txt';
    
            //Đọc File GiftCode
            $giftcode_arr = array();
        	if(is_file($file_giftcode)) {
        		$fopen_host = fopen($file_giftcode, "r");
                $giftcode_read = fgets($fopen_host);
                
                $giftcode_arr = json_decode($giftcode_read, true);
                fclose($fopen_host);
                if(!is_array($giftcode_arr)) $giftcode_arr = array();
            }
            
            $gift_name = $giftcode_arr[$giftcode_week_use]['name'];
            $item_read = $giftcode_arr[$giftcode_week_use]['gift'];
    	break;
    
    	// GiftCode Month
        case 3:
            if($giftcode_acc != $login) {
                echo "Giftcode $giftcode không phải của tài khoản $login. Không thể nhận thưởng"; exit();
            }
            include_once("config/config_giftcode_month.php");
            $file_giftcode = 'config/giftcode_random.txt';
    
            //Đọc File GiftCode
            $giftcode_arr = array();
        	if(is_file($file_giftcode)) {
        		$fopen_host = fopen($file_giftcode, "r");
                $giftcode_read = fgets($fopen_host);
                
                $giftcode_arr = json_decode($giftcode_read, true);
                fclose($fopen_host);
                if(!is_array($giftcode_arr)) $giftcode_arr = array();
            }
            
            $gift_name = $giftcode_arr[$giftcode_month_use]['name'];
            $item_read = $giftcode_arr[$giftcode_month_use]['gift'];
    	break;
        
        // GiftCode Acc
        case 4:
            if($giftcode_acc != $login) {
                echo "Giftcode $giftcode không phải của tài khoản $login. Không thể nhận thưởng"; exit();
            }
            $file_giftcode_type = 'config/giftcode_type.txt';
            $file_giftcode = 'config/giftcode_random.txt';
    
            //Đọc File GiftCode Type
            $giftcode_type_arr = array();
            if(is_file($file_giftcode_type)) {
        		$fopen_host = fopen($file_giftcode_type, "r");
                $giftcode_type_read = fgets($fopen_host);
                
                $giftcode_type_arr = json_decode($giftcode_type_read, true);
        	} else $fopen_host = fopen($file_giftcode_type, "w");
        	fclose($fopen_host);
            
            if(!is_array($giftcode_type_arr)) $giftcode_type_arr = array();
            
            //Đọc File GiftCode
            $giftcode_arr = array();
        	if(is_file($file_giftcode)) {
        		$fopen_host = fopen($file_giftcode, "r");
                $giftcode_read = fgets($fopen_host);
                
                $giftcode_arr = json_decode($giftcode_read, true);
                fclose($fopen_host);
                if(!is_array($giftcode_arr)) $giftcode_arr = array();
            }
            
            $gift_type = $giftcode_type_arr[$giftcode_gifttype]['gift_type'];
            
            $gift_name = $giftcode_arr[$gift_type]['name'];
            $item_read = $giftcode_arr[$gift_type]['gift'];
    	break;
        
        // GiftCode Phát
        case 5:
            $file_giftcode_type = 'config/giftcode_type.txt';
            $file_giftcode = 'config/giftcode_random.txt';
    
            //Đọc File GiftCode Type
            $giftcode_type_arr = array();
            if(is_file($file_giftcode_type)) {
        		$fopen_host = fopen($file_giftcode_type, "r");
                $giftcode_type_read = fgets($fopen_host);
                
                $giftcode_type_arr = json_decode($giftcode_type_read, true);
        	} else $fopen_host = fopen($file_giftcode_type, "w");
        	fclose($fopen_host);
            
            if(!is_array($giftcode_type_arr)) $giftcode_type_arr = array();
            
            //Đọc File GiftCode
            $giftcode_arr = array();
        	if(is_file($file_giftcode)) {
        		$fopen_host = fopen($file_giftcode, "r");
                $giftcode_read = fgets($fopen_host);
                
                $giftcode_arr = json_decode($giftcode_read, true);
                fclose($fopen_host);
                if(!is_array($giftcode_arr)) $giftcode_arr = array();
            }
            
            $danhanh_check_q = "SELECT count(acc) FROM GiftCode WHERE acc='$login' AND type=5 AND giftcode_type=$giftcode_gifttype AND status=2";
            $danhanh_check_r = $db->Execute($danhanh_check_q);
                check_queryerror($danhanh_check_q, $danhanh_check_r);
            $danhanh_check_f = $danhanh_check_r->FetchRow();
            if($danhanh_check_f[0] > 0) {
                $gift_name = $giftcode_type_arr[$giftcode_gifttype]['name'];
                echo "Tài khoản $login đã nhân $gift_name rồi. Giftcode này chỉ được nhận 1 lần duy nhất."; exit();
            }
            
            $gift_type = $giftcode_type_arr[$giftcode_gifttype]['gift_type'];
            
            $gift_name = $giftcode_arr[$gift_type]['name'];
            $item_read = $giftcode_arr[$gift_type]['gift'];
    	break;
        
        // GiftCode 1 For ALL
        //case 6:
        
        // GiftCode Tan thu 2
        case 8:
            if($giftcode_acc != $login) {
                echo "Giftcode $giftcode không phải của tài khoản $login. Không thể nhận thưởng"; exit();
            }
            include_once("config/config_giftcode_tanthu2.php");
            $class_check_query = "SELECT Class FROM Character WHERE Name='$name'";
            $class_check_result = $db->Execute($class_check_query);
                check_queryerror($class_check_query, $class_check_result);
            $class_check = $class_check_result->FetchRow();
            
            $ClassType =  $class_check[0];
            switch ($ClassType){ 
            	case 0:
                case 1:
                case 2:
                case 3:
                    $Class_name = "DW";
                    $GiftCode = $gift_dw;
                    $msg_giftcode = $msg_dw;
                    
                    $GiftCode_time = $gift_time_dw;
                    $msg_giftcode_time = $msg_time_dw;
                    $day_giftcode_time = $day_time_dw;
            	break;
            
            	case 16:
                case 17:
                case 18:
                case 19:
                    $Class_name = "DK";
                    $GiftCode = $gift_dk;
                    $msg_giftcode = $msg_dk;
                    
                    $GiftCode_time = $gift_time_dk;
                    $msg_giftcode_time = $msg_time_dk;
                    $day_giftcode_time = $day_time_dk;
            	break;
            
            	case 32:
                case 33:
                case 34:
                case 35:
                    $Class_name = "ELF";
                    $GiftCode = $gift_elf;
                    $msg_giftcode = $msg_elf;
                    
                    $GiftCode_time = $gift_time_elf;
                    $msg_giftcode_time = $msg_time_elf;
                    $day_giftcode_time = $day_time_elf;
            	break;
                
                case 48:
                case 49:
                case 50:
                    $Class_name = "MG";
                    $GiftCode = $gift_mg;
                    $msg_giftcode = $msg_mg;
                    
                    $GiftCode_time = $gift_time_mg;
                    $msg_giftcode_time = $msg_time_mg;
                    $day_giftcode_time = $day_time_mg;
            	break;
                
                case 64:
                case 65:
                case 66:
                    $Class_name = "DL";
                    $GiftCode = $gift_dl;
                    $msg_giftcode = $msg_dl;
                    
                    $GiftCode_time = $gift_time_dl;
                    $msg_giftcode_time = $msg_time_dl;
                    $day_giftcode_time = $day_time_dl;
            	break;
                
                case 80:
                case 81:
                case 82:
                case 83:
                    $Class_name = "SUM";
                    $GiftCode = $gift_sum;
                    $msg_giftcode = $msg_sum;
                    
                    $GiftCode_time = $gift_time_sum;
                    $msg_giftcode_time = $msg_time_sum;
                    $day_giftcode_time = $day_time_sum;
            	break;
                
                case 96:
                case 97:
                case 98:
                    $Class_name = "RF";
                    $GiftCode = $gift_rf;
                    $msg_giftcode = $msg_rf;
                    
                    $GiftCode_time = $gift_time_rf;
                    $msg_giftcode_time = $msg_time_rf;
                    $day_giftcode_time = $day_time_rf;
            	break;
            
            	default :  echo "Lớp nhân vật chưa định nghĩa."; exit();
            }
                
                $gift_name = "Giftcode Tân thủ 2 $Class_name";
                $item_read[] = array (
    				'itemcode'	=> $GiftCode . $gift_all,
                    'gcoin_km'  =>  $gcent_km,
                    'vpoint_km'  =>  $vcent_km,
                    'bank_zen'  =>  $zen,
                    'bank_chao' =>  $chao,
                    'bank_cre'  =>  $cre,
                    'bank_blue' =>  $blue,
                    'phucloi_point' =>  $pp_extra,
                    'des'   =>  $msg_all ."<br />". $msg_giftcode,
    				'rate'	=> 1,
                    'rate_from'  =>  1,
                    'rate_to'    =>  1
    			);
                
                if(strlen($gift_time_all) > 0) {
                    $item_time_read[0] = array(
                        'itemcode'  =>  $gift_time_all,
                        'des'   =>  $msg_time_all,
                        'day'   =>  $day_time_all
                    );
                }
                
                if(strlen($GiftCode_time) > 0) {
                    $item_time_read[1] = array(
                        'itemcode'  =>  $GiftCode_time,
                        'des'   =>  $msg_giftcode_time,
                        'day'   =>  $day_giftcode_time
                    );
                }
                    
    	break;
        
        // GiftCode Tan thu
        case 9:
            if($giftcode_acc != $login) {
                echo "Giftcode $giftcode không phải của tài khoản $login. Không thể nhận thưởng"; exit();
            }
            include_once("config/config_giftcode_tanthu.php");
            $class_check_query = "SELECT Class FROM Character WHERE Name='$name'";
            $class_check_result = $db->Execute($class_check_query);
                check_queryerror($class_check_query, $class_check_result);
            $class_check = $class_check_result->FetchRow();
            
            $ClassType =  $class_check[0];
            switch ($ClassType){ 
            	case 0:
                case 1:
                case 2:
                case 3:
                    $Class_name = "DW";
                    $GiftCode = $gift_dw;
                    $msg_giftcode = $msg_dw;
            	break;
            
            	case 16:
                case 17:
                case 18:
                case 19:
                    $Class_name = "DK";
                    $GiftCode = $gift_dk;
                    $msg_giftcode = $msg_dk;
            	break;
            
            	case 32:
                case 33:
                case 34:
                case 35:
                    $Class_name = "ELF";
                    $GiftCode = $gift_elf;
                    $msg_giftcode = $msg_elf;
            	break;
                
                case 48:
                case 49:
                case 50:
                    $Class_name = "MG";
                    $GiftCode = $gift_mg;
                    $msg_giftcode = $msg_mg;
            	break;
                
                case 64:
                case 65:
                case 66:
                    $Class_name = "DL";
                    $GiftCode = $gift_dl;
                    $msg_giftcode = $msg_dl;
            	break;
                
                case 80:
                case 81:
                case 82:
                case 83:
                    $Class_name = "SUM";
                    $GiftCode = $gift_sum;
                    $msg_giftcode = $msg_sum;
            	break;
                
                case 96:
                case 97:
                case 98:
                    $Class_name = "RF";
                    $GiftCode = $gift_rf;
                    $msg_giftcode = $msg_rf;
            	break;
            
            	default :  echo "Lớp nhân vật chưa định nghĩa."; exit();
            }
                
                $gift_name = "Giftcode Tân thủ $Class_name";
                $item_read[] = array (
    				'itemcode'	=> $GiftCode,
                    'gcoin_km'  =>  0,
                    'vpoint_km'  =>  0,
                    'bank_zen'  =>  0,
                    'bank_chao' =>  0,
                    'bank_cre'  =>  0,
                    'bank_blue' =>  0,
                    'phucloi_point' =>  0,
                    'des'   =>  $msg_giftcode,
    				'rate'	=> 1,
                    'rate_from'  =>  1,
                    'rate_to'    =>  1
    			);
    	break;
		
		// GiftCode Nạp Thẻ
    	case 10:
            if($giftcode_acc != $login) {
                echo "Giftcode $giftcode không phải của tài khoản $login. Không thể nhận thưởng"; exit();
            }
            include_once("config/config_event.php");
            $file_giftcode = 'config/giftcode_random.txt';
    		if($timestamp>=strtotime($event_napthegc_begin.' 00:00:00') && $timestamp<=strtotime($event_napthegc_end.' 23:59:00'))
			{
				//Đọc File GiftCode
				$giftcode_arr = array();
				if(is_file($file_giftcode)) {
					$fopen_host = fopen($file_giftcode, "r");
					$giftcode_read = fgets($fopen_host);
					
					$giftcode_arr = json_decode($giftcode_read, true);
					fclose($fopen_host);
					if(!is_array($giftcode_arr)) $giftcode_arr = array();
				}
				
				$gift_name = $giftcode_arr[$giftcode_nt_use]['name'];
				$item_read = $giftcode_arr[$giftcode_nt_use]['gift'];
			} else {
				echo "Event $giftcode_nt_name đã kết thúc."; exit();
			}
    	break;
        
    	default : echo "Mã GiftCode không đúng loại."; exit();
    }
}
    
if(isset($item_read)) {
        if(strlen(json_encode($item_read)) < 5) {
        echo "Phần thưởng không thời hạn $gift_name không tồn tại. Vui lòng liên hệ ADMIN để kiểm tra."; exit();
    } else {
        $warehouse_query = "SELECT CAST(Items AS image) FROM warehouse WHERE AccountID = '$login'";
        $warehouse_result = $db->Execute($warehouse_query);
            check_queryerror($warehouse_query, $warehouse_result);
        $warehouse_c = $warehouse_result->NumRows();
        if($warehouse_c == 0) {
            echo "Chưa mở Rương đồ chung. Hãy vào Game, mở Rương đồ chung, sau đó thoát Game rồi nhận thưởng.";
            exit();
        }
        $warehouse_fetch = $warehouse_result->FetchRow();
        $warehouse = $warehouse_fetch[0];
        $warehouse = bin2hex($warehouse);
        $warehouse = strtoupper($warehouse);
        
        $warehouse1 = substr($warehouse,0,120*32);
        $warehouse2 = substr($warehouse,120*32);
        
        if(strlen($warehouse1) < 120*32) {
            echo "Chưa 1 lần mở hòm đồ chung. Vui lòng vào Game mở hòm đồ chung ít nhất 1 lần mới được nhận thưởng GiftCode.";
            exit();
        }
        
        include_once('config_license.php');
        include_once('func_getContent.php');
        $getcontent_url = $url_license . "/api_giftcode_change.php";
        $getcontent_data = array(
            'acclic'    =>  $acclic,
            'key'    =>  $key,
            
            'item_read'    =>  json_encode($item_read),
            'warehouse1'   =>  $warehouse1
        );
        
        $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
        
        if ( empty($reponse) ) {
            $notice = "Server bảo trì vui lòng liên hệ Admin để FIX";
            echo $notice;
            exit();
        }
        else {
            $info = read_TagName($reponse, 'info');
            if($info == "Error") {
                $message = read_TagName($reponse, 'msg');
                echo $message;
                exit();
            } elseif($info == "OK") {
                $item_choise = read_TagName($reponse, 'item_choise');
                $item_gift = read_TagName($reponse, 'item_gift');
                if(strlen($item_choise) == 0) {
                    echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                    $arr_view = "\nDataSend:\n";
                    foreach($getcontent_data as $k => $v) {
                        $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                    }
                    writelog("log_api.txt", $arr_view . $reponse);
                    exit();
                }
            } else {
                echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
                writelog("log_api.txt", $reponse);
                exit();
            }
        }
        $item_gift_arr = json_decode($item_gift, true);
        
                
        $itemgift_count = count($item_gift_arr);
        if($itemgift_count > 0) {
            
            $warehouse1_new = $warehouse1;
            for($i=0; $i<$itemgift_count; $i++) {
                $item = $item_gift_arr[$i]['code'];
                    
                $item_seri = substr($item, 6, 8);
                $item_seri_dec = hexdec($item_seri);
                
                if($item_seri_dec < hexdec('FFFFFFF0')) {
                	$serial = _getSerial();
                	$item = substr_replace($item, $serial, 6, 8);
                }
                $warehouse1_new = substr_replace($warehouse1_new, $item, $item_gift_arr[$i]['vitri']*32, 32);
            }
            kiemtra_online($login);
            
            $warehouse_new = $warehouse1_new . $warehouse2;
            
            $warehouse_update_query = "UPDATE warehouse SET Items=0x$warehouse_new WHERE AccountID='$login'";
            $warehouse_update_result = $db->Execute($warehouse_update_query);
                check_queryerror($warehouse_update_query, $warehouse_update_result);
            
            $notice_giftcode = $item_read[$item_choise]['des'];
        }
        
        $gcoin_km = read_TagName($reponse, 'gcoin_km');
        $vpoint_km = read_TagName($reponse, 'vpoint_km');
        $bank_zen = read_TagName($reponse, 'bank_zen');
        $bank_chao = read_TagName($reponse, 'bank_chao');
        $bank_cre = read_TagName($reponse, 'bank_cre');
        $bank_blue = read_TagName($reponse, 'bank_blue');
        $phucloi_point = read_TagName($reponse, 'phucloi_point');
        $bank_update = "";
        $nganhang_msg = "";
        
        if($gcoin_km > 0) {
            $data['gcoin_km'] = $gcoin_km;
            if(strlen($bank_update) > 0) $bank_update .= ",";
            $bank_update .= "gcoin_km = gcoin_km + $gcoin_km";
            
            if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
            $nganhang_msg .= "$gcoin_km Gcent+";
        }
        if($vpoint_km > 0) {
            $data['vpoint_km'] = $vpoint_km;
            if(strlen($bank_update) > 0) $bank_update .= ",";
            $bank_update .= "vpoint_km = vpoint_km + $vpoint_km";
            
            if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
            $nganhang_msg .= "$vpoint_km VCent+";
        }
        if($bank_zen > 0) {
            $data['bank_zen'] = $bank_zen;
            if(strlen($bank_update) > 0) $bank_update .= ",";
            $bank_update .= "bank = bank + $bank_zen";
            
            if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
            $nganhang_msg .= "$bank_zen ZEN";
        }
        if($bank_chao > 0) {
            $data['bank_chao'] = $bank_chao;
            if(strlen($bank_update) > 0) $bank_update .= ",";
            $bank_update .= "jewel_chao = jewel_chao + $bank_chao";
            
            if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
            $nganhang_msg .= "$bank_chao Ngọc Hỗn Nguyên";
        }
        if($bank_cre > 0) {
            $data['bank_cre'] = $bank_cre;
            if(strlen($bank_update) > 0) $bank_update .= ",";
            $bank_update .= "jewel_cre = jewel_cre + $bank_cre";
            
            if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
            $nganhang_msg .= "$bank_cre Ngọc Sáng Tạo";
        }
        if($bank_blue > 0) {
            $data['bank_blue'] = $bank_blue;
            if(strlen($bank_update) > 0) $bank_update .= ",";
            $bank_update .= "jewel_blue = jewel_blue + $bank_blue";
            
            if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
            $nganhang_msg .= "$bank_blue Lông Vũ";
        }
        if($phucloi_point > 0) {
            $data['nbb_pl_extra'] = $phucloi_point;
            if(strlen($bank_update) > 0) $bank_update .= ",";
            $bank_update .= "nbb_pl_extra = nbb_pl_extra + $phucloi_point";
            
            if(strlen($nganhang_msg) > 0) $nganhang_msg .= ", ";
            $nganhang_msg .= "$phucloi_point PP+";
        }
        
        if(strlen($bank_update) > 0) {
            $bank_update_q = "UPDATE MEMB_INFO SET $bank_update WHERE memb___id='$login'";
            $bank_update_r = $db->Execute($bank_update_q);
                check_queryerror($bank_update_q, $bank_update_r);
        }
        
        if($giftcode_len == 6) {    // Giftcode 1ForALL
            $giftcode_insert_query = "INSERT INTO GiftCode (gift_code, acc, type, gift_time, gift_timeuse, ngay, status) VALUES ('$gift_code', '". $login ."', 6, $timestamp, $timestamp, '".date("Y-m-d",$timestamp)."', 2)";
            $giftcode_insert_result = $db->Execute($giftcode_insert_query);
                check_queryerror($giftcode_insert_query, $giftcode_insert_result);
        } else {
            $giftcode_update_query = "UPDATE GiftCode SET name='$name', acc='$login', status=2, gift_timeuse='$timestamp' WHERE gift_code='$gift_code' AND status=1 AND type=$giftcode_type";
            $giftcode_update_result = $db->Execute($giftcode_update_query);
                check_queryerror($giftcode_update_query, $giftcode_update_result);
        }
    }
}

    
if(isset($item_time_read)) {
    if(strlen(json_encode($item_time_read)) < 5) {
        echo "Phần thưởng có thời hạn $gift_name không tồn tại. Vui lòng liên hệ ADMIN để kiểm tra."; exit();
    } else {
        include_once('config_license.php');
        include_once('func_getContent.php');
        $getcontent_url = $url_license . "/api_giftcode_change.php";
        $getcontent_data = array(
            'acclic'    =>  $acclic,
            'key'    =>  $key,
            
            'type'  =>  'gift_time',
            'item_time_read'    =>  json_encode($item_time_read)
        );
        
        $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
        
        if ( empty($reponse) ) {
            $notice = "Server bảo trì vui lòng liên hệ Admin để FIX";
            echo $notice;
            exit();
        }
        else {
            $info = read_TagName($reponse, 'info');
            if($info == "Error") {
                $message = read_TagName($reponse, 'msg');
                echo $message;
                exit();
            } elseif($info == "OK") {
                $item_time_gift = read_TagName($reponse, 'item_time_gift');
                if(strlen($item_time_gift) == 0) {
                    echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                    $arr_view = "\nDataSend:\n";
                    foreach($getcontent_data as $k => $v) {
                        $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                    }
                    writelog("log_api.txt", $arr_view . $reponse);
                    exit();
                }
            } else {
                echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
                writelog("log_api.txt", $reponse);
                exit();
            }
        }
        
        $item_time_gift_arr = json_decode($item_time_gift, true);
        $item_time_gift_count = count($item_time_gift_arr);
        
        for($i=0; $i< $item_time_gift_count; $i++) {
            $item_time_insert_q = "INSERT INTO Titan_Rewards (AccountID, Name, Zen, VIPMoney, Num, Lvl, Opt, Luck, Skill, Dur, Excellent, Ancient, JOH, Sock1, Sock2, Sock3, Sock4, Sock5, Days, SerialFFFFFFFE) VALUES ('$login', '$name', 0, 0, ". $item_time_gift_arr[$i]['Num'] .", ". $item_time_gift_arr[$i]['Lvl'] .", ". $item_time_gift_arr[$i]['Opt'] .", ". $item_time_gift_arr[$i]['Luck'] .", ". $item_time_gift_arr[$i]['Skill'] .", ". $item_time_gift_arr[$i]['Dur'] .", ". $item_time_gift_arr[$i]['Excellent'] .", ". $item_time_gift_arr[$i]['Ancient'] .", ". $item_time_gift_arr[$i]['JOH'] .", ". $item_time_gift_arr[$i]['Sock1'] .", ". $item_time_gift_arr[$i]['Sock2'] .", ". $item_time_gift_arr[$i]['Sock3'] .", ". $item_time_gift_arr[$i]['Sock4'] .", ". $item_time_gift_arr[$i]['Sock5'] .", ". $item_time_gift_arr[$i]['Days'] .", ". $item_time_gift_arr[$i]['SerialFFFFFFFE'] .")";
            $item_time_insert_r = $db->Execute($item_time_insert_q);
                check_queryerror($item_time_insert_q, $item_time_insert_r);
        }
        
        if(strlen($item_time_read[0]['itemcode']) > 0) {
            $item_time_msg .= "<br />Phần thưởng Item sử dụng trong <strong>". $item_time_read[0]['day'] ." ngày</strong> : ". $item_time_read[0]['des'];
        }
        if(strlen($item_time_read[1]['itemcode']) > 0) {
            $item_time_msg .= "<br />Phần thưởng Item sử dụng trong <strong>". $item_time_read[1]['day'] ." ngày</strong> : ". $item_time_read[1]['des'];
        }
        if(strlen($item_time_msg) > 0) {
            $item_time_msg .= "<br />nhận tại NPC <strong>ReWard - Lorencia (143, 138) Server-1</strong> .";
        }
        
    }
}

if(isset($buff) && $buff > 0 && $buff_day > 0) {
    $buff_get_q = "SELECT SCFSealItem, SCFSealTime FROM Character WHERE Name='$name'";
    $buff_get_r = $db->Execute($buff_get_q);
        check_queryerror($buff_get_q, $buff_get_r);
    $buff_get_f = $buff_get_r->FetchRow();
    
    $data['SCFSealItem'] = $buff;
    
    switch ($buff){ 
    	case 6699:
            $buff_n = "<strong>Bùa tăng Exp</strong> $buff_day ngày";
            if($buff_get_f[0] == 6699) {
                if($buff_get_f[1] > $timestamp) {
                    $SCFSealTime_new = $buff_get_f[1] + $buff_day*24*60*60;
                } else {
                    $SCFSealTime_new = $timestamp + $buff_day*24*60*60;
                }
            } else {
                $SCFSealTime_new = $timestamp + $buff_day*24*60*60;
            }
            $data['SCFSealTime'] = $SCFSealTime_new;
            $buff_q = "UPDATE Character SET SCFSealItem=6699, SCFSealTime = $SCFSealTime_new WHERE Name='$name'";
    	break;
    
    	case 6700:
            $buff_n = "<strong>Bùa Thiên Sứ</strong> $buff_day ngày";
            if($buff_get_f[0] == 6700) {
                if($buff_get_f[1] > $timestamp) {
                    $SCFSealTime_new = $buff_get_f[1] + $buff_day*24*60*60;
                } else {
                    $SCFSealTime_new = $timestamp + $buff_day*24*60*60;
                }
            } else {
                $SCFSealTime_new = $timestamp + $buff_day*24*60*60;
            }
            $data['SCFSealTime'] = $SCFSealTime_new;
            $buff_q = "UPDATE Character SET SCFSealItem=6700, SCFSealTime = $SCFSealTime_new WHERE Name='$name'";
    	break;
    
    	case 6749:
            $buff_n = "<strong>Bùa Master</strong> $buff_day ngày";
            if($buff_get_f[0] == 6749) {
                if($buff_get_f[1] > $timestamp) {
                    $SCFSealTime_new = $buff_get_f[1] + $buff_day*24*60*60;
                } else {
                    $SCFSealTime_new = $timestamp + $buff_day*24*60*60;
                }
            } else {
                $SCFSealTime_new = $timestamp + $buff_day*24*60*60;
            }
            $data['SCFSealTime'] = $SCFSealTime_new;
            $buff_q = "UPDATE Character SET SCFSealItem=6749, SCFSealTime = $SCFSealTime_new WHERE Name='$name'";
    	break;
    }
    
    if(strlen($buff_q) > 0) {
        $buff_r = $db->Execute($buff_q);
            check_queryerror($buff_q, $buff_r);
        $buff_notice = $buff_n;
    }
}

    //Ghi vào Log
            $log_price = "-";
            $log_Des = "Tài khoản $login đã đổi GiftCode $gift_code cho nhân vật : $name. Phần thưởng GiftCode: $notice_giftcode . $item_time_msg . <br />Ngân hàng : $nganhang_msg";
            _writelog_tiente($login, $log_price, $log_Des);
    //End Ghi vào Log nhung nhan vat mua Item
    
    $data['notice'] = "Bạn đã đổi thành công $gift_name cho nhân vật <strong>$name</strong>.";
    if(strlen($notice_giftcode) > 0) {
        $data['notice'] .=  "<br><strong>Phần thưởng không giới hạn thời gian</strong> :<br /> $notice_giftcode <br />đã được đưa vào rương đồ chung.";
    }
    if(strlen($item_time_msg) > 0) {
        $data['notice'] .=   $item_time_msg;
    }
    if(strlen($nganhang_msg) > 0) {
        $data['notice'] .=   "<br /><strong>Ngân hàng</strong> : $nganhang_msg";
    }
    if(strlen($buff_notice) > 0) {
        $data['notice'] .=   "<br />Bùa : $buff_notice";
    }
    
    
    echo "<nbb>OK<nbb>". json_encode($data) ."<nbb>";
}
$db->Close();
?>