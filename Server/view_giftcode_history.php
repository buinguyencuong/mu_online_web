<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
	include_once("security.php");
include_once("config.php");
include_once("function.php");

$passtransfer = $_POST["passtransfer"];

if ($passtransfer == $transfercode) {
    $login = $_POST["login"];
    
    $giftcode_history_data_arr = array();
    
    $time_get_last = _time() - 7*24*60*60;
    // Giftcode SMS
    $giftcode_sms_query = "SELECT KeyXuLy, time, Code FROM SMS WHERE acc='$login' AND (KeyXuLy='GIFTCODE_RS' OR KeyXuLy='GIFTCODE_WEEK' OR KeyXuLy='GIFTCODE_MONTH') AND time>$time_get_last ORDER BY time";
    $giftcode_sms_result = $db->Execute($giftcode_sms_query);
        check_queryerror($giftcode_sms_query, $giftcode_sms_result);
    
    while($giftcode_sms_fetch = $giftcode_sms_result->FetchRow()) {
        $exp = 0;
        switch ($giftcode_sms_fetch[0]){ 
        	case 'GIFTCODE_RS':
                if(_time() - $giftcode_sms_fetch[1] > 60*60) $exp = 1;
        	break;
        
        	case 'GIFTCODE_WEEK':
                if(_time() - $giftcode_sms_fetch[1] > 5*60) $exp = 1;
        	break;
        
        	case 'GIFTCODE_MONTH':
                if(_time() - $giftcode_sms_fetch[1] > 5*60) $exp = 1;
        	break;
        
        	default :
        }
        
        $giftcode_history_data_arr['sms'][] = array (
            'KeyXuLy' =>  $giftcode_sms_fetch[0],
            'time' =>  $giftcode_sms_fetch[1],
            'exp'  =>  $exp,
            'Code' =>  $giftcode_sms_fetch[2]
        );
    }
    
    // Giftcode History UnRecive
    $giftcode_history_un_q = "SELECT gift_code, name, type, gift_time, gift_timeuse, giftcode_type FROM GiftCode WHERE acc='$login' AND status=1 ORDER BY gift_time DESC";
    $giftcode_history_un_r = $db->Execute($giftcode_history_un_q);
        check_queryerror($giftcode_history_un_q, $giftcode_history_un_r);
    
    while($giftcode_history_un_f = $giftcode_history_un_r->FetchRow()) {
        $giftcode_history_data_arr['giftcode'][] = array (
            'gift_code' =>  $giftcode_history_un_f[0],
            'name' =>  $giftcode_history_un_f[1],
            'type' =>  $giftcode_history_un_f[2],
            'giftcode_gifttype' =>  $giftcode_history_un_f[5],
            'gift_time' =>  $giftcode_history_un_f[3],
            'gift_timeuse' =>  $giftcode_history_un_f[4],
            'status' =>  1
        );
    }
    
    // Giftcode History
    $giftcode_history_query = "SELECT TOP 50 gift_code, name, type, gift_time, gift_timeuse, status, giftcode_type FROM GiftCode WHERE acc='$login' AND status<>1 ORDER BY gift_time DESC";
    $giftcode_history_result = $db->Execute($giftcode_history_query);
        check_queryerror($giftcode_history_query, $giftcode_history_result);
    
    while($giftcode_history_fetch = $giftcode_history_result->FetchRow()) {
        $giftcode_history_data_arr['giftcode'][] = array (
            'gift_code' =>  $giftcode_history_fetch[0],
            'name' =>  $giftcode_history_fetch[1],
            'type' =>  $giftcode_history_fetch[2],
            'giftcode_gifttype' =>  $giftcode_history_fetch[6],
            'gift_time' =>  $giftcode_history_fetch[3],
            'gift_timeuse' =>  $giftcode_history_fetch[4],
            'status' =>  $giftcode_history_fetch[5]
        );
    }
    
    
    $giftcode_history_data = serialize($giftcode_history_data_arr);
    echo "<nbb>OK<nbb>" . $giftcode_history_data . "<nbb>";
}
$db->Close();
?>