<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
	include_once("security.php");
include_once("config.php");
include('config/config_webshop.php');
$file_webshopitem = 'config/webshopitem.txt';


$login = $_POST['login'];
$name = $_POST['name'];
$action = $_POST['action'];
$passtransfer = $_POST["passtransfer"];

if ($passtransfer == $transfercode) {
    
    switch ($action) { 
    
    	case 'webshop_exl_buy':
            $webshop_type = abs(intval($_POST['webshop_type']));   if($webshop_type < 1 || $webshop_type > 13) $webshop_type = 99;
            $code = $_POST['code'];
            $luck = abs(intval($_POST['luck']));         if($luck != 1) $luck = 0;
            $exl[1] = abs(intval($_POST['exl1']));         if($exl[1] != 1) $exl[1] = 0;
            $exl[2] = abs(intval($_POST['exl2']));         if($exl[2] != 1) $exl[2] = 0;
            $exl[3] = abs(intval($_POST['exl3']));         if($exl[3] != 1) $exl[3] = 0;
            $exl[4] = abs(intval($_POST['exl4']));         if($exl[4] != 1) $exl[4] = 0;
            $exl[5] = abs(intval($_POST['exl5']));         if($exl[5] != 1) $exl[5] = 0;
            $exl[6] = abs(intval($_POST['exl6']));         if($exl[6] != 1) $exl[6] = 0;
            $lvl = abs(intval($_POST['lvl']));           if($lvl < 0 || $lvl > 15) $lvl = 0;
            $opt = abs(intval($_POST['opt']));           if($opt < 0 || $opt > 7) $opt = 0;
            
            $error = "";
        if (!preg_match("/^[A-F0-9]*$/i", $code))
    	{
            $error .= "Dữ liệu lỗi <strong>Mã Item</strong> : $code . Chi duoc su dung ki tu a-f, A-F, so (1-9).<br />"; 
    	}
        if( strlen($code) <> 32 ) {
            $error .= "Dữ liệu lỗi <strong>Mã Item</strong> : $code phải có 32 ký tự.<br />";
        }
        if($lvl > $item_lv_max) {
            $error .= "Dữ liệu lỗi <strong>Cấp độ Item</strong> : <strong>$code</strong> không được lớn hơn <strong>$item_lv_max</strong>.<br />";
        }
        
        if(strlen($error) == 0) {
            //Đọc File Reward
        	$fopen_host = fopen($file_webshopitem, "r");
            $item_get = fgets($fopen_host);
        	fclose($fopen_host);
            
            $item_listall_arr = json_decode($item_get, true);
            
            $item = array();
            if( isset($item_listall_arr[$webshop_type][$code]) && $item_listall_arr[$webshop_type][$code]['stat'] == 1 ) {
                $item = $item_listall_arr[$webshop_type][$code];
                $item_name = $item['item_name'];
                $price_item = $item['price'];
                
                $item_name_extra = "";
                // Luck
                if($luck == 1) {
                    $price_item += $price_luck;
                    if(strlen($item_name_extra) > 0) $item_name_extra .= ",";
                    $item_name_extra .= "Luck";
                }
                
                switch ($item['exl_type']) {
                	case 0 :
                        $price_exl[1]	= $vukhi_exl[1];
                		$price_exl[2]	= $vukhi_exl[2];
                		$price_exl[3]	= $vukhi_exl[3];
                		$price_exl[4]	= $vukhi_exl[4];
                		$price_exl[5]	= $vukhi_exl[5];
                		$price_exl[6]	= $vukhi_exl[6];
                        
                        $exl_info[1]	= 'Tăng lượng MANA khi giết quái (MANA/8)';
                		$exl_info[2]	= 'Tăng lượng LIFE khi giết quái (LIFE/8)';
                		$exl_info[3]	= 'Tốc độ tấn công +7';
                		$exl_info[4]	= 'Tăng lực tấn công 2%';
                		$exl_info[5]	= 'Tăng lực tấn công (Cấp độ/20)';
                		$exl_info[6]	= 'Khả năng xuất hiện lực tấn công hoàn hảo +10%';
                        $opt_info['option_type'] = 'Tăng thêm sát thương'; 
                        $opt_info['option_mul'] = 4;
                        $opt_info['option_bonus'] = '';
                		break;
                	case 1:
                        $price_exl[1]	= $giap_exl[1];
                		$price_exl[2]	= $giap_exl[2];
                		$price_exl[3]	= $giap_exl[3];
                		$price_exl[4]	= $giap_exl[4];
                		$price_exl[5]	= $giap_exl[5];
                		$price_exl[6]	= $giap_exl[6];
                        
                        $exl_info[1]	= 'Lượng ZEN rơi ra khi giết quái +40%';
                		$exl_info[2]	= 'Khả năng xuất hiện phòng thủ hoàn hảo +10%';
                		$exl_info[3]	= 'Phản hồi sát thương +5%';
                		$exl_info[4]	= 'Giảm sát thương +4%';
                		$exl_info[5]	= 'Lượng MANA tối đa +4%';
                		$exl_info[6]	= 'Lượng HP tối đa +4%';
                        $opt_info['option_type'] = 'Tăng thêm phòng thủ'; 
                        $opt_info['option_mul'] = 5;
                        $opt_info['option_bonus'] = '';
                		break;
                	case 2:
                        $price_exl[1]	= $giap_exl[1];
                		$price_exl[2]	= $giap_exl[2];
                		$price_exl[3]	= $giap_exl[3];
                		$price_exl[4]	= $giap_exl[4];
                		$price_exl[5]	= $giap_exl[5];
                		$price_exl[6]	= $giap_exl[6];
                        
                        $exl_info[1]	= 'Lượng ZEN rơi ra khi giết quái +40%';
                		$exl_info[2]	= 'Khả năng xuất hiện phòng thủ hoàn hảo +10%';
                		$exl_info[3]	= 'Phản hồi sát thương +5%';
                		$exl_info[4]	= 'Giảm sát thương +4%';
                		$exl_info[5]	= 'Lượng MANA tối đa +4%';
                		$exl_info[6]	= 'Lượng HP tối đa +4%';
                        $opt_info['option_type'] = 'Tăng thêm phòng thủ'; 
                        $opt_info['option_mul'] = 4;
                        $opt_info['option_bonus'] = '';
                		break;
                	case 3: 
                        $price_exl[1]	= $wing2_exl[1];
                		$price_exl[2]	= $wing2_exl[2];
                		$price_exl[3]	= $wing2_exl[3];
                		$price_exl[4]	= $wing2_exl[4];
                		$price_exl[5]	= $wing2_exl[5];
                        
                        $exl_info[1]	= '+ 115 Lượng HP tối đa';
                		$exl_info[2]	= '+ 115 Lượng MP tối đa';
                		$exl_info[3]	= 'Khả năng loại bỏ phòng thủ đối phương +3%';
                		$exl_info[4]	= '+ 50 Lực hành động tối đa';
                		$exl_info[5]	= 'Tốc độ tấn công +7';
                        $opt_info['option_type'] = 'Tăng thêm sát thương'; 
                        $opt_info['option_mul'] = 4;
                        $opt_info['option_bonus'] = '';
                		break;
                	case 4:
                        $price_exl[1]	= $giap_exl[1];
                		$price_exl[2]	= $giap_exl[2];
                		$price_exl[3]	= $giap_exl[3];
                		$price_exl[4]	= $giap_exl[4];
                		$price_exl[5]	= $giap_exl[5];
                		$price_exl[6]	= $giap_exl[6];
                        
                        $exl_info[1]	= 'Lượng ZEN rơi ra khi giết quái +40%';
                		$exl_info[2]	= 'Khả năng xuất hiện phòng thủ hoàn hảo +10%';
                		$exl_info[3]	= 'Phản hồi sát thương +5%';
                		$exl_info[4]	= 'Giảm sát thương +4%';
                		$exl_info[5]	= 'Lượng MANA tối đa 4%';
                		$exl_info[6]	= 'Lượng HP tối đa 4%';
                        
                        $price_option[$opt] = $pent_ring_price_option[$opt];
                        
                        $opt_info['option_type'] = 'Tự động hồi phục HP'; 
                        $opt_info['option_mul'] = 1;
                        $opt_info['option_bonus'] = '%';
                		break;
                	case 5:
                        $price_exl[1]	= $vukhi_exl[1];
                		$price_exl[2]	= $vukhi_exl[2];
                		$price_exl[3]	= $vukhi_exl[3];
                		$price_exl[4]	= $vukhi_exl[4];
                		$price_exl[5]	= $vukhi_exl[5];
                		$price_exl[6]	= $vukhi_exl[6];
                        
                        $exl_info[1]	= 'Tăng lượng MANA khi giết quái (MANA/8)';
                		$exl_info[2]	= 'Tăng lượng LIFE khi giết quái (LIFE/8)';
                		$exl_info[3]	= 'Tốc độ tấn công +7';
                		$exl_info[4]	= 'Tăng lực tấn công 2%';
                		$exl_info[5]	= 'Tăng lực tấn công (Cấp độ/20)';
                		$exl_info[6]	= 'Khả năng xuất hiện lực tấn công hoàn hảo +10%';
                        
                        $price_option[$opt] = $pent_ring_price_option[$opt];
                        
                        $opt_info['option_type'] = 'Tự động hồi phục HP'; 
                        $opt_info['option_mul'] = 1;
                        $opt_info['option_bonus'] = '%';
                		break;
                	case 6:
                        $price_exl[1]	= 'Sói Tấn Công';
                		$price_exl[2]	= 'Sói Phòng Thủ';
                		$price_exl[3]	= 'Sói Hoàng Kim';
                        
                        $exl_info[1]	= 'Sói Tấn Công';
                		$exl_info[2]	= 'Sói Phòng Thủ';
                		$exl_info[3]	= 'Sói Hoàng Kim';
                        $opt_info['option_type'] = ''; 
                        $opt_info['option_mul'] = 1;
                        $opt_info['option_bonus'] = '';
                		break;
                    case 7:
                        $price_exl[1]	= $wing3_exl[1];
                		$price_exl[2]	= $wing3_exl[2];
                		$price_exl[3]	= $wing3_exl[3];
                		$price_exl[4]	= $wing3_exl[4];
                        
                        $exl_info[1]	= '5% cơ hội loại bỏ sức phòng thủ';
                		$exl_info[2]	= '5 % phản đòn khi cận chiến';
                		$exl_info[3]	= '5% khả năng hồi phục hoàn toàn HP';
                		$exl_info[4]	= '5% khả năng hồi phục hoàn toàn nội lực';
                        
                        $price_option[$opt] = $w3_price_option[$opt];
                        $price_lv_plus[$lvl] = $w3_price_lv_plus[$lvl];
                        
                        $opt_info['option_type'] = 'Tự động hồi phục HP'; 
                        $opt_info['option_mul'] = 1;
                        $opt_info['option_bonus'] = '%';
                		break;
                    case 8:
                        $price_exl[1]	= $wing25_exl[1];
                		$price_exl[2]	= $wing25_exl[2];
                		$price_exl[3]	= $wing25_exl[3];
                		$price_exl[4]	= $wing25_exl[4];
                        
                        $exl_info[1]	= '3% cơ hội loại bỏ sức phòng thủ';
                		$exl_info[2]	= '3 % phản đòn khi cận chiến';
                		$exl_info[3]	= '3% khả năng hồi phục hoàn toàn HP';
                		$exl_info[4]	= '3% khả năng hồi phục hoàn toàn nội lực';
                        
                        $price_option[$opt] = $wing25_price_option[$opt];
                        $price_lv_plus[$lvl] = $wing25_price_lv_plus[$lvl];
                        
                        $opt_info['option_type'] = 'Tự động hồi phục HP';
                        $opt_info['option_mul'] = 1; 
                        $opt_info['option_bonus'] = '%';
                		break;
                    case 9:
                        $price_exl[1]	= $wing4_exl[1];
                		$price_exl[2]	= $wing4_exl[2];
                		$price_exl[3]	= $wing4_exl[3];
                		$price_exl[4]	= $wing4_exl[4];
                        
                        $exl_info[1]	= '7% cơ hội loại bỏ sức phòng thủ';
                		$exl_info[2]	= '7 % phản đòn khi cận chiến';
                		$exl_info[3]	= '7% khả năng hồi phục hoàn toàn HP';
                		$exl_info[4]	= '7% khả năng hồi phục hoàn toàn nội lực';
                        
                        $price_option[$opt] = $w4_price_option[$opt];
                        $price_lv_plus[$lvl] = $w4_price_lv_plus[$lvl];
                        
                        $opt_info['option_type'] = 'Tự động hồi phục HP'; 
                        $opt_info['option_mul'] = 1;
                        $opt_info['option_bonus'] = '%';
                		break;
                	default:
               	}
                
                // Lvl
                if($lvl >= 1 && $lvl <= 15) {
                    $price_item += $price_lv_plus[$lvl];
                    if(strlen($item_name_extra) > 0) $item_name_extra .= ",";
                    $item_name_extra .= "Lvl $lvl";
                }
                // Option
                if($opt >= 1 && $opt <= 7) {
                    $price_item += $price_option[$opt];
                    if(strlen($item_name_extra) > 0) $item_name_extra .= ",";
                    $item_name_extra .= "Opt $opt";
                }
                
                // Excellent
                $exl_total = 0;
                foreach($exl as $k => $v) {
                    if($v == 1) {
                        ++$exl_total;
                        $price_item += $price_exl[$k];
                        if(strlen($item_name_extra) > 0) $item_name_extra .= ",";
                        $item_name_extra .= $exl_info[$k];
                    }
                }
                
                if($exl_total > 0 && $price_exl_plus[$exl_total] > 0) {
                    $price_item += $price_exl_plus[$exl_total];
                }
                
                $money_q = "SELECT gcoin,gcoin_km FROM MEMB_INFO WHERE memb___id='$login'";
                $money_r = $db->Execute($money_q);
                    check_queryerror($money_q, $money_r);
                $money_f = $money_r->FetchRow();
                $gcoin_before = $money_f[0];
                $gcoin_km_before = $money_f[1];
                $gcoin_total = $gcoin_before + $gcoin_km_before;
                
                if($gcoin_total < $price_item) {
                    $gcoin_less = $price_item - $gcoin_total;
                    $error = "Hiện tại đang có : $gcoin_before Gcoin + $gcoin_km_before Gcon khuyến mãi.<br /> Chi phí cần để thuê Item : $price_item Gcoin.<br /> <strong>Thiếu : $gcoin_less Gcoin</strong>";
                } else {
                    if($gcoin_km_before >= $price_item) {
                        $gcoin_after = $gcoin_before;
                        $gcoin_km_after = $gcoin_km_before - $price_item;
                    }
                    else {
                        $gcoin_after = $gcoin_before - ($price_item - $gcoin_km_before);
                        $gcoin_km_after = 0;
                    }
                    
                    $warehouse_query = "SELECT CAST(Items AS image) FROM warehouse WHERE AccountID='$login'";
                    $warehouse_result = $db->Execute($warehouse_query);
                        check_queryerror($warehouse_query, $warehouse_result);
                    $warehouse_fetch = $warehouse_result->FetchRow();
                    $warehouse = $warehouse_fetch[0];
                    $warehouse = bin2hex($warehouse);
                    $warehouse = strtoupper($warehouse);
                    $warehouse1 = substr($warehouse,0,120*32);
                    $warehouse2 = substr($warehouse,120*32);
                    
                    $serial = _getSerial();

                    include_once('config_license.php');
                    include_once('func_getContent.php');
                    $getcontent_url = $url_license . "/api_webshop.php";
                    $getcontent_data = array(
                        'acclic'    =>  $acclic,
                        'key'    =>  $key,
                        'action'    =>  'webshop_exl_buy',
                        
                        'code'    =>  $code,
                        'luck'    =>  $luck,
                        'exl1'    =>  $exl[1],
                        'exl2'    =>  $exl[2],
                        'exl3'    =>  $exl[3],
                        'exl4'    =>  $exl[4],
                        'exl5'    =>  $exl[5],
                        'exl6'    =>  $exl[6],
                        'lvl'    =>  $lvl,
                        'opt'    =>  $opt,
                        'warehouse1'    =>  $warehouse1,
                        'serial' =>  $serial
                    ); 
                    
                    $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
                
                	if ( empty($reponse) ) {
                        $error = "Server bảo trì vui lòng liên hệ Admin để FIX";
                    }
                    else {
                        $info = read_TagName($reponse, 'info');
                        if($info == "Error") {
                            $error = read_TagName($reponse, 'msg');
                        } elseif ($info == "OK") {
                            $warehouse1_receive = read_TagName($reponse, 'warehouse1');
                            if(strlen($warehouse1_receive) == 0) {
                                echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                                
                                $arr_view = "\nDataSend:\n";
                                foreach($getcontent_data as $k => $v) {
                                    $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                                }
                                writelog("log_api.txt", $arr_view . $reponse);
                                exit();
                            }
                        } else {
                            echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
                            writelog("log_api.txt", $reponse);
                            exit();
                        }
                    }
                    
                    if(strlen($error) == 0) {
                        kiemtra_online($login);

                    	$warehouse_new = $warehouse1_receive . $warehouse2;
                        
                        $warehouse_update_query = "UPDATE warehouse SET Items=0x$warehouse_new WHERE AccountID='$login'";
                        $warehouse_update_result = $db->Execute($warehouse_update_query);
                            check_queryerror($warehouse_update_query, $warehouse_update_result);
                        
                        $money_update_q = "UPDATE MEMB_INFO SET gcoin = $gcoin_after, gcoin_km = $gcoin_km_after WHERE memb___id='$login'";
                        $money_update_r = $db->Execute($money_update_q);
                            check_queryerror($money_update_q, $money_update_r);
                        
                        _use_money($login, $gcoin_before-$gcoin_after, $gcoin_km_before - $gcoin_km_after, 0);
                        
                        //Ghi vào Log nhung nhan vat mua Item
                        $log_price = "- $price_item Gc+,Gc+";
                        $log_Des = "Mua $item_name ($item_name_extra) .Seri: $serial .<br />Trước : $gcoin_before Gc, $gcoin_km_before GcKM, $vpoint_before Vc, $vpoint_km_before VcE .<br />Sau : $gcoin_after Gc, $gcoin_km_after GcKM, $vpoint_after Vc, $vpoint_km_after VcE .";
                        _writelog_tiente($login, $log_price, $log_Des, 3);
                        //End Ghi vào Log nhung nhan vat mua Item
                        
                        echo "<info>OK</info><gcoin>$gcoin_after</gcoin><gcoinkm>$gcoin_km_after</gcoinkm>";
                    }
                }
            } else {
                $error = "Item muốn Thuê không có.";
            }
        }
        
        if(strlen($error) > 0) {
            echo $error;
        }
           
    	break;
        
     }
}
$db->Close();
?>