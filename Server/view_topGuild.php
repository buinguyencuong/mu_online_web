<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
	include_once("security.php");
include_once("config.php");
include ('config/config_thehe.php');
include('config/config_ranking.php');

$passtransfer = $_POST["passtransfer"];

if ($passtransfer == $transfercode) {
    _guild_xh();
    
if(!isset($rank_other_slg) || abs(intval($rank_other_slg)) < 20) $rank_other_slg = 20;
$thehe_query = "";
foreach($thehe_choise as $thehe_key => $thehe_val) {
    if(strlen($thehe_val) > 0) {
        if(strlen($thehe_query) > 0) $thehe_query .= ",";
        $thehe_query .= $thehe_key;
    }
}

    // TOP GULD Point
    $GuildPoint_q = "SELECT TOP $rank_other_slg G_NAME, G_Master, G_PointTotal, G_SlgMem, G_RSTotal, G_Mark, Number, G_Rival, G_Union FROM Guild JOIN Character ON Guild.G_Master collate DATABASE_DEFAULT = Character.Name collate DATABASE_DEFAULT JOIN MEMB_INFO ON Character.AccountID collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe IN (". $thehe_query .") ORDER BY G_PointTotal DESC";
    $GuildPoint_r = $db->Execute($GuildPoint_q);
        check_queryerror($GuildPoint_q, $GuildPoint_r);
    
    $count_guildmember = 0;    
    while($GuildPoint_f = $GuildPoint_r->FetchRow()) {
        $G_Name = $GuildPoint_f[0];
        $G_Master = $GuildPoint_f[1];
        $G_PointTotal = $GuildPoint_f[2];
        $G_SlgMem = $GuildPoint_f[3];
        $G_RSTotal = $GuildPoint_f[4];
        $G_Logo = bin2hex($GuildPoint_f[5]);
        $G_Number = $GuildPoint_f[6];
        $G_Rival = $GuildPoint_f[7];
        $G_Union = $GuildPoint_f[8];
        
        if(strlen($G_Union) > 0) {
            if($G_Number == $G_Union) {
                $G_LM = $G_Name;
            } else {
                $g_lm_q = "SELECT G_NAME FROM Guild WHERE Number=$G_Union";
                $g_lm_r = $db->Execute($g_lm_q);
                    check_queryerror($g_lm_q, $g_lm_r);
                $g_lm_f = $g_lm_r->FetchRow();
                $G_LM = $g_lm_f[0];
            }
        } else {
            $G_LM = '-';
        }
        
        if(strlen($G_Rival) > 0) {
            $g_dd_q = "SELECT G_NAME FROM Guild WHERE Number=$G_Rival";
            $g_dd_r = $db->Execute($g_dd_q);
                check_queryerror($g_dd_q, $g_dd_r);
            $g_dd_f = $g_dd_r->FetchRow();
            $G_DD = $g_dd_f[0];
        } else {
            $G_DD = '-';
        }
        
        $GTop['Point'][$count_guildmember]['GName'] = $G_Name;
        $GTop['Point'][$count_guildmember]['GMaster'] = $G_Master;
        $GTop['Point'][$count_guildmember]['PointTotal'] = $G_PointTotal;
        $GTop['Point'][$count_guildmember]['SlgMem'] = $G_SlgMem;
        $GTop['Point'][$count_guildmember]['RSTotal'] = $G_RSTotal;
        $GTop['Point'][$count_guildmember]['Logo'] = $G_Logo;
        $GTop['Point'][$count_guildmember]['LM'] = $G_LM;
        $GTop['Point'][$count_guildmember]['DD'] = $G_DD;
        
        $count_guildmember++;
    }
	
    // TOP GULD RESET
    $GuildReset_q = "SELECT TOP $rank_other_slg G_NAME, G_Master, G_PointTotal, G_SlgMem, G_RSTotal, G_Mark, Number, G_Rival, G_Union FROM Guild JOIN Character ON Guild.G_Master collate DATABASE_DEFAULT = Character.Name collate DATABASE_DEFAULT JOIN MEMB_INFO ON Character.AccountID collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe IN (". $thehe_query .") ORDER BY G_RSTotal DESC";
    $GuildReset_r = $db->Execute($GuildReset_q);
        check_queryerror($GuildReset_q, $GuildReset_r);
    
    $count_guildreset = 0;
    while($GuildReset_f = $GuildReset_r->FetchRow()) {
        $G_Name = $GuildReset_f[0];
        $G_Master = $GuildReset_f[1];
        $G_PointTotal = $GuildReset_f[2];
        $G_SlgMem = $GuildReset_f[3];
        $G_RSTotal = $GuildReset_f[4];
        $G_Logo = bin2hex($GuildReset_f[5]);
        $G_Number = $GuildReset_f[6];
        $G_Rival = $GuildReset_f[7];
        $G_Union = $GuildReset_f[8];
        
        if(strlen($G_Union) > 0) {
            if($G_Number == $G_Union) {
                $G_LM = $G_Name;
            } else {
                $g_lm_q = "SELECT G_NAME FROM Guild WHERE Number=$G_Union";
                $g_lm_r = $db->Execute($g_lm_q);
                    check_queryerror($g_lm_q, $g_lm_r);
                $g_lm_f = $g_lm_r->FetchRow();
                $G_LM = $g_lm_f[0];
            }
        } else {
            $G_LM = '-';
        }
        
        if(strlen($G_Rival) > 0) {
            $g_dd_q = "SELECT G_NAME FROM Guild WHERE Number=$G_Rival";
            $g_dd_r = $db->Execute($g_dd_q);
                check_queryerror($g_dd_q, $g_dd_r);
            $g_dd_f = $g_dd_r->FetchRow();
            $G_DD = $g_dd_f[0];
        } else {
            $G_DD = '-';
        }
        
        $GTop['RS'][$count_guildreset]['GName'] = $G_Name;
        $GTop['RS'][$count_guildreset]['GMaster'] = $G_Master;
        $GTop['RS'][$count_guildreset]['PointTotal'] = $G_PointTotal;
        $GTop['RS'][$count_guildreset]['SlgMem'] = $G_SlgMem;
        $GTop['RS'][$count_guildreset]['RSTotal'] = $G_RSTotal;
        $GTop['RS'][$count_guildreset]['Logo'] = $G_Logo;
        $GTop['RS'][$count_guildreset]['LM'] = $G_LM;
        $GTop['RS'][$count_guildreset]['DD'] = $G_DD;
        
        $count_guildreset++;
    }
	
	$data_gtop = json_encode($GTop);
	
	echo "<nbb>$data_gtop</nbb>";
}
$db->Close();
?>