<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
$get_class_query = "SELECT Class FROM Character WHERE Name='$name'";
$get_class_result = $db->Execute($get_class_query);
$get_class = $get_class_result->fetchrow();

if ( ($get_class[0] == $class_dw_3) || ($get_class[0] == $class_dk_3) || ($get_class[0] == $class_elf_3) || ($get_class[0] == $class_mg_2) || ($get_class[0] == $class_dl_2) || ($get_class[0] == $class_sum_3) ) {
	//Nếu là Server phát triển theo WebZen (WebZen, ENC,...)
    if($server_wz == 1)
	{
	   
	   $query_get_masterlv = "SELECT MASTER_LEVEL FROM T_MasterLevelSystem WHERE CHAR_NAME='$name'";
		$result_get_masterlv = $db->Execute($query_get_masterlv) OR DIE("Loi Query : $query_get_masterlv");
		$get_masterlv = $result_get_masterlv->fetchrow();
		$masterlv = $get_masterlv[0];
		
		$sql_master_point = "UPDATE T_MasterLevelSystem SET ML_POINT='$masterlv' WHERE CHAR_NAME='$name'";
		$result_matser_point = $db->Execute($sql_master_point) OR DIE("Lỗi Query: $sql_master_point");
        
	}
	//Nếu là Server khác WebZen (SCF,...)
	else {
		$query_get_masterlv = "SELECT SCFMasterLevel FROM Character WHERE Name='$name'";
		$result_get_masterlv = $db->Execute($query_get_masterlv) OR DIE("Loi Query : $query_get_masterlv");
		$get_masterlv = $result_get_masterlv->fetchrow();
		$masterlv = $get_masterlv[0];
		
		$sql_master_point = "UPDATE Character SET SCFMasterPoints='$masterlv',SCFMasterSkills=CONVERT(varbinary(180), null),[MagicList]= CONVERT(varbinary(180), null) WHERE Name='$name'";
		$result_matser_point = $db->Execute($sql_master_point) OR DIE("Lỗi Query: $sql_master_point");
	}
}
?>