<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
 
//Info Data
$type_connect		=		'odbc';			//Dạng kết nối Database: 'odbc' hoac 'mssql'
$sql_version		=		'2008';				// SQL Version : 2000, 2005, 2008
$localhost			=		'localhost';
$databaseuser		=		'sa';			//User quản lý SQL MuOnline (Thường là 'sa')
$databsepassword	=		'16121995';		//Mật khẩu quản lý SQL MuOnline
$database			=		'MuOnline';		//Database MuOnline ('MuOnline' hoặc 'MeMuOnline')
$database_log		=		'MuOnline_Log';		//Database Ghi LOG NWeb
$passadmin			=		'e10adc3949ba59abbe56e057f20f883e';		//Pass để vào xem các trang: Log, Admin. Sử dụng dạng mã hóa MD5
$passviewcard		=		'e10adc3949ba59abbe56e057f20f883e';		//Pass để vào xem các trang: View Card. Sử dụng dạng mã hóa MD5
$passcard			=		'e10adc3949ba59abbe56e057f20f883e';		//Pass để vào xem các trang: CardPhone. Sử dụng dạng mã hóa MD5
$passcode			=		'e10adc3949ba59abbe56e057f20f883e';		//Pass để vào xem các trang: Online, CheckIP. Sử dụng dạng mã hóa MD5
$server_md5			=		0;			// 1: Sử dụng md5, 0: không sử dụng md5
$type_acc			=		0;			// 1: Tài khoản chỉ sử dụng số, 0: Tài khoản sử dụng cả số lẫn chữ
$transfercode		= 		'dwebgame';		// Mã so sánh nhận dữ liệu với Client
$server_wz = 0;		//0-Server SCF (Phat trien tu WebZen SS3) | 1-Server phat trien tu WebZen SS4 (ENC)
// Cach kiem tra loai Server
// Vao Database MuOnline > Character : Design Table
// Trong Table Character neu co : SCFMasterLevel, SCFMasterPoints => La Server SCF
// Vao Database MuOnline : Neu co Table T_MasterLevelSystem => La Server phat trien tu WebZen SS4 (ENC)
// 0 : Character (SCFMasterLevel, SCFMasterPoints), MEMB_INFO (WCoin)
// 1 : T_MasterLevelSystem (MASTER_LEVEL, ML_POINT), GameShop_Data (WCoinC)
// 2 : T_MasterLevelSystem (MASTER_LEVEL, ML_POINT), GameShopPoint (WCoinC)
// 3 (Season 8.3 IGCN) : Character (mLevel, mlPoint), T_InGameShop_Point (WCoinC, WCoinP, GoblinPoint)

//Danh sách IP của Hosting cho phép truy cập vào Web trên Server
$list_ip = array(
	"127.0.0.1",		// Local
	"27.72.60.7"	// Hosting
	);

date_default_timezone_set('Asia/Ho_Chi_Minh');
$timestamp = time();

$day = date("d",$timestamp);
$month = date("m",$timestamp);
$year = date("Y",$timestamp);

include_once("adodb/adodb.inc.php");

if($type_connect == 'odbc') {
	$db = ADONewConnection('odbc');
	$connect_mssql = $db->Connect($database,$databaseuser,$databsepassword);
	if (!$connect_mssql) die("Ket noi voi SQL Server loi! Hay kiem tra lai ODBC ton tai hoac User & Pass SQL dung.");
} elseif ($type_connect == 'mssql'){
	if (extension_loaded('mssql'))
	{echo("");}
	else die("Loi! Khong the load thu vien php_mssql.dll. Hay cho phep su dung php_mssql.dll trong php.ini");

	$db = &ADONewConnection('mssql');
	$connect_mssql = $db->Connect($localhost,$databaseuser,$databsepassword,$database);
	if (!$connect_mssql) die("Loi! Khong the ket noi Database $database");
}

include_once('config/config_dongbo.php');
//Nạp thẻ
$Card_per_page = 30; // Số lượng card tren 1 trang. 
$datedisplay = 'd/m/Y';	//Kiểu ngày tháng hiển thị
	
//Danh sách các Server:
$server[]	=	'Sub-1';	//Tên Sub lấy trong GameServer1\Data\ServerInfo.dat : ServerName
$server[]	=	'Sub-2';	//Tên Sub lấy trong GameServer2\Data\ServerInfo.dat : ServerName
$server[]	=	'Sub-3';	//Tên Sub lấy trong GameServer3\Data\ServerInfo.dat : ServerName
$server[]	=	'Sub-4';	//Tên Sub lấy trong GameServer4\Data\ServerInfo.dat : ServerName
