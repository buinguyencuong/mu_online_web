<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/

include_once('config/config_itemvpoint.php');
include ('config/config_alphatest.php');

if($use_alphatest == 1) {
    $block_dupe = false;
    $allow_itemnoseri = true;
    $remove_item_dupe = false;
} else {
    $block_dupe = false;
    $allow_itemnoseri = false;
    $remove_item_dupe = true;
}

$time_dis_charge = 1;  // Thoat Game 5 phut moi duoc doi Item Vpoint
$noitem = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";

$login=$_POST["login"];
$name=$_POST["name"];
$pass2 = $_POST['pass2'];

$passtransfer = $_POST["passtransfer"];

if ($passtransfer == $transfercode) {

$string_login = $_POST['string_login'];
checklogin($login,$string_login);

if(check_nv($login, $name) == 0) {
    echo "Nhân vật <b>{$name}</b> không nằm trong tài khoản <b>{$login}</b>. Vui lòng kiểm tra lại."; exit();
}

kiemtra_pass2($login,$pass2);

kiemtra_block_acc($login);
kiemtra_block_char($login,$name);
kiemtra_doinv($login,$name);
kiemtra_online($login);

$time_dis_query = "SELECT DisConnectTM FROM MEMB_STAT WHERE memb___id='$login'";
$time_dis_result = $db->Execute($time_dis_query);
$time_dis_fetch = $time_dis_result->FetchRow();
$time_dis = strtotime($time_dis_fetch[0]);
$timewait = ($time_dis + $time_dis_charge*60) - $timestamp;
if($timewait > 0) {
    $phutwait = floor($timewait/60);
    $giaywait = $timewait%60;
    echo "Bạn cần thoát Game và chờ trong vòng <strong>$phutwait phút $giaywait giây</strong> nữa mới có thể đổi Item Vcent.";
    exit();
}

$acc_vpoint_changed = array();
$name_vpoint_changed = array();
$seri_vpoint_changed = array();
$list_vpoint_changed_query = "SELECT acc, name, seri, value, time FROM item_vpoint_changed";
$list_vpoint_changed_result = $db->Execute($list_vpoint_changed_query);
    check_queryerror($list_vpoint_changed_query, $list_vpoint_changed_result);
while($list_vpoint_changed_fetch = $list_vpoint_changed_result->FetchRow()) {
    $seri_get = $list_vpoint_changed_fetch[2];
    
    $acc_vpoint_changed[$seri_get] = $list_vpoint_changed_fetch[0];
    $name_vpoint_changed[$seri_get] = $list_vpoint_changed_fetch[1];
    $seri_vpoint_changed[] = $seri_get;
    $value_vpoint_changed[$seri_get] = $list_vpoint_changed_fetch[3];
    $time_vpoint_changed[$seri_get] = date('H:i:s d/m/Y', $list_vpoint_changed_fetch[4]);
}
        
$inventory_result_sql = $db->Execute("SELECT CAST(Inventory AS image) FROM Character WHERE AccountID = '$login' AND Name='$name'");
$inventory_result = $inventory_result_sql->fetchrow();


$inventory = $inventory_result[0];
$inventory = bin2hex($inventory);
$inventory = strtoupper($inventory);
$inventory1 = substr($inventory,0,12*32);
$inventory2 = substr($inventory,12*32,64*32);
$inventory3 = substr($inventory,76*32);

$zen50k = 0;
$zen = 0;
$gold = 0;

$zen50k_dupe = 0;
$zen_dupe = 0;
$gold_dupe = 0;

$inventory2_after = "";

$seri_arr = array();

$seri_1k_changed = "";
$seri_10k_changed = "";
$seri_50k_changed = "";

$seri_1k_dupe = "";
$seri_10k_dupe = "";
$seri_50k_dupe = "";

$dupe = false;

for($x=0; $x<64; ++$x)
{
	$item = substr($inventory2,$x*32,32);
    if($item != $noitem) {
        $code = substr($item, 0, 4);
		$code2 = substr($item, 18, 1);
        $seri = substr($item, 6, 8);
        $seri_dex = hexdec($seri);
        $item_check = false;
        
		if($code === "0C10" AND $code2 ==="E") {
            $itemtype = "Item Vcent 1k";
            $itemvalue = 1000;
            $item_check = true;
            
            if($allow_itemnoseri === false && ($seri_dex == 0 || $seri_dex >= 4294967280)) {
                $gold_dupe++;
                if(strlen($seri_1k_dupe) > 0) $seri_1k_dupe .= ", ";
                $seri_1k_dupe .= "$seri (Seri Error)";
                $item_check = false;
                if($remove_item_dupe === true) {
                    $item = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                }
            }
            else if(in_array($seri, $seri_vpoint_changed, true) && ($seri_dex > 0 && $seri_dex < 4294967280)) {
                $gold_dupe++;
                if(strlen($seri_1k_dupe) > 0) $seri_1k_dupe .= ", ";
                $seri_1k_dupe .= "$seri (Acc <strong>". $acc_vpoint_changed[$seri] ."</strong> : ". $time_vpoint_changed[$seri] .")";
                $item_check = false;
                if($remove_item_dupe === true) {
                    $item = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                }
            }
		}	
		else if($code === "0C00" AND $code2 ==="E") {
            $itemtype = "Item Vcent 10k";
            $itemvalue = 10000;
            $item_check = true;
            
            if($allow_itemnoseri === false && ($seri_dex == 0 || $seri_dex >= 4294967280)) {
                $zen_dupe++;
                if(strlen($seri_10k_dupe) > 0) $seri_10k_dupe .= ", ";
                $seri_10k_dupe .= "$seri (Seri Error)";
                $item_check = false;
                if($remove_item_dupe === true) {
                    $item = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                }
            }
            else if(in_array($seri, $seri_vpoint_changed, true) && ($seri_dex > 0 && $seri_dex < 4294967280)) {
                $zen_dupe++;
                if(strlen($seri_10k_dupe) > 0) $seri_10k_dupe .= ", ";
                $seri_10k_dupe .= "$seri (Acc <strong>". $acc_vpoint_changed[$seri] ."</strong> : ". $time_vpoint_changed[$seri] .")";
                $item_check = false;
                if($remove_item_dupe === true) {
                    $item = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                }
            }
		}
        else if($code === "0F00" AND $code2 ==="E") {
            $itemtype = "Item Vcent 50k";
            $itemvalue = 50000;
            $item_check = true;
            
            if($allow_itemnoseri === false && ($seri_dex == 0 || $seri_dex >= 4294967280)) {
                $zen50k_dupe++;
                if(strlen($seri_50k_dupe) > 0) $seri_50k_dupe .= ", ";
                $seri_50k_dupe .= "$seri (Seri Error)";
                $item_check = false;
                if($remove_item_dupe === true) {
                    $item = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                }
            }
            else if(in_array($seri, $seri_vpoint_changed, true) && ($seri_dex > 0 && $seri_dex < 4294967280)) {
                $zen50k_dupe++;
                if(strlen($seri_50k_dupe) > 0) $seri_50k_dupe .= ", ";
                $seri_50k_dupe .= "$seri (Acc <strong>". $acc_vpoint_changed[$seri] ."</strong> : ". $time_vpoint_changed[$seri] .")";
                $item_check = false;
                if($remove_item_dupe === true) {
                    $item = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                }
            }
        }
        else if($code === "7800" AND $code2 ==="E") {
            $itemtype = "Item Vcent 50k";
            $itemvalue = 50000;
            $item_check = true;
            
            if($allow_itemnoseri === false && ($seri_dex == 0 || $seri_dex >= 4294967280)) {
                $zen50k_dupe++;
                if(strlen($seri_50k_dupe) > 0) $seri_50k_dupe .= ", ";
                $seri_50k_dupe .= "$seri (Seri Error)";
                $item_check = false;
                if($remove_item_dupe === true) {
                    $item = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                }
            }
            else if(in_array($seri, $seri_vpoint_changed, true) && ($seri_dex > 0 && $seri_dex < 4294967280)) {
                $zen50k_dupe++;
                if(strlen($seri_50k_dupe) > 0) $seri_50k_dupe .= ", ";
                $seri_50k_dupe .= "$seri (Acc <strong>". $acc_vpoint_changed[$seri] ."</strong> : ". $time_vpoint_changed[$seri] .")";
                $item_check = false;
                if($remove_item_dupe === true) {
                    $item = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                }
            }
        }
        
        if($item_check === true) {
            if(!in_array($seri, $seri_arr, true) || ($allow_itemnoseri === true && $seri_dex == 0)) {
                $seri_arr[] = $seri;
                $itemtype_arr[] = $itemtype;
                $vpoint_arr[] = $itemvalue;
                $item = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                
                switch ($itemvalue){ 
                	case 1000:
                        $gold++;
                        if(strlen($seri_1k_changed) > 0) $seri_1k_changed .= ", ";
                        $seri_1k_changed .= "$seri";
                	break;
                
                	case 10000:
                        $zen++;
                        if(strlen($seri_10k_changed) > 0) $seri_10k_changed .= ", ";
                        $seri_10k_changed .= "$seri";
                	break;
                
                	case 50000:
                        $zen50k++;
                        if(strlen($seri_50k_changed) > 0) $seri_50k_changed .= ", ";
                        $seri_50k_changed .= "$seri";
                	break;
                }
            } else {
                $dupe = true;
                switch ($itemvalue){ 
                	case 1000:
                        if(strlen($seri_1k_dupe) > 0) $seri_1k_dupe .= ", ";
                        $seri_1k_dupe .= "$seri";
                        $gold_dupe++;
                	break;
                
                	case 10000:
                        if(strlen($seri_10k_dupe) > 0) $seri_10k_dupe .= ", ";
                        $seri_10k_dupe .= "$seri";
                        $zen_dupe++;
                	break;
                
                	case 50000:
                        if(strlen($seri_50k_dupe) > 0) $seri_50k_dupe .= ", ";
                        $seri_50k_dupe .= "$seri";
                        $zen50k_dupe++;
                	break;
                }
                if($remove_item_dupe === true) {
                    $item = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                }
            }
        }
    }
    $inventory2_after .= $item;
		
}

if(strlen($seri_1k_dupe) > 0 || strlen($seri_10k_dupe) > 0 || strlen($seri_50k_dupe) > 0) {
    if($dupe === true && $block_dupe == true) {
        $char_block_query = "UPDATE Character SET ctlcode='99', ErrorSubBlock=99 WHERE name='$name'";
        $char_block_result = $db->Execute($char_block_query);
        
        $acc_block_query = "Update MEMB_INFO SET [bloc_code]='1',admin_block='1' WHERE memb___id='$login'";
        $acc_block_result = $db->Execute($acc_block_query);
        
        echo "Nhân vật $name chứa Vcent Dupe. Khóa tài khoản, nhân vật."; exit();
    }
}

if ( count($seri_arr) == 0 && strlen($seri_1k_dupe) == 0 && strlen($seri_10k_dupe) == 0 && strlen($seri_50k_dupe) == 0)
{
	echo "Không có Item V.Point để đổi ra V.Point";
}
else {
    $vpoint_add = 0;
    
    if(count($seri_arr) > 0) {
        $query = "select vpoint from MEMB_INFO WHERE memb___id='$login'";
        $result = $db->Execute( $query );
        $row = $result->fetchrow();
    
     	$vpoint_add = $gold*$item_low + $zen*$item_hight + $zen50k*$item_50k;
     	$vpoint_after = $row[0] + $vpoint_add;
     	
        $inventory_after = $inventory1.$inventory2_after.$inventory3;
        
        kiemtra_doinv($login,$name);
        kiemtra_online($login);
    
        $general = "UPDATE Character SET [inventory]=0x$inventory_after WHERE name='$name'";
        $msgeneral = $db->Execute($general) or die("Loi query: $general");
        
        $general1 = "UPDATE MEMB_INFO SET vpoint = $vpoint_after WHERE memb___id='$login'";
        $msgeneral1 = $db->Execute($general1);
        
        // INSERT Item Vpoint Change
        foreach($seri_arr as $key => $val) {
            $vpoint_changed_insert_query = "IF NOT EXISTS (SELECT * FROM item_vpoint_changed WHERE seri='$val') BEGIN INSERT INTO item_vpoint_changed (acc, name, itemtype, seri, value, time) VALUES ('$login', '$name', '". $itemtype_arr[$key] ."', '$val', ". $vpoint_arr[$key] .", $timestamp) END";
            $vpoint_changed_insert_result = $db->Execute($vpoint_changed_insert_query);
                check_queryerror($vpoint_changed_insert_query, $vpoint_changed_insert_result);
        }
    } elseif($remove_item_dupe === true && (strlen($seri_1k_dupe) > 0 || strlen($seri_10k_dupe) > 0 || strlen($seri_50k_dupe) > 0)) {
        $inventory_after = $inventory1.$inventory2_after.$inventory3;
        
        kiemtra_doinv($login,$name);
        kiemtra_online($login);
    
        $general = "UPDATE Character SET [inventory]=0x$inventory_after WHERE name='$name'";
        $msgeneral = $db->Execute($general) or die("Loi query: $general");
    }

//Ghi vào Log nhung nhan vat doi Item Vpoint
        $log_price = "+ $vpoint_add Vcent";
        $log_Des = "$name : Đổi ";
        if($gold > 0) {
            $log_Des .= "<strong>$gold Gold 1k</strong> ($seri_1k_changed)";
        }
        if($zen > 0) {
            if($gold > 0) {
                $log_Des .= ", ";
            }
            $log_Des .= "<strong>$zen Zen 10k</strong> ($seri_10k_changed)";
        }
        if($zen50k > 0) {
            if($gold > 0 || $zen > 0) {
                $log_Des .= ", ";
            }
            $log_Des .= "<strong>$zen50k Zen 50k</strong> ($seri_50k_changed)";
        }
        $log_Des .= " . Nhận <strong>$vpoint_add</strong> VCent.";
        
        if($gold_dupe > 0 || $zen_dupe > 0 || $zen50k_dupe > 0) {
            $log_Des .= "<br />Không được chuyển sang Vcent : ";
            if($gold_dupe > 0) {
                $log_Des .= " $gold_dupe Gold 1k ($seri_1k_dupe)";
            }
            if($zen_dupe > 0) {
                if($gold_dupe > 0) {
                    $log_Des .= ",";
                }
                $log_Des .= " $zen_dupe Zen 10k ($seri_10k_dupe)";
            }
            if($zen50k_dupe > 0) {
                if($gold_dupe > 0 || $zen_dupe > 0) {
                    $log_Des .= ",";
                }
                $log_Des .= " $zen50k_dupe Zend 50k ($seri_50k_dupe)";
            }
            $log_Des .= " Dupe.";
            
            if($remove_item_dupe === true) {
                $log_Des .= " Xóa Item Dupe.";
            }
        }
        $log_Des .= "<br />Trước : ". $row[0] ."| Sau : $vpoint_after Vc";
        $log_time = $timestamp;
        _writelog_tiente($login, $log_price, $log_Des);
//End Ghi vào Log nhung nhan vat doi Item Vpoint


    $content = "OK<nbb>$vpoint_add<nbb>Bạn đã đổi thành công ";
    if($gold > 0) {
        $content .= "<strong>$gold</strong> Gold 1k";
    }
    if($zen > 0) {
        if($gold > 0) {
            $content .= ", ";
        }
        $content .= "<strong>$zen</strong> Zen 10k";
    }
    if($zen50k > 0) {
        if($gold > 0 || $zen > 0) {
            $content .= ", ";
        }
        $content .= "<strong>$zen50k</strong> Zen 50k";
    }
    $content .= " ra <strong>$vpoint_add Vcent</strong>.";
    
    if($gold_dupe > 0 || $zen_dupe > 0 || $zen50k_dupe) {
        $content .= " Nhân vật chứa";
        if($gold_dupe > 0) {
            $content .= " $gold_dupe Item Gold 1k";
        }
        if($zen_dupe > 0) {
            if($gold_dupe > 0) {
                $content .= ",";
            }
            $content .= " $zen_dupe Item Zen 10k";
        }
        if($zen50k_dupe > 0) {
            if($gold_dupe > 0 || $zen_dupe > 0) {
                $content .= ",";
            }
            $content .= " $zen50k_dupe Item Zend 50k";
        }
        $content .= " Dupe, không được chuyển sang Vcent.";
    }
    echo $content;
        
}
}

?>