<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
include_once ("security.php");
include ('../config.php');
include ('function.php');

$file_giftcode_type = '../config/giftcode_type.txt';

$title = "Tạo GiftCode Phát";
session_start();


if(is_file($file_giftcode_type)) {
	$fopen_host = fopen($file_giftcode_type, "r");
    $giftcode_type_read = fgets($fopen_host);
    
    $giftcode_type_arr = json_decode($giftcode_type_read, true);
} else $fopen_host = fopen($file_giftcode_type, "w");
fclose($fopen_host);

if(!is_array($giftcode_type_arr)) $giftcode_type_arr = array();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<title><?php echo $title; ?></title>
<meta http-equiv=Content-Type content="text/html; charset=utf-8" />
</head>
<body>
<?php require('linktop.php'); ?>
<form action="" name="frm_giftphat" id="frm_giftphat" method="POST">
    <center>
    <strong>Số lượng GiftCode</strong> : <input name="gift_slg" id="gift_slg" value="<?php if($_POST['gift_slg']) echo $_POST['gift_slg']; ?>" size="3" /> 
    <select name='giftcode_type'>";
    <?php
        if(is_array($giftcode_type_arr)) {
            foreach($giftcode_type_arr as $k => $v) {
                echo "<option value='$k' ";
                if($_POST['giftcode_type'] == $k) echo "selected";
                echo " >". $v['name'] ."</option>";
            }
        }
    ?>
    </select>
    <input type="submit" name="submit_frm_giftphat" value="Tạo GiftCode Phát" />
    </center>
</form>

<?php

if($_POST['gift_slg']) {
    $giftcode_type = $_POST['giftcode_type'];       $giftcode_type = abs(intval($giftcode_type));
    $gift_slg = abs(intval($_POST['gift_slg']));
    $acc_list_arr = explode(',', $acc_list);
    $err = "";
    $err_flag = false;
    
    if($gift_slg == 0) {
        $err .= "Số lượng GiftCode phải lớn hơn 0.<br />";
        $err_flag = true;
    }
    
    if($err_flag === false) {
        include_once('../config_license.php');
        include_once('../func_getContent.php');
        $getcontent_url = $url_license . "/api_giftcode_create.php";
        $getcontent_data = array(
            'acclic'    =>  $acclic,
            'key'    =>  $key,
            
            'gift_slg'  =>  $gift_slg
        ); 
        
        $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
    
    	if ( empty($reponse) ) {
            $err .= "Server bao tri vui long lien he Admin de FIX";
            $err_flag = true;
        }
        else {
            $info = read_TagName($reponse, 'info');
            if($info == "Error") {
                $message = read_TagName($reponse, 'message');
                $err .= $message;
                $err_flag = true;
            } elseif($info == "OK") {
                $giftcode = read_TagName($reponse, 'giftcode');
                if(strlen($giftcode) == 0) {
                    $err .= "Du lieu tra ve loi. Vui long lien he Admin de FIX";
                    $err_flag = true;
                }
            } else {
                $err .= "Ket noi API gap su co. Admin MU vui long lien he nha cung cap DWebMu de kiem tra";
                $err_flag = true;
            }
        }
    }
    
    $msg = "";
    if($err_flag === false) {
        if($gift_slg == 1) {
            $giftcode_insert_query = "INSERT INTO GiftCode (gift_code, type, giftcode_type, gift_time, ngay, status) VALUES ('$giftcode', 5, $giftcode_type, $timestamp, '".date("Y-m-d",$timestamp)."', 1)";
            $giftcode_insert_result = $db->Execute($giftcode_insert_query);
                check_queryerror($giftcode_insert_query, $giftcode_insert_result);
            $msg .= "GiftCode : <strong>$giftcode</strong> <br />";
        } else {
            $giftcode_arr = json_decode($giftcode, true);
            foreach($giftcode_arr as $giftcode) {
                $giftcode_insert_query = "INSERT INTO GiftCode (gift_code, type, giftcode_type, gift_time, ngay, status) VALUES ('$giftcode', 5, $giftcode_type, $timestamp, '".date("Y-m-d",$timestamp)."', 1)";
                $giftcode_insert_result = $db->Execute($giftcode_insert_query);
                    check_queryerror($giftcode_insert_query, $giftcode_insert_result);
                $msg .= "GiftCode : <strong>$giftcode</strong> <br />";
            }
        }
    }
    
    if($err_flag === false) {
        echo $msg;
    }
    if($err_flag === true) {
        echo "<strong>Lỗi</strong> : <br />" . $err;
    } 
}

$giftphat_unused_arr = array();
$giftphatunuse_q = "SELECT gift_code, giftcode_type FROM GiftCode WHERE type=5 AND status=1 ORDER BY giftcode_type";
$giftphatunuse_r = $db->Execute($giftphatunuse_q);
    check_queryerror($giftphatunuse_q, $giftphatunuse_r);
while($giftphatunuse_f = $giftphatunuse_r->FetchRow()) {
    $gift_type_name = $giftcode_type_arr[$giftphatunuse_f[1]]['name'];
    $giftphat_unused_arr[] = array(
        'gift_code' =>  $giftphatunuse_f[0],
        'gift_type_name'    =>  $gift_type_name
    );
}

if(count($giftphat_unused_arr) > 0) {
    echo "<hr />";
    echo "<strong>Giftcode phát chưa sử dụng</strong><br /><br />";
    echo "<table border='1' cellpadding='5'><tr><td align='center'><strong>Loại GiftCode</strong></td><td align='center'><strong>GiftCode</strong></td></tr>";
    foreach($giftphat_unused_arr as $k => $v) {
        echo "<tr><td align='left'>". $v['gift_type_name'] ."</td><td align='right'>". $v['gift_code'] ."</td></tr>";
    }
    
    echo "</table>";
}

?>

</body>
</html>
<?php
$db->Close();
?>