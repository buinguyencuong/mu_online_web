<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/

/**
 * @author NetBanBe
 * @copyright 2012
 */
session_start();

include_once ("security.php");
include ('../config.php');

echo "Đã xóa : " . $_SESSION['dupe_del'] . " / " . $_SESSION['dupe_total'] . "<hr />";

if($_SESSION['del_finish'] == "unfinish") {
    $noitem = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
    
    $info_dupe_query = "SELECT acc, name, seri, slgitem FROM Dupe_Scan WHERE isdupe=1 ORDER BY acc, name";
    $info_dupe_result = $db->SelectLimit($info_dupe_query, 1, $_SESSION['dupe_del']);
    $info_dupe_fetch = $info_dupe_result->FetchRow();
    
    $acc = $info_dupe_fetch[0];
    $name = $info_dupe_fetch[1];
    $seri_dupe = $info_dupe_fetch[2];
    $slgitem = $info_dupe_fetch[3];
    
    // Neu la nhan vat
    if(strlen($name) > 0) {
        $inventory_query = "SELECT CAST(Inventory AS image) FROM Character WHERE Name='$name'";
        $inventory_result = $db->Execute($inventory_query);
        $inventory_fetch = $inventory_result->FetchRow();
        $inventory = $inventory_fetch[0];
        $inventory = bin2hex($inventory);
        $inventory = strtoupper($inventory);
    
        echo "Đang Xóa đồ Dupe Nhân vật : <strong>$name</strong> của tài khoản <strong>$acc</strong> <br />";
        
        $item_total = floor(strlen($inventory)/32);
        $inventory_new = "";
        for($i=0; $i<$item_total; $i++) {
            $item = substr($inventory,$i*32, 32);
            $seri = substr($item, 6, 8);
            if($seri == $seri_dupe) {
                $inventory_new .= $noitem;
            } else {
                $inventory_new .= $item;
            }
        }
        
        $iventory_update_query = "UPDATE Character SET Inventory=0x$inventory_new WHERE Name='$name'";
        $iventory_update_result = $db->Execute($iventory_update_query);
    }
    // Neu la hom do
    else {
        $warehouse_query = "SELECT CAST(Items AS image) FROM warehouse WHERE AccountID='$acc'";
        $warehouse_result = $db->Execute($warehouse_query);
        $warehouse_fetch = $warehouse_result->FetchRow();
        $warehouse = $warehouse_fetch[0];
        $warehouse = bin2hex($warehouse);
        $warehouse = strtoupper($warehouse);
    
        echo "Đang xóa Item Dupe trong hòm đồ tài khoản : <strong>$acc</strong> <br />";
        
        $item_total = floor(strlen($warehouse)/32);
        $warehouse_new = "";
        for($i=0; $i<$item_total; $i++) {
            $item = substr($warehouse,$i*32, 32);
            $seri = substr($item, 6, 8);
            if($seri == $seri_dupe) {
                $warehouse_new .= $noitem;
            } else {
                $warehouse_new .= $item;
            }
        }
        $warehouse_update_query = "UPDATE warehouse SET Items=0x$warehouse_new WHERE AccountID='$acc'";
        $warehouse_update_result = $db->Execute($warehouse_update_query);
    }
    
    $_SESSION['dupe_del']++;
    if($_SESSION['dupe_del'] >= $_SESSION['dupe_total']) $_SESSION['del_finish'] = "finish";
} else {
    echo "<strong>Đã Xóa đồ Dupe hoàn tất</strong>.";
}
$db->Close();
?>