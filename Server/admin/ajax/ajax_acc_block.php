<?php

/**
 * @author NetBanBe
 * @copyright 2012
 */

// Define Ajax Request
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
if(!IS_AJAX) { echo 'Khong duoc phep truy cap.'; }
else {
    if($_POST['acc'])
	{
        $block_type = abs(intval($_POST['block_type']));
        $acc = $_POST['acc'];
        $day_block = abs(intval($_POST['day_block']));
        $admin = $_POST['admin'];
        $block_des = $_POST['block_des'];
        
        $error = false;
        
        if(empty($acc)) {
            $notice .= "Chưa nhập tài khoản.\n";
            $error = true;
        }
        
        if($block_type == 1) {
            if(strlen($day_block) == 0) {
                $notice .= "Chưa nhập ngày Block.\n";
                $error = true;
            }
                    
            if(!is_numeric($day_block)) {
                $notice .= "Ngày Block không phải dạng số.\n";
                $error = true;
            }
            
            if(intval($day_block) <= 0) {
                $notice .= "Ngày Block phải lớn hơn 0.\n";
                $error = true;
            }
        }
        
        if(empty($admin)) {
            $notice .= "Chưa nhập Nick Admin.\n";
            $error = true;
        }
        
        if(empty($block_des)) {
            $notice .= "Chưa nhập Lý do Block.\n";
            $error = true;
        }
        
        
        if($error == false) {
            switch ($block_type){ 
                case 0:
                    $data_update_q = "UPDATE MEMB_INFO SET admin_block=0, bloc_code='0', BlockTime=0 WHERE memb___id='$acc'";
                    $extra_msg = "Đã giải Block $acc";
                    
                    $log_update_q = "INSERT INTO nbb_block (admin, acc, block_type, block_des, block_time_begin) VALUES ('". $admin ."', '". $acc ."', 0, '". $block_des ."', ". $timestamp .")";
            	break;
                
                case 1:
                    $blocktimer = $timestamp + $day_block*24*60*60;
                    $data_update_q = "UPDATE MEMB_INFO SET admin_block=1, bloc_code='1', BlockTime=$blocktimer WHERE memb___id='$acc'";
                    $block_date = date('H:i d/m/Y', $blocktimer);
                    $extra_msg = "Đã Block $acc trong $day_block ngày (". $block_date .")|". $block_date ."|";
                    
                    $log_update_q = "INSERT INTO nbb_block (admin, acc, block_type, block_des, block_time_begin, day_block, block_time_end) VALUES ('". $admin ."', '". $acc ."', 1, '". $block_des ."', ". $timestamp .", ". $day_block .", ". $blocktimer .")";
            	break;
                
                default:
                    $type_name = "";
                    $data_update_q = "NOT Block Type $block_type";
            }
            
            $data_update_r = $db->Execute($data_update_q);
                check_queryerror($data_update_q, $data_update_r);
                
            $log_update_r = $db->Execute($log_update_q);
                check_queryerror($log_update_q, $log_update_r);
            
            $notice = "|OK|". $extra_msg ."|";
        }
        
        echo $notice;
	}
}

?>