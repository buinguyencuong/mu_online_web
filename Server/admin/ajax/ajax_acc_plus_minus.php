<?php

/**
 * @author NetBanBe
 * @copyright 2012
 */

// Define Ajax Request
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
if(!IS_AJAX) { echo 'Khong duoc phep truy cap.'; }
else {
    if($_POST['acc'])
	{
        $datatype = $_POST['datatype'];
        $acc = $_POST['acc'];
        $plusminus = abs(intval($_POST['plusminus']));
        $value = $_POST['value'];
        
        $error = false;
        
        if(empty($acc)) {
            $notice .= "Chưa nhập tài khoản.<br />";
            $error = true;
        }
        if(strlen($value) == 0) {
            $notice .= "Chưa nhập dữ liệu cần tăng giảm.\n";
            $error = true;
        }
                
        if(!is_numeric($value)) {
            $notice .= "Giá trị tăng giảm không phải dạng số.\n";
            $error = true;
        }
        
        if(intval($value) <= 0) {
            $notice .= "Giá trị tăng giảm phải lớn hơn 0.\n";
            $error = true;
        }
        
        if(isset($plusminus) && $plusminus == 1) {
            $value = -abs(intval($value));
        } else {
            $value = abs(intval($value));
        }
        
        if($error == false) {
            switch ($datatype){ 
                case 'gcoin':
                    $type_name = "Gcent";
                    $data_update_q = "UPDATE MEMB_INFO SET gcoin=gcoin + $value WHERE memb___id='$acc'";
            	break;
                
                case 'gcoin_km':
                    $type_name = "Gcent Khuyến Mãi";
                    $data_update_q = "UPDATE MEMB_INFO SET gcoin_km=gcoin_km + $value WHERE memb___id='$acc'";
            	break;
                
                case 'vpoint':
                    $type_name = "Vcent";
                    $data_update_q = "UPDATE MEMB_INFO SET vpoint=vpoint + $value WHERE memb___id='$acc'";
            	break;
                
                case 'vpoint_km':
                    $type_name = "Vcent Event";
                    $data_update_q = "UPDATE MEMB_INFO SET vpoint_km=vpoint_km + $value WHERE memb___id='$acc'";
            	break;
                
                case 'bank_zen':
                    $type_name = "ZEN ngân hàng";
                    $data_update_q = "UPDATE MEMB_INFO SET bank=bank + $value WHERE memb___id='$acc'";
            	break;
                
                case 'jewel_chao':
                    $type_name = "Ngọc Hỗn Nguyên ngân hàng";
                    $data_update_q = "UPDATE MEMB_INFO SET jewel_chao=jewel_chao + $value WHERE memb___id='$acc'";
            	break;
                
                case 'jewel_cre':
                    $type_name = "Ngọc Sáng Tạo ngân hàng";
                    $data_update_q = "UPDATE MEMB_INFO SET jewel_cre=jewel_cre + $value WHERE memb___id='$acc'";
            	break;
                
                case 'jewel_blue':
                    $type_name = "Lông Chim ngân hàng";
                    $data_update_q = "UPDATE MEMB_INFO SET jewel_blue=jewel_blue + $value WHERE memb___id='$acc'";
            	break;
                
                case 'SCFVipMoney':
                    $type_name = "SCFVipMoney";
                    $data_update_q = "UPDATE MEMB_INFO SET SCFVipMoney=SCFVipMoney + $value WHERE memb___id='$acc'";
            	break;
                
                case 'wcoin':
                    $type_name = "WCoin";
                    $data_update_q = "UPDATE MEMB_INFO SET WCoin=WCoin + $value WHERE memb___id='$acc'";
            	break;
                
                case 'nbb_pl':
                    $type_name = "Điểm PPoint";
                    $data_update_q = "UPDATE MEMB_INFO SET nbb_pl=nbb_pl + $value WHERE memb___id='$acc'";
            	break;
                
                case 'nbb_pl_extra':
                    $type_name = "Điểm PPoint+";
                    $data_update_q = "UPDATE MEMB_INFO SET nbb_pl_extra=nbb_pl_extra + $value WHERE memb___id='$acc'";
            	break;
                
                case 'jewel_heart':
                    $type_name = "Trái Tim ngân hàng";
                    $data_update_q = "UPDATE MEMB_INFO SET jewel_heart=jewel_heart + $value WHERE memb___id='$acc'";
            	break;
                
                default:
                    $type_name = "";
                    $data_update_q = "NOT FUNCTION $datatype";
            }
            
            $data_update_r = $db->Execute($data_update_q);
                check_queryerror($data_update_q, $data_update_r);
            
            if($value > 0) {
                $extra_msg = "Đã tăng <font color='red'><strong>";
            } else {
                $extra_msg = "Đã giảm <font color='red'><strong>";
            }
            $extra_msg .= abs($value);
            $extra_msg .= " $type_name </strong></font> của tài khoản <font color='red'><strong>$acc</strong></font>";
            
            $notice = "|OK|". $extra_msg ."|";
        }
        
        echo $notice;
	}
}

?>