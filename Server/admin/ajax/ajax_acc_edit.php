<?php

/**
 * @author NetBanBe
 * @copyright 2012
 */

// Define Ajax Request
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
if(!IS_AJAX) { echo 'Khong duoc phep truy cap.'; }
else {
    if($_POST['acc'])
	{
        $datatype = $_POST['datatype'];
        $acc = $_POST['acc'];
        $data_new = $_POST['dataedit_new'];
        $data_old = $_POST['dataedit_old'];
        
        $error = false;
        
        if(empty($acc)) {
            $notice .= "Chưa nhập tài khoản.<br />";
            $error = true;
        }
        if(strlen($data_new) == 0) {
            $notice .= "Chưa nhập dữ liệu cần thay đổi.\n";
            $error = true;
        }
                
        switch ($datatype){ 
        	case 'passgame':
                if(strlen($data_new) < 4 || strlen($data_new) > 10) {
                    $notice .= "Mật khẩu '$data_new' phải từ 4-10 ký tự.\n";
                    $error = true;
                }
                
            case 'pass1':
            case 'pass2':
                if ( !preg_match("/^[a-zA-Z0-9_]+$/", $data_new) )
            	{
                    $notice .= "Mật khẩu '$data_new' chỉ được bao gồm ký tự a-z, A-Z, 0-9.\n";
                    $error = true;
            	}
        	break;
            
            case 'gcoin':
            case 'gcoinkm':
            case 'vpoint':
            case 'vpoint_km':
            case 'bank_zen':
            case 'bank_chao':
            case 'bank_cre':
            case 'bank_blue':
            case 'quest':
            case 'SCFVipMoney':
            case 'WCoin':
            case 'nbb_pl':
            case 'nbb_pl_extra':
            case 'jewel_heart':
            case 'block':
                if ( !preg_match("/^[0-9]+$/", $data_new) )
            	{
                    $notice .= "Dữ liệu '$data_new' phải là số 0-9.\n";
                    $error = true;
            	}
                
                if ($data_new < 0) {
                    $notice .= "Dữ liệu '$data_new' phải là số >= 0.\n";
                    $error = true;
                }
                
                
            break;
        
        }
        
        if($error == false) {
            switch ($datatype) { 
        	   case 'block':
                    if($data_new == 1) {
                        $blocktime = abs(intval($_POST['blocktime']));
                        if($blocktime <1) {
                            $notice .= "Dữ liệu Số ngày Block '$blocktime' phải là số > 0.\n";
                            $error = true;
                        }
                    }
               break;
               
            }
        }
        
        if($error == false) {
            $extra_msg = "";
            
            switch ($datatype){ 
            	case 'passgame':
                    if($server_md5 == 1) {
                        $data_update_q = "UPDATE MEMB_INFO SET memb__pwd=[dbo].[fn_md5]('$data_new','$acc') WHERE memb___id='$acc'";
                    } else {
                        $data_update_q = "UPDATE MEMB_INFO SET memb__pwd='$data_new' WHERE memb___id='$acc'";
                    }
                break;
                
                case 'pass1':
                    $data_new_md5 = md5($data_new);
                    $data_update_q = "UPDATE MEMB_INFO SET memb__pwd2='$data_new', memb__pwdmd5='$data_new_md5' WHERE memb___id='$acc'";
                break;
                
                case 'pass2':
                    $data_update_q = "UPDATE MEMB_INFO SET pass2='$data_new' WHERE memb___id='$acc'";
            	break;
                
                case 'tel':
                    $data_update_q = "UPDATE MEMB_INFO SET tel__numb='$data_new' WHERE memb___id='$acc'";
            	break;
                
                case 'mail':
                    $data_update_q = "UPDATE MEMB_INFO SET mail_addr='$data_new' WHERE memb___id='$acc'";
            	break;
                
                case 'quest':
                    $data_update_q = "UPDATE MEMB_INFO SET fpas_ques='$data_new' WHERE memb___id='$acc'";
            	break;
                
                case 'ans':
                    $data_update_q = "UPDATE MEMB_INFO SET fpas_answ='$data_new' WHERE memb___id='$acc'";
            	break;
                
                case 'gcoin':
                    $data_update_q = "UPDATE MEMB_INFO SET gcoin='$data_new' WHERE memb___id='$acc'";
            	break;
                
                case 'gcoinkm':
                    $data_update_q = "UPDATE MEMB_INFO SET gcoin_km='$data_new' WHERE memb___id='$acc'";
            	break;
                
                case 'vpoint':
                    $data_update_q = "UPDATE MEMB_INFO SET vpoint='$data_new' WHERE memb___id='$acc'";
            	break;
                
                case 'vpoint_km':
                    $data_update_q = "UPDATE MEMB_INFO SET vpoint_km='$data_new' WHERE memb___id='$acc'";
            	break;
                
                case 'bank_zen':
                    $data_update_q = "UPDATE MEMB_INFO SET bank='$data_new' WHERE memb___id='$acc'";
            	break;
                
                case 'bank_chao':
                    $data_update_q = "UPDATE MEMB_INFO SET jewel_chao='$data_new' WHERE memb___id='$acc'";
            	break;
                
                case 'bank_cre':
                    $data_update_q = "UPDATE MEMB_INFO SET jewel_cre='$data_new' WHERE memb___id='$acc'";
            	break;
                
                case 'bank_blue':
                    $data_update_q = "UPDATE MEMB_INFO SET jewel_blue='$data_new' WHERE memb___id='$acc'";
            	break;
                
                case 'SCFVipMoney':
                    $data_update_q = "UPDATE MEMB_INFO SET SCFVipMoney='$data_new' WHERE memb___id='$acc'";
            	break;
                
                case 'WCoin':
                    $data_update_q = "UPDATE MEMB_INFO SET WCoin='$data_new' WHERE memb___id='$acc'";
            	break;
                
                case 'nbb_pl':
                    $data_update_q = "UPDATE MEMB_INFO SET nbb_pl='$data_new' WHERE memb___id='$acc'";
            	break;
                
                case 'nbb_pl_extra':
                    $data_update_q = "UPDATE MEMB_INFO SET nbb_pl_extra='$data_new' WHERE memb___id='$acc'";
            	break;
                
                case 'jewel_heart':
                    $data_update_q = "UPDATE MEMB_INFO SET jewel_heart='$data_new' WHERE memb___id='$acc'";
            	break;
                
                case 'block':
                    if($data_new == 1) {
                        $blocktime = abs(intval($_POST['blocktime']));
                        $blocktimer = time() + $blocktime*24*60*60;
                        $data_update_q = "UPDATE MEMB_INFO SET admin_block=1, bloc_code='$data_new', BlockTime=$blocktimer WHERE memb___id='$acc'";
                        $extra_msg = date('H:i d/m/Y', $blocktimer) ."|";
                    } else {
                        $data_update_q = "UPDATE MEMB_INFO SET admin_block=0, bloc_code='$data_new', BlockTime=0 WHERE memb___id='$acc'";
                    }
                    
            	break;
                
                default:
                    $data_update_q = "NOT FUNCTION";
            }
            
            $data_update_r = $db->Execute($data_update_q);
                check_queryerror($data_update_q, $data_update_r);
            
            $notice = "|OK|". $extra_msg;
        }
        
        echo $notice;
	}
}

?>