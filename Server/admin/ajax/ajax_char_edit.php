<?php

/**
 * @author NetBanBe
 * @copyright 2012
 */

// Define Ajax Request
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
if(!IS_AJAX) { echo 'Khong duoc phep truy cap.'; }
else {
    if($_POST['charname'])
	{
        $datatype = $_POST['datatype'];
        $charname = $_POST['charname'];
        $data_new = $_POST['dataedit_new'];
        $data_old = $_POST['dataedit_old'];
        
        $error = false;
        
        if(empty($charname)) {
            $notice .= "Chưa nhập nhân vật.<br />";
            $error = true;
        }
        if(strlen($data_new) == 0) {
            $notice .= "Chưa nhập dữ liệu cần thay đổi.\n";
            $error = true;
        }
                
        switch ($datatype){ 
            case 'pass1':
                if ( !preg_match("/^[a-zA-Z0-9_]+$/", $data_new) )
            	{
                    $notice .= "Mật khẩu '$data_new' chỉ được bao gồm ký tự a-z, A-Z, 0-9.\n";
                    $error = true;
            	}
        	break;
            
            case 'level':
            case 'rs':
            case 'rl':
            case 'point':
            case 'pointdutru':
            case 'pointevent':
            case 'nbbtuluyen_str_point':
            case 'nbbtuluyen_agi_point':
            case 'nbbtuluyen_vit_point':
            case 'nbbtuluyen_ene_point':
            case 'nbbtuluyen_str':
            case 'nbbtuluyen_agi':
            case 'nbbtuluyen_vit':
            case 'nbbtuluyen_ene':
            case 'nbbtuluyen_point':
            case 'class':
            case 'stat':
            case 'scfmaster_level':
            case 'scfmaster_point':
            case 'pointuythac':
            case 'PointUyThac_Event':
            case 'str':
            case 'dex':
            case 'vit':
            case 'ene':
            case 'leadership':
            case 'money':
            case 'pklevel':
            case 'pkcount':
            case 'khoado':
            case 'makhoado':
            case 'SCFPCPoints':
            case 'SCFSealItem':
            case 'SCFSealTime':
            case 'SCFScrollItem':
            case 'SCFScrollTime':
            case 'nbbsongtu_lv':
            case 'nbbsongtu_point':
            case 'nbbhoanhao_point':
            case 'nbbcuonghoa_point':
            case 'nbb_pointmaster':
                if ( !preg_match("/^[0-9]+$/", $data_new) )
            	{
                    $notice .= "Dữ liệu '$data_new' phải là số 0-9.\n";
                    $error = true;
            	}
                
                if ($data_new < 0) {
                    $notice .= "Dữ liệu '$data_new' phải là số >= 0.\n";
                    $error = true;
                }
            break;
        
        }
        
        switch ($datatype){ 
            
            case 'str':
            case 'dex':
            case 'vit':
            case 'ene':
            case 'leadership':
                    if ($data_new > 65000) {
                        $notice .= "Dữ liệu '$data_new' không được vượt 65000.\n";
                        $error = true;
                    }
                break;
            case 'money':
                    if ($data_new > 2000000000) {
                        $notice .= "Dữ liệu '$data_new' không được vượt 2 tỷ.\n";
                        $error = true;
                    }
                break;
        
        }
        
        if($error == false) {
            switch ($datatype) {
                
                case 'level':
                        $data_update_q = "UPDATE Character SET cLevel='$data_new' WHERE Name='$charname'";
                    break;
                case 'rs':
                        $data_update_q = "UPDATE Character SET Resets='$data_new' WHERE Name='$charname'";
                    break;
                case 'rl':
                        $data_update_q = "UPDATE Character SET Relifes='$data_new' WHERE Name='$charname'";
                    break;
                case 'point':
                        $data_update_q = "UPDATE Character SET LevelUpPoint='$data_new' WHERE Name='$charname'";
                    break;
                case 'pointdutru':
                        $data_update_q = "UPDATE Character SET pointdutru='$data_new' WHERE Name='$charname'";
                    break;
                case 'pointevent':
                        $data_update_q = "UPDATE Character SET point_event='$data_new' WHERE Name='$charname'";
                    break;
                case 'nbbtuluyen_str':
                        $data_update_q = "UPDATE Character SET nbbtuluyen_str='$data_new' WHERE Name='$charname'";
                    break;
                case 'nbbtuluyen_str_point':
                        $data_update_q = "UPDATE Character SET nbbtuluyen_str_point='$data_new' WHERE Name='$charname'";
                    break;
                case 'nbbtuluyen_agi':
                        $data_update_q = "UPDATE Character SET nbbtuluyen_agi='$data_new' WHERE Name='$charname'";
                    break;
                case 'nbbtuluyen_agi_point':
                        $data_update_q = "UPDATE Character SET nbbtuluyen_agi_point='$data_new' WHERE Name='$charname'";
                    break;
                case 'nbbtuluyen_vit':
                        $data_update_q = "UPDATE Character SET nbbtuluyen_vit='$data_new' WHERE Name='$charname'";
                    break;
                case 'nbbtuluyen_vit_point':
                        $data_update_q = "UPDATE Character SET nbbtuluyen_vit_point='$data_new' WHERE Name='$charname'";
                    break;
                case 'nbbtuluyen_ene':
                        $data_update_q = "UPDATE Character SET nbbtuluyen_ene='$data_new' WHERE Name='$charname'";
                    break;
                case 'nbbtuluyen_ene_point':
                        $data_update_q = "UPDATE Character SET nbbtuluyen_ene_point='$data_new' WHERE Name='$charname'";
                    break;
                case 'nbbtuluyen_point':
                        $data_update_q = "UPDATE Character SET nbbtuluyen_point='$data_new' WHERE Name='$charname'";
                    break;
                case 'class':
                        $quest_query = "";
                        switch ($data_new){ 
                        	case 2:
                            case 3:
                                $quest_query = ", Quest=0xEAEAFFFF14141401FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                        	break;
                        
                        	case 18:
                            case 19:
                                $quest_query = ", Quest=0xAAEAFFFF14141401FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                        	break;
                        
                        	case 34:
                            case 35:
                                $quest_query = ", Quest=0xEAEAFFFF14141401FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                        	break;
                            
                            case 49:
                            case 50:
                            case 51:
                                $quest_query = ", Quest=0xFFEAFFFF14141401FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                        	break;
                            
                            case 65:
                            case 66:
                            case 67:
                                $quest_query = ", Quest=0xFFEAFFFF14141401FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                        	break;
                            
                            case 82:
                            case 83:
                                $quest_query = ", Quest=0xEAEAFFFF14141401FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                        	break;
                            
                            case 97:
                            case 98:
                                $quest_query = ", Quest=0xFFEAFFFF14141401FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                        	break;
                        }
                        $data_update_q = "UPDATE Character SET Class='$data_new' $quest_query WHERE Name='$charname'";
                    break;
                case 'stat':
                        switch ($data_new){ 
                        
                        	case 32:
                                $data_update_q = "UPDATE Character SET CtlCode=32, ErrorSubBlock=0, uythacoffline_stat=0 WHERE Name='$charname'";
                        	break;
                        
                        	case 99:
                                $data_update_q = "UPDATE Character SET CtlCode=99, ErrorSubBlock=99, uythacoffline_stat=0 WHERE Name='$charname'";
                        	break;
                        
                        	default :
                            $data_update_q = "UPDATE Character SET CtlCode=0, ErrorSubBlock=0, uythacoffline_stat=0 WHERE Name='$charname'";
                        }
                    break;
                case 'scfmaster_level':
                        $data_update_q = "UPDATE Character SET SCFMasterLevel='$data_new' WHERE Name='$charname'";
                    break;
                case 'scfmaster_point':
                        $data_update_q = "UPDATE Character SET SCFMasterPoints='$data_new' WHERE Name='$charname'";
                    break;
                case 'pointuythac':
                        $data_update_q = "UPDATE Character SET PointUyThac='$data_new' WHERE Name='$charname'";
                    break;
                case 'PointUyThac_Event':
                        $data_update_q = "UPDATE Character SET PointUyThac_Event='$data_new' WHERE Name='$charname'";
                    break;
                case 'str':
                        $data_update_q = "UPDATE Character SET Strength='$data_new' WHERE Name='$charname'";
                    break;
                case 'dex':
                        $data_update_q = "UPDATE Character SET Dexterity='$data_new' WHERE Name='$charname'";
                    break;
                case 'vit':
                        $data_update_q = "UPDATE Character SET Vitality='$data_new' WHERE Name='$charname'";
                    break;
                case 'ene':
                        $data_update_q = "UPDATE Character SET Energy='$data_new' WHERE Name='$charname'";
                    break;
                case 'leadership':
                        $data_update_q = "UPDATE Character SET Leadership='$data_new' WHERE Name='$charname'";
                    break;
                case 'money':
                        $data_update_q = "UPDATE Character SET Money='$data_new' WHERE Name='$charname'";
                    break;
                case 'pklevel':
                        $data_update_q = "UPDATE Character SET PkLevel='$data_new' WHERE Name='$charname'";
                    break;
                case 'pkcount':
                        $data_update_q = "UPDATE Character SET PkCount='$data_new' WHERE Name='$charname'";
                    break;
                case 'khoado':
                        $data_update_q = "UPDATE Character SET khoado='$data_new' WHERE Name='$charname'";
                    break;
                case 'makhoado':
                        $data_update_q = "UPDATE Character SET makhoado='$data_new' WHERE Name='$charname'";
                    break;
                case 'SCFPCPoints':
                        $data_update_q = "UPDATE Character SET SCFPCPoints='$data_new' WHERE Name='$charname'";
                    break;
                case 'SCFSealItem':
                        $data_update_q = "UPDATE Character SET SCFSealItem='$data_new' WHERE Name='$charname'";
                    break;
                case 'SCFSealTime':
                        $data_update_q = "UPDATE Character SET SCFSealTime='$data_new' WHERE Name='$charname'";
                    break;
                case 'SCFScrollItem':
                        $data_update_q = "UPDATE Character SET SCFScrollItem='$data_new' WHERE Name='$charname'";
                    break;
                case 'SCFScrollTime':
                        $data_update_q = "UPDATE Character SET SCFScrollTime='$data_new' WHERE Name='$charname'";
                    break;
                case 'nbbsongtu_lv':
                        $data_update_q = "UPDATE Character SET nbbsongtu_lv='$data_new' WHERE Name='$charname'";
                    break;
                case 'nbbsongtu_point':
                        $data_update_q = "UPDATE Character SET nbbsongtu_point='$data_new' WHERE Name='$charname'";
                    break;
                case 'nbbhoanhao_point':
                        $data_update_q = "UPDATE Character SET nbbhoanhao_point='$data_new' WHERE Name='$charname'";
                    break;
                case 'nbbcuonghoa_point':
                        $data_update_q = "UPDATE Character SET nbbcuonghoa_point='$data_new' WHERE Name='$charname'";
                    break;
                case 'nbb_pmaster':
                        $data_update_q = "UPDATE Character SET nbb_pmaster='$data_new' WHERE Name='$charname'";
                    break;
                default:
                    $data_update_q = "NOT FUNCTION";
            }
            
            $data_update_r = $db->Execute($data_update_q);
                check_queryerror($data_update_q, $data_update_r);
            
            $notice = "OK";
        }
        
        echo $notice;
	}
}

?>