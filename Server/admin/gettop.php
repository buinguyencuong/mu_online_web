<?php

/**
 * @author NetBanBe
 * @copyright 2015
 */

date_default_timezone_set('Asia/Ho_Chi_Minh');

include("../adodb/adodb.inc.php");


function check_queryerror($query,$result) {
    if ($result === false) die("Query Error : $query");
}

// Connect DB
$db = &ADONewConnection('mssql');
$connect_mssql = $db->Connect('localhost','sa','1234','MuOnline');
if (!$connect_mssql) die("Loi! Khong the ket noi SQL Server : 'MuOnline'");

$class_dw_query = " AND (Class = 0 OR Class = 1 OR Class = 2 OR Class = 3) ";
$class_dk_query = " AND (Class = 16 OR Class = 17 OR Class = 18 OR Class = 19) ";
$class_elf_query = " AND (Class = 32 OR Class = 33 OR Class = 34 OR Class = 35) ";
$class_mg_query = " AND (Class = 48 OR Class = 49  OR Class = 50) ";
$class_dl_query = " AND (Class = 64 OR Class = 65  OR Class = 66) ";
$class_sum_query = " AND (Class = 80 OR Class = 81 OR Class = 82 OR Class = 83) ";
$class_rf_query = " AND (Class = 96 OR Class = 97 OR Class = 98) ";
$class_query = "";

$char_top_arr = array();
$char_top_q = "SELECT A.name FROM TopReset A JOIN Character B ON A.name=B.Name $class_elf_query GROUP BY A.name HAVING SUM(reset_pri_top) = 60";
$char_top_r = $db->Execute($char_top_q);
    check_queryerror($char_top_q, $char_top_r);
while($char_top_f = $char_top_r->FetchRow()) {
    $char_top_arr[] = $char_top_f[0];
}

// Connect DB Log
$db = &ADONewConnection('mssql');
$connect_mssql = $db->Connect('localhost','sa','1234','MuOnline_Log');
if (!$connect_mssql) die("Loi! Khong the ket noi SQL Server : 'MuOnline_Log'");

$top_arr = array();
foreach($char_top_arr as $char_top_name) {
    $char_top_time_q = "SELECT TOP 1 time FROM (SELECT TOP 20 time FROM Log_TienTe WHERE log_type=1 AND Des like '%\"name\":\"". $char_top_name ."\"%' AND time>1433606400 ORDER BY time) as a ORDER BY time DESC";
    $char_top_time_r = $db->Execute($char_top_time_q);
        check_queryerror($char_top_time_q, $char_top_time_r);
    $char_top_time_f = $char_top_time_r->FetchRow();
    
    $char_top_time = $char_top_time_f[0];
    
    $top_arr[$char_top_time] = $char_top_name;
}

ksort($top_arr);

$count = 0;
foreach($top_arr as $k => $v) {
    $count++;
    echo "$v : ". date('d/m/Y H :i :s', $k) ."<br />";
    if($count == 3) break;
}

echo "<hr />";
// Char
$char = 'llll';
$char_rs_q = "SELECT TOP 20 acc, time, CAST(Des as text) FROM Log_TienTe WHERE log_type=1 AND Des like '%\"name\":\"{$char}\"%' AND time>1433606400 ORDER BY time";
$char_rs_r = $db->Execute($char_rs_q); 
    check_queryerror($char_rs_q, $char_rs_r);

$char_rs_begin = 40;
while($char_rs_f = $char_rs_r->FetchRow()) {
    //if($char_rs_begin == 40) echo $char_rs_f[2];
    $char_rs_begin++;
    echo $char ." RS $char_rs_begin : ".  date('d/m/Y H :i :s', $char_rs_f[1]) ."<br />";
}

?>