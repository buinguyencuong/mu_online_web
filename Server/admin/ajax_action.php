<?php

/**
 * @author NetBanBe
 * @copyright 16/3/2011
 */

session_start();
include_once ("security.php");
include ('../config.php');
include ('function.php');
include ('../config/config_thehe.php');

if( isset($_GET['ajax']) )
{
	if ( preg_match("/^[a-zA-Z0-9_]+$/", $_GET['ajax']) )
	{
        if (is_file("ajax/ajax_". $_GET['ajax'] .".php")) 
		{
            include("ajax/ajax_". $_GET['ajax'] .".php");
		}
        else echo "Not File Ajax ". $_GET['ajax'];
	}
    else echo $_GET['ajax'] . " : Parameter Ajax Don't Allow";
}
?>