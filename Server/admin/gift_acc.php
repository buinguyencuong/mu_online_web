<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
include_once ("security.php");
include ('../config.php');
include ('function.php');

$file_giftcode_type = '../config/giftcode_type.txt';

$title = "Tạo GiftCode Tài Khoản";
session_start();

if(is_file($file_giftcode_type)) {
	$fopen_host = fopen($file_giftcode_type, "r");
    $giftcode_type_read = fgets($fopen_host);
    
    $giftcode_type_arr = json_decode($giftcode_type_read, true);
} else $fopen_host = fopen($file_giftcode_type, "w");
fclose($fopen_host);

if(!is_array($giftcode_type_arr)) $giftcode_type_arr = array();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<title><?php echo $title; ?></title>
<meta http-equiv=Content-Type content="text/html; charset=utf-8" />
</head>
<body>
<?php require('linktop.php'); ?>
<form action="" name="frm_giftacc" id="frm_giftacc" method="POST">
    <center>
    <strong>Tài khoản</strong> <i>(Các tài khoản cách nhau dấu ",")</i> : <input name="acc_list" id="acc_list" value="<?php if($_POST['acc_list']) echo $_POST['acc_list']; ?>" size="50" /> 
    <select name='giftcode_type'>";
    <?php
        if(is_array($giftcode_type_arr)) {
            foreach($giftcode_type_arr as $k => $v) {
                echo "<option value='$k' ";
                if($_POST['giftcode_type'] == $k) echo "selected";
                echo " >". $v['name'] ."</option>";
            }
        }
    ?>
    </select> 
    Slg Max Used : <input name="acc_gift_max" id="acc_gift_max" value="<?php if($_POST['acc_gift_max']) echo $_POST['acc_gift_max']; else echo 0; ?>" size="4" /> 
    <input type="submit" name="submit_frm_giftacc" value="Tạo GiftCode Tài khoản" />
</form>
<br /><br />
<form action="" name="frm_giftnv" id="frm_giftnv" method="POST">
    <center>
    <strong>Nhân vật</strong> <i>(Các nhân vật cách nhau dấu ",")</i> : <input name="nv_list" id="nv_list" value="<?php if($_POST['nv_list']) echo $_POST['nv_list']; ?>" size="50" /> 
    <select name='giftcode_type'>";
    <?php
        if(is_array($giftcode_type_arr)) {
            foreach($giftcode_type_arr as $k => $v) {
                echo "<option value='$k' ";
                if($_POST['giftcode_type'] == $k) echo "selected";
                echo " >". $v['name'] ."</option>";
            }
        }
    ?>
    </select>
    Slg Max Used : <input name="char_gift_max" id="char_gift_max" value="<?php if($_POST['char_gift_max']) echo $_POST['char_gift_max']; else echo 0; ?>" size="4" />
    <input type="submit" name="submit_frm_giftnv" value="Tạo GiftCode Nhân vật" />
</form>
<?php

if($_POST['acc_list']) {
    $giftcode_type = $_POST['giftcode_type'];       $giftcode_type = abs(intval($giftcode_type));
    $gift_max = $_POST['acc_gift_max'];            $gift_max = abs(intval($gift_max));
    $acc_list = $_POST['acc_list'];
    $acc_list_arr = explode(',', $acc_list);
    $err = "";
    $err_flag = false;
    foreach($acc_list_arr as $acc) {
        $acc = preg_replace("/ /", "", $acc);
        if(strlen($acc) > 0) {
            if (!preg_match("/^[a-zA-Z0-9_]*$/i", $acc))
        	{
                $err .= "Tài khoản $acc chứa ký tự đặc biệt.<br />";
                $err_flag = true;
        	}
            $acc_c_q = "SELECT count(memb___id) FROM MEMB_INFO WHERE memb___id='$acc'";
            $acc_c_r = $db->Execute($acc_c_q);
                check_queryerror($acc_c_q, $acc_c_r);
            $acc_c_f = $acc_c_r->FetchRow();
            if($acc_c_f[0] != 1) {
                $err .= "Tài khoản $acc không tồn tại.<br />";
                $err_flag = true;
            } else {
                $acc_arr[] = $acc;
                $gift_count_q = "SELECT count(acc) FROM GiftCode WHERE acc='$acc' AND type=4 AND giftcode_type=$giftcode_type";
                $gift_count_r = $db->Execute($gift_count_q);
                    check_queryerror($gift_count_q, $gift_count_r);
                $gift_count_f = $gift_count_r->FetchRow();
                $gift_count = $gift_count_f[0];
                if($gift_count > $gift_max) {
                    $err .= "Tài khoản $acc đã nhận $gift_count GiftCode loại này. Giới hạn tối đa đã nhận $gift_max GiftCode. Không thể nhận thêm.<br />";
                    $err_flag = true;
                }
            }
        }
    }
    
    if($err_flag === false) {
        $acc_count = count($acc_arr);
        if($acc_count == 0) {
            $err .= "Dữ liệu tài khoản sai.<br />";
            $err_flag = true;
        }
    }
        
    
    if($err_flag === false) {
        include_once('../config_license.php');
        include_once('../func_getContent.php');
        $getcontent_url = $url_license . "/api_giftcode_create.php";
        $getcontent_data = array(
            'acclic'    =>  $acclic,
            'key'    =>  $key,
            
            'gift_slg'  =>  $acc_count
        ); 
        
        $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
    
    	if ( empty($reponse) ) {
            $err .= "Server bao tri vui long lien he Admin de FIX";
            $err_flag = true;
        }
        else {
            $info = read_TagName($reponse, 'info');
            if($info == "Error") {
                $message = read_TagName($reponse, 'message');
                $err .= $message;
                $err_flag = true;
            } elseif($info == "OK") {
                $giftcode = read_TagName($reponse, 'giftcode');
                if(strlen($giftcode) == 0) {
                    $err .= "Du lieu tra ve loi. Vui long lien he Admin de FIX";
                    $err_flag = true;
                }
            } else {
                $err .= "Ket noi API gap su co. Admin MU vui long lien he nha cung cap DWebMu de kiem tra";
                $err_flag = true;
            }
        }
    }
    
    $msg = "";
    if($err_flag === false) {
        if($acc_count == 1) {
            $giftcode_insert_query = "INSERT INTO GiftCode (gift_code, acc, type, giftcode_type, gift_time, ngay, status) VALUES ('$giftcode', '".$acc_arr[0] ."', 4, $giftcode_type, $timestamp, '".date("Y-m-d",$timestamp)."', 1)";
            $giftcode_insert_result = $db->Execute($giftcode_insert_query);
                check_queryerror($giftcode_insert_query, $giftcode_insert_result);
            $msg .= "GiftCode của tài khoản <strong>". $acc_arr[0] ."</strong> : <strong>$giftcode</strong> <br />";
        } else {
            $giftcode_arr = json_decode($giftcode, true);
            foreach($acc_arr as $key => $acc) {
                $giftcode_acc = $giftcode_arr[$key];
                $giftcode_insert_query = "INSERT INTO GiftCode (gift_code, acc, type, giftcode_type, gift_time, ngay, status) VALUES ('$giftcode_acc', '".$acc_arr[$key] ."', 4, $giftcode_type, $timestamp, '".date("Y-m-d",$timestamp)."', 1)";
                $giftcode_insert_result = $db->Execute($giftcode_insert_query);
                    check_queryerror($giftcode_insert_query, $giftcode_insert_result);
                $msg .= "GiftCode của tài khoản <strong>". $acc_arr[$key] ."</strong> : <strong>$giftcode_acc</strong> <br />";
            }
        }
    }
    echo "<hr />";
    if($err_flag === false) {
        echo $msg;
    }
    if($err_flag === true) {
        echo "<strong>Lỗi</strong> : <br />" . $err;
    } 
}

if($_POST['nv_list']) {
    $giftcode_type = $_POST['giftcode_type'];       $giftcode_type = abs(intval($giftcode_type));
    $gift_max = $_POST['char_gift_max'];            $gift_max = abs(intval($gift_max));
    $nv_list = $_POST['nv_list'];
    $nv_list_arr = explode(',', $nv_list);
    $err = "";
    $err_flag = false;
    foreach($nv_list_arr as $nv) {
        $nv = preg_replace("/ /", "", $nv);
        if(strlen($nv) > 0) {
            if (!preg_match("/^[a-zA-Z0-9_]*$/i", $nv))
        	{
                $err .= "Tài khoản $nv chứa ký tự đặc biệt.<br />";
                $err_flag = true;
        	}
            
            
            
            $acc_c_q = "SELECT AccountID FROM Character WHERE Name='$nv'";
            $acc_c_r = $db->Execute($acc_c_q);
                check_queryerror($acc_c_q, $acc_c_r);
            $acc_c_exist = $acc_c_r->NumRows();
            
            if($acc_c_exist == 0) {
                $err .= "Nhân vật $nv không tồn tại.<br />";
                $err_flag = true;
            } else {
                $acc_c_f = $acc_c_r->FetchRow();
                $acc = $acc_c_f[0];
                $acc_arr[] = $acc;
                
                $gift_count_q = "SELECT count(acc) FROM GiftCode WHERE acc='$acc' AND type=4 AND giftcode_type=$giftcode_type";
                $gift_count_r = $db->Execute($gift_count_q);
                    check_queryerror($gift_count_q, $gift_count_r);
                $gift_count_f = $gift_count_r->FetchRow();
                $gift_count = $gift_count_f[0];
                if($gift_count > $gift_max) {
                    $err .= "Tài khoản $acc đã nhận $gift_count GiftCode loại này. Giới hạn tối đa đã nhận $gift_max GiftCode. Không thể nhận thêm.<br />";
                    $err_flag = true;
                }
            }
        }
    }
    
    if($err_flag === false) {
        $acc_count = count($acc_arr);
        if($acc_count == 0) {
            $err .= "Dữ liệu tài khoản sai.<br />";
            $err_flag = true;
        }
    }
        
    
    if($err_flag === false) {
        include_once('../config_license.php');
        include_once('../func_getContent.php');
        $getcontent_url = $url_license . "/api_giftcode_create.php";
        $getcontent_data = array(
            'acclic'    =>  $acclic,
            'key'    =>  $key,
            
            'gift_slg'  =>  $acc_count
        ); 
        
        $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
    
    	if ( empty($reponse) ) {
            $err .= "Server bao tri vui long lien he Admin de FIX";
            $err_flag = true;
        }
        else {
            $info = read_TagName($reponse, 'info');
            if($info == "Error") {
                $message = read_TagName($reponse, 'message');
                $err .= $message;
                $err_flag = true;
            } elseif($info == "OK") {
                $giftcode = read_TagName($reponse, 'giftcode');
                if(strlen($giftcode) == 0) {
                    $err .= "Du lieu tra ve loi. Vui long lien he Admin de FIX";
                    $err_flag = true;
                }
            } else {
                $err .= "Ket noi API gap su co. Admin MU vui long lien he nha cung cap DWebMu de kiem tra";
                $err_flag = true;
            }
        }
    }
    
    $msg = "";
    if($err_flag === false) {
        if($acc_count == 1) {
            $giftcode_insert_query = "INSERT INTO GiftCode (gift_code, acc, type, giftcode_type, gift_time, ngay, status) VALUES ('$giftcode', '".$acc_arr[0] ."', 4, $giftcode_type, $timestamp, '".date("Y-m-d",$timestamp)."', 1)";
            $giftcode_insert_result = $db->Execute($giftcode_insert_query);
                check_queryerror($giftcode_insert_query, $giftcode_insert_result);
            $msg .= "GiftCode của tài khoản <strong>". $acc_arr[0] ."</strong> : <strong>$giftcode</strong> <br />";
        } else {
            $giftcode_arr = json_decode($giftcode, true);
            foreach($acc_arr as $key => $acc) {
                $giftcode_acc = $giftcode_arr[$key];
                $giftcode_insert_query = "INSERT INTO GiftCode (gift_code, acc, type, giftcode_type, gift_time, ngay, status) VALUES ('$giftcode_acc', '".$acc_arr[$key] ."', 4, $giftcode_type, $timestamp, '".date("Y-m-d",$timestamp)."', 1)";
                $giftcode_insert_result = $db->Execute($giftcode_insert_query);
                    check_queryerror($giftcode_insert_query, $giftcode_insert_result);
                $msg .= "GiftCode của tài khoản <strong>". $acc_arr[$key] ."</strong> : <strong>$giftcode_acc</strong> <br />";
            }
        }
    }
    echo "<hr />";
    if($err_flag === false) {
        echo $msg;
    }
    if($err_flag === true) {
        echo "<strong>Lỗi</strong> : <br />" . $err;
    } 
}

?>

</body>
</html>
<?php
$db->Close();
?>