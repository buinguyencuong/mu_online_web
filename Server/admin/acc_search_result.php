<?php

/**
 * @author NetBanBe
 * @copyright 2014
 */

include_once ("security.php");
include ('../config.php');
include ('function.php');
include ('../config/config_thehe.php');
session_start();

$acc = $_GET['acc'];
$search_type = $_GET['search_type'];

$acc_search_q = "SELECT A.memb___id, memb__pwd, memb__pwd2, pass2, tel__numb, mail_addr, fpas_ques, fpas_answ, gcoin, gcoin_km, vpoint, bank, jewel_chao, jewel_cre, jewel_blue, bloc_code, A.ip, thehe, SCFVipMoney, WCoin, nbb_pl, jewel_heart, admin_block, BlockTime, vpoint_km, nbb_pl_extra, B.IP, ConnectStat, ServerName, ConnectTM, DisConnectTM FROM MEMB_INFO A JOIN MEMB_STAT B ON A.memb___id = B.memb___id";
if($search_type == 1) $acc_search_q .= " AND A.memb___id = '$acc'";
else $acc_search_q .= " AND A.memb___id like '%{$acc}%'";
$acc_search_q .= " ORDER BY ConnectStat DESC, ConnectTM DESC, DisConnectTM DESC";

$acc_search_r = $db->Execute($acc_search_q);
    check_queryerror($acc_search_q, $acc_search_r);

$acc_arr = array(); 
$acc_search_count = 0;   
while($acc_search_f = $acc_search_r->FetchRow()) {
    $acc_id = $acc_search_f[0];
    
    //Connect Info
    $ipsv = $acc_search_f[26];
    $connect_stat = $acc_search_f[27];
    $server_name = $acc_search_f[28];
    $connect_time = $acc_search_f[29];
    $disconnect_Time = $acc_search_f[30];
    
    if($connect_stat == 1) {
        $char_online_q = "SELECT GameIDC FROM AccountCharacter WHERE Id='$acc_id'";
        $char_online_r = $db->Execute($char_online_q);
        $char_online_f = $char_online_r->FetchRow();
        $char_online_name = $char_online_f[0];
        
        $online_time = $timestamp - strtotime($connect_time);
        
        $online_time_view = "";
        $day_online = floor($online_time/(24*60*60));
        if($day_online > 0) {
            $online_time_view .= "$day_online ngày ";
        }
        
        $online_time = $online_time - $day_online*24*60*60;
        $hour_online = floor($online_time/(60*60));
        $online_time_view .= "{$hour_online}h";
        
        $online_time = $online_time - $hour_online*60*60;
        $minus_online = floor($online_time/(60));
        $online_time_view .= "{$minus_online}p";
    }
    
    $char_q = "SELECT Name, Resets, Relifes, MapNumber, MapPosX, MapPosY FROM Character WHERE AccountID='$acc_id'";
    $char_r = $db->Execute($char_q);
        check_queryerror($char_q, $char_r);
    $char_arr = array();
    while($char_f = $char_r->FetchRow()) {
        $online_stat = 0;
        if($connect_stat == 1 && $char_f[0] == $char_online_name) {
            $online_stat = 1;
            switch ($char_f[3]) {
                case 0:
                    $map_name = 'Lorencia';
                    break;
                case 1:
                    $map_name = 'Dungeon';
                    break;
                case 2:
                    $map_name = 'Davias';
                    break;
                case 3:
                    $map_name = 'Noria';
                    break;
                case 4:
                    $map_name = 'LostTower';
                    break;
                case 5:
                    $map_name = 'Exile';
                    break;
                case 6:
                    $map_name = 'Stadium';
                    break;
                case 7:
                    $map_name = 'Atlans';
                    break;
                case 8:
                    $map_name = 'Tarkan';
                    break;
                case 10:
                    $map_name = 'Icarus';
                    break;
                case 11:
                    $map_name = 'BloodCastle 1';
                    break;
                case 12:
                    $map_name = 'BloodCastle 2';
                    break;
                case 13:
                    $map_name = 'BloodCastle 3';
                    break;
                case 14:
                    $map_name = 'BloodCastle 4';
                    break;
                case 15:
                    $map_name = 'BloodCastle 5';
                    break;
                case 16:
                    $map_name = 'BloodCastle 6';
                    break;
                case 17:
                    $map_name = 'BloodCastle 7';
                    break;
                case 52:
                    $map_name = 'BloodCastle 8';
                    break;
                case 9:
                    $map_name = 'DevilSquare 1-2-3-4';
                    break;
                case 32:
                    $map_name = 'DevilSquare 1-2-3-4';
                    break;
                case 35:
                    $map_name = 'Lorencia';
                    break;
                case 18:
                    $map_name = 'ChaosCastle 1';
                    break;
                case 19:
                    $map_name = 'ChaosCastle 2';
                    break;
                case 20:
                    $map_name = 'ChaosCastle 3';
                    break;
                case 21:
                    $map_name = 'ChaosCastle 4';
                    break;
                case 22:
                    $map_name = 'ChaosCastle 5';
                    break;
                case 23:
                    $map_name = 'ChaosCastle 6';
                    break;
                case 53:
                    $map_name = 'ChaosCastle 7';
                    break;
                case 24:
                    $map_name = 'Kalima 1';
                    break;
                case 25:
                    $map_name = 'Kalima 2';
                    break;
                case 26:
                    $map_name = 'Kalima 3';
                    break;
                case 27:
                    $map_name = 'Kalima 4';
                    break;
                case 28:
                    $map_name = 'Kalima 5';
                    break;
                case 29:
                    $map_name = 'Kalima 6';
                    break;
                case 36:
                    $map_name = 'Kalima 7';
                    break;
                case 30:
                    $map_name = 'Valley Of Loren';
                    break;
                case 31:
                    $map_name = 'Land Of Trials';
                    break;
                case 33:
                    $map_name = 'Aida';
                    break;
                case 34:
                    $map_name = 'CryWolf';
                    break;
                case 37:
                    $map_name = 'Kantru 1';
                    break;
                case 38:
                    $map_name = 'Kantru 2';
                    break;
                case 39:
                    $map_name = 'Kantru Ref';
                    break;
                case 40:
                    $map_name = 'Silent Map';
                    break;
                case 41:
                    $map_name = 'Balgass Barrack';
                    break;
                case 42:
                    $map_name = 'Balgass Refuge';
                    break;
                case 45:
                    $map_name = 'Illusion Temple 1';
                    break;
                case 46:
                    $map_name = 'Illusion Temple 2';
                    break;
                case 47:
                    $map_name = 'Illusion Temple 3';
                    break;
                case 48:
                    $map_name = 'Illusion Temple 4';
                    break;
                case 49:
                    $map_name = 'Illusion Temple 5';
                    break;
                case 50:
                    $map_name = 'Illusion Temple 6';
                    break;
                case 51:
                    $map_name = 'Elbeland';
                    break;
                case 56:
                    $map_name = 'Swamp Of Calmness';
                    break;
                case 57:
                    $map_name = 'Raklion';
                    break;
                case 58:
                    $map_name = 'Raklion BOSS';
                    break;
                case 62:
                    $map_name = 'Santa Town';
                    break;
                case 63:
                    $map_name = 'Vulcanus';
                    break;
                case 64:
                    $map_name = 'Duel Arena';
                    break;
                case 65:
                    $map_name = 'Doppel Ganger-A';
                    break;
                case 66:
                    $map_name = 'Doppel Ganger-B';
                    break;
                case 67:
                    $map_name = 'Doppel Ganger-C';
                    break;
                case 68:
                    $map_name = 'Doppel Ganger-D';
                    break;
                case 69:
                    $map_name = 'Empire Guardian-A';
                    break;
                case 70:
                    $map_name = 'Empire Guardian-B';
                    break;
                case 71:
                    $map_name = 'Empire Guardian-C';
                    break;
                case 72:
                    $map_name = 'Empire Guardian-D';
                    break;
                case 79:
                    $map_name = 'Market Loren';
                    break;
                default:
                    $map_name = 'Unknow';
            }
        }
        
        $char_arr[] = array(
            'name'  =>  $char_f[0],
            'rs'    =>  $char_f[1],
            'rl'    =>  $char_f[2],
            'online'    =>  $online_stat,
            'server'    =>  $server_name,
            'map'   =>  $map_name,
            'map_x' =>  $char_f[4],
            'map_y' =>  $char_f[5]
        );
    }
    
    switch ($acc_search_f[6]) {
        case 1:
            $quest = "Tên cha của bạn là gì?";
            break;
        case 2:
            $quest = "Tên ngôi trường đầu tiên của bạn là gì?";
            break;
        case 3:
            $quest = "Người anh hùng trong thời thơ ấu của bạn là ai?";
            break;
        case 4:
            $quest = "Khái niệm đẹp của bạn là gì?";
            break;
        case 5:
            $quest = "Đội thể thao bạn thích nhất là đội nào?";
            break;
        case 6:
            $quest = "Vật mang lại may mắn thời học sinh của bạn là gì?";
            break;
        case 7:
            $quest = "Nơi bạn gặp vợ(chồng) của bạn nơi nào?";
            break;
        case 8:
            $quest = "Tên con thú cưng của bạn là gì?";
            break;
    }
    
    $block_admin = $acc_search_f[22];
    
    if ($acc_search_f[15] == 1) {
        if($block_admin == 0) {
            $stat = '<font color="orange">Block : có Item Hack</font>';
        } else {
            $block_time = $acc_search_f[23];
            $block_time_view = date('H:i d/m/Y', $block_time);
            $stat = '<font color="red"><strong>Block</strong></font> : '. $block_time_view;
        }
        
    } else {
        $stat = '<font color="blue">Không Block</font>';
    }
    
    if(strlen($acc_search_f[1]) > 10) $pass_game = "MD5";
    else $pass_game = $acc_search_f[1];
    
    $block_data_q = "SELECT TOP 5 admin, block_type, block_des, day_block, block_time_begin, block_time_end FROM nbb_block WHERE acc='$acc_id' ORDER BY block_time_begin DESC";
    $block_data_r = $db->Execute($block_data_q);
        check_queryerror($block_data_q, $block_data_r);
    
    $block_data = '';
    while($block_data_f = $block_data_r->FetchRow()) {
        if($block_data_f[1] == 1) {
            $block_data .= "<strong>". $block_data_f[0] ."</strong> : <strong><font color='red'>Block</font></strong> trong ". $block_data_f[3] ." ngày (". date('H:i:s d/m/Y', $block_data_f[5]) .") lúc ". date('H:i:s d/m/Y', $block_data_f[4]) .". <strong>Lý do</strong> : ". $block_data_f[2] ."<hr />";
        } else {
            $block_data .= "<strong>". $block_data_f[0] ."</strong> : Giải Block lúc ". date('H:i:s d/m/Y', $block_data_f[4]) .". Lý do : ". $block_data_f[2] ."<hr />";
        }
    }
    
    $acc_arr[$acc_id] = array(
        'id'    =>  $acc_id,
        'char'  =>  $char_arr,
        'pass_game' =>  $pass_game,
        'pass1' =>  $acc_search_f[2],
        'pass2' =>  $acc_search_f[3],
        'tel'   =>  $acc_search_f[4],
        'mail'  =>  $acc_search_f[5],
        'quest' =>  $quest,
        'questid' =>  $acc_search_f[6],
        'ans'   =>  $acc_search_f[7],
        'gcoin' =>  $acc_search_f[8],
        'gcoinkm'  =>  $acc_search_f[9],
        'vpoint'    =>  $acc_search_f[10],
        'vpoint_km'    =>  $acc_search_f[24],
        'bank_zen'  =>  $acc_search_f[11],
        'bank_chao' =>  $acc_search_f[12],
        'bank_cre'  =>  $acc_search_f[13],
        'bank_blue' =>  $acc_search_f[14],
        'stat'  =>  $acc_search_f[15],
        'stat_view'  =>  $stat,
        'ip'    =>  $acc_search_f[16],
        'ipsv'   =>  $ipsv,
        'thehe' =>  $acc_search_f[17],
        'SCFVipMoney'   =>  $acc_search_f[18],
        'WCoin'   =>  $acc_search_f[19],
        'nbb_pl'   =>  $acc_search_f[20],
        'nbb_pl_extra'   =>  $acc_search_f[25],
        'jewel_heart'   =>  $acc_search_f[21],
        'block_data'    =>  $block_data
    );
    
    $acc_search_count++;
}
?>
<table class="sort-table" id="table-1" height="30" border="0" cellpadding="0" cellspacing="0" width="960px">

<thead><tr>
    <td class="text_administrator">#</td>
    <td class="text_administrator">Account</td>
    <td class="text_administrator">Thông tin</td>
</tr></thead>
<?php
    if($acc_search_count > 0) {
        $count = 0;
        foreach($acc_arr as $acc) {
            $count++;
?>
<tr>
    <td align="left" class="text_administrator"><?php echo $count; ?></td>
    <td align="left" class="text_administrator"><?php echo $acc['id']; ?></td>
    <td align="left" class="text_administrator" valign="top">
    	<div style="width: 100%;">
            <div style="float:left; width: 25%;">
        		<li style="position: relative; padding-right:20px">Pass Game : <span id="edit_passgame_<?php echo $acc['id']; ?>"><?php echo $acc['pass_game']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['pass_game']; ?>' datatype="passgame" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
        		</li><li style="position: relative; padding-right:20px">Pass Cấp 1 : <span id="edit_pass1_<?php echo $acc['id']; ?>"><?php echo $acc['pass1']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['pass1']; ?>' datatype="pass1" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
        		</li><li style="position: relative; padding-right:20px">Pass Cấp 2 : <span id="edit_pass2_<?php echo $acc['id']; ?>"><?php echo $acc['pass2']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['pass2']; ?>' datatype="pass2" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
        		</li><li style="position: relative; padding-right:20px">Số ĐT : <span id="edit_tel_<?php echo $acc['id']; ?>"><?php echo $acc['tel']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['tel']; ?>' datatype="tel" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
        		</li><li style="position: relative; padding-right:50px">Email : <span id="edit_mail_<?php echo $acc['id']; ?>"><?php echo $acc['mail']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['mail']; ?>' datatype="mail" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
        	</li></div>
            
        	<div style="float:left; width: 25%;">
        		<li style="position: relative; padding-right:50px">Câu hỏi : <span id="edit_quest_<?php echo $acc['id']; ?>"><?php echo $acc['quest']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['questid']; ?>' datatype="quest" dataview="<?php echo $acc['quest']; ?>" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:20px">Câu trả lời : <span id="edit_ans_<?php echo $acc['id']; ?>"><?php echo $acc['ans']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['ans']; ?>' datatype="ans" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                
                </li><li style="position: relative; padding-right:20px">GCent : <span id="edit_gcoin_<?php echo $acc['id']; ?>"><?php echo $acc['gcoin']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['gcoin']; ?>' datatype="gcoin" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span> <span class="area_plus_minus"><a href='acc_plus_minus.php?acc=<?php echo $acc['id']; ?>&datatype=gcoin' title='Cộng - Trừ Gcent' class='plus_minus' style="position: absolute; top: 0px; right: 25px;"><img src='images/plus-minus.png' border='0' /></a></span>
                
                </li><li style="position: relative; padding-right:20px">GCent KM : <span id="edit_gcoinkm_<?php echo $acc['id']; ?>"><?php echo $acc['gcoinkm']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['gcoinkm']; ?>' datatype="gcoinkm" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span> <span class="area_plus_minus"><a href='acc_plus_minus.php?acc=<?php echo $acc['id']; ?>&datatype=gcoin_km' title='Cộng - Trừ Gcent Khuyến Mãi' class='plus_minus' style="position: absolute; top: 0px; right: 25px;"><img src='images/plus-minus.png' border='0' /></a></span>
                
        		</li><li style="position: relative; padding-right:20px">Vcent : <span id="edit_vpoint_<?php echo $acc['id']; ?>"><?php echo $acc['vpoint']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['vpoint']; ?>' datatype="vpoint" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span> <span class="area_plus_minus"><a href='acc_plus_minus.php?acc=<?php echo $acc['id']; ?>&datatype=vpoint' title='Cộng - Trừ Vcent' class='plus_minus' style="position: absolute; top: 0px; right: 25px;"><img src='images/plus-minus.png' border='0' /></a></span>
                </li><li style="position: relative; padding-right:20px">Vcent Event : <span id="edit_vpoint_km_<?php echo $acc['id']; ?>"><?php echo $acc['vpoint_km']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['vpoint_km']; ?>' datatype="vpoint_km" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span> <span class="area_plus_minus"><a href='acc_plus_minus.php?acc=<?php echo $acc['id']; ?>&datatype=vpoint_km' title='Cộng - Trừ Vcent Event' class='plus_minus' style="position: absolute; top: 0px; right: 25px;"><img src='images/plus-minus.png' border='0' /></a></span>
        	</li></div>
            
        	<div style="float:left; width: 25%;">
        		<li style="position: relative; padding-right:20px">Ngân hàng Chao : <span id="edit_bank_chao_<?php echo $acc['id']; ?>"><?php echo $acc['bank_chao']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['bank_chao']; ?>' datatype="bank_chao" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span> <span class="area_plus_minus"><a href='acc_plus_minus.php?acc=<?php echo $acc['id']; ?>&datatype=jewel_chao' title='Cộng - Trừ Ngọc Hỗn Nguyên ngân hàng' class='plus_minus' style="position: absolute; top: 0px; right: 25px;"><img src='images/plus-minus.png' border='0' /></a></span>
        		</li><li style="position: relative; padding-right:20px">Ngân hàng Cre : <span id="edit_bank_cre_<?php echo $acc['id']; ?>"><?php echo $acc['bank_cre']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['bank_cre']; ?>' datatype="bank_cre" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span> <span class="area_plus_minus"><a href='acc_plus_minus.php?acc=<?php echo $acc['id']; ?>&datatype=jewel_cre' title='Cộng - Trừ Ngọc Sáng Tạo ngân hàng' class='plus_minus' style="position: absolute; top: 0px; right: 25px;"><img src='images/plus-minus.png' border='0' /></a></span>
                </li><li style="position: relative; padding-right:20px">Ngân hàng Blue : <span id="edit_bank_blue_<?php echo $acc['id']; ?>"><?php echo $acc['bank_blue']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['bank_blue']; ?>' datatype="bank_blue" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span> <span class="area_plus_minus"><a href='acc_plus_minus.php?acc=<?php echo $acc['id']; ?>&datatype=jewel_blue' title='Cộng - Trừ Lông Chim ngân hàng' class='plus_minus' style="position: absolute; top: 0px; right: 25px;"><img src='images/plus-minus.png' border='0' /></a></span>
                </li><li style="position: relative; padding-right:50px">Ngân hàng ZEN : <span id="edit_bank_zen_<?php echo $acc['id']; ?>"><?php echo $acc['bank_zen']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['bank_zen']; ?>' datatype="bank_zen" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span> <span class="area_plus_minus"><a href='acc_plus_minus.php?acc=<?php echo $acc['id']; ?>&datatype=bank_zen' title='Cộng - Trừ Zen ngân hàng' class='plus_minus' style="position: absolute; top: 0px; right: 25px;"><img src='images/plus-minus.png' border='0' /></a></span>
                </li><li style="position: relative; padding-right:20px">Ngân hàng Trái Tim : <span id="edit_jewel_heart_<?php echo $acc['id']; ?>"><?php echo $acc['jewel_heart']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['jewel_heart']; ?>' datatype="jewel_heart" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span> <span class="area_plus_minus"><a href='acc_plus_minus.php?acc=<?php echo $acc['id']; ?>&datatype=jewel_heart' title='Cộng - Trừ Trái tim ngân hàng' class='plus_minus' style="position: absolute; top: 0px; right: 25px;"><img src='images/plus-minus.png' border='0' /></a></span>
            </li></div>
            
        	<div style="float:left; width: 25%;">
                <li style="position: relative; padding-right:20px">SCFVipMoney : <span id="edit_SCFVipMoney_<?php echo $acc['id']; ?>"><?php echo $acc['SCFVipMoney']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['SCFVipMoney']; ?>' datatype="SCFVipMoney" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span> <span class="area_plus_minus"><a href='acc_plus_minus.php?acc=<?php echo $acc['id']; ?>&datatype=SCFVipMoney' title='Cộng - Trừ SCFVipMoney' class='plus_minus' style="position: absolute; top: 0px; right: 25px;"><img src='images/plus-minus.png' border='0' /></a></span>
                </li><li style="position: relative; padding-right:20px">WCoin : <span id="edit_WCoin_<?php echo $acc['id']; ?>"><?php echo $acc['WCoin']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['WCoin']; ?>' datatype="WCoin" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span> <span class="area_plus_minus"><a href='acc_plus_minus.php?acc=<?php echo $acc['id']; ?>&datatype=wcoin' title='Cộng - Trừ WCoin' class='plus_minus' style="position: absolute; top: 0px; right: 25px;"><img src='images/plus-minus.png' border='0' /></a></span>
                </li><li style="position: relative; padding-right:20px">Điểm PPoint : <span id="edit_nbb_pl_<?php echo $acc['id']; ?>"><?php echo $acc['nbb_pl']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['nbb_pl']; ?>' datatype="nbb_pl" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span> <span class="area_plus_minus"><a href='acc_plus_minus.php?acc=<?php echo $acc['id']; ?>&datatype=nbb_pl' title='Cộng - Trừ Điểm PPoint' class='plus_minus' style="position: absolute; top: 0px; right: 25px;"><img src='images/plus-minus.png' border='0' /></a></span>
                
                </li><li style="position: relative; padding-right:20px">Điểm PPoint+ : <span id="edit_nbb_pl_extra_<?php echo $acc['id']; ?>"><?php echo $acc['nbb_pl_extra']; ?> <a href='#' acc='<?php echo $acc['id']; ?>' dataedit='<?php echo $acc['nbb_pl_extra']; ?>' datatype="nbb_pl_extra" class='acc_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span> <span class="area_plus_minus"><a href='acc_plus_minus.php?acc=<?php echo $acc['id']; ?>&datatype=nbb_pl_extra' title='Cộng - Trừ Điểm PPoint+' class='plus_minus' style="position: absolute; top: 0px; right: 25px;"><img src='images/plus-minus.png' border='0' /></a></span>
                
                </li><li style="position: relative; padding-right:20px">Block : <span id="edit_block_<?php echo $acc['id']; ?>" class="block_view"><?php echo $acc['stat_view']; ?><span style="position: absolute; display: none; top: 20px; left: -300px; right: 0px; padding: 5px; max-width: 500px; z-index: 1; background-color: black; color: white; " class="block_data"><?php echo $acc['block_data']; ?></span></span> <span class="area_block_acc"><a href='acc_block.php?acc=<?php echo $acc['id']; ?>&block_status=<?php echo $acc['stat']; ?>' title='Khóa tài khoản' class='block_acc' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                    

        		</li><li style="position: relative; padding-right:20px">IP Web : <a href="#" class="ip_search_acc" ip="<?php echo $acc['ip']; ?>" pos="1"><?php echo $acc['ip']; ?></a> <span id="ip_search_loading_1"></span>
                </li><li style="position: relative; padding-right:20px">IP Game : <a href="#" class="ip_search_acc" ip="<?php echo $acc['ipsv']; ?>" pos="2"><?php echo $acc['ipsv']; ?></a> <span id="ip_search_loading_2"></span>
        		</li><li style="position: relative; padding-right:20px">Thế hệ : <?php echo $acc['thehe']; ?>
        	</li></div>
         </div>
    </td>
</tr>

<tr>
    <td></td>
    <td></td>
    <td>
        <table class="sort-table" width="100%">
            <tr>
            	<?php
                    foreach($acc['char'] as $char) {
                ?>
                <td width="20%" class="text_administrator">
                    <a href="#" class="acc_search_char" charname="<?php echo $char['name']; ?>"><?php echo $char['name']; ?></a> <i>(<font color='red'><?php echo $char['rl']; ?></font>/<font color='blue'><?php echo $char['rs']; ?></font>)</i> <span id="acc_search_char_<?php echo $char['name']; ?>"></span>
                    <?php 
                        if($char['online'] == 1) echo "<br /><font color='blue'><strong>ON</strong></font> ". $char['server'] ." | ". $char['map'] ."(". $char['map_x'] .",". $char['map_y'].")<br />". $online_time_view ."";
                    ?>
            	</td>
                <?php } ?>
            </tr>	
        </table>
    </td>
</tr>

<tr>
    <td colspan="3"><hr /></td>
</tr>
<?php
        }
    }
?>

</table>