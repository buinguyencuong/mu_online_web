<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
	include("security.php");

include_once('../config.php');
include_once('function.php');
$noitem = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
$kris_non = "00000000000000000000000000000000";
$title = "Item Log - List Item";

SESSION_start();
?>

<html>
<head>
<title><?php echo $title; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="js/jquery-ui-1.8.21.custom.css" />
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
<script src="js/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $('.itemdel').live("click",function() 
        {
            //dupeseri_$acc_$name_$seri_$item_vitri
            var acc = $(this).attr("acc");
            var name = $(this).attr("name");
            var itemcode = $(this).attr("itemcode");
            var key = $(this).attr("key");
            var dataString = 'acc='+ acc +'&name='+ name +'&itemcode='+ itemcode;
            
            $.ajax({
                type: "POST",
                url: "dupedel_online.php",
                data: dataString,
                cache: false,
                success: function(reponse){
                    $("#itemdel_"+ acc +"_"+ name +"_"+ itemcode +"_"+ key).html(reponse);
                }
            });
            
            return false;
        });
    });
</script>

</head>
<body bgcolor="#F9E7CF">
<?php
    $acc = $_GET['acc'];
    $type = $_GET['type'];
    if($type == 'inventory') {
        $name = $_GET['name'];
    }
    $logtime = $_GET['logtime'];
    if($logtime != 'now') {
        $logtime_show = date('Y-m-d H:i:s', $logtime);
        $logtime = date('Y-m-d H:i:s', $logtime);
    } else {
        $logtime_show = 'Hiện tại';
    }
?>
<table width="100%" cellspacing="1" cellpadding="3" border="0" bgcolor="#0000ff">
<tr bgcolor="#ffffcc" >
	<td colspan="4" align="center">
		Log Item tài khoản <strong><font color='red'><?php echo $acc; ?></font></strong> <?php if(isset($name)) echo "- Nhân vật <strong><font color='blue'>$name</font></strong>" ?> tại thời điểm <strong><font color='brown'><?php echo $logtime_show; ?></font></strong>
	</td>
</tr>

<tr bgcolor="#ffffcc" >
	<td align="center">#</td>
	<td align="center">Hình ảnh</td>
    <td align="center">Item</td>
	<td align="center">Thông tin Item</td>
</tr>


<?php    
    if($logtime != 'now') {
        if($type == 'warehouse') {
            if($sql_version == '2008') {
                $warehouse_query = "SELECT Convert(text,Items) FROM NBB_Log_WareHouse WHERE acc='$acc' AND log_time='$logtime'";
            } else {
                $warehouse_query = "SELECT CAST(Items as image) FROM NBB_Log_WareHouse WHERE acc='$acc' AND log_time='$logtime'";
            }
            
            $warehouse_result = $db->Execute($warehouse_query);
                check_queryerror($warehouse_query, $warehouse_result);
            $warehouse_fetch = $warehouse_result->FetchRow();
            $item_list = $warehouse_fetch[0];
        } else {
            if($sql_version == '2008') {
                $inventory_query = "SELECT Convert(text,Inventory) FROM NBB_Log_Inventory WHERE acc='$acc' AND name='$name' AND log_time='$logtime'";
            } else {
                $inventory_query = "SELECT CAST(Inventory as image) FROM NBB_Log_Inventory WHERE acc='$acc' AND name='$name' AND log_time='$logtime'";
            }
            
            $inventory_result = $db->Execute($inventory_query);
                check_queryerror($inventory_query, $inventory_result);
            $inventory_fetch = $inventory_result->FetchRow();
            $item_list = $inventory_fetch[0];
        }
        if($sql_version != '2008') {
            $item_list = bin2hex($item_list);
        }
    } else {
        if($type_connect == 'odbc') {
        	$db = &ADONewConnection('odbc');
        	$connect_mssql = $db->Connect($database,$databaseuser,$databsepassword);
        	if (!$connect_mssql) die("Ket noi voi SQL Server loi! Hay kiem tra lai ODBC ton tai hoac User & Pass SQL dung.");
        } elseif ($type_connect == 'mssql'){
        	$db = &ADONewConnection('mssql');
        	$connect_mssql = $db->Connect($localhost,$databaseuser,$databsepassword,$database);
        	if (!$connect_mssql) die("Loi! Khong the ket noi SQL Server : $database");
        }
        if($type == 'warehouse') {
            $warehouse_query = "SELECT CAST(Items as image) FROM warehouse WHERE AccountID='$acc'";
            $warehouse_result = $db->Execute($warehouse_query);
                check_queryerror($warehouse_query, $warehouse_result);
            $warehouse_fetch = $warehouse_result->FetchRow();
            $item_list = $warehouse_fetch[0];
        } else {
            $inventory_query = "SELECT CAST(Inventory as image) FROM Character WHERE AccountID='$acc' AND name='$name'";
            $inventory_result = $db->Execute($inventory_query);
                check_queryerror($inventory_query, $inventory_result);
            $inventory_fetch = $inventory_result->FetchRow();
            $item_list = $inventory_fetch[0];
        }
        $item_list = bin2hex($item_list);
    }
    
    $item_list = strtoupper($item_list);
    $item_list = preg_replace('/'. $noitem .'/', '', $item_list);
    $item_total = floor(strlen($item_list)/32);
    
    $item_arr = array();
    for($i=0; $i<$item_total; $i++) {
        $item = substr($item_list,$i*32, 32);
        if($item != $noitem && $item != $kris_non) {
            $item_arr[] = $item;
        }
    }
    
    $item_data = serialize($item_arr);
    include('../config_license.php');
    include_once('../func_getContent.php');
    $getcontent_url = $url_license . "/api_itemlog.php";
    $getcontent_data = array(
        'acclic'    =>  $acclic,
        'key'    =>  $key,
        
        'item_data'    =>  $item_data
    ); 
    
    $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);

	if ( empty($reponse) ) {
        $error = "Không kết nối được đến API";
        $err = true;
    }
    else {
        $info = read_TagName($reponse, 'info');
        if($info == "Error") {
            $error = $reponse;
            $err = true;
        } elseif ($info == "OK") {
            $iteminfo = read_TagName($reponse, 'iteminfo');
            $iteminfo_arr = array();
            $iteminfo_arr = unserialize_safe($iteminfo);
            
            $seri_arr = array();
            
            $stt = 0;
            foreach($iteminfo_arr as $key => $val) {
                ++$stt;
                $dupe = 0;
                $dupe_bg = '';
                $dupe_notice = '';
                $seri_hex = $val['item_seri'];
                $seri_dec = hexdec($seri_hex);
                    if($seri_dec > 0 && $seri_dec < hexdec('FFFFFFF0')) {
                        if(!in_array($seri_dec, $seri_arr, true)) {
                            $seri_arr[] = $seri_dec;
                        } else {
                            $dupe = 1;
                            $dupe_bg = "bgcolor='cyan'";
                            $dupe_notice = "<font color='red'><strong>DUPE</strong></font><br />";
                        }
                    }
                if($seri_dec >= hexdec('FFFFFFF0')) {
                    $seri_hex = "<font color='red'><b>". $seri_hex . "</b></font>";
                    $seri_dec = "<font color='red'><b>". $seri_dec . "</b></font>";
                }
                
                echo "<tr bgcolor='#F9E7CF' >
            		<td align='center'>$stt</td>
                    <td align='center' style='background-color: #121212; font-size: 14px; text-align: center; padding: 10px;'><img src='../items/" . $val['item_image'] . ".gif' border=0 /></td>
            		<td align='center' style='background-color: #121212; font-size: 14px; text-align: center; padding: 10px;'>". $val['item_info'] ."</td>
            		<td align='left' $dupe_bg>
                        <strong>Item Name</strong>: ". $val['item_name'] ."<br />
                        <strong>Item Code</strong>: ". $val['item_code'] ."<br />
                        <strong>Item Seri HEX</strong>: ". $seri_hex ."<br />
                        <strong>Item Seri DEC</strong>: ". $seri_dec ."<br />
                        $dupe_notice
                    ";
                if($logtime == 'now') {
                    echo "<span id='itemdel_". $acc ."_". $name ."_". $val['item_code'] ."_". $key ."'><a href='#' acc='$acc' name='$name' itemcode='". $val['item_code'] ."' key='". $key ."' class='itemdel'>Xóa Item</a></span>";
                }
            	echo "</td></tr>";
            }
                
        } else {
            $error = "Kết nối API gặp sự cố.";
            $err = true;
        }
    }
    
    if($err === true) {
        echo "<tr bgcolor='#F9E7CF' ><td align='center' colspan='3'>$error</td></tr>";
    }
            
?>
												
</table>
<br>
<center>
<?php
$db->Close();
?>
</center>

</body>
</html>