function _pageload(url,data,id){
    $.ajax({
		url: url,
		type: "GET",
		data: data,
		cache: false,
        beforeSend: function() {
            $('#loading').show();
            $('#notice').hide();
            $('#'+ id).hide();
       },
		success: function (html) {
			$('#loading').hide();			
			$('#'+ id).html(html);
			$('#'+ id).fadeIn('medium');
		}		
	});
}

$(document).ready(function() {
    //****************************** Search Acc *******************************
    $("#submit_search_acc").live('click', function(){
        var acc = $("#acc").val();
        var search_type = $('input[name=search_type]:checked').val();
        var dataString = "acc=" + acc + "&search_type=" + search_type;
        
         $.ajax({
           type: "GET",
           url: "acc_search_result.php",
           data: dataString,
           cache: false,
           beforeSend: function() {
                $("#search_loading").html("<img src='images/loading1.gif' border='0' />");
                $("#submit_search_acc").attr('disabled','disabled');
           },
           complete: function() {
                $("#search_loading").html("");
            
           },
           success: function(html) {
                $("#search_result").html(html);
                $("#submit_search_acc").removeAttr('disabled');
           }
        });
        
        return false;
    });
    
    // Enter Search Acc Result
        $('#acc').live('keyup', function (e) { 
            if(e.keyCode == 13) {
                 $('#submit_search_acc').click();
             }
             
             return false;
        });
    
    // Plus & Minus Acc Data
    $('.plus_minus').live('mouseenter', function(){
        $(this).fancybox();
    });
    
    // Block Acc Data
    $('.block_acc').live('mouseenter', function(){
        $(this).fancybox();
    });
    
    $(".char_search_acc").live('click', function(){
        var acc = $(this).attr('accname');
        var dataString = "acc=" + acc + "&search_type=1";
        
         $.ajax({
           type: "GET",
           url: "acc_search_result.php",
           data: dataString,
           cache: false,
           beforeSend: function() {
                $("#char_search_acc_"+ acc).html("<img src='images/loading1.gif' border='0' />");
           },
           complete: function() {
                $("#char_search_acc_"+ acc).html("");
            
           },
           success: function(html) {
                $("#search_result").html(html);
                $('#char').val('');
           }
        });
        
        return false;
    });
    //****************************** Search Acc End *******************************
    
    //****************************** Edit Acc Begin *******************************
    // Edit
    $('.acc_data_edit').live("click", function() {
        var datatype = $(this).attr("datatype");
        var acc = $(this).attr("acc");
        var dataedit = $(this).attr("dataedit");
        
        if(datatype == "quest") {
            var dataview = $(this).attr("dataview");
            
            var select = "<select name='dataedit_new' datatype='"+ datatype +"' acc='"+ acc +"' id='dataedit_new_"+ datatype +"_"+ acc +"' class='acc_dataedit_apply_enter' style='width: 150px;'>"
        	select += "<option value='1' "
                if(dataedit == 1) select += " selected='selected' ";
                select += " >Tên cha của bạn là gì?</option> ";
        	select += "<option value='2' "
                if(dataedit == 2) select += " selected='selected' ";
                select += " >Tên ngôi trường đầu tiên của bạn là gì?</option> ";
        	select += "<option value='3' "
                if(dataedit == 3) select += " selected='selected' ";
                select += " >Người anh hùng trong thời thơ ấu của bạn là ai?</option> ";
        	select += "<option value='4' "
                if(dataedit == 4) select += " selected='selected' ";
                select += " >Khái niệm đẹp của bạn là gì?</option> ";
        	select += "<option value='5' "
                if(dataedit == 5) select += " selected='selected' ";
                select += " >Đội thể thao bạn thích nhất là đội nào?</option> ";
            select += "<option value='6' "
                if(dataedit == 6) select += " selected='selected' ";
                select += " >Vật mang lại may mắn thời học sinh của bạn là gì?</option> ";
            select += "<option value='7' "
                if(dataedit == 7) select += " selected='selected' ";
                select += " >Nơi bạn gặp vợ(chồng) của bạn nơi nào?</option> ";
            select += "<option value='8' "
                if(dataedit == 8) select += " selected='selected' ";
                select += " >Tên con thú cưng của bạn là gì?</option> ";
            select += "</select>"
            select += "<span id='dataedit_wait_"+ datatype +"_"+ acc +"'></span><a href='#' acc='"+ acc +"' dataedit_old='"+ dataedit +"' dataview='"+ dataview +"' class='acc_dataedit_apply' id='dataedit_apply_"+ datatype +"_"+ acc +"' datatype='"+ datatype +"'><img src='images/icon_apply.png' border=0></a> <a href='#' datatype='"+ datatype +"' acc='"+ acc +"' dataedit='"+ dataedit +"' dataview='"+ dataview +"' class='acc_dataedit_cancel' id='dataedit_cancel_"+ datatype +"_"+ acc +"'><img src='images/icon_cancel.png' border=0></a>"
            
            $("#edit_"+ datatype +"_"+ acc).html(select);
        } else if(datatype == "block") {
            var dataview = $(this).attr("dataview");
            
            var select = "<select name='dataedit_new' datatype='"+ datatype +"' acc='"+ acc +"' id='dataedit_new_"+ datatype +"_"+ acc +"' class='acc_dataedit_apply_enter' style='width: 90px;'>"
        	select += "<option value='0' "
                if(dataedit == 0) select += " selected='selected' ";
                select += " >Không Block</option> ";
        	select += "<option value='1' "
                if(dataedit == 1) select += " selected='selected' ";
                select += " >Block</option> ";
            select += "</select>"
            select += "<span id='blocktime_pos'></span><span id='dataedit_wait_"+ datatype +"_"+ acc +"'></span><a href='#' acc='"+ acc +"' dataedit_old='"+ dataedit +"' dataview='"+ dataview +"' class='acc_dataedit_apply' id='dataedit_apply_"+ datatype +"_"+ acc +"' datatype='"+ datatype +"'><img src='images/icon_apply.png' border=0></a> <a href='#' datatype='"+ datatype +"' acc='"+ acc +"' dataedit='"+ dataedit +"' dataview='"+ dataview +"' class='acc_dataedit_cancel' id='dataedit_cancel_"+ datatype +"_"+ acc +"'><img src='images/icon_cancel.png' border=0></a>"
            
            $("#edit_"+ datatype +"_"+ acc).html(select);
        } else {
            $("#edit_"+ datatype +"_"+ acc).html("<input name='dataedit_new' datatype='"+ datatype +"' acc='"+ acc +"' id='dataedit_new_"+ datatype +"_"+ acc +"' class='acc_dataedit_apply_enter' value='"+ dataedit +"' size='6'> <span id='dataedit_wait_"+ datatype +"_"+ acc +"'></span><a href='#' acc='"+ acc +"' dataedit_old='"+ dataedit +"' class='acc_dataedit_apply' id='dataedit_apply_"+ datatype +"_"+ acc +"' datatype='"+ datatype +"'><img src='images/icon_apply.png' border=0></a> <a href='#' datatype='"+ datatype +"' acc='"+ acc +"' dataedit='"+ dataedit +"' class='acc_dataedit_cancel' id='dataedit_cancel_"+ datatype +"_"+ acc +"'><img src='images/icon_cancel.png' border=0></a>");
        }
        $('.area_plus_minus').hide();
        $('#dataedit_new_'+ acc).focus();
        
        return false;
    });
        
    // Cancel Edit
    $('.acc_dataedit_cancel').live("click", function() {
        var datatype = $(this).attr("datatype");
        var acc = $(this).attr("acc");
        var dataedit = $(this).attr("dataedit");
        if(datatype == "quest") {
            var dataview = $(this).attr("dataview");
            
            $("#edit_"+ datatype +"_"+ acc).html(dataview + " <a href='#' acc='"+ acc +"' dataedit='"+ dataedit +"' dataview='"+ dataview +"' datatype='"+ datatype +"' class='acc_data_edit' style='position: absolute; top: 0px; right: 0px;'><img src='images/icon_edit.png' border='0'></a>");
        } else if(datatype == "block") {
            var dataview = $(this).attr("dataview");
            
            $("#edit_"+ datatype +"_"+ acc).html(dataview + " <a href='#' acc='"+ acc +"' dataedit='"+ dataedit +"' dataview='"+ dataview +"' datatype='"+ datatype +"' class='acc_data_edit' style='position: absolute; top: 0px; right: 0px;'><img src='images/icon_edit.png' border='0'></a>");
        } else {
            $("#edit_"+ datatype +"_"+ acc).html(dataedit + " <a href='#' acc='"+ acc +"' dataedit='"+ dataedit +"' datatype='"+ datatype +"' class='acc_data_edit' style='position: absolute; top: 0px; right: 0px;'><img src='images/icon_edit.png' border='0'></a>");
        }
        
        $('.area_plus_minus').show();
        return false;
    });
    
    // Apply Edit
    $('.acc_dataedit_apply').live('click', function () {
        var datatype = $(this).attr("datatype");
        var acc = $(this).attr("acc");
        var dataedit_old = $(this).attr("dataedit_old");
        var dataedit_new = $('#dataedit_new_'+ datatype +'_' + acc).val();
            
        if(dataedit_old == dataedit_new) {
            alert("Dữ liệu cần sửa giống đang sử dụng.");
        } else {
            
            var dataString = 'datatype='+ datatype +'&acc='+ acc +'&dataedit_new='+ dataedit_new +'&dataedit_old='+ dataedit_old;
            if(datatype == "block" && dataedit_new == 1) {
                var blocktime = $('#blocktime').val();
                dataString += '&blocktime='+ blocktime;
            }
            
            $.ajax({
               type: "POST",
               url: "ajax_action.php?ajax=acc_edit",
               data: dataString,
               cache: false,
               beforeSend: function() {
                    $('#dataedit_wait_'+ datatype +'_'+ acc).html("<img src='images/icon_loading.gif' border=0>");
               },
               success: function(data_reponse){
                    $('#dataedit_wait_'+ datatype +'_'+ acc).hide();
                    var data_reponse_split = data_reponse.split("|");
                    if(data_reponse_split[1] == 'OK') {
                        if(datatype == "quest") {
                            switch (dataedit_new){ 
                            	case '2':
                                    var data_view = "Tên ngôi trường đầu tiên của bạn là gì?";
                            	break;
                            
                            	case '3':
                                    var data_view = "Người anh hùng trong thời thơ ấu của bạn là ai?";
                            	break;
                            
                            	case '4':
                                    var data_view = "Khái niệm đẹp của bạn là gì?";
                            	break;
                            
                            	case '5':
                                    var data_view = "Đội thể thao bạn thích nhất là đội nào?";
                            	break;
                            
                            	case '6':
                                    var data_view = "Vật mang lại may mắn thời học sinh của bạn là gì?";
                            	break;
                            
                            	case '7':
                                    var data_view = "Nơi bạn gặp vợ(chồng) của bạn nơi nào?";
                            	break;
                            
                            	case '8':
                                    var data_view = "Tên con thú cưng của bạn là gì?";
                            	break;
                            
                            	default :
                                    var data_view = "Tên cha của bạn là gì?";
                            }
                            
                            $("#edit_"+ datatype +"_"+ acc).html(data_view + " <a href='#' acc='"+ acc +"' dataedit='"+ dataedit_new +"' datatype='"+ datatype +"' dataview='"+ data_view +"' class='acc_data_edit' style='position: absolute; top: 0px; right: 0px;'><img src='images/icon_edit.png' border='0'></a>");
                        } else if(datatype == "block") {
                            switch (dataedit_new){ 
                            	case '1':
                                    var time_block = data_reponse_split[2];
                                    var data_view = '<font color="red">Block</font> : '+ time_block;
                            	break;
                            
                            	default :
                                    var data_view = '<font color="blue">Không Block</font>';
                            }
                            
                            $("#edit_"+ datatype +"_"+ acc).html(data_view + " <a href='#' acc='"+ acc +"' dataedit='"+ dataedit_new +"' datatype='"+ datatype +"' dataview='"+ data_view +"' class='acc_data_edit' style='position: absolute; top: 0px; right: 0px;'><img src='images/icon_edit.png' border='0'></a>");
                        } else {
                            $("#edit_"+ datatype +"_"+ acc).html(dataedit_new + " <a href='#' acc='"+ acc +"' dataedit='"+ dataedit_new +"' datatype='"+ datatype +"' class='acc_data_edit' style='position: absolute; top: 0px; right: 0px;'><img src='images/icon_edit.png' border='0'></a>");
                        }
                    }
                    else {
                        $("#edit_"+ datatype +"_"+ acc).focus();
                        alert("Lỗi : " + data_reponse);
                    }
                }
            });
        }
        
        $('.area_plus_minus').show();
        
        return false;
    });
        
    // Apply Edit Enter OR ESC
    $('.acc_dataedit_apply_enter').live('keyup', function (e) { 
        var datatype = $(this).attr("datatype");
        var acc = $(this).attr("acc");
        if(e.keyCode == 13) {
             $('#dataedit_apply_'+ datatype +'_'+ acc).click();
             $('#dataedit_'+ datatype +'_'+ acc).focus();
         } else if(e.keyCode == 27) {
             $('#dataedit_cancel_'+ datatype +'_'+ acc).click();
         }
         
         return false;
    });
        
    // Change Block
    $('select[datatype=block]').live('change', function () { 
        var block = $(this).val();
        if(block == 1) {
            $('#blocktime_pos').html("<input type='text' id='blocktime' size='1' value='0' />");
        } else {
            $('#blocktime_pos').html("");
        }
         
         return false;
    });
    
    $('#plus_minus_process').live('click', function () {
        var acc = $(this).attr("acc");
        var datatype = $(this).attr("datatype");
        var plusminus = $('input[name=plusminus]:checked').val();
        var value = $('#value').val();
            
        var dataString = 'datatype='+ datatype +'&acc='+ acc +'&plusminus='+ plusminus +'&value='+ value;
        
        $.ajax({
           type: "POST",
           url: "ajax_action.php?ajax=acc_plus_minus",
           data: dataString,
           cache: false,
           beforeSend: function() {
                $('#plus_minus_wait').html("<img src='images/icon_loading.gif' border=0>");
           },
           success: function(data_reponse){
                $('#plus_minus_wait').html("");
                var data_reponse_split = data_reponse.split("|");
                if(data_reponse_split[1] == 'OK') {
                    $("#plus_minus_content").html("<center><font color='blue'>"+ data_reponse_split[2] +"</font></center>");
                } else {
                    $("#value").focus();
                    alert("Lỗi : " + data_reponse);
                }
            }
        });
        
        return false;
    });
    
    $('.block_view').live({
        mouseenter: function () {
            $(this).children('.block_data').css('display', 'inline');
        },
        mouseleave: function () {
            $(this).children('.block_data').css('display', 'none');
        }
    });
    
    $('#block_process').live('click', function () {
        var acc = $(this).attr("acc");
        var block_type = $('input[name=block_type]:checked').val();
        var day_block = $('#day_block').val();
        var admin = $('#admin').val();
        var block_des = $('#block_des').val();
            
        var dataString = 'block_type='+ block_type +'&acc='+ acc +'&day_block='+ day_block +'&admin='+ admin +'&block_des='+ block_des;
        
        $.ajax({
           type: "POST",
           url: "ajax_action.php?ajax=acc_block",
           data: dataString,
           cache: false,
           beforeSend: function() {
                $('#block_wait').html("<img src='images/icon_loading.gif' border=0>");
           },
           success: function(data_reponse){
                $('#block_wait').html("");
                var data_reponse_split = data_reponse.split("|");
                if(data_reponse_split[1] == 'OK') {
                    $("#block_content").html("<center><font color='blue'>"+ data_reponse_split[2] +"</font></center>");
                    if(block_type == 1) {
                        $("#edit_block_"+ acc).html('<font color="red"><strong>Block</strong></font> : '+ data_reponse_split[3]);
                        $(".block_extra").css('display', 'none');
                        $('.block_acc').attr('href', 'acc_block.php?acc='+ acc +'&block_status=1');
                    } else {
                        $("#edit_block_"+ acc).html('<font color="blue">Không Block</font>');
                        $(".block_extra").css('display', 'inline');
                        $('.block_acc').attr('href', 'acc_block.php?acc='+ acc +'&block_status=0');
                    }
                } else {
                    $("#day_block").focus();
                    alert("Lỗi : " + data_reponse);
                }
            }
        });
        
        return false;
    });
    //****************************** Edit Acc End *******************************
    
    //****************************** Search Char *******************************
    $("#submit_search_char").live('click', function(){
        var char = $("#char").val();
        var search_type = $('input[name=search_type]:checked').val();
        var dataString = "char=" + char + "&search_type=" + search_type;
        
         $.ajax({
           type: "GET",
           url: "char_search_result.php",
           data: dataString,
           cache: false,
           beforeSend: function() {
                $("#search_loading").html("<img src='images/loading1.gif' border='0' />");
                $("#submit_search_char").attr('disabled','disabled');
           },
           complete: function() {
                $("#search_loading").html("");
            
           },
           success: function(html) {
                $("#search_result").html(html);
                $("#submit_search_char").removeAttr('disabled');
           }
        });
        
        return false;
    });
    
    // Enter Search Char Result
        $('#char').live('keyup', function (e) { 
            if(e.keyCode == 13) {
                 $('#submit_search_char').click();
             }
             
             return false;
        });
    
    $(".acc_search_char").live('click', function(){
        var char = $(this).attr('charname');
        var dataString = "char=" + char + "&search_type=1";
        
         $.ajax({
           type: "GET",
           url: "char_search_result.php",
           data: dataString,
           cache: false,
           beforeSend: function() {
                $("#acc_search_char_"+ char).html("<img src='images/loading1.gif' border='0' />");
           },
           complete: function() {
                $("#acc_search_char_"+ char).html("");
            
           },
           success: function(html) {
                $("#search_result").html(html);
                $('#char').val(char);
           }
        });
        
        return false;
    });
    //****************************** Search Char End *******************************
    
    //****************************** Edit Char Begin *******************************
    // Edit
        $('.char_data_edit').live("click", function() 
        {
            var datatype = $(this).attr("datatype");
            var charname = $(this).attr("charname");
            var dataedit = $(this).attr("dataedit");
            
            if(datatype == "class") {
                var dataview = $(this).attr("dataview");
                
                var select = "<select name='dataedit_new' datatype='"+ datatype +"' charname='"+ charname +"' id='dataedit_new_"+ datatype +"_"+ charname +"' class='char_dataedit_apply_enter' style='width: 150px;'>"
            	select += "<option value='0' "
                    if(dataedit == 0) select += " selected='selected' ";
                    select += " >Dark Wizard (DW 1)</option> ";
                select += "<option value='1' "
                    if(dataedit == 1) select += " selected='selected' ";
                    select += " >Soul Master (DW 2)</option> ";
            	select += "<option value='2' "
                    if(dataedit == 2) select += " selected='selected' ";
                    select += " >Grand Master (DW 3)</option> ";
                    
            	select += "<option value='16' "
                    if(dataedit == 16) select += " selected='selected' ";
                    select += " >Dark Knight (DK 1)</option> ";
            	select += "<option value='17' "
                    if(dataedit == 17) select += " selected='selected' ";
                    select += " >Blade Knight (DK 2)</option> ";
            	select += "<option value='18' "
                    if(dataedit == 18) select += " selected='selected' ";
                    select += " >Blade Master (DK 3)</option> ";
                    
                select += "<option value='32' "
                    if(dataedit == 32) select += " selected='selected' ";
                    select += " >Elf (ELF 1)</option> ";
            	select += "<option value='33' "
                    if(dataedit == 33) select += " selected='selected' ";
                    select += " >Muse Elf (ELF 2)</option> ";
            	select += "<option value='34' "
                    if(dataedit == 34) select += " selected='selected' ";
                    select += " >Hight Elf (ELF 3)</option> ";
            	
                select += "<option value='48' "
                    if(dataedit == 48) select += " selected='selected' ";
                    select += " >Magic Gladiator (MG 1)</option> ";
           	    select += "<option value='49' "
                    if(dataedit == 49) select += " selected='selected' ";
                    select += " >Duel Master (MG 3)</option> ";
            	
                select += "<option value='64' "
                    if(dataedit == 64) select += " selected='selected' ";
                    select += " >DarkLord (DL 1)</option> ";
           	    select += "<option value='65' "
                    if(dataedit == 65) select += " selected='selected' ";
                    select += " >Lord Emperor (DL 3)</option> ";
           	    
                select += "<option value='80' "
                    if(dataedit == 80) select += " selected='selected' ";
                    select += " >Sumoner (SUM 1)</option> ";
           	    select += "<option value='81' "
                    if(dataedit == 81) select += " selected='selected' ";
                    select += " >Bloody Summoner (SUM 2)</option> ";
           	    select += "<option value='82' "
                    if(dataedit == 82) select += " selected='selected' ";
                    select += " >Dimension Master (SUM 3)</option> ";
           	    
                select += "<option value='96' "
                    if(dataedit == 96) select += " selected='selected' ";
                    select += " >Rage Fighter (RF 1)</option> ";
           	    select += "<option value='97' "
                    if(dataedit == 97) select += " selected='selected' ";
                    select += " >First Class (RF 3)</option> ";
           	    
                select += "</select>"
                select += "<span id='dataedit_wait_"+ datatype +"_"+ charname +"'></span><a href='#' charname='"+ charname +"' dataedit_old='"+ dataedit +"' dataview='"+ dataview +"' class='char_dataedit_apply' id='dataedit_apply_"+ datatype +"_"+ charname +"' datatype='"+ datatype +"'><img src='images/icon_apply.png' border=0></a> <a href='#' datatype='"+ datatype +"' charname='"+ charname +"' dataedit='"+ dataedit +"' dataview='"+ dataview +"' class='char_dataedit_cancel' id='dataedit_cancel_"+ datatype +"_"+ charname +"'><img src='images/icon_cancel.png' border=0></a>"
                
                $("#edit_"+ datatype +"_"+ charname).html(select);
            } else if(datatype == "stat") {
                var dataview = $(this).attr("dataview");
                
                var select = "<select name='dataedit_new' datatype='"+ datatype +"' charname='"+ charname +"' id='dataedit_new_"+ datatype +"_"+ charname +"' class='char_dataedit_apply_enter' style='width: 70px;'>"
            	select += "<option value='0' "
                    if(dataedit == 0) select += " selected='selected' ";
                    select += " >Bình thường</option> ";
                
                select += "<option value='32' "
                    if(dataedit == 32) select += " selected='selected' ";
                    select += " >Game Master</option> ";
                
                select += "<option value='99' "
                    if(dataedit == 99) select += " selected='selected' ";
                    select += " >Admin Block (99)</option> ";
                
                select += "</select>"
                select += "<span id='dataedit_wait_"+ datatype +"_"+ charname +"'></span><a href='#' charname='"+ charname +"' dataedit_old='"+ dataedit +"' dataview='"+ dataview +"' class='char_dataedit_apply' id='dataedit_apply_"+ datatype +"_"+ charname +"' datatype='"+ datatype +"'><img src='images/icon_apply.png' border=0></a> <a href='#' datatype='"+ datatype +"' charname='"+ charname +"' dataedit='"+ dataedit +"' dataview='"+ dataview +"' class='char_dataedit_cancel' id='dataedit_cancel_"+ datatype +"_"+ charname +"'><img src='images/icon_cancel.png' border=0></a>"
                $("#edit_"+ datatype +"_"+ charname).html(select);
            } else if(datatype == "SCFSealItem") {
                var dataview = $(this).attr("dataview");
                
                var select = "<select name='dataedit_new' datatype='"+ datatype +"' charname='"+ charname +"' id='dataedit_new_"+ datatype +"_"+ charname +"' class='char_dataedit_apply_enter' style='width: 150px;'>"
            	select += "<option value='6699' "
                    if(dataedit == 6699) select += " selected='selected' ";
                    select += " >Bùa tăng Exp</option> ";
                
                select += "<option value='6700' "
                    if(dataedit == 6700) select += " selected='selected' ";
                    select += " >Bùa Thiên Sứ</option> ";
                
                select += "<option value='6701' "
                    if(dataedit == 6701) select += " selected='selected' ";
                    select += " >Bùa không tăng Exp</option> ";
                
                select += "<option value='6749' "
                    if(dataedit == 6749) select += " selected='selected' ";
                    select += " >Bùa tăng Exp Master</option> ";
                
                select += "</select>"
                select += "<span id='dataedit_wait_"+ datatype +"_"+ charname +"'></span><a href='#' charname='"+ charname +"' dataedit_old='"+ dataedit +"' dataview='"+ dataview +"' class='char_dataedit_apply' id='dataedit_apply_"+ datatype +"_"+ charname +"' datatype='"+ datatype +"'><img src='images/icon_apply.png' border=0></a> <a href='#' datatype='"+ datatype +"' charname='"+ charname +"' dataedit='"+ dataedit +"' dataview='"+ dataview +"' class='char_dataedit_cancel' id='dataedit_cancel_"+ datatype +"_"+ charname +"'><img src='images/icon_cancel.png' border=0></a>"
                $("#edit_"+ datatype +"_"+ charname).html(select);
            } else if(datatype == "SCFScrollItem") {
                var dataview = $(this).attr("dataview");
                
                var select = "<select name='dataedit_new' datatype='"+ datatype +"' charname='"+ charname +"' id='dataedit_new_"+ datatype +"_"+ charname +"' class='char_dataedit_apply_enter' style='width: 150px;'>"
            	select += "<option value='7240' "
                    if(dataedit == 7240) select += " selected='selected' ";
                    select += " >Bùa tốc độ tấn công</option> ";
                
                select += "<option value='7242' "
                    if(dataedit == 7242) select += " selected='selected' ";
                    select += " >Bùa tăng sát thương</option> ";
                
                select += "<option value='7243' "
                    if(dataedit == 7243) select += " selected='selected' ";
                    select += " >Bùa tăng ma thuật</option> ";
                
                select += "<option value='7266' "
                    if(dataedit == 7266) select += " selected='selected' ";
                    select += " >Bùa tăng tấn công</option> ";
                
                select += "<option value='7245' "
                    if(dataedit == 7245) select += " selected='selected' ";
                    select += " >Bùa tăng Mana</option> ";
                
                select += "<option value='7244' "
                    if(dataedit == 7244) select += " selected='selected' ";
                    select += " >Bùa tăng HP</option> ";
                
                select += "</select>"
                select += "<span id='dataedit_wait_"+ datatype +"_"+ charname +"'></span><a href='#' charname='"+ charname +"' dataedit_old='"+ dataedit +"' dataview='"+ dataview +"' class='char_dataedit_apply' id='dataedit_apply_"+ datatype +"_"+ charname +"' datatype='"+ datatype +"'><img src='images/icon_apply.png' border=0></a> <a href='#' datatype='"+ datatype +"' charname='"+ charname +"' dataedit='"+ dataedit +"' dataview='"+ dataview +"' class='char_dataedit_cancel' id='dataedit_cancel_"+ datatype +"_"+ charname +"'><img src='images/icon_cancel.png' border=0></a>"
                $("#edit_"+ datatype +"_"+ charname).html(select);
            } else {
                $("#edit_"+ datatype +"_"+ charname).html("<input name='dataedit_new' datatype='"+ datatype +"' charname='"+ charname +"' id='dataedit_new_"+ datatype +"_"+ charname +"' class='char_dataedit_apply_enter' value='"+ dataedit +"' size='6'> <span id='dataedit_wait_"+ datatype +"_"+ charname +"'></span><a href='#' charname='"+ charname +"' dataedit_old='"+ dataedit +"' class='char_dataedit_apply' id='dataedit_apply_"+ datatype +"_"+ charname +"' datatype='"+ datatype +"'><img src='images/icon_apply.png' border=0></a> <a href='#' datatype='"+ datatype +"' charname='"+ charname +"' dataedit='"+ dataedit +"' class='char_dataedit_cancel' id='dataedit_cancel_"+ datatype +"_"+ charname +"'><img src='images/icon_cancel.png' border=0></a>");
            }
            
            $('#dataedit_new_'+ charname).focus();
            
            return false;
        });
        
    // Cancel Edit
        $('.char_dataedit_cancel').live("click", function() 
        {
            var datatype = $(this).attr("datatype");
            var charname = $(this).attr("charname");
            var dataedit = $(this).attr("dataedit");
            if(datatype == "class" || datatype == "stat" || datatype == "SCFSealItem" || datatype == "SCFScrollItem") {
                var dataview = $(this).attr("dataview");
                
                $("#edit_"+ datatype +"_"+ charname).html(dataview + " <a href='#' charname='"+ charname +"' dataedit='"+ dataedit +"' dataview='"+ dataview +"' datatype='"+ datatype +"' class='char_data_edit' style='position: absolute; top: 0px; right: 0px;'><img src='images/icon_edit.png' border='0'></a>");
            } else {
                $("#edit_"+ datatype +"_"+ charname).html(dataedit + " <a href='#' charname='"+ charname +"' dataedit='"+ dataedit +"' datatype='"+ datatype +"' class='char_data_edit' style='position: absolute; top: 0px; right: 0px;'><img src='images/icon_edit.png' border='0'></a>");
            }
            return false;
        });
    
    // Apply Edit
        $('.char_dataedit_apply').live('click', function () {
            var datatype = $(this).attr("datatype");
            var charname = $(this).attr("charname");
            var dataedit_old = $(this).attr("dataedit_old");
            var dataedit_new = $('#dataedit_new_'+ datatype +'_' + charname).val();
            
            if(dataedit_old == dataedit_new) {
                alert("Dữ liệu cần sửa giống đang sử dụng.");
            } else {
                
                var dataString = 'datatype='+ datatype + '&charname='+ charname + '&dataedit_new='+ dataedit_new + '&dataedit_old='+ dataedit_old;
                $.ajax({
                   type: "POST",
                   url: "ajax_action.php?ajax=char_edit",
                   data: dataString,
                   cache: false,
                   beforeSend: function() {
                        $('#dataedit_wait_'+ datatype +'_'+ charname).html("<img src='images/icon_loading.gif' border=0>");
                   },
                   success: function(html){
                        $('#dataedit_wait_'+ datatype +'_'+ charname).hide();
                        if( html == "OK") {
                            if(datatype == "class") {
                                switch (dataedit_new){ 
                                	case '0':
                                        var data_view = "Dark Wizard (DW 1)";
                                	break;
                                
                                	case '1':
                                        var data_view = "Soul Master (DW 2)";
                                	break;
                                
                                	case '2':
                                        var data_view = "Grand Master (DW 3)";
                                	break;
                                
                                	case '16':
                                        var data_view = "Dark Knight (DK 1)";
                                	break;
                                
                                	case '17':
                                        var data_view = "Blade Knight (DK 2)";
                                	break;
                                
                                	case '18':
                                        var data_view = "Blade Master (DK 3)";
                                	break;
                                    
                                    case '32':
                                        var data_view = "Elf (ELF 1)";
                                	break;
                                
                                	case '33':
                                        var data_view = "Muse Elf (ELF 2)";
                                	break;
                                
                                	case '34':
                                        var data_view = "Hight Elf (ELF 3)";
                                	break;
                                
                                	case '48':
                                        var data_view = "Magic Gladiator (MG 1)";
                                	break;
                                
                                	case '49':
                                        var data_view = "Duel Master (MG 3)";
                                	break;
                                
                                	case '64':
                                        var data_view = "DarkLord (DL 1)";
                                	break;
                                
                                	case '65':
                                        var data_view = "Lord Emperor (DL 3)";
                                	break;
                                
                                	case '80':
                                        var data_view = "Sumoner (SUM 1)";
                                	break;
                                
                                	case '81':
                                        var data_view = "Bloody Summoner (SUM 2)";
                                	break;
                                
                                	case '82':
                                        var data_view = "Dimension Master (SUM 3)";
                                	break;
                                
                                	case '96':
                                        var data_view = "Rage Fighter (RF 1)";
                                	break;
                                
                                	case '97':
                                        var data_view = "First Class (RF 3)";
                                	break;
                                }
                                
                                $("#edit_"+ datatype +"_"+ charname).html(data_view + " <a href='#' charname='"+ charname +"' dataedit='"+ dataedit_new +"' datatype='"+ datatype +"' dataview='"+ data_view +"' class='char_data_edit' style='position: absolute; top: 0px; right: 0px;'><img src='images/icon_edit.png' border='0'></a>");
                                
                            } else if(datatype == "stat") {
                                switch (dataedit_new){ 
                                	case '0':
                                        var data_view = "Bình thường";
                                	break;
                                
                                	case '32':
                                        var data_view = "Game Master";
                                	break;
                                
                                	case '99':
                                        var data_view = "Admin Block (99)";
                                	break;
                                }
                                
                                $("#edit_"+ datatype +"_"+ charname).html(data_view + " <a href='#' charname='"+ charname +"' dataedit='"+ dataedit_new +"' datatype='"+ datatype +"' dataview='"+ data_view +"' class='char_data_edit' style='position: absolute; top: 0px; right: 0px;'><img src='images/icon_edit.png' border='0'></a>");
                            } else if(datatype == "SCFSealItem") {
                                switch (dataedit_new){ 
                                	case '6699':
                                        var data_view = "Bùa tăng Exp";
                                	break;
                                
                                	case '6700':
                                        var data_view = "Bùa Thiên Sứ";
                                	break;
                                
                                	case '6701':
                                        var data_view = "Bùa không tăng Exp";
                                	break;
                                
                                	case '6749':
                                        var data_view = "Bùa tăng Exp Master";
                                	break;
                                }
                                
                                $("#edit_"+ datatype +"_"+ charname).html(data_view + " <a href='#' charname='"+ charname +"' dataedit='"+ dataedit_new +"' datatype='"+ datatype +"' dataview='"+ data_view +"' class='char_data_edit' style='position: absolute; top: 0px; right: 0px;'><img src='images/icon_edit.png' border='0'></a>");
                            } else if(datatype == "SCFScrollItem") {
                                switch (dataedit_new){ 
                                	case '7240':
                                        var data_view = "Bùa tốc độ tấn công";
                                	break;
                                
                                	case '7242':
                                        var data_view = "Bùa tăng sát thương";
                                	break;
                                
                                	case '7243':
                                        var data_view = "Bùa tăng ma thuật";
                                	break;
                                
                                	case '7266':
                                        var data_view = "Bùa tăng tấn công";
                                	break;
                                
                                	case '7245':
                                        var data_view = "Bùa tăng Mana";
                                	break;
                                
                                	case '7244':
                                        var data_view = "Bùa tăng HP";
                                	break;
                                }
                                
                                $("#edit_"+ datatype +"_"+ charname).html(data_view + " <a href='#' charname='"+ charname +"' dataedit='"+ dataedit_new +"' datatype='"+ datatype +"' dataview='"+ data_view +"' class='char_data_edit' style='position: absolute; top: 0px; right: 0px;'><img src='images/icon_edit.png' border='0'></a>");
                            } else {
                                $("#edit_"+ datatype +"_"+ charname).html(dataedit_new + " <a href='#' charname='"+ charname +"' dataedit='"+ dataedit_new +"' datatype='"+ datatype +"' class='char_data_edit' style='position: absolute; top: 0px; right: 0px;'><img src='images/icon_edit.png' border='0'></a>");
                            }
                        }
                        else {
                            $("#edit_"+ datatype +"_"+ charname).focus();
                            alert("Lỗi : " + html);
                        }
                    }
                });
            }
            
            return false;
        });
        
    // Apply Edit Enter OR ESC
        $('.char_dataedit_apply_enter').live('keyup', function (e) { 
            var datatype = $(this).attr("datatype");
            var charname = $(this).attr("charname");
            if(e.keyCode == 13) {
                 $('#dataedit_apply_'+ datatype +'_'+ charname).click();
                 $('#dataedit_'+ datatype +'_'+ charname).focus();
             } else if(e.keyCode == 27) {
                 $('#dataedit_cancel_'+ datatype +'_'+ charname).click();
             }
             
             return false;
        });
    //****************************** Edit Char End *******************************
    
    //****************************** Search IP *******************************
    $("#submit_search_ip").live('click', function(){
        var ip = $("#ip").val();
        var dataString = "ip=" + ip;
        
         $.ajax({
           type: "GET",
           url: "ip_search_result.php",
           data: dataString,
           cache: false,
           beforeSend: function() {
                $("#search_loading").html("<img src='images/loading1.gif' border='0' />");
                $("#submit_search_ip").attr('disabled','disabled');
           },
           complete: function() {
                $("#search_loading").html("");
            
           },
           success: function(html) {
                $("#search_result").html(html);
                $("#submit_search_ip").removeAttr('disabled');
           }
        });
        
        return false;
    });
    
    // Enter Search Acc Result
        $('#ip').live('keyup', function (e) { 
            if(e.keyCode == 13) {
                 $('#submit_search_ip').click();
             }
             
             return false;
        });
    
    $(".ip_search_acc").live('click', function(){
        var ip = $(this).attr('ip');
        var pos = $(this).attr('pos');
        
        var dataString = "ip=" + ip;
         $.ajax({
           type: "GET",
           url: "ip_search_result.php",
           data: dataString,
           cache: false,
           beforeSend: function() {
                $("#ip_search_loading_"+ pos).html("<img src='images/loading1.gif' border='0' />");
           },
           complete: function() {
                $("#ip_search_loading_"+ pos).html("");
           },
           success: function(html) {
                $("#search_result").html(html);
                $('#ip').val(ip);
           }
        });
        
        return false;
    });
    //****************************** Search IP End *******************************
});


