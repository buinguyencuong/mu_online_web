<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
	include("security.php");
include_once('../config.php');
$title = "Dungeon";
$server_dungeon = "HT-5";


SESSION_start();



$char_dun_query = "SELECT Name, Relifes, Resets, Class, MapPosX, MapPosY FROM Character JOIN AccountCharacter ON MapNumber=1 AND Character.AccountID collate DATABASE_DEFAULT = AccountCharacter.Id collate DATABASE_DEFAULT AND Character.Name=AccountCharacter.GameIDC JOIN MEMB_STAT ON Character.AccountID collate DATABASE_DEFAULT = MEMB_STAT.memb___id collate DATABASE_DEFAULT AND ConnectStat=1 AND ServerName='$server_dungeon' AND MapPosY>140";
$char_dun_result = $db->Execute($char_dun_query);
$count_dun = $char_dun_result->NumRows();
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Refresh" content="10" />
<title><?php echo $count_dun . "-" . $title; ?></title>
<link href="css/tooltip.css" rel="stylesheet" type="text/css" />
</head>
<body bgcolor="#F9E7CF">
<div id="dhtmltooltip"></div>
<img id="dhtmlpointer" src="images/tooltiparrow.gif">

<table width="100%" cellspacing="1" cellpadding="3" border="0" bgcolor="#0000ff">
<tr bgcolor="#ffffcc" >
	<td align="center" colspan="7">Tổng đang ở Dungeon : <?php echo $count_dun; ?></td>
</tr>
<tr bgcolor="#ffffcc" >
	<td align="center">#</td>
	<td align="center">Nhân vật</td>
	<td align="center">ReLife</td>
	<td align="center">ReSet</td>
	<td align="center">Class</td>
	<td align="center">Server</td>
	<td align="center">Map</td>
</tr>


<?php
$stt = 0;
while($char_dun_fetch = $char_dun_result->FetchRow()) {
    switch($char_dun_fetch[3])
    {
        case 0: $ClassChar ='Dark Wizard';        break;
        case 1: $ClassChar ='Soul Master';        break;
        case 2: 
        case 3: $ClassChar ='Grand Master';        break;
        case 16: $ClassChar ='Dark Knight';        break;
        case 17: $ClassChar ='Blade Knight';        break;
        case 18: 
        case 19: $ClassChar ='Blade Master';        break;
        case 32: $ClassChar ='Elf';        break;
        case 33: $ClassChar ='Muse Elf';        break;
        case 34: 
        case 35: $ClassChar ='Hight Elf';       break;
        case 48: $ClassChar ='Magic Gladiator';        break;
        case 49: 
        case 50: $ClassChar ='Duel Master';        break;
        case 64: $ClassChar ='DarkLord';        break;
        case 65: 
        case 66: $ClassChar ='Lord Emperor';        break;
        case 80: $ClassChar ='Sumoner';        break;
        case 81: $ClassChar ='Bloody Summoner';        break;
        case 82: 
        case 83: $ClassChar ='Dimension Master';        break;
        case 96: $ClassChar ='Rage Fighter';        break;
        case 97: 
        case 98: $ClassChar ='First Class';        break;
        default: $ClassChar ='Không xác định Class';
    }
    
    $stt++;
    echo"<tr bgcolor='#F9E7CF' >
		<td align='center'>$stt</td>
		<td align='center'>$char_dun_fetch[0]</td>
		<td align='center'>$char_dun_fetch[1]</td>
		<td align='center'>$char_dun_fetch[2]</td>
		<td align='center'>$ClassChar</td>
		<td align='center'>$server_dungeon</td>
		<td align='center'>Dungeon: $char_dun_fetch[4] , $char_dun_fetch[5]</td>
	</tr>";
}
$db->Close();
?>
</table>
<script type="text/javascript" src="js/tooltip.js"></script>
</body>
</html>