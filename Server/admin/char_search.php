<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
include_once ("security.php");
include ('../config.php');
include ('function.php');
include ('../config/config_thehe.php');
$title = "Admin - Manage Characater";
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<title><?php echo $title; ?></title>
<meta http-equiv=Content-Type content="text/html; charset=utf-8" />
</head>
<body bgcolor="#F9E7CF">
<?php require ('linktop.php'); ?>

<script type="text/javascript" src="fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" href="fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />

<table width="993" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <fieldset>
                <legend>Edit Character</legend>
                <table width="400" border="0" align="center" cellpadding="3" cellspacing="2">
                    <tr>
                        <td align="right">Nhân vật</td>
                        <td><input type="text" name="char" id="char" /></td>
                    </tr>
                    <tr>
                        <td align="right">Kiểu tìm</td>
                        <td>
                            <input type="radio" name="search_type" value="1" checked="checked" /> Chính xác
                            <input type="radio" name="search_type" value="0" /> Gần đúng
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <input type="button" id="submit_search_char" value="Tìm nhân vật" />
                            <span id="search_loading"></span>
                        </td>
                    </tr>
                </table>
                
                
                <div id="search_result"></div>
            </fieldset>
        </td>
    </tr>
</table>
</body>
</html>
<?php
$db->Close();
?>