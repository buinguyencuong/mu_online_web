<?php

/**
 * @author NetBanBe
 * @copyright 2014
 */

include_once ("security.php");
include ('../config.php');
include ('function.php');
include ('../config/config_thehe.php');
session_start();

$char = $_GET['char'];
$search_type = $_GET['search_type'];

$char_search_q = "SELECT AccountID, Name, GameIDC, ConnectStat, ServerName, MEMB_STAT.IP, ConnectTM, DisConnectTM, cLevel, LevelUpPoint, pointdutru, Class, Strength, Dexterity, Vitality, Energy, Leadership, Money, MapNumber, MapPosX, MapPosY, PkCount, PkLevel, PkTime, CtlCode, Quest, Resets, Relifes, Resets_Time, khoado, makhoado, UyThac, uythacoffline_stat, uythacoffline_time, PointUyThac, point_event, SCFMasterLevel, SCFMasterPoints, SCFMarried, SCFMarryHusbandWife, SCFPCPoints, Top50, DGT_Time, SCFSealItem, SCFSealTime, SCFScrollItem, SCFScrollTime, PointUyThac_Event, nbbtuluyen_str_point, nbbtuluyen_agi_point, nbbtuluyen_vit_point, nbbtuluyen_ene_point, nbbtuluyen_point, nbbsongtu_lv, nbbsongtu_point, nbbhoanhao_point, nbbcuonghoa_point, ErrorSubBlock, nbb_pmaster, nbbtuluyen_str, nbbtuluyen_str_exp, nbbtuluyen_str_cp, nbbtuluyen_agi, nbbtuluyen_agi_exp, nbbtuluyen_agi_cp, nbbtuluyen_vit, nbbtuluyen_vit_exp, nbbtuluyen_vit_cp, nbbtuluyen_ene, nbbtuluyen_ene_exp, nbbtuluyen_ene_cp FROM Character JOIN AccountCharacter ON Character.AccountID = AccountCharacter.Id JOIN MEMB_STAT ON Character.AccountID = MEMB_STAT.memb___id ";
if($search_type == 1) $char_search_q .= " AND Name = '$char'";
else $char_search_q .= " AND Name like '%{$char}%'";

$char_search_r = $db->Execute($char_search_q);
    check_queryerror($char_search_q, $char_search_r);

$char_arr = array(); 
$char_search_count = 0;   
while($char_search_f = $char_search_r->FetchRow()) {
    $char_arr[$char_search_count]['acc'] = $char_search_f[0];
    $char_arr[$char_search_count]['name'] = $char_search_f[1];
    $char_arr[$char_search_count]['gameidc'] = $char_search_f[2];
    $char_arr[$char_search_count]['connect_stat'] = $char_search_f[3];
    $char_arr[$char_search_count]['online'] = "<font color='silver'>OFF</font>";
    $char_arr[$char_search_count]['server_name'] = $char_search_f[4];
    $char_arr[$char_search_count]['ipsv'] = $char_search_f[5];
    $char_arr[$char_search_count]['connect_time'] = $char_search_f[6];
    $char_arr[$char_search_count]['disconnect_Time'] = $char_search_f[7];
    $char_arr[$char_search_count]['level'] = $char_search_f[8];
    $char_arr[$char_search_count]['point'] = $char_search_f[9];
    $char_arr[$char_search_count]['pointdutru'] = $char_search_f[10];
    $char_arr[$char_search_count]['class'] = $char_search_f[11];
    $char_arr[$char_search_count]['str'] = $char_search_f[12];
        if($char_arr[$char_search_count]['str'] < 0) $char_arr[$char_search_count]['str'] = $char_arr[$char_search_count]['str'] + 65536;
    $char_arr[$char_search_count]['dex'] = $char_search_f[13];
        if($char_arr[$char_search_count]['dex'] < 0) $char_arr[$char_search_count]['dex'] = $char_arr[$char_search_count]['dex'] + 65536;
    $char_arr[$char_search_count]['vit'] = $char_search_f[14];
        if($char_arr[$char_search_count]['vit'] < 0) $char_arr[$char_search_count]['vit'] = $char_arr[$char_search_count]['vit'] + 65536;
    $char_arr[$char_search_count]['ene'] = $char_search_f[15];
        if($char_arr[$char_search_count]['ene'] < 0) $char_arr[$char_search_count]['ene'] = $char_arr[$char_search_count]['ene'] + 65536;
    $char_arr[$char_search_count]['leadership'] = $char_search_f[16];
        if($char_arr[$char_search_count]['leadership'] < 0) $char_arr[$char_search_count]['leadership'] = $char_arr[$char_search_count]['leadership'] + 65536;
    $char_arr[$char_search_count]['money'] = $char_search_f[17];
    $char_arr[$char_search_count]['mapnum'] = $char_search_f[18];
    $char_arr[$char_search_count]['map_x'] = $char_search_f[19];
    $char_arr[$char_search_count]['map_y'] = $char_search_f[20];
    $char_arr[$char_search_count]['pkcount'] = $char_search_f[21];
    $char_arr[$char_search_count]['pklevel'] = $char_search_f[22];
    $char_arr[$char_search_count]['pktime'] = $char_search_f[23];
    $char_arr[$char_search_count]['ctlcode'] = $char_search_f[24];
    $char_arr[$char_search_count]['quest'] = $char_search_f[25];
    $char_arr[$char_search_count]['rs'] = $char_search_f[26];
    $char_arr[$char_search_count]['rl'] = $char_search_f[27];
    $char_arr[$char_search_count]['rstime'] = $char_search_f[28];
        $char_arr[$char_search_count]['rstime_view'] = date('H:i d/m', $char_arr[$char_search_count]['rstime']);
    $char_arr[$char_search_count]['khoado'] = $char_search_f[29];
        if($char_arr[$char_search_count]['khoado'] == 1) $char_arr[$char_search_count]['khoado_view'] = "<strong>Đã khóa</strong>";
        else $char_arr[$char_search_count]['khoado_view'] = "Chưa khóa";
    $char_arr[$char_search_count]['makhoado'] = $char_search_f[30];
    $char_arr[$char_search_count]['uythac'] = $char_search_f[31];
        if ($row[16] == 1) $char_arr[$char_search_count]['uythac_on'] = 'Bật Ủy Thác ON';
        else $char_arr[$char_search_count]['uythac_on'] = 'Tắt Ủy Thác ON';
    $char_arr[$char_search_count]['uythacoffline_stat'] = $char_search_f[32];
    $char_arr[$char_search_count]['uythacoffline_time'] = $char_search_f[33];
    $char_arr[$char_search_count]['pointuythac'] = $char_search_f[34];
    $char_arr[$char_search_count]['pointevent'] = $char_search_f[35];
    $char_arr[$char_search_count]['scfmaster_level'] = $char_search_f[36];
    $char_arr[$char_search_count]['scfmaster_point'] = $char_search_f[37];
    $char_arr[$char_search_count]['scf_married'] = $char_search_f[38];
    $char_arr[$char_search_count]['SCFMarryHusbandWife'] = $char_search_f[39];
    $char_arr[$char_search_count]['SCFPCPoints'] = $char_search_f[40];
    $char_arr[$char_search_count]['Top50'] = $char_search_f[41];
        if($char_arr[$char_search_count]['Top50'] == 0) $char_arr[$char_search_count]['Top50'] = ">500";
    $char_arr[$char_search_count]['DGT_Time'] = $char_search_f[42];
        if(isset($char_arr[$char_search_count]['DGT_Time']) && date('Y', $char_arr[$char_search_count]['DGT_Time']) > 2000) $char_arr[$char_search_count]['DGT_Time_view'] = date('H:i d/m/Y', $char_arr[$char_search_count]['DGT_Time']);
        else $char_arr[$char_search_count]['DGT_Time_view'] = "None";
    $char_arr[$char_search_count]['SCFSealItem'] = $char_search_f[43];
        switch ($char_arr[$char_search_count]['SCFSealItem']){ 
        	case 6699:
                $char_arr[$char_search_count]['SCFSealItem_view'] = "Bùa tăng Exp";
        	break;
        
        	case 6700:
                $char_arr[$char_search_count]['SCFSealItem_view'] = "Bùa Thiên Sứ";
        	break;
        
        	case 6701:
                $char_arr[$char_search_count]['SCFSealItem_view'] = "Bùa không tăng Exp";
        	break;
        
        	case 6749:
                $char_arr[$char_search_count]['SCFSealItem_view'] = "Bùa tăng Exp Master";
        	break;
        
        	default :
                $char_arr[$char_search_count]['SCFSealItem_view'] = "None";
        }
    $char_arr[$char_search_count]['SCFSealTime'] = $char_search_f[44];
        if($char_arr[$char_search_count]['SCFSealTime'] > 0) $char_arr[$char_search_count]['SCFSealTime_view'] = date('d/m/Y H:i:s', $char_arr[$char_search_count]['SCFSealTime']);
        else $char_arr[$char_search_count]['SCFSealTime_view'] = '';
    $char_arr[$char_search_count]['SCFScrollItem'] = $char_search_f[45];
        switch ($char_arr[$char_search_count]['SCFScrollItem']){ 
        	case 7240:
                $char_arr[$char_search_count]['SCFScrollItem_view'] = "Bùa tốc độ tấn công";
        	break;
        
        	case 7242:
                $char_arr[$char_search_count]['SCFScrollItem_view'] = "Bùa tăng sát thương";
        	break;
        
        	case 7243:
                $char_arr[$char_search_count]['SCFScrollItem_view'] = "Bùa tăng ma thuật";
        	break;
        
        	case 7266:
                $char_arr[$char_search_count]['SCFScrollItem_view'] = "Bùa tăng tấn công";
        	break;
        
        	case 7245:
                $char_arr[$char_search_count]['SCFScrollItem_view'] = "Bùa tăng Mana";
        	break;
        
        	case 7244:
                $char_arr[$char_search_count]['SCFScrollItem_view'] = "Bùa tăng HP";
        	break;
        
        	default :
                $char_arr[$char_search_count]['SCFScrollItem_view'] = "None";
        }
    $char_arr[$char_search_count]['SCFScrollTime'] = $char_search_f[46];
        if($char_arr[$char_search_count]['SCFScrollTime'] > 0) $char_arr[$char_search_count]['SCFScrollTime_view'] = date('d/m/Y H:i:s', $char_arr[$char_search_count]['SCFScrollTime']);
        else $char_arr[$char_search_count]['SCFScrollTime_view'] = '';
    $char_arr[$char_search_count]['PointUyThac_Event'] = $char_search_f[47];
    $char_arr[$char_search_count]['nbbtuluyen_str_point'] = $char_search_f[48];
    $char_arr[$char_search_count]['nbbtuluyen_agi_point'] = $char_search_f[49];
    $char_arr[$char_search_count]['nbbtuluyen_vit_point'] = $char_search_f[50];
    $char_arr[$char_search_count]['nbbtuluyen_ene_point'] = $char_search_f[51];
    $char_arr[$char_search_count]['nbbtuluyen_point'] = $char_search_f[52];
    $char_arr[$char_search_count]['nbbsongtu_lv'] = $char_search_f[53];
    $char_arr[$char_search_count]['nbbsongtu_point'] = $char_search_f[54];
    $char_arr[$char_search_count]['nbbhoanhao_point'] = $char_search_f[55];
    $char_arr[$char_search_count]['nbbcuonghoa_point'] = $char_search_f[56];
    $char_arr[$char_search_count]['ErrorSubBlock'] = $char_search_f[57];
    $char_arr[$char_search_count]['nbb_pmaster'] = $char_search_f[58];
    
    $char_arr[$char_search_count]['nbbtuluyen_str'] = $char_search_f[59];
    $char_arr[$char_search_count]['nbbtuluyen_str_exp'] = $char_search_f[60];
    $char_arr[$char_search_count]['nbbtuluyen_str_cp'] = $char_search_f[61];
    $char_arr[$char_search_count]['nbbtuluyen_agi'] = $char_search_f[62];
    $char_arr[$char_search_count]['nbbtuluyen_agi_exp'] = $char_search_f[63];
    $char_arr[$char_search_count]['nbbtuluyen_agi_cp'] = $char_search_f[64];
    $char_arr[$char_search_count]['nbbtuluyen_vit'] = $char_search_f[65];
    $char_arr[$char_search_count]['nbbtuluyen_vit_exp'] = $char_search_f[66];
    $char_arr[$char_search_count]['nbbtuluyen_vit_cp'] = $char_search_f[67];
    $char_arr[$char_search_count]['nbbtuluyen_ene'] = $char_search_f[68];
    $char_arr[$char_search_count]['nbbtuluyen_ene_exp'] = $char_search_f[69];
    $char_arr[$char_search_count]['nbbtuluyen_ene_cp'] = $char_search_f[70];
    
    $online_time_view = "";
    if($char_arr[$char_search_count]['connect_stat'] == 1 && $char_arr[$char_search_count]['name'] == $char_arr[$char_search_count]['gameidc']) {
        $char_arr[$char_search_count]['online'] = "<font color='blue'><strong>ON</strong></font>";
        $online_time = $timestamp - strtotime($char_arr[$char_search_count]['connect_time']);
        
        $day_online = floor($online_time/(24*60*60));
        if($day_online > 0) {
            $online_time_view .= " $day_online ngày";
        }
        
        $online_time = $online_time - $day_online*24*60*60;
        $hour_online = floor($online_time/(60*60));
        $online_time_view .= " {$hour_online}h";
        
        $online_time = $online_time - $hour_online*60*60;
        $minus_online = floor($online_time/(60));
        $online_time_view .= " {$minus_online}p";
        
        $char_arr[$char_search_count]['online_time'] = $online_time_view;
    }
    $char_arr[$char_search_count]['online_view'] = $char_arr[$char_search_count]['online'] ." ". $char_arr[$char_search_count]['online_time'];
    
    switch ($char_arr[$char_search_count]['mapnum']) {
        case 0:
            $map_name = 'Lorencia';
            break;
        case 1:
            $map_name = 'Dungeon';
            break;
        case 2:
            $map_name = 'Davias';
            break;
        case 3:
            $map_name = 'Noria';
            break;
        case 4:
            $map_name = 'LostTower';
            break;
        case 5:
            $map_name = 'Exile';
            break;
        case 6:
            $map_name = 'Stadium';
            break;
        case 7:
            $map_name = 'Atlans';
            break;
        case 8:
            $map_name = 'Tarkan';
            break;
        case 10:
            $map_name = 'Icarus';
            break;
        case 11:
            $map_name = 'BloodCastle 1';
            break;
        case 12:
            $map_name = 'BloodCastle 2';
            break;
        case 13:
            $map_name = 'BloodCastle 3';
            break;
        case 14:
            $map_name = 'BloodCastle 4';
            break;
        case 15:
            $map_name = 'BloodCastle 5';
            break;
        case 16:
            $map_name = 'BloodCastle 6';
            break;
        case 17:
            $map_name = 'BloodCastle 7';
            break;
        case 52:
            $map_name = 'BloodCastle 8';
            break;
        case 9:
            $map_name = 'DevilSquare 1-2-3-4';
            break;
        case 32:
            $map_name = 'DevilSquare 1-2-3-4';
            break;
        case 35:
            $map_name = 'Lorencia';
            break;
        case 18:
            $map_name = 'ChaosCastle 1';
            break;
        case 19:
            $map_name = 'ChaosCastle 2';
            break;
        case 20:
            $map_name = 'ChaosCastle 3';
            break;
        case 21:
            $map_name = 'ChaosCastle 4';
            break;
        case 22:
            $map_name = 'ChaosCastle 5';
            break;
        case 23:
            $map_name = 'ChaosCastle 6';
            break;
        case 53:
            $map_name = 'ChaosCastle 7';
            break;
        case 24:
            $map_name = 'Kalima 1';
            break;
        case 25:
            $map_name = 'Kalima 2';
            break;
        case 26:
            $map_name = 'Kalima 3';
            break;
        case 27:
            $map_name = 'Kalima 4';
            break;
        case 28:
            $map_name = 'Kalima 5';
            break;
        case 29:
            $map_name = 'Kalima 6';
            break;
        case 36:
            $map_name = 'Kalima 7';
            break;
        case 30:
            $map_name = 'Valley Of Loren';
            break;
        case 31:
            $map_name = 'Land Of Trials';
            break;
        case 33:
            $map_name = 'Aida';
            break;
        case 34:
            $map_name = 'CryWolf';
            break;
        case 37:
            $map_name = 'Kantru 1';
            break;
        case 38:
            $map_name = 'Kantru 2';
            break;
        case 39:
            $map_name = 'Kantru Ref';
            break;
        case 40:
            $map_name = 'Silent Map';
            break;
        case 41:
            $map_name = 'Balgass Barrack';
            break;
        case 42:
            $map_name = 'Balgass Refuge';
            break;
        case 45:
            $map_name = 'Illusion Temple 1';
            break;
        case 46:
            $map_name = 'Illusion Temple 2';
            break;
        case 47:
            $map_name = 'Illusion Temple 3';
            break;
        case 48:
            $map_name = 'Illusion Temple 4';
            break;
        case 49:
            $map_name = 'Illusion Temple 5';
            break;
        case 50:
            $map_name = 'Illusion Temple 6';
            break;
        case 51:
            $map_name = 'Elbeland';
            break;
        case 56:
            $map_name = 'Swamp Of Calmness';
            break;
        case 57:
            $map_name = 'Raklion';
            break;
        case 58:
            $map_name = 'Raklion BOSS';
            break;
        case 62:
            $map_name = 'Santa Town';
            break;
        case 63:
            $map_name = 'Vulcanus';
            break;
        case 64:
            $map_name = 'Duel Arena';
            break;
        case 65:
            $map_name = 'Doppel Ganger-A';
            break;
        case 66:
            $map_name = 'Doppel Ganger-B';
            break;
        case 67:
            $map_name = 'Doppel Ganger-C';
            break;
        case 68:
            $map_name = 'Doppel Ganger-D';
            break;
        case 69:
            $map_name = 'Empire Guardian-A';
            break;
        case 70:
            $map_name = 'Empire Guardian-B';
            break;
        case 71:
            $map_name = 'Empire Guardian-C';
            break;
        case 72:
            $map_name = 'Empire Guardian-D';
            break;
        case 79:
            $map_name = 'Market Loren';
            break;
        default:
            $map_name = 'Unknow';
    }
    
    $char_arr[$char_search_count]['map_name'] = $map_name;
    
    switch ($char_arr[$char_search_count]['class']) {
        case 0:
            $char_arr[$char_search_count]['class_name'] = 'DW 1';
            break;
        case 1:
            $char_arr[$char_search_count]['class_name'] = 'DW 2';
            break;
        case 2:
        case 3:
            $char_arr[$char_search_count]['class_name'] = 'DW 3';
            break;

        case 16:
            $char_arr[$char_search_count]['class_name'] = 'DK 1';
            break;
        case 17:
            $char_arr[$char_search_count]['class_name'] = 'DK 2';
            break;
        case 18:
        case 19:
            $char_arr[$char_search_count]['class_name'] = 'DK 3';
            break;

        case 32:
            $char_arr[$char_search_count]['class_name'] = 'ELF 1';
            break;
        case 33:
            $char_arr[$char_search_count]['class_name'] = 'ELF 2';
            break;
        case 34:
        case 35:
            $char_arr[$char_search_count]['class_name'] = 'ELF 3';
            break;

        case 48:
            $char_arr[$char_search_count]['class_name'] = 'MG 1';
            break;
        case 49:
        case 50:
            $char_arr[$char_search_count]['class_name'] = 'MG 3';
            break;

        case 64:
            $char_arr[$char_search_count]['class_name'] = 'DL 1';
            break;
        case 65:
        case 66:
            $char_arr[$char_search_count]['class_name'] = 'DL 3';
            break;

        case 80:
            $char_arr[$char_search_count]['class_name'] = 'SUM 1';
            break;
        case 81:
            $char_arr[$char_search_count]['class_name'] = 'SUM 2';
            break;
        case 82:
        case 83:
            $char_arr[$char_search_count]['class_name'] = 'SUM 3';
            break;

        case 96:
            $char_arr[$char_search_count]['class_name'] = 'RF 1';
            break;
        case 97:
        case 98:
            $char_arr[$char_search_count]['class_name'] = 'RF 3';
            break;
    }
    
    switch ($char_arr[$char_search_count]['ctlcode']) {
        case 0:
            $char_arr[$char_search_count]['stat'] = 'Bình thường';
            break;
        case 32:
            $char_arr[$char_search_count]['stat'] = '<font color="brown">GameMaster</font>';
            break;
        case 18:
            $char_arr[$char_search_count]['stat'] = '<font color="yellow">Khóa đồ</font>';
            break;
        case 1:
            if($char_arr[$char_search_count]['uythacoffline_stat'] == 1) $char_arr[$char_search_count]['stat'] = '<font color="blue">Ủy Thác OFF</font>';
            else $char_arr[$char_search_count]['stat'] = '<font color="green">Block (1)</font>';
            break;
        case 99:
            if($char_arr[$char_search_count]['ErrorSubBlock'] == 99) {
                $char_arr[$char_search_count]['stat'] = '<font color="red">Admin Block (99)</font>';
            } else {
                $char_arr[$char_search_count]['stat'] = '<font color="yellow">Nhầm SV (99)</font>';
            }
            
            break;
    }
    
    $char_arr[$char_search_count]['resetday'] = _get_reset_day($char_arr[$char_search_count]['name'],$timestamp);
    $char_arr[$char_search_count]['resetmonth'] = _get_reset_month($char_arr[$char_search_count]['name']);
    
    $char_inacc_q = "SELECT Name, Resets, Relifes FROM Character WHERE AccountID='". $char_arr[$char_search_count]['acc'] ."' AND Name <> '". $char_arr[$char_search_count]['name'] ."'";
    $char_inacc_r = $db->Execute($char_inacc_q);
        check_queryerror($char_inacc_q, $char_inacc_r);
    $char_inacc_arr = array();
    while($char_inacc_f = $char_inacc_r->FetchRow()) {
        if($char_arr[$char_search_count]['connect_stat'] == 1 && $char_arr[$char_search_count]['gameidc'] == $char_inacc_f[0]) $online_char_inacc = 1;
        else $online_char_inacc = 0;
        $char_inacc_arr[] = array(
            'name'  =>  $char_inacc_f[0],
            'rs'    =>  $char_inacc_f[1],
            'rl'    =>  $char_inacc_f[2],
            'online'    =>  $online_char_inacc
        );
    }
    
    $char_arr[$char_search_count]['charlist'] = $char_inacc_arr;
    
    $char_search_count++;
}

    
?>
<table class="sort-table" id="table-1" height="30" border="0" cellpadding="0" cellspacing="0" width="960px">

<thead><tr>
    <td class="text_administrator">#</td>
    <td class="text_administrator">Character</td>
    <td class="text_administrator">Info</td>
</tr></thead>
<?php
    if($char_search_count > 0) {
        $count = 0;
        foreach($char_arr as $char) {
            $count++;
?>
<tr>
    <td align="left" class="text_administrator"><?php echo $count; ?></td>
    <td align="left" class="text_administrator" valign="top">
        Acc : <a href="#" class="char_search_acc" accname="<?php echo $char['acc']; ?>"><?php echo $char['acc']; ?></a> <span id="char_search_acc_<?php echo $char['acc']; ?>"></span>
        <hr />
        <div align="right"><a href="#" class="acc_search_char" charname="<?php echo $char['name']; ?>"><font color='red'><strong><?php echo $char['name']; ?></strong></font></a></div>
        <hr />
        <?php
            foreach($char['charlist'] as $char_inacc) {
        ?>
            <a href="#" class="acc_search_char" charname="<?php echo $char_inacc['name']; ?>"><?php echo $char_inacc['name']; ?></a> <br /><div align="right"><i>(<font color='red'><?php echo $char_inacc['rl']; ?></font>/<font color='blue'><?php echo $char_inacc['rs']; ?></font>)</i> 
            <?php 
                if($char_inacc['online'] == 1) echo "<font color='blue'><strong>ON</strong></font>";
                else echo "<font color='silver'><strong>OFF</strong></font>";
            ?>
            <span id="acc_search_char_<?php echo $char_inacc['name']; ?>"></span></div>
        <?php } ?>
    </td>
    <td align="left" class="text_administrator" valign="top">
    	<div style="width: 100%;">
            <div style="float:left; width: 20%;">
        		<li style="position: relative; padding-right:30px">Level : <span id="edit_level_<?php echo $char['name']; ?>"><?php echo $char['level']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['level']; ?>' datatype="level" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Reset : <span id="edit_rs_<?php echo $char['name']; ?>"><?php echo $char['rs']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['rs']; ?>' datatype="rs" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span> <br />RS lúc : <?php echo $char['rstime_view']; ?>
                </li><li style="position: relative; padding-right:30px">Relife : <span id="edit_rl_<?php echo $char['name']; ?>"><?php echo $char['rl']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['rl']; ?>' datatype="rl" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Class : <span id="edit_class_<?php echo $char['name']; ?>"><?php echo $char['class_name']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['class']; ?>' dataview='<?php echo $char['class_name']; ?>' datatype="class" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Stat : <span id="edit_stat_<?php echo $char['name']; ?>"><?php echo $char['stat']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['ctlcode']; ?>' dataview='<?php echo $char['stat']; ?>' datatype="stat" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Online : <?php echo $char['online_view']; ?>
                </li><li style="position: relative; padding-right:30px">Server : <?php echo $char['server_name']; ?>
                </li><li style="position: relative; padding-right:30px"><?php echo $char['map_name']; ?> : <?php echo $char['map_x']; ?>/<?php echo $char['map_y']; ?>
                </li><li style="position: relative; padding-right:30px">Top 0h : <?php echo $char['Top50']; ?>
                </li><li style="position: relative; padding-right:30px">Đổi giới tính : <?php echo $char['DGT_Time_view']; ?>
        	</li></div>
            
        	<div style="float:left; width: 20%;">
                <li style="position: relative; padding-right:30px">Point chưa cộng : <span id="edit_point_<?php echo $char['name']; ?>"><?php echo $char['point']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['point']; ?>' datatype="point" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Point dự trữ : <span id="edit_pointdutru_<?php echo $char['name']; ?>"><?php echo $char['pointdutru']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['pointdutru']; ?>' datatype="pointdutru" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Point Event : <span id="edit_pointevent_<?php echo $char['name']; ?>"><?php echo $char['pointevent']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['pointevent']; ?>' datatype="pointevent" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">TL Str LV: <span id="edit_nbbtuluyen_str_<?php echo $char['name']; ?>"><?php echo $char['nbbtuluyen_str']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['nbbtuluyen_str']; ?>' datatype="nbbtuluyen_str" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">TL Str Point: <span id="edit_nbbtuluyen_str_point_<?php echo $char['name']; ?>"><?php echo $char['nbbtuluyen_str_point']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['nbbtuluyen_str_point']; ?>' datatype="nbbtuluyen_str_point" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">TL AGI LV: <span id="edit_nbbtuluyen_agi_<?php echo $char['name']; ?>"><?php echo $char['nbbtuluyen_agi']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['nbbtuluyen_agi']; ?>' datatype="nbbtuluyen_agi" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">TL AGI Point: <span id="edit_nbbtuluyen_agi_point_<?php echo $char['name']; ?>"><?php echo $char['nbbtuluyen_agi_point']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['nbbtuluyen_agi_point']; ?>' datatype="nbbtuluyen_agi_point" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">TL VIT LV: <span id="edit_nbbtuluyen_vit_<?php echo $char['name']; ?>"><?php echo $char['nbbtuluyen_vit']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['nbbtuluyen_vit']; ?>' datatype="nbbtuluyen_vit" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">TL VIT Point : <span id="edit_nbbtuluyen_vit_point_<?php echo $char['name']; ?>"><?php echo $char['nbbtuluyen_vit_point']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['nbbtuluyen_vit_point']; ?>' datatype="nbbtuluyen_vit_point" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">TL ENE LV: <span id="edit_nbbtuluyen_ene_<?php echo $char['name']; ?>"><?php echo $char['nbbtuluyen_ene']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['nbbtuluyen_ene']; ?>' datatype="nbbtuluyen_ene" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">TL ENE Point : <span id="edit_nbbtuluyen_ene_point_<?php echo $char['name']; ?>"><?php echo $char['nbbtuluyen_ene_point']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['nbbtuluyen_ene_point']; ?>' datatype="nbbtuluyen_ene_point" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">TL Điểm : <span id="edit_nbbtuluyen_point_<?php echo $char['name']; ?>"><?php echo $char['nbbtuluyen_point']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['nbbtuluyen_point']; ?>' datatype="nbbtuluyen_point" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                
        	</li></div>
            
        	<div style="float:left; width: 20%;">
        		<li style="position: relative; padding-right:30px">Master LV : <span id="edit_scfmaster_level_<?php echo $char['name']; ?>"><?php echo $char['scfmaster_level']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['scfmaster_level']; ?>' datatype="scfmaster_level" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Master Point : <span id="edit_scfmaster_point_<?php echo $char['name']; ?>"><?php echo $char['scfmaster_point']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['scfmaster_point']; ?>' datatype="scfmaster_point" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Master Point Ex : <span id="edit_nbb_pmaster_<?php echo $char['name']; ?>"><?php echo $char['nbb_pmaster']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['nbb_pmaster']; ?>' datatype="nbb_pmaster" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Point Ủy Thác : <span id="edit_pointuythac_<?php echo $char['name']; ?>"><?php echo $char['pointuythac']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['pointuythac']; ?>' datatype="pointuythac" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Point Ủy Thác Event : <span id="edit_PointUyThac_Event_<?php echo $char['name']; ?>"><?php echo $char['PointUyThac_Event']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['PointUyThac_Event']; ?>' datatype="PointUyThac_Event" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Sức mạnh : <span id="edit_str_<?php echo $char['name']; ?>"><?php echo $char['str']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['str']; ?>' datatype="str" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Nhanh nhẹn : <span id="edit_dex_<?php echo $char['name']; ?>"><?php echo $char['dex']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['dex']; ?>' datatype="dex" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Thể Lực : <span id="edit_vit_<?php echo $char['name']; ?>"><?php echo $char['vit']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['vit']; ?>' datatype="vit" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Năng Lượng : <span id="edit_ene_<?php echo $char['name']; ?>"><?php echo $char['ene']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['ene']; ?>' datatype="ene" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Mệnh Lệnh : <span id="edit_leadership_<?php echo $char['name']; ?>"><?php echo $char['leadership']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['leadership']; ?>' datatype="leadership" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
            </li></div>
            
        	<div style="float:left; width: 20%;">
                <li style="position: relative; padding-right:30px">Zen : <span id="edit_money_<?php echo $char['name']; ?>"><?php echo $char['money']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['money']; ?>' datatype="money" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">RS/ngày : <?php echo $char['resetday']; ?>
                </li><li style="position: relative; padding-right:30px">RS/tháng : <?php echo $char['resetmonth']; ?>
                </li><li style="position: relative; padding-right:30px">Phạm tội : <span id="edit_pklevel_<?php echo $char['name']; ?>"><?php echo $char['pklevel']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['pklevel']; ?>' datatype="pklevel" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Giết người : <span id="edit_pkcount_<?php echo $char['name']; ?>"><?php echo $char['pkcount']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['pkcount']; ?>' datatype="pkcount" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Khóa đồ : <span id="edit_khoado_<?php echo $char['name']; ?>"><?php echo $char['khoado_view']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['khoado']; ?>' khoado_view="<?php echo $char['khoado_view']; ?>" datatype="khoado" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Mã Khóa đồ : <span id="edit_makhoado_<?php echo $char['name']; ?>"><?php echo $char['makhoado']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['makhoado']; ?>' datatype="makhoado" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">PCPoint : <span id="edit_SCFPCPoints_<?php echo $char['name']; ?>"><?php echo $char['SCFPCPoints']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['SCFPCPoints']; ?>' datatype="SCFPCPoints" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
        	</li></div>
            
            <div style="float:left; width: 20%;">
                <li style="position: relative; padding-right:30px">Bùa Exp : <span id="edit_SCFSealItem_<?php echo $char['name']; ?>"><?php echo $char['SCFSealItem_view']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['SCFSealItem']; ?>' dataview='<?php echo $char['SCFSealItem_view']; ?>' datatype="SCFSealItem" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Bùa Exp - thời hạn : <span id="edit_SCFSealTime_<?php echo $char['name']; ?>"><?php echo $char['SCFSealTime_view']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['SCFSealTime']; ?>' dataview='<?php echo $char['SCFSealTime_view']; ?>' datatype="SCFSealTime" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Bùa chỉ số : <span id="edit_SCFScrollItem_<?php echo $char['name']; ?>"><?php echo $char['SCFScrollItem_view']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['SCFScrollItem']; ?>' dataview='<?php echo $char['SCFScrollItem_view']; ?>' datatype="SCFScrollItem" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Bùa chỉ số - thời hạn : <span id="edit_SCFScrollTime_<?php echo $char['name']; ?>"><?php echo $char['SCFScrollTime_view']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['SCFScrollTime']; ?>' dataview='<?php echo $char['SCFScrollTime_view']; ?>' datatype="SCFScrollTime" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Song Tu LV : <span id="edit_nbbsongtu_lv_<?php echo $char['name']; ?>"><?php echo $char['nbbsongtu_lv']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['nbbsongtu_lv']; ?>' datatype="nbbsongtu_lv" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Song Tu Point : <span id="edit_nbbsongtu_point_<?php echo $char['name']; ?>"><?php echo $char['nbbsongtu_point']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['nbbsongtu_point']; ?>' datatype="nbbsongtu_point" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Hoàn Hảo Point : <span id="edit_nbbhoanhao_point_<?php echo $char['name']; ?>"><?php echo $char['nbbhoanhao_point']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['nbbhoanhao_point']; ?>' datatype="nbbhoanhao_point" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
                </li><li style="position: relative; padding-right:30px">Cường Hóa Point : <span id="edit_nbbcuonghoa_point_<?php echo $char['name']; ?>"><?php echo $char['nbbcuonghoa_point']; ?> <a href='#' charname='<?php echo $char['name']; ?>' dataedit='<?php echo $char['nbbcuonghoa_point']; ?>' datatype="nbbcuonghoa_point" class='char_data_edit' style="position: absolute; top: 0px; right: 0px;"><img src='images/icon_edit.png' border='0' style="padding-right: 10px;" /></a></span>
        	</li></div>
         </div>
    </td>
</tr>

<tr>
    <td colspan="4"><hr /></td>
</tr>
<?php
        }
    }
?>

</table>