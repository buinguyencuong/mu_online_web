<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
	include("security.php");
include_once('../config.php');
include('function.php');
$title = "Tel Gamer Active";
$day_active = 3;
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo $title; ?></title>
<link href="css/tooltip.css" rel="stylesheet" type="text/css" />
</head>
<body bgcolor="#F9E7CF">
<div id="dhtmltooltip"></div>
<img id="dhtmlpointer" src="images/tooltiparrow.gif">
<?php require('linktop.php'); ?>


<?php
    $time_active = $timestamp - $day_active * 24*60*60;
    $datetime_active = date('Y-m-d H:i:s', $time_active);
	$tel_active_q = "SELECT tel__numb FROM MEMB_INFO A JOIN MEMB_STAT B ON A.memb___id = B.memb___id AND ConnectTM > '{$datetime_active}'";
    $tel_active_r = $db->Execute($tel_active_q);
        check_queryerror($tel_active_q, $tel_active_r);
    $tel_arr = array();
    $tel_count = 0;
    while($tel_active_f = $tel_active_r->FetchRow()) {
        $tel_arr[] = $tel_active_f[0];
        $tel_count++;
    }
    $tel_data = json_encode($tel_arr);
    echo "Tel Count : " . $tel_count ."<br /><br />";
    echo $tel_data;
$db->Close();
?>

</body>
</html>