<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/

/**
 * @author NetBanBe
 * @copyright 2011
 */
 include_once("security.php");
include('../config.php');

// Connect DB Log

	$title = "Kiểm tra Log";
session_start();

function antiinject_query($value) {
    $value = stripslashes($value);
    $value = htmlspecialchars($value);
    return $value;
}

function check_queryerror($query,$result) {
    if ($result === false) die("Query Error : $query");
}

function _dellog($daybefore) {
    global $db, $timestamp;
    
    $file_time_del_log = "log_time_dellog.txt";
    $time_del_log = filemtime($file_time_del_log);
    
    if($timestamp - $time_del_log > 1*24*60*60) {
        $timedel = $timestamp - ($daybefore * 24*60*60);
        $dellog_query = "DELETE FROM Log_TienTe WHERE Des not like '%Bảo vệ Item%' AND Des not like '%Nạp thẻ%'";
        $dellog_query .= " AND time<$timedel";
        $dellog_result = $db->Execute($dellog_query);
            check_queryerror($dellog_query, $dellog_result);
            
        //Ghi vào file
		$fp = fopen($file_time_del_log, "w");  
		fputs ($fp, $timestamp);
		fclose($fp);
		//End Ghi vào File
    }
}

// DELETE LOG > 1 month
_dellog(30);
// End DELETE LOG > 1 month

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title><?php echo $title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
</head>
<body>
<?php require('linktop.php'); ?>
<?php
if($_POST['dellog']) {
    $monthdel = abs(intval($_POST['dellog']));
    $lognotice = "";
    if($monthdel > 0) {
        _dellog($monthdel);
        $lognotice = "Chỉ để lại Log trong vòng $monthdel ngày gần đây.";
    }
    echo "<center><b><font color=red>Xóa Log thành công.<br /> $lognotice</font></b></center>";
}
?>
<div align='right'>
<form action="" method="POST">
Xóa Log
<select name="dellog">
    <option value="all"> - Xóa tất cả Log - </option>
    
    <option value="1"> - Chỉ để lại Log trong vòng 1 ngày gần đây - </option>
    
    <option value="7"> - Chỉ để lại Log trong vòng 1 tuần gần đây - </option>
    <option value="14"> - Chỉ để lại Log trong vòng 2 tuần gần đây - </option>
    
    <option value="30"> - Chỉ để lại Log trong vòng 1 tháng gần đây - </option>
    <option value="60"> - Chỉ để lại Log trong vòng 2 tháng gần đây - </option>
    <option value="90"> - Chỉ để lại Log trong vòng 3 tháng gần đây - </option>
</select>
<input type="submit" value="Xóa Log" />
</form>
</div>
<center>
<form name="checklog" method="GET" action="">
    Tài khoản cần kiểm tra : <input name="character" value="<?php echo $_GET['character']; ?>" /> 
    <select name="type_log">
        <option value="all">- ALL -</option>
        <option value="27" <?php if($_GET['type_log'] == 27) echo "selected='selected'"; ?> >Nạp Thẻ</option>
        <option value="31" <?php if($_GET['type_log'] == 31) echo "selected='selected'"; ?> >Lô</option>
        <option value="32" <?php if($_GET['type_log'] == 32) echo "selected='selected'"; ?> >Đề</option>
        <option value="1" <?php if($_GET['type_log'] == 1) echo "selected='selected'"; ?> >WebShop</option>
        <option value="13" <?php if($_GET['type_log'] == 13) echo "selected='selected'"; ?> >Đổi Vcent sang Gcoin</option>
        <option value="14" <?php if($_GET['type_log'] == 14) echo "selected='selected'"; ?> >Đổi Gcoin sang Vcent</option>
        <option value="18" <?php if($_GET['type_log'] == 18) echo "selected='selected'"; ?> >Đổi Gcoin sang Wcoin</option>
        <option value="19" <?php if($_GET['type_log'] == 19) echo "selected='selected'"; ?> >Đổi Gcoin sang WCoinP</option>
        <option value="20" <?php if($_GET['type_log'] == 20) echo "selected='selected'"; ?> >Đổi Gcoin sang Goblin Coin</option>
        <option value="21" <?php if($_GET['type_log'] == 21) echo "selected='selected'"; ?> >Đổi Gcoin sang VipMoney</option>
        <option value="15" <?php if($_GET['type_log'] == 15) echo "selected='selected'"; ?> >Mua Item Vcent</option>
        <option value="16" <?php if($_GET['type_log'] == 16) echo "selected='selected'"; ?> >Đổi Item Vcent ra Vcent</option>
        <option value="34" <?php if($_GET['type_log'] == 34) echo "selected='selected'"; ?> >Reset</option>
        <option value="2" <?php if($_GET['type_log'] == 2) echo "selected='selected'"; ?> >Reset VIP</option>
        <option value="22" <?php if($_GET['type_log'] == 22) echo "selected='selected'"; ?> >Reset Ủy Thác</option>
        <option value="3" <?php if($_GET['type_log'] == 3) echo "selected='selected'"; ?> >Reset Ủy Thác VIP</option>
        <option value="28" <?php if($_GET['type_log'] == 28) echo "selected='selected'"; ?> >ReLife</option>
        <option value="35" <?php if($_GET['type_log'] == 35) echo "selected='selected'"; ?> >TOP Bá Đạo - Bá Giả nhận thưởng</option>
        <option value="23" <?php if($_GET['type_log'] == 23) echo "selected='selected'"; ?> >Ủy Thác</option>
        <option value="4" <?php if($_GET['type_log'] == 4) echo "selected='selected'"; ?> >Đổi Giới Tính</option>
        <option value="30" <?php if($_GET['type_log'] == 30) echo "selected='selected'"; ?> >Tẩy Tủy</option>
        <option value="24" <?php if($_GET['type_log'] == 24) echo "selected='selected'"; ?> >Đổi Tên Nhân Vật</option>
        <option value="25" <?php if($_GET['type_log'] == 25) echo "selected='selected'"; ?> >Chuyển nhân vật sang tài khoản khác</option>
        <option value="33" <?php if($_GET['type_log'] == 33) echo "selected='selected'"; ?> >Đổi GiftCode</option>
        <option value="" disabled="disabled">--------------------</option>
        <option value="29" <?php if($_GET['type_log'] == 29) echo "selected='selected'"; ?> >Bảo vệ Item</option>
        <option value="5" <?php if($_GET['type_log'] == 5) echo "selected='selected'"; ?> >Máy Quay Chao</option>
        <option value="6" <?php if($_GET['type_log'] == 6) echo "selected='selected'"; ?> >Máy Quay Chao lên 10</option>
        <option value="7" <?php if($_GET['type_log'] == 7) echo "selected='selected'"; ?> >Máy Quay Chao lên 11</option>
        <option value="8" <?php if($_GET['type_log'] == 8) echo "selected='selected'"; ?> >Máy Quay Chao lên 12</option>
        <option value="9" <?php if($_GET['type_log'] == 9) echo "selected='selected'"; ?> >Máy Quay Chao lên 13</option>
        <option value="10" <?php if($_GET['type_log'] == 10) echo "selected='selected'"; ?> >Máy Quay Chao lên 14</option>
        <option value="11" <?php if($_GET['type_log'] == 11) echo "selected='selected'"; ?> >Máy Quay Chao lên 15</option>
        <option value="" disabled="disabled">--------------------</option>
        <option value="12" <?php if($_GET['type_log'] == 12) echo "selected='selected'"; ?> >Hack Reset</option>
        <option value="17" <?php if($_GET['type_log'] == 17) echo "selected='selected'"; ?> >Dupe</option>
        <option value="26" <?php if($_GET['type_log'] == 26) echo "selected='selected'"; ?> >Dupe Vcent</option>
    </select>
    <input type="submit" value="Kiểm tra Log" />
</form>
</center>

<?php
$name = $_GET['character'];    $name = antiinject_query(trim($name));
$type_log = $_GET['type_log'];    $type_log = antiinject_query(trim($type_log));
if($type_log)
{
$page = $_GET['page'];  $page = abs(intval($page));
if($page == 0) $page = 1;
$row_per_page = 15;
$row_start = ($page-1)*$row_per_page;
$query = "SELECT gcoin, gcoin_km, vpoint, price, CAST(Des as text), time, acc, vpoint_km, log_type FROM Log_TienTe";
$query_count = "SELECT count(*) FROM Log_TienTe";

/**
 * Type		type_log		     log_type :
 * Reset		-			      1
 * Reset VIP	2			      2
 * WebShop		1			      3
 * WebShop Zen	-		          4
 * Nap the		27			      5
 * RS Bu		-			      6
 * RS Ho		-			      7
 * Event TOP Auto	-			  8
 * Warehouse Secure	-			  9
*/

switch($type_log) {
    case '1':
        $query_extra = " WHERE (Des like '%Mua%' OR log_type=3)";
        break;
    case 2:
        $query_extra = " WHERE (Des like '%Reset VIP%' OR log_type=2)";
        break;
    case 3:
        $query_extra = " WHERE Des like '%Reset UT VIP%'";
        break;
    case 4:
        $query_extra = " WHERE Des like '%Đổi Giới tính%'";
        break;
    case 5:
        $query_extra = " WHERE Des like '%UP Item%'";
        break;
    case 6:
        $query_extra = " WHERE Des like '%UP Item%10%'";
        break;
    case 7:
        $query_extra = " WHERE Des like '%UP Item%11%'";
        break;
    case 8:
        $query_extra = " WHERE Des like '%UP Item%12%'";
        break;
    case 9:
        $query_extra = " WHERE Des like '%UP Item%13%'";
        break;
    case 10:
        $query_extra = " WHERE Des like '%UP Item%14%'";
        break;
    case 11:
        $query_extra = " WHERE Des like '%UP Item%15%'";
        break;
    case 12:
        $query_extra = " WHERE Des like '%quá nhanh%'";
        break;
    case 13:
        $query_extra = " WHERE Des like '%Vcent sang%Gcoin%'";
        break;
    case 14:
        $query_extra = " WHERE Des like '%Gcoin sang%Vcent%'";
        break;
    case 15:
        $query_extra = " WHERE Des like '%Mua%Item%'";
        break;
    case 16:
        $query_extra = " WHERE Des like '%Đổi%Gold%Zen%' OR Des like '%Đổi%Nhận%VCent%'";
        break;
    case 17:
        $query_extra = " WHERE Des like '%Dup%'";
        break;
    case 18:
        $query_extra = " WHERE Des like '%Gcoin%WCoin%'";
        break;
    case 19:
        $query_extra = " WHERE Des like '%Gcoin%WCoinP%'";
        break;
    case 20:
        $query_extra = " WHERE Des like '%Gcoin%GoblinCoin%'";
        break;
    case 21:
        $query_extra = " WHERE Des like '%Gcoin%VipMoney%'";
        break;
    case 22:
        $query_extra = " WHERE Des like '%Reset UT từ%'";
        break;
    case 23:
        $query_extra = " WHERE Des like '%Ủy Thác%'";
        break;
    case 24:
        $query_extra = " WHERE Des like '%Đổi tên sang%'";
        break;
    case 25:
        $query_extra = " WHERE Des like '%Chuyển sang Tài khoản%'";
        break;
    case 26:
        $query_extra = " WHERE Des like '%Vcent%Dupe%'";
        break;
    case 27:
        $query_extra = " WHERE (Des like '%Nạp thẻ%' OR log_type=5)";
        break;
    case 28:
        $query_extra = " WHERE Des like '%Relife lần thứ%'";
        break;
    case 29:
        $query_extra = " WHERE Des like '%Bảo vệ Item%'";
        break;
    case 30:
        $query_extra = " WHERE Des like '%Tẩy tủy%'";
        break;
    case 31:
        $query_extra = " WHERE Des like '%ánh%ô%'";
        break;
    case 32:
        $query_extra = " WHERE Des like '%ánh%đề%'";
        break;
    case 33:
        $query_extra = " WHERE Des like '%GiftCode%'";
        break;
    case 34:
        $query_extra = " WHERE log_type = 1";
        break;
    case 35:
        $query_extra = " WHERE Des like '%TOP%Bá%' OR log_type = 8";
        break;
    default: $query_extra = "";
}

$query .= $query_extra;
$query_count .= $query_extra;

if($name) {
    if($type_log != 'all') $query_name = " AND acc='$name'";
    else $query_name = " WHERE acc='$name'";
    
    $query .= $query_name;
    $query_count .= $query_name;
}

$query .= " ORDER BY time DESC";
$total_row_result = $db->execute($query_count);
    check_queryerror($query_count, $total_row_result);
$total_row_fetch = $total_row_result->FetchRow();
$total_row = $total_row_fetch[0];

if($total_row == 0) echo "<center>Không tồn tại tài LOG</center>";
else
{
$total_page = ceil($total_row/$row_per_page);
$result = $db->SelectLimit($query, $row_per_page, $row_start);


    if ($total_page > 1) {
		echo "<center>Trang: [".$total_page."] ";
        $page_check = 1;
    	while($page_check <= $total_page && $page_check<=50){
    		if($page_check == $page){
    			echo " [<font color='red' size=5>{$page}</font>] ";
    		} else {
    			echo "<a href=\"?character=$name&type_log=$type_log&page=$page_check\">[$page_check]</a> ";
    		} 
    		$page_check++; 
    	} 

		echo "</center>";
	}
?>
<table align="center" border="1" style="border-collapse: collapse;" cellpadding="3" cellspacing="3" >
<tr>
	<th width="150">Thời gian</th>
	<th>Gcent</th>
	<th>Gcent KM</th>
	<th>Vcent</th>
    <th>Vcent Event</th>
	<th>+/-</th>
	<th>Mô tả</th>
</tr>
<?php

while($row = $result->fetchrow())
{
    $time = date('d/m H:i:s',$row[5]);
    
    $log_type = $row[8];
    $log_des = '';
    switch ($log_type){ 
    	case 1:    // Reset
            $log_arr = json_decode($row[4], true);
            $log_des .= "<b>". $log_arr['name'] ."</b> Reset lần thứ <b>". $log_arr['rsup'] ."</b> khi ". $log_arr['level'] ." Level/". $log_arr['rsold'] ." Reset, Relife ". $log_arr['relife'] .".";
            if(isset($log_arr['resetpoint'])) $log_des .= "<br />Point theo Reset : <strong>". number_format($log_arr['resetpoint'], 0, ',', '.') ." Point</strong>.";
            
            if($log_arr['vip_rs_point_incre'] > 0 || $log_arr['vip_point_percent_add'] > 0) {
                
                $log_des .= "<br />Hệ thống Hỗ Trợ ";
                if($log_arr['vip_day'] > 0) $log_des .= "<strong>". $log_arr['vip_day'] ."d</strong>";
                $log_des .= " : ";
                
                if($log_arr['vip_rs_point_incre'] > 0) {
                    $log_des .= "<strong>". number_format($log_arr['vip_rs_point_incre'], 0, ',', '.') ." Point</strong> ";
                }
                if($log_arr['vip_point_percent_add'] > 0) {
                    $log_des .= " + ";
                    $log_des .= "<strong>". number_format($log_arr['vip_point_percent_add'], 0, ',', '.') ." Point</strong>";
                    if($log_arr['vip_rs_point_percent_incre_val'] > 0) $log_des .= " (". $log_arr['vip_rs_point_percent_incre_val'] ."%)"; 
                }
                
            }
            
            if(isset($log_arr['point_event'])) $log_des .= "<br />Point Event Huy chương : <strong>". $log_arr['point_event'] ." Point</strong>";
            if(isset($log_arr['point_rsday_percent']) && $log_arr['point_rsday_percent'] > 0) $log_des .= "<br />Ngày hôm qua Reset ". $log_arr['rsday_yesterday'] ." lần, thưởng ". $log_arr['point_rsday_percent'] ." % : + <strong>". $log_arr['point_rsday'] ." Point</strong> (". $log_arr['cfg_point_rsday_rs'] ." RS / ". $log_arr['cfg_point_rsday_percent'] ." % Point)";
            if(isset($log_arr['songtu_not_marry']) && $log_arr['songtu_not_marry'] == 1) $log_des .= "<br />Bạn chưa Kết hôn, không nhận được thêm điểm thưởng do kết hôn.";
            if(isset($log_arr['point_songtu_percent']) && $log_arr['point_songtu_percent'] > 0) $log_des .= "<br />Cấp độ Song Tu : ". $log_arr['songtulv'] ." cấp. Nhận thêm ". $log_arr['point_songtu_percent'] ." % = + <strong>". $log_arr['point_songtu'] ." Point</strong>.";
            if(isset($log_arr['point_total'])) $log_des .= "<br /><strong>Tổng Point nhận được : <strong>". $log_arr['point_total'] ." Point</strong></strong>";
            if(isset($log_arr['guild_blance']) && $log_arr['guild_blance'] == 1) {
                $log_des .= "<hr /><strong>Chính sách cân bằng thế lực :</strong>";
                if(isset($log_arr['is_not_guild']) && $log_arr['is_not_guild'] == 1) $log_des .= "<br />Bạn chưa vào Guild. Không nhận được hỗ trợ khi Reset.";
                if(isset($log_arr['guild_maxmem_redurepoint']) && $log_arr['guild_maxmem_redurepoint'] > 0) $log_des .= "<br />Guild bạn có <strong>". $log_arr['G_SlgMem'] ." thành viên</strong>. Nhiều hơn mức cho phép ". $log_arr['guild_maxmem'] ." thành viên. <strong>Bị trừ ". $log_arr['guild_maxmem_redurepoint'] ." %</strong> = - <strong>". $log_arr['point_overmem_redure'] ." Point</strong>.";
                if(isset($log_arr['guild_top12lm_redurepoint']) && $log_arr['guild_top12lm_redurepoint'] > 0) $log_des .= "<br />Guild bạn <strong>TOP Point ". $log_arr['G_TopPoint'] ."</strong> Liên Minh với Guild <strong>TOP Point ". $log_arr['toplm'] ."</strong> . Bị trừ ". $log_arr['guild_top12lm_redurepoint'] ." % = <strong>- ". $log_arr['point_union12_redure'] ." Point</strong>.";
                if(isset($log_arr['guild_addpoint_require_mem'])) $log_des .= "<br />Guild bạn chỉ có <strong>". $log_arr['G_SlgMem'] ." thành viên</strong>. Không đủ điều kiện ". $log_arr['guild_addpoint_require_mem'] ." thành viên để nhận Point hỗ trợ.";
                if(isset($log_arr['G_Created_day'])) $log_des .= "<br />Guild bạn mới <strong>thành lập ". $log_arr['G_Created_day'] ." ngày</strong>. Không đủ điều kiện Guild thành lập trên ". $log_arr['guild_addpoint_require_day'] ." ngày để nhận Point hỗ trợ.";
                if(isset($log_arr['G_RSYesterday'])) $log_des .= "<br />Guild bạn <strong>hôm qua thực hiện ". $log_arr['G_RSYesterday'] ." lần Reset</strong>. Không đủ điều kiện tổng số lần thực hiện Reset trong ngày hôm qua là ". $log_arr['guild_addpoint_require_rs'] ." để nhận Point hỗ trợ.";
                if(isset($log_arr['guild_topaddpoint'])) $log_des .= "<br />Guild bạn <strong>TOP ". $log_arr['G_TopPoint'] ." Guild Point</strong>. Nhận thêm hỗ trợ ". $log_arr['guild_topaddpoint'] ." % = <strong>+ ". $log_arr['point_gtop_extra'] ." Point</strong>.";
                if(isset($log_arr['guild_top10overaddpoint'])) $log_des .= "<br />Guild bạn <strong>không nằm trong TOP 10 Guild Point</strong>. Nhận thêm hỗ trợ ". $log_arr['guild_top10overaddpoint'] ." % = <strong>+ ". $log_arr['point_gtop_extra'] ." Point</strong>.";
            }
            if(isset($log_arr['point_total_2'])) $log_des .= "<br /><strong>Tổng Point cuối cùng nhận được : ". number_format($log_arr['point_total_2'], 0, ',', '.') ." Point</strong>";
            if(isset($log_arr['danhvong_basic'])) $log_des .= "<hr />Nhận được : <strong>". $log_arr['danhvong_basic'] ." điểm Danh Vọng</strong>.";
            if(isset($log_arr['danhvong_extra'])) $log_des .= "<br /> Reset trên 300LV nhận thêm : <strong>". $log_arr['danhvong_extra'] ." điểm Danh Vọng</strong> (tăng ". $log_arr['danhvong_extra_percent'] ." %).<br />Tổng Điểm Danh Vọng nhận được : <strong>". $log_arr['danhvong_point'] ."</strong> Điểm Danh Vọng.";
            if(isset($log_arr['danhvong_low300'])) $log_des .= "<br />Reset dưới 300LV, không được nhận thêm Điểm Danh Vọng.";
            
    	break;
        
        case 2:     // Reset VIP
            $log_arr = json_decode($row[4], true);
            $log_des .= "<b>". $log_arr['name'] ."</b> Reset VIP lần thứ <b>". $log_arr['rsup'] ."</b> khi ". $log_arr['level'] ." Level/". $log_arr['rsold'] ." Reset, Relife ". $log_arr['relife'] .".";
            if(isset($log_arr['resetpoint'])) $log_des .= "<br />Point theo Reset : <strong>". number_format($log_arr['resetpoint'], 0, ',', '.') ." Point</strong>.";
            
            if($log_arr['vip_rs_point_incre'] > 0 || $log_arr['vip_point_percent_add'] > 0) {
                
                $log_des .= "<br />Hệ thống Hỗ Trợ ";
                if($log_arr['vip_day'] > 0) $log_des .= "<strong>". $log_arr['vip_day'] ."d</strong>";
                $log_des .= " : ";
                
                if($log_arr['vip_rs_point_incre'] > 0) {
                    $log_des .= "<strong>". number_format($log_arr['vip_rs_point_incre'], 0, ',', '.') ." Point</strong> ";
                }
                if($log_arr['vip_point_percent_add'] > 0) {
                    $log_des .= " + ";
                    $log_des .= "<strong>". number_format($log_arr['vip_point_percent_add'], 0, ',', '.') ." Point</strong>";
                    if($log_arr['vip_rs_point_percent_incre_val'] > 0) $log_des .= " (". $log_arr['vip_rs_point_percent_incre_val'] ."%)"; 
                }
                
            }
            
            if(isset($log_arr['point_event'])) $log_des .= "<br />Point Event Huy chương : <strong>". $log_arr['point_event'] ." Point</strong>";
            if(isset($log_arr['point_rsday_percent']) && $log_arr['point_rsday_percent'] > 0) $log_des .= "<br />Ngày hôm qua Reset ". $log_arr['rsday_yesterday'] ." lần, thưởng ". $log_arr['point_rsday_percent'] ." % : + <strong>". $log_arr['point_rsday'] ." Point</strong> (". $log_arr['cfg_point_rsday_rs'] ." RS / ". $log_arr['cfg_point_rsday_percent'] ." % Point)";
            if(isset($log_arr['songtu_not_marry']) && $log_arr['songtu_not_marry'] == 1) $log_des .= "<br />Bạn chưa Kết hôn, không nhận được thêm điểm thưởng do kết hôn.";
            if(isset($log_arr['point_songtu_percent']) && $log_arr['point_songtu_percent'] > 0) $log_des .= "<br />Cấp độ Song Tu : ". $log_arr['songtulv'] ." cấp. Nhận thêm ". $log_arr['point_songtu_percent'] ." % = + <strong>". $log_arr['point_songtu'] ." Point</strong>.";
            if(isset($log_arr['point_total'])) $log_des .= "<br /><strong>Tổng Point nhận được : <strong>". $log_arr['point_total'] ." Point</strong></strong>";
            if(isset($log_arr['guild_blance']) && $log_arr['guild_blance'] == 1) {
                $log_des .= "<hr /><strong>Chính sách cân bằng thế lực :</strong>";
                if(isset($log_arr['is_not_guild']) && $log_arr['is_not_guild'] == 1) $log_des .= "<br />Bạn chưa vào Guild. Không nhận được hỗ trợ khi Reset.";
                if(isset($log_arr['guild_maxmem_redurepoint']) && $log_arr['guild_maxmem_redurepoint'] > 0) $log_des .= "<br />Guild bạn có <strong>". $log_arr['G_SlgMem'] ." thành viên</strong>. Nhiều hơn mức cho phép ". $log_arr['guild_maxmem'] ." thành viên. <strong>Bị trừ ". $log_arr['guild_maxmem_redurepoint'] ." %</strong> = - <strong>". $log_arr['point_overmem_redure'] ." Point</strong>.";
                if(isset($log_arr['guild_top12lm_redurepoint']) && $log_arr['guild_top12lm_redurepoint'] > 0) $log_des .= "<br />Guild bạn <strong>TOP Point ". $log_arr['G_TopPoint'] ."</strong> Liên Minh với Guild <strong>TOP Point ". $log_arr['toplm'] ."</strong> . Bị trừ ". $log_arr['guild_top12lm_redurepoint'] ." % = <strong>- ". $log_arr['point_union12_redure'] ." Point</strong>.";
                if(isset($log_arr['guild_addpoint_require_mem'])) $log_des .= "<br />Guild bạn chỉ có <strong>". $log_arr['G_SlgMem'] ." thành viên</strong>. Không đủ điều kiện ". $log_arr['guild_addpoint_require_mem'] ." thành viên để nhận Point hỗ trợ.";
                if(isset($log_arr['G_Created_day'])) $log_des .= "<br />Guild bạn mới <strong>thành lập ". $log_arr['G_Created_day'] ." ngày</strong>. Không đủ điều kiện Guild thành lập trên ". $log_arr['guild_addpoint_require_day'] ." ngày để nhận Point hỗ trợ.";
                if(isset($log_arr['G_RSYesterday'])) $log_des .= "<br />Guild bạn <strong>hôm qua thực hiện ". $log_arr['G_RSYesterday'] ." lần Reset</strong>. Không đủ điều kiện tổng số lần thực hiện Reset trong ngày hôm qua là ". $log_arr['guild_addpoint_require_rs'] ." để nhận Point hỗ trợ.";
                if(isset($log_arr['guild_topaddpoint'])) $log_des .= "<br />Guild bạn <strong>TOP ". $log_arr['G_TopPoint'] ." Guild Point</strong>. Nhận thêm hỗ trợ ". $log_arr['guild_topaddpoint'] ." % = <strong>+ ". $log_arr['point_gtop_extra'] ." Point</strong>.";
                if(isset($log_arr['guild_top10overaddpoint'])) $log_des .= "<br />Guild bạn <strong>không nằm trong TOP 10 Guild Point</strong>. Nhận thêm hỗ trợ ". $log_arr['guild_top10overaddpoint'] ." % = <strong>+ ". $log_arr['point_gtop_extra'] ." Point</strong>.";
            }
            if(isset($log_arr['point_total_2'])) $log_des .= "<br /><strong>Tổng Point cuối cùng nhận được : ". number_format($log_arr['point_total_2'], 0, ',', '.') ." Point</strong>";
            if(isset($log_arr['danhvong_basic'])) $log_des .= "<hr />Nhận được : <strong>". $log_arr['danhvong_basic'] ." điểm Danh Vọng</strong>.";
            if(isset($log_arr['danhvong_extra'])) $log_des .= "<br /> Reset trên 300LV nhận thêm : <strong>". $log_arr['danhvong_extra'] ." điểm Danh Vọng</strong> (tăng ". $log_arr['danhvong_extra_percent'] ." %).<br />Tổng Điểm Danh Vọng nhận được : <strong>". $log_arr['danhvong_point'] ."</strong> Điểm Danh Vọng.";
            if(isset($log_arr['danhvong_low300'])) $log_des .= "<br />Reset dưới 300LV, không được nhận thêm Điểm Danh Vọng.";
            if(isset($log_arr['danhvong_vip'])) $log_des .= "<br />Reset VIP nhận thêm <strong>". $log_arr['danhvong_vip'] ." điểm Danh Vọng</strong> (tăng ". $log_arr['danhvong_vip_percent'] ." %).<br />Tổng Điểm Danh Vọng nhận được : <strong>". $log_arr['danhvong_point'] ."</strong> Điểm Danh Vọng.";
    	break;
        
        case 6:     // Reset Bu
            $log_arr = json_decode($row[4], true);
            $log_des .= "<b>". $log_arr['name'] ."</b> Reset Bù lần thứ <b>". $log_arr['rsup'] ."</b> khi ". $log_arr['level'] ." Level/". $log_arr['rsold'] ." Reset, Relife ". $log_arr['relife'] .".";
            if(isset($log_arr['resetpoint'])) $log_des .= "<br />Point theo Reset : <strong>". number_format($log_arr['resetpoint'], 0, ',', '.') ." Point</strong>.";
            
            if($log_arr['vip_rs_point_incre'] > 0 || $log_arr['vip_point_percent_add'] > 0) {
                
                $log_des .= "<br />Hệ thống Hỗ Trợ ";
                if($log_arr['vip_day'] > 0) $log_des .= "<strong>". $log_arr['vip_day'] ."d</strong>";
                $log_des .= " : ";
                
                if($log_arr['vip_rs_point_incre'] > 0) {
                    $log_des .= "<strong>". number_format($log_arr['vip_rs_point_incre'], 0, ',', '.') ." Point</strong> ";
                }
                if($log_arr['vip_point_percent_add'] > 0) {
                    $log_des .= " + ";
                    $log_des .= "<strong>". number_format($log_arr['vip_point_percent_add'], 0, ',', '.') ." Point</strong>";
                    if($log_arr['vip_rs_point_percent_incre_val'] > 0) $log_des .= " (". $log_arr['vip_rs_point_percent_incre_val'] ."%)"; 
                }
                
            }
            
            if(isset($log_arr['point_event'])) $log_des .= "<br />Point Event Huy chương : <strong>". $log_arr['point_event'] ." Point</strong>";
            if(isset($log_arr['point_rsday_percent']) && $log_arr['point_rsday_percent'] > 0) $log_des .= "<br />Ngày hôm qua Reset ". $log_arr['rsday_yesterday'] ." lần, thưởng ". $log_arr['point_rsday_percent'] ." % : + <strong>". $log_arr['point_rsday'] ." Point</strong> (". $log_arr['cfg_point_rsday_rs'] ." RS / ". $log_arr['cfg_point_rsday_percent'] ." % Point)";
            if(isset($log_arr['songtu_not_marry']) && $log_arr['songtu_not_marry'] == 1) $log_des .= "<br />Bạn chưa Kết hôn, không nhận được thêm điểm thưởng do kết hôn.";
            if(isset($log_arr['point_songtu_percent']) && $log_arr['point_songtu_percent'] > 0) $log_des .= "<br />Cấp độ Song Tu : ". $log_arr['songtulv'] ." cấp. Nhận thêm ". $log_arr['point_songtu_percent'] ." % = + <strong>". $log_arr['point_songtu'] ." Point</strong>.";
            if(isset($log_arr['point_total'])) $log_des .= "<br /><strong>Tổng Point nhận được : <strong>". $log_arr['point_total'] ." Point</strong></strong>";
            if(isset($log_arr['guild_blance']) && $log_arr['guild_blance'] == 1) {
                $log_des .= "<hr /><strong>Chính sách cân bằng thế lực :</strong>";
                if(isset($log_arr['is_not_guild']) && $log_arr['is_not_guild'] == 1) $log_des .= "<br />Bạn chưa vào Guild. Không nhận được hỗ trợ khi Reset.";
                if(isset($log_arr['guild_maxmem_redurepoint']) && $log_arr['guild_maxmem_redurepoint'] > 0) $log_des .= "<br />Guild bạn có <strong>". $log_arr['G_SlgMem'] ." thành viên</strong>. Nhiều hơn mức cho phép ". $log_arr['guild_maxmem'] ." thành viên. <strong>Bị trừ ". $log_arr['guild_maxmem_redurepoint'] ." %</strong> = - <strong>". $log_arr['point_overmem_redure'] ." Point</strong>.";
                if(isset($log_arr['guild_top12lm_redurepoint']) && $log_arr['guild_top12lm_redurepoint'] > 0) $log_des .= "<br />Guild bạn <strong>TOP Point ". $log_arr['G_TopPoint'] ."</strong> Liên Minh với Guild <strong>TOP Point ". $log_arr['toplm'] ."</strong> . Bị trừ ". $log_arr['guild_top12lm_redurepoint'] ." % = <strong>- ". $log_arr['point_union12_redure'] ." Point</strong>.";
                if(isset($log_arr['guild_addpoint_require_mem'])) $log_des .= "<br />Guild bạn chỉ có <strong>". $log_arr['G_SlgMem'] ." thành viên</strong>. Không đủ điều kiện ". $log_arr['guild_addpoint_require_mem'] ." thành viên để nhận Point hỗ trợ.";
                if(isset($log_arr['G_Created_day'])) $log_des .= "<br />Guild bạn mới <strong>thành lập ". $log_arr['G_Created_day'] ." ngày</strong>. Không đủ điều kiện Guild thành lập trên ". $log_arr['guild_addpoint_require_day'] ." ngày để nhận Point hỗ trợ.";
                if(isset($log_arr['G_RSYesterday'])) $log_des .= "<br />Guild bạn <strong>hôm qua thực hiện ". $log_arr['G_RSYesterday'] ." lần Reset</strong>. Không đủ điều kiện tổng số lần thực hiện Reset trong ngày hôm qua là ". $log_arr['guild_addpoint_require_rs'] ." để nhận Point hỗ trợ.";
                if(isset($log_arr['guild_topaddpoint'])) $log_des .= "<br />Guild bạn <strong>TOP ". $log_arr['G_TopPoint'] ." Guild Point</strong>. Nhận thêm hỗ trợ ". $log_arr['guild_topaddpoint'] ." % = <strong>+ ". $log_arr['point_gtop_extra'] ." Point</strong>.";
                if(isset($log_arr['guild_top10overaddpoint'])) $log_des .= "<br />Guild bạn <strong>không nằm trong TOP 10 Guild Point</strong>. Nhận thêm hỗ trợ ". $log_arr['guild_top10overaddpoint'] ." % = <strong>+ ". $log_arr['point_gtop_extra'] ." Point</strong>.";
            }
            if(isset($log_arr['point_total_2'])) $log_des .= "<br /><strong>Tổng Point cuối cùng nhận được : ". number_format($log_arr['point_total_2'], 0, ',', '.') ." Point</strong>";
    	break;
        
        case 7:     // Reset Ho
            $log_arr = json_decode($row[4], true);
            $log_des .= "<b>". $log_arr['name'] ."</b> Reset Hộ lần thứ <b>". $log_arr['rsup'] ."</b> khi ". $log_arr['level'] ." Level/". $log_arr['rsold'] ." Reset, Relife ". $log_arr['relife'] .".";
    	break;
        
        case 9:     // Ruong do an toan
            $log_arr = json_decode($row[4], true);
            if($log_arr['warehouse_secure_type'] == 1) {
                $warehouse_secure_log_view = "Cất ITEM <strong>". $log_arr['itemname'] ."</strong> : ". $log_arr['itemcode'] ."";
            } elseif($log_arr['warehouse_secure_type'] == 2) {
                $warehouse_secure_log_view = "Rút ITEM <strong>". $log_arr['itemname'] ."</strong> : ". $log_arr['itemcode'] ."";
            } elseif($log_arr['warehouse_secure_type'] == 3) {
                $warehouse_secure_log_view = "Mua 1 Rương An Toàn";
            }
            $log_des .= "[<strong>Rương An Toàn</strong>] $warehouse_secure_log_view";
        break;
        
    	default :
            $log_des = $row[4];
    }
    
    
    echo "<tr>";
        echo "<td align='center'>$time</td>";
        echo "<td align='center'>". number_format($row[0], 0, ',', '.') ."</td>";
        echo "<td align='center'>". number_format($row[1], 0, ',', '.') ."</td>";
        echo "<td align='center'>". number_format($row[2], 0, ',', '.') ."</td>";
        echo "<td align='center'>". number_format($row[7], 0, ',', '.') ."</td>";
        echo "<td align='center'>$row[3]</td>";
        echo "<td align='center'><strong>$row[6]</strong> : $log_des</td>";
    echo "</tr>";
}
echo "</table>";

    if ($total_page > 1) {
		echo "<center>Trang: [".$total_page."] ";
        $page_check = 1;
    	while($page_check <= $total_page && $page_check<=50){
    		if($page_check == $page){
    			echo " [<font color='red' size=5>{$page}</font>] ";
    		} else {
    			echo "<a href=\"?character=$name&type_log=$type_log&page=$page_check\">[$page_check]</a> ";
    		} 
    		$page_check++; 
    	} 

		echo "</center>";
	}
echo "</center>";
}
}
$db->Close();
?>
</body>
</html>