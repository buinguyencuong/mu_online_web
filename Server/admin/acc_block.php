<?php

/**
 * @author NetBanBe
 * @copyright 2014
 */

$acc = $_GET['acc'];
$block_status = $_GET['block_status'];

if($block_status == 1) {
    $block_extra_style = "display: none;";
    $check_block = '';
    $check_noneblock = 'checked="checked"';
    $block_avaiable = ' disabled="disabled" ';
    $noneblock_avaiable = '';
} else {
    $check_block = 'checked="checked"';
    $check_noneblock = '';
    $block_avaiable = '';
    $noneblock_avaiable = ' disabled="disabled" ';
}

?>
<center>Block tài khoản <strong><?php echo $acc; ?></strong></center>
<div id="block_content">
<table width="500" cellpadding="3" cellspacing="3">
    <tr>
        <td align="right">Tài khoản</td>
        <td align="left"><?php echo $acc; ?></td>
    </tr>
    <tr>
        <td align="right">&nbsp;</td>
        <td align="left">
            <input type="radio" name="block_type" value="1" <?php echo $check_block . $block_avaiable; ?> /> Block<br />
            <input type="radio" name="block_type" value="0" <?php echo $check_noneblock . $noneblock_avaiable; ?> /> Giải Block
        </td>
    </tr>
    <tr class="block_extra" style="<?php echo $block_extra_style; ?>">
        <td align="right">Số ngày Block :</td>
        <td align="left"><input type="text" name="day_block" id="day_block" value="1" /></td>
    </tr>
    <tr>
        <td align="right">Admin Nick :</td>
        <td align="left"><input type="text" name="admin" id="admin" maxlength="50" /></td>
    </tr>
    <tr>
        <td align="right">Lý do :</td>
        <td align="left"><input type="text" name="block_des" id="block_des" maxlength="250" /></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td align="left"><input type="button" value="Thực hiện" id="block_process" acc="<?php echo $acc; ?>" /> <span id="block_wait"></span></td>
    </tr>
</table>
</div>