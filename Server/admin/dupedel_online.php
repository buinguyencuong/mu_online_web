<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/

/**
 * @author NetBanBe
 * @copyright 25/3/2012
 */
include_once ("security.php");
include ('../config.php');
$noitem = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
    
// Define Ajax Request
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
if(!IS_AJAX) { echo 'Không được phép truy cập.'; }
else {
    $acc = $_POST['acc'];
    $name = $_POST['name'];
    $itemcode = $_POST['itemcode'];
    
    // Neu do dupe tren nguoi
    if(strlen($name) > 0) {
        $inventory_chardupe_query = "SELECT CAST(Inventory AS image) FROM Character WHERE name='$name'";
        $inventory_chardupe_result = $db->Execute($inventory_chardupe_query) OR DIE("Query Error : $inventory_chardupe_query");
        $inventory_chardupe_fetch = $inventory_chardupe_result->FetchRow();
        $inventory_chardupe = $inventory_chardupe_fetch[0];
        $inventory_chardupe = bin2hex($inventory_chardupe);
        if(strlen($inventory_chardupe) > 0) {
            $inventory_chardupe = strtoupper($inventory_chardupe);
            $inventory_new = preg_replace('/'. $itemcode .'/', $noitem, $inventory_chardupe, 1);
           
            if($inventory_new !=  $inventory_chardupe) {
                $inventory_update_query = "UPDATE Character SET Inventory=0x$inventory_new WHERE Name='$name'";
                $inventory_update_result = $db->Execute($inventory_update_query) OR DIE("Query Error : $inventory_update_query");
            } else {
                $error .= "Item đã bị thay đổi vị trí. Không thể xóa.";
            }
        } else {
            $error .= "Không lấy được dữ liệu Item trên nhân vật <strong>$name</strong>";
        }
            
    }
    // Neu do Dupe trong ruong
    else {
        $warehouse_query = "SELECT CAST(Items AS image) FROM warehouse WHERE AccountID='$acc'";
        $warehouse_result = $db->Execute($warehouse_query) OR DIE("Query Error : $warehouse_query");
        $warehouse_fetch = $warehouse_result->FetchRow();
        $warehouse = $warehouse_fetch[0];
        $warehouse = bin2hex($warehouse);
        if(strlen($warehouse) > 0) {
            $warehouse = strtoupper($warehouse);
            $warehouse_new = preg_replace('/'. $itemcode .'/', $noitem, $warehouse, 1);
            
            if($warehouse_new != $warehouse) {
                $warehouse_update_query = "UPDATE warehouse SET Items=0x$warehouse_new WHERE AccountID='$acc'";
                $warehouse_update_result = $db->Execute($warehouse_update_query) OR DIE("Query Error : $warehouse_update_query");
            } else {
                $error .= "Item đã bị thay đổi vị trí. Không thể xóa.";
            }
        } else {
            $error .= "Không lấy được dữ liệu Item trong hòm đồ tài khoản $acc";
        }
    }
    
    if(strlen($error) > 0) echo $error;
    else echo "Đã Xóa";
}

?>