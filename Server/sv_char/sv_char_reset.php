<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/

include_once('config/config_reset.php');
include_once('config/config_hotrotanthu.php');
include_once('config/config_gioihanrs.php');
include_once('config/config_relife.php');
include_once('config/config_event.php');
include_once('config/config_chucnang.php');
include_once('config/config_point_rsday.php');
include_once('config/config_guild_balance.php');
include_once('config/config_tuluyen.php');
include_once('config/config_cuonghoa.php');
include_once('config/config_vip.php');
include_once('config/config_vip_system.php');

$login = $_POST['login'];
$name = $_POST['name'];

$resetnow = $_POST['resetnow'];  $resetnow = abs(intval($resetnow));

$passtransfer = $_POST['passtransfer'];

if ($passtransfer == $transfercode) {

$string_login = $_POST['string_login'];
checklogin($login,$string_login);

if(check_nv($login, $name) == 0) {
    echo "Nhân vật <b>{$name}</b> không nằm trong tài khoản <b>{$login}</b>. Vui lòng kiểm tra lại."; exit();
}


fixrs($name);

kiemtra_doinv($login,$name);
kiemtra_online($login);
_guild_xh();

$sql_PkLevel_check = $db->Execute("SELECT PkLevel FROM Character WHERE PkLevel > 3 AND AccountID = '$login' AND Name='$name'");
$PkLevel_check = $sql_PkLevel_check->numrows();

$sql_char_back_reged_check = $db->Execute("SELECT Name FROM Character_back WHERE Name='$name' and AccountID = '$login'"); 
$char_back_reged_check = $sql_char_back_reged_check->numrows();

$result = $db->Execute("Select Clevel,Resets,Money,LevelUpPoint,Class,Relifes,NoResetInDay,Resets_Time, Top50, NBB_Relifes_0h, NBB_Resets_0h, nbb_point_default_str, nbb_point_default_agi, nbb_point_default_vit, nbb_point_default_ene From Character WHERE AccountID = '$login' AND Name='$name'");
$row = $result->fetchrow();
$clevel_before = $row[0];
$char_in_top = $row[8];
$char_relfe = $row[5];
$char_rsday = $row[6];
$reset_now = $row[1];
$relife_now = $row[5];
$reset_0h = $row[10];
$relife_0h = $row[9];
$zen_char_before = $row[2];

$nbb_point_default_str = $row[11];
$nbb_point_default_agi = $row[12];
$nbb_point_default_vit = $row[13];
$nbb_point_default_ene = $row[14];

if($resetnow >0 && abs(intval($row[1])) == 0) {
    echo "Hệ thống Reset bị gián đoạn. Vui lòng Reset lại";
    exit();
}


$acc_q = "SELECT jewel_chao,jewel_cre,jewel_blue, thehe, nbb_vip_time, nbb_vip_time_begin, bank, acc_vip, acc_vip_day, acc_vip_time FROM MEMB_INFO WHERE memb___id='$login'";
$acc_r = $db->Execute($acc_q);
    check_queryerror($acc_q, $acc_r);
$acc_f = $acc_r->FetchRow();
$jewel_chao = $acc_f[0];
$jewel_cre = $acc_f[1];
$jewel_blue = $acc_f[2];
$thehe = $acc_f[3];
$nbb_vip_time = $acc_f[4];
$nbb_vip_time_begin = $acc_f[5];
$zen_bank_before = $acc_f[6];
$acc_vip = $acc_f[7];
$acc_vip_day = $acc_f[8];
$acc_vip_time = $acc_f[9];

$ResetDay = _get_reset_day($name,$timestamp);
if($char_rsday == 0 && $ResetDay > 0) {
    _topreset_erase_month($name, $month);
    $ResetDay = 0;
}
$CountNoResetInDay=$ResetDay+1;

//Begin Giới hạn Reset trong ngày
if($use_gioihanrs[$thehe] == 1) {
    
    include_once('config_license.php');
    include_once('func_getContent.php');
    $getcontent_url = $url_license . "/api_gioihanrs2.php";
    $getcontent_data = array(
        'acclic'    =>  $acclic,
        'key'    =>  $key,
        
        'char_in_top'    =>  $char_in_top,
        'ghrs_cap_max'  =>  $ghrs_cap_max,
        'ghrs_top_cap'  =>  $ghrs_top_cap,
        'ghrs_rsmax_cap'    =>  $ghrs_rsmax_cap[$thehe],
		'gioihanrs_rs_bu'	=>	$gioihanrs_rs_bu[$thehe],
		'gioihanrs_up_rs'	=>	$gioihanrs_up_rs[$thehe],
        'overrs_sat_extra'    =>  $overrs_sat_extra[$thehe],
        'overrs_sun_extra'    =>  $overrs_sun_extra[$thehe],
        'dayofweek'         =>  date('w', $timestamp)
    ); 
    
    $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);

	if ( empty($reponse) ) {
        $notice = "Server bảo trì vui lòng liên hệ Admin để FIX";
    }
    else {
        $info = read_TagName($reponse, 'info');
        if($info == "Error") {
            $message = read_TagName($reponse, 'message');
        } elseif ($info == "OK") {
            $gioihanrs = read_TagName($reponse, 'gioihanrs');
            if(strlen($gioihanrs) == 0) {
                echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                
                $arr_view = "\nDataSend:\n";
                foreach($getcontent_data as $k => $v) {
                    $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                }
                writelog("log_api.txt", $arr_view . $reponse);
                exit();
            }
        } else {
            echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
            writelog("log_api.txt", $reponse);
            exit();
        }
    }

	if( isset($gioihanrs) && $ResetDay >= $gioihanrs) {
		echo "Bạn đã Reset hết số lần Reset trong ngày. Xin vui lòng Ủy thác và đợi Reset tiếp vào ngày mai"; exit();
	}
} elseif($use_gioihanrs[$thehe] == 2) {
    $top1_q = "SELECT NBB_Relifes_0h, NBB_Resets_0h FROM Character JOIN MEMB_INFO ON Character.Top50=1 AND Character.AccountID collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=$thehe";
    $top1_r = $db->Execute($top1_q);
        check_queryerror($top1_q, $top1_r);
    $top1_rl = 0;
    $top1_rs = 0;
    $top1_c = $top1_r->NumRows();
    
    if($top1_c > 1) {
        //Reset TOP 50
        $resettop50_query = "UPDATE Character SET Top50=0 FROM Character JOIN MEMB_INFO ON Character.AccountID collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=$thehe AND Top50>0";
        $resettop50_result = $db->Execute($resettop50_query);
            check_queryerror($resettop50_query, $resettop50_result);
            
        $query_top50 = "SELECT TOP 500 Name FROM Character JOIN MEMB_INFO ON Character.AccountID collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=$thehe ORDER BY NBB_Relifes_0h DESC, NBB_Resets_0h DESC, NBB_Clevel_0h DESC, NBB_Resets_Lastime_0h";
    	$result_top50 = $db->Execute($query_top50);
            check_queryerror($query_top50, $result_top50);
        $top = 1;
        while( $top50 = $result_top50->fetchrow() )
    	{
    	   $name = $top50[0];
    		$updatetop50_query = "UPDATE Character SET Top50=$top WHERE Name='$name'";
            $updatetop50_result = $db->Execute($updatetop50_query);
                check_queryerror($updatetop50_query, $updatetop50_result);
            $top++;
    	}
        
        $top1_q = "SELECT NBB_Relifes_0h, NBB_Resets_0h FROM Character JOIN MEMB_INFO ON Character.Top50=1 AND Character.AccountID collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=$thehe";
        $top1_r = $db->Execute($top1_q);
            check_queryerror($top1_q, $top1_r);
        $top1_c = $top1_r->NumRows();
    }
    
    if($top1_c > 0) {
        $top1_f = $top1_r->FetchRow();
        $top1_rl = $top1_f[0];
        $top1_rs = $top1_f[1];
    }
    
    $reset_uythac_day = _get_reset_uythac_day($name,$timestamp);
    
    include_once('config_license.php');
    include_once('func_getContent.php');
    $getcontent_url = $url_license . "/api_gioihanrs3.php";
    $getcontent_data = array(
        'acclic'    =>  $acclic,
        'key'    =>  $key,
        
        'rl_reset_relife' =>  $rl_reset_relife,
                
        'top1_rl'   =>  $top1_rl,
        'top1_rs'   =>  $top1_rs,
        
        'resetday'  =>  $ResetDay,
        'char_rs_now'   =>  $reset_now,
        'char_rl_now'   =>  $relife_now,
        'char_rs_0h'   =>  $reset_0h,
        'char_rl_0h'   =>  $relife_0h,
        'rs_uythac_day' =>  $reset_uythac_day,
        
        'char_in_top'    =>  $char_in_top,
        
        'ghrs_cap_max'  =>  $ghrs_cap_max,
        'ghrs2_top_cap'  =>  $ghrs2_top_cap,
        'ghrs2_rsmax_cap'    =>  $ghrs2_rsmax_cap[$thehe],
        'ghrs2_rsmax_daily'    =>  $ghrs2_rsmax_daily[$thehe],
		
		'gioihanrs_rs_bu'	=>	$gioihanrs2_rs_bu[$thehe],
		'gioihanrs_up_rs'	=>	$gioihanrs2_up_rs[$thehe],
        
        'overrs_sat_extra'    =>  $overrs_sat_extra[$thehe],
        'overrs_sun_extra'    =>  $overrs_sun_extra[$thehe],
        'dayofweek'         =>  date('w', $timestamp)
    ); 
    
    $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);

	if ( empty($reponse) ) {
        $notice = "Server bảo trì vui lòng liên hệ Admin để FIX";
    }
    else {
        $info = read_TagName($reponse, 'info');
        if($info == "Error") {
            $message = read_TagName($reponse, 'message');
        } elseif ($info == "OK") {
            $gioihanrs = read_TagName($reponse, 'gioihanrs');
            if(strlen($gioihanrs) == 0 || intval($gioihanrs) != $gioihanrs) {
                echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                
                $arr_view = "\nDataSend:\n";
                foreach($getcontent_data as $k => $v) {
                    $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                }
                writelog("log_api.txt", $arr_view . $reponse);
                exit();
            }
        } else {
            echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
            writelog("log_api.txt", $reponse);
            exit();
        }
    }

	if( isset($gioihanrs) && $ResetDay >= $gioihanrs) {
		echo "Bạn đã Reset hết số lần Reset trong ngày. Xin vui lòng Ủy thác và đợi Reset tiếp vào ngày mai"; exit();
	}
}
//End Giới hạn Reset trong ngày

if ($PkLevel_check > 0){ 
	 echo "Bạn đang là Sát thủ. Phải rửa tội trước khi Reset.<p><center><a class='btn btn-green btn-block btn-lg' rel='ajax' href='#char_manager&amp;act=pk'>Rửa Tội</a></center></p>"; exit();
}

if ( ($row[1] >= $reset_cap_0) AND ($row[1] < $reset_cap_1) )
{
	$level = $level_cap_1;
	$zen = $zen_cap_1;
	$time_reset_next = $time_reset_next_1;
	$chao = $chao_cap_1;
	$cre = $cre_cap_1;
	$blue = $blue_cap_1;
}
elseif ( ($row[1] >= $reset_cap_1) AND ($row[1] < $reset_cap_2) )
{
	$level = $level_cap_2;
	$zen = $zen_cap_2;
	$time_reset_next = $time_reset_next_2;
	$chao = $chao_cap_2;
	$cre = $cre_cap_2;
	$blue = $blue_cap_2;
}
elseif ( ($row[1] >= $reset_cap_2) AND ($row[1] < $reset_cap_3) )
{
	$level = $level_cap_3;
	$zen = $zen_cap_3;
	$time_reset_next = $time_reset_next_3;
	$chao = $chao_cap_3;
	$cre = $cre_cap_3;
	$blue = $blue_cap_3;
}
elseif ( ($row[1] >= $reset_cap_3) AND ($row[1] < $reset_cap_4) )
{
	$level = $level_cap_4;
	$zen = $zen_cap_4;
	$time_reset_next = $time_reset_next_4;
	$chao = $chao_cap_4;
	$cre = $cre_cap_4;
	$blue = $blue_cap_4;
}
elseif ( ($row[1] >= $reset_cap_4) AND ($row[1] < $reset_cap_5) )
{
	$level = $level_cap_5;
	$zen = $zen_cap_5;
	$time_reset_next = $time_reset_next_5;
	$chao = $chao_cap_5;
	$cre = $cre_cap_5;
	$blue = $blue_cap_5;
}
elseif ( ($row[1] >= $reset_cap_5) AND ($row[1] < $reset_cap_6) )
{
	$level = $level_cap_6;
	$zen = $zen_cap_6;
	$time_reset_next = $time_reset_next_6;
	$chao = $chao_cap_6;
	$cre = $cre_cap_6;
	$blue = $blue_cap_6;
}
elseif ( ($row[1] >= $reset_cap_6) AND ($row[1] < $reset_cap_7) )
{
	$level = $level_cap_7;
	$zen = $zen_cap_7;
	$time_reset_next = $time_reset_next_7;
	$chao = $chao_cap_7;
	$cre = $cre_cap_7;
	$blue = $blue_cap_7;
}
elseif ( ($row[1] >= $reset_cap_7) AND ($row[1] < $reset_cap_8) )
{
	$level = $level_cap_8;
	$zen = $zen_cap_8;
	$time_reset_next = $time_reset_next_8;
	$chao = $chao_cap_8;
	$cre = $cre_cap_8;
	$blue = $blue_cap_8;
}
elseif ( ($row[1] >= $reset_cap_8) AND ($row[1] < $reset_cap_9) )
{
	$level = $level_cap_9;
	$zen = $zen_cap_9;
	$time_reset_next = $time_reset_next_9;
	$chao = $chao_cap_9;
	$cre = $cre_cap_9;
	$blue = $blue_cap_9;
}
elseif ( ($row[1] >= $reset_cap_9) AND ($row[1] < $reset_cap_10) )
{
	$level = $level_cap_10;
	$zen = $zen_cap_10;
	$time_reset_next = $time_reset_next_10;
	$chao = $chao_cap_10;
	$cre = $cre_cap_10;
	$blue = $blue_cap_10;
}
elseif ( ($row[1] >= $reset_cap_10) AND ($row[1] < $reset_cap_11) )
{
	$level = $level_cap_11;
	$zen = $zen_cap_11;
	$time_reset_next = $time_reset_next_11;
	$chao = $chao_cap_11;
	$cre = $cre_cap_11;
	$blue = $blue_cap_11;
}
elseif ( ($row[1] >= $reset_cap_11) AND ($row[1] < $reset_cap_12) )
{
	$level = $level_cap_12;
	$zen = $zen_cap_12;
	$time_reset_next = $time_reset_next_12;
	$chao = $chao_cap_12;
	$cre = $cre_cap_12;
	$blue = $blue_cap_12;
}
elseif ( ($row[1] >= $reset_cap_12) AND ($row[1] < $reset_cap_13) )
{
	$level = $level_cap_13;
	$zen = $zen_cap_13;
	$time_reset_next = $time_reset_next_13;
	$chao = $chao_cap_13;
	$cre = $cre_cap_13;
	$blue = $blue_cap_13;
}
elseif ( ($row[1] >= $reset_cap_13) AND ($row[1] < $reset_cap_14) )
{
	$level = $level_cap_14;
	$zen = $zen_cap_14;
	$time_reset_next = $time_reset_next_14;
	$chao = $chao_cap_14;
	$cre = $cre_cap_14;
	$blue = $blue_cap_14;
}
elseif ( ($row[1] >= $reset_cap_14) AND ($row[1] < $reset_cap_15) )
{
	$level = $level_cap_15;
	$zen = $zen_cap_15;
	$time_reset_next = $time_reset_next_15;
	$chao = $chao_cap_15;
	$cre = $cre_cap_15;
	$blue = $blue_cap_15;
}
elseif ( ($row[1] >= $reset_cap_15) AND ($row[1] < $reset_cap_16) )
{
	$level = $level_cap_16;
	$zen = $zen_cap_16;
	$time_reset_next = $time_reset_next_16;
	$chao = $chao_cap_16;
	$cre = $cre_cap_16;
	$blue = $blue_cap_16;
}
elseif ( ($row[1] >= $reset_cap_16) AND ($row[1] < $reset_cap_17) )
{
	$level = $level_cap_17;
	$zen = $zen_cap_17;
	$time_reset_next = $time_reset_next_17;
	$chao = $chao_cap_17;
	$cre = $cre_cap_17;
	$blue = $blue_cap_17;
}
elseif ( ($row[1] >= $reset_cap_17) AND ($row[1] < $reset_cap_18) )
{
	$level = $level_cap_18;
	$zen = $zen_cap_18;
	$time_reset_next = $time_reset_next_18;
	$chao = $chao_cap_18;
	$cre = $cre_cap_18;
	$blue = $blue_cap_18;
}
elseif ( ($row[1] >= $reset_cap_18) AND ($row[1] < $reset_cap_19) )
{
	$level = $level_cap_19;
	$zen = $zen_cap_19;
	$time_reset_next = $time_reset_next_19;
	$chao = $chao_cap_19;
	$cre = $cre_cap_19;
	$blue = $blue_cap_19;
}
elseif ( ($row[1] >= $reset_cap_19) AND ($row[1] < $reset_cap_20) )
{
	$level = $level_cap_20;
	$zen = $zen_cap_20;
	$time_reset_next = $time_reset_next_20;
	$chao = $chao_cap_20;
	$cre = $cre_cap_20;
	$blue = $blue_cap_20;
}
$chao_rs = $chao;
$cre_rs = $cre;
$blue_rs = $blue;

//Begin hỗ trợ tân thủ
$zen_rs_redure = 0;
if ($hotrotanthu == 1 && $char_in_top != 1) {
	
    if(!is_array($tanthu_time_rs_redure)) $tanthu_time_rs_redure = array();
    if(!is_array($tanthu_zen_rs_redure)) $tanthu_zen_rs_redure = array();
    if(!is_array($tanthu_money_rs_redure)) $tanthu_money_rs_redure = array();
    
    include_once('config_license.php');
    include_once('func_getContent.php');
    $getcontent_url = $url_license . "/api_hotrotanthu2.php";
    $getcontent_data = array(
        'acclic'    =>  $acclic,
        'key'    =>  $key,
        
        'level'    =>  $level,
        'timers'    =>  $time_reset_next,
        'char_in_top'    =>  $char_in_top,
        
        'tanthu_cap_max'    =>  $tanthu_cap_max,
        'tanthu_top_cap'    =>  $tanthu_top_cap,
        'tanthu_lvredure'   =>  $tanthu_lvredure,
        'tanthu_time_rs_redure'   =>  $tanthu_time_rs_redure,
        'tanthu_zen_rs_redure'  =>  $tanthu_zen_rs_redure,
        'tanthu_money_rs_redure'    =>  $tanthu_money_rs_redure
    ); 
    
    $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);

	if ( empty($reponse) ) {
        $notice = "Server bảo trì vui lòng liên hệ Admin để FIX";
    }
    else {
        $info = read_TagName($reponse, 'info');
        if($info == "Error") {
            $message = read_TagName($reponse, 'message');
        } elseif ($info == "OK") {
            $level = read_TagName($reponse, 'level');
            $timers = read_TagName($reponse, 'timers');
            $zen_rs_redure = read_TagName($reponse, 'zen_rs_redure');
            if(strlen($level) == 0 || strlen($timers) == 0) {
                echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                $arr_view = "\nDataSend:\n";
                foreach($getcontent_data as $k => $v) {
                    $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                }
                writelog("log_api.txt", $arr_view . $reponse);
                exit();
            }
        } else {
            echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
            writelog("log_api.txt", $reponse);
            exit();
        }
    }
}
$zen_rs = $zen - floor($zen*$zen_rs_redure/100);
//End hỗ trợ tân thủ

if($Use_VIP == 1 && $nbb_vip_time > $timestamp) {
    $nbb_vip_time_used = $timestamp - $nbb_vip_time_begin;
    $nbb_vip_time_used_day = floor($nbb_vip_time_used/(24*60*60));
    $vip_lv_index = 0;
    if(is_array($vip_level_day)) {
        foreach($vip_level_day as $k => $v) {
            if($nbb_vip_time_used_day >= $v) {
                $vip_lv_index = $k;
                $vip_day_lv = $v;
            }
            else break;
        }
    }
    
    $vip_day_view = '';
    if($vip_day_lv > 0) $vip_day_view = "<span>". $vip_day_lv ."d</span>";
    
    $level = $level - $vip_rs_lv_decre[$vip_lv_index];
    $vip_rs_point_incre_val = $vip_rs_point_incre[$vip_lv_index];
    $vip_rs_point_percent_incre_val = $vip_rs_point_percent_incre[$vip_lv_index];
    
    $vip_rs_price_decre_flag = -1;
    if(is_array($vip_rs_price_decre_rsday)) {
        foreach($vip_rs_price_decre_rsday as $k => $v) {
            if($CountNoResetInDay >= $v && $vip_rs_price_decre_rsday[$vip_rs_price_decre_flag] < $v) {
                $vip_rs_price_decre_flag = $k;
            }
        }
    }
    if($vip_rs_price_decre_flag >= 0) {
        $zen_acc_vip_giam = floor($zen*$vip_rs_price_decre_percent[$vip_rs_price_decre_flag]/100);
        $zen_rs = $zen_rs - $zen_acc_vip_giam;
        
        $chao_acc_vip_giam = floor($chao*$vip_rs_price_decre_percent[$vip_rs_price_decre_flag]/100);
        $chao_rs = $chao_rs - $chao_acc_vip_giam;
        
        $cre_acc_vip_giam = floor($cre*$vip_rs_price_decre_percent[$vip_rs_price_decre_flag]/100);
        $cre_rs = $cre_rs - $cre_acc_vip_giam;
        
        $blue_acc_vip_giam = floor($blue*$vip_rs_price_decre_percent[$vip_rs_price_decre_flag]/100);
        $blue_rs = $blue_rs - $blue_acc_vip_giam;
        
        
    }
}

if($Use_VIP == 2 && $acc_vip > 0 && $acc_vip_day >= $enable_vip['level_reset'] && $acc_vip_time > $timestamp) {
	if($acc_vip == 1) {
		$extra_level_reset = floor($level*$gold_vip['level_reset']/100);
		$level = $level - $extra_level_reset;
		$vip_rs_point_percent_incre_val = $gold_vip['point_reset']; 
		$extra_zen_reset = floor($zen*$gold_vip['zen_reset']/100);
        $zen_rs = $zen_rs - $extra_zen_reset;
	}
	elseif($acc_vip == 2) {
		$extra_level_reset = floor($level*$silver_vip['level_reset']/100);
		$level = $level - $extra_level_reset;
		$vip_rs_point_percent_incre_val = $silver_vip['point_reset']; 
		$extra_zen_reset = floor($zen*$silver_vip['zen_reset']/100);
        $zen_rs = $zen_rs - $extra_zen_reset;
	}   
}

if($zen_rs < 0) $zen_rs = 0;
if($chao_rs < 0) $chao_rs = 0;
if($cre_rs < 0) $cre_rs = 0;
if($blue_rs < 0) $blue_rs = 0;


if($row[7] > $timestamp) $row[7] = 0;
$timers = $row[7]+$timers*60;
if ($timers > $timestamp) {
	$time_free = $timers - $timestamp;
	echo "$name cần $time_free giây nữa để Reset lần tiếp theo."; exit();
}


$zen_after = _price_zen($zen_char_before, $zen_bank_before, $zen_rs);
if($zen_after['status'] == 0) {
    echo "Không đủ ZEN tiến hành Reset. Cần tối thiểu ". number_format($rsmaster_price_zen, 0, ',', '.') ." ZEN tổng trong ngân hàng và nhân vật.";
    exit();
}
$zen_bank_after = $zen_after['zen_bank'];
$zen_char_after = $zen_after['zen_char'];

$char_relife_next = $char_relfe + 1;

$reset_relifes = $rl_reset_relife[$char_relife_next];
$point_relifes = $rl_point_relife[$char_relfe];
$ml_relifes = $rl_ml_relife[$char_relfe];

if ( $row[1] >= $reset_relifes ) { 
	 echo "$name đang ReLife: $char_relfe - Reset: $row[1]. Để Reset tiếp bạn cần phải ReLife."; exit();
}

$relife = $char_relfe;
$resetold = $row[1];        $resetold = abs(intval($resetold));
$resetup = $resetold + 1;

//Cong thuc Reset
if ( ($jewel_chao < $chao_rs) OR ($jewel_cre < $cre_rs) OR ($jewel_blue < $blue_rs) )
{
	echo "Bạn không đủ Jewel trong ngân hàng.<br>Số lần Reset hiện tại của bạn là $row[1]. Bạn cần $chao Chao , $cre Creation , $blue Blue Feather."; exit();
}

$chao_after = $jewel_chao - $chao_rs;
$cre_after = $jewel_cre - $cre_rs;
$blue_after = $jewel_blue - $blue_rs;

if ($row[0] < $level) {echo "$name cần $level level để Reset lần $resetup."; exit();}



//Reset lần 1
if ($row[1] == $reset_cap_0)
	{
		$resetpoint=$point_relifes+$point_cap_1;
		$leadership = $ml_relifes+$ml_cap_1;
	}
//Reset cấp 1
elseif ($row[1] < $reset_cap_1)
	{
		$resetpoint=$point_relifes+$point_cap_1+$row[1]*$point_cap_1;
		$leadership = $ml_relifes+$ml_relifes+$ml_cap_1+$row[1]*$ml_cap_1;
	}
//Reset cấp 1 -> 2
elseif ($row[1] >= $reset_cap_1 AND $row[1] < $reset_cap_2)
	{
		$resetpoint=$point_relifes+($point_cap_1*$reset_cap_1)+($row[1]-($reset_cap_1-1))*$point_cap_2;
		$leadership=$ml_relifes+($ml_cap_1*$reset_cap_1)+($row[1]-($reset_cap_1-1))*$ml_cap_2;
	}
//Reset cấp 2 -> 3
elseif ($row[1] >= $reset_cap_2 AND $row[1] < $reset_cap_3)
	{
		$resetpoint=$point_relifes+($point_cap_1*$reset_cap_1)+($point_cap_2*($reset_cap_2-$reset_cap_1))+($row[1]-($reset_cap_2-1))*$point_cap_3;
		$leadership=$ml_relifes+($ml_cap_1*$reset_cap_1)+($ml_cap_2*($reset_cap_2-$reset_cap_1))+($row[1]-($reset_cap_2-1))*$ml_cap_3;
	}
//Reset cấp 3 -> 4
elseif ($row[1] >= $reset_cap_3 AND $row[1] < $reset_cap_4)
	{
		$resetpoint=$point_relifes+($point_cap_1*$reset_cap_1)+($point_cap_2*($reset_cap_2-$reset_cap_1))+($point_cap_3*($reset_cap_3-$reset_cap_2))+($row[1]-($reset_cap_3-1))*$point_cap_4;
		$leadership=$ml_relifes+($ml_cap_1*$reset_cap_1)+($ml_cap_2*($reset_cap_2-$reset_cap_1))+($ml_cap_3*($reset_cap_3-$reset_cap_2))+($row[1]-($reset_cap_3-1))*$ml_cap_4;
	}
//Reset cấp 4 -> 5
elseif ($row[1] >= $reset_cap_4 AND $row[1] < $reset_cap_5)
	{
		$resetpoint=$point_relifes+($point_cap_1*$reset_cap_1)+($point_cap_2*($reset_cap_2-$reset_cap_1))+($point_cap_3*($reset_cap_3-$reset_cap_2))+($point_cap_4*($reset_cap_4-$reset_cap_3))+($row[1]-($reset_cap_4-1))*$point_cap_5;
		$leadership=$ml_relifes+($ml_cap_1*$reset_cap_1)+($ml_cap_2*($reset_cap_2-$reset_cap_1))+($ml_cap_3*($reset_cap_3-$reset_cap_2))+($ml_cap_4*($reset_cap_4-$reset_cap_3))+($row[1]-($reset_cap_4-1))*$ml_cap_5;
	}
//Reset cấp 5 -> 6
elseif ($row[1] >= $reset_cap_5 AND $row[1] < $reset_cap_6)
	{
		$resetpoint=$point_relifes+($point_cap_1*$reset_cap_1)+($point_cap_2*($reset_cap_2-$reset_cap_1))+($point_cap_3*($reset_cap_3-$reset_cap_2))+($point_cap_4*($reset_cap_4-$reset_cap_3))+($point_cap_5*($reset_cap_5-$reset_cap_4))+($row[1]-($reset_cap_5-1))*$point_cap_6;
		$leadership=$ml_relifes+($ml_cap_1*$reset_cap_1)+($ml_cap_2*($reset_cap_2-$reset_cap_1))+($ml_cap_3*($reset_cap_3-$reset_cap_2))+($ml_cap_4*($reset_cap_4-$reset_cap_3))+($ml_cap_5*($reset_cap_5-$reset_cap_4))+($row[1]-($reset_cap_5-1))*$ml_cap_6;
	}
//Reset cấp 6 -> 7
elseif ($row[1] >= $reset_cap_6 AND $row[1] < $reset_cap_7)
	{
		$resetpoint=$point_relifes+($point_cap_1*$reset_cap_1)+($point_cap_2*($reset_cap_2-$reset_cap_1))+($point_cap_3*($reset_cap_3-$reset_cap_2))+($point_cap_4*($reset_cap_4-$reset_cap_3))+($point_cap_5*($reset_cap_5-$reset_cap_4))+($point_cap_6*($reset_cap_6-$reset_cap_5))+($row[1]-($reset_cap_6-1))*$point_cap_7;
		$leadership=$ml_relifes+($ml_cap_1*$reset_cap_1)+($ml_cap_2*($reset_cap_2-$reset_cap_1))+($ml_cap_3*($reset_cap_3-$reset_cap_2))+($ml_cap_4*($reset_cap_4-$reset_cap_3))+($ml_cap_5*($reset_cap_5-$reset_cap_4))+($ml_cap_6*($reset_cap_6-$reset_cap_5))+($row[1]-($reset_cap_6-1))*$ml_cap_7;
	}
//Reset cấp 7 -> 8
elseif ($row[1] >= $reset_cap_7 AND $row[1] < $reset_cap_8)
	{
		$resetpoint=$point_relifes+($point_cap_1*$reset_cap_1)+($point_cap_2*($reset_cap_2-$reset_cap_1))+($point_cap_3*($reset_cap_3-$reset_cap_2))+($point_cap_4*($reset_cap_4-$reset_cap_3))+($point_cap_5*($reset_cap_5-$reset_cap_4))+($point_cap_6*($reset_cap_6-$reset_cap_5))+($point_cap_7*($reset_cap_7-$reset_cap_6))+($row[1]-($reset_cap_7-1))*$point_cap_8;
		$leadership=$ml_relifes+($ml_cap_1*$reset_cap_1)+($ml_cap_2*($reset_cap_2-$reset_cap_1))+($ml_cap_3*($reset_cap_3-$reset_cap_2))+($ml_cap_4*($reset_cap_4-$reset_cap_3))+($ml_cap_5*($reset_cap_5-$reset_cap_4))+($ml_cap_6*($reset_cap_6-$reset_cap_5))+($ml_cap_7*($reset_cap_7-$reset_cap_6))+($row[1]-($reset_cap_7-1))*$ml_cap_8;
	}
//Reset cấp 8 -> 9
elseif ($row[1] >= $reset_cap_8 AND $row[1] < $reset_cap_9)
	 {
		$resetpoint=$point_relifes+($point_cap_1*$reset_cap_1)+($point_cap_2*($reset_cap_2-$reset_cap_1))+($point_cap_3*($reset_cap_3-$reset_cap_2))+($point_cap_4*($reset_cap_4-$reset_cap_3))+($point_cap_5*($reset_cap_5-$reset_cap_4))+($point_cap_6*($reset_cap_6-$reset_cap_5))+($point_cap_7*($reset_cap_7-$reset_cap_6))+($point_cap_8*($reset_cap_8-$reset_cap_7))+($row[1]-($reset_cap_8-1))*$point_cap_9;
		$leadership=$ml_relifes+($ml_cap_1*$reset_cap_1)+($ml_cap_2*($reset_cap_2-$reset_cap_1))+($ml_cap_3*($reset_cap_3-$reset_cap_2))+($ml_cap_4*($reset_cap_4-$reset_cap_3))+($ml_cap_5*($reset_cap_5-$reset_cap_4))+($ml_cap_6*($reset_cap_6-$reset_cap_5))+($ml_cap_7*($reset_cap_7-$reset_cap_6))+($ml_cap_8*($reset_cap_8-$reset_cap_7))+($row[1]-($reset_cap_8-1))*$ml_cap_9;
	}
//Reset cấp 9 -> 10
elseif ($row[1] >= $reset_cap_9 AND $row[1] < $reset_cap_10)
	{
		$resetpoint=$point_relifes+($point_cap_1*$reset_cap_1)+($point_cap_2*($reset_cap_2-$reset_cap_1))+($point_cap_3*($reset_cap_3-$reset_cap_2))+($point_cap_4*($reset_cap_4-$reset_cap_3))+($point_cap_5*($reset_cap_5-$reset_cap_4))+($point_cap_6*($reset_cap_6-$reset_cap_5))+($point_cap_7*($reset_cap_7-$reset_cap_6))+($point_cap_8*($reset_cap_8-$reset_cap_7))+($point_cap_9*($reset_cap_9-$reset_cap_8))+($row[1]-($reset_cap_9-1))*$point_cap_10;
		$leadership=$ml_relifes+($ml_cap_1*$reset_cap_1)+($ml_cap_2*($reset_cap_2-$reset_cap_1))+($ml_cap_3*($reset_cap_3-$reset_cap_2))+($ml_cap_4*($reset_cap_4-$reset_cap_3))+($ml_cap_5*($reset_cap_5-$reset_cap_4))+($ml_cap_6*($reset_cap_6-$reset_cap_5))+($ml_cap_7*($reset_cap_7-$reset_cap_6))+($ml_cap_8*($reset_cap_8-$reset_cap_7))+($ml_cap_9*($reset_cap_9-$reset_cap_8))+($row[1]-($reset_cap_9-1))*$ml_cap_10;
	 }
//Reset cấp 10 -> 11
elseif ($row[1] >= $reset_cap_10 AND $row[1] < $reset_cap_11)
	{
		$resetpoint=$point_relifes+($point_cap_1*$reset_cap_1)+($point_cap_2*($reset_cap_2-$reset_cap_1))+($point_cap_3*($reset_cap_3-$reset_cap_2))+($point_cap_4*($reset_cap_4-$reset_cap_3))+($point_cap_5*($reset_cap_5-$reset_cap_4))+($point_cap_6*($reset_cap_6-$reset_cap_5))+($point_cap_7*($reset_cap_7-$reset_cap_6))+($point_cap_8*($reset_cap_8-$reset_cap_7))+($point_cap_9*($reset_cap_9-$reset_cap_8))+($point_cap_10*($reset_cap_10-$reset_cap_9))+($row[1]-($reset_cap_10-1))*$point_cap_11;
		$leadership=$ml_relifes+($ml_cap_1*$reset_cap_1)+($ml_cap_2*($reset_cap_2-$reset_cap_1))+($ml_cap_3*($reset_cap_3-$reset_cap_2))+($ml_cap_4*($reset_cap_4-$reset_cap_3))+($ml_cap_5*($reset_cap_5-$reset_cap_4))+($ml_cap_6*($reset_cap_6-$reset_cap_5))+($ml_cap_7*($reset_cap_7-$reset_cap_6))+($ml_cap_8*($reset_cap_8-$reset_cap_7))+($ml_cap_9*($reset_cap_9-$reset_cap_8))+($ml_cap_10*($reset_cap_10-$reset_cap_9))+($row[1]-($reset_cap_10-1))*$ml_cap_11;
	}
//Reset cấp 11 -> 12
elseif ($row[1] >= $reset_cap_11 AND $row[1] < $reset_cap_12)
	{
		$resetpoint=$point_relifes+($point_cap_1*$reset_cap_1)+($point_cap_2*($reset_cap_2-$reset_cap_1))+($point_cap_3*($reset_cap_3-$reset_cap_2))+($point_cap_4*($reset_cap_4-$reset_cap_3))+($point_cap_5*($reset_cap_5-$reset_cap_4))+($point_cap_6*($reset_cap_6-$reset_cap_5))+($point_cap_7*($reset_cap_7-$reset_cap_6))+($point_cap_8*($reset_cap_8-$reset_cap_7))+($point_cap_9*($reset_cap_9-$reset_cap_8))+($point_cap_10*($reset_cap_10-$reset_cap_9))+($point_cap_11*($reset_cap_11-$reset_cap_10))+($row[1]-($reset_cap_11-1))*$point_cap_12;
		$leadership=$ml_relifes+($ml_cap_1*$reset_cap_1)+($ml_cap_2*($reset_cap_2-$reset_cap_1))+($ml_cap_3*($reset_cap_3-$reset_cap_2))+($ml_cap_4*($reset_cap_4-$reset_cap_3))+($ml_cap_5*($reset_cap_5-$reset_cap_4))+($ml_cap_6*($reset_cap_6-$reset_cap_5))+($ml_cap_7*($reset_cap_7-$reset_cap_6))+($ml_cap_8*($reset_cap_8-$reset_cap_7))+($ml_cap_9*($reset_cap_9-$reset_cap_8))+($ml_cap_10*($reset_cap_10-$reset_cap_9))+($ml_cap_11*($reset_cap_11-$reset_cap_10))+($row[1]-($reset_cap_11-1))*$ml_cap_12;
	 }
//Reset cấp 12 -> 13
elseif ($row[1] >= $reset_cap_12 AND $row[1] < $reset_cap_13)
	{
		$resetpoint=$point_relifes+($point_cap_1*$reset_cap_1)+($point_cap_2*($reset_cap_2-$reset_cap_1))+($point_cap_3*($reset_cap_3-$reset_cap_2))+($point_cap_4*($reset_cap_4-$reset_cap_3))+($point_cap_5*($reset_cap_5-$reset_cap_4))+($point_cap_6*($reset_cap_6-$reset_cap_5))+($point_cap_7*($reset_cap_7-$reset_cap_6))+($point_cap_8*($reset_cap_8-$reset_cap_7))+($point_cap_9*($reset_cap_9-$reset_cap_8))+($point_cap_10*($reset_cap_10-$reset_cap_9))+($point_cap_11*($reset_cap_11-$reset_cap_10))+($point_cap_12*($reset_cap_12-$reset_cap_11))+($row[1]-($reset_cap_12-1))*$point_cap_13;
		$leadership=$ml_relifes+($ml_cap_1*$reset_cap_1)+($ml_cap_2*($reset_cap_2-$reset_cap_1))+($ml_cap_3*($reset_cap_3-$reset_cap_2))+($ml_cap_4*($reset_cap_4-$reset_cap_3))+($ml_cap_5*($reset_cap_5-$reset_cap_4))+($ml_cap_6*($reset_cap_6-$reset_cap_5))+($ml_cap_7*($reset_cap_7-$reset_cap_6))+($ml_cap_8*($reset_cap_8-$reset_cap_7))+($ml_cap_9*($reset_cap_9-$reset_cap_8))+($ml_cap_10*($reset_cap_10-$reset_cap_9))+($ml_cap_11*($reset_cap_11-$reset_cap_10))+($ml_cap_12*($reset_cap_12-$reset_cap_11))+($row[1]-($reset_cap_12-1))*$ml_cap_13;
	}
//Reset cấp 13 -> 14
elseif ($row[1] >= $reset_cap_13 AND $row[1] < $reset_cap_14)
	{
		$resetpoint=$point_relifes+($point_cap_1*$reset_cap_1)+($point_cap_2*($reset_cap_2-$reset_cap_1))+($point_cap_3*($reset_cap_3-$reset_cap_2))+($point_cap_4*($reset_cap_4-$reset_cap_3))+($point_cap_5*($reset_cap_5-$reset_cap_4))+($point_cap_6*($reset_cap_6-$reset_cap_5))+($point_cap_7*($reset_cap_7-$reset_cap_6))+($point_cap_8*($reset_cap_8-$reset_cap_7))+($point_cap_9*($reset_cap_9-$reset_cap_8))+($point_cap_10*($reset_cap_10-$reset_cap_9))+($point_cap_11*($reset_cap_11-$reset_cap_10))+($point_cap_12*($reset_cap_12-$reset_cap_11))+($point_cap_13*($reset_cap_13-$reset_cap_12))+($row[1]-($reset_cap_13-1))*$point_cap_14;
		$leadership=$ml_relifes+($ml_cap_1*$reset_cap_1)+($ml_cap_2*($reset_cap_2-$reset_cap_1))+($ml_cap_3*($reset_cap_3-$reset_cap_2))+($ml_cap_4*($reset_cap_4-$reset_cap_3))+($ml_cap_5*($reset_cap_5-$reset_cap_4))+($ml_cap_6*($reset_cap_6-$reset_cap_5))+($ml_cap_7*($reset_cap_7-$reset_cap_6))+($ml_cap_8*($reset_cap_8-$reset_cap_7))+($ml_cap_9*($reset_cap_9-$reset_cap_8))+($ml_cap_10*($reset_cap_10-$reset_cap_9))+($ml_cap_11*($reset_cap_11-$reset_cap_10))+($ml_cap_12*($reset_cap_12-$reset_cap_11))+($ml_cap_13*($reset_cap_13-$reset_cap_12))+($row[1]-($reset_cap_13-1))*$ml_cap_14;
	}
//Reset cấp 14 -> 15
elseif ($row[1] >= $reset_cap_14 AND $row[1] < $reset_cap_15)
	{
		$resetpoint=$point_relifes+($point_cap_1*$reset_cap_1)+($point_cap_2*($reset_cap_2-$reset_cap_1))+($point_cap_3*($reset_cap_3-$reset_cap_2))+($point_cap_4*($reset_cap_4-$reset_cap_3))+($point_cap_5*($reset_cap_5-$reset_cap_4))+($point_cap_6*($reset_cap_6-$reset_cap_5))+($point_cap_7*($reset_cap_7-$reset_cap_6))+($point_cap_8*($reset_cap_8-$reset_cap_7))+($point_cap_9*($reset_cap_9-$reset_cap_8))+($point_cap_10*($reset_cap_10-$reset_cap_9))+($point_cap_11*($reset_cap_11-$reset_cap_10))+($point_cap_12*($reset_cap_12-$reset_cap_11))+($point_cap_13*($reset_cap_13-$reset_cap_12))+($point_cap_14*($reset_cap_14-$reset_cap_13))+($row[1]-($reset_cap_14-1))*$point_cap_15;
		$leadership=$ml_relifes+($ml_cap_1*$reset_cap_1)+($ml_cap_2*($reset_cap_2-$reset_cap_1))+($ml_cap_3*($reset_cap_3-$reset_cap_2))+($ml_cap_4*($reset_cap_4-$reset_cap_3))+($ml_cap_5*($reset_cap_5-$reset_cap_4))+($ml_cap_6*($reset_cap_6-$reset_cap_5))+($ml_cap_7*($reset_cap_7-$reset_cap_6))+($ml_cap_8*($reset_cap_8-$reset_cap_7))+($ml_cap_9*($reset_cap_9-$reset_cap_8))+($ml_cap_10*($reset_cap_10-$reset_cap_9))+($ml_cap_11*($reset_cap_11-$reset_cap_10))+($ml_cap_12*($reset_cap_12-$reset_cap_11))+($ml_cap_13*($reset_cap_13-$reset_cap_12))+($ml_cap_14*($reset_cap_14-$reset_cap_13))+($row[1]-($reset_cap_14-1))*$ml_cap_15;
	}
//Reset cấp 15 -> 16
elseif ($row[1] >= $reset_cap_15 AND $row[1] < $reset_cap_16)
	{
		$resetpoint=$point_relifes+($point_cap_1*$reset_cap_1)+($point_cap_2*($reset_cap_2-$reset_cap_1))+($point_cap_3*($reset_cap_3-$reset_cap_2))+($point_cap_4*($reset_cap_4-$reset_cap_3))+($point_cap_5*($reset_cap_5-$reset_cap_4))+($point_cap_6*($reset_cap_6-$reset_cap_5))+($point_cap_7*($reset_cap_7-$reset_cap_6))+($point_cap_8*($reset_cap_8-$reset_cap_7))+($point_cap_9*($reset_cap_9-$reset_cap_8))+($point_cap_10*($reset_cap_10-$reset_cap_9))+($point_cap_11*($reset_cap_11-$reset_cap_10))+($point_cap_12*($reset_cap_12-$reset_cap_11))+($point_cap_13*($reset_cap_13-$reset_cap_12))+($point_cap_14*($reset_cap_14-$reset_cap_13))+($point_cap_15*($reset_cap_15-$reset_cap_14))+($row[1]-($reset_cap_15-1))*$point_cap_16;
		$leadership=$ml_relifes+($ml_cap_1*$reset_cap_1)+($ml_cap_2*($reset_cap_2-$reset_cap_1))+($ml_cap_3*($reset_cap_3-$reset_cap_2))+($ml_cap_4*($reset_cap_4-$reset_cap_3))+($ml_cap_5*($reset_cap_5-$reset_cap_4))+($ml_cap_6*($reset_cap_6-$reset_cap_5))+($ml_cap_7*($reset_cap_7-$reset_cap_6))+($ml_cap_8*($reset_cap_8-$reset_cap_7))+($ml_cap_9*($reset_cap_9-$reset_cap_8))+($ml_cap_10*($reset_cap_10-$reset_cap_9))+($ml_cap_11*($reset_cap_11-$reset_cap_10))+($ml_cap_12*($reset_cap_12-$reset_cap_11))+($ml_cap_13*($reset_cap_13-$reset_cap_12))+($ml_cap_14*($reset_cap_14-$reset_cap_13))+($ml_cap_15*($reset_cap_15-$reset_cap_14))+($row[1]-($reset_cap_15-1))*$ml_cap_16;
	}
//Reset cấp 16 -> 17
elseif ($row[1] >= $reset_cap_16 AND $row[1] < $reset_cap_17)
	{
		$resetpoint=$point_relifes+($point_cap_1*$reset_cap_1)+($point_cap_2*($reset_cap_2-$reset_cap_1))+($point_cap_3*($reset_cap_3-$reset_cap_2))+($point_cap_4*($reset_cap_4-$reset_cap_3))+($point_cap_5*($reset_cap_5-$reset_cap_4))+($point_cap_6*($reset_cap_6-$reset_cap_5))+($point_cap_7*($reset_cap_7-$reset_cap_6))+($point_cap_8*($reset_cap_8-$reset_cap_7))+($point_cap_9*($reset_cap_9-$reset_cap_8))+($point_cap_10*($reset_cap_10-$reset_cap_9))+($point_cap_11*($reset_cap_11-$reset_cap_10))+($point_cap_12*($reset_cap_12-$reset_cap_11))+($point_cap_13*($reset_cap_13-$reset_cap_12))+($point_cap_14*($reset_cap_14-$reset_cap_13))+($point_cap_15*($reset_cap_15-$reset_cap_14))+($point_cap_16*($reset_cap_16-$reset_cap_15))+($row[1]-($reset_cap_16-1))*$point_cap_17;
		$leadership=$ml_relifes+($ml_cap_1*$reset_cap_1)+($ml_cap_2*($reset_cap_2-$reset_cap_1))+($ml_cap_3*($reset_cap_3-$reset_cap_2))+($ml_cap_4*($reset_cap_4-$reset_cap_3))+($ml_cap_5*($reset_cap_5-$reset_cap_4))+($ml_cap_6*($reset_cap_6-$reset_cap_5))+($ml_cap_7*($reset_cap_7-$reset_cap_6))+($ml_cap_8*($reset_cap_8-$reset_cap_7))+($ml_cap_9*($reset_cap_9-$reset_cap_8))+($ml_cap_10*($reset_cap_10-$reset_cap_9))+($ml_cap_11*($reset_cap_11-$reset_cap_10))+($ml_cap_12*($reset_cap_12-$reset_cap_11))+($ml_cap_13*($reset_cap_13-$reset_cap_12))+($ml_cap_14*($reset_cap_14-$reset_cap_13))+($ml_cap_15*($reset_cap_15-$reset_cap_14))+($ml_cap_16*($reset_cap_16-$reset_cap_15))+($row[1]-($reset_cap_16-1))*$ml_cap_17;
	}
//Reset cấp 17 -> 18
elseif ($row[1] >= $reset_cap_17 AND $row[1] < $reset_cap_18)
	{
		$resetpoint=$point_relifes+($point_cap_1*$reset_cap_1)+($point_cap_2*($reset_cap_2-$reset_cap_1))+($point_cap_3*($reset_cap_3-$reset_cap_2))+($point_cap_4*($reset_cap_4-$reset_cap_3))+($point_cap_5*($reset_cap_5-$reset_cap_4))+($point_cap_6*($reset_cap_6-$reset_cap_5))+($point_cap_7*($reset_cap_7-$reset_cap_6))+($point_cap_8*($reset_cap_8-$reset_cap_7))+($point_cap_9*($reset_cap_9-$reset_cap_8))+($point_cap_10*($reset_cap_10-$reset_cap_9))+($point_cap_11*($reset_cap_11-$reset_cap_10))+($point_cap_12*($reset_cap_12-$reset_cap_11))+($point_cap_13*($reset_cap_13-$reset_cap_12))+($point_cap_14*($reset_cap_14-$reset_cap_13))+($point_cap_15*($reset_cap_15-$reset_cap_14))+($point_cap_16*($reset_cap_16-$reset_cap_15))+($point_cap_17*($reset_cap_17-$reset_cap_16))+($row[1]-($reset_cap_17-1))*$point_cap_18;
		$leadership=$ml_relifes+($ml_cap_1*$reset_cap_1)+($ml_cap_2*($reset_cap_2-$reset_cap_1))+($ml_cap_3*($reset_cap_3-$reset_cap_2))+($ml_cap_4*($reset_cap_4-$reset_cap_3))+($ml_cap_5*($reset_cap_5-$reset_cap_4))+($ml_cap_6*($reset_cap_6-$reset_cap_5))+($ml_cap_7*($reset_cap_7-$reset_cap_6))+($ml_cap_8*($reset_cap_8-$reset_cap_7))+($ml_cap_9*($reset_cap_9-$reset_cap_8))+($ml_cap_10*($reset_cap_10-$reset_cap_9))+($ml_cap_11*($reset_cap_11-$reset_cap_10))+($ml_cap_12*($reset_cap_12-$reset_cap_11))+($ml_cap_13*($reset_cap_13-$reset_cap_12))+($ml_cap_14*($reset_cap_14-$reset_cap_13))+($ml_cap_15*($reset_cap_15-$reset_cap_14))+($ml_cap_16*($reset_cap_16-$reset_cap_15))+($ml_cap_17*($reset_cap_17-$reset_cap_16))+($row[1]-($reset_cap_17-1))*$ml_cap_18;
	}
//Reset cấp 18 -> 19
elseif ($row[1] >= $reset_cap_18 AND $row[1] < $reset_cap_19)
	{
		$resetpoint=$point_relifes+($point_cap_1*$reset_cap_1)+($point_cap_2*($reset_cap_2-$reset_cap_1))+($point_cap_3*($reset_cap_3-$reset_cap_2))+($point_cap_4*($reset_cap_4-$reset_cap_3))+($point_cap_5*($reset_cap_5-$reset_cap_4))+($point_cap_6*($reset_cap_6-$reset_cap_5))+($point_cap_7*($reset_cap_7-$reset_cap_6))+($point_cap_8*($reset_cap_8-$reset_cap_7))+($point_cap_9*($reset_cap_9-$reset_cap_8))+($point_cap_10*($reset_cap_10-$reset_cap_9))+($point_cap_11*($reset_cap_11-$reset_cap_10))+($point_cap_12*($reset_cap_12-$reset_cap_11))+($point_cap_13*($reset_cap_13-$reset_cap_12))+($point_cap_14*($reset_cap_14-$reset_cap_13))+($point_cap_15*($reset_cap_15-$reset_cap_14))+($point_cap_16*($reset_cap_16-$reset_cap_15))+($point_cap_17*($reset_cap_17-$reset_cap_16))+($point_cap_18*($reset_cap_18-$reset_cap_17))+($row[1]-($reset_cap_18-1))*$point_cap_19;
		$leadership=$ml_relifes+($ml_cap_1*$reset_cap_1)+($ml_cap_2*($reset_cap_2-$reset_cap_1))+($ml_cap_3*($reset_cap_3-$reset_cap_2))+($ml_cap_4*($reset_cap_4-$reset_cap_3))+($ml_cap_5*($reset_cap_5-$reset_cap_4))+($ml_cap_6*($reset_cap_6-$reset_cap_5))+($ml_cap_7*($reset_cap_7-$reset_cap_6))+($ml_cap_8*($reset_cap_8-$reset_cap_7))+($ml_cap_9*($reset_cap_9-$reset_cap_8))+($ml_cap_10*($reset_cap_10-$reset_cap_9))+($ml_cap_11*($reset_cap_11-$reset_cap_10))+($ml_cap_12*($reset_cap_12-$reset_cap_11))+($ml_cap_13*($reset_cap_13-$reset_cap_12))+($ml_cap_14*($reset_cap_14-$reset_cap_13))+($ml_cap_15*($reset_cap_15-$reset_cap_14))+($ml_cap_16*($reset_cap_16-$reset_cap_15))+($ml_cap_17*($reset_cap_17-$reset_cap_16))+($ml_cap_18*($reset_cap_18-$reset_cap_17))+($row[1]-($reset_cap_18-1))*$ml_cap_19;
	}
//Reset cấp 19 -> 20
elseif ($row[1] >= $reset_cap_19 AND $row[1] < $reset_cap_20)
	{
		$resetpoint=$point_relifes+($point_cap_1*$reset_cap_1)+($point_cap_2*($reset_cap_2-$reset_cap_1))+($point_cap_3*($reset_cap_3-$reset_cap_2))+($point_cap_4*($reset_cap_4-$reset_cap_3))+($point_cap_5*($reset_cap_5-$reset_cap_4))+($point_cap_6*($reset_cap_6-$reset_cap_5))+($point_cap_7*($reset_cap_7-$reset_cap_6))+($point_cap_8*($reset_cap_8-$reset_cap_7))+($point_cap_9*($reset_cap_9-$reset_cap_8))+($point_cap_10*($reset_cap_10-$reset_cap_9))+($point_cap_11*($reset_cap_11-$reset_cap_10))+($point_cap_12*($reset_cap_12-$reset_cap_11))+($point_cap_13*($reset_cap_13-$reset_cap_12))+($point_cap_14*($reset_cap_14-$reset_cap_13))+($point_cap_15*($reset_cap_15-$reset_cap_14))+($point_cap_16*($reset_cap_16-$reset_cap_15))+($point_cap_17*($reset_cap_17-$reset_cap_16))+($point_cap_18*($reset_cap_18-$reset_cap_17))+($point_cap_19*($reset_cap_19-$reset_cap_18))+($row[1]-($reset_cap_19-1))*$point_cap_20;
		$leadership=$ml_relifes+($ml_cap_1*$reset_cap_1)+($ml_cap_2*($reset_cap_2-$reset_cap_1))+($ml_cap_3*($reset_cap_3-$reset_cap_2))+($ml_cap_4*($reset_cap_4-$reset_cap_3))+($ml_cap_5*($reset_cap_5-$reset_cap_4))+($ml_cap_6*($reset_cap_6-$reset_cap_5))+($ml_cap_7*($reset_cap_7-$reset_cap_6))+($ml_cap_8*($reset_cap_8-$reset_cap_7))+($ml_cap_9*($reset_cap_9-$reset_cap_8))+($ml_cap_10*($reset_cap_10-$reset_cap_9))+($ml_cap_11*($reset_cap_11-$reset_cap_10))+($ml_cap_12*($reset_cap_12-$reset_cap_11))+($ml_cap_13*($reset_cap_13-$reset_cap_12))+($ml_cap_14*($reset_cap_14-$reset_cap_13))+($ml_cap_15*($reset_cap_15-$reset_cap_14))+($ml_cap_16*($reset_cap_16-$reset_cap_15))+($ml_cap_17*($reset_cap_17-$reset_cap_16))+($ml_cap_18*($reset_cap_18-$reset_cap_17))+($ml_cap_19*($reset_cap_19-$reset_cap_18))+($row[1]-($reset_cap_19-1))*$ml_cap_20;
	}
//Fix Menh lenh DarkLord > 32k
	if ( $leadership>32000 ) $leadership=32000;


$ClassType =  $row[4];
    switch ($ClassType){ 
    	case 0:
        case 1:
        case 2:
        case 3:
            $Class_Default = 0;
    	break;
    
    	case 16:
        case 17:
        case 18:
        case 19:
            $Class_Default = 16;
    	break;
    
    	case 32:
        case 33:
        case 34:
        case 35:
            $Class_Default = 32;
    	break;
        
        case 48:
        case 49:
        case 50:
            $Class_Default = 48;
    	break;
        
        case 64:
        case 65:
        case 66:
            $Class_Default = 64;
    	break;
        
        case 80:
        case 81:
        case 82:
        case 83:
            $Class_Default = 80;
    	break;
        
        case 96:
        case 97:
        case 98:
            $Class_Default = 96;
    	break;
    
    	default :
            $Class_Default = 0;
    }
    $default_query = "SELECT Strength, Dexterity, Vitality, Energy, Life, MaxLife, Mana, MaxMana, MapNumber, MapPosX, MapPosY FROM DefaultClassType WHERE Class=" . $Class_Default;
    $default_result = $db->execute($default_query);
        check_queryerror($default_query, $default_result);
    $point_default = $default_result->fetchrow();
    $Strength_Default = $point_default[0];
    $Dexterity_Default = $point_default[1];
    $Vitality_Default = $point_default[2];
    $Energy_Default = $point_default[3];
    $Life_Default = abs(intval($point_default[4]));
    $MaxLife_Default = abs(intval($point_default[5]));
    $Mana_Default = abs(intval($point_default[6]));
    $MaxMana_Default = abs(intval($point_default[7]));
    $MapNumber_Default = abs(intval($point_default[8]));
    $MapPosX_Default = abs(intval($point_default[9]));
    $MapPosY_Default = abs(intval($point_default[10]));
    
    $Life = $Life_Default;
    $MaxLife = $MaxLife_Default;
    $Mana = $Mana_Default;
    $MaxMana = $MaxMana_Default;
    $MapNumber = $MapNumber_Default;
    $MapPosX = $MapPosX_Default;
    $MapPosY = $MapPosY_Default;
    $Mapdir=0;
    
//End Cong thuc Reset

//Tat ca cac Quest
$all_quest="Update character set Quest=0xaaeaffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff where AccountID = '$login' AND name='$name'";
if( intval($level_after_reset) > 6 ) $clevel = $level_after_reset;
else $clevel = 6;

// Point Reset Day
    $point_rsday = 0;
    $rsday_yesterday = _get_reset_day($name, $timestamp - 24*60*60);
    $point_rsday_percent = floor($rsday_yesterday/$cfg_point_rsday_rs) * $cfg_point_rsday_percent;
    $point_rsday = floor($resetpoint * $point_rsday_percent / 100);
// Point Reset Day End

$point_event_qr = $db->Execute("SELECT point_event FROM Character WHERE AccountID = '$login' AND Name='$name'");
$point_event_f = $point_event_qr->fetchrow();
$point_event = $point_event_f[0];
    
    $point_total = $resetpoint + $point_event + $point_rsday;

$point_songtu_msg = "";
$point_songtu = 0;

if($Use_SongTu == 1) {
    include_once('config/config_songtu.php');
    $songtu_q = "SELECT SCFMarried, nbbsongtu_lv FROM Character WHERE Name='$name'";
    $songtu_r = $db->Execute($songtu_q);
        check_queryerror($songtu_q, $songtu_r);
    $songtu_f = $songtu_r->FetchRow();
    $married = $songtu_f[0];
    $songtulv = $songtu_f[1];
    if($married != 1) {
        $point_songtu_msg = "<br />Bạn chưa Kết hôn, không nhận được thêm điểm thưởng do kết hôn.";
        $log_arr['songtu_not_marry'] = 1;
    } else {
        $point_songtu_percent = $songtu_pointpercent * (1 + $songtulv);
        $point_songtu = floor($point_songtu_percent * $resetpoint / 100);
        $point_total += $point_songtu;
        $point_songtu_msg = "<br />Cấp độ Song Tu : $songtulv cấp. Nhận thêm $point_songtu_percent % = + <strong>$point_songtu Point</strong>.";
        $log_arr['songtulv'] = $songtulv;
        $log_arr['point_songtu_percent'] = $point_songtu_percent;
        $log_arr['point_songtu'] = $point_songtu;
    }
}

if($Use_VIP == 1 && $nbb_vip_time > $timestamp) {
    $vip_point_percent_add = floor($resetpoint * ($vip_rs_point_percent_incre_val/100));
    $point_total += $vip_rs_point_incre_val;
    $point_total += $vip_point_percent_add;
    $log_arr['vip_day'] = $vip_day_view;
    $log_arr['vip_rs_point_incre'] = $vip_rs_point_incre_val;
    $log_arr['vip_rs_point_percent_incre_val'] = $vip_rs_point_percent_incre_val;
    $log_arr['vip_point_percent_add'] = $vip_point_percent_add;
    $vip_lv = $vip_lv_index + 1;
    $vip_msg = "<br /><span class='vip-box' title='VIP'><img src='images/super.gif' />$vip_day_view</span> <span class='tag tag-orange'>nhận thêm : ". number_format($vip_rs_point_incre_val, 0, ',', '.') ." Point và ". number_format($vip_point_percent_add, 0, ',', '.') ." Point ($vip_rs_point_percent_incre_val%)</span>";
}

if($Use_VIP == 2 && $acc_vip > 0 && $acc_vip_day >= $enable_vip['level_reset'] && $acc_vip_time > $timestamp) {
    $vip_point_percent_add = floor($resetpoint * ($vip_rs_point_percent_incre_val/100));
    $point_total += $vip_point_percent_add;
    $log_arr['acc_vip_day'] = $acc_vip_day;
    $log_arr['vip_rs_point_percent_incre_val'] = $vip_rs_point_percent_incre_val;
    $log_arr['vip_point_percent_add'] = $vip_point_percent_add;
	if($acc_vip == 1) {
		$vip_msg = "<br /><span class='vip-box' title='VIP'><img src='templates/images/icons/vip/gold-title-$acc_vip_day.png' /></span> <span class='tag tag-orange'>nhận thêm : ". number_format($vip_point_percent_add, 0, ',', '.') ." Point ($vip_rs_point_percent_incre_val%)</span>";
	}
	elseif($acc_vip == 2) {
		$vip_msg = "<br /><span class='vip-box' title='VIP'><img src='templates/images/icons/vip/silver-title-$acc_vip_day.png' /></span> <span class='tag tag-orange'>nhận thêm : ". number_format($vip_point_percent_add, 0, ',', '.') ." Point ($vip_rs_point_percent_incre_val%)</span>";
	}   
}

$point_msg = "Point theo Reset : <strong>$resetpoint Point</strong>.";
$point_msg .= $vip_msg;
$point_msg .= "<br />Point Event Huy chương : <strong>$point_event Point</strong>";
$point_msg .= "<br />Ngày hôm qua Reset $rsday_yesterday lần, thưởng $point_rsday_percent % : + <strong>$point_rsday Point</strong> ($cfg_point_rsday_rs RS / $cfg_point_rsday_percent % Point)";
$point_msg .= $point_songtu_msg;
$point_msg .= "<br /><strong>Tổng Point nhận được : <strong>$point_total Point</strong></strong>";

$log_arr['resetpoint'] = $resetpoint;
$log_arr['point_event'] = $point_event;
$log_arr['rsday_yesterday'] = $rsday_yesterday;
$log_arr['point_rsday_percent'] = $point_rsday_percent;
$log_arr['point_rsday'] = $point_rsday;
$log_arr['cfg_point_rsday_rs'] = $cfg_point_rsday_rs;
$log_arr['cfg_point_rsday_percent'] = $cfg_point_rsday_percent;
$log_arr['point_total'] = $point_total;

// Save TOP Point
$datetime_now = date('Y-m-d H:i:s', $timestamp);
$point_tuluyen = $point_tl['str'] + $point_tl['agi'] + $point_tl['vit'] + $point_tl['ene'];
$toppoint_check_q = "SELECT count(acc) FROM nbb_toppoint WHERE acc='$login' AND name='$name'";
$toppoint_check_r = $db->Execute($toppoint_check_q);
    check_queryerror($toppoint_check_q, $toppoint_check_r);
$toppoint_check_f = $toppoint_check_r->FetchRow();
$toppoint_check = $toppoint_check_f[0];
if($toppoint_check == 0) {  // Chua co du lieu, khoi tao moi
    $toppoint_q = "INSERT INTO nbb_toppoint (acc, name, point_total, point_rs, point_rsday, point_event, point_songtu, point_tuluyen, time_begin) VALUES ('$login', '$name', $point_total, $resetpoint, $point_rsday, $point_event, $point_songtu, $point_tuluyen, '$datetime_now')";
} else {
    $toppoint_q = "UPDATE nbb_toppoint SET point_total = $point_total, point_rs = $resetpoint, point_rsday = $point_rsday, point_event = $point_event, point_songtu = $point_songtu, point_tuluyen = $point_tuluyen, time_update = '$datetime_now' WHERE acc='$login' AND name='$name'";
}
$toppoint_r = $db->Execute($toppoint_q);
    check_queryerror($toppoint_q, $toppoint_r);
// Save TOP Point End

// Extra Point From Guild
$point_msg .= "";
if($guild_maxmem_redurepoint > 0 || $guild_top12lm_redurepoint > 0 || $guild_top2addpoint > 0 || $guild_top3addpoint > 0 || $guild_top4addpoint > 0 || $guild_top5addpoint > 0 || $guild_top6overaddpoint > 0 || $guild_top10overaddpoint > 0) {
    $point_msg .= "<hr /><strong>Chính sách cân bằng thế lực :</strong>";
    $log_arr['guild_blance'] = 1;
    $guild_extra_point = _guild_extra_point($name);
    if($guild_extra_point != 'none') {
        $log_arr['G_SlgMem'] = $guild_extra_point['G_SlgMem'];
        $log_arr['G_TopPoint'] = $guild_extra_point['G_TopPoint'];
        // Guild nhieu hon so thanh vien cho phep
        if($guild_maxmem_redurepoint > 0 && $guild_extra_point['G_SlgMem'] > $guild_maxmem) {
            $point_overmem_redure = floor($guild_maxmem_redurepoint * $resetpoint / 100);
            $point_total -= $point_overmem_redure;
            $point_overmem_msg = "<br />Guild bạn có <strong>". $guild_extra_point['G_SlgMem'] ." thành viên</strong>. Nhiều hơn mức cho phép $guild_maxmem thành viên. <strong>Bị trừ $guild_maxmem_redurepoint %</strong> = - <strong>$point_overmem_redure Point</strong>.";
            $point_msg .= $point_overmem_msg;
            $log_arr['guild_maxmem'] = $guild_maxmem;
            $log_arr['guild_maxmem_redurepoint'] = $guild_maxmem_redurepoint;
            $log_arr['point_overmem_redure'] = $point_overmem_redure;
        }
        
        // Guild TOP 1 Lien Minh Guild TOP 2
        if($guild_top12lm_redurepoint > 0 && ($guild_extra_point['G_TopPoint'] == 1 || $guild_extra_point['G_TopPoint'] == 2)) {
            $g_union_q = "SELECT G_Union FROM GUILD JOIN GuildMember ON GUILD.G_Name collate DATABASE_DEFAULT = GuildMember.G_Name collate DATABASE_DEFAULT AND Name='$name'";
            $g_union_r = $db->Execute($g_union_q);
                check_queryerror($g_union_q, $g_union_r);
            $g_union_f = $g_union_r->FetchRow();
            $g_union = $g_union_f[0];
            
            if($g_union > 0) {
            	if($guild_extra_point['G_TopPoint'] == 1) {
    	            $toplm = 2;
    	        } else {
    	            $toplm = 1;
    	        }
    	        $g_union_top12chk_q = "SELECT count(*) FROM GUILD WHERE G_Union=$g_union AND G_TopPoint = $toplm";
    	        $g_union_top12chk_r = $db->Execute($g_union_top12chk_q);
    	            check_queryerror($g_union_top12chk_q, $g_union_top12chk_r);
    	        $g_union_top12chk_f = $g_union_top12chk_r->FetchRow();
    	        
    	        if($g_union_top12chk_f[0] >= 1) {
    	            $point_union12_redure = floor($guild_top12lm_redurepoint * $resetpoint / 100);
    	            $point_total -= $point_union12_redure;
    	            $point_union12_msg = "<br />Guild bạn <strong>TOP Point ". $guild_extra_point['G_TopPoint'] ."</strong> Liên Minh với Guild <strong>TOP Point $toplm</strong> . Bị trừ $guild_top12lm_redurepoint % = <strong>- $point_union12_redure Point</strong>.";
                    $point_msg .= $point_union12_msg;
                    $log_arr['toplm'] = $toplm;
                    $log_arr['guild_top12lm_redurepoint'] = $guild_top12lm_redurepoint;
                    $log_arr['point_union12_redure'] = $point_union12_redure;                    
    	        }
            }
        }
        
        // Ho tro Point Guild Yeu
        if($guild_top2addpoint > 0 || $guild_top3addpoint > 0 || $guild_top4addpoint > 0 || $guild_top5addpoint > 0 || $guild_top6overaddpoint > 0 || $guild_top10overaddpoint > 0) {
            $point_gyeu_flag = true;
            if($guild_extra_point['G_SlgMem'] < $guild_addpoint_require_mem) {
                $point_gtop_msg .= "<br />Guild bạn chỉ có <strong>". $guild_extra_point['G_SlgMem'] ." thành viên</strong>. Không đủ điều kiện $guild_addpoint_require_mem thành viên để nhận Point hỗ trợ.";
                $point_gyeu_flag = false;
                $log_arr['guild_addpoint_require_mem'] = $guild_addpoint_require_mem;
            }
            if($guild_extra_point['G_Created_day'] < $guild_addpoint_require_day) {
                $point_gtop_msg .= "<br />Guild bạn mới <strong>thành lập ". $guild_extra_point['G_Created_day'] ." ngày</strong>. Không đủ điều kiện Guild thành lập trên $guild_addpoint_require_day ngày để nhận Point hỗ trợ.";
                $point_gyeu_flag = false;
                $log_arr['guild_addpoint_require_day'] = $guild_addpoint_require_day;
                $log_arr['G_Created_day'] = $guild_extra_point['G_Created_day'];
            }
            if($guild_extra_point['G_RSYesterday'] < $guild_addpoint_require_rs) {
                $point_gtop_msg .= "<br />Guild bạn <strong>hôm qua thực hiện ". $guild_extra_point['G_RSYesterday'] ." lần Reset</strong>. Không đủ điều kiện tổng số lần thực hiện Reset trong ngày hôm qua là $guild_addpoint_require_rs để nhận Point hỗ trợ.";
                $point_gyeu_flag = false;
                $log_arr['guild_addpoint_require_rs'] = $guild_addpoint_require_rs;
                $log_arr['G_RSYesterday'] = $guild_extra_point['G_RSYesterday'];
            }
            
            if($point_gyeu_flag == true) {
                switch ($guild_extra_point['G_TopPoint']) {
                	case 1:
                        $log_arr['guild_topaddpoint'] = 0;
                        $log_arr['point_gtop_extra'] = 0;
                        $point_gtop_msg = "";
                    break;
                    
                    case 2:
                        
                        $point_gtop_extra = floor($guild_top2addpoint * $resetpoint / 100);
                        $point_total += $point_gtop_extra;
                        $point_gtop_msg = "<br />Guild bạn <strong>TOP ". $guild_extra_point['G_TopPoint'] ." Guild Point</strong>. Nhận thêm hỗ trợ $guild_top2addpoint % = <strong>+ $point_gtop_extra Point</strong>.";
                        $log_arr['guild_topaddpoint'] = $guild_top2addpoint;
                        $log_arr['point_gtop_extra'] = $point_gtop_extra;
                	break;
                
                	case 3:
                        $point_gtop_extra = floor($guild_top3addpoint * $resetpoint / 100);
                        $point_total += $point_gtop_extra;
                        $point_gtop_msg = "<br />Guild bạn <strong>TOP ". $guild_extra_point['G_TopPoint'] ." Guild Point</strong>. Nhận thêm hỗ trợ $guild_top3addpoint % = <strong>+ $point_gtop_extra Point</strong>.";
                        $log_arr['guild_topaddpoint'] = $guild_top3addpoint;
                        $log_arr['point_gtop_extra'] = $point_gtop_extra;
                	break;
                    
                    case 4:
                        $point_gtop_extra = floor($guild_top4addpoint * $resetpoint / 100);
                        $point_total += $point_gtop_extra;
                        $point_gtop_msg = "<br />Guild bạn <strong>TOP ". $guild_extra_point['G_TopPoint'] ." Guild Point</strong>. Nhận thêm hỗ trợ $guild_top4addpoint % = <strong>+ $point_gtop_extra Point</strong>.";
                        $log_arr['guild_topaddpoint'] = $guild_top4addpoint;
                        $log_arr['point_gtop_extra'] = $point_gtop_extra;
                	break;
                    
                    case 5:
                        $point_gtop_extra = floor($guild_top5addpoint * $resetpoint / 100);
                        $point_total += $point_gtop_extra;
                        $point_gtop_msg = "<br />Guild bạn <strong>TOP ". $guild_extra_point['G_TopPoint'] ." Guild Point</strong>. Nhận thêm hỗ trợ $guild_top5addpoint % = <strong>+ $point_gtop_extra Point</strong>.";
                        $log_arr['guild_topaddpoint'] = $guild_top5addpoint;
                        $log_arr['point_gtop_extra'] = $point_gtop_extra;
                	break;
                    
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                        $point_gtop_extra = floor($guild_top6overaddpoint * $resetpoint / 100);
                        $point_total += $point_gtop_extra;
                        $point_gtop_msg = "<br />Guild bạn <strong>TOP ". $guild_extra_point['G_TopPoint'] ." Guild Point</strong>. Nhận thêm hỗ trợ $guild_top6overaddpoint % = <strong>+ $point_gtop_extra Point</strong>.";
                        $log_arr['guild_topaddpoint'] = $guild_top6overaddpoint;
                        $log_arr['point_gtop_extra'] = $point_gtop_extra;
                	break;
                    
                	default :
                        $point_gtop_extra = floor($guild_top10overaddpoint * $resetpoint / 100);
                        $point_total += $point_gtop_extra;
                        $point_gtop_msg = "<br />Guild bạn <strong>không nằm trong TOP 10 Guild Point</strong>. Nhận thêm hỗ trợ $guild_top10overaddpoint % = <strong>+ $point_gtop_extra Point</strong>.";
                        $log_arr['guild_top10overaddpoint'] = $guild_top10overaddpoint;
                        $log_arr['point_gtop_extra'] = $point_gtop_extra;
                }
            }
            
            $point_msg .= $point_gtop_msg;
        }
        $point_msg .= "<br /><strong>Tổng Point cuối cùng nhận được : $point_total Point</strong>";
        $log_arr['point_total_2'] = $point_total;
    } else {
        $point_msg .= "<br />Bạn chưa vào Guild. Không nhận được hỗ trợ khi Reset.";
        $log_arr['is_not_guild'] = 1;
    }
}
// Extra Point From Guild End

kiemtra_doinv($login,$name);
kiemtra_online($login);

// Ghi log nhung nhan vat Reset qua nhanh
$rsnhanh_phut_check = 0;    // So phut cach lan RS truoc.
if($timestamp < $row[7] + $rsnhanh_phut_check*60) {
    $time_chenh = $timestamp - $row[7];
    $phut_chenh = floor($time_chenh/60);
    $giay_chenh = $time_chenh%60;
    $log_Des = "$name Reset lần thứ $resetup quá nhanh. Cách lần Reset trước $phut_chenh phút $giay_chenh giây";
    if($timestamp < $row[7] + 60) {
        $log_Des .= ". Khóa nhân vật.";
        //Block Char
        $block_query = "UPDATE Character SET ctlcode='99',ErrorSubBlock=99 WHERE AccountID = '$login' AND name='$name'";
        $block_result = $db->Execute($block_query);
            check_queryerror($block_query, $block_result);
        
        // Tru 1 lan Reset
        $resetup = $resetup - 1;
    }
    //Ghi vào Log Reset nhanh
        $log_price = " - ";
        _writelog_tiente($login, $log_price, $log_Des);
    //End Ghi vào Log Reset nhanh
}
// End Ghi log nhung nhan vat Reset qua nhanh

// Reset Total
$reset_total = 0;
for($relife_i = 0; $relife_i <= $char_relfe; $relife_i++) {
    $reset_total += $rl_reset_relife[$relife_i];
}
$reset_total += $resetup;
// Reset Total End

    $point_tl = _point_tuluyen($name);
    $Strength = $Strength_Default + $point_tl['str'];
    $Dexterity = $Dexterity_Default + $point_tl['agi'];
    $Vitality = $Vitality_Default + $point_tl['vit'];
    $Energy = $Energy_Default + $point_tl['ene'];
	if( ($point_total > ($nbb_point_default_str + $nbb_point_default_agi + $nbb_point_default_vit + $nbb_point_default_ene)) && ($nbb_point_default_str + $nbb_point_default_agi + $nbb_point_default_vit + $nbb_point_default_ene) > 200) {
        $Strength += $nbb_point_default_str;
        $Dexterity += $nbb_point_default_agi;
        $Vitality += $nbb_point_default_vit;
        $Energy += $nbb_point_default_ene;
        
        $point_total = $point_total - ($nbb_point_default_str + $nbb_point_default_agi + $nbb_point_default_vit + $nbb_point_default_ene);
	}
    $Point_Default_total = $Strength + $Dexterity + $Vitality + $Energy;
    
if( $point_total > 65000) {
    $LevelUpPoint = 65000;
    $point_dutru = $point_total - 65000;
} else {
    $LevelUpPoint = $point_total;
    $point_dutru = 0;
}

    
    
// Reset nhan vat la Darklord
if ($row[4] == 64 OR $row[4] == 65 OR $row[4] == 66)
  {
	$sql_reset_script="Update dbo.character set [clevel]=$clevel,[experience]=0,[money]=$zen_char_after,[LevelUpPoint]=$LevelUpPoint,[pointdutru]=$point_dutru,[resets]=$resetup, ResetNBB=$resetup,[strength]=$Strength,[dexterity]=$Dexterity,[vitality]=$Vitality,[energy]=$Energy,[Life]=$Life,[MaxLife]=$MaxLife,[Mana]=$Mana,[MaxMana]=$MaxMana,[MapNumber]=$MapNumber,[MapPosX]=$MapPosX,[MapPosY]=$MapPosY,[MapDir]=0,[Leadership]=$leadership,[isThuePoint]=0,[NoResetInDay]=$CountNoResetInDay,[Resets_Time]=$timestamp,[ResetVIP]=0,[PointThue]=0, [PkLevel] = 3,[PkTime] = 0,[pkcount] = 0, [reset_total]=$reset_total where AccountID = '$login' AND name='$name'";
	$sql_reset_exec = $db->Execute($sql_reset_script) or die("Lỗi Query: $sql_reset_script");

//Nhan vat dang ki tai sinh
	if ($char_back_reged_check > 0) {
	$msquery = "Update Character_back set [Resets]=$resetup, [LevelUpPoint]=$point_total,[Class]=$row[4],[Leadership]=$leadership,[Relifes]=$char_relfe where AccountID = '$login' AND name='$name'";
	$msresults= $db->Execute($msquery);
	}
}

//Reset nhan vat khong phai la DarkLord
else
 {
	$sql_reset_script="Update Character set [clevel]=$clevel,[experience]=0,[money]=$zen_char_after,[LevelUpPoint]=$LevelUpPoint,[pointdutru]=$point_dutru,[resets]=$resetup, ResetNBB=$resetup,[strength]=$Strength,[dexterity]=$Dexterity,[vitality]=$Vitality,[energy]=$Energy,[Life]=$Life,[MaxLife]=$MaxLife,[Mana]=$Mana,[MaxMana]=$MaxMana,[MapNumber]=$MapNumber,[MapPosX]=$MapPosX,[MapPosY]=$MapPosY,[MapDir]=$Mapdir,[Leadership]=0,[isThuePoint]=0,[NoResetInDay]=$CountNoResetInDay,[Resets_Time]=$timestamp,[ResetVIP]=0,[PointThue]=0, [PkLevel] = 3,[PkTime] = 0,[pkcount] = 0, [reset_total]=$reset_total where AccountID = '$login' AND name='$name'";
	$sql_reset_exec = $db->Execute($sql_reset_script) or die("Lỗi Query: $sql_reset_script");

//All Quest For Class 3
//if ($row[4] == $class_dw_3 OR $row[4] == $class_dk_3 OR $row[4] == $class_elf_3 OR $row[4] == $class_mg_2 OR $row[4] == $class_dl_2 OR $row[4] == $class_sum_3) {
//		$sql_all_quest = $db->Execute($all_quest);
//	}
	
	if ($char_back_reged_check > 0) {
		$msquery = "Update Character_back set [Resets]=$resetup,[LevelUpPoint]='$point_total',[Class]='$row[4]',[Relifes]='$char_relfe' where AccountID = '$login' AND name='$name'";
		$msresults= $db->Execute($msquery);
	}
}

//Reset Point Master Skill
//include_once('MasterLV.php');

$tru_jewel = $db->Execute("UPDATE MEMB_INFO SET jewel_chao=$chao_after,jewel_cre=$cre_after,jewel_blue=$blue_after, bank=$zen_bank_after WHERE memb___id='$login'");

if($Use_TuLuyen == 1 && $tl_point_type == 0) {
    if($CountNoResetInDay <= $tl_rsmax) {
        $tuluyen_point = $CountNoResetInDay;
        $tuluyen_point_update_query = "UPDATE Character SET nbbtuluyen_point = nbbtuluyen_point + $tuluyen_point WHERE Name='$name' AND AccountID='$login'";
        $tuluyen_point_update_result = $db->Execute($tuluyen_point_update_query);
            check_queryerror($tuluyen_point_update_query, $tuluyen_point_update_result);
    }
}

/*
if($Use_SongTu == 1) {
    $songtu_point = $CountNoResetInDay;
    $songtu_point_update_query = "UPDATE Character SET nbbsongtu_point = nbbsongtu_point + $songtu_point WHERE Name='$name' AND AccountID='$login'";
    $songtu_point_update_result = $db->Execute($songtu_point_update_query);
        check_queryerror($songtu_point_update_query, $songtu_point_update_result);
}
*/

if($Use_CuongHoa == 1 && $ch_point_type == 0) {
    if($CountNoResetInDay <= $ch_rsmax) {
	    $cuonghoa_point = $CountNoResetInDay;
	    $cuonghoa_point_update_q = "UPDATE Character SET nbbcuonghoa_point = nbbcuonghoa_point + $cuonghoa_point WHERE Name='$name' AND AccountID='$login'";
	    $cuonghoa_point_update_r = $db->Execute($cuonghoa_point_update_q);
	        check_queryerror($cuonghoa_point_update_q, $cuonghoa_point_update_r);
	    $cuonghoa_msg = "<span class='tag tag-orange'>Nhận : <strong>$cuonghoa_point Điểm Cường Hóa</strong> tương ứng lần Reset thứ $CountNoResetInDay trong ngày.</span>";
    }
}
/*
if($Use_HoanHaoHoa == 1) {
    $hoanhaohoa_point = $CountNoResetInDay;
    $hoanhao_point_update_q = "UPDATE Character SET nbbhoanhao_point = nbbhoanhao_point + $hoanhaohoa_point WHERE Name='$name' AND AccountID='$login'";
    $hoanhao_point_update_r = $db->Execute($hoanhao_point_update_q);
        check_queryerror($hoanhao_point_update_q, $hoanhao_point_update_r);
    $hoanhaohoa_msg = "<hr />Nhận : <strong>$hoanhaohoa_point Điểm Hoàn Hảo Hóa</strong> tương ứng lần Reset thứ $CountNoResetInDay trong ngày.";
}
*/
$danhvong_basic = $clevel_before;

$danhvong_extra_mul = ($danhvong_basic - 300)/50;
if($danhvong_extra_mul < 0) $danhvong_extra_mul = 0;
$danhvong_extra = floor($danhvong_basic*$danhvong_extra_mul);
$danhvong_extra_percent = number_format($danhvong_extra_mul * 100, 0, ',', '.');

$danhvong_point = $danhvong_basic + $danhvong_extra;


$danhvong_msg = "Nhận được : <strong>$danhvong_basic điểm Danh Vọng</strong>.";
$log_arr['danhvong_basic'] = $danhvong_basic;
if($clevel_before >= 300) {
    $danhvong_msg .= "<br /> Reset trên 300LV nhận thêm : <strong>$danhvong_extra điểm Danh Vọng</strong> (tăng $danhvong_extra_percent %).<br />Tổng Điểm Danh Vọng nhận được : <strong>$danhvong_point</strong> Điểm Danh Vọng.";
    $log_arr['danhvong_extra'] = $danhvong_extra;
    $log_arr['danhvong_extra_percent'] = $danhvong_extra_percent;
    $log_arr['danhvong_point'] = $danhvong_point;
} else {
    $danhvong_msg .= "<br />Reset dưới 300LV, không được nhận thêm Điểm Danh Vọng.";
    $log_arr['danhvong_low300'] = 1;
}

if(isset($event_toprs_maxrs) && abs(intval($event_toprs_maxrs)) > 0 && $event_toprs_on == 1 && (strtotime($event_toprs_begin) < $timestamp) && (strtotime($event_toprs_end) + 24*60*60 > $timestamp) ) {
    $event_toprs_maxrs = abs(intval($event_toprs_maxrs));
} else {
    $event_toprs_maxrs = 9999;
}

$rs_data_stop = 1;
if($CountNoResetInDay <= $event_toprs_maxrs) {
	$rs_data_stop = 0;
	_topreset_score($login, $name, $danhvong_point);
}

_topreset($login, $name, 0, 0, 0, $rs_data_stop);

//Event GiftCode Reset
include_once('sv_giftcode_rs.php');

if(file_exists('config/config_sendmess.php')) {
    include_once('config/config_sendmess.php');
    if($Use_SendMess_RS == 1) {
        $thehe_query = "Select thehe From MEMB_INFO where memb___id='$login'";
        $thehe_result = $db->Execute($thehe_query);
            check_queryerror($thehe_query, $thehe_result);
        $thehe_fetch = $thehe_result->fetchrow();
        $thehe = $thehe_fetch[0];
        
        include('config/config_thehe.php');
        $thehe_name = $thehe_choise[$thehe];
        $mess_send = '['. $thehe_name. '] '. $name .' Reset lần thứ '. $resetup;
        
        include_once('config_license.php');
        include_once('func_getContent.php');
        $getcontent_url = $url_license . "/api_sendmess.php";
        $getcontent_data = array(
            'acclic'    =>  $acclic,
            'key'    =>  $key,
            
            'mess_send'    =>  $mess_send
        ); 
        
        $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
    
        $info = read_TagName($reponse, 'info');
        if ($info == "OK") {
            $mess_receive = read_TagName($reponse, 'mess_receive', 0);
            $mess_total = $mess_receive[0];
            
            for($i=1; $i<=$mess_total; $i++) {
                $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
                if ($x = socket_connect($socket, '127.0.0.1', $joinserver_port))
                {
                    socket_write($socket, $mess_receive[$i]);
                } else {
                    socket_close($socket);
                    break;
                }
                socket_close($socket);
            }
        }
    }
}
        $log_arr['name'] = $name;
        $log_arr['rsup'] = $resetup;
        $log_arr['level'] = $row[0];
        $log_arr['rsold'] = $resetold;
        $log_arr['relife'] = $relife;


        $log_price = " - ";
        //$log_Des = "<b>$name</b> Reset lần thứ <b>$resetup</b> khi ". $row[0] ." Level/". $resetold ." Reset, Relife $relife.<br />$point_msg .<br /> $danhvong_msg";
        $log_Des = json_encode($log_arr);
        _writelog_tiente($login, $log_price, $log_Des, 1);
        
//End Ghi vào Log nhung nhan vàt Reset tren 0 lan

    $reponse = "<info>OK</info>
    <zen_char>$zen_char_after</zen_char>
    <zen_bank>$zen_bank_after</zen_bank>
    <chao>$chao_after</chao>
    <cre>$cre_after</cre>
    <blue>$blue_after</blue>
    <resetpoint>$LevelUpPoint</resetpoint>
    <pointdutru>$point_dutru</pointdutru>
    <str>$Strength</str>
    <dex>$Dexterity</dex>
    <vit>$Vitality</vit>
    <ene>$Energy</ene>
    <com>$leadership</com>
    <messenge>$name Reset lần thứ $resetup thành công!<hr />$danhvong_msg";
    
    
    if($Use_TuLuyen == 1) {
        $reponse .= "<hr />";
        if(isset($tl_point_type) && $tl_point_type == 0) {
            $reponse .= "<span class='tag tag-orange'>Nhận : <strong>$tuluyen_point Điểm Tu Luyện</strong> tương ứng lần Reset thứ $CountNoResetInDay trong ngày.</span><br />";
        }
        $reponse .= "Tu Luyện Sức Mạnh : ". $point_tl['str'] ." điểm.<br />Tu Luyện Nhanh Nhẹn : ". $point_tl['agi'] ." điểm.<br />Tu Luyện Thể Lực : ". $point_tl['vit'] ." điểm.<br />Tu Luyện Năng Lượng : ". $point_tl['ene'] ." điểm.<br />";
    }
    
    /*
    if($Use_SongTu == 1) {
        $reponse .= "<hr />Nhận : <strong>$songtu_point Điểm Song Tu</strong> tương ứng lần Reset thứ $CountNoResetInDay trong ngày.";
    }
    */
    if(strlen($cuonghoa_msg)> 0) {
        $reponse .= $cuonghoa_msg;
    }
    /*
    if($Use_HoanHaoHoa == 1) {
        $reponse .= $hoanhaohoa_msg;
    }
    */
    
    $reponse .= "<br />$point_msg.";
    
    $reponse .= "<hr>$messenge_giftcode";
    $reponse .= "<center><a class='btn btn-green btn-block btn-lg' rel='ajax' href='#char_manager&amp;act=addpoint'>Cộng Điểm</a></center>";
    $reponse .= "</messenge>";
    
    echo $reponse;
}

?>