<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
include('config/config_tuluyen.php');
include_once('config/config_chucnang.php');
include_once('config/config_vip_system.php');

$login = $_POST['login'];
$name = $_POST['name'];
$action = $_POST['action'];

$passtransfer = $_POST['passtransfer'];

if ($passtransfer == $transfercode) {

$string_login = $_POST['string_login'];
checklogin($login,$string_login);

if(check_nv($login, $name) == 0) {
    echo "Nhân vật <b>{$name}</b> không nằm trong tài khoản <b>{$login}</b>. Vui lòng kiểm tra lại."; exit();
}
    
    switch ($action) {
	case 'update':
        $tltype = $_POST['tltype'];
        $tl_exp = abs(intval($_POST['tl_exp']));
        
        if($tl_exp <= 0) {
            echo "Chưa chọn điểm Exp muốn Tu Luyện";
            exit();
        }
        
        if($tltype == 'random') {
            $tl_ran = mt_rand(1, 4);
            switch ($tl_ran){ 
            	case 1:
                    $tltype = 'str';
            	break;
            
            	case 2:
                    $tltype = 'agi';
            	break;
            
            	case 3:
                    $tltype = 'vit';
            	break;
            
            	default :
                    $tltype = 'ene';
            }
            
            $money = 0;
        } else {
            $money = $tl_vip_money_price * $tl_exp;
        }
        
        $pp_q = "SELECT nbb_pl, nbb_pl_extra FROM MEMB_INFO WHERE memb___id='$login'";
        $pp_r = $db->Execute($pp_q);
            check_queryerror($pp_q, $pp_r);
        $pp_f = $pp_r->FetchRow();
        $pp_before = $pp_f[0];
        $pp_extra_before = $pp_f[1];
        
        $pp_after = $pp_before;
        $pp_extra_after = $pp_extra_before;
        
        switch ($tltype) { 
        	case 'str':
                $tuluyen_query = "SELECT nbbtuluyen_str, nbbtuluyen_str_point, nbbtuluyen_str_exp, nbbtuluyen_point FROM Character WHERE Name='$name' AND AccountID='$login'";
                $type_name = "Sức mạnh";
        	break;
        
        	case 'agi':
                $tuluyen_query = "SELECT nbbtuluyen_agi, nbbtuluyen_agi_point, nbbtuluyen_agi_exp, nbbtuluyen_point FROM Character WHERE Name='$name' AND AccountID='$login'";
                $type_name = "Nhanh nhẹn";
        	break;
        
        	case 'vit':
                $tuluyen_query = "SELECT nbbtuluyen_vit, nbbtuluyen_vit_point, nbbtuluyen_vit_exp, nbbtuluyen_point FROM Character WHERE Name='$name' AND AccountID='$login'";
                $type_name = "Thể lực";
        	break;
        
        	case 'ene':
                $tuluyen_query = "SELECT nbbtuluyen_ene, nbbtuluyen_ene_point, nbbtuluyen_ene_exp, nbbtuluyen_point FROM Character WHERE Name='$name' AND AccountID='$login'";
                $type_name = "Năng lượng";
        	break;
        
        	default :
                echo "Dữ liệu tu luyện sai."; exit();
        }
        
        $tuluyen_result = $db->Execute($tuluyen_query);
            check_queryerror($tuluyen_query, $tuluyen_result);
        $tuluyen_fetch = $tuluyen_result->FetchRow();
        $tltype_cap = $tuluyen_fetch[0];
        $tltype_point = $tuluyen_fetch[1];
        $tltype_exp = $tuluyen_fetch[2];
        $tl_point_before = $tuluyen_fetch[3];
        $tl_point_after = $tl_point_before;
        
        if($tltype_cap >= $tl_max) {
            echo "Tu Luyện <strong>$type_name</strong> đã đạt cấp tối đa <strong>$tl_max</strong>. Không thể tu luyện tiếp."; exit();
        }
        
        if(!isset($tl_point_type) || $tl_point_type == 0) {        // Su dung diem PP, PP+, Point Tu Luyen
            $tl_point_after = $tl_point_before - $tl_exp;
            if($tl_point_after < 0) {
                $pp_extra_after = $pp_extra_before + $tl_point_after;
                $tl_point_after = 0;
                if($pp_extra_after < 0) {
                    $pp_after = $pp_before + $pp_extra_after;
                    $pp_extra_after = 0;
                }
            }
        } else {    // Chi su dung diem PP & PP+
            $pp_extra_after = $pp_extra_before - $tl_exp;
            if($pp_extra_after < 0) {
                $pp_after = $pp_before + $pp_extra_after;
                $pp_extra_after = 0;
            }
        }
        
        if($pp_after >= 0) {
            
            include_once('config_license.php');
            include_once('func_getContent.php');
            $getcontent_url = $url_license . "/api_tuluyen.php";
            $getcontent_data = array(
                'acclic'    =>  $acclic,
                'key'    =>  $key,
                'action'    =>  'update',
                
                'tl_exp'  =>  $tl_exp,
                'tltype_cap'  =>  $tltype_cap,
                'tltype_exp'  =>  $tltype_exp,
                'tuluyen_expcoban' =>  $tuluyen_expcoban,
                'tuluyen_expextra' =>  $tuluyen_expextra
            ); 
            
            $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
        
        	if ( empty($reponse) ) {
                echo "Server bảo trì vui lòng liên hệ Admin để FIX";
                exit();
            }
            else {
                $info = read_TagName($reponse, 'info');
                if ($info == "OK") {
                    $tuluyen_data = read_TagName($reponse, 'tuluyen');
                    if(strlen($tuluyen_data) == 0) {
                        echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                        
                        $arr_view = "\nDataSend:\n";
                        foreach($getcontent_data as $k => $v) {
                            $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                        }
                        writelog("log_api.txt", $arr_view . $reponse);
                        exit();
                    } else {
                        $tuluyen_arr = json_decode($tuluyen_data, true);
                        $tltype_exp_new = $tuluyen_arr['tltype_exp_new'];
                        $tangcap = $tuluyen_arr['tangcap'];
                        $tl_percent_new = $tuluyen_arr['tl_percent_new'];
                    }
                } else {
                    echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
                    writelog("log_api.txt", $reponse);
                    exit();
                }
            }
            
            $pp_part_update_q = '';
            $pp_extra_update = '';
            if($pp_extra_after != $pp_extra_before) {
                if(strlen($pp_part_update_q) > 0) $pp_part_update_q .= ',';
                $pp_part_update_q = "nbb_pl_extra = $pp_extra_after";
                $pp_extra_update = $pp_extra_after;
            }
            $pp_update = '';
            if($pp_after != $pp_before) {
                if(strlen($pp_part_update_q) > 0) $pp_part_update_q .= ',';
                $pp_part_update_q .= "nbb_pl = $pp_after";
                $pp_update = $pp_after;
            }
            
            if($money > 0) {
                $money_read_q = "SELECT gcoin, gcoin_km, vpoint, vpoint_km FROM MEMB_INFO WHERE memb___id='$login'";
                $money_read_r = $db->Execute($money_read_q);
                    check_queryerror($money_read_q, $money_read_r);
                $money_read_f = $money_read_r->FetchRow();
                
                $gcoin_before = $money_read_f[0];
                $gcoin_km_before = $money_read_f[1];
                $vpoint_before = $money_read_f[2];
                $vpoint_km_before = $money_read_f[3];
                
                $gcoin_after = $money_read_f[0];
                $gcoin_km_after = $money_read_f[1];
                $vpoint_after = $money_read_f[2];
                $vpoint_km_after = $money_read_f[3];
                
                if($tl_vip_money_type == 0) {
                    $vpoint_km_after = $vpoint_km_before - $money;
                    if($vpoint_km_after < 0) {
                        $gcoin_km_after = $gcoin_km_before + $vpoint_km_after;
                        $vpoint_km_after = 0;
                    }
                } else {
                    $gcoin_km_after = $gcoin_km_before - $money;
                }
                
                if($gcoin_km_after < 0) {
                    $vpoint_after = $vpoint_before + $gcoin_km_after;
                    $gcoin_km_after = 0;
                }
                if($vpoint_after < 0) {
                    $gcoin_after = $gcoin_before + $vpoint_after;
                    $vpoint_after = 0;
                }
                
                if($gcoin_after >= 0) {
                    if($vpoint_km_before != $vpoint_km_after) {
                        if(strlen($pp_part_update_q) > 0) $pp_part_update_q .= ',';
                        $pp_part_update_q .= "vpoint_km = $vpoint_km_after";
                        $vpoint_km_update = $vpoint_km_after;
                    }
                    
                    if($gcoin_km_after != $gcoin_km_before) {
                        if(strlen($pp_part_update_q) > 0) $pp_part_update_q .= ',';
                        $pp_part_update_q .= "gcoin_km = $gcoin_km_after";
                        $gcoin_km_update = $gcoin_km_after;
                    }
                    
                    if($vpoint_after != $vpoint_before) {
                        if(strlen($pp_part_update_q) > 0) $pp_part_update_q .= ',';
                        $pp_part_update_q .= "vpoint = $vpoint_after";
                        $vpoint_update = $vpoint_after;
                    }
                    
                    if($gcoin_after != $gcoin_before) {
                        if(strlen($pp_part_update_q) > 0) $pp_part_update_q .= ',';
                        $pp_part_update_q .= "gcoin = $gcoin_after";
                        $gcoin_update = $gcoin_after;
                    }
                        
                    
                } else {
                    echo "Không đủ $money tiền tệ để tiến hành Tu Luyện."; exit();
                }
            }
            
            $tl_point_update = '';
            $tl_point_update_q = '';
            if($tl_point_after != $tl_point_before) {
                $tl_point_update_q = ", nbbtuluyen_point = $tl_point_after";
                $tl_point_update = $tl_point_after;
            }
            
            switch ($tltype){ 
            	case 'str':
                    $tuluyen_update_query = "UPDATE Character SET nbbtuluyen_str_exp = $tltype_exp_new $tl_point_update_q WHERE Name='$name' AND AccountID='$login'";
            	break;
            
            	case 'agi':
                    $tuluyen_update_query = "UPDATE Character SET nbbtuluyen_agi_exp = $tltype_exp_new $tl_point_update_q WHERE Name='$name' AND AccountID='$login'";
            	break;
            
            	case 'vit':
                    $tuluyen_update_query = "UPDATE Character SET nbbtuluyen_vit_exp = $tltype_exp_new $tl_point_update_q WHERE Name='$name' AND AccountID='$login'";
            	break;
            
            	case 'ene':
                    $tuluyen_update_query = "UPDATE Character SET nbbtuluyen_ene_exp = $tltype_exp_new $tl_point_update_q WHERE Name='$name' AND AccountID='$login'";
            	break;
            
            	default :
                    echo "Dữ liệu tu luyện sai."; exit();
            }
            
            $tuluyen_update_result = $db->Execute($tuluyen_update_query);
                check_queryerror($tuluyen_update_query, $tuluyen_update_result);
            
            if(strlen($pp_part_update_q) > 0) {
                $pp_update_q = "UPDATE MEMB_INFO SET $pp_part_update_q WHERE memb___id='$login'";
                $pp_update_r = $db->Execute($pp_update_q);
                    check_queryerror($pp_update_q, $pp_update_r);
            }
            
            $tuluyen_updated_arr = array(
                'exp'   =>  $tltype_exp_new,
                'tlpoint'   =>  $tl_point_update,
                'pp'   =>  $pp_update,
                'pp_extra'   =>  $pp_extra_update,
                'tangcap'   =>  $tangcap,
                'tltype'   =>  $tltype,
                'tl_percent_new'   =>  $tl_percent_new,
                'vpoint_km'   =>  $vpoint_km_update,
                'gcoin_km'   =>  $gcoin_km_update,
                'vpoint'   =>  $vpoint_update,
                'gcoin'   =>  $gcoin_update
            );
            $tuluyen_updated = json_encode($tuluyen_updated_arr);
            
            echo "<info>OK</info><tuluyen_info>$tuluyen_updated</tuluyen_info>";
            
            
            //Ghi vào Log nhung nhan vat gui Jewel vao ngan hang
                $log_price = "";
                $log_Des = "Nhân vật <strong>$name</strong> tu luyện <strong>$type_name</strong> cấp $tltype_cap từ $tltype_exp lên $tltype_exp_new /.";
                _writelog_tiente($login, $log_price, $log_Des);
        	//End Ghi vào Log nhung nhan vat gui Jewel vao ngan hang
        	
        } else {
            if(!isset($tl_point_type) || $tl_point_type == 0) {
                echo "Không đủ $tl_exp điểm PP, PP+, Điểm Tu Luyện. Không thể thực hiện tu luyện"; exit();
            } else {
                echo "Không đủ $tl_exp điểm PP, PP+. Không thể thực hiện tu luyện"; exit();
            }
        }
            
	break;

	case 'thangcap':
        $chao_query = "SELECT jewel_chao, acc_vip, acc_vip_day, acc_vip_time FROM MEMB_INFO WHERE memb___id='$login'";
        $chao_result = $db->Execute($chao_query);
            check_queryerror($chao_query, $chao_result);
        $chao_fetch = $chao_result->FetchRow();
        $chao = $chao_fetch[0];
        $acc_vip = $chao_fetch[1];
        $acc_vip_day = $chao_fetch[2];
        $acc_vip_time = $chao_fetch[3];
        
        if($chao < 1) { // Chao check fail
            echo "Cần ít nhất 1 Chao trong ngân hàng để tiến hành Thăng cấp Tu Luyện."; exit();
        } else {    // Chao check ok
            $tltype = $_POST['tltype'];
            switch ($tltype){ 
            	case 'str':
                    $tuluyen_query = "SELECT nbbtuluyen_str, nbbtuluyen_str_point, nbbtuluyen_str_exp, nbbtuluyen_str_cp FROM Character WHERE Name='$name' AND AccountID='$login'";
                    $type_name = "Sức mạnh";
            	break;
            
            	case 'agi':
                    $tuluyen_query = "SELECT nbbtuluyen_agi, nbbtuluyen_agi_point, nbbtuluyen_agi_exp, nbbtuluyen_agi_cp FROM Character WHERE Name='$name' AND AccountID='$login'";
                    $type_name = "Nhanh nhẹn";
            	break;
            
            	case 'vit':
                    $tuluyen_query = "SELECT nbbtuluyen_vit, nbbtuluyen_vit_point, nbbtuluyen_vit_exp, nbbtuluyen_vit_cp FROM Character WHERE Name='$name' AND AccountID='$login'";
                    $type_name = "Thể lực";
            	break;
            
            	case 'ene':
                    $tuluyen_query = "SELECT nbbtuluyen_ene, nbbtuluyen_ene_point, nbbtuluyen_ene_exp, nbbtuluyen_ene_cp FROM Character WHERE Name='$name' AND AccountID='$login'";
                    $type_name = "Năng lượng";
            	break;
            
            	default :
                    echo "Dữ liệu tu luyện sai."; exit();
            }
            
            $tuluyen_result = $db->Execute($tuluyen_query);
                check_queryerror($tuluyen_query, $tuluyen_result);
            $tuluyen_fetch = $tuluyen_result->FetchRow();
            $tltype_cap = $tuluyen_fetch[0];
            $tltype_point = $tuluyen_fetch[1];
            $tltype_exp = $tuluyen_fetch[2];
            $tltype_cp = $tuluyen_fetch[3];
       
            include_once('config_license.php');
            include_once('func_getContent.php');
            $getcontent_url = $url_license . "/api_tuluyen.php";
            $getcontent_data = array(
                'acclic'    =>  $acclic,
                'key'    =>  $key,
                'action'    =>  'thangcap',
                
                'tltype_cap'  =>  $tltype_cap,
                'tuluyen_expcoban' =>  $tuluyen_expcoban,
                'tuluyen_expextra' =>  $tuluyen_expextra,
                'tuluyen_cpcoban' =>  $tuluyen_cpcoban,
                'tuluyen_cpextra' =>  $tuluyen_cpextra,
                'tuluyen_pointcoban'   =>  $tuluyen_pointcoban,
                'tuluyen_pointextra'   =>  $tuluyen_pointextra,
                'tltype_cp'    =>  $tltype_cp,
                'tltype_exp'   =>  $tltype_exp,
                'tl_cp_min'   =>  $tl_cp_min,
                'tl_cp_max'   =>  $tl_cp_max
            ); 
            
            $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
        
        	if ( empty($reponse) ) {
                echo "Server bảo trì vui lòng liên hệ Admin để FIX";
                exit();
            }
            else {
                $info = read_TagName($reponse, 'info');
                if ($info == "OK") {
                    $tuluyen_data = read_TagName($reponse, 'tuluyen');
                    if(strlen($tuluyen_data) == 0) {
                        echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                        
                        $arr_view = "\nDataSend:\n";
                        foreach($getcontent_data as $k => $v) {
                            $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                        }
                        writelog("log_api.txt", $arr_view . $reponse);
                        exit();
                    } else {
                        $tuluyen_arr = json_decode($tuluyen_data, true);
                        $exp_tuluyen_sum = $tuluyen_arr['exp_tuluyen_sum'];
                        $cp_rand = $tuluyen_arr['cp_rand'];
                        $tltype_cp_new = $tuluyen_arr['tltype_cp_new'];
                        $cp_sum = $tuluyen_arr['cp_sum'];
                        $tltype_cp_percent = $tuluyen_arr['tltype_cp_percent'];
                        $point_new = $tuluyen_arr['point_new'];
                        $point_next = $tuluyen_arr['point_next'];
                        $exp_tuluyen_next = $tuluyen_arr['exp_tuluyen_next'];
                    }
                } else {
                    echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
                    writelog("log_api.txt", $reponse);
                    exit();
                }
            }
            //VIP2
				if($Use_VIP == 2 && $acc_vip > 0 && $acc_vip_day >= $enable_vip['tuluyen'] && $acc_vip_time > $timestamp) {
					if($acc_vip == 1) {
						$extra_exp_tuluyen = $gold_vip['exp_tuluyen'];
						$exp_tuluyen_sum = $exp_tuluyen_sum - $gold_vip['exp_tuluyen'];
					}
					elseif($acc_vip == 2) {						
						$extra_exp_tuluyen = $silver_vip['exp_tuluyen'] ;
						$exp_tuluyen_sum = $exp_tuluyen_sum - $silver_vip['exp_tuluyen'];		
					}   
				}
			//End VIP2
            if( $tltype_exp < $exp_tuluyen_sum ) {   // Kg tang cap
                echo "Chưa đủ Điểm Tu Luyện thực hiện Thăng Cấp|$tltype_exp|$exp_tuluyen_sum";
            } else {
                $chao_new = $chao - 1;
                $chao_update_query = "UPDATE MEMB_INFO SET jewel_chao = $chao_new WHERE memb___id='$login'";
                $chao_update_result = $db->Execute($chao_update_query);
                    check_queryerror($chao_update_query, $chao_update_result);
                    
                
                if($tltype_cp_new < $cp_sum) { // Thang cap that bai
                    switch ($tltype) { 
                    	case 'str':
                            $thangcap_update_query = "UPDATE Character SET nbbtuluyen_str_cp = $tltype_cp_new WHERE Name='$name' AND AccountID='$login'";
                    	break;
                    
                    	case 'agi':
                            $thangcap_update_query = "UPDATE Character SET nbbtuluyen_agi_cp = $tltype_cp_new WHERE Name='$name' AND AccountID='$login'";
                    	break;
                    
                    	case 'vit':
                            $thangcap_update_query = "UPDATE Character SET nbbtuluyen_vit_cp = $tltype_cp_new WHERE Name='$name' AND AccountID='$login'";
                    	break;
                    
                    	case 'ene':
                            $thangcap_update_query = "UPDATE Character SET nbbtuluyen_ene_cp = $tltype_cp_new WHERE Name='$name' AND AccountID='$login'";
                    	break;
                    
                    	default :
                            echo "Dữ liệu tu luyện sai."; exit();
                    }
                    
                    $thangcap_update_result = $db->Execute($thangcap_update_query);
                        check_queryerror($thangcap_update_query, $thangcap_update_result);
                        
                    $tangcap = 0;
                    $thangcap_updated_arr = array(
                        'tlcp_rand'  =>  $cp_rand,
                        'tlcp'   =>  $tltype_cp_percent,
                        'chao'  =>  $chao_new,
                        'tangcap'   =>  $tangcap
                    );
                    $tltype_cap_new = $tltype_cap + 1;
                    
                    $log_Des = "Nhân vật <strong>$name</strong> thăng cấp <strong>$type_name</strong> từ cấp <strong>$tltype_cap</strong> lên ". $tltype_cap_new ." thất bại. Chúc phúc đạt <strong>$tltype_cp_percent %</strong>.";
                } else { // Thang cap thanh cong
                    $tltype_cap_new = $tltype_cap + 1;
                    switch ($tltype) { 
                    	case 'str':
                            $thangcap_update_query = "UPDATE Character SET nbbtuluyen_str = $tltype_cap_new, nbbtuluyen_str_cp = 0, nbbtuluyen_str_point = $point_new WHERE Name='$name' AND AccountID='$login'";
                    	break;
                    
                    	case 'agi':
                            $thangcap_update_query = "UPDATE Character SET nbbtuluyen_agi = $tltype_cap_new, nbbtuluyen_agi_cp = 0, nbbtuluyen_agi_point = $point_new WHERE Name='$name' AND AccountID='$login'";
                    	break;
                    
                    	case 'vit':
                            $thangcap_update_query = "UPDATE Character SET nbbtuluyen_vit = $tltype_cap_new, nbbtuluyen_vit_cp = 0, nbbtuluyen_vit_point = $point_new WHERE Name='$name' AND AccountID='$login'";
                    	break;
                    
                    	case 'ene':
                            $thangcap_update_query = "UPDATE Character SET nbbtuluyen_ene = $tltype_cap_new, nbbtuluyen_ene_cp = 0, nbbtuluyen_ene_point = $point_new WHERE Name='$name' AND AccountID='$login'";
                    	break;
                    
                    	default :
                            echo "Dữ liệu tu luyện sai."; exit();
                    }
                    
                    $thangcap_update_result = $db->Execute($thangcap_update_query);
                        check_queryerror($thangcap_update_query, $thangcap_update_result);
                        
                    $tangcap = 1;
                    
                    $thangcap_updated_arr = array(
                        'tl_cap'   =>  $tltype_cap_new,
                        'tl_point'   =>  $point_new,
                        'tl_point_next'   =>  $point_next,
                        'tl_exp'   =>  $tltype_exp,
                        'tl_exp_next'   =>  $exp_tuluyen_next,
                        'tangcap'   =>  $tangcap,
                        'tl_vip_money_price'  =>  $tl_vip_money_price*5
                    );
                    
                    $log_Des = "Nhân vật <strong>$name</strong> thăng cấp <strong>$type_name</strong> từ cấp <strong>$tltype_cap</strong> lên <strong>". $tltype_cap + 1 ."</strong> thành công.";
                }
                
                
                $thangcap_updated = json_encode($thangcap_updated_arr);
                
                echo "<info>OK</info><tuluyen_info>$thangcap_updated</tuluyen_info>";
                
                //Ghi vào Log nhung nhan vat gui Jewel vao ngan hang
                    $log_price = "";
                    _writelog_tiente($login, $log_price, $log_Des);
            	//End Ghi vào Log nhung nhan vat gui Jewel vao ngan hang
            }
        } // End Chao check
            
	break;
    
    default :
        $tuluyen_query = "SELECT nbbtuluyen_str, nbbtuluyen_str_point, nbbtuluyen_str_exp, nbbtuluyen_str_cp, nbbtuluyen_agi, nbbtuluyen_agi_point, nbbtuluyen_agi_exp, nbbtuluyen_agi_cp, nbbtuluyen_vit, nbbtuluyen_vit_point, nbbtuluyen_vit_exp, nbbtuluyen_vit_cp, nbbtuluyen_ene, nbbtuluyen_ene_point, nbbtuluyen_ene_exp, nbbtuluyen_ene_cp, nbbtuluyen_point FROM Character WHERE Name='$name' AND AccountID='$login'";
        $tuluyen_result = $db->Execute($tuluyen_query);
            check_queryerror($tuluyen_query, $tuluyen_result);
        $tuluyen_fetch = $tuluyen_result->FetchRow();
        
        include_once('config_license.php');
        include_once('func_getContent.php');
        $getcontent_url = $url_license . "/api_tuluyen.php";
        $getcontent_data = array(
            'acclic'    =>  $acclic,
            'key'    =>  $key,
            'action'    =>  'tuluyen_list',
            
            'tuluyen_fetch'    =>  json_encode($tuluyen_fetch),
            'tuluyen_cpcoban'  =>  $tuluyen_cpcoban,
            'tuluyen_cpextra'  =>  $tuluyen_cpextra
        ); 
        
        $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
    
    	if ( empty($reponse) ) {
            echo "Server bảo trì vui lòng liên hệ Admin để FIX";
            exit();
        }
        else {
            $info = read_TagName($reponse, 'info');
            if ($info == "OK") {
                $tuluyen_data = read_TagName($reponse, 'tuluyen');
                if(strlen($tuluyen_data) == 0) {
                    echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                    
                    $arr_view = "\nDataSend:\n";
                    foreach($getcontent_data as $k => $v) {
                        $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                    }
                    writelog("log_api.txt", $arr_view . $reponse);
                    exit();
                }
            } else {
                echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
                writelog("log_api.txt", $reponse);
                exit();
            }
        }

        echo "<nbb>OK<nbb>$tuluyen_data<nbb>";
}

	
	

}

?>