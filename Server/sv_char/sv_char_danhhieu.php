<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
$file_edit = 'config/config_title.txt';
$fopen_host = fopen($file_edit, "r");
$title_data = fgets($fopen_host);
fclose($fopen_host);
$title_arr = json_decode($title_data, true);
if(!is_array($title_arr)) $title_arr = array();

$login = $_POST['login'];
$name = $_POST['name'];
$action = $_POST['action'];

$passtransfer = $_POST['passtransfer'];

if ($passtransfer == $transfercode) {

    $string_login = $_POST['string_login'];
    checklogin($login,$string_login);
    
    if(check_nv($login, $name) == 0) {
        echo "Nhân vật <b>{$name}</b> không nằm trong tài khoản <b>{$login}</b>. Vui lòng kiểm tra lại."; exit();
    }
    
    switch ($action) {
        default :
            $danhhieu_type = $_POST['danhhieu_type'];
            $danhhieu_id = $_POST['danhhieu_id'];
            if(!isset($title_arr[$danhhieu_type][$danhhieu_id])) {
                echo "Danh Hiệu không tồn tại"; exit();
            } else {
                if($danhhieu_type == 'rs') {
                    $rs_yeucau = $title_arr[$danhhieu_type][$danhhieu_id][0];
                    $danhhieu_index = $title_arr[$danhhieu_type][$danhhieu_id][1];
                    switch ($title_arr[$danhhieu_type][$danhhieu_id][5]){ 
                    	case 1:
                            $danhhieu_data = "RankTitle1";
                    	break;
                    
                    	case 2:
                            $danhhieu_data = "RankTitle2";
                    	break;
                    
                    	case 3:
                            $danhhieu_data = "RankTitle3";
                    	break;
                    
                    	default :
                            $danhhieu_data = "danhhieu";
                    }
                    
                    $rs_q = "SELECT Resets, Relifes, reset_total, ". $danhhieu_data ." FROM Character WHERE AccountID='$login' AND Name='$name'";
                    $rs_r = $db->Execute($rs_q);
                        check_queryerror($rs_q, $rs_r);
                    $rs_f = $rs_r->FetchRow();
                    
                    if($rs_f[1] == 0 && $rs_f[0] > $rs_f[2]) $rs_total = $rs_f[0];
                    else $rs_total = $rs_f[2];
                    
                    if($danhhieu_index == $rs_f[3]) {
                        echo "Danh hiệu này hiện đang sử dụng. Không thể chọn lại."; exit();
                    }
                    else if($rs_total < $rs_yeucau) {
                        echo "Bạn chỉ có $rs_total Reset tổng, nhỏ hơn yêu cầu : $rs_yeucau Reset. <br />Không được chọn danh hiệu này."; exit();
                    } else {
                        $danhhieu_update_q = "UPDATE Character SET ". $danhhieu_data ."=$danhhieu_index WHERE AccountID='$login' AND Name='$name'";
                        $danhhieu_update_r = $db->Execute($danhhieu_update_q);
                            check_queryerror($danhhieu_update_q,$danhhieu_update_r);
                        
                        echo "<info>OK</info><danhhieu_index>$danhhieu_index</danhhieu_index>";
                    }
                    
                } elseif($danhhieu_type == 'card') {
                    $card_yeucau = $title_arr[$danhhieu_type][$danhhieu_id][0];
                    $danhhieu_index = $title_arr[$danhhieu_type][$danhhieu_id][1];
                    switch ($title_arr[$danhhieu_type][$danhhieu_id][5]){ 
                    	case 1:
                            $danhhieu_data = "RankTitle1";
                    	break;
                    
                    	case 2:
                            $danhhieu_data = "RankTitle2";
                    	break;
                    
                    	case 3:
                            $danhhieu_data = "RankTitle3";
                    	break;
                    
                    	default :
                            $danhhieu_data = "danhhieu";
                    }
                    
                    $card_total_q = "SELECT SUM(menhgia) FROM CardPhone WHERE acc='$login' AND status=2";
                    $card_total_r = $db->Execute($card_total_q);
                        check_queryerror($card_total_q, $card_total_r);
                    $card_total_f = $card_total_r->FetchRow();
                    $card_total = abs(intval($card_total_f[0]));
                    
                    $danhhieu_current_q = "SELECT ". $danhhieu_data ." FROM Character WHERE AccountID='$login' AND Name='$name'";
                    $danhhieu_current_r = $db->Execute($danhhieu_current_q);
                        check_queryerror($danhhieu_current_q, $danhhieu_current_r);
                    $danhhieu_current_f = $danhhieu_current_r->FetchRow();
                    
                    if($danhhieu_index == $danhhieu_current_f[0]) {
                        echo "Danh hiệu này hiện đang sử dụng. Không thể chọn lại."; exit();
                    }
                    else if($card_total < $card_yeucau) {
                        echo "Bạn mới chỉ nạp $card_total VNĐ, nhỏ hơn yêu cầu : $card_yeucau VNĐ. <br />Không được chọn danh hiệu này."; exit();
                    } else {
                        $danhhieu_update_q = "UPDATE Character SET ". $danhhieu_data ."=$danhhieu_index WHERE AccountID='$login' AND Name='$name'";
                        $danhhieu_update_r = $db->Execute($danhhieu_update_q);
                            check_queryerror($danhhieu_update_q,$danhhieu_update_r);
                        
                        echo "<info>OK</info><danhhieu_index>$danhhieu_index</danhhieu_index>";
                    }
                    
                } elseif($danhhieu_type == 'vip2') {
                    $vip_yeucau = $title_arr[$danhhieu_type][$danhhieu_id][0];
                    $danhhieu_index = $title_arr[$danhhieu_type][$danhhieu_id][1];
                    switch ($title_arr[$danhhieu_type][$danhhieu_id][5]){ 
                    	case 1:
                            $danhhieu_data = "RankTitle1";
                    	break;
                    
                    	case 2:
                            $danhhieu_data = "RankTitle2";
                    	break;
                    
                    	case 3:
                            $danhhieu_data = "RankTitle3";
                    	break;
                    
                    	default :
                            $danhhieu_data = "danhhieu";
                    }
                    
                    $acc_vip_q = "SELECT acc_vip FROM MEMB_INFO WHERE memb___id='$login' AND acc_vip_time>=$timestamp";
                    $acc_vip_r = $db->Execute($acc_vip_q);
                        check_queryerror($acc_vip_q, $acc_vip_r);
                    $acc_vip_f = $acc_vip_r->FetchRow();
                    $acc_vip = abs(intval($acc_vip_f[0]));
					if($acc_vip == 1) $vip = "VIP Vàng";
                    elseif($acc_vip == 2) $vip = "VIP Bạc";

                    $danhhieu_current_q = "SELECT ". $danhhieu_data ." FROM Character WHERE AccountID='$login' AND Name='$name'";
                    $danhhieu_current_r = $db->Execute($danhhieu_current_q);
                        check_queryerror($danhhieu_current_q, $danhhieu_current_r);
                    $danhhieu_current_f = $danhhieu_current_r->FetchRow();
                    
                    if($danhhieu_index == $danhhieu_current_f[0]) {
                        echo "Danh hiệu này hiện đang sử dụng. Không thể chọn lại."; exit();
                    }
                    else if($acc_vip < $vip_yeucau) {
                        echo "Bạn chưa đăng kí gói Hỗ trợ $vip. <br />Không được chọn danh hiệu này."; exit();
                    } else {
                        $danhhieu_update_q = "UPDATE Character SET ". $danhhieu_data ."=$danhhieu_index WHERE AccountID='$login' AND Name='$name'";
                        $danhhieu_update_r = $db->Execute($danhhieu_update_q);
                            check_queryerror($danhhieu_update_q,$danhhieu_update_r);
                        
                        echo "<info>OK</info><danhhieu_index>$danhhieu_index</danhhieu_index>";
                    }
                    
                } else {
                    echo "Loại danh hiệu chưa định nghĩa"; exit();
                }
            }
    }


}

?>