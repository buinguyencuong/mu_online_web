<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
include('config/config_questdaily.php');
include_once('config/config_chucnang.php');
include_once('config/config_vip_system.php');

$login = $_POST['login'];
$name = $_POST['name'];
$action = $_POST['action'];

$passtransfer = $_POST['passtransfer'];

if ($passtransfer == $transfercode) {

$string_login = $_POST['string_login'];
checklogin($login,$string_login);

if(check_nv($login, $name) == 0) {
    echo "Nhân vật <b>{$name}</b> không nằm trong tài khoản <b>{$login}</b>. Vui lòng kiểm tra lại."; exit();
}

$datenow = date('Y-m-d', $timestamp);
$timenow_s = date('Y-m-d H:i:s', $timestamp);

    switch ($action) {
        case 'plchange':
            $pl_point_to = $_POST['pl_point_to'];
            $pl_point_change = abs(intval($_POST['pl_point_change']));
            
            $plpoint_q = "SELECT nbb_pl FROM MEMB_INFO WHERE memb___id='$login'";
            $plpoint_r = $db->Execute($plpoint_q);
                check_queryerror($plpoint_q, $plpoint_r);
            $plpoint_f = $plpoint_r->FetchRow();
            $plpoint = $plpoint_f[0];
            
            if($pl_point_change == 0) {
                echo "Điểm Phúc Lợi muốn đổi phải lớn hơn 0.";
            } elseif($pl_point_change > $plpoint) {
                echo "Điểm Phúc Lợi muốn đổi <strong>$pl_point_change</strong> lớn hơn Điểm Phúc Lợi hiện có <strong>$plpoint</strong>.<br />Không thể đổi.<br /> Vui lòng chọn giá trị nhỏ hơn hoặc bằng <strong>$plpoint</strong>";
            } else {
                $plpoint_new = $plpoint - $pl_point_change;
                $plupdate_q = "UPDATE MEMB_INFO SET nbb_pl = $plpoint_new WHERE memb___id='$login'";
                $plupdate_r = $db->Execute($plupdate_q);
                    check_queryerror($plupdate_q, $plupdate_r);
                
                switch ($pl_point_to){ 
                	case 'tl':     // Đổi sang điểm Tu Luyện
                        $tlupdate_q = "UPDATE Character SET nbbtuluyen_point = nbbtuluyen_point + $pl_point_change WHERE AccountID='$login' AND Name='$name'";
                        $tlupdate_r = $db->Execute($tlupdate_q);
                            check_queryerror($tlupdate_q, $tlupdate_r);
                        
                        $msg = "Đã đổi <strong>$pl_point_change Điểm Phúc Lợi</strong> sang <strong>Điểm Tu Luyện</strong>";
                        echo "<info>OK</info><pl>$plpoint_new</pl><msg>$msg</msg>";
                	break;
                    
                    case 'st':      // Đổi sang điểm Song Tu
                            
                        $stupdate_q = "UPDATE Character SET nbbsongtu_point = nbbsongtu_point + $pl_point_change WHERE AccountID='$login' AND Name='$name'";
                        $stupdate_r = $db->Execute($stupdate_q);
                            check_queryerror($stupdate_q, $stupdate_r);
                            
                        $msg = "Đã đổi <strong>$pl_point_change Điểm Phúc Lợi</strong> sang <strong>Điểm Song Tu</strong>";
                        echo "<info>OK</info><pl>$plpoint_new</pl><msg>$msg</msg>";
                    break;
                    
                    case 'ch':      // Đổi sang điểm Cường Hóa
                        
                        $chupdate_q = "UPDATE Character SET nbbcuonghoa_point = nbbcuonghoa_point + $pl_point_change WHERE AccountID='$login' AND Name='$name'";
                        $chupdate_r = $db->Execute($chupdate_q);
                            check_queryerror($chupdate_q, $chupdate_r);
                            
                        $msg = "Đã đổi <strong>$pl_point_change Điểm Phúc Lợi</strong> sang <strong>Điểm Cường Hóa</strong>";
                        echo "<info>OK</info><pl>$plpoint_new</pl><msg>$msg</msg>";
                    break;
                    
                    case 'hh':      // Đổi sang điểm Cường Hóa
                        $hhupdate_q = "UPDATE Character SET nbbhoanhao_point = nbbhoanhao_point + $pl_point_change WHERE AccountID='$login' AND Name='$name'";
                        $hhupdate_r = $db->Execute($hhupdate_q);
                            check_queryerror($hhupdate_q, $hhupdate_r);
                            
                        $msg = "Đã đổi <strong>$pl_point_change Điểm Phúc Lợi</strong> sang <strong>Điểm Hoàn Hảo Hóa</strong>";
                        echo "<info>OK</info><pl>$plpoint_new</pl><msg>$msg</msg>";
                    break;
                    
                	default :
                        echo "Dữ liệu đổi sang không đúng.";
                }
            }
                    
                
        break;
    
        case 'nhanthuong':
            $qindex = abs(intval($_POST['qindex']));
            
            $quest_danhan_q = "SELECT count(qindex) FROM nbb_quest_daily WHERE acc='$login' AND name='$name' AND DATEADD(day, DATEDIFF(day, 0, date), 0)='$datenow'";
            $quest_danhan_r = $db->Execute($quest_danhan_q);
                check_queryerror($quest_danhan_q, $quest_danhan_r);
            $quest_danhan_f = $quest_danhan_r->FetchRow();
            $quest_danhan = $quest_danhan_f[0];
			$vip_check = $db->Execute("Select acc_vip, acc_vip_day, acc_vip_time From MEMB_INFO where memb___id='$login'");
			$vip = $vip_check->fetchrow();
			$acc_vip = $vip[2];
			$acc_vip_day = $vip[3];
			$acc_vip_time = $vip[4];			
			//VIP2
			if($Use_VIP == 2 && $acc_vip > 0 && $acc_vip_day >= $enable_vip['questdaily'] && $acc_vip_time > $timestamp) {
				if($acc_vip == 1) {
					$extra_questdaily_slg = $gold_vip['questdaily'];
					$quest_daily_slgmax = $quest_daily_slgmax + $gold_vip['questdaily'];
				}
				elseif($acc_vip == 2) {
					$extra_questdaily_slg = $silver_vip['questdaily'] ;
					$quest_daily_slgmax = $quest_daily_slgmax + $silver_vip['questdaily'];		
				}   
			}
			//End VIP2            
            if($quest_danhan >= $quest_daily_slgmax) {
                echo "Hôm nay bạn đã nhận đủ $quest_danhan phần thưởng nhiệm vụ.<br /> Hẹn gặp bạn vào ngày mai.";
            } else {
                if($qindex > 0 AND $qindex <=49) {
                    $qindex_q = "SELECT count(qindex) FROM nbb_quest_daily WHERE acc='$login' AND name='$name' AND qindex=$qindex AND DATEADD(day, DATEDIFF(day, 0, date), 0)='$datenow'"; 
                    $qindex_r = $db->Execute($qindex_q);
                        check_queryerror($qindex_q, $qindex_r);
                    $qindex_f = $qindex_r->FetchRow();
                    if($qindex_f[0] >0) {
                        echo "Nhiệm vụ này đã nhận thưởng. Không thể nhận tiếp.";
                    } else {
                        $quest_arr = _quest_daily($login, $name);
                        $qwait = $quest_arr['quest_wait'];
                        $qfinish = $quest_arr[$qindex];
                        
                        if($qfinish == 1) { // hoan thanh nhiem vu
                            
                            $phanthuong_gcoin = $quest_daily_gcoin[$qindex];
                            $phanthuong_gcoinkm = $quest_daily_gcoinkm[$qindex];
                            
                            $phanthuong_vpoint = $quest_daily_vpoint[$qindex];
                            $phanthuong_vpoint_km = $quest_daily_vpoint_km[$qindex];
                            
                            $phanthuong_pl = $quest_daily_pl[$qindex];
                            $phanthuong_pl_extra = $quest_daily_pl_extra[$qindex];
                            
                            $phanthuong_pcpoint = $quest_daily_pcpoint[$qindex];
                            $phanthuong_wcoin = $quest_daily_wcoin[$qindex];
                            
                            $phanthuong_chao = $quest_daily_chao[$qindex];
                            $phanthuong_cre = $quest_daily_cre[$qindex];
                            $phanthuong_blue = $quest_daily_blue[$qindex];
                            
                            $phanthuong_pmaster = $quest_daily_pmaster[$qindex];
                            
                            if(isset($quest_daily_exp[$qindex])) $phanthuong_exp = $quest_daily_exp[$qindex];
                                else $phanthuong_exp = 0;
                            if(isset($quest_daily_giftcode[$qindex])) $phanthuong_giftcode = $quest_daily_giftcode[$qindex];
                                else $phanthuong_giftcode = 999999;
                            
                            $phanthuong_item_reset = $quest_daily_item_reset[$qindex];
                            $phanthuong_item_time = $quest_daily_item_time[$qindex];
                            $phanthuong_item_time_info = $quest_daily_item_time_info[$qindex];
                            $phanthuong_item_time_day = $quest_daily_item_time_day[$qindex];
                            $phanthuong_item = $quest_daily_item[$qindex];
                            $phanthuong_item_info = $quest_daily_item_info[$qindex];
                            
                            $notice = "";
                            $log_Des_phanthuong = "";
                            $phanthuong_up = "";
                            $online_check = 0;
                            
                            if($phanthuong_gcoin > 0) {
                                if(strlen($log_Des_phanthuong) > 0) {$log_Des_phanthuong .= " + "; $phanthuong_up .= ",";}
                                $log_Des_phanthuong .= "$phanthuong_gcoin GCent";
                                 $notice .=  "<div class='item-box' title='GCent'><img src='templates/images/icons/g.png' /><span>x". $phanthuong_gcoin ."</span></div>";
                                $phanthuong_up .= "gcoin=gcoin+$phanthuong_gcoin";
                            }
                            if($phanthuong_gcoinkm > 0) {
                                if(strlen($log_Des_phanthuong) > 0) {$log_Des_phanthuong .= " + "; $phanthuong_up .= ",";}
                                $log_Des_phanthuong .= "$phanthuong_gcoinkm Gcent+";
                                $notice .=  "<div class='item-box' title='GCent khuyến mại'><img src='templates/images/icons/g+.png' /><span>x". $phanthuong_gcoinkm ."</span></div>";
                                $phanthuong_up .= "gcoin_km=gcoin_km+$phanthuong_gcoinkm";
                            }
                            
                            if($phanthuong_vpoint > 0) {
                                if(strlen($log_Des_phanthuong) > 0) {$log_Des_phanthuong .= " + "; $phanthuong_up .= ",";}
                                $log_Des_phanthuong .= "$phanthuong_vpoint Vcent";
                                $notice .=  "<div class='item-box' title='Vcent'><img src='templates/images/icons/v.png' /><span>x". $phanthuong_vpoint ."</span></div>";
                                $phanthuong_up .= "vpoint=vpoint+$phanthuong_vpoint";
                            }
                            if($phanthuong_vpoint_km > 0) {
                                if(strlen($log_Des_phanthuong) > 0) {$log_Des_phanthuong .= " + "; $phanthuong_up .= ",";}
                                $log_Des_phanthuong .= "$phanthuong_vpoint_km Vcent+";
                                $notice .=  "<div class='item-box' title='Vcent Event'><img src='templates/images/icons/v+.png' /><span>x". $phanthuong_vpoint_km ."</span></div>";
                                $phanthuong_up .= "vpoint_km=vpoint_km+$phanthuong_vpoint_km";
                            }
                            
                            if($phanthuong_pl > 0) {
                                if(strlen($log_Des_phanthuong) > 0) {$log_Des_phanthuong .= " + "; $phanthuong_up .= ",";}
                                $log_Des_phanthuong .= "$phanthuong_pl PPoint";
                                $notice .=  "<div class='item-box' title='PPoint'><img src='templates/images/icons/p.png' /><span>x". $phanthuong_pl ."</span></div>";
                                $phanthuong_up .= "nbb_pl=nbb_pl+$phanthuong_pl";
                            }
                            if($phanthuong_pl_extra > 0) {
                                if(strlen($log_Des_phanthuong) > 0) {$log_Des_phanthuong .= " + "; $phanthuong_up .= ",";}
                                $log_Des_phanthuong .= "$phanthuong_pl_extra PPoint+";
                                $notice .=  "<div class='item-box' title='PPoint+'><img src='templates/images/icons/p+.png' /><span>x". $phanthuong_pl_extra ."</span></div>";
                                $phanthuong_up .= "nbb_pl_extra=nbb_pl_extra+$phanthuong_pl_extra";
                            }
                            
                            if($phanthuong_wcoin > 0) {
                                if($online_check == 0) {
                                    kiemtra_online($login);
                                    $online_check = 1;
                                }
                                
                                if(strlen($log_Des_phanthuong) > 0) {$log_Des_phanthuong .= " + "; $phanthuong_up .= ",";}
                                $log_Des_phanthuong .= "$phanthuong_wcoin WCoin";
                                $notice .=  "<div class='item-box' title='WCoin'><img src='templates/images/icons/w.png' /><span>x". $phanthuong_wcoin ."</span></div>";
                                $phanthuong_up .= "WCoin=WCoin+$phanthuong_wcoin";
                            }
                            
                            if($phanthuong_chao > 0) {
                                if(strlen($log_Des_phanthuong) > 0) {$log_Des_phanthuong .= " + "; $phanthuong_up .= ",";}
                                $log_Des_phanthuong .= "$phanthuong_chao Chao";
                                $notice .=  "<div class='item-box' title='Ngọc Hỗn Nguyên ngân hàng'><img src='templates/images/icons/items/chaos.png' /><span>x". $phanthuong_chao ."</span></div>";
                                $phanthuong_up .= "jewel_chao=jewel_chao+$phanthuong_chao";
                            }
                            
                            if($phanthuong_cre > 0) {
                                if(strlen($log_Des_phanthuong) > 0) {$log_Des_phanthuong .= " + "; $phanthuong_up .= ",";}
                                $log_Des_phanthuong .= "$phanthuong_cre Cre";
                                $notice .=  "<div class='item-box' title='Ngọc Sáng Tạo ngân hàng'><img src='templates/images/icons/items/creation.png' /><span>x". $phanthuong_cre ."</span></div>";
                                $phanthuong_up .= "jewel_cre=jewel_cre+$phanthuong_cre";
                            }
                            
                            if($phanthuong_blue > 0) {
                                if(strlen($log_Des_phanthuong) > 0) {$log_Des_phanthuong .= " + "; $phanthuong_up .= ",";}
                                $log_Des_phanthuong .= "$phanthuong_blue Lông Vũ";
                                $notice .=  "<div class='item-box' title='Lông Vũ ngân hàng'><img src='templates/images/icons/items/blue.png' /><span>x". $phanthuong_blue ."</span></div>";
                                $phanthuong_up .= "jewel_blue=jewel_blue+$phanthuong_blue";
                            }
                            
                            $char_phanthuong_up = '';
                            if($phanthuong_pcpoint > 0) {
                                if($online_check == 0) {
                                    kiemtra_online($login);
                                    $online_check = 1;
                                }
                                
                                if(strlen($log_Des_phanthuong) > 0) $log_Des_phanthuong .= " + ";
                                $log_Des_phanthuong .= "$phanthuong_pcpoint PCPoint";
                                $notice .=  "<div class='item-box' title='PCPoint'><img src='templates/images/icons/pc-point.png' /><span>x". $phanthuong_pcpoint ."</span></div>";
                                
                                if(strlen($char_phanthuong_up) > 0) $char_phanthuong_up .= ",";
                                $char_phanthuong_up .= "SCFPCPoints=SCFPCPoints+$phanthuong_pcpoint";
                            }
                            if($phanthuong_pmaster > 0) {
                                if($online_check == 0) {
                                    kiemtra_online($login);
                                    $online_check = 1;
                                }
                                
                                $nbb_pmaster_q = "SELECT nbb_pmaster FROM Character WHERE AccountID='$login' AND Name='$name'";
                                $nbb_pmaster_r = $db->Execute($nbb_pmaster_q);
                                    check_queryerror($nbb_pmaster_q, $nbb_pmaster_r);
                                $nbb_pmaster_f = $nbb_pmaster_r->FetchRow();
                                $nbb_pmaster_before = $nbb_pmaster_f[0];
                                $nbb_pmaster_after = $nbb_pmaster_before + $phanthuong_pmaster;
                                
                                if(strlen($log_Des_phanthuong) > 0) $log_Des_phanthuong .= " + ";
                                $log_Des_phanthuong .= "$phanthuong_pmaster Skill Master ($nbb_pmaster_before -> <strong>$nbb_pmaster_after</strong>)";
                                $notice .=  "<div class='item-box' title='Điểm Skill Master'><img src='templates/images/icons/skill-master-point.png' /><span>x". $phanthuong_pmaster ."</span></div>";
                                
                                if(strlen($char_phanthuong_up) > 0) $char_phanthuong_up .= ",";
                                $char_phanthuong_up .= "SCFMasterPoints=SCFMasterPoints+$phanthuong_pmaster,nbb_pmaster=$nbb_pmaster_after";
                            }
                            
                            if($phanthuong_exp > 0) {
                                if($online_check == 0) {
                                    kiemtra_online($login);
                                    $online_check = 1;
                                }
                                
                                if(strlen($log_Des_phanthuong) > 0) $log_Des_phanthuong .= " + ";
                                $log_Des_phanthuong .= "Bùa Exp $phanthuong_exp giờ";
                                $notice .=  "<div class='item-box' title='Bùa Exp ". $phanthuong_exp ." giờ'><img src='templates/images/icons/exp.png' /><span>x". $phanthuong_exp ." h</span></div>";
                                
                                if(strlen($char_phanthuong_up) > 0) $char_phanthuong_up .= ",";
                                
                                $buff_get_q = "SELECT SCFSealItem, SCFSealTime FROM Character WHERE Name='$name'";
                                $buff_get_r = $db->Execute($buff_get_q);
                                    check_queryerror($buff_get_q, $buff_get_r);
                                $buff_get_f = $buff_get_r->FetchRow();
                                
                                if($buff_get_f[1] > $timestamp) {
                                    $SCFSealItem_new = $buff_get_f[0];
                                    $SCFSealTime_new = $buff_get_f[1] + $phanthuong_exp*60*60;
                                } else {
                                    $SCFSealItem_new = 6700;
                                    $SCFSealTime_new = $timestamp + $phanthuong_exp*60*60;
                                }
                                
                                $char_phanthuong_up .= "SCFSealItem=$SCFSealItem_new, SCFSealTime=$SCFSealTime_new";
                            }
                            
                            
                            if( (isset($phanthuong_item_time) && strlen($phanthuong_item_time) > 0 && $phanthuong_item_time_day > 0) || (isset($phanthuong_item) && strlen($phanthuong_item) > 0) ) {
                                $char_chk_q = "SELECT count(*) FROM Character WHERE AccountID='$login' AND Name='$name' AND Resets> $phanthuong_item_reset";
                                $char_chk_r = $db->Execute($char_chk_q);
                                $char_chk_f = $char_chk_r->FetchRow();
                                if($char_chk_f[0] > 0) {
                                    // Phan thuong vinh vien
                                    if(isset($phanthuong_item) && strlen($phanthuong_item) > 0) {
                                        $warehouse_query = "SELECT CAST(Items AS image) FROM warehouse WHERE AccountID = '$login'";
                                        $warehouse_result = $db->Execute($warehouse_query);
                                            check_queryerror($warehouse_query, $warehouse_result);
                                        $warehouse_c = $warehouse_result->NumRows();
                                        if($warehouse_c == 0) {
                                            echo "Chưa mở Rương đồ chung. Hãy vào Game, mở Rương đồ chung, sau đó thoát Game rồi nhận thưởng.";
                                            exit();
                                        }
                                        $warehouse_fetch = $warehouse_result->FetchRow();
                                        $warehouse = $warehouse_fetch[0];
                                        $warehouse = bin2hex($warehouse);
                                        $warehouse = strtoupper($warehouse);
                                        
                                        $warehouse1 = substr($warehouse,0,120*32);
                                        $warehouse2 = substr($warehouse,120*32);
                                        
                                        if(strlen($warehouse1) < 120*32) {
                                            echo "Chưa 1 lần mở hòm đồ chung. Vui lòng vào Game mở hòm đồ chung ít nhất 1 lần mới được nhận thưởng GiftCode.";
                                            exit();
                                        }
                                        
                                        include_once('config_license.php');
                                        include_once('func_getContent.php');
                                        $getcontent_url = $url_license . "/api_quest_daily.php";
                                        $getcontent_data = array(
                                            'acclic'    =>  $acclic,
                                            'key'    =>  $key,
                                            
                                            'item'    =>  $phanthuong_item,
                                            'warehouse1'   =>  $warehouse1
                                        );
                                        
                                        $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
                                        
                                        if ( empty($reponse) ) {
                                            $notice = "Server bảo trì vui lòng liên hệ Admin để FIX";
                                            echo $notice;
                                            exit();
                                        }
                                        else {
                                            $info = read_TagName($reponse, 'info');
                                            if($info == "Error") {
                                                $message = read_TagName($reponse, 'msg');
                                                echo $message;
                                                exit();
                                            } elseif($info == "OK") {
                                                $item_data = read_TagName($reponse, 'item_data');
                                                if(strlen($item_data) == 0) {
                                                    echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                                                    $arr_view = "\nDataSend:\n";
                                                    foreach($getcontent_data as $k => $v) {
                                                        $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                                                    }
                                                    writelog("log_api.txt", $arr_view . $reponse);
                                                    exit();
                                                }
                                            } else {
                                                echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
                                                writelog("log_api.txt", $reponse);
                                                exit();
                                            }
                                        }
                                        
                                        $item_data_arr = json_decode($item_data, true);
                                        $item_count = count($item_data_arr);
                                        if($item_count > 0) {
                                            $warehouse1_new = $warehouse1;
                                            for($i=0; $i<$item_count; $i++) {
                                                $item = $item_data_arr[$i]['code'];
                                                    
                                                $item_seri = substr($item, 6, 8);
                                                $item_seri_dec = hexdec($item_seri);
                                                
                                                if($item_seri_dec < hexdec('FFFFFFF0')) {
                                                	$serial = _getSerial();
                                                	$item = substr_replace($item, $serial, 6, 8);
                                                }
                                                $warehouse1_new = substr_replace($warehouse1_new, $item, $item_data_arr[$i]['vitri']*32, 32);
                                            }
                                            kiemtra_online($login);
                                            
                                            $warehouse_new = $warehouse1_new . $warehouse2;
                                            
                                            $warehouse_update_query = "UPDATE warehouse SET Items=0x$warehouse_new WHERE AccountID='$login'";
                                        }
                                    }
                                    
                                    
                                    // Phan thuong co thoi han
                                    if(isset($phanthuong_item_time) && strlen($phanthuong_item_time) > 0 && $phanthuong_item_time_day > 0) {
                                        include_once('config_license.php');
                                        include_once('func_getContent.php');
                                        $getcontent_url = $url_license . "/api_quest_daily.php";
                                        $getcontent_data = array(
                                            'acclic'    =>  $acclic,
                                            'key'    =>  $key,
                                            
                                            'type'  =>  'item_time',
                                            'item_time'    =>  $phanthuong_item_time
                                        );
                                        
                                        $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
                                        
                                        if ( empty($reponse) ) {
                                            $notice = "Server bảo trì vui lòng liên hệ Admin để FIX";
                                            echo $notice;
                                            exit();
                                        }
                                        else {
                                            $info = read_TagName($reponse, 'info');
                                            if($info == "Error") {
                                                $message = read_TagName($reponse, 'msg');
                                                echo $message;
                                                exit();
                                            } elseif($info == "OK") {
                                                $item_time = read_TagName($reponse, 'item_time');
                                                $item_time_arr = json_decode($item_time, true);
                                                if(strlen($item_time) == 0) {
                                                    echo "Dữ liệu trả về lỗi. Vui lòng liên hệ Admin để FIX";
                                                    $arr_view = "\nDataSend:\n";
                                                    foreach($getcontent_data as $k => $v) {
                                                        $arr_view .= "\t". $k ."\t=>\t". $v .",\n"; 
                                                    }
                                                    writelog("log_api.txt", $arr_view . $reponse);
                                                    exit();
                                                }
                                            } else {
                                                echo "Kết nối API gặp sự cố. Vui lòng liên hệ nhà cung cấp DWebMU để kiểm tra.";
                                                writelog("log_api.txt", $reponse);
                                                exit();
                                            }
                                        }
                                    }
                                }
                            }
                            
                            if($phanthuong_giftcode != 999999) {
                                //Đọc File GiftCode Type
                                $file_giftcode_type = 'config/giftcode_type.txt';
                                $giftcode_type_arr = array();
                                if(is_file($file_giftcode_type)) {
                            		$fopen_host = fopen($file_giftcode_type, "r");
                                    $giftcode_type_read = fgets($fopen_host);
                                    
                                    $giftcode_type_arr = json_decode($giftcode_type_read, true);
                            	} else $fopen_host = fopen($file_giftcode_type, "w");
                            	fclose($fopen_host);
                                
                                if(!is_array($giftcode_type_arr)) $giftcode_type_arr = array();
                                
                                include_once('config_license.php');
                                include_once('func_getContent.php');
                                $getcontent_url = $url_license . "/api_giftcode_create.php";
                                $getcontent_data = array(
                                    'acclic'    =>  $acclic,
                                    'key'    =>  $key,
                                    
                                    'gift_slg'  =>  1
                                ); 
                                
                                $reponse = _getContent($getcontent_url, $getcontent_data, $getcontent_method, $getcontent_curl);
                            
                            	if ( empty($reponse) ) {
                                    $err .= "Server bao tri vui long lien he Admin de FIX";
                                    $err_flag = true;
                                }
                                else {
                                    $info = read_TagName($reponse, 'info');
                                    if($info == "Error") {
                                        $message = read_TagName($reponse, 'message');
                                        $err .= $message;
                                        $err_flag = true;
                                    } elseif($info == "OK") {
                                        $giftcode = read_TagName($reponse, 'giftcode');
                                        if(strlen($giftcode) == 0) {
                                            $err .= "Du lieu tra ve loi. Vui long lien he Admin de FIX";
                                            $err_flag = true;
                                        }
                                    } else {
                                        $err .= "Ket noi API gap su co. Admin MU vui long lien he nha cung cap DWebMu de kiem tra";
                                        $err_flag = true;
                                    }
                                }
                                
                                if(strlen($log_Des_phanthuong) > 0) $log_Des_phanthuong .= " + ";
                                $log_Des_phanthuong .= $giftcode_type_arr[$phanthuong_giftcode]['name'];
                                $notice .=  "<div class='item-box' title='". $giftcode_type_arr[$phanthuong_giftcode]['name'] ."'><img src='templates/images/icons/Gift-icon.png' /></div>";
                                
                                $giftcode_insert_query = "INSERT INTO GiftCode (gift_code, acc, type, giftcode_type, gift_time, ngay, status) VALUES ('$giftcode', '". $login ."', 4, $phanthuong_giftcode, $timestamp, '".date("Y-m-d",$timestamp)."', 1)";
                                $giftcode_insert_result = $db->Execute($giftcode_insert_query);
                                    check_queryerror($giftcode_insert_query, $giftcode_insert_result);
                            }
                            
                            if(strlen($phanthuong_up) > 0) {
                                $phanthuong_update_q = "UPDATE MEMB_INFO SET {$phanthuong_up} WHERE memb___id='$login'";
                                $phanthuong_update_r = $db->Execute($phanthuong_update_q);
                                    check_queryerror($phanthuong_update_q, $phanthuong_update_r);
                            }
                            
                            if(strlen($char_phanthuong_up) > 0) {
                                $char_phanthuong_update_q = "UPDATE Character SET {$char_phanthuong_up} WHERE AccountID='$login' AND Name='$name'";
                                $char_phanthuong_update_r = $db->Execute($char_phanthuong_update_q);
                                    check_queryerror($char_phanthuong_update_q, $char_phanthuong_update_r);
                            }
                            
                            if(count($item_time_arr) > 0) {
                                $item_time_count = count($item_time_arr);
                                
                                for($i=0; $i< $item_time_count; $i++) {
                                	$nbb_time_del = $timestamp + 24*60*60;
                                    $item_time_insert_q = "INSERT INTO Titan_Rewards (AccountID, Name, Zen, VIPMoney, Num, Lvl, Opt, Luck, Skill, Dur, Excellent, Ancient, JOH, Sock1, Sock2, Sock3, Sock4, Sock5, Days, SerialFFFFFFFE, type, nbb_time_add, nbb_time_del) VALUES ('$login', '$name', 0, 0, ". $item_time_arr[$i]['Num'] .", ". $item_time_arr[$i]['Lvl'] .", ". $item_time_arr[$i]['Opt'] .", ". $item_time_arr[$i]['Luck'] .", ". $item_time_arr[$i]['Skill'] .", ". $item_time_arr[$i]['Dur'] .", ". $item_time_arr[$i]['Excellent'] .", ". $item_time_arr[$i]['Ancient'] .", ". $item_time_arr[$i]['JOH'] .", ". $item_time_arr[$i]['Sock1'] .", ". $item_time_arr[$i]['Sock2'] .", ". $item_time_arr[$i]['Sock3'] .", ". $item_time_arr[$i]['Sock4'] .", ". $item_time_arr[$i]['Sock5'] .", ". $phanthuong_item_time_day .", ". 0 .", 99, $timestamp, $nbb_time_del)";
                                    $item_time_insert_r = $db->Execute($item_time_insert_q);
                                        check_queryerror($item_time_insert_q, $item_time_insert_r);
                                }
                                
                                $phanthuong_item_time_info_arr = json_decode($phanthuong_item_time_info, true);
                                foreach($phanthuong_item_time_info_arr as $v) {
                                    if(strlen($log_Des_phanthuong) > 0) $log_Des_phanthuong .= " + ";
                                    $log_Des_phanthuong .= $v['item_name'] ." ". $phanthuong_item_time_day ." ngày";
                                    $notice .= "<div class='item-box' title='". $v['item_name'] ." ". $phanthuong_item_time_day ." ngày'><img src='items/". $v['item_image'] .".gif' height='35' /><span style='color: red;'>{$phanthuong_item_time_day}d</span></div>";
                                }
                            }
                            
                            if(strlen($warehouse_update_query) > 0) {
                                $warehouse_update_result = $db->Execute($warehouse_update_query);
                                    check_queryerror($warehouse_update_query, $warehouse_update_result);
                                    
                                $phanthuong_item_info_arr = json_decode($phanthuong_item_info, true);
                                foreach($phanthuong_item_info_arr as $v) {
                                    if(strlen($log_Des_phanthuong) > 0) $log_Des_phanthuong .= " + ";
                                    $log_Des_phanthuong .= $v['item_name'];
                                    $notice .= "<div class='item-box' title='". $v['item_name'] ." vĩnh viễn'><img src='items/". $v['item_image'] .".gif' height='35' /><span style='color: blue;'>vĩnh viễn</span></div>";
                                }
                            }
                            
                            $quest_finish_q = "INSERT INTO nbb_quest_daily (acc, name, qindex, date) VALUES ('$login', '$name', $qindex, '$timenow_s')";
                            $quest_finish_r = $db->Execute($quest_finish_q);
                                check_queryerror($quest_finish_q, $quest_finish_r);
                            
                            if($phanthuong_pl > 0 || $phanthuong_pl_extra > 0) {
                                $pl_incre = $phanthuong_pl + $phanthuong_pl_extra;
                                
                                $pldaily_check_q = "SELECT count(acc) FROM nbb_pl_daily WHERE acc='$login' AND date='$datenow'";
                                $pldaily_check_r = $db->Execute($pldaily_check_q);
                                    check_queryerror($pldaily_check_q, $pldaily_check_r);
                                $pldaily_check_f = $pldaily_check_r->FetchRow();
                                if($pldaily_check_f[0] == 0) {
                                    $pldaily_q = "INSERT INTO nbb_pl_daily (acc, plpoint, date) VALUES ('$login', $pl_incre, '$datenow')";
                                    
                                } else {
                                    $pldaily_q = "UPDATE nbb_pl_daily SET plpoint = plpoint + $pl_incre WHERE acc='$login' AND date='$datenow'";
                                }
                                    $pldaily_r = $db->Execute($pldaily_q);
                                        check_queryerror($pldaily_q, $pldaily_r);
                            }
                            
                            //Ghi vào Log
                            switch ($qindex){ 
                            	case 1:
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                case 6:
                                    $quest_name = "Online ". $quest_daily_dk[$qindex] ." giờ";
                            	break;
                                
                                case 7:
                                case 8:
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                    $quest_name = "RS Tổng ". $quest_daily_dk[$qindex] ." lần";
                            	break;
                            
                            	case 14:
                                case 15:
                                case 16:
                                case 17:
                                case 18:
                                case 19:
                                case 20:
                                    $quest_name = "RS VIP Tổng ". $quest_daily_dk[$qindex] ." lần";
                            	break;
                            
                            	case 21:
                                case 22:
                                case 23:
                                case 24:
                                case 25:
                                case 26:
                                case 27:
                                    $quest_name = "RS OVER Tổng ". $quest_daily_dk[$qindex] ." lần";
                            	break;
                                
                                case 28:
                                case 29:
                                case 30:
                                case 31:
                                case 32:
                                case 33:
                                case 34:
                                    $quest_name = "RS OVER VIP ". $quest_daily_dk[$qindex] ." lần";
                            	break;
                            
                            	case 35:
                                    $quest_name = "Nạp thẻ lần đầu";
                            	break;
                            
                            	case 36:
                                case 37:
                                case 38:
                                case 39:
                                case 40:
                                case 41:
                                case 42:
                                    $quest_name = "Nạp thẻ " . number_format($quest_daily_dk[$qindex], 0, ',', '.') ." VNĐ";
                            	break;
                            
                          		case 43:
                                case 44:
                                case 45:
                                case 46:
                                case 47:
                                case 48:
                                case 49:
                                    $quest_name = "Sử dụng tiền tệ " . number_format($quest_daily_dk[$qindex], 0, ',', '.');
                            	break;
                            	
                                default :
                                    $quest_name = "Chưa định nghĩa";
                            }
                            $log_price = "";
                            $log_Des = "<strong>$name</strong> đã hoàn thành nhiệm vụ <strong>$quest_name</strong>.<br />Nhận : $log_Des_phanthuong";
                            _writelog_tiente($login, $log_price, $log_Des);
                        //End Ghi vào Log
                        
                            $money_get_q = "SELECT gcoin, gcoin_km, vpoint, vpoint_km, WCoin, nbb_pl, nbb_pl_extra FROM MEMB_INFO WHERE memb___id='$login'";
                            $money_get_r = $db->Execute($money_get_q);
                                check_queryerror($money_get_q, $money_get_r);
                            $money_get_f = $money_get_r->FetchRow();
                            
                            $quest_arr['quest_finish']++;
                            $qwait--;
                            
                            echo "<info>OK</info><qwait>$qwait</qwait><qfinish>". $quest_arr['quest_finish'] ."</qfinish><msg>Hoàn thành nhiệm vụ.<br /> <strong>Đã Nhận</strong> :<br />$notice</msg><gcoin>$money_get_f[0]</gcoin><gcoin_km>$money_get_f[1]</gcoin_km><vpoint>$money_get_f[2]</vpoint><vpoint_km>$money_get_f[3]</vpoint_km><WCoin>$money_get_f[4]</WCoin><nbb_pl>$money_get_f[5]</nbb_pl><nbb_pl_extra>$money_get_f[6]</nbb_pl_extra><chao_add>$phanthuong_chao</chao_add><cre_add>$phanthuong_cre</cre_add><blue_add>$phanthuong_blue</blue_add>";
                        } else {
                            echo "Chưa hoàn thành nhiệm vụ. Không thể nhận giải.";
                        }
                    }
                } else {
                    echo "Dữ liệu nhiệm vụ không đúng.";
                }
            }
        break;
        
        default :
            $nvchinh_query = "SELECT TOP 1 Name FROM Character WHERE AccountID='$login' ORDER BY Relifes DESC, Resets DESC";
            $nvchinh_result = $db->Execute($nvchinh_query);
                check_queryerror($nvchinh_query, $nvchinh_result);
            $nvchinh_fetch = $nvchinh_result->FetchRow();
            if($nvchinh_fetch[0] != $name) {
                $quest_arr['nvchinh'] = 0;
            } else {
                $quest_arr['nvchinh'] = 1;
                
                $quest_data = _quest_daily($login, $name);
                foreach($quest_data as $qk => $qv) {
                    $quest_arr[$qk] = $qv;
                }
            }
            
            
            $quest_data = json_encode($quest_arr);
            echo "<nbb>OK<nbb>$quest_data<nbb>";
    }

}

?>