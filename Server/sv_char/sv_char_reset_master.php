<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/
include('config/config_rsmaster.php');

$login = $_POST['login'];
$name = $_POST['name'];
$pass2 = $_POST['pass2'];

$passtransfer = $_POST['passtransfer'];

if ($passtransfer == $transfercode) {   // Begin Pass Transfer

$string_login = $_POST['string_login'];
checklogin($login,$string_login);

if(check_nv($login, $name) == 0) {
    echo "Nhân vật <b>{$name}</b> không nằm trong tài khoản <b>{$login}</b>. Vui lòng kiểm tra lại."; exit();
}

kiemtra_pass2($login,$pass2);
kiemtra_char($login,$name);
kiemtra_doinv($login,$name);


$money_q = "Select gcoin, gcoin_km, vpoint, vpoint_km, bank From MEMB_INFO where memb___id='$login'";
$money_r = $db->Execute($money_q);
    check_queryerror($money_q, $money_r);
$money_f = $money_r->FetchRow();

$zen_bank_before = $money_f[4];

$gcoin_before = $money_f[0];
$gcoin_km_before = $money_f[1];
$vpoint_before = $money_f[2];
$vpoint_km_before = $money_f[3];

$gcoin_after = $gcoin_before;
$gcoin_km_after = $gcoin_km_before;
$vpoint_after = $vpoint_before;
$vpoint_km_after = $vpoint_km_before;

if($money_type == 2) {
    $vpoint_total = $vpoint_before + $vpoint_km_before;
    
    if ($vpoint_total < $rsmaster_price) {
        echo "Bạn có $vpoint_total Vcent. Bạn cần $rsmaster_price Vcent để Reset Master."; exit();
    } else {
        if($vpoint_km_before >= $rsmaster_price) $vpoint_km_after = $vpoint_km_before - $rsmaster_price;
        else {
            $vpoint_after = $vpoint_before - ($rsmaster_price - $vpoint_km_before);
            $vpoint_km_after = 0;
        }
    }
} else {
    $gcoin_total = $gcoin_before + $gcoin_km_before;
    
    if ($gcoin_total < $rsmaster_price) {
        echo "Bạn có $gcoin_total Gcent. Bạn cần $rsmaster_price Gcent để Reset Master."; exit();
    } else {
        if($gcoin_km_before >= $rsmaster_price) $gcoin_km_after = $gcoin_km_before - $rsmaster_price;
        else {
            $gcoin_after = $gcoin_before - ($rsmaster_price - $gcoin_km_before);
            $gcoin_km_after = 0;
        }
    }
}
    

$get_class_query = "SELECT Class, Money FROM Character WHERE Name='$name'";
$get_class_result = $db->Execute($get_class_query);
$get_class_f = $get_class_result->fetchrow();
$get_class = abs(intval($get_class_f[0]));
$zen_char_before = $get_class_f[1];

$zen_after = _price_zen($zen_char_before, $zen_bank_before, $rsmaster_price_zen);
if($zen_after['status'] == 0) {
    echo "Không đủ ZEN tiến hành Reset Master. Cần tối thiểu ". number_format($rsmaster_price_zen, 0, ',', '.') ." ZEN tổng trong ngân hàng và nhân vật.";
    exit();
}
$zen_bank_after = $zen_after['zen_bank'];
$zen_char_after = $zen_after['zen_char'];

$arr_classmaster = array(2,3,18,19,34,35,49,50,65,66,82,83,97,98);
if (in_array($get_class, $arr_classmaster, true)) 
{
	//Nếu là Server phát triển theo WebZen (WebZen, ENC,...)
    if( in_array($server_wz, array(1,2), true) )
	{
	   
        $nbb_pmaster_q = "SELECT nbb_pmaster FROM Character WHERE Name='$name'";
        $nbb_pmaster_r = $db->Execute($nbb_pmaster_q);
            check_queryerror($nbb_pmaster_q, $nbb_pmaster_r);
        $nbb_pmaster_f = $nbb_pmaster_r->FetchRow();
        $nbb_pmaster = $nbb_pmaster_f[0];
        
        
        $query_get_masterlv = "SELECT MASTER_LEVEL FROM T_MasterLevelSystem WHERE CHAR_NAME='$name'";
		$result_get_masterlv = $db->Execute($query_get_masterlv);
            check_queryerror($query_get_masterlv, $result_get_masterlv);
		$get_masterlv = $result_get_masterlv->fetchrow();
		$masterlv = $get_masterlv[0] + $nbb_pmaster;
		
		$sql_master_point = "UPDATE T_MasterLevelSystem SET ML_POINT='$masterlv' WHERE CHAR_NAME='$name'";
		$result_matser_point = $db->Execute($sql_master_point);
            check_queryerror($sql_master_point, $result_matser_point);
        
        $zen_char_update_q = "UPDATE Character SET Money=$zen_char_after WHERE Name='$name'";
        $zen_char_update_r = $db->Execute($zen_char_update_q);
            check_queryerror($zen_char_update_q, $zen_char_update_r);
        
	}
    
    elseif( in_array($server_wz, array(3), true) ) {
        $query_get_masterlv = "SELECT mLevel, nbb_pmaster FROM Character WHERE Name='$name'";
		$result_get_masterlv = $db->Execute($query_get_masterlv);
            check_queryerror($query_get_masterlv, $result_get_masterlv);
		$get_masterlv = $result_get_masterlv->fetchrow();
		$masterlv = $get_masterlv[0];
        $masterlv_extra = $get_masterlv[1];
        
        $master_p_total = $masterlv + $masterlv_extra;
		
		$sql_master_point = "UPDATE Character SET mlPoint=$master_p_total, MagicList=CONVERT(varbinary(350), null), Money=$zen_char_after WHERE Name='$name'";
		$result_matser_point = $db->Execute($sql_master_point);
            check_queryerror($sql_master_point, $result_matser_point);
    }
	//Nếu là Server khác WebZen (SCF,...)
	else {
		$query_get_masterlv = "SELECT SCFMasterLevel, nbb_pmaster FROM Character WHERE Name='$name'";
		$result_get_masterlv = $db->Execute($query_get_masterlv);
            check_queryerror($query_get_masterlv, $result_get_masterlv);
		$get_masterlv = $result_get_masterlv->fetchrow();
		$masterlv = $get_masterlv[0];
        $masterlv_extra = $get_masterlv[1];
        
        $master_p_total = $masterlv + $masterlv_extra;
		
		$sql_master_point = "UPDATE Character SET SCFMasterPoints=$master_p_total,SCFMasterSkill=CONVERT(varbinary(180), null), Money=$zen_char_after WHERE Name='$name'";
		$result_matser_point = $db->Execute($sql_master_point);
            check_queryerror($sql_master_point, $result_matser_point);
	}
    
    $money_update_q = "UPDATE MEMB_INFO SET gcoin=$gcoin_after, gcoin_km=$gcoin_km_after, vpoint=$vpoint_after, vpoint_km=$vpoint_km_after, bank=$zen_bank_after WHERE memb___id='$login'";
    $money_update_r = $db->Execute($money_update_q);
        check_queryerror($money_update_q, $money_update_r);
    
    _use_money($login, $gcoin_before - $gcoin_after, $gcoin_km_before - $gcoin_km_after, $vpoint_before - $vpoint_after, $vpoint_km_before - $vpoint_km_after);
    
    //Ghi vào Log
        if ($money_type == 2) 
        {
            $log_price = "- $rsmaster_price Vcent";
        } else 
        {
            $log_price = " - $rsmaster_price Gcent";
        }
        
        $log_Des = "<b>$name</b> Reset Master. LV Master : $masterlv. Point Master Extra : $masterlv_extra. Nhận : $master_p_total Point Master.";
        _writelog_tiente($login, $log_price, $log_Des);
    //End Ghi vào Log
    
    
    echo "
        <info>OK</info>
        <gcoin>$gcoin_after</gcoin>
        <gcoin_km>$gcoin_km_after</gcoin_km>
        <vpoint>$vpoint_after</vpoint>
        <vpoint_km>$vpoint_km_after</vpoint_km>
        <zen_char>$zen_char_after</zen_char>
        <zen_bank>$zen_bank_after</zen_bank>
        <msg>Reset Master Thành Công</msg>
    ";
}
else
{
    echo "
        <info>Err</info>
        <msg>Nhân vật không phải cấp độ Master | $get_class</msg>
    ";
}

} // End Pass Transfer

?>