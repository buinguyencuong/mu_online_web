<?php
/**
 * @author		Dwebgame
 * @copyright	2014 - 2016
 * @website		http://dwebgame.net
 * @HotLine		0169 330 22 84
 * @Version		VIP v1.01
*/

$login =  $_POST['login'];
$name =  $_POST['name'];
$strength =  $_POST['str'];     $strength = abs(intval($strength));
$dexterity =  $_POST['dex'];    $dexterity = abs(intval($dexterity));
$vitality =  $_POST['vit'];     $vitality = abs(intval($vitality));
$energy =  $_POST['ene'];       $energy = abs(intval($energy));
$menhlenh =  $_POST['ml'];      $menhlenh = abs(intval($menhlenh));
$keeppoint =  $_POST['keeppoint'];      $keeppoint = abs(intval($keeppoint));

$passtransfer = $_POST['passtransfer'];

if ($passtransfer == $transfercode) {

$string_login = $_POST['string_login'];
checklogin($login,$string_login);

if(check_nv($login, $name) == 0) {
    echo "Nhân vật <b>{$name}</b> không nằm trong tài khoản <b>{$login}</b>. Vui lòng kiểm tra lại."; exit();
}

kiemtra_doinv($login,$name);

$query = "SELECT Strength, Dexterity, Vitality, Energy, Leadership, LevelUpPoint, Class, pointdutru, Resets_Time from Character WHERE Name='$name'";
$result = $db->Execute($query);
    check_queryerror($query, $result);
$row = $result->fetchrow();
    
    $strength_get = $row[0];
    $dexterity_get = $row[1];
    $vitality_get = $row[2];
    $energy_get = $row[3];
    $menhlenh_get = $row[4];
    $leveluppoint = $row[5];
    $pointdutru = $row[7];
    $RS_Time = $row[8];
    
	if ($strength_get < 0) {$strength_get = $strength_get+65536;}
	if ($dexterity_get < 0) {$dexterity_get = $dexterity_get+65536;}
	if ($vitality_get < 0) {$vitality_get = $vitality_get+65536;}
	if ($energy_get < 0) {$energy_get = $energy_get+65536;}
	if ($menhlenh_get < 0) {$menhlenh_get = $menhlenh_get+65536;}
	
$new_str = $strength_get + $strength;
$new_agi = $dexterity_get + $dexterity;
$new_vit = $vitality_get + $vitality;
$new_ene = $energy_get + $energy;
$new_menh = $menhlenh_get + $menhlenh;

if($new_str > 65000) {
    echo "Điểm Sức Mạnh sau khi cộng $new_str vượt 65.000 . Sức mạnh tối đa chỉ đạt 65.000. Vui lòng cộng lại Sức Mạnh."; exit();
}
if($new_agi > 65000) {
    echo "Điểm Nhanh nhẹn sau khi cộng $new_agi vượt 65.000 . Nhanh nhẹn tối đa chỉ đạt 65.000. Vui lòng cộng lại Nhanh nhẹn."; exit();
}
if($new_vit > 65000) {
    echo "Điểm Thể lực sau khi cộng $new_vit vượt 65.000 . Thể lực tối đa chỉ đạt 65.000. Vui lòng cộng lại Thể lực."; exit();
}
if($new_ene > 65000) {
    echo "Điểm Năng lượng sau khi cộng $new_ene vượt 65.000 . Năng lượng tối đa chỉ đạt 65.000. Vui lòng cộng lại Năng lượng."; exit();
}
if($new_menh > 65000) {
    echo "Điểm Mệnh Lệnh sau khi cộng $new_menh vượt 65.000 . Mệnh Lệnh tối đa chỉ đạt 65.000. Vui lòng cộng lại Mệnh Lệnh."; exit();
}

$total_pointadd = $vitality + $strength + $energy + $dexterity + $menhlenh;
$total_point_chuatang = $leveluppoint + $pointdutru;
$thua = $total_point_chuatang - $total_pointadd;

 
if ($thua < 0) { 
        echo "Bạn chỉ có $total_point_chuatang điểm chưa cộng. Tổng điểm muốn cộng : $total_pointadd vượt quá số điểm chưa cộng $thua điểm."; exit();
}

$pointdutru_new = $pointdutru;
$leveluppoint_new = $leveluppoint;
if($pointdutru >= $total_pointadd) $pointdutru_new = $pointdutru - $total_pointadd;
else {
    $leveluppoint_new = $leveluppoint - ($total_pointadd - $pointdutru);
    $pointdutru_new = 0;
}

$default_point_q = "";
if($keeppoint == 1) {    // Trong vong 60s sau khi RS va tong diem ban dau <200
    $default_point_q = ", [nbb_point_default_str] = $strength, [nbb_point_default_agi] = $dexterity, [nbb_point_default_vit] = $vitality, [nbb_point_default_ene] = $energy";
}

$msquery = "UPDATE dbo.Character SET [Strength] = $new_str, [Dexterity] = $new_agi, [Vitality] = $new_vit, [Energy] = $new_ene, [Leadership] = $new_menh, [LevelUpPoint] = $leveluppoint_new, [pointdutru] = $pointdutru_new $default_point_q WHERE Name = '$name' AND AccountID = '$login'";
$msresults= $db->Execute($msquery);


echo "OK<nbb>$name đã cộng điểm thành công.<br>Sức mạnh: $new_str.<br>Nhanh nhẹn: $new_agi.<br>Sức khỏe: $new_vit.<br>Năng lượng: $new_ene.<br>Mệnh lệnh: $new_menh.<br>Bạn còn thừa $thua điểm.";
}

?>