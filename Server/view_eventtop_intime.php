<?php
/**
 * @author		NetBanBe
 * @copyright	2005 - 2012
 * @website		http://netbanbe.net
 * @Email		DWebMu@gmail.com
 * @HotLine		094 92 92 290
 * @Version		v5.12.0722
 * @Release		22/07/2012
 
 * WebSite hoan toan duoc thiet ke boi NetBanBe.
 * Vi vay, hay ton trong ban quyen tri tue cua NetBanBe
 * Hay ton trong cong suc, tri oc NetBanBe da bo ra de thiet ke nen DWebMu
 * Hay su dung ban quyen duoc cung cap boi NetBanBe de gop 1 phan nho chi phi phat trien DWebMu
 * Khong nen su dung DWebMu ban crack hoac tu nguoi khac dua cho. Nhung hanh dong nhu vay se lam kim ham su phat trien cua DWebMu do khong co kinh phi phat trien cung nhu san pham tri tue bi danh cap.
 * Cac ban hay su dung DWebMu duoc cung cap boi NetBanBe de NetBanBe co dieu kien phat trien them nhieu tinh nang hay hon, tot hon.
 * Cam on nhieu!
 */
 
	include_once("security.php");
include_once('config.php');
include_once('config/config_thehe.php');
include_once('config/config_event.php');
include_once('config/config_vip.php');
include_once('config/config_ranking.php');

$action = $_POST['action'];
$index = $_POST['index'];
    if(intval($index) < 1) $index = 1;
$passtransfer = $_POST['passtransfer'];

if ($passtransfer == $transfercode) {

    switch ($action)
    {
	case 'view_toprs':
		if(!isset($rank_event_rs_slg) || abs(intval($rank_event_rs_slg)) < 20) $rank_event_rs_slg = 20;
        
        if($index == 1) {
            $time_begin = strtotime($event_toprs_begin);
    		$time_end = strtotime($event_toprs_end) + 24*60*60 - 1;
		} else {
            $time_begin = strtotime($event_toprs[$index]['begin']);
    		$time_end = strtotime($event_toprs[$index]['end']) + 24*60*60 - 1;
            $event_toprs_rstype = $event_toprs[$index]['rstype'];
		}
        
        
        if(!isset($event_toprs_rstype) || $event_toprs_rstype == 0) {   // TOP Reset
            $query_danhvong_rstype = "SUM(reset_score_pri)";
            $query_rs_rstype = "SUM(reset_pri + reset_uythac)";
            $query_rs_time = "lastrs_time_pri";
        } else if($event_toprs_rstype == 1) {   // TOP RS + RS Over
            $query_danhvong_rstype = "SUM(reset_score)";
            $query_rs_rstype = "SUM(reset_top)";
            $query_rs_time = "lastrs_time_top";
        } else { //TOP RS + RS OVer + RS Uy Thac
            $query_danhvong_rstype = "SUM(reset_score)";
            $query_rs_rstype = "SUM(reset_all_top)";
            $query_rs_time = "lastrs_time_top";
        }
        
        // TOP Danh Vong
		for($i=1; $i<count($thehe_choise); $i++) {
            if(strlen($thehe_choise[$i]) > 0) {
                for($class_i=0; $class_i<=7; $class_i++) {
                    switch ($class_i) {
                    	case 1: $class_name = 'dw'; $class_query = " AND (Class = 0 OR Class = 1 OR Class = 2 OR Class = 3) "; break;         // DW
                        case 2: $class_name = 'dk'; $class_query = " AND (Class = 16 OR Class = 17 OR Class = 18 OR Class = 19) "; break;     // DK
                        case 3: $class_name = 'elf'; $class_query = " AND (Class = 32 OR Class = 33 OR Class = 34 OR Class = 35) "; break;     // ELF
                        case 4: $class_name = 'mg'; $class_query = " AND (Class = 48 OR Class = 49  OR Class = 50) "; break;                  // MG
                        case 5: $class_name = 'dl'; $class_query = " AND (Class = 64 OR Class = 65  OR Class = 66) "; break;                  // DL
                        case 6: $class_name = 'sum'; $class_query = " AND (Class = 80 OR Class = 81 OR Class = 82 OR Class = 83) "; break;     // SUM
                        case 7: $class_name = 'rf'; $class_query = " AND (Class = 96 OR Class = 97 OR Class = 98) "; break;                   // RF
                        default : $class_name = 'all'; $class_query = "";                                                                      // ALL
                    }
                    
                    $query = "SELECT TOP $rank_event_rs_slg A.name, $query_danhvong_rstype AS ResetScoreTotal, Class, (SELECT TOP 1 lastrs_time FROM TopResetScore C WHERE C.name=A.name AND lastrs_time <= $time_end ORDER BY lastrs_time DESC) AS lastreset_time, nbb_vip_time, nbb_vip_time_begin FROM TopResetScore A JOIN MEMB_INFO B ON A.acc collate DATABASE_DEFAULT = B.memb___id collate DATABASE_DEFAULT AND thehe=$i AND [time] >= {$time_begin} AND [time] <= {$time_end} JOIN Character ON A.name collate DATABASE_DEFAULT = Character.Name collate DATABASE_DEFAULT ". $class_query ." GROUP BY A.name, Class, Thehe, nbb_vip_time, nbb_vip_time_begin ORDER BY ResetScoreTotal DESC, lastreset_time";
                    $result = $db->Execute($query);
                        check_queryerror($query, $result);
                    
                    while($row = $result->fetchrow()) 	{
            			if($row[4] > $timestamp) {
                            if($row[5] > 0) {
                                $nbb_vip_time_used = $timestamp - $row[5];
                                $nbb_vip_time_used_day = floor($nbb_vip_time_used/(24*60*60));
                                $vip = 1;
                                if(is_array($vip_level_day)) {
                                    foreach($vip_level_day as $k => $v) {
                                        if($nbb_vip_time_used_day >= $v) $vip = $k+1;
                                        else break;
                                    }
                                }
                            } else $vip = 1;
                        } else $vip = 0;
                        
                        $vip_day = 0;
                        if($vip > 1) $vip_day = $vip_level_day[$vip - 1];
                        
                        $event_toprs_array['danhvong'][$i][$class_name][] = array(
                            'name' => $row[0],
                            'datatop'  =>  $row[1],
                            'class' =>  $row[2],
                            'vip' =>  $vip,
                            'vip_day' =>  $vip_day,
                            'lastrs_time' =>  $row[3]
                        );
            		}
                }
            }
		}
        
        // Top Reset
        for($i=1; $i<count($thehe_choise); $i++) {
            if(strlen($thehe_choise[$i]) > 0) {
                for($class_i=0; $class_i<=7; $class_i++) {
                    switch ($class_i) {
                    	case 1: $class_name = 'dw'; $class_query = " AND (Class = 0 OR Class = 1 OR Class = 2 OR Class = 3) "; break;         // DW
                        case 2: $class_name = 'dk'; $class_query = " AND (Class = 16 OR Class = 17 OR Class = 18 OR Class = 19) "; break;     // DK
                        case 3: $class_name = 'elf'; $class_query = " AND (Class = 32 OR Class = 33 OR Class = 34 OR Class = 35) "; break;     // ELF
                        case 4: $class_name = 'mg'; $class_query = " AND (Class = 48 OR Class = 49  OR Class = 50) "; break;                  // MG
                        case 5: $class_name = 'dl'; $class_query = " AND (Class = 64 OR Class = 65  OR Class = 66) "; break;                  // DL
                        case 6: $class_name = 'sum'; $class_query = " AND (Class = 80 OR Class = 81 OR Class = 82 OR Class = 83) "; break;     // SUM
                        case 7: $class_name = 'rf'; $class_query = " AND (Class = 96 OR Class = 97 OR Class = 98) "; break;                   // RF
                        default : $class_name = 'all'; $class_query = "";                                                                      // ALL
                    }
                    
                    $query = "SELECT TOP $rank_event_rs_slg A.name, $query_rs_rstype AS ResetTotal, Class, (SELECT TOP 1 $query_rs_time FROM TopReset C WHERE C.name=A.name AND $query_rs_time <= $time_end ORDER BY $query_rs_time DESC) AS lastreset_time, nbb_vip_time, nbb_vip_time_begin FROM TopReset A JOIN MEMB_INFO B ON A.acc collate DATABASE_DEFAULT = B.memb___id collate DATABASE_DEFAULT AND thehe=$i AND [time] >= {$time_begin} AND [time] <= {$time_end} JOIN Character ON A.name collate DATABASE_DEFAULT = Character.Name collate DATABASE_DEFAULT ". $class_query ." GROUP BY A.name, Class, Thehe, nbb_vip_time, nbb_vip_time_begin ORDER BY ResetTotal DESC, lastreset_time";
                    $result = $db->Execute($query);
                        check_queryerror($query, $result);
                    
                    while($row = $result->fetchrow()) 	{
            			if($row[4] > $timestamp) {
                            if($row[5] > 0) {
                                $nbb_vip_time_used = $timestamp - $row[5];
                                $nbb_vip_time_used_day = floor($nbb_vip_time_used/(24*60*60));
                                $vip = 1;
                                if(is_array($vip_level_day)) {
                                    foreach($vip_level_day as $k => $v) {
                                        if($nbb_vip_time_used_day >= $v) $vip = $k+1;
                                        else break;
                                    }
                                }
                            } else $vip = 1;
                        } else $vip = 0;
                        
                        $vip_day = 0;
                        if($vip > 1) $vip_day = $vip_level_day[$vip - 1];
                        
                        $event_toprs_array['rs'][$i][$class_name][] = array(
                            'name' => $row[0],
                            'datatop'  =>  $row[1],
                            'class' =>  $row[2],
                            'vip' =>  $vip,
                            'vip_day' =>  $vip_day,
                            'lastrs_time' =>  $row[3]
                        );
            		}
                }
            }
		}
        
        $event_toprs_data = json_encode($event_toprs_array);
        echo '<nbb>'. $event_toprs_data .'<nbb>';
            
		break;
		
	case 'view_toppoint':
		if(!isset($rank_other_slg) || abs(intval($rank_other_slg)) < 20) $rank_other_slg = 20;
		for($i=1; $i<count($thehe_choise); $i++) {
            if(strlen($thehe_choise[$i]) > 0) {
                $query = "SELECT TOP $rank_other_slg name, SUM(points), nbb_vip_time, nbb_vip_time_begin FROM Event_TOP_Point JOIN MEMB_INFO ON Event_TOP_Point.acc collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=$i AND [time] >= '$event_toppoint_begin' AND [time] <= '$event_toppoint_end' GROUP BY name, nbb_vip_time, nbb_vip_time_begin ORDER BY SUM(points) DESC";
        		$result = $db->Execute($query);

        		while($row = $result->fetchrow()) 	{
                    if($row[2] > $timestamp) {
                        if($row[3] > 0) {
                            $nbb_vip_time_used = $timestamp - $row[3];
                            $nbb_vip_time_used_day = floor($nbb_vip_time_used/(24*60*60));
                            $vip = 1;
                            if(is_array($vip_level_day)) {
                                foreach($vip_level_day as $k => $v) {
                                    if($nbb_vip_time_used_day >= $v) $vip = $k+1;
                                    else break;
                                }
                            }
                        } else $vip = 1;
                    } else $vip = 0;
                        
                    $vip_day = 0;
                    if($vip > 1) $vip_day = $vip_level_day[$vip - 1];
                    
                    $event_toppoint_array[$i][] = array(
                        'name' => $row[0],
                        'datatop'  =>  $row[1],
                        'vip' =>  $vip,
                        'vip_day' =>  $vip_day
                    );
        		}
            }
		}
        
        $event_toppoint_data = json_encode($event_toppoint_array);
        echo '<nbb>'. $event_toppoint_data .'<nbb>';
                
		break;
		
	case 'view_topcard':
		if(!isset($rank_event_card_slg) || abs(intval($rank_event_card_slg)) < 20) $rank_event_card_slg = 20;
        if($index == 1) {
            $event_topcard_begin = $event_topcard_begin;
    		$event_topcard_end = $event_topcard_end;
		} else {
            $event_topcard_begin = $event_topcard[$index]['begin'];
    		$event_topcard_end = $event_topcard[$index]['end'];
		}
		for($i=1; $i<count($thehe_choise); $i++) {
            if(strlen($thehe_choise[$i]) > 0) {
                $query = "SELECT TOP $rank_event_card_slg acc, SUM(Event_TOP_Card.gcoin), nbb_vip_time, nbb_vip_time_begin FROM Event_TOP_Card JOIN MEMB_INFO ON Event_TOP_Card.acc collate DATABASE_DEFAULT = MEMB_INFO.memb___id collate DATABASE_DEFAULT AND thehe=$i AND [time] >= '$event_topcard_begin' AND [time] <= '$event_topcard_end' GROUP BY acc, nbb_vip_time, nbb_vip_time_begin ORDER BY SUM(Event_TOP_Card.gcoin) DESC";
        		$result = $db->Execute($query);
  
        		while($row = $result->fetchrow()) 	{
        			if($row[2] > $timestamp) {
                        if($row[3] > 0) {
                            $nbb_vip_time_used = $timestamp - $row[3];
                            $nbb_vip_time_used_day = floor($nbb_vip_time_used/(24*60*60));
                            $vip = 1;
                            if(is_array($vip_level_day)) {
                                foreach($vip_level_day as $k => $v) {
                                    if($nbb_vip_time_used_day >= $v) $vip = $k+1;
                                    else break;
                                }
                            }
                        } else $vip = 1;
                    } else $vip = 0;
                        
                    $vip_day = 0;
                    if($vip > 1) $vip_day = $vip_level_day[$vip - 1];
                    
                    $sql_char_check = $db->Execute("Select TOP 1 Name From Character where AccountID='$row[0]' ORDER BY Relifes DESC, Resets DESC, cLevel DESC");
        			$char_check = $sql_char_check->fetchrow();
                    
                    $event_topcard_array[$i][] = array(
                        'name' => $char_check[0],
                        'datatop'  =>  $row[1],
                        'vip' =>  $vip,
                        'vip_day' =>  $vip_day
                    );
        		}
            }
		}
        
        $event_topcard_data = json_encode($event_topcard_array);
        echo '<nbb>'. $event_topcard_data .'<nbb>';
        
		break;
			
    }
}
?>